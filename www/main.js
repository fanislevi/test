(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/admin/admin.component.css":
/*!*******************************************!*\
  !*** ./src/app/admin/admin.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAvYWRtaW4vYWRtaW4uY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/admin/admin.component.html":
/*!********************************************!*\
  !*** ./src/app/admin/admin.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<link  href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css\"  rel=\"stylesheet\">\n\n\n<div class=\"container-fluid\">\n   \n  \n  <div class=\"row\">\n     \n    \n        \n      <div class=\"col-sm-12\" align=\"center\" style=\"background-color:blue;\"  >   <input type=\"radio\" name=\"Emily\" value=\"Emily\" [(ngModel)]=\"Emily\" checked >Emily Breakfast<br> </div>\n      \n      <div class=\"col-sm-12\" align=\"center\" style=\"background-color:blue;\"  >   <input type=\"text\" name=\"usr_time\" [(ngModel)]=\"usr_time\"><br> </div> \n   \n      <div class=\"col-sm-6\" style=\"background-color:blue;\"><input type=\"radio\" name=\"sitting\" value=\"sitting\" [(ngModel)]=\"sitting\" checked >WakeUp<br></div>\n      <div class=\"col-sm-6\" style=\"background-color:blue;\"><input type=\"radio\" name=\"sitting\" value=\"notsitting\" [(ngModel)]=\"notsitting\">WakeUp<br></div>\n\n      <div class=\"col-sm-6\" style=\"background-color:blue;\"><input type=\"radio\" name=\"house\" value=\"inhouse\" [(ngModel)]=\"inhouse\" checked > Inhousesensor<br></div>\n      <div class=\"col-sm-6\" style=\"background-color:blue;\"><input type=\"radio\" name=\"house\" value=\"outhouse\" [(ngModel)]=\"outhouse\"> Outhousesensor<br></div>\n\n      <div class=\"col-sm-6\" style=\"background-color:blue;\"><input type=\"radio\" name=\"heart\" value=\"beats\" [(ngModel)]=\"beats\" checked > Heart beats<br></div>\n      <div class=\"col-sm-6\" style=\"background-color:blue;\"><input type=\"radio\" name=\"heart\" value=\"notbeats\" [(ngModel)]=\"notbeats\"> Heart not beats<br></div>\n  \n    \n\n      <div class=\"col-sm-12\"   align=\"center\" style=\"background-color:red;\"  > <button type=\"button\"   style=\"background-color:red;\"  (click)=\"submitted=false\" (click)=\"sensorValues()\">SENSORS</button>  </div>\n   \n\n\n\n      \n      \n      <div class=\"col-sm-12\"  align=\"center\"  style=\"background-color:red;\"><input id=\"file\" type=\"file\" accept=\".csv\" (change)=\"fileUpload($event.target.files)\"></div>\n   \n   \n      <h1>\n         {{title}}\n      </h1>\n     \n     <!--\n      <div class=\"col-sm-12\"   align=\"center\" style=\"background-color:red;\"  >  <button (click)=\"notification()\">Notification</button>  </div>-->\n      <div class=\"col-sm-12\"   align=\"center\" style=\"background-color:red;\"  > <button type=\"button\"   style=\"background-color:red;\"  (click)=\"submitted=false\" (click)=\"submit()\">Menu</button>  </div>\n   \n      <div class=\"col-sm-12\"   align=\"center\" style=\"background-color:red;\"  > <button type=\"button\"   style=\"background-color:red;\"  (click)=\"submitted=false\" (click)=\"makeProgram()\">Program Make</button>  </div>\n    </div>\n  </div>\n \n\n\n"

/***/ }),

/***/ "./src/app/admin/admin.component.ts":
/*!******************************************!*\
  !*** ./src/app/admin/admin.component.ts ***!
  \******************************************/
/*! exports provided: AdminComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminComponent", function() { return AdminComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./src/node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _admin_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./admin.service */ "./src/app/admin/admin.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _calendar_calendar_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../calendar/calendar.service */ "./src/app/calendar/calendar.service.ts");
/* harmony import */ var ngx_push_notifications__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-push-notifications */ "./node_modules/ngx-push-notifications/fesm5/ngx-push-notifications.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");








var AdminComponent = /** @class */ (function () {
    function AdminComponent(_pushNotificationService, AdminService, CalendarService, http, router) {
        this._pushNotificationService = _pushNotificationService;
        this.AdminService = AdminService;
        this.CalendarService = CalendarService;
        this.http = http;
        this.router = router;
        this.url = "http://localhost:3000/api/things/getmenu/breakfast/";
        this.urlinter1 = "http://localhost:3000/api/things/getmenu/inter1/";
        this.urlunch = "http://localhost:3000/api/things/getmenu/lunch/";
        this.urlinter2 = "http://localhost:3000/api/things/getmenu/inter2/";
        this.urldinner = "http://localhost:3000/api/things/getmenu/dinner/";
    }
    AdminComponent.prototype.sensorValues = function () {
        this.sensorhouse = jquery__WEBPACK_IMPORTED_MODULE_2__("input[name='house']:checked").val();
        this.sensorheart = jquery__WEBPACK_IMPORTED_MODULE_2__("input[name='heart']:checked").val();
        this.sensorsitting = jquery__WEBPACK_IMPORTED_MODULE_2__("input[name='sitting']:checked").val();
        //alert("Sensor is:"+this.sensorhouse+this.sensorheart+this.sensorsitting+this.usr_time);
        this.AdminService.setSensors(this.sensorhouse, this.sensorheart, this.sensorsitting, this.usr_time).subscribe(function (res) {
            // alert("Sensor called");
            //alert("Data post each time");
            // alert(JSON.stringify(res));
            //var i=JSON.stringify(res);
            // var stringify = JSON.parse(i);
            //alert("Hier vars"+stringify.name);
            //alert(stringify['name']);
        }, function (error) {
            alert("error hier");
        });
    };
    AdminComponent.prototype.notification = function () {
        function test() {
            // alert("finalaa");
            //alert(location.host);
            //alert(location.hostname);
            // alert("concat");
            // alert(location.host.toString().concat("/main"));
            var i = location.host.toString().concat("/notification").toString().trim();
            // alert(i);
            var all = "http://".concat(i);
            // alert("all are");
            //alert(all);
            window.location.href = all; // i;//'http://www.google.com';
        }
        var title = 'Meal Recommendation';
        var options = new ngx_push_notifications__WEBPACK_IMPORTED_MODULE_6__["PushNotificationOptions"]();
        options.body = 'Its time for meal now';
        options.icon = 'https://banner2.kisspng.com/20180219/zqq/kisspng-hamburger-fast-food-restaurant-junk-food-kfc-cartoon-french-fries-5a8b6353d9a269.5412864515190843718914.jpg'; //adding an icon
        this._pushNotificationService.create(title, options).subscribe(function (notif) {
            if (notif.event.type === 'show') {
                console.log('onshow');
                setTimeout(function () {
                    // notif.notification.close();
                }, 9000);
            }
            if (notif.event.type === 'click') {
                console.log('click');
                alert("Clicked");
                //notif.notification.close();
                // this.goToPage("/","calendar");
                test();
                notif.notification.close();
            }
            if (notif.event.type === 'close') {
                console.log('close');
            }
        }, function (err) {
            console.log(err);
        });
    };
    AdminComponent.prototype.goToPage = function (pagename, parameter) {
        this.router.navigate([pagename, parameter]);
    };
    AdminComponent.prototype.fileUpload = function (files) {
        var _this = this;
        /* this.sensorhouse = $("input[name='house']:checked").val();
         this.sensorheart = $("input[name='heart']:checked").val();
         this.sensorsitting=$("input[name='sitting']:checked").val();
        // this.sensoralarm=$("input[name='usr_time']:checked").val();*/
        alert("Sensor is:" + this.sensorhouse + this.sensorheart + this.sensorsitting + this.usr_time);
        // alert("file upload called");
        if (files && files.length > 0) {
            var file = files.item(0);
            var reader_1 = new FileReader();
            reader_1.readAsText(file);
            reader_1.onload = function (e) {
                var res = reader_1.result; // This variable contains your file as text
                var lines = res.split('\n'); // Splits you file into lines
                var date = [];
                var minuteslightlyactive = [];
                var minutesfairlyactive = [];
                var minutesverylyactive = [];
                lines.forEach(function (line) {
                    date.push(line.split(',')[3]);
                    minuteslightlyactive.push(line.split(',')[0]);
                    minutesfairlyactive.push(line.split(',')[1]);
                    minutesverylyactive.push(line.split(',')[2]);
                    ///////////////////////////////////////////////////////////////
                    var name = jquery__WEBPACK_IMPORTED_MODULE_2__("input[name='Emily']:checked").val();
                    //alert("name is"+name);
                    _this.AdminService.setWearable(name, line.split(',')[3], line.split(',')[0], String((line.split(',')[1])), String((line.split(',')[2])), "", "", "", "").subscribe(function (res) {
                        //alert("Data post each time");
                        // alert(JSON.stringify(res));
                        //var i=JSON.stringify(res);
                        // var stringify = JSON.parse(i);
                        //alert("Hier vars"+stringify.name);
                        //alert(stringify['name']);
                    }, function (error) {
                        alert("error hier");
                    });
                    ////////////////////////////////////////////////////////////////
                });
                // alert(date);
                // alert(minutesfairlyactive);
                // alert(minutesfairlyactive);
                // alert(minutesverylyactive);
                //console.log(minuteslightlyactive);
            };
        }
    };
    AdminComponent.prototype.makeProgram = function () {
        var _this = this;
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        var day = +dd;
        var days = day;
        var todays = mm + '/' + days + '/' + yyyy;
        function delay(ms) {
            return new Promise(function (resolve) { return setTimeout(resolve, ms); });
        }
        var daymenu = "monday";
        this.http.get("" + this.url + daymenu, { responseType: "json" }).subscribe(function (response) {
            alert("Response for one value is" + response[0].energycontent);
            _this.CalendarService.setTempCalendar("Emily", response[0].name, response[0].type, response[0].energycontent, response[0].nutrition, response[0].wayofcooking, response[0].calories, response[0].kg, response[0].info, daymenu, response[0].imageurl).subscribe(function (res) {
                // alert("Set temp calendar hier");
            }, function (error) {
                alert("error hier");
            });
        });
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(1000)];
                    case 1:
                        _a.sent();
                        this.http.get("" + this.urlinter1 + daymenu, { responseType: "json" }).subscribe(function (response) {
                            alert("Response for one value is" + response[0].energycontent);
                            _this.CalendarService.setTempCalendar("Emily", response[0].name, response[0].type, response[0].energycontent, response[0].nutrition, response[0].wayofcooking, response[0].calories, response[0].kg, response[0].info, daymenu, response[0].imageurl).subscribe(function (res) {
                                // alert("Set temp calendar hier");
                            }, function (error) {
                                alert("error hier");
                            });
                        });
                        return [2 /*return*/];
                }
            });
        }); })();
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(2000)];
                    case 1:
                        _a.sent();
                        this.http.get("" + this.urlunch + daymenu, { responseType: "json" }).subscribe(function (response) {
                            alert("Response for one value is" + response[0].energycontent);
                            _this.CalendarService.setTempCalendar("Emily", response[0].name, response[0].type, response[0].energycontent, response[0].nutrition, response[0].wayofcooking, response[0].calories, response[0].kg, response[0].info, daymenu, response[0].imageurl).subscribe(function (res) {
                                // alert("Set temp calendar hier");
                            }, function (error) {
                                alert("error hier");
                            });
                        });
                        return [2 /*return*/];
                }
            });
        }); })();
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(3000)];
                    case 1:
                        _a.sent();
                        this.http.get("" + this.urlinter2 + daymenu, { responseType: "json" }).subscribe(function (response) {
                            alert("Response for one value is" + response[0].energycontent);
                            _this.CalendarService.setTempCalendar("Emily", response[0].name, response[0].type, response[0].energycontent, response[0].nutrition, response[0].wayofcooking, response[0].calories, response[0].kg, response[0].info, daymenu, response[0].imageurl).subscribe(function (res) {
                                // alert("Set temp calendar hier");
                            }, function (error) {
                                alert("error hier");
                            });
                        });
                        return [2 /*return*/];
                }
            });
        }); })();
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(4000)];
                    case 1:
                        _a.sent();
                        this.http.get("" + this.urldinner + daymenu, { responseType: "json" }).subscribe(function (response) {
                            alert("Response for one value is" + response[0].energycontent);
                            _this.CalendarService.setTempCalendar("Emily", response[0].name, response[0].type, response[0].energycontent, response[0].nutrition, response[0].wayofcooking, response[0].calories, response[0].kg, response[0].info, daymenu, response[0].imageurl).subscribe(function (res) {
                                // alert("Set temp calendar hier");
                            }, function (error) {
                                alert("error hier");
                            });
                        });
                        return [2 /*return*/];
                }
            });
        }); })();
        var daymenu2 = "tuesday";
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(5000)];
                    case 1:
                        _a.sent();
                        this.http.get("" + this.url + daymenu2, { responseType: "json" }).subscribe(function (response) {
                            alert("Response for one value is" + response[0].energycontent);
                            //alert("Response for one value is"+response[0].name);
                            _this.CalendarService.setTempCalendar("Emily", response[0].name, response[0].type, response[0].energycontent, response[0].nutrition, response[0].wayofcooking, response[0].calories, response[0].kg, response[0].info, daymenu2, response[0].imageurl).subscribe(function (res) {
                                // alert("Set temp calendar hier");
                            }, function (error) {
                                alert("error hier");
                            });
                        });
                        return [2 /*return*/];
                }
            });
        }); })();
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(6000)];
                    case 1:
                        _a.sent();
                        this.http.get("" + this.urlinter1 + daymenu2, { responseType: "json" }).subscribe(function (response) {
                            // alert("Response for one value is"+response[0].name);
                            _this.CalendarService.setTempCalendar("Emily", response[0].name, response[0].type, response[0].energycontent, response[0].nutrition, response[0].wayofcooking, response[0].calories, response[0].kg, response[0].info, daymenu2, response[0].imageurl).subscribe(function (res) {
                                // alert("Set temp calendar hier");
                            }, function (error) {
                                alert("error hier");
                            });
                        });
                        return [2 /*return*/];
                }
            });
        }); })();
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(7000)];
                    case 1:
                        _a.sent();
                        this.http.get("" + this.urlunch + daymenu2, { responseType: "json" }).subscribe(function (response) {
                            alert("Breakfast hier" + response[0].type);
                            //alert("Response for one value is"+response[0].name);
                            _this.CalendarService.setTempCalendar("Emily", response[0].name, response[0].type, response[0].energycontent, response[0].nutrition, response[0].wayofcooking, response[0].calories, response[0].kg, response[0].info, daymenu2, response[0].imageurl).subscribe(function (res) {
                                alert("Kableddd");
                            }, function (error) {
                                alert("error hier");
                            });
                        });
                        return [2 /*return*/];
                }
            });
        }); })();
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(8000)];
                    case 1:
                        _a.sent();
                        this.http.get("" + this.urlinter2 + daymenu2, { responseType: "json" }).subscribe(function (response) {
                            // alert("Response for one value is"+response[0].name);
                            _this.CalendarService.setTempCalendar("Emily", response[0].name, response[0].type, response[0].energycontent, response[0].nutrition, response[0].wayofcooking, response[0].calories, response[0].kg, response[0].info, daymenu2, response[0].imageurl).subscribe(function (res) {
                                // alert("Set temp calendar hier");
                            }, function (error) {
                                alert("error hier");
                            });
                        });
                        return [2 /*return*/];
                }
            });
        }); })();
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(9000)];
                    case 1:
                        _a.sent();
                        this.http.get("" + this.urldinner + daymenu2, { responseType: "json" }).subscribe(function (response) {
                            // alert("Response for one value is"+response[0].name);
                            _this.CalendarService.setTempCalendar("Emily", response[0].name, response[0].type, response[0].energycontent, response[0].nutrition, response[0].wayofcooking, response[0].calories, response[0].kg, response[0].info, daymenu2, response[0].imageurl).subscribe(function (res) {
                                // alert("Set temp calendar hier");
                            }, function (error) {
                                alert("error hier");
                            });
                        });
                        return [2 /*return*/];
                }
            });
        }); })();
        ///////////////////////////
        var daymenu3 = "wednesday";
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(10000)];
                    case 1:
                        _a.sent();
                        this.http.get("" + this.url + daymenu3, { responseType: "json" }).subscribe(function (response) {
                            alert("Breakfast hier" + response[0].type);
                            //alert("Response for one value is"+response[0].name);
                            _this.CalendarService.setTempCalendar("Emily", response[0].name, response[0].type, response[0].energycontent, response[0].nutrition, response[0].wayofcooking, response[0].calories, response[0].kg, response[0].info, daymenu3, response[0].imageurl).subscribe(function (res) {
                                // alert("Set temp calendar hier");
                            }, function (error) {
                                alert("error hier");
                            });
                        });
                        return [2 /*return*/];
                }
            });
        }); })();
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(11000)];
                    case 1:
                        _a.sent();
                        this.http.get("" + this.urlinter1 + daymenu3, { responseType: "json" }).subscribe(function (response) {
                            // alert("Response for one value is"+response[0].name);
                            _this.CalendarService.setTempCalendar("Emily", response[0].name, response[0].type, response[0].energycontent, response[0].nutrition, response[0].wayofcooking, response[0].calories, response[0].kg, response[0].info, daymenu3, response[0].imageurl).subscribe(function (res) {
                                // alert("Set temp calendar hier");
                            }, function (error) {
                                alert("error hier");
                            });
                        });
                        return [2 /*return*/];
                }
            });
        }); })();
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(12000)];
                    case 1:
                        _a.sent();
                        this.http.get("" + this.urlunch + daymenu3, { responseType: "json" }).subscribe(function (response) {
                            alert("Breakfast hier" + response[0].type);
                            //alert("Response for one value is"+response[0].name);
                            _this.CalendarService.setTempCalendar("Emily", response[0].name, response[0].type, response[0].energycontent, response[0].nutrition, response[0].wayofcooking, response[0].calories, response[0].kg, response[0].info, daymenu3, response[0].imageurl).subscribe(function (res) {
                                alert("Kableddd");
                            }, function (error) {
                                alert("error hier");
                            });
                        });
                        return [2 /*return*/];
                }
            });
        }); })();
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(13000)];
                    case 1:
                        _a.sent();
                        this.http.get("" + this.urlinter2 + daymenu3, { responseType: "json" }).subscribe(function (response) {
                            // alert("Response for one value is"+response[0].name);
                            _this.CalendarService.setTempCalendar("Emily", response[0].name, response[0].type, response[0].energycontent, response[0].nutrition, response[0].wayofcooking, response[0].calories, response[0].kg, response[0].info, daymenu3, response[0].imageurl).subscribe(function (res) {
                                // alert("Set temp calendar hier");
                            }, function (error) {
                                alert("error hier");
                            });
                        });
                        return [2 /*return*/];
                }
            });
        }); })();
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(14000)];
                    case 1:
                        _a.sent();
                        this.http.get("" + this.urldinner + daymenu3, { responseType: "json" }).subscribe(function (response) {
                            // alert("Response for one value is"+response[0].name);
                            _this.CalendarService.setTempCalendar("Emily", response[0].name, response[0].type, response[0].energycontent, response[0].nutrition, response[0].wayofcooking, response[0].calories, response[0].kg, response[0].info, daymenu3, response[0].imageurl).subscribe(function (res) {
                                // alert("Set temp calendar hier");
                            }, function (error) {
                                alert("error hier");
                            });
                        });
                        return [2 /*return*/];
                }
            });
        }); })();
        var daymenu4 = "thursday";
        /////////////////////////////////////////////
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(15000)];
                    case 1:
                        _a.sent();
                        this.http.get("" + this.url + daymenu4, { responseType: "json" }).subscribe(function (response) {
                            alert("Breakfast hier" + response[0].type);
                            //alert("Response for one value is"+response[0].name);
                            _this.CalendarService.setTempCalendar("Emily", response[0].name, response[0].type, response[0].energycontent, response[0].nutrition, response[0].wayofcooking, response[0].calories, response[0].kg, response[0].info, daymenu4, response[0].imageurl).subscribe(function (res) {
                                // alert("Set temp calendar hier");
                            }, function (error) {
                                alert("error hier");
                            });
                        });
                        return [2 /*return*/];
                }
            });
        }); })();
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(16000)];
                    case 1:
                        _a.sent();
                        this.http.get("" + this.urlinter1 + daymenu4, { responseType: "json" }).subscribe(function (response) {
                            // alert("Response for one value is"+response[0].name);
                            _this.CalendarService.setTempCalendar("Emily", response[0].name, response[0].type, response[0].energycontent, response[0].nutrition, response[0].wayofcooking, response[0].calories, response[0].kg, response[0].info, daymenu4, response[0].imageurl).subscribe(function (res) {
                                // alert("Set temp calendar hier");
                            }, function (error) {
                                alert("error hier");
                            });
                        });
                        return [2 /*return*/];
                }
            });
        }); })();
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(17000)];
                    case 1:
                        _a.sent();
                        this.http.get("" + this.urlunch + daymenu4, { responseType: "json" }).subscribe(function (response) {
                            alert("Breakfast hier" + response[0].type);
                            //alert("Response for one value is"+response[0].name);
                            _this.CalendarService.setTempCalendar("Emily", response[0].name, response[0].type, response[0].wayofcooking, response[0].nutrition, response[0].wayofcooking, response[0].calories, response[0].kg, response[0].info, daymenu4, response[0].imageurl).subscribe(function (res) {
                                alert("Kableddd");
                            }, function (error) {
                                alert("error hier");
                            });
                        });
                        return [2 /*return*/];
                }
            });
        }); })();
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(18000)];
                    case 1:
                        _a.sent();
                        this.http.get("" + this.urlinter2 + daymenu4, { responseType: "json" }).subscribe(function (response) {
                            // alert("Response for one value is"+response[0].name);
                            _this.CalendarService.setTempCalendar("Emily", response[0].name, response[0].type, response[0].wayofcooking, response[0].nutrition, response[0].wayofcooking, response[0].calories, response[0].kg, response[0].info, daymenu4, response[0].imageurl).subscribe(function (res) {
                                // alert("Set temp calendar hier");
                            }, function (error) {
                                alert("error hier");
                            });
                        });
                        return [2 /*return*/];
                }
            });
        }); })();
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(19000)];
                    case 1:
                        _a.sent();
                        this.http.get("" + this.urldinner + daymenu4, { responseType: "json" }).subscribe(function (response) {
                            // alert("Response for one value is"+response[0].name);
                            _this.CalendarService.setTempCalendar("Emily", response[0].name, response[0].type, response[0].wayofcooking, response[0].nutrition, response[0].wayofcooking, response[0].calories, response[0].kg, response[0].info, daymenu4, response[0].imageurl).subscribe(function (res) {
                                // alert("Set temp calendar hier");
                            }, function (error) {
                                alert("error hier");
                            });
                        });
                        return [2 /*return*/];
                }
            });
        }); })();
        ////////////////////////////////////////////////
        var daymenu5 = "friday";
        /////////////////////////////////////////////
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(20000)];
                    case 1:
                        _a.sent();
                        this.http.get("" + this.url + daymenu5, { responseType: "json" }).subscribe(function (response) {
                            alert("Breakfast hier" + response[0].type);
                            //alert("Response for one value is"+response[0].name);
                            _this.CalendarService.setTempCalendar("Emily", response[0].name, response[0].type, response[0].wayofcooking, response[0].nutrition, response[0].wayofcooking, response[0].calories, response[0].kg, response[0].info, daymenu5, response[0].imageurl).subscribe(function (res) {
                                // alert("Set temp calendar hier");
                            }, function (error) {
                                alert("error hier");
                            });
                        });
                        return [2 /*return*/];
                }
            });
        }); })();
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(21000)];
                    case 1:
                        _a.sent();
                        this.http.get("" + this.urlinter1 + daymenu5, { responseType: "json" }).subscribe(function (response) {
                            // alert("Response for one value is"+response[0].name);
                            _this.CalendarService.setTempCalendar("Emily", response[0].name, response[0].type, response[0].wayofcooking, response[0].nutrition, response[0].wayofcooking, response[0].calories, response[0].kg, response[0].info, daymenu5, response[0].imageurl).subscribe(function (res) {
                                // alert("Set temp calendar hier");
                            }, function (error) {
                                alert("error hier");
                            });
                        });
                        return [2 /*return*/];
                }
            });
        }); })();
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(22000)];
                    case 1:
                        _a.sent();
                        this.http.get("" + this.urlunch + daymenu5, { responseType: "json" }).subscribe(function (response) {
                            alert("Breakfast hier" + response[0].type);
                            //alert("Response for one value is"+response[0].name);
                            _this.CalendarService.setTempCalendar("Emily", response[0].name, response[0].type, response[0].wayofcooking, response[0].nutrition, response[0].wayofcooking, response[0].calories, response[0].kg, response[0].info, daymenu5, response[0].imageurl).subscribe(function (res) {
                                alert("Kableddd");
                            }, function (error) {
                                alert("error hier");
                            });
                        });
                        return [2 /*return*/];
                }
            });
        }); })();
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(23000)];
                    case 1:
                        _a.sent();
                        this.http.get("" + this.urlinter2 + daymenu5, { responseType: "json" }).subscribe(function (response) {
                            // alert("Response for one value is"+response[0].name);
                            _this.CalendarService.setTempCalendar("Emily", response[0].name, response[0].type, response[0].wayofcooking, response[0].nutrition, response[0].wayofcooking, response[0].calories, response[0].kg, response[0].info, daymenu5, response[0].imageurl).subscribe(function (res) {
                                // alert("Set temp calendar hier");
                            }, function (error) {
                                alert("error hier");
                            });
                        });
                        return [2 /*return*/];
                }
            });
        }); })();
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(24000)];
                    case 1:
                        _a.sent();
                        this.http.get("" + this.urldinner + daymenu5, { responseType: "json" }).subscribe(function (response) {
                            // alert("Response for one value is"+response[0].name);
                            _this.CalendarService.setTempCalendar("Emily", response[0].name, response[0].type, response[0].wayofcooking, response[0].nutrition, response[0].wayofcooking, response[0].calories, response[0].kg, response[0].info, daymenu5, response[0].imageurl).subscribe(function (res) {
                                // alert("Set temp calendar hier");
                            }, function (error) {
                                alert("error hier");
                            });
                        });
                        return [2 /*return*/];
                }
            });
        }); })();
        /////////////////////////////////////
    };
    AdminComponent.prototype.submit = function () {
        var _this = this;
        alert("Clicked");
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        var day = +dd;
        var days = day;
        var todays = mm + '/' + days + '/' + yyyy;
        this.AdminService.getRecipes("").subscribe(function (res) {
            for (var i in res) {
                alert("new image url is");
                alert(res[i].imageurl);
                _this.AdminService.setMenu(res[i].name, res[i].type, res[i].day, res[i].energycontent, res[i].wayofcooking, res[i].nutrition, res[i].calories, res[i].kg, res[i].info, res[i].imageurl).subscribe(function (res) {
                }, function (error) {
                    alert("error hier");
                });
            }
            function delay(ms) {
                return new Promise(function (resolve) { return setTimeout(resolve, ms); });
            }
            /*(async () => {
              await delay(1000);
            this.http.get('http://localhost:3000/api/things/getsensorvalues/niko',{responseType:"json"}).subscribe(
              response => {
              var sample=JSON.parse(JSON.stringify(response));
              var a=String(sample.status);
            
            
            });///hier end calendar
              
            
            })();*/
            //////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////
        }, function (error) {
            alert("error hier");
        });
    };
    AdminComponent.prototype.ngOnInit = function () {
        jquery__WEBPACK_IMPORTED_MODULE_2__("#main").hide();
        this._pushNotificationService.requestPermission();
        /* this.AdminService.getRecipes("").subscribe((res)=>{
            alert("resipes respose is:");
           for(var i in res){
              alert(res[i].name);
              alert(res[i].type);
           }
          }, (error) =>
          {
              alert("error hier");
             
          });*/
    };
    AdminComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-admin',
            template: __webpack_require__(/*! ./admin.component.html */ "./src/app/admin/admin.component.html"),
            styles: [__webpack_require__(/*! ./admin.component.css */ "./src/app/admin/admin.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [ngx_push_notifications__WEBPACK_IMPORTED_MODULE_6__["PushNotificationService"], _admin_service__WEBPACK_IMPORTED_MODULE_3__["AdminService"], _calendar_calendar_service__WEBPACK_IMPORTED_MODULE_5__["CalendarService"], _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"]])
    ], AdminComponent);
    return AdminComponent;
}());



/***/ }),

/***/ "./src/app/admin/admin.service.ts":
/*!****************************************!*\
  !*** ./src/app/admin/admin.service.ts ***!
  \****************************************/
/*! exports provided: AdminService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminService", function() { return AdminService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./src/node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");



var AdminService = /** @class */ (function () {
    function AdminService(httpClient) {
        this.httpClient = httpClient;
        this.apiURL = 'http://139.91.200.103:3000/api/things/setwearable';
        this.apiURLMENU = 'http://139.91.200.103:3000/api/things/setmenu';
        this.apiURLCAL = 'http://139.91.200.103:3000/api/things/setcalendarmenu';
        this.apiURLSEN = 'http://139.91.200.103:3000/api/things/setsensor/';
    }
    AdminService.prototype.setTempCalendar2 = function (username, name, type, energycontent, content, wayofcooking, nutrition, calories, kg, info, date) {
        var data = {
            "name": "fr"
        };
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
            })
        };
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
            .set('username', String(username))
            .set('name', String(name))
            .set('type', String(type))
            .set('food', String(content))
            .set('energycontent', String(energycontent))
            .set('wayofcooking', String(wayofcooking))
            .set('nutrition', String(nutrition))
            .set('calories', String(calories))
            .set('kg', String(kg))
            .set('info', String(info))
            .set('date', String(date));
        var options = {
            httpOptions: httpOptions,
            params: params,
            withCredentials: false
        };
        return this.httpClient.post("" + this.apiURLCAL, null, options);
    };
    AdminService.prototype.setWearable = function (username, date, minuteslightlyactive, minutesfairlyactive, minutesveryactive, sensor, sensor2, sensor3, sensor4) {
        var data = {
            "name": "fr"
        };
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
            })
        };
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
            .set('username', String(username))
            .set('date', String(date))
            .set('minuteslightlyactive', String(minuteslightlyactive))
            .set('minutesfairlyactive', String(minutesfairlyactive))
            .set('minutesveryactive', String(minutesveryactive))
            .set('sensorhouse', String(sensor))
            .set('sensorheart', String(sensor2))
            .set('sensorsitting', String(sensor3))
            .set('sensoralarm', String(sensor4));
        var options = {
            httpOptions: httpOptions,
            params: params,
            withCredentials: false
        };
        return this.httpClient.post("" + this.apiURL, null, options);
    };
    AdminService.prototype.setTempCalendar = function (username, name, type, energycontent, wayofcooking, nutrition, calories, kg, info, date) {
        var data = {
            "name": "fr"
        };
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
            })
        };
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
            .set('username', String(username))
            .set('name', String(name))
            .set('type', String(type))
            .set('energycontent', String(energycontent))
            .set('wayofcooking', String(wayofcooking))
            .set('nutrition', String(nutrition))
            .set('calories', String(calories))
            .set('kg', String(kg))
            .set('info', String(info))
            .set('date', String(date));
        var options = {
            httpOptions: httpOptions,
            params: params,
            withCredentials: false
        };
        return this.httpClient.post("" + this.apiURLCAL, null, options);
    };
    AdminService.prototype.setSensors = function (sensor, sensor2, sensor3, sensor4) {
        //  alert("Called setsensor");
        var data = {
            "name": "fr"
        };
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
            })
        };
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
            .set('sensorhouse', String(sensor))
            .set('sensorheart', String(sensor2))
            .set('sensorsitting', String(sensor3))
            .set('sensoralarm', String(sensor4));
        var options = {
            httpOptions: httpOptions,
            params: params,
            withCredentials: false
        };
        return this.httpClient.post("http://127.0.0.1:3000/api/things/setsensorsvalue/", null, options);
    };
    AdminService.prototype.setMenu = function (name, type, day, energycontent, wayofcooking, nutrition, calories, kg, info, imageurl) {
        var data = {
            "name": "fr"
        };
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
            })
        };
        // alert("energycontent"+String(energycontent));
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
            .set('name', String(name))
            .set('type', String(type))
            .set('day', String(day))
            .set('energycontent', String(energycontent))
            .set('wayofcooking', String(wayofcooking))
            .set('nutrition', String(nutrition))
            .set('calories', String(calories))
            .set('kg', String(kg))
            .set('info', String(info))
            .set('imageurl', String(imageurl));
        var options = {
            httpOptions: httpOptions,
            params: params,
            withCredentials: false
        };
        return this.httpClient.post("" + this.apiURLMENU, null, options);
    };
    AdminService.prototype.getRecipes = function (input) {
        var data = {
            "name": "fr"
        };
        return this.httpClient.get("http://localhost:3000/api/things/recipes", { responseType: "json" });
    };
    AdminService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], AdminService);
    return AdminService;
}());



/***/ }),

/***/ "./src/app/analytic/analytic.component.css":
/*!*************************************************!*\
  !*** ./src/app/analytic/analytic.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "body{background:black}\r\n#progress-container{width:80%;height:40px;background:#edebee;position:absolute;top:50%;left:50%;transform:translate(-50%,-50%)}\r\n#progress-bar{width:2%;height:100%;background:red;color:white;text-align:center;line-height:40px}\r\n#button1 {\r\n    background-color: #b2b2b2\t; \r\n    border: none;\r\n    color: white;\r\n    padding-bottom: 25px 22px;\r\n    text-align: center;\r\n    text-decoration: none;\r\n    display: inline-block;\r\n    font-size: 36px;\r\n    margin: 4px 18px;\r\n    cursor: pointer;\r\n  }\r\n#button2 {\r\n   background-color: #b2b2b2\t; \r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n#button3 {\r\n   background-color: #b2b2b2\t; \r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n#button4 {\r\n   background-color: #b2b2b2\t;\r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n#all{\r\n     text-align: center;\r\n     position: fixed;\r\n     left: 0;\r\n     bottom: 0;\r\n     width: 100%;\r\n     background-color: \t\t#b2b2b2;\r\n     color: white;\r\n     text-align: center;\r\n    \r\n  }\r\n#maincontenthome{\r\n    padding-bottom: 20%\r\n   \r\n }\r\ndiv.home {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\ndiv.home input {\r\n    background: url('home.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\ndiv.analytic {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\ndiv.analytic input {\r\n    background: url('analytic.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\ndiv.notification {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\ndiv.notification input {\r\n    background: url('outline-today-24px.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\ndiv.setting {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\ndiv.setting input {\r\n    background: url('settings.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\nspan.home {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 6px;\r\n  }\r\nspan.analytic {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 2px;\r\n  }\r\nspan.notification {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 2px;\r\n  }\r\nspan.setting {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 12px;\r\n  }\r\n#myProgress {\r\n    margin-top: 60px;\r\n    \r\n    border-radius: 45px;\r\n    width: 100%;\r\n    \r\n    background-color: #ddd;\r\n   \r\n  }\r\n#myBar {\r\n    margin-top: 60px;\r\n    \r\n    border-radius: 45px;\r\n    padding-top: 15px;\r\n    width: 1%;\r\n    height: 15px;\r\n    background-color: \t#0000FF;\r\n   \r\n  }\r\n* {\r\n      /* border: 1px solid red; */\r\n  }\r\n:root {\r\n      --font: \"Bitter\", serif;\r\n      --title-size: 36px;\r\n      --sub-size: 18px;\r\n  }\r\nbody {\r\n      background: #e5e5e5;\r\n  }\r\n.title h1 {\r\n      margin: 4px;\r\n      font-family: var(--font);\r\n      font-size: var(--title-size);\r\n      color: #333;\r\n  }\r\n.title p {\r\n      margin: 4px;\r\n      padding-bottom: 25px;\r\n      font-family: var(--font);\r\n      font-size: var(--sub-size);\r\n      color: #888;\r\n  }\r\n.container {\r\n      text-align: center;\r\n  }\r\n.github {\r\n      margin: 40px;\r\n  }\r\n.progress {\r\n      display: inline-block;\r\n      width: 400px;\r\n      height: 50px;\r\n      margin: 35px;\r\n      border-radius: 20px;\r\n      background: #f9f9f9;\r\n  }\r\n.bar {\r\n      border-radius: 20px;\r\n      width: 0%;\r\n      height: 100%;\r\n      transition: width;\r\n      transition-duration: 1s;\r\n      transition-timing-function: cubic-bezier(0.36, 0.55, 0.63, 0.48);\r\n      \r\n  }\r\n.mobile {\r\n      display: none;\r\n  }\r\n.shadow {\r\n      /* 25 50 */\r\n      box-shadow: 0px 45px 50px rgba(0, 0, 0, 0.25);\r\n  }\r\n.crosses {\r\n      background-color: #dfdbe5;\r\n      background-image: url(\"data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='25' height='25' viewBox='0 0 40 40'%3E%3Cg fill-rule='evenodd'%3E%3Cg fill='%239C92AC' fill-opacity='0.4'%3E%3Cpath d='M0 38.59l2.83-2.83 1.41 1.41L1.41 40H0v-1.41zM0 1.4l2.83 2.83 1.41-1.41L1.41 0H0v1.41zM38.59 40l-2.83-2.83 1.41-1.41L40 38.59V40h-1.41zM40 1.41l-2.83 2.83-1.41-1.41L38.59 0H40v1.41zM20 18.6l2.83-2.83 1.41 1.41L21.41 20l2.83 2.83-1.41 1.41L20 21.41l-2.83 2.83-1.41-1.41L18.59 20l-2.83-2.83 1.41-1.41L20 18.59z'/%3E%3C/g%3E%3C/g%3E%3C/svg%3E\");\r\n  }\r\n.jupiter {\r\n      background-color: #f395a5;\r\n      background-image: url(\"data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='52' height='52' viewBox='0 0 52 52'%3E%3Cpath fill='%23e6cca5' fill-opacity='0.4' d='M0 17.83V0h17.83a3 3 0 0 1-5.66 2H5.9A5 5 0 0 1 2 5.9v6.27a3 3 0 0 1-2 5.66zm0 18.34a3 3 0 0 1 2 5.66v6.27A5 5 0 0 1 5.9 52h6.27a3 3 0 0 1 5.66 0H0V36.17zM36.17 52a3 3 0 0 1 5.66 0h6.27a5 5 0 0 1 3.9-3.9v-6.27a3 3 0 0 1 0-5.66V52H36.17zM0 31.93v-9.78a5 5 0 0 1 3.8.72l4.43-4.43a3 3 0 1 1 1.42 1.41L5.2 24.28a5 5 0 0 1 0 5.52l4.44 4.43a3 3 0 1 1-1.42 1.42L3.8 31.2a5 5 0 0 1-3.8.72zm52-14.1a3 3 0 0 1 0-5.66V5.9A5 5 0 0 1 48.1 2h-6.27a3 3 0 0 1-5.66-2H52v17.83zm0 14.1a4.97 4.97 0 0 1-1.72-.72l-4.43 4.44a3 3 0 1 1-1.41-1.42l4.43-4.43a5 5 0 0 1 0-5.52l-4.43-4.43a3 3 0 1 1 1.41-1.41l4.43 4.43c.53-.35 1.12-.6 1.72-.72v9.78zM22.15 0h9.78a5 5 0 0 1-.72 3.8l4.44 4.43a3 3 0 1 1-1.42 1.42L29.8 5.2a5 5 0 0 1-5.52 0l-4.43 4.44a3 3 0 1 1-1.41-1.42l4.43-4.43a5 5 0 0 1-.72-3.8zm0 52c.13-.6.37-1.19.72-1.72l-4.43-4.43a3 3 0 1 1 1.41-1.41l4.43 4.43a5 5 0 0 1 5.52 0l4.43-4.43a3 3 0 1 1 1.42 1.41l-4.44 4.43c.36.53.6 1.12.72 1.72h-9.78zm9.75-24a5 5 0 0 1-3.9 3.9v6.27a3 3 0 1 1-2 0V31.9a5 5 0 0 1-3.9-3.9h-6.27a3 3 0 1 1 0-2h6.27a5 5 0 0 1 3.9-3.9v-6.27a3 3 0 1 1 2 0v6.27a5 5 0 0 1 3.9 3.9h6.27a3 3 0 1 1 0 2H31.9z'%3E%3C/path%3E%3C/svg%3E\");\r\n  }\r\n.piano {\r\n      background-color: #cccccc;\r\n      background-image: url(\"data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='70' height='46' viewBox='0 0 70 46'%3E%3Cg fill-rule='evenodd'%3E%3Cg fill='%23333333' fill-opacity='0.4'%3E%3Cpolygon points='68 44 62 44 62 46 56 46 56 44 52 44 52 46 46 46 46 44 40 44 40 46 38 46 38 44 32 44 32 46 26 46 26 44 22 44 22 46 16 46 16 44 12 44 12 46 6 46 6 44 0 44 0 42 8 42 8 28 6 28 6 0 12 0 12 28 10 28 10 42 18 42 18 28 16 28 16 0 22 0 22 28 20 28 20 42 28 42 28 28 26 28 26 0 32 0 32 28 30 28 30 42 38 42 38 0 40 0 40 42 48 42 48 28 46 28 46 0 52 0 52 28 50 28 50 42 58 42 58 28 56 28 56 0 62 0 62 28 60 28 60 42 68 42 68 0 70 0 70 46 68 46'/%3E%3C/g%3E%3C/g%3E%3C/svg%3E\");\r\n  }\r\n.dominos {\r\n      background-color: #fff6bd;\r\n      background-image: url(\"data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='126' height='84' viewBox='0 0 126 84'%3E%3Cg fill-rule='evenodd'%3E%3Cg fill='%23333333' fill-opacity='0.4'%3E%3Cpath d='M126 83v1H0v-2h40V42H0v-2h40V0h2v40h40V0h2v40h40V0h2v83zm-2-1V42H84v40h40zM82 42H42v40h40V42zm-50-6a4 4 0 1 1 0-8 4 4 0 0 1 0 8zM8 12a4 4 0 1 1 0-8 4 4 0 0 1 0 8zm96 12a4 4 0 1 1 0-8 4 4 0 0 1 0 8zm-42 0a4 4 0 1 1 0-8 4 4 0 0 1 0 8zm30-12a4 4 0 1 1 0-8 4 4 0 0 1 0 8zM20 54a4 4 0 1 1 0-8 4 4 0 0 1 0 8zm12 24a4 4 0 1 1 0-8 4 4 0 0 1 0 8zM8 54a4 4 0 1 1 0-8 4 4 0 0 1 0 8zm24 0a4 4 0 1 1 0-8 4 4 0 0 1 0 8zM8 78a4 4 0 1 1 0-8 4 4 0 0 1 0 8zm12 0a4 4 0 1 1 0-8 4 4 0 0 1 0 8zm54 0a4 4 0 1 1 0-8 4 4 0 0 1 0 8zM50 54a4 4 0 1 1 0-8 4 4 0 0 1 0 8zm24 0a4 4 0 1 1 0-8 4 4 0 0 1 0 8zM50 78a4 4 0 1 1 0-8 4 4 0 0 1 0 8zm54-12a4 4 0 1 1 0-8 4 4 0 0 1 0 8zm12 12a4 4 0 1 1 0-8 4 4 0 0 1 0 8zM92 54a4 4 0 1 1 0-8 4 4 0 0 1 0 8zm24 0a4 4 0 1 1 0-8 4 4 0 0 1 0 8zM92 78a4 4 0 1 1 0-8 4 4 0 0 1 0 8zm24-42a4 4 0 1 1 0-8 4 4 0 0 1 0 8z'/%3E%3C/g%3E%3C/g%3E%3C/svg%3E\");\r\n  }\r\n.pie {\r\n      background-color: #faaca8;\r\n      background-image: url(\"data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='50' height='50' viewBox='0 0 60 60'%3E%3Cg fill-rule='evenodd'%3E%3Cg fill='%23f55b53' fill-opacity='0.4' fill-rule='nonzero'%3E%3Cpath d='M29 58.58l7.38-7.39A30.95 30.95 0 0 1 29 37.84a30.95 30.95 0 0 1-7.38 13.36l7.37 7.38zm1.4 1.41l.01.01h-2.84l-7.37-7.38A30.95 30.95 0 0 1 6.84 60H0v-1.02a28.9 28.9 0 0 0 18.79-7.78L0 32.41v-4.84L18.78 8.79A28.9 28.9 0 0 0 0 1.02V0h6.84a30.95 30.95 0 0 1 13.35 7.38L27.57 0h2.84l7.39 7.38A30.95 30.95 0 0 1 51.16 0H60v27.58-.01V60h-8.84a30.95 30.95 0 0 1-13.37-7.4L30.4 60zM29 1.41l-7.4 7.38A30.95 30.95 0 0 1 29 22.16 30.95 30.95 0 0 1 36.38 8.8L29 1.4zM58 1A28.9 28.9 0 0 0 39.2 8.8L58 27.58V1.02zm-20.2 9.2A28.9 28.9 0 0 0 30.02 29h26.56L37.8 10.21zM30.02 31a28.9 28.9 0 0 0 7.77 18.79l18.79-18.79H30.02zm9.18 20.2A28.9 28.9 0 0 0 58 59V32.4L39.2 51.19zm-19-1.4a28.9 28.9 0 0 0 7.78-18.8H1.41l18.8 18.8zm7.78-20.8A28.9 28.9 0 0 0 20.2 10.2L1.41 29h26.57z'/%3E%3C/g%3E%3C/g%3E%3C/svg%3E\");\r\n  }\r\n.bees {\r\n      background-color: #fcc846;\r\n      background-image: url(\"data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='28' height='49' viewBox='0 0 28 49'%3E%3Cg fill-rule='evenodd'%3E%3Cg id='hexagons' fill='%237a4948' fill-opacity='0.83' fill-rule='nonzero'%3E%3Cpath d='M13.99 9.25l13 7.5v15l-13 7.5L1 31.75v-15l12.99-7.5zM3 17.9v12.7l10.99 6.34 11-6.35V17.9l-11-6.34L3 17.9zM0 15l12.98-7.5V0h-2v6.35L0 12.69v2.3zm0 18.5L12.98 41v8h-2v-6.85L0 35.81v-2.3zM15 0v7.5L27.99 15H28v-2.31h-.01L17 6.35V0h-2zm0 49v-8l12.99-7.5H28v2.31h-.01L17 42.15V49h-2z'/%3E%3C/g%3E%3C/g%3E%3C/svg%3E\");\r\n  }\r\n.food {\r\n      background-color: #ffcb05;\r\n      background-image: url(\"data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='120' height='120' viewBox='0 0 260 260'%3E%3Cg fill-rule='evenodd'%3E%3Cg fill='%23ec008c' fill-opacity='0.83'%3E%3Cpath d='M24.37 16c.2.65.39 1.32.54 2H21.17l1.17 2.34.45.9-.24.11V28a5 5 0 0 1-2.23 8.94l-.02.06a8 8 0 0 1-7.75 6h-20a8 8 0 0 1-7.74-6l-.02-.06A5 5 0 0 1-17.45 28v-6.76l-.79-1.58-.44-.9.9-.44.63-.32H-20a23.01 23.01 0 0 1 44.37-2zm-36.82 2a1 1 0 0 0-.44.1l-3.1 1.56.89 1.79 1.31-.66a3 3 0 0 1 2.69 0l2.2 1.1a1 1 0 0 0 .9 0l2.21-1.1a3 3 0 0 1 2.69 0l2.2 1.1a1 1 0 0 0 .9 0l2.21-1.1a3 3 0 0 1 2.69 0l2.2 1.1a1 1 0 0 0 .86.02l2.88-1.27a3 3 0 0 1 2.43 0l2.88 1.27a1 1 0 0 0 .85-.02l3.1-1.55-.89-1.79-1.42.71a3 3 0 0 1-2.56.06l-2.77-1.23a1 1 0 0 0-.4-.09h-.01a1 1 0 0 0-.4.09l-2.78 1.23a3 3 0 0 1-2.56-.06l-2.3-1.15a1 1 0 0 0-.45-.11h-.01a1 1 0 0 0-.44.1L.9 19.22a3 3 0 0 1-2.69 0l-2.2-1.1a1 1 0 0 0-.45-.11h-.01a1 1 0 0 0-.44.1l-2.21 1.11a3 3 0 0 1-2.69 0l-2.2-1.1a1 1 0 0 0-.45-.11h-.01zm0-2h-4.9a21.01 21.01 0 0 1 39.61 0h-2.09l-.06-.13-.26.13h-32.31zm30.35 7.68l1.36-.68h1.3v2h-36v-1.15l.34-.17 1.36-.68h2.59l1.36.68a3 3 0 0 0 2.69 0l1.36-.68h2.59l1.36.68a3 3 0 0 0 2.69 0L2.26 23h2.59l1.36.68a3 3 0 0 0 2.56.06l1.67-.74h3.23l1.67.74a3 3 0 0 0 2.56-.06zM-13.82 27l16.37 4.91L18.93 27h-32.75zm-.63 2h.34l16.66 5 16.67-5h.33a3 3 0 1 1 0 6h-34a3 3 0 1 1 0-6zm1.35 8a6 6 0 0 0 5.65 4h20a6 6 0 0 0 5.66-4H-13.1z'/%3E%3Cpath id='path6_fill-copy' d='M284.37 16c.2.65.39 1.32.54 2H281.17l1.17 2.34.45.9-.24.11V28a5 5 0 0 1-2.23 8.94l-.02.06a8 8 0 0 1-7.75 6h-20a8 8 0 0 1-7.74-6l-.02-.06a5 5 0 0 1-2.24-8.94v-6.76l-.79-1.58-.44-.9.9-.44.63-.32H240a23.01 23.01 0 0 1 44.37-2zm-36.82 2a1 1 0 0 0-.44.1l-3.1 1.56.89 1.79 1.31-.66a3 3 0 0 1 2.69 0l2.2 1.1a1 1 0 0 0 .9 0l2.21-1.1a3 3 0 0 1 2.69 0l2.2 1.1a1 1 0 0 0 .9 0l2.21-1.1a3 3 0 0 1 2.69 0l2.2 1.1a1 1 0 0 0 .86.02l2.88-1.27a3 3 0 0 1 2.43 0l2.88 1.27a1 1 0 0 0 .85-.02l3.1-1.55-.89-1.79-1.42.71a3 3 0 0 1-2.56.06l-2.77-1.23a1 1 0 0 0-.4-.09h-.01a1 1 0 0 0-.4.09l-2.78 1.23a3 3 0 0 1-2.56-.06l-2.3-1.15a1 1 0 0 0-.45-.11h-.01a1 1 0 0 0-.44.1l-2.21 1.11a3 3 0 0 1-2.69 0l-2.2-1.1a1 1 0 0 0-.45-.11h-.01a1 1 0 0 0-.44.1l-2.21 1.11a3 3 0 0 1-2.69 0l-2.2-1.1a1 1 0 0 0-.45-.11h-.01zm0-2h-4.9a21.01 21.01 0 0 1 39.61 0h-2.09l-.06-.13-.26.13h-32.31zm30.35 7.68l1.36-.68h1.3v2h-36v-1.15l.34-.17 1.36-.68h2.59l1.36.68a3 3 0 0 0 2.69 0l1.36-.68h2.59l1.36.68a3 3 0 0 0 2.69 0l1.36-.68h2.59l1.36.68a3 3 0 0 0 2.56.06l1.67-.74h3.23l1.67.74a3 3 0 0 0 2.56-.06zM246.18 27l16.37 4.91L278.93 27h-32.75zm-.63 2h.34l16.66 5 16.67-5h.33a3 3 0 1 1 0 6h-34a3 3 0 1 1 0-6zm1.35 8a6 6 0 0 0 5.65 4h20a6 6 0 0 0 5.66-4H246.9z'/%3E%3Cpath d='M159.5 21.02A9 9 0 0 0 151 15h-42a9 9 0 0 0-8.5 6.02 6 6 0 0 0 .02 11.96A8.99 8.99 0 0 0 109 45h42a9 9 0 0 0 8.48-12.02 6 6 0 0 0 .02-11.96zM151 17h-42a7 7 0 0 0-6.33 4h54.66a7 7 0 0 0-6.33-4zm-9.34 26a8.98 8.98 0 0 0 3.34-7h-2a7 7 0 0 1-7 7h-4.34a8.98 8.98 0 0 0 3.34-7h-2a7 7 0 0 1-7 7h-4.34a8.98 8.98 0 0 0 3.34-7h-2a7 7 0 0 1-7 7h-7a7 7 0 1 1 0-14h42a7 7 0 1 1 0 14h-9.34zM109 27a9 9 0 0 0-7.48 4H101a4 4 0 1 1 0-8h58a4 4 0 0 1 0 8h-.52a9 9 0 0 0-7.48-4h-42z'/%3E%3Cpath d='M39 115a8 8 0 1 0 0-16 8 8 0 0 0 0 16zm6-8a6 6 0 1 1-12 0 6 6 0 0 1 12 0zm-3-29v-2h8v-6H40a4 4 0 0 0-4 4v10H22l-1.33 4-.67 2h2.19L26 130h26l3.81-40H58l-.67-2L56 84H42v-6zm-4-4v10h2V74h8v-2h-8a2 2 0 0 0-2 2zm2 12h14.56l.67 2H22.77l.67-2H40zm13.8 4H24.2l3.62 38h22.36l3.62-38z'/%3E%3Cpath d='M129 92h-6v4h-6v4h-6v14h-3l.24 2 3.76 32h36l3.76-32 .24-2h-3v-14h-6v-4h-6v-4h-8zm18 22v-12h-4v4h3v8h1zm-3 0v-6h-4v6h4zm-6 6v-16h-4v19.17c1.6-.7 2.97-1.8 4-3.17zm-6 3.8V100h-4v23.8a10.04 10.04 0 0 0 4 0zm-6-.63V104h-4v16a10.04 10.04 0 0 0 4 3.17zm-6-9.17v-6h-4v6h4zm-6 0v-8h3v-4h-4v12h1zm27-12v-4h-4v4h3v4h1v-4zm-6 0v-8h-4v4h3v4h1zm-6-4v-4h-4v8h1v-4h3zm-6 4v-4h-4v8h1v-4h3zm7 24a12 12 0 0 0 11.83-10h7.92l-3.53 30h-32.44l-3.53-30h7.92A12 12 0 0 0 130 126z'/%3E%3Cpath d='M212 86v2h-4v-2h4zm4 0h-2v2h2v-2zm-20 0v.1a5 5 0 0 0-.56 9.65l.06.25 1.12 4.48a2 2 0 0 0 1.94 1.52h.01l7.02 24.55a2 2 0 0 0 1.92 1.45h4.98a2 2 0 0 0 1.92-1.45l7.02-24.55a2 2 0 0 0 1.95-1.52L224.5 96l.06-.25a5 5 0 0 0-.56-9.65V86a14 14 0 0 0-28 0zm4 0h6v2h-9a3 3 0 1 0 0 6H223a3 3 0 1 0 0-6H220v-2h2a12 12 0 1 0-24 0h2zm-1.44 14l-1-4h24.88l-1 4h-22.88zm8.95 26l-6.86-24h18.7l-6.86 24h-4.98zM150 242a22 22 0 1 0 0-44 22 22 0 0 0 0 44zm24-22a24 24 0 1 1-48 0 24 24 0 0 1 48 0zm-28.38 17.73l2.04-.87a6 6 0 0 1 4.68 0l2.04.87a2 2 0 0 0 2.5-.82l1.14-1.9a6 6 0 0 1 3.79-2.75l2.15-.5a2 2 0 0 0 1.54-2.12l-.19-2.2a6 6 0 0 1 1.45-4.46l1.45-1.67a2 2 0 0 0 0-2.62l-1.45-1.67a6 6 0 0 1-1.45-4.46l.2-2.2a2 2 0 0 0-1.55-2.13l-2.15-.5a6 6 0 0 1-3.8-2.75l-1.13-1.9a2 2 0 0 0-2.5-.8l-2.04.86a6 6 0 0 1-4.68 0l-2.04-.87a2 2 0 0 0-2.5.82l-1.14 1.9a6 6 0 0 1-3.79 2.75l-2.15.5a2 2 0 0 0-1.54 2.12l.19 2.2a6 6 0 0 1-1.45 4.46l-1.45 1.67a2 2 0 0 0 0 2.62l1.45 1.67a6 6 0 0 1 1.45 4.46l-.2 2.2a2 2 0 0 0 1.55 2.13l2.15.5a6 6 0 0 1 3.8 2.75l1.13 1.9a2 2 0 0 0 2.5.8zm2.82.97a4 4 0 0 1 3.12 0l2.04.87a4 4 0 0 0 4.99-1.62l1.14-1.9a4 4 0 0 1 2.53-1.84l2.15-.5a4 4 0 0 0 3.09-4.24l-.2-2.2a4 4 0 0 1 .97-2.98l1.45-1.67a4 4 0 0 0 0-5.24l-1.45-1.67a4 4 0 0 1-.97-2.97l.2-2.2a4 4 0 0 0-3.09-4.25l-2.15-.5a4 4 0 0 1-2.53-1.84l-1.14-1.9a4 4 0 0 0-5-1.62l-2.03.87a4 4 0 0 1-3.12 0l-2.04-.87a4 4 0 0 0-4.99 1.62l-1.14 1.9a4 4 0 0 1-2.53 1.84l-2.15.5a4 4 0 0 0-3.09 4.24l.2 2.2a4 4 0 0 1-.97 2.98l-1.45 1.67a4 4 0 0 0 0 5.24l1.45 1.67a4 4 0 0 1 .97 2.97l-.2 2.2a4 4 0 0 0 3.09 4.25l2.15.5a4 4 0 0 1 2.53 1.84l1.14 1.9a4 4 0 0 0 5 1.62l2.03-.87zM152 207a1 1 0 1 1 2 0 1 1 0 0 1-2 0zm6 2a1 1 0 1 1 2 0 1 1 0 0 1-2 0zm-11 1a1 1 0 1 1 2 0 1 1 0 0 1-2 0zm-6 0a1 1 0 1 1 2 0 1 1 0 0 1-2 0zm3-5a1 1 0 1 1 2 0 1 1 0 0 1-2 0zm-8 8a1 1 0 1 1 2 0 1 1 0 0 1-2 0zm3 6a1 1 0 1 1 2 0 1 1 0 0 1-2 0zm0 6a1 1 0 1 1 2 0 1 1 0 0 1-2 0zm4 7a1 1 0 1 1 2 0 1 1 0 0 1-2 0zm5-2a1 1 0 1 1 2 0 1 1 0 0 1-2 0zm5 4a1 1 0 1 1 2 0 1 1 0 0 1-2 0zm4-6a1 1 0 1 1 2 0 1 1 0 0 1-2 0zm6-4a1 1 0 1 1 2 0 1 1 0 0 1-2 0zm-4-3a1 1 0 1 1 2 0 1 1 0 0 1-2 0zm4-3a1 1 0 1 1 2 0 1 1 0 0 1-2 0zm-5-4a1 1 0 1 1 2 0 1 1 0 0 1-2 0zm-24 6a1 1 0 1 1 2 0 1 1 0 0 1-2 0zm16 5a5 5 0 1 0 0-10 5 5 0 0 0 0 10zm7-5a7 7 0 1 1-14 0 7 7 0 0 1 14 0zm86-29a1 1 0 0 0 0 2h2a1 1 0 0 0 0-2h-2zm19 9a1 1 0 0 1 1-1h2a1 1 0 0 1 0 2h-2a1 1 0 0 1-1-1zm-14 5a1 1 0 0 0 0 2h2a1 1 0 0 0 0-2h-2zm-25 1a1 1 0 0 0 0 2h2a1 1 0 0 0 0-2h-2zm5 4a1 1 0 0 0 0 2h2a1 1 0 0 0 0-2h-2zm9 0a1 1 0 0 1 1-1h2a1 1 0 0 1 0 2h-2a1 1 0 0 1-1-1zm15 1a1 1 0 0 1 1-1h2a1 1 0 0 1 0 2h-2a1 1 0 0 1-1-1zm12-2a1 1 0 0 0 0 2h2a1 1 0 0 0 0-2h-2zm-11-14a1 1 0 0 1 1-1h2a1 1 0 0 1 0 2h-2a1 1 0 0 1-1-1zm-19 0a1 1 0 0 0 0 2h2a1 1 0 0 0 0-2h-2zm6 5a1 1 0 0 1 1-1h2a1 1 0 0 1 0 2h-2a1 1 0 0 1-1-1zm-25 15c0-.47.01-.94.03-1.4a5 5 0 0 1-1.7-8 3.99 3.99 0 0 1 1.88-5.18 5 5 0 0 1 3.4-6.22 3 3 0 0 1 1.46-1.05 5 5 0 0 1 7.76-3.27A30.86 30.86 0 0 1 246 184c6.79 0 13.06 2.18 18.17 5.88a5 5 0 0 1 7.76 3.27 3 3 0 0 1 1.47 1.05 5 5 0 0 1 3.4 6.22 4 4 0 0 1 1.87 5.18 4.98 4.98 0 0 1-1.7 8c.02.46.03.93.03 1.4v1h-62v-1zm.83-7.17a30.9 30.9 0 0 0-.62 3.57 3 3 0 0 1-.61-4.2c.37.28.78.49 1.23.63zm1.49-4.61c-.36.87-.68 1.76-.96 2.68a2 2 0 0 1-.21-3.71c.33.4.73.75 1.17 1.03zm2.32-4.54c-.54.86-1.03 1.76-1.49 2.68a3 3 0 0 1-.07-4.67 3 3 0 0 0 1.56 1.99zm1.14-1.7c.35-.5.72-.98 1.1-1.46a1 1 0 1 0-1.1 1.45zm5.34-5.77c-1.03.86-2 1.79-2.9 2.77a3 3 0 0 0-1.11-.77 3 3 0 0 1 4-2zm42.66 2.77c-.9-.98-1.87-1.9-2.9-2.77a3 3 0 0 1 4.01 2 3 3 0 0 0-1.1.77zm1.34 1.54c.38.48.75.96 1.1 1.45a1 1 0 1 0-1.1-1.45zm3.73 5.84c-.46-.92-.95-1.82-1.5-2.68a3 3 0 0 0 1.57-1.99 3 3 0 0 1-.07 4.67zm1.8 4.53c-.29-.9-.6-1.8-.97-2.67.44-.28.84-.63 1.17-1.03a2 2 0 0 1-.2 3.7zm1.14 5.51c-.14-1.21-.35-2.4-.62-3.57.45-.14.86-.35 1.23-.63a2.99 2.99 0 0 1-.6 4.2zM275 214a29 29 0 0 0-57.97 0h57.96zM72.33 198.12c-.21-.32-.34-.7-.34-1.12v-12h-2v12a4.01 4.01 0 0 0 7.09 2.54c.57-.69.91-1.57.91-2.54v-12h-2v12a1.99 1.99 0 0 1-2 2 2 2 0 0 1-1.66-.88zM75 176c.38 0 .74-.04 1.1-.12a4 4 0 0 0 6.19 2.4A13.94 13.94 0 0 1 84 185v24a6 6 0 0 1-6 6h-3v9a5 5 0 1 1-10 0v-9h-3a6 6 0 0 1-6-6v-24a14 14 0 0 1 14-14 5 5 0 0 0 5 5zm-17 15v12a1.99 1.99 0 0 0 1.22 1.84 2 2 0 0 0 2.44-.72c.21-.32.34-.7.34-1.12v-12h2v12a3.98 3.98 0 0 1-5.35 3.77 3.98 3.98 0 0 1-.65-.3V209a4 4 0 0 0 4 4h16a4 4 0 0 0 4-4v-24c.01-1.53-.23-2.88-.72-4.17-.43.1-.87.16-1.28.17a6 6 0 0 1-5.2-3 7 7 0 0 1-6.47-4.88A12 12 0 0 0 58 185v6zm9 24v9a3 3 0 1 0 6 0v-9h-6z'/%3E%3Cpath d='M-17 191a1 1 0 0 0 0 2h2a1 1 0 0 0 0-2h-2zm19 9a1 1 0 0 1 1-1h2a1 1 0 0 1 0 2H3a1 1 0 0 1-1-1zm-14 5a1 1 0 0 0 0 2h2a1 1 0 0 0 0-2h-2zm-25 1a1 1 0 0 0 0 2h2a1 1 0 0 0 0-2h-2zm5 4a1 1 0 0 0 0 2h2a1 1 0 0 0 0-2h-2zm9 0a1 1 0 0 1 1-1h2a1 1 0 0 1 0 2h-2a1 1 0 0 1-1-1zm15 1a1 1 0 0 1 1-1h2a1 1 0 0 1 0 2h-2a1 1 0 0 1-1-1zm12-2a1 1 0 0 0 0 2h2a1 1 0 0 0 0-2H4zm-11-14a1 1 0 0 1 1-1h2a1 1 0 0 1 0 2h-2a1 1 0 0 1-1-1zm-19 0a1 1 0 0 0 0 2h2a1 1 0 0 0 0-2h-2zm6 5a1 1 0 0 1 1-1h2a1 1 0 0 1 0 2h-2a1 1 0 0 1-1-1zm-25 15c0-.47.01-.94.03-1.4a5 5 0 0 1-1.7-8 3.99 3.99 0 0 1 1.88-5.18 5 5 0 0 1 3.4-6.22 3 3 0 0 1 1.46-1.05 5 5 0 0 1 7.76-3.27A30.86 30.86 0 0 1-14 184c6.79 0 13.06 2.18 18.17 5.88a5 5 0 0 1 7.76 3.27 3 3 0 0 1 1.47 1.05 5 5 0 0 1 3.4 6.22 4 4 0 0 1 1.87 5.18 4.98 4.98 0 0 1-1.7 8c.02.46.03.93.03 1.4v1h-62v-1zm.83-7.17a30.9 30.9 0 0 0-.62 3.57 3 3 0 0 1-.61-4.2c.37.28.78.49 1.23.63zm1.49-4.61c-.36.87-.68 1.76-.96 2.68a2 2 0 0 1-.21-3.71c.33.4.73.75 1.17 1.03zm2.32-4.54c-.54.86-1.03 1.76-1.49 2.68a3 3 0 0 1-.07-4.67 3 3 0 0 0 1.56 1.99zm1.14-1.7c.35-.5.72-.98 1.1-1.46a1 1 0 1 0-1.1 1.45zm5.34-5.77c-1.03.86-2 1.79-2.9 2.77a3 3 0 0 0-1.11-.77 3 3 0 0 1 4-2zm42.66 2.77c-.9-.98-1.87-1.9-2.9-2.77a3 3 0 0 1 4.01 2 3 3 0 0 0-1.1.77zm1.34 1.54c.38.48.75.96 1.1 1.45a1 1 0 1 0-1.1-1.45zm3.73 5.84c-.46-.92-.95-1.82-1.5-2.68a3 3 0 0 0 1.57-1.99 3 3 0 0 1-.07 4.67zm1.8 4.53c-.29-.9-.6-1.8-.97-2.67.44-.28.84-.63 1.17-1.03a2 2 0 0 1-.2 3.7zm1.14 5.51c-.14-1.21-.35-2.4-.62-3.57.45-.14.86-.35 1.23-.63a2.99 2.99 0 0 1-.6 4.2zM15 214a29 29 0 0 0-57.97 0h57.96z'/%3E%3C/g%3E%3C/g%3E%3C/svg%3E\");\r\n  }\r\n.floor {\r\n      background-color: #00b9f2;\r\n      background-image: url(\"data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='20' height='20' viewBox='0 0 80 80'%3E%3Cg fill='%2392278f' fill-opacity='0.71'%3E%3Cpath fill-rule='evenodd' d='M0 0h40v40H0V0zm40 40h40v40H40V40zm0-40h2l-2 2V0zm0 4l4-4h2l-6 6V4zm0 4l8-8h2L40 10V8zm0 4L52 0h2L40 14v-2zm0 4L56 0h2L40 18v-2zm0 4L60 0h2L40 22v-2zm0 4L64 0h2L40 26v-2zm0 4L68 0h2L40 30v-2zm0 4L72 0h2L40 34v-2zm0 4L76 0h2L40 38v-2zm0 4L80 0v2L42 40h-2zm4 0L80 4v2L46 40h-2zm4 0L80 8v2L50 40h-2zm4 0l28-28v2L54 40h-2zm4 0l24-24v2L58 40h-2zm4 0l20-20v2L62 40h-2zm4 0l16-16v2L66 40h-2zm4 0l12-12v2L70 40h-2zm4 0l8-8v2l-6 6h-2zm4 0l4-4v2l-2 2h-2z'/%3E%3C/g%3E%3C/svg%3E\");\r\n  }\r\n.wiggle {\r\n      background-color: #dbbef9;\r\n      background-image: url(\"data:image/svg+xml,%3Csvg width='52' height='26' viewBox='0 0 52 26' xmlns='http://www.w3.org/2000/svg'%3E%3Cg fill='none' fill-rule='evenodd'%3E%3Cg fill='%23ff7c7c' fill-opacity='1'%3E%3Cpath d='M10 10c0-2.21-1.79-4-4-4-3.314 0-6-2.686-6-6h2c0 2.21 1.79 4 4 4 3.314 0 6 2.686 6 6 0 2.21 1.79 4 4 4 3.314 0 6 2.686 6 6 0 2.21 1.79 4 4 4v2c-3.314 0-6-2.686-6-6 0-2.21-1.79-4-4-4-3.314 0-6-2.686-6-6zm25.464-1.95l8.486 8.486-1.414 1.414-8.486-8.486 1.414-1.414z' /%3E%3C/g%3E%3C/g%3E%3C/svg%3E\");\r\n  }\r\n.bars {\r\n      background-color: #ffe67c;\r\n      background-image: url(\"data:image/svg+xml,%3Csvg width='40' height='40' viewBox='0 0 40 40' xmlns='http://www.w3.org/2000/svg'%3E%3Cg fill='%238fe1e7' fill-opacity='1' fill-rule='evenodd'%3E%3Cpath d='M0 40L40 0H20L0 20M40 40V20L20 40'/%3E%3C/g%3E%3C/svg%3E\");\r\n  }\r\n.bubbles {\r\n      background-color: #beffc2;\r\n      background-image: url(\"data:image/svg+xml,%3Csvg width='100' height='100' viewBox='0 0 100 100' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M11 18c3.866 0 7-3.134 7-7s-3.134-7-7-7-7 3.134-7 7 3.134 7 7 7zm48 25c3.866 0 7-3.134 7-7s-3.134-7-7-7-7 3.134-7 7 3.134 7 7 7zm-43-7c1.657 0 3-1.343 3-3s-1.343-3-3-3-3 1.343-3 3 1.343 3 3 3zm63 31c1.657 0 3-1.343 3-3s-1.343-3-3-3-3 1.343-3 3 1.343 3 3 3zM34 90c1.657 0 3-1.343 3-3s-1.343-3-3-3-3 1.343-3 3 1.343 3 3 3zm56-76c1.657 0 3-1.343 3-3s-1.343-3-3-3-3 1.343-3 3 1.343 3 3 3zM12 86c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm28-65c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm23-11c2.76 0 5-2.24 5-5s-2.24-5-5-5-5 2.24-5 5 2.24 5 5 5zm-6 60c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm29 22c2.76 0 5-2.24 5-5s-2.24-5-5-5-5 2.24-5 5 2.24 5 5 5zM32 63c2.76 0 5-2.24 5-5s-2.24-5-5-5-5 2.24-5 5 2.24 5 5 5zm57-13c2.76 0 5-2.24 5-5s-2.24-5-5-5-5 2.24-5 5 2.24 5 5 5zm-9-21c1.105 0 2-.895 2-2s-.895-2-2-2-2 .895-2 2 .895 2 2 2zM60 91c1.105 0 2-.895 2-2s-.895-2-2-2-2 .895-2 2 .895 2 2 2zM35 41c1.105 0 2-.895 2-2s-.895-2-2-2-2 .895-2 2 .895 2 2 2zM12 60c1.105 0 2-.895 2-2s-.895-2-2-2-2 .895-2 2 .895 2 2 2z' fill='%23e6afff' fill-opacity='1' fill-rule='evenodd'/%3E%3C/svg%3E\");\r\n  }\r\n.ticTac {\r\n      background-color: #ffefaa;\r\n      background-image: url(\"data:image/svg+xml,%3Csvg width='76' height='76' viewBox='0 0 64 64' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M8 16c4.418 0 8-3.582 8-8s-3.582-8-8-8-8 3.582-8 8 3.582 8 8 8zm0-2c3.314 0 6-2.686 6-6s-2.686-6-6-6-6 2.686-6 6 2.686 6 6 6zm33.414-6l5.95-5.95L45.95.636 40 6.586 34.05.636 32.636 2.05 38.586 8l-5.95 5.95 1.414 1.414L40 9.414l5.95 5.95 1.414-1.414L41.414 8zM40 48c4.418 0 8-3.582 8-8s-3.582-8-8-8-8 3.582-8 8 3.582 8 8 8zm0-2c3.314 0 6-2.686 6-6s-2.686-6-6-6-6 2.686-6 6 2.686 6 6 6zM9.414 40l5.95-5.95-1.414-1.414L8 38.586l-5.95-5.95L.636 34.05 6.586 40l-5.95 5.95 1.414 1.414L8 41.414l5.95 5.95 1.414-1.414L9.414 40z' fill='%23ffadad' fill-opacity='0.84' fill-rule='evenodd'/%3E%3C/svg%3E\");\r\n  }\r\n.zigZag {\r\n      background-color: #00dac3;\r\n      background-image: url(\"data:image/svg+xml,%3Csvg width='40' height='12' viewBox='0 0 40 12' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M0 6.172L6.172 0h5.656L0 11.828V6.172zm40 5.656L28.172 0h5.656L40 6.172v5.656zM6.172 12l12-12h3.656l12 12h-5.656L20 3.828 11.828 12H6.172zm12 0L20 10.172 21.828 12h-3.656z' fill='%23008386' fill-opacity='0.7' fill-rule='evenodd'/%3E%3C/svg%3E\");\r\n  }\r\n.stripes {\r\n      background-color: #ffffff;\r\n      background-image: url(\"data:image/svg+xml,%3Csvg width='25' height='1' viewBox='0 0 40 1' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M0 0h20v1H0z' fill='%23d09af3' fill-opacity='0.54' fill-rule='evenodd'/%3E%3C/svg%3E\");\r\n  }\r\n.clouds {\r\n      background-color: #959bb5;\r\n      background-image: url(\"data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 56 28' width='56' height='28'%3E%3Cpath fill='%23f0d519' fill-opacity='0.89' d='M56 26v2h-7.75c2.3-1.27 4.94-2 7.75-2zm-26 2a2 2 0 1 0-4 0h-4.09A25.98 25.98 0 0 0 0 16v-2c.67 0 1.34.02 2 .07V14a2 2 0 0 0-2-2v-2a4 4 0 0 1 3.98 3.6 28.09 28.09 0 0 1 2.8-3.86A8 8 0 0 0 0 6V4a9.99 9.99 0 0 1 8.17 4.23c.94-.95 1.96-1.83 3.03-2.63A13.98 13.98 0 0 0 0 0h7.75c2 1.1 3.73 2.63 5.1 4.45 1.12-.72 2.3-1.37 3.53-1.93A20.1 20.1 0 0 0 14.28 0h2.7c.45.56.88 1.14 1.29 1.74 1.3-.48 2.63-.87 4-1.15-.11-.2-.23-.4-.36-.59H26v.07a28.4 28.4 0 0 1 4 0V0h4.09l-.37.59c1.38.28 2.72.67 4.01 1.15.4-.6.84-1.18 1.3-1.74h2.69a20.1 20.1 0 0 0-2.1 2.52c1.23.56 2.41 1.2 3.54 1.93A16.08 16.08 0 0 1 48.25 0H56c-4.58 0-8.65 2.2-11.2 5.6 1.07.8 2.09 1.68 3.03 2.63A9.99 9.99 0 0 1 56 4v2a8 8 0 0 0-6.77 3.74c1.03 1.2 1.97 2.5 2.79 3.86A4 4 0 0 1 56 10v2a2 2 0 0 0-2 2.07 28.4 28.4 0 0 1 2-.07v2c-9.2 0-17.3 4.78-21.91 12H30zM7.75 28H0v-2c2.81 0 5.46.73 7.75 2zM56 20v2c-5.6 0-10.65 2.3-14.28 6h-2.7c4.04-4.89 10.15-8 16.98-8zm-39.03 8h-2.69C10.65 24.3 5.6 22 0 22v-2c6.83 0 12.94 3.11 16.97 8zm15.01-.4a28.09 28.09 0 0 1 2.8-3.86 8 8 0 0 0-13.55 0c1.03 1.2 1.97 2.5 2.79 3.86a4 4 0 0 1 7.96 0zm14.29-11.86c1.3-.48 2.63-.87 4-1.15a25.99 25.99 0 0 0-44.55 0c1.38.28 2.72.67 4.01 1.15a21.98 21.98 0 0 1 36.54 0zm-5.43 2.71c1.13-.72 2.3-1.37 3.54-1.93a19.98 19.98 0 0 0-32.76 0c1.23.56 2.41 1.2 3.54 1.93a15.98 15.98 0 0 1 25.68 0zm-4.67 3.78c.94-.95 1.96-1.83 3.03-2.63a13.98 13.98 0 0 0-22.4 0c1.07.8 2.09 1.68 3.03 2.63a9.99 9.99 0 0 1 16.34 0z'%3E%3C/path%3E%3C/svg%3E\");\r\n  }\r\n.aztec {\r\n      background-color: #d59242;\r\n      background-image: url(\"data:image/svg+xml,%3Csvg width='100' height='20' viewBox='0 0 100 20' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M21.184 20c.357-.13.72-.264 1.088-.402l1.768-.661C33.64 15.347 39.647 14 50 14c10.271 0 15.362 1.222 24.629 4.928.955.383 1.869.74 2.75 1.072h6.225c-2.51-.73-5.139-1.691-8.233-2.928C65.888 13.278 60.562 12 50 12c-10.626 0-16.855 1.397-26.66 5.063l-1.767.662c-2.475.923-4.66 1.674-6.724 2.275h6.335zm0-20C13.258 2.892 8.077 4 0 4V2c5.744 0 9.951-.574 14.85-2h6.334zM77.38 0C85.239 2.966 90.502 4 100 4V2c-6.842 0-11.386-.542-16.396-2h-6.225zM0 14c8.44 0 13.718-1.21 22.272-4.402l1.768-.661C33.64 5.347 39.647 4 50 4c10.271 0 15.362 1.222 24.629 4.928C84.112 12.722 89.438 14 100 14v-2c-10.271 0-15.362-1.222-24.629-4.928C65.888 3.278 60.562 2 50 2 39.374 2 33.145 3.397 23.34 7.063l-1.767.662C13.223 10.84 8.163 12 0 12v2z' fill='%230d37c2' fill-opacity='0.4' fill-rule='evenodd'/%3E%3C/svg%3E\");\r\n  }\r\n.circuit {\r\n      background-color: #00b497;\r\n      background-image: url(\"data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 304 304' width='150' height='150'%3E%3Cpath fill='%23333333' fill-opacity='0.4' d='M44.1 224a5 5 0 1 1 0 2H0v-2h44.1zm160 48a5 5 0 1 1 0 2H82v-2h122.1zm57.8-46a5 5 0 1 1 0-2H304v2h-42.1zm0 16a5 5 0 1 1 0-2H304v2h-42.1zm6.2-114a5 5 0 1 1 0 2h-86.2a5 5 0 1 1 0-2h86.2zm-256-48a5 5 0 1 1 0 2H0v-2h12.1zm185.8 34a5 5 0 1 1 0-2h86.2a5 5 0 1 1 0 2h-86.2zM258 12.1a5 5 0 1 1-2 0V0h2v12.1zm-64 208a5 5 0 1 1-2 0v-54.2a5 5 0 1 1 2 0v54.2zm48-198.2V80h62v2h-64V21.9a5 5 0 1 1 2 0zm16 16V64h46v2h-48V37.9a5 5 0 1 1 2 0zm-128 96V208h16v12.1a5 5 0 1 1-2 0V210h-16v-76.1a5 5 0 1 1 2 0zm-5.9-21.9a5 5 0 1 1 0 2H114v48H85.9a5 5 0 1 1 0-2H112v-48h12.1zm-6.2 130a5 5 0 1 1 0-2H176v-74.1a5 5 0 1 1 2 0V242h-60.1zm-16-64a5 5 0 1 1 0-2H114v48h10.1a5 5 0 1 1 0 2H112v-48h-10.1zM66 284.1a5 5 0 1 1-2 0V274H50v30h-2v-32h18v12.1zM236.1 176a5 5 0 1 1 0 2H226v94h48v32h-2v-30h-48v-98h12.1zm25.8-30a5 5 0 1 1 0-2H274v44.1a5 5 0 1 1-2 0V146h-10.1zm-64 96a5 5 0 1 1 0-2H208v-80h16v-14h-42.1a5 5 0 1 1 0-2H226v18h-16v80h-12.1zm86.2-210a5 5 0 1 1 0 2H272V0h2v32h10.1zM98 101.9V146H53.9a5 5 0 1 1 0-2H96v-42.1a5 5 0 1 1 2 0zM53.9 34a5 5 0 1 1 0-2H80V0h2v34H53.9zm60.1 3.9V66H82v64H69.9a5 5 0 1 1 0-2H80V64h32V37.9a5 5 0 1 1 2 0zM101.9 82a5 5 0 1 1 0-2H128V37.9a5 5 0 1 1 2 0V82h-28.1zm16-64a5 5 0 1 1 0-2H146v44.1a5 5 0 1 1-2 0V18h-26.1zm102.2 270a5 5 0 1 1 0 2H98v14h-2v-16h124.1zM242 149.9V160h16v34h-16v62h48v48h-2v-46h-48v-66h16v-30h-16v-12.1a5 5 0 1 1 2 0zM53.9 18a5 5 0 1 1 0-2H64V2H48V0h18v18H53.9zm112 32a5 5 0 1 1 0-2H192V0h50v2h-48v48h-28.1zm-48-48a5 5 0 0 1-9.8-2h2.07a3 3 0 1 0 5.66 0H178v34h-18V21.9a5 5 0 1 1 2 0V32h14V2h-58.1zm0 96a5 5 0 1 1 0-2H137l32-32h39V21.9a5 5 0 1 1 2 0V66h-40.17l-32 32H117.9zm28.1 90.1a5 5 0 1 1-2 0v-76.51L175.59 80H224V21.9a5 5 0 1 1 2 0V82h-49.59L146 112.41v75.69zm16 32a5 5 0 1 1-2 0v-99.51L184.59 96H300.1a5 5 0 0 1 3.9-3.9v2.07a3 3 0 0 0 0 5.66v2.07a5 5 0 0 1-3.9-3.9H185.41L162 121.41v98.69zm-144-64a5 5 0 1 1-2 0v-3.51l48-48V48h32V0h2v50H66v55.41l-48 48v2.69zM50 53.9v43.51l-48 48V208h26.1a5 5 0 1 1 0 2H0v-65.41l48-48V53.9a5 5 0 1 1 2 0zm-16 16V89.41l-34 34v-2.82l32-32V69.9a5 5 0 1 1 2 0zM12.1 32a5 5 0 1 1 0 2H9.41L0 43.41V40.6L8.59 32h3.51zm265.8 18a5 5 0 1 1 0-2h18.69l7.41-7.41v2.82L297.41 50H277.9zm-16 160a5 5 0 1 1 0-2H288v-71.41l16-16v2.82l-14 14V210h-28.1zm-208 32a5 5 0 1 1 0-2H64v-22.59L40.59 194H21.9a5 5 0 1 1 0-2H41.41L66 216.59V242H53.9zm150.2 14a5 5 0 1 1 0 2H96v-56.6L56.6 162H37.9a5 5 0 1 1 0-2h19.5L98 200.6V256h106.1zm-150.2 2a5 5 0 1 1 0-2H80v-46.59L48.59 178H21.9a5 5 0 1 1 0-2H49.41L82 208.59V258H53.9zM34 39.8v1.61L9.41 66H0v-2h8.59L32 40.59V0h2v39.8zM2 300.1a5 5 0 0 1 3.9 3.9H3.83A3 3 0 0 0 0 302.17V256h18v48h-2v-46H2v42.1zM34 241v63h-2v-62H0v-2h34v1zM17 18H0v-2h16V0h2v18h-1zm273-2h14v2h-16V0h2v16zm-32 273v15h-2v-14h-14v14h-2v-16h18v1zM0 92.1A5.02 5.02 0 0 1 6 97a5 5 0 0 1-6 4.9v-2.07a3 3 0 1 0 0-5.66V92.1zM80 272h2v32h-2v-32zm37.9 32h-2.07a3 3 0 0 0-5.66 0h-2.07a5 5 0 0 1 9.8 0zM5.9 0A5.02 5.02 0 0 1 0 5.9V3.83A3 3 0 0 0 3.83 0H5.9zm294.2 0h2.07A3 3 0 0 0 304 3.83V5.9a5 5 0 0 1-3.9-5.9zm3.9 300.1v2.07a3 3 0 0 0-1.83 1.83h-2.07a5 5 0 0 1 3.9-3.9zM97 100a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm0-16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16 16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16 16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm0 16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-48 32a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16 16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm32 48a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-16 16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm32-16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm0-32a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16 32a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm32 16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm0-16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-16-64a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16 96a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm0 16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16 16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16-144a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm0 32a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16-32a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16-16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-96 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm0 16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16-32a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm96 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-16-64a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16-16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-32 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm0-16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-16 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-16 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-16 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zM49 36a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-32 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm32 16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zM33 68a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16-48a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm0 240a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16 32a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-16-64a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm0 16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-16-32a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm80-176a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-16-16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm32 48a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16-16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm0-32a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm112 176a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-16 16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm0 16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm0 16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zM17 180a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm0 16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm0-32a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zM17 84a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm32 64a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16-16a3 3 0 1 0 0-6 3 3 0 0 0 0 6z'%3E%3C/path%3E%3C/svg%3E\");\r\n  }\r\n.dots {\r\n      background-color: #ffcb05;\r\n      background-image: url(\"data:image/svg+xml,%3Csvg width='16' height='16' viewBox='0 0 20 20' xmlns='http://www.w3.org/2000/svg'%3E%3Cg fill='%2300a99d' fill-opacity='0.71' fill-rule='evenodd'%3E%3Ccircle cx='3' cy='3' r='3'/%3E%3Ccircle cx='13' cy='13' r='3'/%3E%3C/g%3E%3C/svg%3E\");\r\n  }\r\n.lines {\r\n      background-color: #efefef;\r\n      background-image: url(\"data:image/svg+xml,%3Csvg width='80' height='80' viewBox='0 0 120 120' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M9 0h2v20H9V0zm25.134.84l1.732 1-10 17.32-1.732-1 10-17.32zm-20 20l1.732 1-10 17.32-1.732-1 10-17.32zM58.16 4.134l1 1.732-17.32 10-1-1.732 17.32-10zm-40 40l1 1.732-17.32 10-1-1.732 17.32-10zM80 9v2H60V9h20zM20 69v2H0v-2h20zm79.32-55l-1 1.732-17.32-10L82 4l17.32 10zm-80 80l-1 1.732-17.32-10L2 84l17.32 10zm96.546-75.84l-1.732 1-10-17.32 1.732-1 10 17.32zm-100 100l-1.732 1-10-17.32 1.732-1 10 17.32zM38.16 24.134l1 1.732-17.32 10-1-1.732 17.32-10zM60 29v2H40v-2h20zm19.32 5l-1 1.732-17.32-10L62 24l17.32 10zm16.546 4.16l-1.732 1-10-17.32 1.732-1 10 17.32zM111 40h-2V20h2v20zm3.134.84l1.732 1-10 17.32-1.732-1 10-17.32zM40 49v2H20v-2h20zm19.32 5l-1 1.732-17.32-10L42 44l17.32 10zm16.546 4.16l-1.732 1-10-17.32 1.732-1 10 17.32zM91 60h-2V40h2v20zm3.134.84l1.732 1-10 17.32-1.732-1 10-17.32zm24.026 3.294l1 1.732-17.32 10-1-1.732 17.32-10zM39.32 74l-1 1.732-17.32-10L22 64l17.32 10zm16.546 4.16l-1.732 1-10-17.32 1.732-1 10 17.32zM71 80h-2V60h2v20zm3.134.84l1.732 1-10 17.32-1.732-1 10-17.32zm24.026 3.294l1 1.732-17.32 10-1-1.732 17.32-10zM120 89v2h-20v-2h20zm-84.134 9.16l-1.732 1-10-17.32 1.732-1 10 17.32zM51 100h-2V80h2v20zm3.134.84l1.732 1-10 17.32-1.732-1 10-17.32zm24.026 3.294l1 1.732-17.32 10-1-1.732 17.32-10zM100 109v2H80v-2h20zm19.32 5l-1 1.732-17.32-10 1-1.732 17.32 10zM31 120h-2v-20h2v20z' fill='%23efb4a3' fill-opacity='0.84' fill-rule='evenodd'/%3E%3C/svg%3E\");\r\n  }\r\n.leaf {\r\n      background-color: #b5ccbf;\r\n      background-image: url(\"data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 80 40' width='60' height='30'%3E%3Cpath fill='%2340584a' fill-opacity='0.47' d='M0 40a19.96 19.96 0 0 1 5.9-14.11 20.17 20.17 0 0 1 19.44-5.2A20 20 0 0 1 20.2 40H0zM65.32.75A20.02 20.02 0 0 1 40.8 25.26 20.02 20.02 0 0 1 65.32.76zM.07 0h20.1l-.08.07A20.02 20.02 0 0 1 .75 5.25 20.08 20.08 0 0 1 .07 0zm1.94 40h2.53l4.26-4.24v-9.78A17.96 17.96 0 0 0 2 40zm5.38 0h9.8a17.98 17.98 0 0 0 6.67-16.42L7.4 40zm3.43-15.42v9.17l11.62-11.59c-3.97-.5-8.08.3-11.62 2.42zm32.86-.78A18 18 0 0 0 63.85 3.63L43.68 23.8zm7.2-19.17v9.15L62.43 2.22c-3.96-.5-8.05.3-11.57 2.4zm-3.49 2.72c-4.1 4.1-5.81 9.69-5.13 15.03l6.61-6.6V6.02c-.51.41-1 .85-1.48 1.33zM17.18 0H7.42L3.64 3.78A18 18 0 0 0 17.18 0zM2.08 0c-.01.8.04 1.58.14 2.37L4.59 0H2.07z'%3E%3C/path%3E%3C/svg%3E\");\r\n  }\r\n.overlap {\r\n      background-color: #ffadff;\r\n      background-image: url(\"data:image/svg+xml,%3Csvg width='60' height='60' viewBox='0 0 80 80' xmlns='http://www.w3.org/2000/svg'%3E%3Cg fill='none' fill-rule='evenodd'%3E%3Cg fill='%232e78ff' fill-opacity='0.82'%3E%3Cpath d='M50 50c0-5.523 4.477-10 10-10s10 4.477 10 10-4.477 10-10 10c0 5.523-4.477 10-10 10s-10-4.477-10-10 4.477-10 10-10zM10 10c0-5.523 4.477-10 10-10s10 4.477 10 10-4.477 10-10 10c0 5.523-4.477 10-10 10S0 25.523 0 20s4.477-10 10-10zm10 8c4.418 0 8-3.582 8-8s-3.582-8-8-8-8 3.582-8 8 3.582 8 8 8zm40 40c4.418 0 8-3.582 8-8s-3.582-8-8-8-8 3.582-8 8 3.582 8 8 8z' /%3E%3C/g%3E%3C/g%3E%3C/svg%3E\");\r\n  }\r\n@media screen and (max-width: 500px) {\r\n      .progress {\r\n          width: calc(100vw * 0.7);\r\n          /* height: calc(100vw * 0.7 * 0.145); */\r\n          height: 40px;\r\n          margin: 25px;\r\n      }\r\n  \r\n      .github {\r\n          margin: 20px;\r\n      }\r\n  \r\n      .desktop {\r\n          display: none;\r\n      }\r\n  \r\n      .mobile {\r\n          display: inherit;\r\n      }\r\n  }\r\n/* Indeterminate Classes and Animations */\r\n.indeterminate-16 {\r\n      -webkit-animation: indeterminate-16 0.25s linear infinite;\r\n              animation: indeterminate-16 0.25s linear infinite;\r\n  }\r\n.indeterminate-20 {\r\n      -webkit-animation: indeterminate-20 0.5s linear infinite;\r\n              animation: indeterminate-20 0.5s linear infinite;\r\n  }\r\n.indeterminate-25 {\r\n      -webkit-animation: indeterminate-25 0.5s linear infinite;\r\n              animation: indeterminate-25 0.5s linear infinite;\r\n  }\r\n.indeterminate-28 {\r\n      -webkit-animation: indeterminate-28 0.25s linear infinite;\r\n              animation: indeterminate-28 0.25s linear infinite;\r\n  }\r\n.indeterminate-40 {\r\n      -webkit-animation: indeterminate-40 0.5s linear infinite;\r\n              animation: indeterminate-40 0.5s linear infinite;\r\n  }\r\n.indeterminate-52 {\r\n      -webkit-animation: indeterminate-52 0.5s linear infinite;\r\n              animation: indeterminate-52 0.5s linear infinite;\r\n  }\r\n.indeterminate-56 {\r\n      -webkit-animation: indeterminate-56 0.5s linear infinite;\r\n              animation: indeterminate-56 0.5s linear infinite;\r\n  }\r\n.indeterminate-50 {\r\n      -webkit-animation: indeterminate-50 0.5s linear infinite;\r\n              animation: indeterminate-50 0.5s linear infinite;\r\n  }\r\n.indeterminate-60 {\r\n      -webkit-animation: indeterminate-60 0.5s linear infinite;\r\n              animation: indeterminate-60 0.5s linear infinite;\r\n  }\r\n.indeterminate-70 {\r\n      -webkit-animation: indeterminate-70 0.5s linear infinite;\r\n              animation: indeterminate-70 0.5s linear infinite;\r\n  }\r\n.indeterminate-76 {\r\n      -webkit-animation: indeterminate-76 0.5s linear infinite;\r\n              animation: indeterminate-76 0.5s linear infinite;\r\n  }\r\n.indeterminate-80 {\r\n      -webkit-animation: indeterminate-80 0.5s linear infinite;\r\n              animation: indeterminate-80 0.5s linear infinite;\r\n  }\r\n.indeterminate-100 {\r\n      -webkit-animation: indeterminate-100 1s linear infinite;\r\n              animation: indeterminate-100 1s linear infinite;\r\n  }\r\n.indeterminate-120 {\r\n      -webkit-animation: indeterminate-120 1s linear infinite;\r\n              animation: indeterminate-120 1s linear infinite;\r\n  }\r\n.indeterminate-126 {\r\n      -webkit-animation: indeterminate-126 1s linear infinite;\r\n              animation: indeterminate-126 1s linear infinite;\r\n  }\r\n.indeterminate-150 {\r\n      -webkit-animation: indeterminate-150 1s linear infinite;\r\n              animation: indeterminate-150 1s linear infinite;\r\n  }\r\n@-webkit-keyframes indeterminate-16 {\r\n      from {\r\n          background-position: 16px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@keyframes indeterminate-16 {\r\n      from {\r\n          background-position: 16px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@-webkit-keyframes indeterminate-20 {\r\n      from {\r\n          background-position: 20px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@keyframes indeterminate-20 {\r\n      from {\r\n          background-position: 20px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@-webkit-keyframes indeterminate-25 {\r\n      from {\r\n          background-position: 25px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@keyframes indeterminate-25 {\r\n      from {\r\n          background-position: 25px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@-webkit-keyframes indeterminate-28 {\r\n      from {\r\n          background-position: 28px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@keyframes indeterminate-28 {\r\n      from {\r\n          background-position: 28px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@-webkit-keyframes indeterminate-40 {\r\n      from {\r\n          background-position: 40px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@keyframes indeterminate-40 {\r\n      from {\r\n          background-position: 40px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@-webkit-keyframes indeterminate-50 {\r\n      from {\r\n          background-position: 50px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@keyframes indeterminate-50 {\r\n      from {\r\n          background-position: 50px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@-webkit-keyframes indeterminate-52 {\r\n      from {\r\n          background-position: 52px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@keyframes indeterminate-52 {\r\n      from {\r\n          background-position: 52px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@-webkit-keyframes indeterminate-56 {\r\n      from {\r\n          background-position: 56px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@keyframes indeterminate-56 {\r\n      from {\r\n          background-position: 56px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@-webkit-keyframes indeterminate-60 {\r\n      from {\r\n          background-position: 60px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@keyframes indeterminate-60 {\r\n      from {\r\n          background-position: 60px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@-webkit-keyframes indeterminate-70 {\r\n      from {\r\n          background-position: 70px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@keyframes indeterminate-70 {\r\n      from {\r\n          background-position: 70px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@-webkit-keyframes indeterminate-76 {\r\n      from {\r\n          background-position: 76px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@keyframes indeterminate-76 {\r\n      from {\r\n          background-position: 76px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@-webkit-keyframes indeterminate-80 {\r\n      from {\r\n          background-position: 80px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@keyframes indeterminate-80 {\r\n      from {\r\n          background-position: 80px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@-webkit-keyframes indeterminate-100 {\r\n      from {\r\n          background-position: 100px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@keyframes indeterminate-100 {\r\n      from {\r\n          background-position: 100px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@-webkit-keyframes indeterminate-120 {\r\n      from {\r\n          background-position: 120px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@keyframes indeterminate-120 {\r\n      from {\r\n          background-position: 120px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@-webkit-keyframes indeterminate-126 {\r\n      from {\r\n          background-position: 126px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@keyframes indeterminate-126 {\r\n      from {\r\n          background-position: 126px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@-webkit-keyframes indeterminate-150 {\r\n      from {\r\n          background-position: 150px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@keyframes indeterminate-150 {\r\n      from {\r\n          background-position: 150px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n  \r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9hbmFseXRpYy9hbmFseXRpYy5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLEtBQUssZ0JBQWdCO0FBQ3JCLG9CQUFvQixTQUFTLENBQUMsV0FBVyxDQUFDLGtCQUFrQixDQUFDLGlCQUFpQixDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsOEJBQThCO0FBRTlILGNBQWMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLGlCQUFpQixDQUFDLGdCQUFnQjtBQUVoRztJQUNJLDBCQUEwQjtJQUMxQixZQUFZO0lBQ1osWUFBWTtJQUNaLHlCQUF5QjtJQUN6QixrQkFBa0I7SUFDbEIscUJBQXFCO0lBQ3JCLHFCQUFxQjtJQUNyQixlQUFlO0lBQ2YsZ0JBQWdCO0lBQ2hCLGVBQWU7RUFDakI7QUFHRjtHQUNHLDBCQUEwQjtHQUMxQixZQUFZO0dBQ1osWUFBWTtHQUNaLHlCQUF5QjtHQUN6QixrQkFBa0I7R0FDbEIscUJBQXFCO0dBQ3JCLHFCQUFxQjtHQUNyQixlQUFlO0dBQ2YsZ0JBQWdCO0dBQ2hCLGVBQWU7Q0FDakI7QUFDQTtHQUNFLDBCQUEwQjtHQUMxQixZQUFZO0dBQ1osWUFBWTtHQUNaLHlCQUF5QjtHQUN6QixrQkFBa0I7R0FDbEIscUJBQXFCO0dBQ3JCLHFCQUFxQjtHQUNyQixlQUFlO0dBQ2YsZ0JBQWdCO0dBQ2hCLGVBQWU7Q0FDakI7QUFFQTtHQUNFLDBCQUEwQjtHQUMxQixZQUFZO0dBQ1osWUFBWTtHQUNaLHlCQUF5QjtHQUN6QixrQkFBa0I7R0FDbEIscUJBQXFCO0dBQ3JCLHFCQUFxQjtHQUNyQixlQUFlO0dBQ2YsZ0JBQWdCO0dBQ2hCLGVBQWU7Q0FDakI7QUFFQztLQUNHLGtCQUFrQjtLQUNsQixlQUFlO0tBQ2YsT0FBTztLQUNQLFNBQVM7S0FDVCxXQUFXO0tBQ1gsMkJBQTJCO0tBQzNCLFlBQVk7S0FDWixrQkFBa0I7O0VBRXJCO0FBQ0E7SUFDRTs7Q0FFSDtBQUNDO0lBQ0Usa0JBQWtCO0lBQ2xCLHFCQUFxQjtFQUN2QjtBQUVBO0lBQ0UscUNBQTJEO0lBQzNELGVBQWU7SUFDZixXQUFXO0lBQ1gsWUFBWTtJQUNaLFlBQVk7RUFDZDtBQUNBO0lBQ0Usa0JBQWtCO0lBQ2xCLHFCQUFxQjtFQUN2QjtBQUVBO0lBQ0UseUNBQStEO0lBQy9ELGVBQWU7SUFDZixXQUFXO0lBQ1gsWUFBWTtJQUNaLFlBQVk7RUFDZDtBQUVBO0lBQ0Usa0JBQWtCO0lBQ2xCLHFCQUFxQjtFQUN2QjtBQUVBO0lBQ0UsbURBQTJFO0lBQzNFLGVBQWU7SUFDZixXQUFXO0lBQ1gsWUFBWTtJQUNaLFlBQVk7RUFDZDtBQUVBO0lBQ0Usa0JBQWtCO0lBQ2xCLHFCQUFxQjtFQUN2QjtBQUVBO0lBQ0UseUNBQStEO0lBQy9ELGVBQWU7SUFDZixXQUFXO0lBQ1gsWUFBWTtJQUNaLFlBQVk7RUFDZDtBQUNBO0lBQ0Usa0JBQWtCO0lBQ2xCLFNBQVM7SUFDVCxTQUFTO0VBQ1g7QUFFQTtJQUNFLGtCQUFrQjtJQUNsQixTQUFTO0lBQ1QsU0FBUztFQUNYO0FBRUE7SUFDRSxrQkFBa0I7SUFDbEIsU0FBUztJQUNULFNBQVM7RUFDWDtBQUVBO0lBQ0Usa0JBQWtCO0lBQ2xCLFNBQVM7SUFDVCxVQUFVO0VBQ1o7QUFHQTtJQUNFLGdCQUFnQjs7SUFFaEIsbUJBQW1CO0lBQ25CLFdBQVc7O0lBRVgsc0JBQXNCOztFQUV4QjtBQUVBO0lBQ0UsZ0JBQWdCOztJQUVoQixtQkFBbUI7SUFDbkIsaUJBQWlCO0lBQ2pCLFNBQVM7SUFDVCxZQUFZO0lBQ1osMEJBQTBCOztFQUU1QjtBQU9BO01BQ0ksMkJBQTJCO0VBQy9CO0FBRUE7TUFDSSx1QkFBdUI7TUFDdkIsa0JBQWtCO01BQ2xCLGdCQUFnQjtFQUNwQjtBQUVBO01BQ0ksbUJBQW1CO0VBQ3ZCO0FBRUE7TUFDSSxXQUFXO01BQ1gsd0JBQXdCO01BQ3hCLDRCQUE0QjtNQUM1QixXQUFXO0VBQ2Y7QUFFQTtNQUNJLFdBQVc7TUFDWCxvQkFBb0I7TUFDcEIsd0JBQXdCO01BQ3hCLDBCQUEwQjtNQUMxQixXQUFXO0VBQ2Y7QUFFQTtNQUNJLGtCQUFrQjtFQUN0QjtBQUVBO01BQ0ksWUFBWTtFQUNoQjtBQUVBO01BQ0kscUJBQXFCO01BQ3JCLFlBQVk7TUFDWixZQUFZO01BQ1osWUFBWTtNQUNaLG1CQUFtQjtNQUNuQixtQkFBbUI7RUFDdkI7QUFFQTtNQUNJLG1CQUFtQjtNQUNuQixTQUFTO01BQ1QsWUFBWTtNQUNaLGlCQUFpQjtNQUNqQix1QkFBdUI7TUFDdkIsZ0VBQWdFOztFQUVwRTtBQUVBO01BQ0ksYUFBYTtFQUNqQjtBQUVBO01BQ0ksVUFBVTtNQUNWLDZDQUE2QztFQUNqRDtBQUVBO01BQ0kseUJBQXlCO01BQ3pCLHVpQkFBdWlCO0VBQzNpQjtBQUVBO01BQ0kseUJBQXlCO01BQ3pCLDJ4Q0FBMnhDO0VBQy94QztBQUVBO01BQ0kseUJBQXlCO01BQ3pCLDRxQkFBNHFCO0VBQ2hyQjtBQUVBO01BQ0kseUJBQXlCO01BQ3pCLHlpQ0FBeWlDO0VBQzdpQztBQUVBO01BQ0kseUJBQXlCO01BQ3pCLDIvQkFBMi9CO0VBQy8vQjtBQUVBO01BQ0kseUJBQXlCO01BQ3pCLDJpQkFBMmlCO0VBQy9pQjtBQUVBO01BQ0kseUJBQXlCO01BQ3pCLDJ0VEFBMnRUO0VBQy90VDtBQUVBO01BQ0kseUJBQXlCO01BQ3pCLGlxQkFBaXFCO0VBQ3JxQjtBQUVBO01BQ0kseUJBQXlCO01BQ3pCLHFnQkFBcWdCO0VBQ3pnQjtBQUVBO01BQ0kseUJBQXlCO01BQ3pCLG1RQUFtUTtFQUN2UTtBQUVBO01BQ0kseUJBQXlCO01BQ3pCLG91Q0FBb3VDO0VBQ3h1QztBQUVBO01BQ0kseUJBQXlCO01BQ3pCLDZ0QkFBNnRCO0VBQ2p1QjtBQUVBO01BQ0kseUJBQXlCO01BQ3pCLGdZQUFnWTtFQUNwWTtBQUVBO01BQ0kseUJBQXlCO01BQ3pCLGdPQUFnTztFQUNwTztBQUVBO01BQ0kseUJBQXlCO01BQ3pCLHFtREFBcW1EO0VBQ3ptRDtBQUVBO01BQ0kseUJBQXlCO01BQ3pCLHc2QkFBdzZCO0VBQzU2QjtBQUVBO01BQ0kseUJBQXlCO01BQ3pCLDB1S0FBMHVLO0VBQzl1SztBQUVBO01BQ0kseUJBQXlCO01BQ3pCLHlSQUF5UjtFQUM3UjtBQUVBO01BQ0kseUJBQXlCO01BQ3pCLDYrQ0FBNitDO0VBQ2ovQztBQUVBO01BQ0kseUJBQXlCO01BQ3pCLCswQkFBKzBCO0VBQ24xQjtBQUVBO01BQ0kseUJBQXlCO01BQ3pCLGttQkFBa21CO0VBQ3RtQjtBQUVBO01BQ0k7VUFDSSx3QkFBd0I7VUFDeEIsdUNBQXVDO1VBQ3ZDLFlBQVk7VUFDWixZQUFZO01BQ2hCOztNQUVBO1VBQ0ksWUFBWTtNQUNoQjs7TUFFQTtVQUNJLGFBQWE7TUFDakI7O01BRUE7VUFDSSxnQkFBZ0I7TUFDcEI7RUFDSjtBQUdBLHlDQUF5QztBQUV6QztNQUNJLHlEQUFpRDtjQUFqRCxpREFBaUQ7RUFDckQ7QUFFQTtNQUNJLHdEQUFnRDtjQUFoRCxnREFBZ0Q7RUFDcEQ7QUFFQTtNQUNJLHdEQUFnRDtjQUFoRCxnREFBZ0Q7RUFDcEQ7QUFFQTtNQUNJLHlEQUFpRDtjQUFqRCxpREFBaUQ7RUFDckQ7QUFFQTtNQUNJLHdEQUFnRDtjQUFoRCxnREFBZ0Q7RUFDcEQ7QUFFQTtNQUNJLHdEQUFnRDtjQUFoRCxnREFBZ0Q7RUFDcEQ7QUFFQTtNQUNJLHdEQUFnRDtjQUFoRCxnREFBZ0Q7RUFDcEQ7QUFFQTtNQUNJLHdEQUFnRDtjQUFoRCxnREFBZ0Q7RUFDcEQ7QUFFQTtNQUNJLHdEQUFnRDtjQUFoRCxnREFBZ0Q7RUFDcEQ7QUFFQTtNQUNJLHdEQUFnRDtjQUFoRCxnREFBZ0Q7RUFDcEQ7QUFFQTtNQUNJLHdEQUFnRDtjQUFoRCxnREFBZ0Q7RUFDcEQ7QUFFQTtNQUNJLHdEQUFnRDtjQUFoRCxnREFBZ0Q7RUFDcEQ7QUFFQTtNQUNJLHVEQUErQztjQUEvQywrQ0FBK0M7RUFDbkQ7QUFFQTtNQUNJLHVEQUErQztjQUEvQywrQ0FBK0M7RUFDbkQ7QUFFQTtNQUNJLHVEQUErQztjQUEvQywrQ0FBK0M7RUFDbkQ7QUFFQTtNQUNJLHVEQUErQztjQUEvQywrQ0FBK0M7RUFDbkQ7QUFFQTtNQUNJO1VBQ0ksMkJBQTJCO01BQy9CO01BQ0E7VUFDSSx3QkFBd0I7TUFDNUI7RUFDSjtBQVBBO01BQ0k7VUFDSSwyQkFBMkI7TUFDL0I7TUFDQTtVQUNJLHdCQUF3QjtNQUM1QjtFQUNKO0FBRUE7TUFDSTtVQUNJLDJCQUEyQjtNQUMvQjtNQUNBO1VBQ0ksd0JBQXdCO01BQzVCO0VBQ0o7QUFQQTtNQUNJO1VBQ0ksMkJBQTJCO01BQy9CO01BQ0E7VUFDSSx3QkFBd0I7TUFDNUI7RUFDSjtBQUVBO01BQ0k7VUFDSSwyQkFBMkI7TUFDL0I7TUFDQTtVQUNJLHdCQUF3QjtNQUM1QjtFQUNKO0FBUEE7TUFDSTtVQUNJLDJCQUEyQjtNQUMvQjtNQUNBO1VBQ0ksd0JBQXdCO01BQzVCO0VBQ0o7QUFFQTtNQUNJO1VBQ0ksMkJBQTJCO01BQy9CO01BQ0E7VUFDSSx3QkFBd0I7TUFDNUI7RUFDSjtBQVBBO01BQ0k7VUFDSSwyQkFBMkI7TUFDL0I7TUFDQTtVQUNJLHdCQUF3QjtNQUM1QjtFQUNKO0FBRUE7TUFDSTtVQUNJLDJCQUEyQjtNQUMvQjtNQUNBO1VBQ0ksd0JBQXdCO01BQzVCO0VBQ0o7QUFQQTtNQUNJO1VBQ0ksMkJBQTJCO01BQy9CO01BQ0E7VUFDSSx3QkFBd0I7TUFDNUI7RUFDSjtBQUVBO01BQ0k7VUFDSSwyQkFBMkI7TUFDL0I7TUFDQTtVQUNJLHdCQUF3QjtNQUM1QjtFQUNKO0FBUEE7TUFDSTtVQUNJLDJCQUEyQjtNQUMvQjtNQUNBO1VBQ0ksd0JBQXdCO01BQzVCO0VBQ0o7QUFFQTtNQUNJO1VBQ0ksMkJBQTJCO01BQy9CO01BQ0E7VUFDSSx3QkFBd0I7TUFDNUI7RUFDSjtBQVBBO01BQ0k7VUFDSSwyQkFBMkI7TUFDL0I7TUFDQTtVQUNJLHdCQUF3QjtNQUM1QjtFQUNKO0FBRUE7TUFDSTtVQUNJLDJCQUEyQjtNQUMvQjtNQUNBO1VBQ0ksd0JBQXdCO01BQzVCO0VBQ0o7QUFQQTtNQUNJO1VBQ0ksMkJBQTJCO01BQy9CO01BQ0E7VUFDSSx3QkFBd0I7TUFDNUI7RUFDSjtBQUVBO01BQ0k7VUFDSSwyQkFBMkI7TUFDL0I7TUFDQTtVQUNJLHdCQUF3QjtNQUM1QjtFQUNKO0FBUEE7TUFDSTtVQUNJLDJCQUEyQjtNQUMvQjtNQUNBO1VBQ0ksd0JBQXdCO01BQzVCO0VBQ0o7QUFFQTtNQUNJO1VBQ0ksMkJBQTJCO01BQy9CO01BQ0E7VUFDSSx3QkFBd0I7TUFDNUI7RUFDSjtBQVBBO01BQ0k7VUFDSSwyQkFBMkI7TUFDL0I7TUFDQTtVQUNJLHdCQUF3QjtNQUM1QjtFQUNKO0FBRUE7TUFDSTtVQUNJLDJCQUEyQjtNQUMvQjtNQUNBO1VBQ0ksd0JBQXdCO01BQzVCO0VBQ0o7QUFQQTtNQUNJO1VBQ0ksMkJBQTJCO01BQy9CO01BQ0E7VUFDSSx3QkFBd0I7TUFDNUI7RUFDSjtBQUVBO01BQ0k7VUFDSSwyQkFBMkI7TUFDL0I7TUFDQTtVQUNJLHdCQUF3QjtNQUM1QjtFQUNKO0FBUEE7TUFDSTtVQUNJLDJCQUEyQjtNQUMvQjtNQUNBO1VBQ0ksd0JBQXdCO01BQzVCO0VBQ0o7QUFFQTtNQUNJO1VBQ0ksNEJBQTRCO01BQ2hDO01BQ0E7VUFDSSx3QkFBd0I7TUFDNUI7RUFDSjtBQVBBO01BQ0k7VUFDSSw0QkFBNEI7TUFDaEM7TUFDQTtVQUNJLHdCQUF3QjtNQUM1QjtFQUNKO0FBRUE7TUFDSTtVQUNJLDRCQUE0QjtNQUNoQztNQUNBO1VBQ0ksd0JBQXdCO01BQzVCO0VBQ0o7QUFQQTtNQUNJO1VBQ0ksNEJBQTRCO01BQ2hDO01BQ0E7VUFDSSx3QkFBd0I7TUFDNUI7RUFDSjtBQUVBO01BQ0k7VUFDSSw0QkFBNEI7TUFDaEM7TUFDQTtVQUNJLHdCQUF3QjtNQUM1QjtFQUNKO0FBUEE7TUFDSTtVQUNJLDRCQUE0QjtNQUNoQztNQUNBO1VBQ0ksd0JBQXdCO01BQzVCO0VBQ0o7QUFFQTtNQUNJO1VBQ0ksNEJBQTRCO01BQ2hDO01BQ0E7VUFDSSx3QkFBd0I7TUFDNUI7RUFDSjtBQVBBO01BQ0k7VUFDSSw0QkFBNEI7TUFDaEM7TUFDQTtVQUNJLHdCQUF3QjtNQUM1QjtFQUNKIiwiZmlsZSI6ImFwcC9hbmFseXRpYy9hbmFseXRpYy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiYm9keXtiYWNrZ3JvdW5kOmJsYWNrfVxyXG4jcHJvZ3Jlc3MtY29udGFpbmVye3dpZHRoOjgwJTtoZWlnaHQ6NDBweDtiYWNrZ3JvdW5kOiNlZGViZWU7cG9zaXRpb246YWJzb2x1dGU7dG9wOjUwJTtsZWZ0OjUwJTt0cmFuc2Zvcm06dHJhbnNsYXRlKC01MCUsLTUwJSl9XHJcblxyXG4jcHJvZ3Jlc3MtYmFye3dpZHRoOjIlO2hlaWdodDoxMDAlO2JhY2tncm91bmQ6cmVkO2NvbG9yOndoaXRlO3RleHQtYWxpZ246Y2VudGVyO2xpbmUtaGVpZ2h0OjQwcHh9XHJcblxyXG4jYnV0dG9uMSB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjYjJiMmIyXHQ7IFxyXG4gICAgYm9yZGVyOiBub25lO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgcGFkZGluZy1ib3R0b206IDI1cHggMjJweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIGZvbnQtc2l6ZTogMzZweDtcclxuICAgIG1hcmdpbjogNHB4IDE4cHg7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgfVxyXG5cclxuICBcclxuI2J1dHRvbjIge1xyXG4gICBiYWNrZ3JvdW5kLWNvbG9yOiAjYjJiMmIyXHQ7IFxyXG4gICBib3JkZXI6IG5vbmU7XHJcbiAgIGNvbG9yOiB3aGl0ZTtcclxuICAgcGFkZGluZy1ib3R0b206IDI1cHggMjJweDtcclxuICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgZm9udC1zaXplOiAzNnB4O1xyXG4gICBtYXJnaW46IDRweCAxOHB4O1xyXG4gICBjdXJzb3I6IHBvaW50ZXI7XHJcbiB9XHJcbiAjYnV0dG9uMyB7XHJcbiAgIGJhY2tncm91bmQtY29sb3I6ICNiMmIyYjJcdDsgXHJcbiAgIGJvcmRlcjogbm9uZTtcclxuICAgY29sb3I6IHdoaXRlO1xyXG4gICBwYWRkaW5nLWJvdHRvbTogMjVweCAyMnB4O1xyXG4gICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICBmb250LXNpemU6IDM2cHg7XHJcbiAgIG1hcmdpbjogNHB4IDE4cHg7XHJcbiAgIGN1cnNvcjogcG9pbnRlcjtcclxuIH1cclxuXHJcbiAjYnV0dG9uNCB7XHJcbiAgIGJhY2tncm91bmQtY29sb3I6ICNiMmIyYjJcdDtcclxuICAgYm9yZGVyOiBub25lO1xyXG4gICBjb2xvcjogd2hpdGU7XHJcbiAgIHBhZGRpbmctYm90dG9tOiAyNXB4IDIycHg7XHJcbiAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgIGZvbnQtc2l6ZTogMzZweDtcclxuICAgbWFyZ2luOiA0cHggMThweDtcclxuICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gfVxyXG4gXHJcbiAgI2FsbHtcclxuICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgcG9zaXRpb246IGZpeGVkO1xyXG4gICAgIGxlZnQ6IDA7XHJcbiAgICAgYm90dG9tOiAwO1xyXG4gICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgIGJhY2tncm91bmQtY29sb3I6IFx0XHQjYjJiMmIyO1xyXG4gICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBcclxuICB9XHJcbiAgI21haW5jb250ZW50aG9tZXtcclxuICAgIHBhZGRpbmctYm90dG9tOiAyMCVcclxuICAgXHJcbiB9XHJcbiAgZGl2LmhvbWUge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIH1cclxuXHJcbiAgZGl2LmhvbWUgaW5wdXQge1xyXG4gICAgYmFja2dyb3VuZDogdXJsKCdzcmMvYXBwL2hlYWRlci9pbWFnZXMvaG9tZS5zdmcnKSBuby1yZXBlYXQ7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICB3aWR0aDogNTBweDtcclxuICAgIGhlaWdodDogNTBweDtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICB9XHJcbiAgZGl2LmFuYWx5dGljIHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICB9XHJcblxyXG4gIGRpdi5hbmFseXRpYyBpbnB1dCB7XHJcbiAgICBiYWNrZ3JvdW5kOiB1cmwoJ3NyYy9hcHAvaGVhZGVyL2ltYWdlcy9hbmFseXRpYy5zdmcnKSBuby1yZXBlYXQ7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICB3aWR0aDogNTBweDtcclxuICAgIGhlaWdodDogNTBweDtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICB9XHJcblxyXG4gIGRpdi5ub3RpZmljYXRpb24ge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIH1cclxuXHJcbiAgZGl2Lm5vdGlmaWNhdGlvbiBpbnB1dCB7XHJcbiAgICBiYWNrZ3JvdW5kOiB1cmwoJ3NyYy9hcHAvY2FsZW5kYXIvaW1hZ2VzL291dGxpbmUtdG9kYXktMjRweC5zdmcnKSBuby1yZXBlYXQ7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICB3aWR0aDogNTBweDtcclxuICAgIGhlaWdodDogNTBweDtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICB9XHJcbiAgXHJcbiAgZGl2LnNldHRpbmcge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIH1cclxuXHJcbiAgZGl2LnNldHRpbmcgaW5wdXQge1xyXG4gICAgYmFja2dyb3VuZDogdXJsKCdzcmMvYXBwL2hlYWRlci9pbWFnZXMvc2V0dGluZ3Muc3ZnJykgbm8tcmVwZWF0O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgd2lkdGg6IDUwcHg7XHJcbiAgICBoZWlnaHQ6IDUwcHg7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgfVxyXG4gIHNwYW4uaG9tZSB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDMycHg7XHJcbiAgICBsZWZ0OiA2cHg7XHJcbiAgfVxyXG4gIFxyXG4gIHNwYW4uYW5hbHl0aWMge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAzMnB4O1xyXG4gICAgbGVmdDogMnB4O1xyXG4gIH1cclxuXHJcbiAgc3Bhbi5ub3RpZmljYXRpb24ge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAzMnB4O1xyXG4gICAgbGVmdDogMnB4O1xyXG4gIH1cclxuXHJcbiAgc3Bhbi5zZXR0aW5nIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogMzJweDtcclxuICAgIGxlZnQ6IDEycHg7XHJcbiAgfVxyXG5cclxuXHJcbiAgI215UHJvZ3Jlc3Mge1xyXG4gICAgbWFyZ2luLXRvcDogNjBweDtcclxuICAgIFxyXG4gICAgYm9yZGVyLXJhZGl1czogNDVweDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgXHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZGRkO1xyXG4gICBcclxuICB9XHJcbiAgXHJcbiAgI215QmFyIHtcclxuICAgIG1hcmdpbi10b3A6IDYwcHg7XHJcbiAgICBcclxuICAgIGJvcmRlci1yYWRpdXM6IDQ1cHg7XHJcbiAgICBwYWRkaW5nLXRvcDogMTVweDtcclxuICAgIHdpZHRoOiAxJTtcclxuICAgIGhlaWdodDogMTVweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IFx0IzAwMDBGRjtcclxuICAgXHJcbiAgfVxyXG5cclxuXHJcblxyXG5cclxuICBAaW1wb3J0IHVybChcImh0dHBzOi8vZm9udHMuZ29vZ2xlYXBpcy5jb20vY3NzP2ZhbWlseT1CaXR0ZXJcIik7XHJcblxyXG4gICoge1xyXG4gICAgICAvKiBib3JkZXI6IDFweCBzb2xpZCByZWQ7ICovXHJcbiAgfVxyXG4gIFxyXG4gIDpyb290IHtcclxuICAgICAgLS1mb250OiBcIkJpdHRlclwiLCBzZXJpZjtcclxuICAgICAgLS10aXRsZS1zaXplOiAzNnB4O1xyXG4gICAgICAtLXN1Yi1zaXplOiAxOHB4O1xyXG4gIH1cclxuICBcclxuICBib2R5IHtcclxuICAgICAgYmFja2dyb3VuZDogI2U1ZTVlNTtcclxuICB9XHJcbiAgXHJcbiAgLnRpdGxlIGgxIHtcclxuICAgICAgbWFyZ2luOiA0cHg7XHJcbiAgICAgIGZvbnQtZmFtaWx5OiB2YXIoLS1mb250KTtcclxuICAgICAgZm9udC1zaXplOiB2YXIoLS10aXRsZS1zaXplKTtcclxuICAgICAgY29sb3I6ICMzMzM7XHJcbiAgfVxyXG4gIFxyXG4gIC50aXRsZSBwIHtcclxuICAgICAgbWFyZ2luOiA0cHg7XHJcbiAgICAgIHBhZGRpbmctYm90dG9tOiAyNXB4O1xyXG4gICAgICBmb250LWZhbWlseTogdmFyKC0tZm9udCk7XHJcbiAgICAgIGZvbnQtc2l6ZTogdmFyKC0tc3ViLXNpemUpO1xyXG4gICAgICBjb2xvcjogIzg4ODtcclxuICB9XHJcbiAgXHJcbiAgLmNvbnRhaW5lciB7XHJcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICB9XHJcbiAgXHJcbiAgLmdpdGh1YiB7XHJcbiAgICAgIG1hcmdpbjogNDBweDtcclxuICB9XHJcbiAgXHJcbiAgLnByb2dyZXNzIHtcclxuICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgICB3aWR0aDogNDAwcHg7XHJcbiAgICAgIGhlaWdodDogNTBweDtcclxuICAgICAgbWFyZ2luOiAzNXB4O1xyXG4gICAgICBib3JkZXItcmFkaXVzOiAyMHB4O1xyXG4gICAgICBiYWNrZ3JvdW5kOiAjZjlmOWY5O1xyXG4gIH1cclxuICBcclxuICAuYmFyIHtcclxuICAgICAgYm9yZGVyLXJhZGl1czogMjBweDtcclxuICAgICAgd2lkdGg6IDAlO1xyXG4gICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgIHRyYW5zaXRpb246IHdpZHRoO1xyXG4gICAgICB0cmFuc2l0aW9uLWR1cmF0aW9uOiAxcztcclxuICAgICAgdHJhbnNpdGlvbi10aW1pbmctZnVuY3Rpb246IGN1YmljLWJlemllcigwLjM2LCAwLjU1LCAwLjYzLCAwLjQ4KTtcclxuICAgICAgXHJcbiAgfVxyXG4gIFxyXG4gIC5tb2JpbGUge1xyXG4gICAgICBkaXNwbGF5OiBub25lO1xyXG4gIH1cclxuICBcclxuICAuc2hhZG93IHtcclxuICAgICAgLyogMjUgNTAgKi9cclxuICAgICAgYm94LXNoYWRvdzogMHB4IDQ1cHggNTBweCByZ2JhKDAsIDAsIDAsIDAuMjUpO1xyXG4gIH1cclxuICBcclxuICAuY3Jvc3NlcyB7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICNkZmRiZTU7XHJcbiAgICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybChcImRhdGE6aW1hZ2Uvc3ZnK3htbCwlM0NzdmcgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJyB3aWR0aD0nMjUnIGhlaWdodD0nMjUnIHZpZXdCb3g9JzAgMCA0MCA0MCclM0UlM0NnIGZpbGwtcnVsZT0nZXZlbm9kZCclM0UlM0NnIGZpbGw9JyUyMzlDOTJBQycgZmlsbC1vcGFjaXR5PScwLjQnJTNFJTNDcGF0aCBkPSdNMCAzOC41OWwyLjgzLTIuODMgMS40MSAxLjQxTDEuNDEgNDBIMHYtMS40MXpNMCAxLjRsMi44MyAyLjgzIDEuNDEtMS40MUwxLjQxIDBIMHYxLjQxek0zOC41OSA0MGwtMi44My0yLjgzIDEuNDEtMS40MUw0MCAzOC41OVY0MGgtMS40MXpNNDAgMS40MWwtMi44MyAyLjgzLTEuNDEtMS40MUwzOC41OSAwSDQwdjEuNDF6TTIwIDE4LjZsMi44My0yLjgzIDEuNDEgMS40MUwyMS40MSAyMGwyLjgzIDIuODMtMS40MSAxLjQxTDIwIDIxLjQxbC0yLjgzIDIuODMtMS40MS0xLjQxTDE4LjU5IDIwbC0yLjgzLTIuODMgMS40MS0xLjQxTDIwIDE4LjU5eicvJTNFJTNDL2clM0UlM0MvZyUzRSUzQy9zdmclM0VcIik7XHJcbiAgfVxyXG4gIFxyXG4gIC5qdXBpdGVyIHtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2YzOTVhNTtcclxuICAgICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiZGF0YTppbWFnZS9zdmcreG1sLCUzQ3N2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPSc1MicgaGVpZ2h0PSc1Micgdmlld0JveD0nMCAwIDUyIDUyJyUzRSUzQ3BhdGggZmlsbD0nJTIzZTZjY2E1JyBmaWxsLW9wYWNpdHk9JzAuNCcgZD0nTTAgMTcuODNWMGgxNy44M2EzIDMgMCAwIDEtNS42NiAySDUuOUE1IDUgMCAwIDEgMiA1Ljl2Ni4yN2EzIDMgMCAwIDEtMiA1LjY2em0wIDE4LjM0YTMgMyAwIDAgMSAyIDUuNjZ2Ni4yN0E1IDUgMCAwIDEgNS45IDUyaDYuMjdhMyAzIDAgMCAxIDUuNjYgMEgwVjM2LjE3ek0zNi4xNyA1MmEzIDMgMCAwIDEgNS42NiAwaDYuMjdhNSA1IDAgMCAxIDMuOS0zLjl2LTYuMjdhMyAzIDAgMCAxIDAtNS42NlY1MkgzNi4xN3pNMCAzMS45M3YtOS43OGE1IDUgMCAwIDEgMy44LjcybDQuNDMtNC40M2EzIDMgMCAxIDEgMS40MiAxLjQxTDUuMiAyNC4yOGE1IDUgMCAwIDEgMCA1LjUybDQuNDQgNC40M2EzIDMgMCAxIDEtMS40MiAxLjQyTDMuOCAzMS4yYTUgNSAwIDAgMS0zLjguNzJ6bTUyLTE0LjFhMyAzIDAgMCAxIDAtNS42NlY1LjlBNSA1IDAgMCAxIDQ4LjEgMmgtNi4yN2EzIDMgMCAwIDEtNS42Ni0ySDUydjE3Ljgzem0wIDE0LjFhNC45NyA0Ljk3IDAgMCAxLTEuNzItLjcybC00LjQzIDQuNDRhMyAzIDAgMSAxLTEuNDEtMS40Mmw0LjQzLTQuNDNhNSA1IDAgMCAxIDAtNS41MmwtNC40My00LjQzYTMgMyAwIDEgMSAxLjQxLTEuNDFsNC40MyA0LjQzYy41My0uMzUgMS4xMi0uNiAxLjcyLS43MnY5Ljc4ek0yMi4xNSAwaDkuNzhhNSA1IDAgMCAxLS43MiAzLjhsNC40NCA0LjQzYTMgMyAwIDEgMS0xLjQyIDEuNDJMMjkuOCA1LjJhNSA1IDAgMCAxLTUuNTIgMGwtNC40MyA0LjQ0YTMgMyAwIDEgMS0xLjQxLTEuNDJsNC40My00LjQzYTUgNSAwIDAgMS0uNzItMy44em0wIDUyYy4xMy0uNi4zNy0xLjE5LjcyLTEuNzJsLTQuNDMtNC40M2EzIDMgMCAxIDEgMS40MS0xLjQxbDQuNDMgNC40M2E1IDUgMCAwIDEgNS41MiAwbDQuNDMtNC40M2EzIDMgMCAxIDEgMS40MiAxLjQxbC00LjQ0IDQuNDNjLjM2LjUzLjYgMS4xMi43MiAxLjcyaC05Ljc4em05Ljc1LTI0YTUgNSAwIDAgMS0zLjkgMy45djYuMjdhMyAzIDAgMSAxLTIgMFYzMS45YTUgNSAwIDAgMS0zLjktMy45aC02LjI3YTMgMyAwIDEgMSAwLTJoNi4yN2E1IDUgMCAwIDEgMy45LTMuOXYtNi4yN2EzIDMgMCAxIDEgMiAwdjYuMjdhNSA1IDAgMCAxIDMuOSAzLjloNi4yN2EzIDMgMCAxIDEgMCAySDMxLjl6JyUzRSUzQy9wYXRoJTNFJTNDL3N2ZyUzRVwiKTtcclxuICB9XHJcbiAgXHJcbiAgLnBpYW5vIHtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2NjY2NjYztcclxuICAgICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiZGF0YTppbWFnZS9zdmcreG1sLCUzQ3N2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPSc3MCcgaGVpZ2h0PSc0Nicgdmlld0JveD0nMCAwIDcwIDQ2JyUzRSUzQ2cgZmlsbC1ydWxlPSdldmVub2RkJyUzRSUzQ2cgZmlsbD0nJTIzMzMzMzMzJyBmaWxsLW9wYWNpdHk9JzAuNCclM0UlM0Nwb2x5Z29uIHBvaW50cz0nNjggNDQgNjIgNDQgNjIgNDYgNTYgNDYgNTYgNDQgNTIgNDQgNTIgNDYgNDYgNDYgNDYgNDQgNDAgNDQgNDAgNDYgMzggNDYgMzggNDQgMzIgNDQgMzIgNDYgMjYgNDYgMjYgNDQgMjIgNDQgMjIgNDYgMTYgNDYgMTYgNDQgMTIgNDQgMTIgNDYgNiA0NiA2IDQ0IDAgNDQgMCA0MiA4IDQyIDggMjggNiAyOCA2IDAgMTIgMCAxMiAyOCAxMCAyOCAxMCA0MiAxOCA0MiAxOCAyOCAxNiAyOCAxNiAwIDIyIDAgMjIgMjggMjAgMjggMjAgNDIgMjggNDIgMjggMjggMjYgMjggMjYgMCAzMiAwIDMyIDI4IDMwIDI4IDMwIDQyIDM4IDQyIDM4IDAgNDAgMCA0MCA0MiA0OCA0MiA0OCAyOCA0NiAyOCA0NiAwIDUyIDAgNTIgMjggNTAgMjggNTAgNDIgNTggNDIgNTggMjggNTYgMjggNTYgMCA2MiAwIDYyIDI4IDYwIDI4IDYwIDQyIDY4IDQyIDY4IDAgNzAgMCA3MCA0NiA2OCA0NicvJTNFJTNDL2clM0UlM0MvZyUzRSUzQy9zdmclM0VcIik7XHJcbiAgfVxyXG4gIFxyXG4gIC5kb21pbm9zIHtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjZiZDtcclxuICAgICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiZGF0YTppbWFnZS9zdmcreG1sLCUzQ3N2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMjYnIGhlaWdodD0nODQnIHZpZXdCb3g9JzAgMCAxMjYgODQnJTNFJTNDZyBmaWxsLXJ1bGU9J2V2ZW5vZGQnJTNFJTNDZyBmaWxsPSclMjMzMzMzMzMnIGZpbGwtb3BhY2l0eT0nMC40JyUzRSUzQ3BhdGggZD0nTTEyNiA4M3YxSDB2LTJoNDBWNDJIMHYtMmg0MFYwaDJ2NDBoNDBWMGgydjQwaDQwVjBoMnY4M3ptLTItMVY0Mkg4NHY0MGg0MHpNODIgNDJINDJ2NDBoNDBWNDJ6bS01MC02YTQgNCAwIDEgMSAwLTggNCA0IDAgMCAxIDAgOHpNOCAxMmE0IDQgMCAxIDEgMC04IDQgNCAwIDAgMSAwIDh6bTk2IDEyYTQgNCAwIDEgMSAwLTggNCA0IDAgMCAxIDAgOHptLTQyIDBhNCA0IDAgMSAxIDAtOCA0IDQgMCAwIDEgMCA4em0zMC0xMmE0IDQgMCAxIDEgMC04IDQgNCAwIDAgMSAwIDh6TTIwIDU0YTQgNCAwIDEgMSAwLTggNCA0IDAgMCAxIDAgOHptMTIgMjRhNCA0IDAgMSAxIDAtOCA0IDQgMCAwIDEgMCA4ek04IDU0YTQgNCAwIDEgMSAwLTggNCA0IDAgMCAxIDAgOHptMjQgMGE0IDQgMCAxIDEgMC04IDQgNCAwIDAgMSAwIDh6TTggNzhhNCA0IDAgMSAxIDAtOCA0IDQgMCAwIDEgMCA4em0xMiAwYTQgNCAwIDEgMSAwLTggNCA0IDAgMCAxIDAgOHptNTQgMGE0IDQgMCAxIDEgMC04IDQgNCAwIDAgMSAwIDh6TTUwIDU0YTQgNCAwIDEgMSAwLTggNCA0IDAgMCAxIDAgOHptMjQgMGE0IDQgMCAxIDEgMC04IDQgNCAwIDAgMSAwIDh6TTUwIDc4YTQgNCAwIDEgMSAwLTggNCA0IDAgMCAxIDAgOHptNTQtMTJhNCA0IDAgMSAxIDAtOCA0IDQgMCAwIDEgMCA4em0xMiAxMmE0IDQgMCAxIDEgMC04IDQgNCAwIDAgMSAwIDh6TTkyIDU0YTQgNCAwIDEgMSAwLTggNCA0IDAgMCAxIDAgOHptMjQgMGE0IDQgMCAxIDEgMC04IDQgNCAwIDAgMSAwIDh6TTkyIDc4YTQgNCAwIDEgMSAwLTggNCA0IDAgMCAxIDAgOHptMjQtNDJhNCA0IDAgMSAxIDAtOCA0IDQgMCAwIDEgMCA4eicvJTNFJTNDL2clM0UlM0MvZyUzRSUzQy9zdmclM0VcIik7XHJcbiAgfVxyXG4gIFxyXG4gIC5waWUge1xyXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmFhY2E4O1xyXG4gICAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJkYXRhOmltYWdlL3N2Zyt4bWwsJTNDc3ZnIHhtbG5zPSdodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Zycgd2lkdGg9JzUwJyBoZWlnaHQ9JzUwJyB2aWV3Qm94PScwIDAgNjAgNjAnJTNFJTNDZyBmaWxsLXJ1bGU9J2V2ZW5vZGQnJTNFJTNDZyBmaWxsPSclMjNmNTViNTMnIGZpbGwtb3BhY2l0eT0nMC40JyBmaWxsLXJ1bGU9J25vbnplcm8nJTNFJTNDcGF0aCBkPSdNMjkgNTguNThsNy4zOC03LjM5QTMwLjk1IDMwLjk1IDAgMCAxIDI5IDM3Ljg0YTMwLjk1IDMwLjk1IDAgMCAxLTcuMzggMTMuMzZsNy4zNyA3LjM4em0xLjQgMS40MWwuMDEuMDFoLTIuODRsLTcuMzctNy4zOEEzMC45NSAzMC45NSAwIDAgMSA2Ljg0IDYwSDB2LTEuMDJhMjguOSAyOC45IDAgMCAwIDE4Ljc5LTcuNzhMMCAzMi40MXYtNC44NEwxOC43OCA4Ljc5QTI4LjkgMjguOSAwIDAgMCAwIDEuMDJWMGg2Ljg0YTMwLjk1IDMwLjk1IDAgMCAxIDEzLjM1IDcuMzhMMjcuNTcgMGgyLjg0bDcuMzkgNy4zOEEzMC45NSAzMC45NSAwIDAgMSA1MS4xNiAwSDYwdjI3LjU4LS4wMVY2MGgtOC44NGEzMC45NSAzMC45NSAwIDAgMS0xMy4zNy03LjRMMzAuNCA2MHpNMjkgMS40MWwtNy40IDcuMzhBMzAuOTUgMzAuOTUgMCAwIDEgMjkgMjIuMTYgMzAuOTUgMzAuOTUgMCAwIDEgMzYuMzggOC44TDI5IDEuNHpNNTggMUEyOC45IDI4LjkgMCAwIDAgMzkuMiA4LjhMNTggMjcuNThWMS4wMnptLTIwLjIgOS4yQTI4LjkgMjguOSAwIDAgMCAzMC4wMiAyOWgyNi41NkwzNy44IDEwLjIxek0zMC4wMiAzMWEyOC45IDI4LjkgMCAwIDAgNy43NyAxOC43OWwxOC43OS0xOC43OUgzMC4wMnptOS4xOCAyMC4yQTI4LjkgMjguOSAwIDAgMCA1OCA1OVYzMi40TDM5LjIgNTEuMTl6bS0xOS0xLjRhMjguOSAyOC45IDAgMCAwIDcuNzgtMTguOEgxLjQxbDE4LjggMTguOHptNy43OC0yMC44QTI4LjkgMjguOSAwIDAgMCAyMC4yIDEwLjJMMS40MSAyOWgyNi41N3onLyUzRSUzQy9nJTNFJTNDL2clM0UlM0Mvc3ZnJTNFXCIpO1xyXG4gIH1cclxuICBcclxuICAuYmVlcyB7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmY2M4NDY7XHJcbiAgICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybChcImRhdGE6aW1hZ2Uvc3ZnK3htbCwlM0NzdmcgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJyB3aWR0aD0nMjgnIGhlaWdodD0nNDknIHZpZXdCb3g9JzAgMCAyOCA0OSclM0UlM0NnIGZpbGwtcnVsZT0nZXZlbm9kZCclM0UlM0NnIGlkPSdoZXhhZ29ucycgZmlsbD0nJTIzN2E0OTQ4JyBmaWxsLW9wYWNpdHk9JzAuODMnIGZpbGwtcnVsZT0nbm9uemVybyclM0UlM0NwYXRoIGQ9J00xMy45OSA5LjI1bDEzIDcuNXYxNWwtMTMgNy41TDEgMzEuNzV2LTE1bDEyLjk5LTcuNXpNMyAxNy45djEyLjdsMTAuOTkgNi4zNCAxMS02LjM1VjE3LjlsLTExLTYuMzRMMyAxNy45ek0wIDE1bDEyLjk4LTcuNVYwaC0ydjYuMzVMMCAxMi42OXYyLjN6bTAgMTguNUwxMi45OCA0MXY4aC0ydi02Ljg1TDAgMzUuODF2LTIuM3pNMTUgMHY3LjVMMjcuOTkgMTVIMjh2LTIuMzFoLS4wMUwxNyA2LjM1VjBoLTJ6bTAgNDl2LThsMTIuOTktNy41SDI4djIuMzFoLS4wMUwxNyA0Mi4xNVY0OWgtMnonLyUzRSUzQy9nJTNFJTNDL2clM0UlM0Mvc3ZnJTNFXCIpO1xyXG4gIH1cclxuICBcclxuICAuZm9vZCB7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmNiMDU7XHJcbiAgICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybChcImRhdGE6aW1hZ2Uvc3ZnK3htbCwlM0NzdmcgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJyB3aWR0aD0nMTIwJyBoZWlnaHQ9JzEyMCcgdmlld0JveD0nMCAwIDI2MCAyNjAnJTNFJTNDZyBmaWxsLXJ1bGU9J2V2ZW5vZGQnJTNFJTNDZyBmaWxsPSclMjNlYzAwOGMnIGZpbGwtb3BhY2l0eT0nMC44MyclM0UlM0NwYXRoIGQ9J00yNC4zNyAxNmMuMi42NS4zOSAxLjMyLjU0IDJIMjEuMTdsMS4xNyAyLjM0LjQ1LjktLjI0LjExVjI4YTUgNSAwIDAgMS0yLjIzIDguOTRsLS4wMi4wNmE4IDggMCAwIDEtNy43NSA2aC0yMGE4IDggMCAwIDEtNy43NC02bC0uMDItLjA2QTUgNSAwIDAgMS0xNy40NSAyOHYtNi43NmwtLjc5LTEuNTgtLjQ0LS45LjktLjQ0LjYzLS4zMkgtMjBhMjMuMDEgMjMuMDEgMCAwIDEgNDQuMzctMnptLTM2LjgyIDJhMSAxIDAgMCAwLS40NC4xbC0zLjEgMS41Ni44OSAxLjc5IDEuMzEtLjY2YTMgMyAwIDAgMSAyLjY5IDBsMi4yIDEuMWExIDEgMCAwIDAgLjkgMGwyLjIxLTEuMWEzIDMgMCAwIDEgMi42OSAwbDIuMiAxLjFhMSAxIDAgMCAwIC45IDBsMi4yMS0xLjFhMyAzIDAgMCAxIDIuNjkgMGwyLjIgMS4xYTEgMSAwIDAgMCAuODYuMDJsMi44OC0xLjI3YTMgMyAwIDAgMSAyLjQzIDBsMi44OCAxLjI3YTEgMSAwIDAgMCAuODUtLjAybDMuMS0xLjU1LS44OS0xLjc5LTEuNDIuNzFhMyAzIDAgMCAxLTIuNTYuMDZsLTIuNzctMS4yM2ExIDEgMCAwIDAtLjQtLjA5aC0uMDFhMSAxIDAgMCAwLS40LjA5bC0yLjc4IDEuMjNhMyAzIDAgMCAxLTIuNTYtLjA2bC0yLjMtMS4xNWExIDEgMCAwIDAtLjQ1LS4xMWgtLjAxYTEgMSAwIDAgMC0uNDQuMUwuOSAxOS4yMmEzIDMgMCAwIDEtMi42OSAwbC0yLjItMS4xYTEgMSAwIDAgMC0uNDUtLjExaC0uMDFhMSAxIDAgMCAwLS40NC4xbC0yLjIxIDEuMTFhMyAzIDAgMCAxLTIuNjkgMGwtMi4yLTEuMWExIDEgMCAwIDAtLjQ1LS4xMWgtLjAxem0wLTJoLTQuOWEyMS4wMSAyMS4wMSAwIDAgMSAzOS42MSAwaC0yLjA5bC0uMDYtLjEzLS4yNi4xM2gtMzIuMzF6bTMwLjM1IDcuNjhsMS4zNi0uNjhoMS4zdjJoLTM2di0xLjE1bC4zNC0uMTcgMS4zNi0uNjhoMi41OWwxLjM2LjY4YTMgMyAwIDAgMCAyLjY5IDBsMS4zNi0uNjhoMi41OWwxLjM2LjY4YTMgMyAwIDAgMCAyLjY5IDBMMi4yNiAyM2gyLjU5bDEuMzYuNjhhMyAzIDAgMCAwIDIuNTYuMDZsMS42Ny0uNzRoMy4yM2wxLjY3Ljc0YTMgMyAwIDAgMCAyLjU2LS4wNnpNLTEzLjgyIDI3bDE2LjM3IDQuOTFMMTguOTMgMjdoLTMyLjc1em0tLjYzIDJoLjM0bDE2LjY2IDUgMTYuNjctNWguMzNhMyAzIDAgMSAxIDAgNmgtMzRhMyAzIDAgMSAxIDAtNnptMS4zNSA4YTYgNiAwIDAgMCA1LjY1IDRoMjBhNiA2IDAgMCAwIDUuNjYtNEgtMTMuMXonLyUzRSUzQ3BhdGggaWQ9J3BhdGg2X2ZpbGwtY29weScgZD0nTTI4NC4zNyAxNmMuMi42NS4zOSAxLjMyLjU0IDJIMjgxLjE3bDEuMTcgMi4zNC40NS45LS4yNC4xMVYyOGE1IDUgMCAwIDEtMi4yMyA4Ljk0bC0uMDIuMDZhOCA4IDAgMCAxLTcuNzUgNmgtMjBhOCA4IDAgMCAxLTcuNzQtNmwtLjAyLS4wNmE1IDUgMCAwIDEtMi4yNC04Ljk0di02Ljc2bC0uNzktMS41OC0uNDQtLjkuOS0uNDQuNjMtLjMySDI0MGEyMy4wMSAyMy4wMSAwIDAgMSA0NC4zNy0yem0tMzYuODIgMmExIDEgMCAwIDAtLjQ0LjFsLTMuMSAxLjU2Ljg5IDEuNzkgMS4zMS0uNjZhMyAzIDAgMCAxIDIuNjkgMGwyLjIgMS4xYTEgMSAwIDAgMCAuOSAwbDIuMjEtMS4xYTMgMyAwIDAgMSAyLjY5IDBsMi4yIDEuMWExIDEgMCAwIDAgLjkgMGwyLjIxLTEuMWEzIDMgMCAwIDEgMi42OSAwbDIuMiAxLjFhMSAxIDAgMCAwIC44Ni4wMmwyLjg4LTEuMjdhMyAzIDAgMCAxIDIuNDMgMGwyLjg4IDEuMjdhMSAxIDAgMCAwIC44NS0uMDJsMy4xLTEuNTUtLjg5LTEuNzktMS40Mi43MWEzIDMgMCAwIDEtMi41Ni4wNmwtMi43Ny0xLjIzYTEgMSAwIDAgMC0uNC0uMDloLS4wMWExIDEgMCAwIDAtLjQuMDlsLTIuNzggMS4yM2EzIDMgMCAwIDEtMi41Ni0uMDZsLTIuMy0xLjE1YTEgMSAwIDAgMC0uNDUtLjExaC0uMDFhMSAxIDAgMCAwLS40NC4xbC0yLjIxIDEuMTFhMyAzIDAgMCAxLTIuNjkgMGwtMi4yLTEuMWExIDEgMCAwIDAtLjQ1LS4xMWgtLjAxYTEgMSAwIDAgMC0uNDQuMWwtMi4yMSAxLjExYTMgMyAwIDAgMS0yLjY5IDBsLTIuMi0xLjFhMSAxIDAgMCAwLS40NS0uMTFoLS4wMXptMC0yaC00LjlhMjEuMDEgMjEuMDEgMCAwIDEgMzkuNjEgMGgtMi4wOWwtLjA2LS4xMy0uMjYuMTNoLTMyLjMxem0zMC4zNSA3LjY4bDEuMzYtLjY4aDEuM3YyaC0zNnYtMS4xNWwuMzQtLjE3IDEuMzYtLjY4aDIuNTlsMS4zNi42OGEzIDMgMCAwIDAgMi42OSAwbDEuMzYtLjY4aDIuNTlsMS4zNi42OGEzIDMgMCAwIDAgMi42OSAwbDEuMzYtLjY4aDIuNTlsMS4zNi42OGEzIDMgMCAwIDAgMi41Ni4wNmwxLjY3LS43NGgzLjIzbDEuNjcuNzRhMyAzIDAgMCAwIDIuNTYtLjA2ek0yNDYuMTggMjdsMTYuMzcgNC45MUwyNzguOTMgMjdoLTMyLjc1em0tLjYzIDJoLjM0bDE2LjY2IDUgMTYuNjctNWguMzNhMyAzIDAgMSAxIDAgNmgtMzRhMyAzIDAgMSAxIDAtNnptMS4zNSA4YTYgNiAwIDAgMCA1LjY1IDRoMjBhNiA2IDAgMCAwIDUuNjYtNEgyNDYuOXonLyUzRSUzQ3BhdGggZD0nTTE1OS41IDIxLjAyQTkgOSAwIDAgMCAxNTEgMTVoLTQyYTkgOSAwIDAgMC04LjUgNi4wMiA2IDYgMCAwIDAgLjAyIDExLjk2QTguOTkgOC45OSAwIDAgMCAxMDkgNDVoNDJhOSA5IDAgMCAwIDguNDgtMTIuMDIgNiA2IDAgMCAwIC4wMi0xMS45NnpNMTUxIDE3aC00MmE3IDcgMCAwIDAtNi4zMyA0aDU0LjY2YTcgNyAwIDAgMC02LjMzLTR6bS05LjM0IDI2YTguOTggOC45OCAwIDAgMCAzLjM0LTdoLTJhNyA3IDAgMCAxLTcgN2gtNC4zNGE4Ljk4IDguOTggMCAwIDAgMy4zNC03aC0yYTcgNyAwIDAgMS03IDdoLTQuMzRhOC45OCA4Ljk4IDAgMCAwIDMuMzQtN2gtMmE3IDcgMCAwIDEtNyA3aC03YTcgNyAwIDEgMSAwLTE0aDQyYTcgNyAwIDEgMSAwIDE0aC05LjM0ek0xMDkgMjdhOSA5IDAgMCAwLTcuNDggNEgxMDFhNCA0IDAgMSAxIDAtOGg1OGE0IDQgMCAwIDEgMCA4aC0uNTJhOSA5IDAgMCAwLTcuNDgtNGgtNDJ6Jy8lM0UlM0NwYXRoIGQ9J00zOSAxMTVhOCA4IDAgMSAwIDAtMTYgOCA4IDAgMCAwIDAgMTZ6bTYtOGE2IDYgMCAxIDEtMTIgMCA2IDYgMCAwIDEgMTIgMHptLTMtMjl2LTJoOHYtNkg0MGE0IDQgMCAwIDAtNCA0djEwSDIybC0xLjMzIDQtLjY3IDJoMi4xOUwyNiAxMzBoMjZsMy44MS00MEg1OGwtLjY3LTJMNTYgODRINDJ2LTZ6bS00LTR2MTBoMlY3NGg4di0yaC04YTIgMiAwIDAgMC0yIDJ6bTIgMTJoMTQuNTZsLjY3IDJIMjIuNzdsLjY3LTJINDB6bTEzLjggNEgyNC4ybDMuNjIgMzhoMjIuMzZsMy42Mi0zOHonLyUzRSUzQ3BhdGggZD0nTTEyOSA5MmgtNnY0aC02djRoLTZ2MTRoLTNsLjI0IDIgMy43NiAzMmgzNmwzLjc2LTMyIC4yNC0yaC0zdi0xNGgtNnYtNGgtNnYtNGgtOHptMTggMjJ2LTEyaC00djRoM3Y4aDF6bS0zIDB2LTZoLTR2Nmg0em0tNiA2di0xNmgtNHYxOS4xN2MxLjYtLjcgMi45Ny0xLjggNC0zLjE3em0tNiAzLjhWMTAwaC00djIzLjhhMTAuMDQgMTAuMDQgMCAwIDAgNCAwem0tNi0uNjNWMTA0aC00djE2YTEwLjA0IDEwLjA0IDAgMCAwIDQgMy4xN3ptLTYtOS4xN3YtNmgtNHY2aDR6bS02IDB2LThoM3YtNGgtNHYxMmgxem0yNy0xMnYtNGgtNHY0aDN2NGgxdi00em0tNiAwdi04aC00djRoM3Y0aDF6bS02LTR2LTRoLTR2OGgxdi00aDN6bS02IDR2LTRoLTR2OGgxdi00aDN6bTcgMjRhMTIgMTIgMCAwIDAgMTEuODMtMTBoNy45MmwtMy41MyAzMGgtMzIuNDRsLTMuNTMtMzBoNy45MkExMiAxMiAwIDAgMCAxMzAgMTI2eicvJTNFJTNDcGF0aCBkPSdNMjEyIDg2djJoLTR2LTJoNHptNCAwaC0ydjJoMnYtMnptLTIwIDB2LjFhNSA1IDAgMCAwLS41NiA5LjY1bC4wNi4yNSAxLjEyIDQuNDhhMiAyIDAgMCAwIDEuOTQgMS41MmguMDFsNy4wMiAyNC41NWEyIDIgMCAwIDAgMS45MiAxLjQ1aDQuOThhMiAyIDAgMCAwIDEuOTItMS40NWw3LjAyLTI0LjU1YTIgMiAwIDAgMCAxLjk1LTEuNTJMMjI0LjUgOTZsLjA2LS4yNWE1IDUgMCAwIDAtLjU2LTkuNjVWODZhMTQgMTQgMCAwIDAtMjggMHptNCAwaDZ2MmgtOWEzIDMgMCAxIDAgMCA2SDIyM2EzIDMgMCAxIDAgMC02SDIyMHYtMmgyYTEyIDEyIDAgMSAwLTI0IDBoMnptLTEuNDQgMTRsLTEtNGgyNC44OGwtMSA0aC0yMi44OHptOC45NSAyNmwtNi44Ni0yNGgxOC43bC02Ljg2IDI0aC00Ljk4ek0xNTAgMjQyYTIyIDIyIDAgMSAwIDAtNDQgMjIgMjIgMCAwIDAgMCA0NHptMjQtMjJhMjQgMjQgMCAxIDEtNDggMCAyNCAyNCAwIDAgMSA0OCAwem0tMjguMzggMTcuNzNsMi4wNC0uODdhNiA2IDAgMCAxIDQuNjggMGwyLjA0Ljg3YTIgMiAwIDAgMCAyLjUtLjgybDEuMTQtMS45YTYgNiAwIDAgMSAzLjc5LTIuNzVsMi4xNS0uNWEyIDIgMCAwIDAgMS41NC0yLjEybC0uMTktMi4yYTYgNiAwIDAgMSAxLjQ1LTQuNDZsMS40NS0xLjY3YTIgMiAwIDAgMCAwLTIuNjJsLTEuNDUtMS42N2E2IDYgMCAwIDEtMS40NS00LjQ2bC4yLTIuMmEyIDIgMCAwIDAtMS41NS0yLjEzbC0yLjE1LS41YTYgNiAwIDAgMS0zLjgtMi43NWwtMS4xMy0xLjlhMiAyIDAgMCAwLTIuNS0uOGwtMi4wNC44NmE2IDYgMCAwIDEtNC42OCAwbC0yLjA0LS44N2EyIDIgMCAwIDAtMi41LjgybC0xLjE0IDEuOWE2IDYgMCAwIDEtMy43OSAyLjc1bC0yLjE1LjVhMiAyIDAgMCAwLTEuNTQgMi4xMmwuMTkgMi4yYTYgNiAwIDAgMS0xLjQ1IDQuNDZsLTEuNDUgMS42N2EyIDIgMCAwIDAgMCAyLjYybDEuNDUgMS42N2E2IDYgMCAwIDEgMS40NSA0LjQ2bC0uMiAyLjJhMiAyIDAgMCAwIDEuNTUgMi4xM2wyLjE1LjVhNiA2IDAgMCAxIDMuOCAyLjc1bDEuMTMgMS45YTIgMiAwIDAgMCAyLjUuOHptMi44Mi45N2E0IDQgMCAwIDEgMy4xMiAwbDIuMDQuODdhNCA0IDAgMCAwIDQuOTktMS42MmwxLjE0LTEuOWE0IDQgMCAwIDEgMi41My0xLjg0bDIuMTUtLjVhNCA0IDAgMCAwIDMuMDktNC4yNGwtLjItMi4yYTQgNCAwIDAgMSAuOTctMi45OGwxLjQ1LTEuNjdhNCA0IDAgMCAwIDAtNS4yNGwtMS40NS0xLjY3YTQgNCAwIDAgMS0uOTctMi45N2wuMi0yLjJhNCA0IDAgMCAwLTMuMDktNC4yNWwtMi4xNS0uNWE0IDQgMCAwIDEtMi41My0xLjg0bC0xLjE0LTEuOWE0IDQgMCAwIDAtNS0xLjYybC0yLjAzLjg3YTQgNCAwIDAgMS0zLjEyIDBsLTIuMDQtLjg3YTQgNCAwIDAgMC00Ljk5IDEuNjJsLTEuMTQgMS45YTQgNCAwIDAgMS0yLjUzIDEuODRsLTIuMTUuNWE0IDQgMCAwIDAtMy4wOSA0LjI0bC4yIDIuMmE0IDQgMCAwIDEtLjk3IDIuOThsLTEuNDUgMS42N2E0IDQgMCAwIDAgMCA1LjI0bDEuNDUgMS42N2E0IDQgMCAwIDEgLjk3IDIuOTdsLS4yIDIuMmE0IDQgMCAwIDAgMy4wOSA0LjI1bDIuMTUuNWE0IDQgMCAwIDEgMi41MyAxLjg0bDEuMTQgMS45YTQgNCAwIDAgMCA1IDEuNjJsMi4wMy0uODd6TTE1MiAyMDdhMSAxIDAgMSAxIDIgMCAxIDEgMCAwIDEtMiAwem02IDJhMSAxIDAgMSAxIDIgMCAxIDEgMCAwIDEtMiAwem0tMTEgMWExIDEgMCAxIDEgMiAwIDEgMSAwIDAgMS0yIDB6bS02IDBhMSAxIDAgMSAxIDIgMCAxIDEgMCAwIDEtMiAwem0zLTVhMSAxIDAgMSAxIDIgMCAxIDEgMCAwIDEtMiAwem0tOCA4YTEgMSAwIDEgMSAyIDAgMSAxIDAgMCAxLTIgMHptMyA2YTEgMSAwIDEgMSAyIDAgMSAxIDAgMCAxLTIgMHptMCA2YTEgMSAwIDEgMSAyIDAgMSAxIDAgMCAxLTIgMHptNCA3YTEgMSAwIDEgMSAyIDAgMSAxIDAgMCAxLTIgMHptNS0yYTEgMSAwIDEgMSAyIDAgMSAxIDAgMCAxLTIgMHptNSA0YTEgMSAwIDEgMSAyIDAgMSAxIDAgMCAxLTIgMHptNC02YTEgMSAwIDEgMSAyIDAgMSAxIDAgMCAxLTIgMHptNi00YTEgMSAwIDEgMSAyIDAgMSAxIDAgMCAxLTIgMHptLTQtM2ExIDEgMCAxIDEgMiAwIDEgMSAwIDAgMS0yIDB6bTQtM2ExIDEgMCAxIDEgMiAwIDEgMSAwIDAgMS0yIDB6bS01LTRhMSAxIDAgMSAxIDIgMCAxIDEgMCAwIDEtMiAwem0tMjQgNmExIDEgMCAxIDEgMiAwIDEgMSAwIDAgMS0yIDB6bTE2IDVhNSA1IDAgMSAwIDAtMTAgNSA1IDAgMCAwIDAgMTB6bTctNWE3IDcgMCAxIDEtMTQgMCA3IDcgMCAwIDEgMTQgMHptODYtMjlhMSAxIDAgMCAwIDAgMmgyYTEgMSAwIDAgMCAwLTJoLTJ6bTE5IDlhMSAxIDAgMCAxIDEtMWgyYTEgMSAwIDAgMSAwIDJoLTJhMSAxIDAgMCAxLTEtMXptLTE0IDVhMSAxIDAgMCAwIDAgMmgyYTEgMSAwIDAgMCAwLTJoLTJ6bS0yNSAxYTEgMSAwIDAgMCAwIDJoMmExIDEgMCAwIDAgMC0yaC0yem01IDRhMSAxIDAgMCAwIDAgMmgyYTEgMSAwIDAgMCAwLTJoLTJ6bTkgMGExIDEgMCAwIDEgMS0xaDJhMSAxIDAgMCAxIDAgMmgtMmExIDEgMCAwIDEtMS0xem0xNSAxYTEgMSAwIDAgMSAxLTFoMmExIDEgMCAwIDEgMCAyaC0yYTEgMSAwIDAgMS0xLTF6bTEyLTJhMSAxIDAgMCAwIDAgMmgyYTEgMSAwIDAgMCAwLTJoLTJ6bS0xMS0xNGExIDEgMCAwIDEgMS0xaDJhMSAxIDAgMCAxIDAgMmgtMmExIDEgMCAwIDEtMS0xem0tMTkgMGExIDEgMCAwIDAgMCAyaDJhMSAxIDAgMCAwIDAtMmgtMnptNiA1YTEgMSAwIDAgMSAxLTFoMmExIDEgMCAwIDEgMCAyaC0yYTEgMSAwIDAgMS0xLTF6bS0yNSAxNWMwLS40Ny4wMS0uOTQuMDMtMS40YTUgNSAwIDAgMS0xLjctOCAzLjk5IDMuOTkgMCAwIDEgMS44OC01LjE4IDUgNSAwIDAgMSAzLjQtNi4yMiAzIDMgMCAwIDEgMS40Ni0xLjA1IDUgNSAwIDAgMSA3Ljc2LTMuMjdBMzAuODYgMzAuODYgMCAwIDEgMjQ2IDE4NGM2Ljc5IDAgMTMuMDYgMi4xOCAxOC4xNyA1Ljg4YTUgNSAwIDAgMSA3Ljc2IDMuMjcgMyAzIDAgMCAxIDEuNDcgMS4wNSA1IDUgMCAwIDEgMy40IDYuMjIgNCA0IDAgMCAxIDEuODcgNS4xOCA0Ljk4IDQuOTggMCAwIDEtMS43IDhjLjAyLjQ2LjAzLjkzLjAzIDEuNHYxaC02MnYtMXptLjgzLTcuMTdhMzAuOSAzMC45IDAgMCAwLS42MiAzLjU3IDMgMyAwIDAgMS0uNjEtNC4yYy4zNy4yOC43OC40OSAxLjIzLjYzem0xLjQ5LTQuNjFjLS4zNi44Ny0uNjggMS43Ni0uOTYgMi42OGEyIDIgMCAwIDEtLjIxLTMuNzFjLjMzLjQuNzMuNzUgMS4xNyAxLjAzem0yLjMyLTQuNTRjLS41NC44Ni0xLjAzIDEuNzYtMS40OSAyLjY4YTMgMyAwIDAgMS0uMDctNC42NyAzIDMgMCAwIDAgMS41NiAxLjk5em0xLjE0LTEuN2MuMzUtLjUuNzItLjk4IDEuMS0xLjQ2YTEgMSAwIDEgMC0xLjEgMS40NXptNS4zNC01Ljc3Yy0xLjAzLjg2LTIgMS43OS0yLjkgMi43N2EzIDMgMCAwIDAtMS4xMS0uNzcgMyAzIDAgMCAxIDQtMnptNDIuNjYgMi43N2MtLjktLjk4LTEuODctMS45LTIuOS0yLjc3YTMgMyAwIDAgMSA0LjAxIDIgMyAzIDAgMCAwLTEuMS43N3ptMS4zNCAxLjU0Yy4zOC40OC43NS45NiAxLjEgMS40NWExIDEgMCAxIDAtMS4xLTEuNDV6bTMuNzMgNS44NGMtLjQ2LS45Mi0uOTUtMS44Mi0xLjUtMi42OGEzIDMgMCAwIDAgMS41Ny0xLjk5IDMgMyAwIDAgMS0uMDcgNC42N3ptMS44IDQuNTNjLS4yOS0uOS0uNi0xLjgtLjk3LTIuNjcuNDQtLjI4Ljg0LS42MyAxLjE3LTEuMDNhMiAyIDAgMCAxLS4yIDMuN3ptMS4xNCA1LjUxYy0uMTQtMS4yMS0uMzUtMi40LS42Mi0zLjU3LjQ1LS4xNC44Ni0uMzUgMS4yMy0uNjNhMi45OSAyLjk5IDAgMCAxLS42IDQuMnpNMjc1IDIxNGEyOSAyOSAwIDAgMC01Ny45NyAwaDU3Ljk2ek03Mi4zMyAxOTguMTJjLS4yMS0uMzItLjM0LS43LS4zNC0xLjEydi0xMmgtMnYxMmE0LjAxIDQuMDEgMCAwIDAgNy4wOSAyLjU0Yy41Ny0uNjkuOTEtMS41Ny45MS0yLjU0di0xMmgtMnYxMmExLjk5IDEuOTkgMCAwIDEtMiAyIDIgMiAwIDAgMS0xLjY2LS44OHpNNzUgMTc2Yy4zOCAwIC43NC0uMDQgMS4xLS4xMmE0IDQgMCAwIDAgNi4xOSAyLjRBMTMuOTQgMTMuOTQgMCAwIDEgODQgMTg1djI0YTYgNiAwIDAgMS02IDZoLTN2OWE1IDUgMCAxIDEtMTAgMHYtOWgtM2E2IDYgMCAwIDEtNi02di0yNGExNCAxNCAwIDAgMSAxNC0xNCA1IDUgMCAwIDAgNSA1em0tMTcgMTV2MTJhMS45OSAxLjk5IDAgMCAwIDEuMjIgMS44NCAyIDIgMCAwIDAgMi40NC0uNzJjLjIxLS4zMi4zNC0uNy4zNC0xLjEydi0xMmgydjEyYTMuOTggMy45OCAwIDAgMS01LjM1IDMuNzcgMy45OCAzLjk4IDAgMCAxLS42NS0uM1YyMDlhNCA0IDAgMCAwIDQgNGgxNmE0IDQgMCAwIDAgNC00di0yNGMuMDEtMS41My0uMjMtMi44OC0uNzItNC4xNy0uNDMuMS0uODcuMTYtMS4yOC4xN2E2IDYgMCAwIDEtNS4yLTMgNyA3IDAgMCAxLTYuNDctNC44OEExMiAxMiAwIDAgMCA1OCAxODV2NnptOSAyNHY5YTMgMyAwIDEgMCA2IDB2LTloLTZ6Jy8lM0UlM0NwYXRoIGQ9J00tMTcgMTkxYTEgMSAwIDAgMCAwIDJoMmExIDEgMCAwIDAgMC0yaC0yem0xOSA5YTEgMSAwIDAgMSAxLTFoMmExIDEgMCAwIDEgMCAySDNhMSAxIDAgMCAxLTEtMXptLTE0IDVhMSAxIDAgMCAwIDAgMmgyYTEgMSAwIDAgMCAwLTJoLTJ6bS0yNSAxYTEgMSAwIDAgMCAwIDJoMmExIDEgMCAwIDAgMC0yaC0yem01IDRhMSAxIDAgMCAwIDAgMmgyYTEgMSAwIDAgMCAwLTJoLTJ6bTkgMGExIDEgMCAwIDEgMS0xaDJhMSAxIDAgMCAxIDAgMmgtMmExIDEgMCAwIDEtMS0xem0xNSAxYTEgMSAwIDAgMSAxLTFoMmExIDEgMCAwIDEgMCAyaC0yYTEgMSAwIDAgMS0xLTF6bTEyLTJhMSAxIDAgMCAwIDAgMmgyYTEgMSAwIDAgMCAwLTJINHptLTExLTE0YTEgMSAwIDAgMSAxLTFoMmExIDEgMCAwIDEgMCAyaC0yYTEgMSAwIDAgMS0xLTF6bS0xOSAwYTEgMSAwIDAgMCAwIDJoMmExIDEgMCAwIDAgMC0yaC0yem02IDVhMSAxIDAgMCAxIDEtMWgyYTEgMSAwIDAgMSAwIDJoLTJhMSAxIDAgMCAxLTEtMXptLTI1IDE1YzAtLjQ3LjAxLS45NC4wMy0xLjRhNSA1IDAgMCAxLTEuNy04IDMuOTkgMy45OSAwIDAgMSAxLjg4LTUuMTggNSA1IDAgMCAxIDMuNC02LjIyIDMgMyAwIDAgMSAxLjQ2LTEuMDUgNSA1IDAgMCAxIDcuNzYtMy4yN0EzMC44NiAzMC44NiAwIDAgMS0xNCAxODRjNi43OSAwIDEzLjA2IDIuMTggMTguMTcgNS44OGE1IDUgMCAwIDEgNy43NiAzLjI3IDMgMyAwIDAgMSAxLjQ3IDEuMDUgNSA1IDAgMCAxIDMuNCA2LjIyIDQgNCAwIDAgMSAxLjg3IDUuMTggNC45OCA0Ljk4IDAgMCAxLTEuNyA4Yy4wMi40Ni4wMy45My4wMyAxLjR2MWgtNjJ2LTF6bS44My03LjE3YTMwLjkgMzAuOSAwIDAgMC0uNjIgMy41NyAzIDMgMCAwIDEtLjYxLTQuMmMuMzcuMjguNzguNDkgMS4yMy42M3ptMS40OS00LjYxYy0uMzYuODctLjY4IDEuNzYtLjk2IDIuNjhhMiAyIDAgMCAxLS4yMS0zLjcxYy4zMy40LjczLjc1IDEuMTcgMS4wM3ptMi4zMi00LjU0Yy0uNTQuODYtMS4wMyAxLjc2LTEuNDkgMi42OGEzIDMgMCAwIDEtLjA3LTQuNjcgMyAzIDAgMCAwIDEuNTYgMS45OXptMS4xNC0xLjdjLjM1LS41LjcyLS45OCAxLjEtMS40NmExIDEgMCAxIDAtMS4xIDEuNDV6bTUuMzQtNS43N2MtMS4wMy44Ni0yIDEuNzktMi45IDIuNzdhMyAzIDAgMCAwLTEuMTEtLjc3IDMgMyAwIDAgMSA0LTJ6bTQyLjY2IDIuNzdjLS45LS45OC0xLjg3LTEuOS0yLjktMi43N2EzIDMgMCAwIDEgNC4wMSAyIDMgMyAwIDAgMC0xLjEuNzd6bTEuMzQgMS41NGMuMzguNDguNzUuOTYgMS4xIDEuNDVhMSAxIDAgMSAwLTEuMS0xLjQ1em0zLjczIDUuODRjLS40Ni0uOTItLjk1LTEuODItMS41LTIuNjhhMyAzIDAgMCAwIDEuNTctMS45OSAzIDMgMCAwIDEtLjA3IDQuNjd6bTEuOCA0LjUzYy0uMjktLjktLjYtMS44LS45Ny0yLjY3LjQ0LS4yOC44NC0uNjMgMS4xNy0xLjAzYTIgMiAwIDAgMS0uMiAzLjd6bTEuMTQgNS41MWMtLjE0LTEuMjEtLjM1LTIuNC0uNjItMy41Ny40NS0uMTQuODYtLjM1IDEuMjMtLjYzYTIuOTkgMi45OSAwIDAgMS0uNiA0LjJ6TTE1IDIxNGEyOSAyOSAwIDAgMC01Ny45NyAwaDU3Ljk2eicvJTNFJTNDL2clM0UlM0MvZyUzRSUzQy9zdmclM0VcIik7XHJcbiAgfVxyXG4gIFxyXG4gIC5mbG9vciB7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICMwMGI5ZjI7XHJcbiAgICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybChcImRhdGE6aW1hZ2Uvc3ZnK3htbCwlM0NzdmcgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJyB3aWR0aD0nMjAnIGhlaWdodD0nMjAnIHZpZXdCb3g9JzAgMCA4MCA4MCclM0UlM0NnIGZpbGw9JyUyMzkyMjc4ZicgZmlsbC1vcGFjaXR5PScwLjcxJyUzRSUzQ3BhdGggZmlsbC1ydWxlPSdldmVub2RkJyBkPSdNMCAwaDQwdjQwSDBWMHptNDAgNDBoNDB2NDBINDBWNDB6bTAtNDBoMmwtMiAyVjB6bTAgNGw0LTRoMmwtNiA2VjR6bTAgNGw4LThoMkw0MCAxMFY4em0wIDRMNTIgMGgyTDQwIDE0di0yem0wIDRMNTYgMGgyTDQwIDE4di0yem0wIDRMNjAgMGgyTDQwIDIydi0yem0wIDRMNjQgMGgyTDQwIDI2di0yem0wIDRMNjggMGgyTDQwIDMwdi0yem0wIDRMNzIgMGgyTDQwIDM0di0yem0wIDRMNzYgMGgyTDQwIDM4di0yem0wIDRMODAgMHYyTDQyIDQwaC0yem00IDBMODAgNHYyTDQ2IDQwaC0yem00IDBMODAgOHYyTDUwIDQwaC0yem00IDBsMjgtMjh2Mkw1NCA0MGgtMnptNCAwbDI0LTI0djJMNTggNDBoLTJ6bTQgMGwyMC0yMHYyTDYyIDQwaC0yem00IDBsMTYtMTZ2Mkw2NiA0MGgtMnptNCAwbDEyLTEydjJMNzAgNDBoLTJ6bTQgMGw4LTh2MmwtNiA2aC0yem00IDBsNC00djJsLTIgMmgtMnonLyUzRSUzQy9nJTNFJTNDL3N2ZyUzRVwiKTtcclxuICB9XHJcbiAgXHJcbiAgLndpZ2dsZSB7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICNkYmJlZjk7XHJcbiAgICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybChcImRhdGE6aW1hZ2Uvc3ZnK3htbCwlM0Nzdmcgd2lkdGg9JzUyJyBoZWlnaHQ9JzI2JyB2aWV3Qm94PScwIDAgNTIgMjYnIHhtbG5zPSdodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyclM0UlM0NnIGZpbGw9J25vbmUnIGZpbGwtcnVsZT0nZXZlbm9kZCclM0UlM0NnIGZpbGw9JyUyM2ZmN2M3YycgZmlsbC1vcGFjaXR5PScxJyUzRSUzQ3BhdGggZD0nTTEwIDEwYzAtMi4yMS0xLjc5LTQtNC00LTMuMzE0IDAtNi0yLjY4Ni02LTZoMmMwIDIuMjEgMS43OSA0IDQgNCAzLjMxNCAwIDYgMi42ODYgNiA2IDAgMi4yMSAxLjc5IDQgNCA0IDMuMzE0IDAgNiAyLjY4NiA2IDYgMCAyLjIxIDEuNzkgNCA0IDR2MmMtMy4zMTQgMC02LTIuNjg2LTYtNiAwLTIuMjEtMS43OS00LTQtNC0zLjMxNCAwLTYtMi42ODYtNi02em0yNS40NjQtMS45NWw4LjQ4NiA4LjQ4Ni0xLjQxNCAxLjQxNC04LjQ4Ni04LjQ4NiAxLjQxNC0xLjQxNHonIC8lM0UlM0MvZyUzRSUzQy9nJTNFJTNDL3N2ZyUzRVwiKTtcclxuICB9XHJcbiAgXHJcbiAgLmJhcnMge1xyXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZlNjdjO1xyXG4gICAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJkYXRhOmltYWdlL3N2Zyt4bWwsJTNDc3ZnIHdpZHRoPSc0MCcgaGVpZ2h0PSc0MCcgdmlld0JveD0nMCAwIDQwIDQwJyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnJTNFJTNDZyBmaWxsPSclMjM4ZmUxZTcnIGZpbGwtb3BhY2l0eT0nMScgZmlsbC1ydWxlPSdldmVub2RkJyUzRSUzQ3BhdGggZD0nTTAgNDBMNDAgMEgyMEwwIDIwTTQwIDQwVjIwTDIwIDQwJy8lM0UlM0MvZyUzRSUzQy9zdmclM0VcIik7XHJcbiAgfVxyXG4gIFxyXG4gIC5idWJibGVzIHtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2JlZmZjMjtcclxuICAgICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiZGF0YTppbWFnZS9zdmcreG1sLCUzQ3N2ZyB3aWR0aD0nMTAwJyBoZWlnaHQ9JzEwMCcgdmlld0JveD0nMCAwIDEwMCAxMDAnIHhtbG5zPSdodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyclM0UlM0NwYXRoIGQ9J00xMSAxOGMzLjg2NiAwIDctMy4xMzQgNy03cy0zLjEzNC03LTctNy03IDMuMTM0LTcgNyAzLjEzNCA3IDcgN3ptNDggMjVjMy44NjYgMCA3LTMuMTM0IDctN3MtMy4xMzQtNy03LTctNyAzLjEzNC03IDcgMy4xMzQgNyA3IDd6bS00My03YzEuNjU3IDAgMy0xLjM0MyAzLTNzLTEuMzQzLTMtMy0zLTMgMS4zNDMtMyAzIDEuMzQzIDMgMyAzem02MyAzMWMxLjY1NyAwIDMtMS4zNDMgMy0zcy0xLjM0My0zLTMtMy0zIDEuMzQzLTMgMyAxLjM0MyAzIDMgM3pNMzQgOTBjMS42NTcgMCAzLTEuMzQzIDMtM3MtMS4zNDMtMy0zLTMtMyAxLjM0My0zIDMgMS4zNDMgMyAzIDN6bTU2LTc2YzEuNjU3IDAgMy0xLjM0MyAzLTNzLTEuMzQzLTMtMy0zLTMgMS4zNDMtMyAzIDEuMzQzIDMgMyAzek0xMiA4NmMyLjIxIDAgNC0xLjc5IDQtNHMtMS43OS00LTQtNC00IDEuNzktNCA0IDEuNzkgNCA0IDR6bTI4LTY1YzIuMjEgMCA0LTEuNzkgNC00cy0xLjc5LTQtNC00LTQgMS43OS00IDQgMS43OSA0IDQgNHptMjMtMTFjMi43NiAwIDUtMi4yNCA1LTVzLTIuMjQtNS01LTUtNSAyLjI0LTUgNSAyLjI0IDUgNSA1em0tNiA2MGMyLjIxIDAgNC0xLjc5IDQtNHMtMS43OS00LTQtNC00IDEuNzktNCA0IDEuNzkgNCA0IDR6bTI5IDIyYzIuNzYgMCA1LTIuMjQgNS01cy0yLjI0LTUtNS01LTUgMi4yNC01IDUgMi4yNCA1IDUgNXpNMzIgNjNjMi43NiAwIDUtMi4yNCA1LTVzLTIuMjQtNS01LTUtNSAyLjI0LTUgNSAyLjI0IDUgNSA1em01Ny0xM2MyLjc2IDAgNS0yLjI0IDUtNXMtMi4yNC01LTUtNS01IDIuMjQtNSA1IDIuMjQgNSA1IDV6bS05LTIxYzEuMTA1IDAgMi0uODk1IDItMnMtLjg5NS0yLTItMi0yIC44OTUtMiAyIC44OTUgMiAyIDJ6TTYwIDkxYzEuMTA1IDAgMi0uODk1IDItMnMtLjg5NS0yLTItMi0yIC44OTUtMiAyIC44OTUgMiAyIDJ6TTM1IDQxYzEuMTA1IDAgMi0uODk1IDItMnMtLjg5NS0yLTItMi0yIC44OTUtMiAyIC44OTUgMiAyIDJ6TTEyIDYwYzEuMTA1IDAgMi0uODk1IDItMnMtLjg5NS0yLTItMi0yIC44OTUtMiAyIC44OTUgMiAyIDJ6JyBmaWxsPSclMjNlNmFmZmYnIGZpbGwtb3BhY2l0eT0nMScgZmlsbC1ydWxlPSdldmVub2RkJy8lM0UlM0Mvc3ZnJTNFXCIpO1xyXG4gIH1cclxuICBcclxuICAudGljVGFjIHtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZWZhYTtcclxuICAgICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiZGF0YTppbWFnZS9zdmcreG1sLCUzQ3N2ZyB3aWR0aD0nNzYnIGhlaWdodD0nNzYnIHZpZXdCb3g9JzAgMCA2NCA2NCcgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJyUzRSUzQ3BhdGggZD0nTTggMTZjNC40MTggMCA4LTMuNTgyIDgtOHMtMy41ODItOC04LTgtOCAzLjU4Mi04IDggMy41ODIgOCA4IDh6bTAtMmMzLjMxNCAwIDYtMi42ODYgNi02cy0yLjY4Ni02LTYtNi02IDIuNjg2LTYgNiAyLjY4NiA2IDYgNnptMzMuNDE0LTZsNS45NS01Ljk1TDQ1Ljk1LjYzNiA0MCA2LjU4NiAzNC4wNS42MzYgMzIuNjM2IDIuMDUgMzguNTg2IDhsLTUuOTUgNS45NSAxLjQxNCAxLjQxNEw0MCA5LjQxNGw1Ljk1IDUuOTUgMS40MTQtMS40MTRMNDEuNDE0IDh6TTQwIDQ4YzQuNDE4IDAgOC0zLjU4MiA4LThzLTMuNTgyLTgtOC04LTggMy41ODItOCA4IDMuNTgyIDggOCA4em0wLTJjMy4zMTQgMCA2LTIuNjg2IDYtNnMtMi42ODYtNi02LTYtNiAyLjY4Ni02IDYgMi42ODYgNiA2IDZ6TTkuNDE0IDQwbDUuOTUtNS45NS0xLjQxNC0xLjQxNEw4IDM4LjU4NmwtNS45NS01Ljk1TC42MzYgMzQuMDUgNi41ODYgNDBsLTUuOTUgNS45NSAxLjQxNCAxLjQxNEw4IDQxLjQxNGw1Ljk1IDUuOTUgMS40MTQtMS40MTRMOS40MTQgNDB6JyBmaWxsPSclMjNmZmFkYWQnIGZpbGwtb3BhY2l0eT0nMC44NCcgZmlsbC1ydWxlPSdldmVub2RkJy8lM0UlM0Mvc3ZnJTNFXCIpO1xyXG4gIH1cclxuICBcclxuICAuemlnWmFnIHtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzAwZGFjMztcclxuICAgICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiZGF0YTppbWFnZS9zdmcreG1sLCUzQ3N2ZyB3aWR0aD0nNDAnIGhlaWdodD0nMTInIHZpZXdCb3g9JzAgMCA0MCAxMicgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJyUzRSUzQ3BhdGggZD0nTTAgNi4xNzJMNi4xNzIgMGg1LjY1NkwwIDExLjgyOFY2LjE3MnptNDAgNS42NTZMMjguMTcyIDBoNS42NTZMNDAgNi4xNzJ2NS42NTZ6TTYuMTcyIDEybDEyLTEyaDMuNjU2bDEyIDEyaC01LjY1NkwyMCAzLjgyOCAxMS44MjggMTJINi4xNzJ6bTEyIDBMMjAgMTAuMTcyIDIxLjgyOCAxMmgtMy42NTZ6JyBmaWxsPSclMjMwMDgzODYnIGZpbGwtb3BhY2l0eT0nMC43JyBmaWxsLXJ1bGU9J2V2ZW5vZGQnLyUzRSUzQy9zdmclM0VcIik7XHJcbiAgfVxyXG4gIFxyXG4gIC5zdHJpcGVzIHtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcclxuICAgICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiZGF0YTppbWFnZS9zdmcreG1sLCUzQ3N2ZyB3aWR0aD0nMjUnIGhlaWdodD0nMScgdmlld0JveD0nMCAwIDQwIDEnIHhtbG5zPSdodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyclM0UlM0NwYXRoIGQ9J00wIDBoMjB2MUgweicgZmlsbD0nJTIzZDA5YWYzJyBmaWxsLW9wYWNpdHk9JzAuNTQnIGZpbGwtcnVsZT0nZXZlbm9kZCcvJTNFJTNDL3N2ZyUzRVwiKTtcclxuICB9XHJcbiAgXHJcbiAgLmNsb3VkcyB7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICM5NTliYjU7XHJcbiAgICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybChcImRhdGE6aW1hZ2Uvc3ZnK3htbCwlM0NzdmcgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJyB2aWV3Qm94PScwIDAgNTYgMjgnIHdpZHRoPSc1NicgaGVpZ2h0PScyOCclM0UlM0NwYXRoIGZpbGw9JyUyM2YwZDUxOScgZmlsbC1vcGFjaXR5PScwLjg5JyBkPSdNNTYgMjZ2MmgtNy43NWMyLjMtMS4yNyA0Ljk0LTIgNy43NS0yem0tMjYgMmEyIDIgMCAxIDAtNCAwaC00LjA5QTI1Ljk4IDI1Ljk4IDAgMCAwIDAgMTZ2LTJjLjY3IDAgMS4zNC4wMiAyIC4wN1YxNGEyIDIgMCAwIDAtMi0ydi0yYTQgNCAwIDAgMSAzLjk4IDMuNiAyOC4wOSAyOC4wOSAwIDAgMSAyLjgtMy44NkE4IDggMCAwIDAgMCA2VjRhOS45OSA5Ljk5IDAgMCAxIDguMTcgNC4yM2MuOTQtLjk1IDEuOTYtMS44MyAzLjAzLTIuNjNBMTMuOTggMTMuOTggMCAwIDAgMCAwaDcuNzVjMiAxLjEgMy43MyAyLjYzIDUuMSA0LjQ1IDEuMTItLjcyIDIuMy0xLjM3IDMuNTMtMS45M0EyMC4xIDIwLjEgMCAwIDAgMTQuMjggMGgyLjdjLjQ1LjU2Ljg4IDEuMTQgMS4yOSAxLjc0IDEuMy0uNDggMi42My0uODcgNC0xLjE1LS4xMS0uMi0uMjMtLjQtLjM2LS41OUgyNnYuMDdhMjguNCAyOC40IDAgMCAxIDQgMFYwaDQuMDlsLS4zNy41OWMxLjM4LjI4IDIuNzIuNjcgNC4wMSAxLjE1LjQtLjYuODQtMS4xOCAxLjMtMS43NGgyLjY5YTIwLjEgMjAuMSAwIDAgMC0yLjEgMi41MmMxLjIzLjU2IDIuNDEgMS4yIDMuNTQgMS45M0ExNi4wOCAxNi4wOCAwIDAgMSA0OC4yNSAwSDU2Yy00LjU4IDAtOC42NSAyLjItMTEuMiA1LjYgMS4wNy44IDIuMDkgMS42OCAzLjAzIDIuNjNBOS45OSA5Ljk5IDAgMCAxIDU2IDR2MmE4IDggMCAwIDAtNi43NyAzLjc0YzEuMDMgMS4yIDEuOTcgMi41IDIuNzkgMy44NkE0IDQgMCAwIDEgNTYgMTB2MmEyIDIgMCAwIDAtMiAyLjA3IDI4LjQgMjguNCAwIDAgMSAyLS4wN3YyYy05LjIgMC0xNy4zIDQuNzgtMjEuOTEgMTJIMzB6TTcuNzUgMjhIMHYtMmMyLjgxIDAgNS40Ni43MyA3Ljc1IDJ6TTU2IDIwdjJjLTUuNiAwLTEwLjY1IDIuMy0xNC4yOCA2aC0yLjdjNC4wNC00Ljg5IDEwLjE1LTggMTYuOTgtOHptLTM5LjAzIDhoLTIuNjlDMTAuNjUgMjQuMyA1LjYgMjIgMCAyMnYtMmM2LjgzIDAgMTIuOTQgMy4xMSAxNi45NyA4em0xNS4wMS0uNGEyOC4wOSAyOC4wOSAwIDAgMSAyLjgtMy44NiA4IDggMCAwIDAtMTMuNTUgMGMxLjAzIDEuMiAxLjk3IDIuNSAyLjc5IDMuODZhNCA0IDAgMCAxIDcuOTYgMHptMTQuMjktMTEuODZjMS4zLS40OCAyLjYzLS44NyA0LTEuMTVhMjUuOTkgMjUuOTkgMCAwIDAtNDQuNTUgMGMxLjM4LjI4IDIuNzIuNjcgNC4wMSAxLjE1YTIxLjk4IDIxLjk4IDAgMCAxIDM2LjU0IDB6bS01LjQzIDIuNzFjMS4xMy0uNzIgMi4zLTEuMzcgMy41NC0xLjkzYTE5Ljk4IDE5Ljk4IDAgMCAwLTMyLjc2IDBjMS4yMy41NiAyLjQxIDEuMiAzLjU0IDEuOTNhMTUuOTggMTUuOTggMCAwIDEgMjUuNjggMHptLTQuNjcgMy43OGMuOTQtLjk1IDEuOTYtMS44MyAzLjAzLTIuNjNhMTMuOTggMTMuOTggMCAwIDAtMjIuNCAwYzEuMDcuOCAyLjA5IDEuNjggMy4wMyAyLjYzYTkuOTkgOS45OSAwIDAgMSAxNi4zNCAweiclM0UlM0MvcGF0aCUzRSUzQy9zdmclM0VcIik7XHJcbiAgfVxyXG4gIFxyXG4gIC5henRlYyB7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICNkNTkyNDI7XHJcbiAgICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybChcImRhdGE6aW1hZ2Uvc3ZnK3htbCwlM0Nzdmcgd2lkdGg9JzEwMCcgaGVpZ2h0PScyMCcgdmlld0JveD0nMCAwIDEwMCAyMCcgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJyUzRSUzQ3BhdGggZD0nTTIxLjE4NCAyMGMuMzU3LS4xMy43Mi0uMjY0IDEuMDg4LS40MDJsMS43NjgtLjY2MUMzMy42NCAxNS4zNDcgMzkuNjQ3IDE0IDUwIDE0YzEwLjI3MSAwIDE1LjM2MiAxLjIyMiAyNC42MjkgNC45MjguOTU1LjM4MyAxLjg2OS43NCAyLjc1IDEuMDcyaDYuMjI1Yy0yLjUxLS43My01LjEzOS0xLjY5MS04LjIzMy0yLjkyOEM2NS44ODggMTMuMjc4IDYwLjU2MiAxMiA1MCAxMmMtMTAuNjI2IDAtMTYuODU1IDEuMzk3LTI2LjY2IDUuMDYzbC0xLjc2Ny42NjJjLTIuNDc1LjkyMy00LjY2IDEuNjc0LTYuNzI0IDIuMjc1aDYuMzM1em0wLTIwQzEzLjI1OCAyLjg5MiA4LjA3NyA0IDAgNFYyYzUuNzQ0IDAgOS45NTEtLjU3NCAxNC44NS0yaDYuMzM0ek03Ny4zOCAwQzg1LjIzOSAyLjk2NiA5MC41MDIgNCAxMDAgNFYyYy02Ljg0MiAwLTExLjM4Ni0uNTQyLTE2LjM5Ni0yaC02LjIyNXpNMCAxNGM4LjQ0IDAgMTMuNzE4LTEuMjEgMjIuMjcyLTQuNDAybDEuNzY4LS42NjFDMzMuNjQgNS4zNDcgMzkuNjQ3IDQgNTAgNGMxMC4yNzEgMCAxNS4zNjIgMS4yMjIgMjQuNjI5IDQuOTI4Qzg0LjExMiAxMi43MjIgODkuNDM4IDE0IDEwMCAxNHYtMmMtMTAuMjcxIDAtMTUuMzYyLTEuMjIyLTI0LjYyOS00LjkyOEM2NS44ODggMy4yNzggNjAuNTYyIDIgNTAgMiAzOS4zNzQgMiAzMy4xNDUgMy4zOTcgMjMuMzQgNy4wNjNsLTEuNzY3LjY2MkMxMy4yMjMgMTAuODQgOC4xNjMgMTIgMCAxMnYyeicgZmlsbD0nJTIzMGQzN2MyJyBmaWxsLW9wYWNpdHk9JzAuNCcgZmlsbC1ydWxlPSdldmVub2RkJy8lM0UlM0Mvc3ZnJTNFXCIpO1xyXG4gIH1cclxuICBcclxuICAuY2lyY3VpdCB7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICMwMGI0OTc7XHJcbiAgICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybChcImRhdGE6aW1hZ2Uvc3ZnK3htbCwlM0NzdmcgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJyB2aWV3Qm94PScwIDAgMzA0IDMwNCcgd2lkdGg9JzE1MCcgaGVpZ2h0PScxNTAnJTNFJTNDcGF0aCBmaWxsPSclMjMzMzMzMzMnIGZpbGwtb3BhY2l0eT0nMC40JyBkPSdNNDQuMSAyMjRhNSA1IDAgMSAxIDAgMkgwdi0yaDQ0LjF6bTE2MCA0OGE1IDUgMCAxIDEgMCAySDgydi0yaDEyMi4xem01Ny44LTQ2YTUgNSAwIDEgMSAwLTJIMzA0djJoLTQyLjF6bTAgMTZhNSA1IDAgMSAxIDAtMkgzMDR2MmgtNDIuMXptNi4yLTExNGE1IDUgMCAxIDEgMCAyaC04Ni4yYTUgNSAwIDEgMSAwLTJoODYuMnptLTI1Ni00OGE1IDUgMCAxIDEgMCAySDB2LTJoMTIuMXptMTg1LjggMzRhNSA1IDAgMSAxIDAtMmg4Ni4yYTUgNSAwIDEgMSAwIDJoLTg2LjJ6TTI1OCAxMi4xYTUgNSAwIDEgMS0yIDBWMGgydjEyLjF6bS02NCAyMDhhNSA1IDAgMSAxLTIgMHYtNTQuMmE1IDUgMCAxIDEgMiAwdjU0LjJ6bTQ4LTE5OC4yVjgwaDYydjJoLTY0VjIxLjlhNSA1IDAgMSAxIDIgMHptMTYgMTZWNjRoNDZ2MmgtNDhWMzcuOWE1IDUgMCAxIDEgMiAwem0tMTI4IDk2VjIwOGgxNnYxMi4xYTUgNSAwIDEgMS0yIDBWMjEwaC0xNnYtNzYuMWE1IDUgMCAxIDEgMiAwem0tNS45LTIxLjlhNSA1IDAgMSAxIDAgMkgxMTR2NDhIODUuOWE1IDUgMCAxIDEgMC0ySDExMnYtNDhoMTIuMXptLTYuMiAxMzBhNSA1IDAgMSAxIDAtMkgxNzZ2LTc0LjFhNSA1IDAgMSAxIDIgMFYyNDJoLTYwLjF6bS0xNi02NGE1IDUgMCAxIDEgMC0ySDExNHY0OGgxMC4xYTUgNSAwIDEgMSAwIDJIMTEydi00OGgtMTAuMXpNNjYgMjg0LjFhNSA1IDAgMSAxLTIgMFYyNzRINTB2MzBoLTJ2LTMyaDE4djEyLjF6TTIzNi4xIDE3NmE1IDUgMCAxIDEgMCAySDIyNnY5NGg0OHYzMmgtMnYtMzBoLTQ4di05OGgxMi4xem0yNS44LTMwYTUgNSAwIDEgMSAwLTJIMjc0djQ0LjFhNSA1IDAgMSAxLTIgMFYxNDZoLTEwLjF6bS02NCA5NmE1IDUgMCAxIDEgMC0ySDIwOHYtODBoMTZ2LTE0aC00Mi4xYTUgNSAwIDEgMSAwLTJIMjI2djE4aC0xNnY4MGgtMTIuMXptODYuMi0yMTBhNSA1IDAgMSAxIDAgMkgyNzJWMGgydjMyaDEwLjF6TTk4IDEwMS45VjE0Nkg1My45YTUgNSAwIDEgMSAwLTJIOTZ2LTQyLjFhNSA1IDAgMSAxIDIgMHpNNTMuOSAzNGE1IDUgMCAxIDEgMC0ySDgwVjBoMnYzNEg1My45em02MC4xIDMuOVY2Nkg4MnY2NEg2OS45YTUgNSAwIDEgMSAwLTJIODBWNjRoMzJWMzcuOWE1IDUgMCAxIDEgMiAwek0xMDEuOSA4MmE1IDUgMCAxIDEgMC0ySDEyOFYzNy45YTUgNSAwIDEgMSAyIDBWODJoLTI4LjF6bTE2LTY0YTUgNSAwIDEgMSAwLTJIMTQ2djQ0LjFhNSA1IDAgMSAxLTIgMFYxOGgtMjYuMXptMTAyLjIgMjcwYTUgNSAwIDEgMSAwIDJIOTh2MTRoLTJ2LTE2aDEyNC4xek0yNDIgMTQ5LjlWMTYwaDE2djM0aC0xNnY2Mmg0OHY0OGgtMnYtNDZoLTQ4di02NmgxNnYtMzBoLTE2di0xMi4xYTUgNSAwIDEgMSAyIDB6TTUzLjkgMThhNSA1IDAgMSAxIDAtMkg2NFYySDQ4VjBoMTh2MThINTMuOXptMTEyIDMyYTUgNSAwIDEgMSAwLTJIMTkyVjBoNTB2MmgtNDh2NDhoLTI4LjF6bS00OC00OGE1IDUgMCAwIDEtOS44LTJoMi4wN2EzIDMgMCAxIDAgNS42NiAwSDE3OHYzNGgtMThWMjEuOWE1IDUgMCAxIDEgMiAwVjMyaDE0VjJoLTU4LjF6bTAgOTZhNSA1IDAgMSAxIDAtMkgxMzdsMzItMzJoMzlWMjEuOWE1IDUgMCAxIDEgMiAwVjY2aC00MC4xN2wtMzIgMzJIMTE3Ljl6bTI4LjEgOTAuMWE1IDUgMCAxIDEtMiAwdi03Ni41MUwxNzUuNTkgODBIMjI0VjIxLjlhNSA1IDAgMSAxIDIgMFY4MmgtNDkuNTlMMTQ2IDExMi40MXY3NS42OXptMTYgMzJhNSA1IDAgMSAxLTIgMHYtOTkuNTFMMTg0LjU5IDk2SDMwMC4xYTUgNSAwIDAgMSAzLjktMy45djIuMDdhMyAzIDAgMCAwIDAgNS42NnYyLjA3YTUgNSAwIDAgMS0zLjktMy45SDE4NS40MUwxNjIgMTIxLjQxdjk4LjY5em0tMTQ0LTY0YTUgNSAwIDEgMS0yIDB2LTMuNTFsNDgtNDhWNDhoMzJWMGgydjUwSDY2djU1LjQxbC00OCA0OHYyLjY5ek01MCA1My45djQzLjUxbC00OCA0OFYyMDhoMjYuMWE1IDUgMCAxIDEgMCAySDB2LTY1LjQxbDQ4LTQ4VjUzLjlhNSA1IDAgMSAxIDIgMHptLTE2IDE2Vjg5LjQxbC0zNCAzNHYtMi44MmwzMi0zMlY2OS45YTUgNSAwIDEgMSAyIDB6TTEyLjEgMzJhNSA1IDAgMSAxIDAgMkg5LjQxTDAgNDMuNDFWNDAuNkw4LjU5IDMyaDMuNTF6bTI2NS44IDE4YTUgNSAwIDEgMSAwLTJoMTguNjlsNy40MS03LjQxdjIuODJMMjk3LjQxIDUwSDI3Ny45em0tMTYgMTYwYTUgNSAwIDEgMSAwLTJIMjg4di03MS40MWwxNi0xNnYyLjgybC0xNCAxNFYyMTBoLTI4LjF6bS0yMDggMzJhNSA1IDAgMSAxIDAtMkg2NHYtMjIuNTlMNDAuNTkgMTk0SDIxLjlhNSA1IDAgMSAxIDAtMkg0MS40MUw2NiAyMTYuNTlWMjQySDUzLjl6bTE1MC4yIDE0YTUgNSAwIDEgMSAwIDJIOTZ2LTU2LjZMNTYuNiAxNjJIMzcuOWE1IDUgMCAxIDEgMC0yaDE5LjVMOTggMjAwLjZWMjU2aDEwNi4xem0tMTUwLjIgMmE1IDUgMCAxIDEgMC0ySDgwdi00Ni41OUw0OC41OSAxNzhIMjEuOWE1IDUgMCAxIDEgMC0ySDQ5LjQxTDgyIDIwOC41OVYyNThINTMuOXpNMzQgMzkuOHYxLjYxTDkuNDEgNjZIMHYtMmg4LjU5TDMyIDQwLjU5VjBoMnYzOS44ek0yIDMwMC4xYTUgNSAwIDAgMSAzLjkgMy45SDMuODNBMyAzIDAgMCAwIDAgMzAyLjE3VjI1NmgxOHY0OGgtMnYtNDZIMnY0Mi4xek0zNCAyNDF2NjNoLTJ2LTYySDB2LTJoMzR2MXpNMTcgMThIMHYtMmgxNlYwaDJ2MThoLTF6bTI3My0yaDE0djJoLTE2VjBoMnYxNnptLTMyIDI3M3YxNWgtMnYtMTRoLTE0djE0aC0ydi0xNmgxOHYxek0wIDkyLjFBNS4wMiA1LjAyIDAgMCAxIDYgOTdhNSA1IDAgMCAxLTYgNC45di0yLjA3YTMgMyAwIDEgMCAwLTUuNjZWOTIuMXpNODAgMjcyaDJ2MzJoLTJ2LTMyem0zNy45IDMyaC0yLjA3YTMgMyAwIDAgMC01LjY2IDBoLTIuMDdhNSA1IDAgMCAxIDkuOCAwek01LjkgMEE1LjAyIDUuMDIgMCAwIDEgMCA1LjlWMy44M0EzIDMgMCAwIDAgMy44MyAwSDUuOXptMjk0LjIgMGgyLjA3QTMgMyAwIDAgMCAzMDQgMy44M1Y1LjlhNSA1IDAgMCAxLTMuOS01Ljl6bTMuOSAzMDAuMXYyLjA3YTMgMyAwIDAgMC0xLjgzIDEuODNoLTIuMDdhNSA1IDAgMCAxIDMuOS0zLjl6TTk3IDEwMGEzIDMgMCAxIDAgMC02IDMgMyAwIDAgMCAwIDZ6bTAtMTZhMyAzIDAgMSAwIDAtNiAzIDMgMCAwIDAgMCA2em0xNiAxNmEzIDMgMCAxIDAgMC02IDMgMyAwIDAgMCAwIDZ6bTE2IDE2YTMgMyAwIDEgMCAwLTYgMyAzIDAgMCAwIDAgNnptMCAxNmEzIDMgMCAxIDAgMC02IDMgMyAwIDAgMCAwIDZ6bS00OCAzMmEzIDMgMCAxIDAgMC02IDMgMyAwIDAgMCAwIDZ6bTE2IDE2YTMgMyAwIDEgMCAwLTYgMyAzIDAgMCAwIDAgNnptMzIgNDhhMyAzIDAgMSAwIDAtNiAzIDMgMCAwIDAgMCA2em0tMTYgMTZhMyAzIDAgMSAwIDAtNiAzIDMgMCAwIDAgMCA2em0zMi0xNmEzIDMgMCAxIDAgMC02IDMgMyAwIDAgMCAwIDZ6bTAtMzJhMyAzIDAgMSAwIDAtNiAzIDMgMCAwIDAgMCA2em0xNiAzMmEzIDMgMCAxIDAgMC02IDMgMyAwIDAgMCAwIDZ6bTMyIDE2YTMgMyAwIDEgMCAwLTYgMyAzIDAgMCAwIDAgNnptMC0xNmEzIDMgMCAxIDAgMC02IDMgMyAwIDAgMCAwIDZ6bS0xNi02NGEzIDMgMCAxIDAgMC02IDMgMyAwIDAgMCAwIDZ6bTE2IDBhMyAzIDAgMSAwIDAtNiAzIDMgMCAwIDAgMCA2em0xNiA5NmEzIDMgMCAxIDAgMC02IDMgMyAwIDAgMCAwIDZ6bTAgMTZhMyAzIDAgMSAwIDAtNiAzIDMgMCAwIDAgMCA2em0xNiAxNmEzIDMgMCAxIDAgMC02IDMgMyAwIDAgMCAwIDZ6bTE2LTE0NGEzIDMgMCAxIDAgMC02IDMgMyAwIDAgMCAwIDZ6bTAgMzJhMyAzIDAgMSAwIDAtNiAzIDMgMCAwIDAgMCA2em0xNi0zMmEzIDMgMCAxIDAgMC02IDMgMyAwIDAgMCAwIDZ6bTE2LTE2YTMgMyAwIDEgMCAwLTYgMyAzIDAgMCAwIDAgNnptLTk2IDBhMyAzIDAgMSAwIDAtNiAzIDMgMCAwIDAgMCA2em0wIDE2YTMgMyAwIDEgMCAwLTYgMyAzIDAgMCAwIDAgNnptMTYtMzJhMyAzIDAgMSAwIDAtNiAzIDMgMCAwIDAgMCA2em05NiAwYTMgMyAwIDEgMCAwLTYgMyAzIDAgMCAwIDAgNnptLTE2LTY0YTMgMyAwIDEgMCAwLTYgMyAzIDAgMCAwIDAgNnptMTYtMTZhMyAzIDAgMSAwIDAtNiAzIDMgMCAwIDAgMCA2em0tMzIgMGEzIDMgMCAxIDAgMC02IDMgMyAwIDAgMCAwIDZ6bTAtMTZhMyAzIDAgMSAwIDAtNiAzIDMgMCAwIDAgMCA2em0tMTYgMGEzIDMgMCAxIDAgMC02IDMgMyAwIDAgMCAwIDZ6bS0xNiAwYTMgMyAwIDEgMCAwLTYgMyAzIDAgMCAwIDAgNnptLTE2IDBhMyAzIDAgMSAwIDAtNiAzIDMgMCAwIDAgMCA2ek00OSAzNmEzIDMgMCAxIDAgMC02IDMgMyAwIDAgMCAwIDZ6bS0zMiAwYTMgMyAwIDEgMCAwLTYgMyAzIDAgMCAwIDAgNnptMzIgMTZhMyAzIDAgMSAwIDAtNiAzIDMgMCAwIDAgMCA2ek0zMyA2OGEzIDMgMCAxIDAgMC02IDMgMyAwIDAgMCAwIDZ6bTE2LTQ4YTMgMyAwIDEgMCAwLTYgMyAzIDAgMCAwIDAgNnptMCAyNDBhMyAzIDAgMSAwIDAtNiAzIDMgMCAwIDAgMCA2em0xNiAzMmEzIDMgMCAxIDAgMC02IDMgMyAwIDAgMCAwIDZ6bS0xNi02NGEzIDMgMCAxIDAgMC02IDMgMyAwIDAgMCAwIDZ6bTAgMTZhMyAzIDAgMSAwIDAtNiAzIDMgMCAwIDAgMCA2em0tMTYtMzJhMyAzIDAgMSAwIDAtNiAzIDMgMCAwIDAgMCA2em04MC0xNzZhMyAzIDAgMSAwIDAtNiAzIDMgMCAwIDAgMCA2em0xNiAwYTMgMyAwIDEgMCAwLTYgMyAzIDAgMCAwIDAgNnptLTE2LTE2YTMgMyAwIDEgMCAwLTYgMyAzIDAgMCAwIDAgNnptMzIgNDhhMyAzIDAgMSAwIDAtNiAzIDMgMCAwIDAgMCA2em0xNi0xNmEzIDMgMCAxIDAgMC02IDMgMyAwIDAgMCAwIDZ6bTAtMzJhMyAzIDAgMSAwIDAtNiAzIDMgMCAwIDAgMCA2em0xMTIgMTc2YTMgMyAwIDEgMCAwLTYgMyAzIDAgMCAwIDAgNnptLTE2IDE2YTMgMyAwIDEgMCAwLTYgMyAzIDAgMCAwIDAgNnptMCAxNmEzIDMgMCAxIDAgMC02IDMgMyAwIDAgMCAwIDZ6bTAgMTZhMyAzIDAgMSAwIDAtNiAzIDMgMCAwIDAgMCA2ek0xNyAxODBhMyAzIDAgMSAwIDAtNiAzIDMgMCAwIDAgMCA2em0wIDE2YTMgMyAwIDEgMCAwLTYgMyAzIDAgMCAwIDAgNnptMC0zMmEzIDMgMCAxIDAgMC02IDMgMyAwIDAgMCAwIDZ6bTE2IDBhMyAzIDAgMSAwIDAtNiAzIDMgMCAwIDAgMCA2ek0xNyA4NGEzIDMgMCAxIDAgMC02IDMgMyAwIDAgMCAwIDZ6bTMyIDY0YTMgMyAwIDEgMCAwLTYgMyAzIDAgMCAwIDAgNnptMTYtMTZhMyAzIDAgMSAwIDAtNiAzIDMgMCAwIDAgMCA2eiclM0UlM0MvcGF0aCUzRSUzQy9zdmclM0VcIik7XHJcbiAgfVxyXG4gIFxyXG4gIC5kb3RzIHtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmY2IwNTtcclxuICAgICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiZGF0YTppbWFnZS9zdmcreG1sLCUzQ3N2ZyB3aWR0aD0nMTYnIGhlaWdodD0nMTYnIHZpZXdCb3g9JzAgMCAyMCAyMCcgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJyUzRSUzQ2cgZmlsbD0nJTIzMDBhOTlkJyBmaWxsLW9wYWNpdHk9JzAuNzEnIGZpbGwtcnVsZT0nZXZlbm9kZCclM0UlM0NjaXJjbGUgY3g9JzMnIGN5PSczJyByPSczJy8lM0UlM0NjaXJjbGUgY3g9JzEzJyBjeT0nMTMnIHI9JzMnLyUzRSUzQy9nJTNFJTNDL3N2ZyUzRVwiKTtcclxuICB9XHJcbiAgXHJcbiAgLmxpbmVzIHtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2VmZWZlZjtcclxuICAgICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiZGF0YTppbWFnZS9zdmcreG1sLCUzQ3N2ZyB3aWR0aD0nODAnIGhlaWdodD0nODAnIHZpZXdCb3g9JzAgMCAxMjAgMTIwJyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnJTNFJTNDcGF0aCBkPSdNOSAwaDJ2MjBIOVYwem0yNS4xMzQuODRsMS43MzIgMS0xMCAxNy4zMi0xLjczMi0xIDEwLTE3LjMyem0tMjAgMjBsMS43MzIgMS0xMCAxNy4zMi0xLjczMi0xIDEwLTE3LjMyek01OC4xNiA0LjEzNGwxIDEuNzMyLTE3LjMyIDEwLTEtMS43MzIgMTcuMzItMTB6bS00MCA0MGwxIDEuNzMyLTE3LjMyIDEwLTEtMS43MzIgMTcuMzItMTB6TTgwIDl2Mkg2MFY5aDIwek0yMCA2OXYySDB2LTJoMjB6bTc5LjMyLTU1bC0xIDEuNzMyLTE3LjMyLTEwTDgyIDRsMTcuMzIgMTB6bS04MCA4MGwtMSAxLjczMi0xNy4zMi0xMEwyIDg0bDE3LjMyIDEwem05Ni41NDYtNzUuODRsLTEuNzMyIDEtMTAtMTcuMzIgMS43MzItMSAxMCAxNy4zMnptLTEwMCAxMDBsLTEuNzMyIDEtMTAtMTcuMzIgMS43MzItMSAxMCAxNy4zMnpNMzguMTYgMjQuMTM0bDEgMS43MzItMTcuMzIgMTAtMS0xLjczMiAxNy4zMi0xMHpNNjAgMjl2Mkg0MHYtMmgyMHptMTkuMzIgNWwtMSAxLjczMi0xNy4zMi0xMEw2MiAyNGwxNy4zMiAxMHptMTYuNTQ2IDQuMTZsLTEuNzMyIDEtMTAtMTcuMzIgMS43MzItMSAxMCAxNy4zMnpNMTExIDQwaC0yVjIwaDJ2MjB6bTMuMTM0Ljg0bDEuNzMyIDEtMTAgMTcuMzItMS43MzItMSAxMC0xNy4zMnpNNDAgNDl2MkgyMHYtMmgyMHptMTkuMzIgNWwtMSAxLjczMi0xNy4zMi0xMEw0MiA0NGwxNy4zMiAxMHptMTYuNTQ2IDQuMTZsLTEuNzMyIDEtMTAtMTcuMzIgMS43MzItMSAxMCAxNy4zMnpNOTEgNjBoLTJWNDBoMnYyMHptMy4xMzQuODRsMS43MzIgMS0xMCAxNy4zMi0xLjczMi0xIDEwLTE3LjMyem0yNC4wMjYgMy4yOTRsMSAxLjczMi0xNy4zMiAxMC0xLTEuNzMyIDE3LjMyLTEwek0zOS4zMiA3NGwtMSAxLjczMi0xNy4zMi0xMEwyMiA2NGwxNy4zMiAxMHptMTYuNTQ2IDQuMTZsLTEuNzMyIDEtMTAtMTcuMzIgMS43MzItMSAxMCAxNy4zMnpNNzEgODBoLTJWNjBoMnYyMHptMy4xMzQuODRsMS43MzIgMS0xMCAxNy4zMi0xLjczMi0xIDEwLTE3LjMyem0yNC4wMjYgMy4yOTRsMSAxLjczMi0xNy4zMiAxMC0xLTEuNzMyIDE3LjMyLTEwek0xMjAgODl2MmgtMjB2LTJoMjB6bS04NC4xMzQgOS4xNmwtMS43MzIgMS0xMC0xNy4zMiAxLjczMi0xIDEwIDE3LjMyek01MSAxMDBoLTJWODBoMnYyMHptMy4xMzQuODRsMS43MzIgMS0xMCAxNy4zMi0xLjczMi0xIDEwLTE3LjMyem0yNC4wMjYgMy4yOTRsMSAxLjczMi0xNy4zMiAxMC0xLTEuNzMyIDE3LjMyLTEwek0xMDAgMTA5djJIODB2LTJoMjB6bTE5LjMyIDVsLTEgMS43MzItMTcuMzItMTAgMS0xLjczMiAxNy4zMiAxMHpNMzEgMTIwaC0ydi0yMGgydjIweicgZmlsbD0nJTIzZWZiNGEzJyBmaWxsLW9wYWNpdHk9JzAuODQnIGZpbGwtcnVsZT0nZXZlbm9kZCcvJTNFJTNDL3N2ZyUzRVwiKTtcclxuICB9XHJcbiAgXHJcbiAgLmxlYWYge1xyXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjYjVjY2JmO1xyXG4gICAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJkYXRhOmltYWdlL3N2Zyt4bWwsJTNDc3ZnIHhtbG5zPSdodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Zycgdmlld0JveD0nMCAwIDgwIDQwJyB3aWR0aD0nNjAnIGhlaWdodD0nMzAnJTNFJTNDcGF0aCBmaWxsPSclMjM0MDU4NGEnIGZpbGwtb3BhY2l0eT0nMC40NycgZD0nTTAgNDBhMTkuOTYgMTkuOTYgMCAwIDEgNS45LTE0LjExIDIwLjE3IDIwLjE3IDAgMCAxIDE5LjQ0LTUuMkEyMCAyMCAwIDAgMSAyMC4yIDQwSDB6TTY1LjMyLjc1QTIwLjAyIDIwLjAyIDAgMCAxIDQwLjggMjUuMjYgMjAuMDIgMjAuMDIgMCAwIDEgNjUuMzIuNzZ6TS4wNyAwaDIwLjFsLS4wOC4wN0EyMC4wMiAyMC4wMiAwIDAgMSAuNzUgNS4yNSAyMC4wOCAyMC4wOCAwIDAgMSAuMDcgMHptMS45NCA0MGgyLjUzbDQuMjYtNC4yNHYtOS43OEExNy45NiAxNy45NiAwIDAgMCAyIDQwem01LjM4IDBoOS44YTE3Ljk4IDE3Ljk4IDAgMCAwIDYuNjctMTYuNDJMNy40IDQwem0zLjQzLTE1LjQydjkuMTdsMTEuNjItMTEuNTljLTMuOTctLjUtOC4wOC4zLTExLjYyIDIuNDJ6bTMyLjg2LS43OEExOCAxOCAwIDAgMCA2My44NSAzLjYzTDQzLjY4IDIzLjh6bTcuMi0xOS4xN3Y5LjE1TDYyLjQzIDIuMjJjLTMuOTYtLjUtOC4wNS4zLTExLjU3IDIuNHptLTMuNDkgMi43MmMtNC4xIDQuMS01LjgxIDkuNjktNS4xMyAxNS4wM2w2LjYxLTYuNlY2LjAyYy0uNTEuNDEtMSAuODUtMS40OCAxLjMzek0xNy4xOCAwSDcuNDJMMy42NCAzLjc4QTE4IDE4IDAgMCAwIDE3LjE4IDB6TTIuMDggMGMtLjAxLjguMDQgMS41OC4xNCAyLjM3TDQuNTkgMEgyLjA3eiclM0UlM0MvcGF0aCUzRSUzQy9zdmclM0VcIik7XHJcbiAgfVxyXG4gIFxyXG4gIC5vdmVybGFwIHtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmYWRmZjtcclxuICAgICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiZGF0YTppbWFnZS9zdmcreG1sLCUzQ3N2ZyB3aWR0aD0nNjAnIGhlaWdodD0nNjAnIHZpZXdCb3g9JzAgMCA4MCA4MCcgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJyUzRSUzQ2cgZmlsbD0nbm9uZScgZmlsbC1ydWxlPSdldmVub2RkJyUzRSUzQ2cgZmlsbD0nJTIzMmU3OGZmJyBmaWxsLW9wYWNpdHk9JzAuODInJTNFJTNDcGF0aCBkPSdNNTAgNTBjMC01LjUyMyA0LjQ3Ny0xMCAxMC0xMHMxMCA0LjQ3NyAxMCAxMC00LjQ3NyAxMC0xMCAxMGMwIDUuNTIzLTQuNDc3IDEwLTEwIDEwcy0xMC00LjQ3Ny0xMC0xMCA0LjQ3Ny0xMCAxMC0xMHpNMTAgMTBjMC01LjUyMyA0LjQ3Ny0xMCAxMC0xMHMxMCA0LjQ3NyAxMCAxMC00LjQ3NyAxMC0xMCAxMGMwIDUuNTIzLTQuNDc3IDEwLTEwIDEwUzAgMjUuNTIzIDAgMjBzNC40NzctMTAgMTAtMTB6bTEwIDhjNC40MTggMCA4LTMuNTgyIDgtOHMtMy41ODItOC04LTgtOCAzLjU4Mi04IDggMy41ODIgOCA4IDh6bTQwIDQwYzQuNDE4IDAgOC0zLjU4MiA4LThzLTMuNTgyLTgtOC04LTggMy41ODItOCA4IDMuNTgyIDggOCA4eicgLyUzRSUzQy9nJTNFJTNDL2clM0UlM0Mvc3ZnJTNFXCIpO1xyXG4gIH1cclxuICBcclxuICBAbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA1MDBweCkge1xyXG4gICAgICAucHJvZ3Jlc3Mge1xyXG4gICAgICAgICAgd2lkdGg6IGNhbGMoMTAwdncgKiAwLjcpO1xyXG4gICAgICAgICAgLyogaGVpZ2h0OiBjYWxjKDEwMHZ3ICogMC43ICogMC4xNDUpOyAqL1xyXG4gICAgICAgICAgaGVpZ2h0OiA0MHB4O1xyXG4gICAgICAgICAgbWFyZ2luOiAyNXB4O1xyXG4gICAgICB9XHJcbiAgXHJcbiAgICAgIC5naXRodWIge1xyXG4gICAgICAgICAgbWFyZ2luOiAyMHB4O1xyXG4gICAgICB9XHJcbiAgXHJcbiAgICAgIC5kZXNrdG9wIHtcclxuICAgICAgICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgICAgIH1cclxuICBcclxuICAgICAgLm1vYmlsZSB7XHJcbiAgICAgICAgICBkaXNwbGF5OiBpbmhlcml0O1xyXG4gICAgICB9XHJcbiAgfVxyXG4gIFxyXG4gIFxyXG4gIC8qIEluZGV0ZXJtaW5hdGUgQ2xhc3NlcyBhbmQgQW5pbWF0aW9ucyAqL1xyXG4gIFxyXG4gIC5pbmRldGVybWluYXRlLTE2IHtcclxuICAgICAgYW5pbWF0aW9uOiBpbmRldGVybWluYXRlLTE2IDAuMjVzIGxpbmVhciBpbmZpbml0ZTtcclxuICB9XHJcbiAgXHJcbiAgLmluZGV0ZXJtaW5hdGUtMjAge1xyXG4gICAgICBhbmltYXRpb246IGluZGV0ZXJtaW5hdGUtMjAgMC41cyBsaW5lYXIgaW5maW5pdGU7XHJcbiAgfVxyXG4gIFxyXG4gIC5pbmRldGVybWluYXRlLTI1IHtcclxuICAgICAgYW5pbWF0aW9uOiBpbmRldGVybWluYXRlLTI1IDAuNXMgbGluZWFyIGluZmluaXRlO1xyXG4gIH1cclxuICBcclxuICAuaW5kZXRlcm1pbmF0ZS0yOCB7XHJcbiAgICAgIGFuaW1hdGlvbjogaW5kZXRlcm1pbmF0ZS0yOCAwLjI1cyBsaW5lYXIgaW5maW5pdGU7XHJcbiAgfVxyXG4gIFxyXG4gIC5pbmRldGVybWluYXRlLTQwIHtcclxuICAgICAgYW5pbWF0aW9uOiBpbmRldGVybWluYXRlLTQwIDAuNXMgbGluZWFyIGluZmluaXRlO1xyXG4gIH1cclxuICBcclxuICAuaW5kZXRlcm1pbmF0ZS01MiB7XHJcbiAgICAgIGFuaW1hdGlvbjogaW5kZXRlcm1pbmF0ZS01MiAwLjVzIGxpbmVhciBpbmZpbml0ZTtcclxuICB9XHJcbiAgXHJcbiAgLmluZGV0ZXJtaW5hdGUtNTYge1xyXG4gICAgICBhbmltYXRpb246IGluZGV0ZXJtaW5hdGUtNTYgMC41cyBsaW5lYXIgaW5maW5pdGU7XHJcbiAgfVxyXG4gIFxyXG4gIC5pbmRldGVybWluYXRlLTUwIHtcclxuICAgICAgYW5pbWF0aW9uOiBpbmRldGVybWluYXRlLTUwIDAuNXMgbGluZWFyIGluZmluaXRlO1xyXG4gIH1cclxuICBcclxuICAuaW5kZXRlcm1pbmF0ZS02MCB7XHJcbiAgICAgIGFuaW1hdGlvbjogaW5kZXRlcm1pbmF0ZS02MCAwLjVzIGxpbmVhciBpbmZpbml0ZTtcclxuICB9XHJcbiAgXHJcbiAgLmluZGV0ZXJtaW5hdGUtNzAge1xyXG4gICAgICBhbmltYXRpb246IGluZGV0ZXJtaW5hdGUtNzAgMC41cyBsaW5lYXIgaW5maW5pdGU7XHJcbiAgfVxyXG4gIFxyXG4gIC5pbmRldGVybWluYXRlLTc2IHtcclxuICAgICAgYW5pbWF0aW9uOiBpbmRldGVybWluYXRlLTc2IDAuNXMgbGluZWFyIGluZmluaXRlO1xyXG4gIH1cclxuICBcclxuICAuaW5kZXRlcm1pbmF0ZS04MCB7XHJcbiAgICAgIGFuaW1hdGlvbjogaW5kZXRlcm1pbmF0ZS04MCAwLjVzIGxpbmVhciBpbmZpbml0ZTtcclxuICB9XHJcbiAgXHJcbiAgLmluZGV0ZXJtaW5hdGUtMTAwIHtcclxuICAgICAgYW5pbWF0aW9uOiBpbmRldGVybWluYXRlLTEwMCAxcyBsaW5lYXIgaW5maW5pdGU7XHJcbiAgfVxyXG4gIFxyXG4gIC5pbmRldGVybWluYXRlLTEyMCB7XHJcbiAgICAgIGFuaW1hdGlvbjogaW5kZXRlcm1pbmF0ZS0xMjAgMXMgbGluZWFyIGluZmluaXRlO1xyXG4gIH1cclxuICBcclxuICAuaW5kZXRlcm1pbmF0ZS0xMjYge1xyXG4gICAgICBhbmltYXRpb246IGluZGV0ZXJtaW5hdGUtMTI2IDFzIGxpbmVhciBpbmZpbml0ZTtcclxuICB9XHJcbiAgXHJcbiAgLmluZGV0ZXJtaW5hdGUtMTUwIHtcclxuICAgICAgYW5pbWF0aW9uOiBpbmRldGVybWluYXRlLTE1MCAxcyBsaW5lYXIgaW5maW5pdGU7XHJcbiAgfVxyXG4gIFxyXG4gIEBrZXlmcmFtZXMgaW5kZXRlcm1pbmF0ZS0xNiB7XHJcbiAgICAgIGZyb20ge1xyXG4gICAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogMTZweCAwO1xyXG4gICAgICB9XHJcbiAgICAgIHRvIHtcclxuICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IDAgMDtcclxuICAgICAgfVxyXG4gIH1cclxuICBcclxuICBAa2V5ZnJhbWVzIGluZGV0ZXJtaW5hdGUtMjAge1xyXG4gICAgICBmcm9tIHtcclxuICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IDIwcHggMDtcclxuICAgICAgfVxyXG4gICAgICB0byB7XHJcbiAgICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiAwIDA7XHJcbiAgICAgIH1cclxuICB9XHJcbiAgXHJcbiAgQGtleWZyYW1lcyBpbmRldGVybWluYXRlLTI1IHtcclxuICAgICAgZnJvbSB7XHJcbiAgICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiAyNXB4IDA7XHJcbiAgICAgIH1cclxuICAgICAgdG8ge1xyXG4gICAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogMCAwO1xyXG4gICAgICB9XHJcbiAgfVxyXG4gIFxyXG4gIEBrZXlmcmFtZXMgaW5kZXRlcm1pbmF0ZS0yOCB7XHJcbiAgICAgIGZyb20ge1xyXG4gICAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogMjhweCAwO1xyXG4gICAgICB9XHJcbiAgICAgIHRvIHtcclxuICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IDAgMDtcclxuICAgICAgfVxyXG4gIH1cclxuICBcclxuICBAa2V5ZnJhbWVzIGluZGV0ZXJtaW5hdGUtNDAge1xyXG4gICAgICBmcm9tIHtcclxuICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IDQwcHggMDtcclxuICAgICAgfVxyXG4gICAgICB0byB7XHJcbiAgICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiAwIDA7XHJcbiAgICAgIH1cclxuICB9XHJcbiAgXHJcbiAgQGtleWZyYW1lcyBpbmRldGVybWluYXRlLTUwIHtcclxuICAgICAgZnJvbSB7XHJcbiAgICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiA1MHB4IDA7XHJcbiAgICAgIH1cclxuICAgICAgdG8ge1xyXG4gICAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogMCAwO1xyXG4gICAgICB9XHJcbiAgfVxyXG4gIFxyXG4gIEBrZXlmcmFtZXMgaW5kZXRlcm1pbmF0ZS01MiB7XHJcbiAgICAgIGZyb20ge1xyXG4gICAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogNTJweCAwO1xyXG4gICAgICB9XHJcbiAgICAgIHRvIHtcclxuICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IDAgMDtcclxuICAgICAgfVxyXG4gIH1cclxuICBcclxuICBAa2V5ZnJhbWVzIGluZGV0ZXJtaW5hdGUtNTYge1xyXG4gICAgICBmcm9tIHtcclxuICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IDU2cHggMDtcclxuICAgICAgfVxyXG4gICAgICB0byB7XHJcbiAgICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiAwIDA7XHJcbiAgICAgIH1cclxuICB9XHJcbiAgXHJcbiAgQGtleWZyYW1lcyBpbmRldGVybWluYXRlLTYwIHtcclxuICAgICAgZnJvbSB7XHJcbiAgICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiA2MHB4IDA7XHJcbiAgICAgIH1cclxuICAgICAgdG8ge1xyXG4gICAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogMCAwO1xyXG4gICAgICB9XHJcbiAgfVxyXG4gIFxyXG4gIEBrZXlmcmFtZXMgaW5kZXRlcm1pbmF0ZS03MCB7XHJcbiAgICAgIGZyb20ge1xyXG4gICAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogNzBweCAwO1xyXG4gICAgICB9XHJcbiAgICAgIHRvIHtcclxuICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IDAgMDtcclxuICAgICAgfVxyXG4gIH1cclxuICBcclxuICBAa2V5ZnJhbWVzIGluZGV0ZXJtaW5hdGUtNzYge1xyXG4gICAgICBmcm9tIHtcclxuICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IDc2cHggMDtcclxuICAgICAgfVxyXG4gICAgICB0byB7XHJcbiAgICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiAwIDA7XHJcbiAgICAgIH1cclxuICB9XHJcbiAgXHJcbiAgQGtleWZyYW1lcyBpbmRldGVybWluYXRlLTgwIHtcclxuICAgICAgZnJvbSB7XHJcbiAgICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiA4MHB4IDA7XHJcbiAgICAgIH1cclxuICAgICAgdG8ge1xyXG4gICAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogMCAwO1xyXG4gICAgICB9XHJcbiAgfVxyXG4gIFxyXG4gIEBrZXlmcmFtZXMgaW5kZXRlcm1pbmF0ZS0xMDAge1xyXG4gICAgICBmcm9tIHtcclxuICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IDEwMHB4IDA7XHJcbiAgICAgIH1cclxuICAgICAgdG8ge1xyXG4gICAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogMCAwO1xyXG4gICAgICB9XHJcbiAgfVxyXG4gIFxyXG4gIEBrZXlmcmFtZXMgaW5kZXRlcm1pbmF0ZS0xMjAge1xyXG4gICAgICBmcm9tIHtcclxuICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IDEyMHB4IDA7XHJcbiAgICAgIH1cclxuICAgICAgdG8ge1xyXG4gICAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogMCAwO1xyXG4gICAgICB9XHJcbiAgfVxyXG4gIFxyXG4gIEBrZXlmcmFtZXMgaW5kZXRlcm1pbmF0ZS0xMjYge1xyXG4gICAgICBmcm9tIHtcclxuICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IDEyNnB4IDA7XHJcbiAgICAgIH1cclxuICAgICAgdG8ge1xyXG4gICAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogMCAwO1xyXG4gICAgICB9XHJcbiAgfVxyXG4gIFxyXG4gIEBrZXlmcmFtZXMgaW5kZXRlcm1pbmF0ZS0xNTAge1xyXG4gICAgICBmcm9tIHtcclxuICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IDE1MHB4IDA7XHJcbiAgICAgIH1cclxuICAgICAgdG8ge1xyXG4gICAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogMCAwO1xyXG4gICAgICB9XHJcbiAgfVxyXG4gICJdfQ== */"

/***/ }),

/***/ "./src/app/analytic/analytic.component.html":
/*!**************************************************!*\
  !*** ./src/app/analytic/analytic.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<link  href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css\"  rel=\"stylesheet\">\n\n\n\n\n\n\n\n\n<!--\n<div id=\"main\">\n <nav id=\"header\" class=\"navbar navbar-expand-lg navbar-dark bg-dark\">\n  <a class=\"navbar-brand\" href=\"#\">\n    FANI\n  </a>\n\n  <button class=\"navbar-toggler\" type=\"button\"\n          (click)=\"toggleNavbar()\">\n    <span class=\"navbar-toggler-icon\"></span>\n  </button>\n\n\n  <div class=\"collapse navbar-collapse\"\n       [ngClass]=\"{ 'show': navbarOpen }\">\n    <ul class=\"navbar-nav mr-auto\">\n      <li class=\"nav-item\">\n        <a class=\"nav-link\"routerLink=\"/signup\">calendar</a>\n      </li>\n\n      <li class=\"nav-item\">\n        <a class=\"nav-link\" href=\"#\">logout</a>\n      </li>\n    </ul>\n  </div>\n </nav>\n\n</div>-->\n\n<div id=\"maincontenthome\">\n\n  \n<!--style=\"background-color:rgb(245, 245, 245);\"-->\n\n<div class=\"container\">\n  <div class=\"row\">\n    <div class=\"col-md-12\" >\n        <h2  align=\"left\" style=\"color:#808080;\">Last week total</h2>\n      <!-- <h4  align=\"right\" style=\"color:#13101b;\">Today </h4>-->\n    <!--  <div id=\"myProgress\">\n           <div id=\"myBar\"></div>\n      </div>-->\n     <!-- <div id=\"txt\"></div>-->\n      <!--<h6  align=\"center\" style=\"color:#13101b;\"> <div id=\"calories\"></div></h6>-->\n    </div>\n\n    <div class=\"col-md-12\" >\n        <h6 id=\"day1\" align=\"right\" style=\"color:#13101b;\"> </h6>\n        <div  align=\"right\" id=\"txt\"></div>\n        \n        <div class=\"progress\">\n          \n            <div class=\"bar shadow indeterminate-60 leaf\"></div>\n            \n        </div> \n        <h6 id=\"day2\"  align=\"right\" style=\"color:#13101b;\"> </h6>\n        <div  align=\"right\" id=\"txt2\"></div>\n        <div class=\"progress\">\n            <div class=\"bar shadow indeterminate-40 bars\"></div>\n        </div>\n        <h6 id=\"day3\"  align=\"right\" style=\"color:#13101b;\"> </h6>\n        <div  align=\"right\" id=\"txt3\"></div>\n        <div class=\"progress\">\n            <div class=\"bar shadow indeterminate-150 circuit\"></div>\n        </div>\n        <h6 id=\"day4\"  align=\"right\" style=\"color:#13101b;\"> </h6>\n        <div  align=\"right\" id=\"txt4\"></div>\n        <div class=\"progress\">\n            <div class=\"bar shadow indeterminate-100 aztec\"></div>\n        </div>\n\n     \n        \n      \n    </div>\n\n   \n    <div class=\"col-md-12\" >\n        \n    \n      <div class=\"container\">\n         \n        <!--  <div id=\"bar1\" class=\"progress\">\n              <div class=\"bar shadow indeterminate-60 overlap\"></div>\n          </div>-->\n         \n        \n        <!--  <div class=\"progress\">\n              <div class=\"bar shadow indeterminate-40 bars\"></div>\n          </div>-->\n        <!--  <div class=\"progress\">\n              <div class=\"bar shadow indeterminate-80 lines\"></div>\n          </div>\n          <div class=\"progress\">\n              <div class=\"bar shadow indeterminate-52 wiggle\"></div>\n          </div>\n          <div class=\"progress\">\n              <div class=\"bar shadow indeterminate-16 dots\"></div>\n          </div>\n          <div class=\"progress\">\n              <div class=\"bar shadow indeterminate-150 circuit\"></div>\n          </div>\n          <div class=\"progress\">\n              <div class=\"bar shadow indeterminate-100 aztec\"></div>\n          </div>\n          <div class=\"progress\">\n              <div class=\"bar shadow indeterminate-28 bees\"></div>\n          </div>\n          <div class=\"progress\">\n              <div class=\"bar shadow indeterminate-120 food\"></div>\n          </div>\n          <div class=\"progress\">\n              <div class=\"bar shadow indeterminate-56 clouds\"></div>\n          </div>\n          <div class=\"progress\">\n              <div class=\"bar shadow indeterminate-25 stripes\"></div>\n          </div>\n          <div class=\"progress\">\n              <div class=\"bar shadow indeterminate-25 crosses\"></div>\n          </div>\n          <div class=\"progress\">\n              <div class=\"bar shadow indeterminate-52 jupiter\"></div>\n          </div>\n          <div class=\"progress\">\n              <div class=\"bar shadow indeterminate-70 piano\"></div>\n          </div>\n          <div class=\"progress\">\n              <div class=\"bar shadow indeterminate-126 dominos\"></div>\n          </div>\n          <div class=\"progress\">\n              <div class=\"bar shadow indeterminate-50 pie\"></div>\n          </div>\n          <div class=\"progress\">\n              <div class=\"bar shadow indeterminate-20 floor\"></div>\n          </div>\n          <div class=\"progress\">\n              <div class=\"bar shadow indeterminate-100 bubbles\"></div>\n          </div>\n          <div class=\"progress\">\n              <div class=\"bar shadow indeterminate-76 ticTac\"></div>\n          </div>\n          <div class=\"progress\">\n              <div class=\"bar shadow indeterminate-40 zigZag\"></div>\n          </div>-->\n      </div>\n    </div>\n  </div>\n</div>\n</div>\n\n\n<div id=\"all\">\n\n  \n    <div class=\"home\">\n        <span id=\"home\" class=\"home\">Home  </span>\n        <input id=\"button1\" type=\"button\"  (click)=\"submitted=false\"  (click)=\"goToPage('/', 'main')\">\n      \n    </div>\n\n     <div class=\"analytic\">\n      <span id=\"analytic\" class=\"analytic\">Analytics  </span>\n      <input id=\"button2\" type=\"button\" alt=\"Submit\" width=\"48\" height=\"48\" (click)=\"submitted=false\" (click)=\"goToPage('/', 'analytic')\">\n  \n    </div>\n\n    <div class=\"notification\">\n        <span id=\"notification\" class=\"notification\">Calendar  </span>\n        <input id=\"button3\" type=\"button\" alt=\"Submit\" width=\"48\" height=\"48\" (click)=\"submitted=false\" (click)=\"goToPage('/', 'calendar')\">\n    \n      </div>\n    \n      <div class=\"setting\">\n          <span id=\"setting\" class=\"setting\">Settings  </span>\n          <input id=\"button4\" type=\"button\" alt=\"Submit\" width=\"48\" height=\"48\" (click)=\"submitted=false\" (click)=\"goToPage('/', 'settings')\">\n      \n        </div>\n   \n \n \n</div>\n\n\n<!--\n<div class=\"container\">\n  <div class=\"row\">\n    <div class=\"col-sm\">\n      <button class=\"button\">Green</button>\n    </div>\n    <div class=\"col-sm\">\n      <button class=\"button\">Green</button>\n    </div>\n    <div class=\"col-sm\">\n        <button class=\"button\">Green</button>\n     \n    </div>\n\n    <div class=\"col-sm\">\n        <button class=\"button\">Green</button>\n     \n    </div>\n\n   \n  </div>\n</div>\n-->\n\n\n"

/***/ }),

/***/ "./src/app/analytic/analytic.component.ts":
/*!************************************************!*\
  !*** ./src/app/analytic/analytic.component.ts ***!
  \************************************************/
/*! exports provided: AnalyticComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AnalyticComponent", function() { return AnalyticComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./src/node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _analytic__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./analytic */ "./src/app/analytic/analytic.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");






var AnalyticComponent = /** @class */ (function () {
    function AnalyticComponent(router, httpClient) {
        this.router = router;
        this.httpClient = httpClient;
        this.apiURL = 'http://139.91.200.103:3000/api/things/setlasthistoryinfo/';
        this.apiURLHistory = 'http://139.91.200.103:3000/api/things/gethistoryvalue/';
        this.navbarOpen = false;
    }
    AnalyticComponent.prototype.bars = function (arg, value) {
        // arg="0";
        //alert(arg);
        //alert(value);
        switch (arg) {
            case "0": {
                var a = 100;
                var bars = document.querySelectorAll('.bar')[arg];
                var progress = document.querySelectorAll('.progress');
                var bar1 = document.getElementById("bar1");
                var randomWidth = Math.floor((Math.random() * 65) + 10);
                bars.style.width = randomWidth + "%";
                // progress[arg].addEventListener('mouseover', () => {
                var randomTiming = Math.floor((Math.random() * 2) + 2);
                //  alert(randomTiming);
                console.log(randomTiming);
                bars.style.transitionDuration = randomTiming + "s";
                bars.style.width = value.toString() + "%";
                // });
            }
            case "1": {
                var a = 20;
                var bars = document.querySelectorAll('.bar')[1];
                var progress = document.querySelectorAll('.progress');
                var bar1 = document.getElementById("bar1");
                var randomWidth = Math.floor((Math.random() * 65) + 10);
                bars.style.width = randomWidth + "%";
                // progress[1].addEventListener('mouseover', () => {
                var randomTiming = Math.floor((Math.random() * 2) + 2);
                console.log(randomTiming);
                bars.style.transitionDuration = randomTiming + "s";
                bars.style.width = value.toString() + "%";
                //  });
            }
            case "2": {
                // var a=20;
                var bars = document.querySelectorAll('.bar')[2];
                var progress = document.querySelectorAll('.progress');
                var bar1 = document.getElementById("bar1");
                var randomWidth = Math.floor((Math.random() * 65) + 10);
                bars.style.width = randomWidth + "%";
                //   progress[2].addEventListener('mouseover', () => {
                var randomTiming = Math.floor((Math.random() * 2) + 2);
                console.log(randomTiming);
                bars.style.transitionDuration = randomTiming + "s";
                bars.style.width = value.toString() + "%";
                // });
            }
            case "3": {
                // var a=20;
                var bars = document.querySelectorAll('.bar')[3];
                var progress = document.querySelectorAll('.progress');
                var bar1 = document.getElementById("bar1");
                var randomWidth = Math.floor((Math.random() * 65) + 10);
                bars.style.width = randomWidth + "%";
                //   progress[2].addEventListener('mouseover', () => {
                var randomTiming = Math.floor((Math.random() * 2) + 2);
                console.log(randomTiming);
                bars.style.transitionDuration = randomTiming + "s";
                bars.style.width = value.toString() + "%";
                // });
            }
        }
    };
    AnalyticComponent.prototype.getAnalytic = function (name) {
        var data = {
            "name": "fr"
        };
        return this.httpClient.get("" + this.apiURLHistory + name, { responseType: "json" });
    };
    AnalyticComponent.prototype.ngOnInit = function () {
        var _this = this;
        var day = new Date().getDay().toString();
        var month = new Date().getMonth().toString();
        var year = new Date().getFullYear().toString();
        // alert((new Date()).getDay());
        // alert((new Date()).getMonth());
        // alert((new Date()).getFullYear());
        jquery__WEBPACK_IMPORTED_MODULE_2__("#session").hide();
        //$("#back").hide();
        //$("#date").show();
        /////////////////////////////////////////
        /* this.getAnalytic("Emily").subscribe((res)=>{
         //  alert("Hi to all analytic");
          // alert(res[0].date);
          // alert(res[0].date);
          }, (error) =>
          {
            //alert("error hier");
          //  alert(error);
              // if the request fails, this callback will be invoked
          });*/
        ///////////////////////////////////////  
        var prevScrollpos = window.pageYOffset;
        window.onscroll = function () {
            var currentScrollPos = window.pageYOffset;
            if (prevScrollpos > currentScrollPos) {
                document.getElementById("navbar").style.top = "0";
            }
            else {
                document.getElementById("navbar").style.top = "0px";
            }
            prevScrollpos = currentScrollPos;
        };
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        var todays = mm + '/' + dd + '/' + yyyy;
        //alert(todays);
        new _analytic__WEBPACK_IMPORTED_MODULE_4__["Analytic"]().setDate(todays, month, year);
        function delay(ms) {
            return new Promise(function (resolve) { return setTimeout(resolve, ms); });
        }
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(1000)];
                    case 1:
                        _a.sent();
                        this.bars("0", 90);
                        this.bars("1", 50);
                        this.bars("2", 80);
                        this.bars("3", 90);
                        new _analytic__WEBPACK_IMPORTED_MODULE_4__["Analytic"]().processPercentage(25, "1100/2400", "1200/2400", "1300/2400", "1400/2400");
                        return [2 /*return*/];
                }
            });
        }); })();
        var num1 = 1250;
        document.getElementById("analytic").style.backgroundColor = "#696969";
        //document.getElementById("calories").textContent="Current "+num1+" calories";
        //this.move();
    };
    AnalyticComponent.prototype.move = function () {
        /* let percent:number = 1;
         var elem = document.getElementById("myBar");
         var perc = document.getElementById("txt");
         var width = 0;
         var percentTotal=72;
         var id = setInterval(frame, 33);
         function frame() {
           if (width >= 90&&width <= 100) {
             document.getElementById("cur").textContent="You need more exercise";
             document.getElementById("myProgress").style.backgroundColor="#8B0000";
             clearInterval(id);
           }else if(width>80&&width<90){
             document.getElementById("myProgress").style.backgroundColor="#7CFC00";
             document.getElementById("cur").textContent="You are good";
             clearInterval(id);
           } else {
             width++;
             percent++;
             percentTotal++;
             perc.textContent=""+percent+"%";
             elem.style.width = width + '%';
           }
         }*/
    };
    AnalyticComponent.prototype.toggleNavbar = function () {
        this.navbarOpen = !this.navbarOpen;
    };
    AnalyticComponent.prototype.goToPage = function (pagename, parameter) {
        this.router.navigate([pagename, parameter]);
    };
    AnalyticComponent.prototype.submit1 = function () {
        document.getElementById("home").style.backgroundColor = "#696969";
    };
    AnalyticComponent.prototype.submit2 = function () {
    };
    AnalyticComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-analytic',
            template: __webpack_require__(/*! ./analytic.component.html */ "./src/app/analytic/analytic.component.html"),
            styles: [__webpack_require__(/*! ./analytic.component.css */ "./src/app/analytic/analytic.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"]])
    ], AnalyticComponent);
    return AnalyticComponent;
}());



/***/ }),

/***/ "./src/app/analytic/analytic.ts":
/*!**************************************!*\
  !*** ./src/app/analytic/analytic.ts ***!
  \**************************************/
/*! exports provided: Analytic */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Analytic", function() { return Analytic; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./src/node_modules/tslib/tslib.es6.js");

var Analytic = /** @class */ (function () {
    function Analytic() {
    }
    Analytic.prototype.processDates = function () {
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        var day = +dd;
        var days = day - 1;
        var todays = mm + '/' + days + '/' + yyyy;
        document.getElementById("day1").textContent = todays;
        var days1 = day - 2;
        var todays1 = mm + '/' + days1 + '/' + yyyy;
        document.getElementById("day2").textContent = todays1;
        var days2 = day - 3;
        var todays2 = mm + '/' + days2 + '/' + yyyy;
        document.getElementById("day3").textContent = todays2;
        var days3 = day - 4;
        var todays3 = mm + '/' + days3 + '/' + yyyy;
        document.getElementById("day4").textContent = todays3;
    };
    Analytic.prototype.processPercentage = function (percentage, value, value2, value3, value4) {
        var _this = this;
        var percent = 1;
        //var elem = document.getElementById("myBar");  
        var perc = document.getElementById("txt");
        var perc2 = document.getElementById("txt2");
        var perc3 = document.getElementById("txt3");
        var perc4 = document.getElementById("txt4");
        var width = 0;
        var percentTotal = 72;
        function delay(ms) {
            return new Promise(function (resolve) { return setTimeout(resolve, ms); });
        }
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(4000)];
                    case 1:
                        _a.sent();
                        perc.textContent = "Your calories " + value + "";
                        perc2.textContent = "Your calories " + value2 + "";
                        perc3.textContent = "Your calories " + value3 + "";
                        perc4.textContent = "Your calories " + value4 + "";
                        return [2 /*return*/];
                }
            });
        }); })();
        //var id = setInterval(frame, 27);
        /* function frame() {
           if (width >= percentage&&width <= 100) {
             perc.textContent="Your calories "+value+"";
             clearInterval(id);
           }else {
             width++;
             percent++;
             percentTotal++;
           
            // elem.style.width = width + '%';
           }
         }*/
    };
    Analytic.prototype.setDate = function (date1, date2, date3) {
        date1 = date1;
        date2 = date2;
        date3 = date3;
        this.processDates();
        //this.processPercentage();
        // alert(date1);
        //alert(date2);
        // alert(date3);
        // let fulldate:any=date1+"/"+date2+"/"+date3;
        //console.log("date1:"+date1);
    };
    return Analytic;
}());



/***/ }),

/***/ "./src/app/animations.ts":
/*!*******************************!*\
  !*** ./src/app/animations.ts ***!
  \*******************************/
/*! exports provided: slideInAnimation */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "slideInAnimation", function() { return slideInAnimation; });
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm5/animations.js");

var slideInAnimation = Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["trigger"])('routeAnimations', [
    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["transition"])('HomePage <=> AboutPage', [
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ position: 'relative' }),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["query"])(':enter, :leave', [
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%'
            })
        ]),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["query"])(':enter', [
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ left: '-100%' })
        ]),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["query"])(':leave', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animateChild"])()),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["group"])([
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["query"])(':leave', [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('300ms ease-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ left: '100%' }))
            ]),
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["query"])(':enter', [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('300ms ease-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ left: '0%' }))
            ])
        ]),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animateChild"])()),
    ]),
    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["transition"])('* <=> FilterPage', [
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ position: 'relative' }),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["query"])(':enter, :leave', [
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%'
            })
        ]),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["query"])(':enter', [
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ left: '-100%' })
        ]),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["query"])(':leave', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animateChild"])()),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["group"])([
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["query"])(':leave', [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('200ms ease-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ left: '100%' }))
            ]),
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["query"])(':enter', [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('300ms ease-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ left: '0%' }))
            ])
        ]),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animateChild"])()),
    ])
]);


/***/ }),

/***/ "./src/app/api.service.ts":
/*!********************************!*\
  !*** ./src/app/api.service.ts ***!
  \********************************/
/*! exports provided: ApiService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApiService", function() { return ApiService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./src/node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");



//import { Customer } from './customer';
var ApiService = /** @class */ (function () {
    function ApiService(httpClient) {
        this.httpClient = httpClient;
        this.apiURL = 'http://localhost:3000/api/things/';
        this.firstPage = "";
        this.prevPage = "";
        this.nextPage = "";
        this.lastPage = "";
    }
    ApiService.prototype.getCustomers = function () {
        alert("called customers");
        /* return this.httpClient.get(`${this.apiURL}`, {
           params: {
             id: '3'
            
           },
           observe: 'response'
         })*/
        var i = 3;
        var data = {
            name: "dede",
            name2: "nou"
        };
        return this.httpClient.get("" + this.apiURL + JSON.stringify(data));
    };
    ApiService.prototype.createCustomer = function () {
        /* const httpOptions = {
           headers: new HttpHeaders({
             'Content-Type':  'application/json',
             'Authorization': 'my-auth-token'
           })
         };*/
        /*
        this.http.post(
           "http://localhost:3000/contacts",
           JSON.stringify({id: 4, name: 'some'}),
           httpOptions
        ).subscribe();*/
        //var HttpClient = new HttpClient();
        //HttpClient.append('Content-Type', 'application/json');
        //return this.httpClient.post(`${this.apiURL}`,JSON.stringify({name:"okza"}));
        /*const headers={
          'Content-Type':  'application/json'
        
        }*/
        //const headers = new HttpHeaders().set('Content-Type', 'application/json');
        var data = {
            name: "dede"
        };
        var headerDict = {
            'Content-Type': 'application/json',
        };
        var requestOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"](headerDict),
        };
        var res = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]().set("name", "1");
        alert("fani" + JSON.stringify(data));
        return this.httpClient.post("" + this.apiURL, JSON.stringify(data));
        // return this.httpClient.post(`${this.apiURL}`,JSON.stringify(data) )
    };
    ApiService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], ApiService);
    return ApiService;
}());



/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./src/node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _signup_signup_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./signup/signup.component */ "./src/app/signup/signup.component.ts");
/* harmony import */ var _signin_signin_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./signin/signin.component */ "./src/app/signin/signin.component.ts");
/* harmony import */ var _start_start_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./start/start.component */ "./src/app/start/start.component.ts");
/* harmony import */ var _header_header_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./header/header.component */ "./src/app/header/header.component.ts");
/* harmony import */ var _analytic_analytic_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./analytic/analytic.component */ "./src/app/analytic/analytic.component.ts");
/* harmony import */ var _submit_form_submit_form_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./submit-form/submit-form.component */ "./src/app/submit-form/submit-form.component.ts");
/* harmony import */ var _header_diagrams_diagrams_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./header/diagrams/diagrams.component */ "./src/app/header/diagrams/diagrams.component.ts");
/* harmony import */ var _admin_admin_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./admin/admin.component */ "./src/app/admin/admin.component.ts");
/* harmony import */ var _calendar_calendar_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./calendar/calendar.component */ "./src/app/calendar/calendar.component.ts");
/* harmony import */ var _notification_notification_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./notification/notification.component */ "./src/app/notification/notification.component.ts");
/* harmony import */ var _infos_infos_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./infos/infos.component */ "./src/app/infos/infos.component.ts");
/* harmony import */ var _settings_settings_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./settings/settings.component */ "./src/app/settings/settings.component.ts");
/* harmony import */ var _error_error_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./error/error.component */ "./src/app/error/error.component.ts");
/* harmony import */ var _sockets_sockets_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./sockets/sockets.component */ "./src/app/sockets/sockets.component.ts");
/* harmony import */ var _qrcode_qrcode_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./qrcode/qrcode.component */ "./src/app/qrcode/qrcode.component.ts");


















var routes = [
    { path: 'signup', component: _signup_signup_component__WEBPACK_IMPORTED_MODULE_3__["SignupComponent"] },
    { path: '', component: _signin_signin_component__WEBPACK_IMPORTED_MODULE_4__["SigninComponent"] },
    { path: 'start', component: _start_start_component__WEBPACK_IMPORTED_MODULE_5__["StartComponent"] },
    { path: 'component', component: _submit_form_submit_form_component__WEBPACK_IMPORTED_MODULE_8__["SubmitFormComponent"] },
    { path: 'main', component: _header_header_component__WEBPACK_IMPORTED_MODULE_6__["HeaderComponent"] },
    { path: 'analytic', component: _analytic_analytic_component__WEBPACK_IMPORTED_MODULE_7__["AnalyticComponent"] },
    { path: 'diagrams', component: _header_diagrams_diagrams_component__WEBPACK_IMPORTED_MODULE_9__["DiagramsComponent"] },
    { path: 'calendar', component: _calendar_calendar_component__WEBPACK_IMPORTED_MODULE_11__["CalendarComponent"] },
    { path: 'paneladmin', component: _admin_admin_component__WEBPACK_IMPORTED_MODULE_10__["AdminComponent"] },
    { path: 'notification', component: _notification_notification_component__WEBPACK_IMPORTED_MODULE_12__["NotificationComponent"] },
    { path: 'infos/:name', component: _infos_infos_component__WEBPACK_IMPORTED_MODULE_13__["InfosComponent"] },
    { path: 'settings', component: _settings_settings_component__WEBPACK_IMPORTED_MODULE_14__["SettingsComponent"] },
    { path: 'error', component: _error_error_component__WEBPACK_IMPORTED_MODULE_15__["ErrorComponent"] },
    { path: 'sockets', component: _sockets_sockets_component__WEBPACK_IMPORTED_MODULE_16__["SocketsComponent"] },
    { path: 'qrcode', component: _qrcode_qrcode_component__WEBPACK_IMPORTED_MODULE_17__["QrcodeComponent"] }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/*\r\nul {\r\n    max-width: 500px;\r\n    color: #333;\r\n    list-style: none;\r\n    margin: 50px auto;\r\n    padding: 10px;\r\n}\r\n\r\n.list li {\r\n    padding: 25px;\r\n    background: #FAFAAF;\r\n    border-radius: 5px;\r\n    margin-bottom: 5px;\r\n    text-align: center;\r\n    font-size: 32px;\r\n    font-weight: 600;\r\n}\r\n\r\n.center {\r\n    text-align: center;\r\n}*/\r\n\r\nbody {\r\n  font-family: \"Lato\", sans-serif;\r\n}\r\n\r\n.sidenav {\r\n  height: 100%;\r\n  width: 0;\r\n  position: fixed;\r\n  z-index: 1;\r\n  top: 0;\r\n  left: 0;\r\n  background-color: #ffffff;\r\n  overflow-x: hidden;\r\n  transition: 0.5s;\r\n  padding-top: 60px;\r\n}\r\n\r\n.sidenav a {\r\n  padding: 8px 8px 8px 32px;\r\n  text-decoration: none;\r\n  font-size: 25px;\r\n  color: #818181;\r\n  display: block;\r\n  transition: 0.3s;\r\n}\r\n\r\n.sidenav a:hover {\r\n  color: #f1f1f1;\r\n}\r\n\r\n.sidenav .closebtn {\r\n  position: absolute;\r\n  top: 0;\r\n  right: 25px;\r\n  font-size: 36px;\r\n  margin-left: 50px;\r\n}\r\n\r\n@media screen and (max-height: 450px) {\r\n  .sidenav {padding-top: 15px;}\r\n  .sidenav a {font-size: 18px;}\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9hcHAuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0VBcUJFOztBQUVGO0VBQ0UsK0JBQStCO0FBQ2pDOztBQUVBO0VBQ0UsWUFBWTtFQUNaLFFBQVE7RUFDUixlQUFlO0VBQ2YsVUFBVTtFQUNWLE1BQU07RUFDTixPQUFPO0VBQ1AseUJBQXlCO0VBQ3pCLGtCQUFrQjtFQUNsQixnQkFBZ0I7RUFDaEIsaUJBQWlCO0FBQ25COztBQUVBO0VBQ0UseUJBQXlCO0VBQ3pCLHFCQUFxQjtFQUNyQixlQUFlO0VBQ2YsY0FBYztFQUNkLGNBQWM7RUFDZCxnQkFBZ0I7QUFDbEI7O0FBRUE7RUFDRSxjQUFjO0FBQ2hCOztBQUVBO0VBQ0Usa0JBQWtCO0VBQ2xCLE1BQU07RUFDTixXQUFXO0VBQ1gsZUFBZTtFQUNmLGlCQUFpQjtBQUNuQjs7QUFFQTtFQUNFLFVBQVUsaUJBQWlCLENBQUM7RUFDNUIsWUFBWSxlQUFlLENBQUM7QUFDOUIiLCJmaWxlIjoiYXBwL2FwcC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxudWwge1xyXG4gICAgbWF4LXdpZHRoOiA1MDBweDtcclxuICAgIGNvbG9yOiAjMzMzO1xyXG4gICAgbGlzdC1zdHlsZTogbm9uZTtcclxuICAgIG1hcmdpbjogNTBweCBhdXRvO1xyXG4gICAgcGFkZGluZzogMTBweDtcclxufVxyXG5cclxuLmxpc3QgbGkge1xyXG4gICAgcGFkZGluZzogMjVweDtcclxuICAgIGJhY2tncm91bmQ6ICNGQUZBQUY7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICBtYXJnaW4tYm90dG9tOiA1cHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBmb250LXNpemU6IDMycHg7XHJcbiAgICBmb250LXdlaWdodDogNjAwO1xyXG59XHJcblxyXG4uY2VudGVyIHtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxufSovXHJcblxyXG5ib2R5IHtcclxuICBmb250LWZhbWlseTogXCJMYXRvXCIsIHNhbnMtc2VyaWY7XHJcbn1cclxuXHJcbi5zaWRlbmF2IHtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbiAgd2lkdGg6IDA7XHJcbiAgcG9zaXRpb246IGZpeGVkO1xyXG4gIHotaW5kZXg6IDE7XHJcbiAgdG9wOiAwO1xyXG4gIGxlZnQ6IDA7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcclxuICBvdmVyZmxvdy14OiBoaWRkZW47XHJcbiAgdHJhbnNpdGlvbjogMC41cztcclxuICBwYWRkaW5nLXRvcDogNjBweDtcclxufVxyXG5cclxuLnNpZGVuYXYgYSB7XHJcbiAgcGFkZGluZzogOHB4IDhweCA4cHggMzJweDtcclxuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgZm9udC1zaXplOiAyNXB4O1xyXG4gIGNvbG9yOiAjODE4MTgxO1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIHRyYW5zaXRpb246IDAuM3M7XHJcbn1cclxuXHJcbi5zaWRlbmF2IGE6aG92ZXIge1xyXG4gIGNvbG9yOiAjZjFmMWYxO1xyXG59XHJcblxyXG4uc2lkZW5hdiAuY2xvc2VidG4ge1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICB0b3A6IDA7XHJcbiAgcmlnaHQ6IDI1cHg7XHJcbiAgZm9udC1zaXplOiAzNnB4O1xyXG4gIG1hcmdpbi1sZWZ0OiA1MHB4O1xyXG59XHJcblxyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LWhlaWdodDogNDUwcHgpIHtcclxuICAuc2lkZW5hdiB7cGFkZGluZy10b3A6IDE1cHg7fVxyXG4gIC5zaWRlbmF2IGEge2ZvbnQtc2l6ZTogMThweDt9XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n\n<link  href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css\"  rel=\"stylesheet\">\n\n\n\n<div id=\"main\">\n  \n<!--\n\nserver/node_modules/.bin/ts-node-dev server/server.ts\n\n-->\n<div id=\"mySidenav\" class=\"sidenav\">\n  <a href=\"javascript:void(0)\" class=\"closebtn\" (click)=\"closeNav()\">&times;</a>\n  <a routerLink=\"/notification\">Notification</a>\n  <a routerLink=\"/qrcode\">Bonus</a>\n  <a href=\"#\">Clients</a>\n  <a href=\"#\">Log out</a>\n</div>\n\n\n<span style=\"font-size:30px;cursor:pointer\" (click)=\"openNav()\">&#9776; open</span>\n\n\n\n\n\n\n<a id=\"session\" class=\"navbar-brand\" href=\"#\"></a>\n \n\n</div>\n\n<router-outlet></router-outlet>\n\n<div id=\"alllist\">\n <!--   <h1 class=\"center\">Virtual Scroll using Angular 7</h1>\n    \n    <ul class=\"list\">\n      <cdk-virtual-scroll-viewport  style=\"height: 500px\" itemSize=\"90\" >\n        <ng-container *cdkVirtualFor=\"let n of numbers\">\n          <li> {{n}} </li>\n        </ng-container>\n      </cdk-virtual-scroll-viewport>\n    </ul>-->\n    </div>\n<!-- You can now use it in app.component.html -->\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./src/node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./api.service */ "./src/app/api.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _animations__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./animations */ "./src/app/animations.ts");
/* harmony import */ var ng_push__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ng-push */ "./node_modules/ng-push/ng-push.umd.js");
/* harmony import */ var ng_push__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(ng_push__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var ngx_push_notifications__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-push-notifications */ "./node_modules/ngx-push-notifications/fesm5/ngx-push-notifications.js");






//import { PushNotificationService } from 'ngx-push-notifications';

//const socket = io('http://localhost?token=abc');
//import { Socket } from 'ngx-socket-io';
//import { Analytic } from './analytic';
var AppComponent = /** @class */ (function () {
    function AppComponent(_pushNotificationService, _pushNotifications, apiService, router) {
        this._pushNotificationService = _pushNotificationService;
        this._pushNotifications = _pushNotifications;
        this.apiService = apiService;
        this.router = router;
        this.title = 'angularfinal';
        this.numbers = [];
        this._pushNotifications.requestPermission();
    }
    ;
    //title = 'Web push Notifications!';
    /* constructor( ) {
       this._pushNotifications.requestPermission();
     }*/
    AppComponent.prototype.myFunction = function () {
        var _this = this;
        var title = 'Hello';
        var options = new ngx_push_notifications__WEBPACK_IMPORTED_MODULE_6__["PushNotificationOptions"]();
        options.body = 'Native Push Notification';
        this._pushNotificationService.create(title, options).subscribe(function (notif) {
            if (notif.event.type === 'show') {
                console.log('onshow');
                setTimeout(function () {
                    notif.notification.close();
                }, 9000);
            }
            if (notif.event.type === 'click') {
                console.log('click');
                alert("Clicked");
                //notif.notification.close();
                _this.goToPage("/", "calendar");
                notif.notification.close();
            }
            if (notif.event.type === 'close') {
                console.log('close');
            }
        }, function (err) {
            console.log(err);
        });
    };
    AppComponent.prototype.notify = function () {
        var options = {
            body: "The c is, I'am Iron Man!",
            href: "http://154.57.7.149:8080/notification",
            icon: "assets/images/ironman.png" //adding an icon
        };
        this._pushNotifications.create('Iron Man', options).subscribe(//creates a notification
        function (//creates a notification
        res) { return console.log(res); }, function (err) { return console.log(err); });
    };
    AppComponent.prototype.openNav = function () {
        //  alert("calaled");
        document.getElementById("mySidenav").style.width = "250px";
    };
    AppComponent.prototype.closeNav = function () {
        document.getElementById("mySidenav").style.width = "0";
    };
    AppComponent.prototype.ngOnInit = function () {
        //document.getElementById("main").style.backgroundColor="red";
        //  $("#main").hide();
        /*  this._pushNotificationService.requestPermission();
          this.chat.messages.subscribe(msg => {
            console.log(msg);
          })*/
        // new WebsocketService(null).sendMessage("sasa");
        //   alert( new WebsocketService(null).getMessage());
        //alert("loc");
        // alert(location.pathname);
        // alert(   location.href);
        // new WebsocketService(Socket.arguments("s")).sendMessage("s");
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        var day = +dd;
        var days = day;
        var todays = mm + '/' + days + '/' + yyyy;
        //document.getElementById("date").textContent=todays;
        var prevScrollpos = window.pageYOffset;
        window.onscroll = function () {
            /* var currentScrollPos = window.pageYOffset;
               if (prevScrollpos > currentScrollPos) {
                 document.getElementById("navbar").style.top = "20";
               } else {
                 document.getElementById("navbar").style.top = "20px";
               }
               prevScrollpos = currentScrollPos;*/
        };
        if (location.pathname.trim() == "/signin") {
            // alert("equal");
            //  $('#main').hide();
        }
        else if (location.pathname.trim() == "/signup") {
            // $('#main').hide();
        }
        else {
            // $('#main').show();
        }
        /*  this.apiService.getCustomers().subscribe((res)=>{
             console.log("called");
             alert("cabled"+res[0].info);
               console.log(res);
           });*/
        /*var customer = {
         
           "name": "xaxa",
          
         }
         this.apiService.createCustomer(customer).subscribe((res)=>{
           alert("response nouli is is"+res);
           console.log("Created a customer");
          });*/
    };
    /* sendMessage() {
      this.chat.sendMsg("Test Message");
     }*/
    //navbarOpen = false;
    AppComponent.prototype.toggleNavbar = function () {
        // this.navbarOpen = !this.navbarOpen;
    };
    AppComponent.prototype.goToPage = function (pagename, parameter) {
        //this.router.navigate([pagename, parameter]);
    };
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            animations: [
                _animations__WEBPACK_IMPORTED_MODULE_4__["slideInAnimation"]
                // animation triggers go here
            ],
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [ngx_push_notifications__WEBPACK_IMPORTED_MODULE_6__["PushNotificationService"], ng_push__WEBPACK_IMPORTED_MODULE_5__["PushNotificationsService"], _api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./src/node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _signin_signin_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./signin/signin.component */ "./src/app/signin/signin.component.ts");
/* harmony import */ var _signup_signup_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./signup/signup.component */ "./src/app/signup/signup.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _start_start_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./start/start.component */ "./src/app/start/start.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _submit_form_submit_form_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./submit-form/submit-form.component */ "./src/app/submit-form/submit-form.component.ts");
/* harmony import */ var _header_header_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./header/header.component */ "./src/app/header/header.component.ts");
/* harmony import */ var _analytic_analytic_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./analytic/analytic.component */ "./src/app/analytic/analytic.component.ts");
/* harmony import */ var _header_diagrams_diagrams_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./header/diagrams/diagrams.component */ "./src/app/header/diagrams/diagrams.component.ts");
/* harmony import */ var ng_circle_progress__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ng-circle-progress */ "./node_modules/ng-circle-progress/index.js");
/* harmony import */ var _calendar_calendar_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./calendar/calendar.component */ "./src/app/calendar/calendar.component.ts");
/* harmony import */ var ng_push__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ng-push */ "./node_modules/ng-push/ng-push.umd.js");
/* harmony import */ var ng_push__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(ng_push__WEBPACK_IMPORTED_MODULE_16__);
/* harmony import */ var _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/cdk/scrolling */ "./node_modules/@angular/cdk/esm5/scrolling.es5.js");
/* harmony import */ var _admin_admin_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./admin/admin.component */ "./src/app/admin/admin.component.ts");
/* harmony import */ var ngx_push_notifications__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ngx-push-notifications */ "./node_modules/ngx-push-notifications/fesm5/ngx-push-notifications.js");
/* harmony import */ var _notification_notification_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./notification/notification.component */ "./src/app/notification/notification.component.ts");
/* harmony import */ var _infos_infos_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./infos/infos.component */ "./src/app/infos/infos.component.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _settings_settings_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./settings/settings.component */ "./src/app/settings/settings.component.ts");
/* harmony import */ var ngx_editor__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ngx-editor */ "./node_modules/ngx-editor/fesm5/ngx-editor.js");
/* harmony import */ var _zxing_ngx_scanner__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! @zxing/ngx-scanner */ "./src/node_modules/@zxing/ngx-scanner/fesm5/zxing-ngx-scanner.js");
/* harmony import */ var ng6_socket_io__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ng6-socket-io */ "./node_modules/ng6-socket-io/dist/index.js");
/* harmony import */ var ng6_socket_io__WEBPACK_IMPORTED_MODULE_26___default = /*#__PURE__*/__webpack_require__.n(ng6_socket_io__WEBPACK_IMPORTED_MODULE_26__);
/* harmony import */ var _auto_generated_auto_generated_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./auto-generated/auto-generated.component */ "./src/app/auto-generated/auto-generated.component.ts");
/* harmony import */ var _error_error_component__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./error/error.component */ "./src/app/error/error.component.ts");
/* harmony import */ var _sockets_sockets_component__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./sockets/sockets.component */ "./src/app/sockets/sockets.component.ts");
/* harmony import */ var _qrcode_qrcode_component__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ./qrcode/qrcode.component */ "./src/app/qrcode/qrcode.component.ts");
/* harmony import */ var ngx_qrcode2__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ngx-qrcode2 */ "./src/node_modules/ngx-qrcode2/index.js");














// Import ng-circle-progress


 //import the module
/////animate//









//...






var config = { url: 'http://localhost:8988', options: {} };
//import { BrowserModule } from '@angular/platform-browser';
/////
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
                _signin_signin_component__WEBPACK_IMPORTED_MODULE_5__["SigninComponent"],
                _signup_signup_component__WEBPACK_IMPORTED_MODULE_6__["SignupComponent"],
                _start_start_component__WEBPACK_IMPORTED_MODULE_8__["StartComponent"],
                _submit_form_submit_form_component__WEBPACK_IMPORTED_MODULE_10__["SubmitFormComponent"],
                _header_header_component__WEBPACK_IMPORTED_MODULE_11__["HeaderComponent"],
                _analytic_analytic_component__WEBPACK_IMPORTED_MODULE_12__["AnalyticComponent"],
                _header_diagrams_diagrams_component__WEBPACK_IMPORTED_MODULE_13__["DiagramsComponent"],
                _calendar_calendar_component__WEBPACK_IMPORTED_MODULE_15__["CalendarComponent"],
                _admin_admin_component__WEBPACK_IMPORTED_MODULE_18__["AdminComponent"],
                _notification_notification_component__WEBPACK_IMPORTED_MODULE_20__["NotificationComponent"],
                _infos_infos_component__WEBPACK_IMPORTED_MODULE_21__["InfosComponent"],
                _settings_settings_component__WEBPACK_IMPORTED_MODULE_23__["SettingsComponent"],
                _auto_generated_auto_generated_component__WEBPACK_IMPORTED_MODULE_27__["AutoGeneratedComponent"],
                _error_error_component__WEBPACK_IMPORTED_MODULE_28__["ErrorComponent"],
                _sockets_sockets_component__WEBPACK_IMPORTED_MODULE_29__["SocketsComponent"],
                _qrcode_qrcode_component__WEBPACK_IMPORTED_MODULE_30__["QrcodeComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_7__["HttpClientModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_9__["FormsModule"], _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_17__["ScrollingModule"],
                ng_push__WEBPACK_IMPORTED_MODULE_16__["PushNotificationsModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_22__["CommonModule"],
                ngx_qrcode2__WEBPACK_IMPORTED_MODULE_31__["NgxQRCodeModule"],
                _zxing_ngx_scanner__WEBPACK_IMPORTED_MODULE_25__["ZXingScannerModule"],
                // Specify ng-circle-progress as an import
                ng_circle_progress__WEBPACK_IMPORTED_MODULE_14__["NgCircleProgressModule"].forRoot({
                    // set defaults here
                    radius: 100,
                    outerStrokeWidth: 16,
                    innerStrokeWidth: 8,
                    outerStrokeColor: "#78C000",
                    innerStrokeColor: "#C7E596",
                    animationDuration: 300,
                }),
                ngx_editor__WEBPACK_IMPORTED_MODULE_24__["NgxEditorModule"],
                ng6_socket_io__WEBPACK_IMPORTED_MODULE_26__["SocketIoModule"].forRoot(config)
            ],
            providers: [ngx_push_notifications__WEBPACK_IMPORTED_MODULE_19__["PushNotificationService"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/auto-generated/auto-generated.component.css":
/*!*************************************************************!*\
  !*** ./src/app/auto-generated/auto-generated.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAvYXV0by1nZW5lcmF0ZWQvYXV0by1nZW5lcmF0ZWQuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/auto-generated/auto-generated.component.html":
/*!**************************************************************!*\
  !*** ./src/app/auto-generated/auto-generated.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  auto-generated works!\n</p>\n"

/***/ }),

/***/ "./src/app/auto-generated/auto-generated.component.ts":
/*!************************************************************!*\
  !*** ./src/app/auto-generated/auto-generated.component.ts ***!
  \************************************************************/
/*! exports provided: AutoGeneratedComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AutoGeneratedComponent", function() { return AutoGeneratedComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./src/node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AutoGeneratedComponent = /** @class */ (function () {
    function AutoGeneratedComponent() {
    }
    AutoGeneratedComponent.prototype.ngOnInit = function () {
    };
    AutoGeneratedComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-auto-generated',
            template: __webpack_require__(/*! ./auto-generated.component.html */ "./src/app/auto-generated/auto-generated.component.html"),
            styles: [__webpack_require__(/*! ./auto-generated.component.css */ "./src/app/auto-generated/auto-generated.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], AutoGeneratedComponent);
    return AutoGeneratedComponent;
}());



/***/ }),

/***/ "./src/app/calendar/calendar.component.css":
/*!*************************************************!*\
  !*** ./src/app/calendar/calendar.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/*\r\n#myProgress {\r\n  width: 100%;\r\n  background-color: #ddd;\r\n}\r\n\r\n#myBar {\r\n  width: 1%;\r\n  height: 30px;\r\n  background-color: #4CAF50;\r\n}*/\r\n#button1 {\r\n    background-color: #b2b2b2\t; \r\n    border: none;\r\n    color: white;\r\n    padding-bottom: 25px 22px;\r\n    text-align: center;\r\n    text-decoration: none;\r\n    display: inline-block;\r\n    font-size: 36px;\r\n    margin: 4px 18px;\r\n    cursor: pointer;\r\n    \r\n  }\r\n#button2 {\r\n   background-color: #b2b2b2\t; \r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n#button3 {\r\n   background-color: #b2b2b2\t; \r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n#button4 {\r\n   background-color: #b2b2b2\t;\r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n#buttona {\r\n    border-radius: 25px;\r\n    background-color: #b2b2b2\t; \r\n    border: none;\r\n    color: white;\r\n    padding-bottom: 25px 22px;\r\n    text-align: center;\r\n    text-decoration: none;\r\n    display: inline-block;\r\n    font-size: 36px;\r\n    margin: 4px 18px;\r\n    cursor: pointer;\r\n    \r\n  }\r\n#buttonb {\r\n    border-radius: 25px;\r\n   background-color: #b2b2b2\t; \r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n#buttonc {\r\n    border-radius: 25px;\r\n   background-color: #b2b2b2\t; \r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n#buttond {\r\n    border-radius: 25px;\r\n   background-color: #b2b2b2\t;\r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n#buttone {\r\n    border-radius: 25px;\r\n    background-color: #b2b2b2\t;\r\n    border: none;\r\n    color: white;\r\n    padding-bottom: 25px 22px;\r\n    text-align: center;\r\n    text-decoration: none;\r\n    display: inline-block;\r\n    font-size: 36px;\r\n    margin: 4px 18px;\r\n    cursor: pointer;\r\n  }\r\n#all{\r\n     text-align: center;\r\n     position: fixed;\r\n     left: 0;\r\n     bottom: 0;\r\n     width: 100%;\r\n     background-color: \t\t#b2b2b2;\r\n     color: white;\r\n     text-align: center;\r\n  }\r\ndiv.home {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\ndiv.home input {\r\n    background: url('home.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\ndiv.analytic {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\ndiv.analytic input {\r\n    background: url('analytic.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\ndiv.notification {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\ndiv.notification input {\r\n    background: url('outline-today-24px.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\ndiv.setting {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\ndiv.setting input {\r\n    background: url('settings.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\nspan.home {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 6px;\r\n  }\r\nspan.analytic {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 2px;\r\n  }\r\nspan.notification {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 2px;\r\n  }\r\nspan.setting {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 12px;\r\n  }\r\n.headercalendar {\r\n    overflow: hidden;\r\n    background-color: #f2f2f2;\r\n    padding: 10px 50px;\r\n    text-align: center;\r\n    \r\n  }\r\ndiv.day1 {\r\n    position: relative;\r\n    display: inline-block;\r\n    \r\n    \r\n    \r\n  }\r\ndiv.day1 input {\r\n    background: url('outline-today-24px.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 70px;\r\n    border: none;\r\n  }\r\nspan.day1 {\r\n    position: absolute;\r\n    top: 28px;\r\n    left: 28px;\r\n  }\r\ndiv.day2 {\r\n    position: relative;\r\n    display: inline-block;\r\n    \r\n    \r\n    \r\n  }\r\ndiv.day2 input {\r\n    background: url('outline-today-24px.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 70px;\r\n    border: none;\r\n  }\r\nspan.day2 {\r\n    position: absolute;\r\n    top: 28px;\r\n    left: 28px;\r\n  }\r\ndiv.day3 {\r\n    position: relative;\r\n    display: inline-block;\r\n    \r\n    \r\n    \r\n  }\r\ndiv.day3 input {\r\n    background: url('outline-today-24px.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 70px;\r\n    border: none;\r\n  }\r\nspan.day3 {\r\n    position: absolute;\r\n    top: 28px;\r\n    left: 28px;\r\n  }\r\ndiv.day4 {\r\n    position: relative;\r\n    display: inline-block;\r\n    \r\n    \r\n    \r\n  }\r\ndiv.day4 input {\r\n    background: url('outline-today-24px.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 70px;\r\n    border: none;\r\n  }\r\nspan.day4 {\r\n    position: absolute;\r\n    top: 28px;\r\n    left: 28px;\r\n  }\r\ndiv.day5 {\r\n    position: relative;\r\n    display: inline-block;\r\n    \r\n    \r\n    \r\n  }\r\ndiv.day5 input {\r\n    background: url('outline-today-24px.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 70px;\r\n    border: none;\r\n  }\r\nspan.day5 {\r\n    position: absolute;\r\n    top: 28px;\r\n    left: 28px;\r\n  }\r\nul {\r\n    max-width: 600px;\r\n    color: #333;\r\n    list-style: none;\r\n    margin: 50px auto;\r\n    padding: 10px;\r\n}\r\n.list li {\r\n    padding: 25px;\r\n    background: #f2f2f2;\r\n    border-radius: 5px;\r\n    margin-bottom: 5px;\r\n    text-align: center;\r\n    font-size: 32px;\r\n    font-weight: 600;\r\n}\r\n.center {\r\n    text-align: center;\r\n}\r\n/*\r\n#myProgress {\r\n  width: 100%;\r\n  background-color: #ddd;\r\n}\r\n\r\n#myBar {\r\n  width: 1%;\r\n  height: 30px;\r\n  background-color: #ff8000;\r\n}\r\n*/\r\n#barall {\r\npadding-top:130px;\r\n\r\n}\r\n.loader {\r\n  border: 16px solid #f3f3f3;\r\n  border-radius: 50%;\r\n  border-top: 16px solid #ff8000;\r\n  margin: auto;\r\n  width: 120px;\r\n  height: 120px;\r\n  -webkit-animation: spin 2s linear infinite; /* Safari */\r\n  animation: spin 2s linear infinite;\r\n}\r\n/* Safari */\r\n@-webkit-keyframes spin {\r\n  0% { -webkit-transform: rotate(0deg); }\r\n  100% { -webkit-transform: rotate(360deg); }\r\n}\r\n@keyframes spin {\r\n  0% { transform: rotate(0deg); }\r\n  100% { transform: rotate(360deg); }\r\n}\r\ndiv.bullet1 input {\r\n  background: url('outline-keyboard_arrow_right-24px.svg') no-repeat;\r\n  cursor: pointer;\r\n  width: 50px;\r\n  height: 50px;\r\n  border: none;\r\n}\r\nspan.bullet1 {\r\n  position: absolute;\r\n  top: 32px;\r\n  left: 2px;\r\n}\r\ndiv.bullet1 {\r\n  position: relative;\r\n  display: inline-block;\r\n}\r\ndiv.icon1 input {\r\n  background: url('sharp-local_dining-24px.svg') no-repeat;\r\n  cursor: pointer;\r\n  width: 50px;\r\n  height: 50px;\r\n  border: none;\r\n}\r\nspan.icon1 {\r\n  position: absolute;\r\n  top: 32px;\r\n  left: 2px;\r\n}\r\ndiv.icon1 {\r\n  position: relative;\r\n  display: inline-block;\r\n}\r\ndiv.icon2 input {\r\n  background: url('twotone-watch_later-24px.svg') no-repeat;\r\n  cursor: pointer;\r\n  width: 50px;\r\n  height: 50px;\r\n  border: none;\r\n}\r\nspan.icon2 {\r\n  position: absolute;\r\n  top: 32px;\r\n  left: 2px;\r\n}\r\ndiv.icon2 {\r\n  position: relative;\r\n  display: inline-block;\r\n}\r\n#round {\r\n  border-radius: 15px 50px 30px;\r\n /* border-radius: 15px 50px 30px;\r\n  background: #73AD21;*/\r\n  padding: 0px; \r\n  width: 90px;\r\n  height: 70px; \r\n}\r\n#move {\r\n \r\n  padding-left: 160px; \r\n  \r\n}\r\n#nameid {\r\n \r\n  padding-left: 145px; \r\n \r\n  \r\n}\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jYWxlbmRhci9jYWxlbmRhci5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7O0VBVUU7QUFDRjtJQUNJLDBCQUEwQjtJQUMxQixZQUFZO0lBQ1osWUFBWTtJQUNaLHlCQUF5QjtJQUN6QixrQkFBa0I7SUFDbEIscUJBQXFCO0lBQ3JCLHFCQUFxQjtJQUNyQixlQUFlO0lBQ2YsZ0JBQWdCO0lBQ2hCLGVBQWU7O0VBRWpCO0FBR0Y7R0FDRywwQkFBMEI7R0FDMUIsWUFBWTtHQUNaLFlBQVk7R0FDWix5QkFBeUI7R0FDekIsa0JBQWtCO0dBQ2xCLHFCQUFxQjtHQUNyQixxQkFBcUI7R0FDckIsZUFBZTtHQUNmLGdCQUFnQjtHQUNoQixlQUFlO0NBQ2pCO0FBQ0E7R0FDRSwwQkFBMEI7R0FDMUIsWUFBWTtHQUNaLFlBQVk7R0FDWix5QkFBeUI7R0FDekIsa0JBQWtCO0dBQ2xCLHFCQUFxQjtHQUNyQixxQkFBcUI7R0FDckIsZUFBZTtHQUNmLGdCQUFnQjtHQUNoQixlQUFlO0NBQ2pCO0FBRUE7R0FDRSwwQkFBMEI7R0FDMUIsWUFBWTtHQUNaLFlBQVk7R0FDWix5QkFBeUI7R0FDekIsa0JBQWtCO0dBQ2xCLHFCQUFxQjtHQUNyQixxQkFBcUI7R0FDckIsZUFBZTtHQUNmLGdCQUFnQjtHQUNoQixlQUFlO0NBQ2pCO0FBS0E7SUFDRyxtQkFBbUI7SUFDbkIsMEJBQTBCO0lBQzFCLFlBQVk7SUFDWixZQUFZO0lBQ1oseUJBQXlCO0lBQ3pCLGtCQUFrQjtJQUNsQixxQkFBcUI7SUFDckIscUJBQXFCO0lBQ3JCLGVBQWU7SUFDZixnQkFBZ0I7SUFDaEIsZUFBZTs7RUFFakI7QUFHRjtJQUNJLG1CQUFtQjtHQUNwQiwwQkFBMEI7R0FDMUIsWUFBWTtHQUNaLFlBQVk7R0FDWix5QkFBeUI7R0FDekIsa0JBQWtCO0dBQ2xCLHFCQUFxQjtHQUNyQixxQkFBcUI7R0FDckIsZUFBZTtHQUNmLGdCQUFnQjtHQUNoQixlQUFlO0NBQ2pCO0FBQ0E7SUFDRyxtQkFBbUI7R0FDcEIsMEJBQTBCO0dBQzFCLFlBQVk7R0FDWixZQUFZO0dBQ1oseUJBQXlCO0dBQ3pCLGtCQUFrQjtHQUNsQixxQkFBcUI7R0FDckIscUJBQXFCO0dBQ3JCLGVBQWU7R0FDZixnQkFBZ0I7R0FDaEIsZUFBZTtDQUNqQjtBQUVBO0lBQ0csbUJBQW1CO0dBQ3BCLDBCQUEwQjtHQUMxQixZQUFZO0dBQ1osWUFBWTtHQUNaLHlCQUF5QjtHQUN6QixrQkFBa0I7R0FDbEIscUJBQXFCO0dBQ3JCLHFCQUFxQjtHQUNyQixlQUFlO0dBQ2YsZ0JBQWdCO0dBQ2hCLGVBQWU7Q0FDakI7QUFDQTtJQUNHLG1CQUFtQjtJQUNuQiwwQkFBMEI7SUFDMUIsWUFBWTtJQUNaLFlBQVk7SUFDWix5QkFBeUI7SUFDekIsa0JBQWtCO0lBQ2xCLHFCQUFxQjtJQUNyQixxQkFBcUI7SUFDckIsZUFBZTtJQUNmLGdCQUFnQjtJQUNoQixlQUFlO0VBQ2pCO0FBR0E7S0FDRyxrQkFBa0I7S0FDbEIsZUFBZTtLQUNmLE9BQU87S0FDUCxTQUFTO0tBQ1QsV0FBVztLQUNYLDJCQUEyQjtLQUMzQixZQUFZO0tBQ1osa0JBQWtCO0VBQ3JCO0FBRUE7SUFDRSxrQkFBa0I7SUFDbEIscUJBQXFCO0VBQ3ZCO0FBRUE7SUFDRSxxQ0FBMkQ7SUFDM0QsZUFBZTtJQUNmLFdBQVc7SUFDWCxZQUFZO0lBQ1osWUFBWTtFQUNkO0FBQ0E7SUFDRSxrQkFBa0I7SUFDbEIscUJBQXFCO0VBQ3ZCO0FBRUE7SUFDRSx5Q0FBK0Q7SUFDL0QsZUFBZTtJQUNmLFdBQVc7SUFDWCxZQUFZO0lBQ1osWUFBWTtFQUNkO0FBRUE7SUFDRSxrQkFBa0I7SUFDbEIscUJBQXFCO0VBQ3ZCO0FBRUE7SUFDRSxtREFBMkU7SUFDM0UsZUFBZTtJQUNmLFdBQVc7SUFDWCxZQUFZO0lBQ1osWUFBWTtFQUNkO0FBRUE7SUFDRSxrQkFBa0I7SUFDbEIscUJBQXFCO0VBQ3ZCO0FBRUE7SUFDRSx5Q0FBK0Q7SUFDL0QsZUFBZTtJQUNmLFdBQVc7SUFDWCxZQUFZO0lBQ1osWUFBWTtFQUNkO0FBQ0E7SUFDRSxrQkFBa0I7SUFDbEIsU0FBUztJQUNULFNBQVM7RUFDWDtBQUVBO0lBQ0Usa0JBQWtCO0lBQ2xCLFNBQVM7SUFDVCxTQUFTO0VBQ1g7QUFFQTtJQUNFLGtCQUFrQjtJQUNsQixTQUFTO0lBQ1QsU0FBUztFQUNYO0FBRUE7SUFDRSxrQkFBa0I7SUFDbEIsU0FBUztJQUNULFVBQVU7RUFDWjtBQVFBO0lBQ0UsZ0JBQWdCO0lBQ2hCLHlCQUF5QjtJQUN6QixrQkFBa0I7SUFDbEIsa0JBQWtCOztFQUVwQjtBQUlBO0lBQ0Usa0JBQWtCO0lBQ2xCLHFCQUFxQjs7OztFQUl2QjtBQUVBO0lBQ0UsbURBQTJFO0lBQzNFLGVBQWU7SUFDZixXQUFXO0lBQ1gsWUFBWTtJQUNaLFlBQVk7RUFDZDtBQUVBO0lBQ0Usa0JBQWtCO0lBQ2xCLFNBQVM7SUFDVCxVQUFVO0VBQ1o7QUFNQTtJQUNFLGtCQUFrQjtJQUNsQixxQkFBcUI7Ozs7RUFJdkI7QUFFQTtJQUNFLG1EQUEyRTtJQUMzRSxlQUFlO0lBQ2YsV0FBVztJQUNYLFlBQVk7SUFDWixZQUFZO0VBQ2Q7QUFFQTtJQUNFLGtCQUFrQjtJQUNsQixTQUFTO0lBQ1QsVUFBVTtFQUNaO0FBS0E7SUFDRSxrQkFBa0I7SUFDbEIscUJBQXFCOzs7O0VBSXZCO0FBRUE7SUFDRSxtREFBMkU7SUFDM0UsZUFBZTtJQUNmLFdBQVc7SUFDWCxZQUFZO0lBQ1osWUFBWTtFQUNkO0FBRUE7SUFDRSxrQkFBa0I7SUFDbEIsU0FBUztJQUNULFVBQVU7RUFDWjtBQUtBO0lBQ0Usa0JBQWtCO0lBQ2xCLHFCQUFxQjs7OztFQUl2QjtBQUVBO0lBQ0UsbURBQTJFO0lBQzNFLGVBQWU7SUFDZixXQUFXO0lBQ1gsWUFBWTtJQUNaLFlBQVk7RUFDZDtBQUVBO0lBQ0Usa0JBQWtCO0lBQ2xCLFNBQVM7SUFDVCxVQUFVO0VBQ1o7QUFLQTtJQUNFLGtCQUFrQjtJQUNsQixxQkFBcUI7Ozs7RUFJdkI7QUFFQTtJQUNFLG1EQUEyRTtJQUMzRSxlQUFlO0lBQ2YsV0FBVztJQUNYLFlBQVk7SUFDWixZQUFZO0VBQ2Q7QUFFQTtJQUNFLGtCQUFrQjtJQUNsQixTQUFTO0lBQ1QsVUFBVTtFQUNaO0FBVUE7SUFDRSxnQkFBZ0I7SUFDaEIsV0FBVztJQUNYLGdCQUFnQjtJQUNoQixpQkFBaUI7SUFDakIsYUFBYTtBQUNqQjtBQUVBO0lBQ0ksYUFBYTtJQUNiLG1CQUFtQjtJQUNuQixrQkFBa0I7SUFDbEIsa0JBQWtCO0lBQ2xCLGtCQUFrQjtJQUNsQixlQUFlO0lBQ2YsZ0JBQWdCO0FBQ3BCO0FBRUE7SUFDSSxrQkFBa0I7QUFDdEI7QUFDQTs7Ozs7Ozs7Ozs7Q0FXQztBQUVEO0FBQ0EsaUJBQWlCOztBQUVqQjtBQUtBO0VBQ0UsMEJBQTBCO0VBQzFCLGtCQUFrQjtFQUNsQiw4QkFBOEI7RUFDOUIsWUFBWTtFQUNaLFlBQVk7RUFDWixhQUFhO0VBQ2IsMENBQTBDLEVBQUUsV0FBVztFQUN2RCxrQ0FBa0M7QUFDcEM7QUFFQSxXQUFXO0FBQ1g7RUFDRSxLQUFLLCtCQUErQixFQUFFO0VBQ3RDLE9BQU8saUNBQWlDLEVBQUU7QUFDNUM7QUFFQTtFQUNFLEtBQUssdUJBQXVCLEVBQUU7RUFDOUIsT0FBTyx5QkFBeUIsRUFBRTtBQUNwQztBQUdBO0VBQ0Usa0VBQXVGO0VBQ3ZGLGVBQWU7RUFDZixXQUFXO0VBQ1gsWUFBWTtFQUNaLFlBQVk7QUFDZDtBQUVBO0VBQ0Usa0JBQWtCO0VBQ2xCLFNBQVM7RUFDVCxTQUFTO0FBQ1g7QUFDQTtFQUNFLGtCQUFrQjtFQUNsQixxQkFBcUI7QUFDdkI7QUFDQTtFQUNFLHdEQUE2RTtFQUM3RSxlQUFlO0VBQ2YsV0FBVztFQUNYLFlBQVk7RUFDWixZQUFZO0FBQ2Q7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQixTQUFTO0VBQ1QsU0FBUztBQUNYO0FBQ0E7RUFDRSxrQkFBa0I7RUFDbEIscUJBQXFCO0FBQ3ZCO0FBS0E7RUFDRSx5REFBOEU7RUFDOUUsZUFBZTtFQUNmLFdBQVc7RUFDWCxZQUFZO0VBQ1osWUFBWTtBQUNkO0FBRUE7RUFDRSxrQkFBa0I7RUFDbEIsU0FBUztFQUNULFNBQVM7QUFDWDtBQUNBO0VBQ0Usa0JBQWtCO0VBQ2xCLHFCQUFxQjtBQUN2QjtBQUdBO0VBQ0UsNkJBQTZCO0NBQzlCO3VCQUNzQjtFQUNyQixZQUFZO0VBQ1osV0FBVztFQUNYLFlBQVk7QUFDZDtBQUdBOztFQUVFLG1CQUFtQjs7QUFFckI7QUFHQTs7RUFFRSxtQkFBbUI7OztBQUdyQiIsImZpbGUiOiJhcHAvY2FsZW5kYXIvY2FsZW5kYXIuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbiNteVByb2dyZXNzIHtcclxuICB3aWR0aDogMTAwJTtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZGRkO1xyXG59XHJcblxyXG4jbXlCYXIge1xyXG4gIHdpZHRoOiAxJTtcclxuICBoZWlnaHQ6IDMwcHg7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzRDQUY1MDtcclxufSovXHJcbiNidXR0b24xIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNiMmIyYjJcdDsgXHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMjVweCAyMnB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgZm9udC1zaXplOiAzNnB4O1xyXG4gICAgbWFyZ2luOiA0cHggMThweDtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIFxyXG4gIH1cclxuXHJcbiAgXHJcbiNidXR0b24yIHtcclxuICAgYmFja2dyb3VuZC1jb2xvcjogI2IyYjJiMlx0OyBcclxuICAgYm9yZGVyOiBub25lO1xyXG4gICBjb2xvcjogd2hpdGU7XHJcbiAgIHBhZGRpbmctYm90dG9tOiAyNXB4IDIycHg7XHJcbiAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgIGZvbnQtc2l6ZTogMzZweDtcclxuICAgbWFyZ2luOiA0cHggMThweDtcclxuICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gfVxyXG4gI2J1dHRvbjMge1xyXG4gICBiYWNrZ3JvdW5kLWNvbG9yOiAjYjJiMmIyXHQ7IFxyXG4gICBib3JkZXI6IG5vbmU7XHJcbiAgIGNvbG9yOiB3aGl0ZTtcclxuICAgcGFkZGluZy1ib3R0b206IDI1cHggMjJweDtcclxuICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgZm9udC1zaXplOiAzNnB4O1xyXG4gICBtYXJnaW46IDRweCAxOHB4O1xyXG4gICBjdXJzb3I6IHBvaW50ZXI7XHJcbiB9XHJcblxyXG4gI2J1dHRvbjQge1xyXG4gICBiYWNrZ3JvdW5kLWNvbG9yOiAjYjJiMmIyXHQ7XHJcbiAgIGJvcmRlcjogbm9uZTtcclxuICAgY29sb3I6IHdoaXRlO1xyXG4gICBwYWRkaW5nLWJvdHRvbTogMjVweCAyMnB4O1xyXG4gICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICBmb250LXNpemU6IDM2cHg7XHJcbiAgIG1hcmdpbjogNHB4IDE4cHg7XHJcbiAgIGN1cnNvcjogcG9pbnRlcjtcclxuIH1cclxuXHJcblxyXG5cclxuXHJcbiAjYnV0dG9uYSB7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2IyYjJiMlx0OyBcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIHBhZGRpbmctYm90dG9tOiAyNXB4IDIycHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBmb250LXNpemU6IDM2cHg7XHJcbiAgICBtYXJnaW46IDRweCAxOHB4O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgXHJcbiAgfVxyXG5cclxuICBcclxuI2J1dHRvbmIge1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcclxuICAgYmFja2dyb3VuZC1jb2xvcjogI2IyYjJiMlx0OyBcclxuICAgYm9yZGVyOiBub25lO1xyXG4gICBjb2xvcjogd2hpdGU7XHJcbiAgIHBhZGRpbmctYm90dG9tOiAyNXB4IDIycHg7XHJcbiAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgIGZvbnQtc2l6ZTogMzZweDtcclxuICAgbWFyZ2luOiA0cHggMThweDtcclxuICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gfVxyXG4gI2J1dHRvbmMge1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcclxuICAgYmFja2dyb3VuZC1jb2xvcjogI2IyYjJiMlx0OyBcclxuICAgYm9yZGVyOiBub25lO1xyXG4gICBjb2xvcjogd2hpdGU7XHJcbiAgIHBhZGRpbmctYm90dG9tOiAyNXB4IDIycHg7XHJcbiAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgIGZvbnQtc2l6ZTogMzZweDtcclxuICAgbWFyZ2luOiA0cHggMThweDtcclxuICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gfVxyXG5cclxuICNidXR0b25kIHtcclxuICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbiAgIGJhY2tncm91bmQtY29sb3I6ICNiMmIyYjJcdDtcclxuICAgYm9yZGVyOiBub25lO1xyXG4gICBjb2xvcjogd2hpdGU7XHJcbiAgIHBhZGRpbmctYm90dG9tOiAyNXB4IDIycHg7XHJcbiAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgIGZvbnQtc2l6ZTogMzZweDtcclxuICAgbWFyZ2luOiA0cHggMThweDtcclxuICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gfVxyXG4gI2J1dHRvbmUge1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNiMmIyYjJcdDtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIHBhZGRpbmctYm90dG9tOiAyNXB4IDIycHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBmb250LXNpemU6IDM2cHg7XHJcbiAgICBtYXJnaW46IDRweCAxOHB4O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gIH1cclxuICBcclxuIFxyXG4gICNhbGx7XHJcbiAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgIHBvc2l0aW9uOiBmaXhlZDtcclxuICAgICBsZWZ0OiAwO1xyXG4gICAgIGJvdHRvbTogMDtcclxuICAgICB3aWR0aDogMTAwJTtcclxuICAgICBiYWNrZ3JvdW5kLWNvbG9yOiBcdFx0I2IyYjJiMjtcclxuICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIH1cclxuXHJcbiAgZGl2LmhvbWUge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIH1cclxuXHJcbiAgZGl2LmhvbWUgaW5wdXQge1xyXG4gICAgYmFja2dyb3VuZDogdXJsKCdzcmMvYXBwL2hlYWRlci9pbWFnZXMvaG9tZS5zdmcnKSBuby1yZXBlYXQ7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICB3aWR0aDogNTBweDtcclxuICAgIGhlaWdodDogNTBweDtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICB9XHJcbiAgZGl2LmFuYWx5dGljIHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICB9XHJcblxyXG4gIGRpdi5hbmFseXRpYyBpbnB1dCB7XHJcbiAgICBiYWNrZ3JvdW5kOiB1cmwoJ3NyYy9hcHAvaGVhZGVyL2ltYWdlcy9hbmFseXRpYy5zdmcnKSBuby1yZXBlYXQ7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICB3aWR0aDogNTBweDtcclxuICAgIGhlaWdodDogNTBweDtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICB9XHJcblxyXG4gIGRpdi5ub3RpZmljYXRpb24ge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIH1cclxuXHJcbiAgZGl2Lm5vdGlmaWNhdGlvbiBpbnB1dCB7XHJcbiAgICBiYWNrZ3JvdW5kOiB1cmwoJ3NyYy9hcHAvY2FsZW5kYXIvaW1hZ2VzL291dGxpbmUtdG9kYXktMjRweC5zdmcnKSBuby1yZXBlYXQ7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICB3aWR0aDogNTBweDtcclxuICAgIGhlaWdodDogNTBweDtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICB9XHJcbiAgXHJcbiAgZGl2LnNldHRpbmcge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIH1cclxuXHJcbiAgZGl2LnNldHRpbmcgaW5wdXQge1xyXG4gICAgYmFja2dyb3VuZDogdXJsKCdzcmMvYXBwL2hlYWRlci9pbWFnZXMvc2V0dGluZ3Muc3ZnJykgbm8tcmVwZWF0O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgd2lkdGg6IDUwcHg7XHJcbiAgICBoZWlnaHQ6IDUwcHg7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgfVxyXG4gIHNwYW4uaG9tZSB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDMycHg7XHJcbiAgICBsZWZ0OiA2cHg7XHJcbiAgfVxyXG4gIFxyXG4gIHNwYW4uYW5hbHl0aWMge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAzMnB4O1xyXG4gICAgbGVmdDogMnB4O1xyXG4gIH1cclxuXHJcbiAgc3Bhbi5ub3RpZmljYXRpb24ge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAzMnB4O1xyXG4gICAgbGVmdDogMnB4O1xyXG4gIH1cclxuXHJcbiAgc3Bhbi5zZXR0aW5nIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogMzJweDtcclxuICAgIGxlZnQ6IDEycHg7XHJcbiAgfVxyXG5cclxuXHJcbiAgXHJcblxyXG5cclxuXHJcblxyXG4gIC5oZWFkZXJjYWxlbmRhciB7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2YyZjJmMjtcclxuICAgIHBhZGRpbmc6IDEwcHggNTBweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIFxyXG4gIH1cclxuXHJcblxyXG5cclxuICBkaXYuZGF5MSB7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBcclxuICAgIFxyXG4gICAgXHJcbiAgfVxyXG5cclxuICBkaXYuZGF5MSBpbnB1dCB7XHJcbiAgICBiYWNrZ3JvdW5kOiB1cmwoJ3NyYy9hcHAvY2FsZW5kYXIvaW1hZ2VzL291dGxpbmUtdG9kYXktMjRweC5zdmcnKSBuby1yZXBlYXQ7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICB3aWR0aDogNTBweDtcclxuICAgIGhlaWdodDogNzBweDtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICB9XHJcblxyXG4gIHNwYW4uZGF5MSB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDI4cHg7XHJcbiAgICBsZWZ0OiAyOHB4O1xyXG4gIH1cclxuXHJcblxyXG5cclxuXHJcbiAgXHJcbiAgZGl2LmRheTIge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgXHJcbiAgICBcclxuICAgIFxyXG4gIH1cclxuXHJcbiAgZGl2LmRheTIgaW5wdXQge1xyXG4gICAgYmFja2dyb3VuZDogdXJsKCdzcmMvYXBwL2NhbGVuZGFyL2ltYWdlcy9vdXRsaW5lLXRvZGF5LTI0cHguc3ZnJykgbm8tcmVwZWF0O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgd2lkdGg6IDUwcHg7XHJcbiAgICBoZWlnaHQ6IDcwcHg7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgfVxyXG5cclxuICBzcGFuLmRheTIge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAyOHB4O1xyXG4gICAgbGVmdDogMjhweDtcclxuICB9XHJcblxyXG5cclxuXHJcbiAgXHJcbiAgZGl2LmRheTMge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgXHJcbiAgICBcclxuICAgIFxyXG4gIH1cclxuXHJcbiAgZGl2LmRheTMgaW5wdXQge1xyXG4gICAgYmFja2dyb3VuZDogdXJsKCdzcmMvYXBwL2NhbGVuZGFyL2ltYWdlcy9vdXRsaW5lLXRvZGF5LTI0cHguc3ZnJykgbm8tcmVwZWF0O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgd2lkdGg6IDUwcHg7XHJcbiAgICBoZWlnaHQ6IDcwcHg7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgfVxyXG5cclxuICBzcGFuLmRheTMge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAyOHB4O1xyXG4gICAgbGVmdDogMjhweDtcclxuICB9XHJcblxyXG5cclxuXHJcbiAgXHJcbiAgZGl2LmRheTQge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgXHJcbiAgICBcclxuICAgIFxyXG4gIH1cclxuXHJcbiAgZGl2LmRheTQgaW5wdXQge1xyXG4gICAgYmFja2dyb3VuZDogdXJsKCdzcmMvYXBwL2NhbGVuZGFyL2ltYWdlcy9vdXRsaW5lLXRvZGF5LTI0cHguc3ZnJykgbm8tcmVwZWF0O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgd2lkdGg6IDUwcHg7XHJcbiAgICBoZWlnaHQ6IDcwcHg7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgfVxyXG5cclxuICBzcGFuLmRheTQge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAyOHB4O1xyXG4gICAgbGVmdDogMjhweDtcclxuICB9XHJcblxyXG5cclxuXHJcbiBcclxuICBkaXYuZGF5NSB7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBcclxuICAgIFxyXG4gICAgXHJcbiAgfVxyXG5cclxuICBkaXYuZGF5NSBpbnB1dCB7XHJcbiAgICBiYWNrZ3JvdW5kOiB1cmwoJ3NyYy9hcHAvY2FsZW5kYXIvaW1hZ2VzL291dGxpbmUtdG9kYXktMjRweC5zdmcnKSBuby1yZXBlYXQ7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICB3aWR0aDogNTBweDtcclxuICAgIGhlaWdodDogNzBweDtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICB9XHJcblxyXG4gIHNwYW4uZGF5NSB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDI4cHg7XHJcbiAgICBsZWZ0OiAyOHB4O1xyXG4gIH1cclxuXHJcblxyXG5cclxuXHJcblxyXG5cclxuXHJcblxyXG4gIFxyXG4gIHVsIHtcclxuICAgIG1heC13aWR0aDogNjAwcHg7XHJcbiAgICBjb2xvcjogIzMzMztcclxuICAgIGxpc3Qtc3R5bGU6IG5vbmU7XHJcbiAgICBtYXJnaW46IDUwcHggYXV0bztcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbn1cclxuXHJcbi5saXN0IGxpIHtcclxuICAgIHBhZGRpbmc6IDI1cHg7XHJcbiAgICBiYWNrZ3JvdW5kOiAjZjJmMmYyO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogNXB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC1zaXplOiAzMnB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcclxufVxyXG5cclxuLmNlbnRlciB7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuLypcclxuI215UHJvZ3Jlc3Mge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNkZGQ7XHJcbn1cclxuXHJcbiNteUJhciB7XHJcbiAgd2lkdGg6IDElO1xyXG4gIGhlaWdodDogMzBweDtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmY4MDAwO1xyXG59XHJcbiovXHJcbiAgXHJcbiNiYXJhbGwge1xyXG5wYWRkaW5nLXRvcDoxMzBweDtcclxuXHJcbn1cclxuICBcclxuXHJcblxyXG5cclxuLmxvYWRlciB7XHJcbiAgYm9yZGVyOiAxNnB4IHNvbGlkICNmM2YzZjM7XHJcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gIGJvcmRlci10b3A6IDE2cHggc29saWQgI2ZmODAwMDtcclxuICBtYXJnaW46IGF1dG87XHJcbiAgd2lkdGg6IDEyMHB4O1xyXG4gIGhlaWdodDogMTIwcHg7XHJcbiAgLXdlYmtpdC1hbmltYXRpb246IHNwaW4gMnMgbGluZWFyIGluZmluaXRlOyAvKiBTYWZhcmkgKi9cclxuICBhbmltYXRpb246IHNwaW4gMnMgbGluZWFyIGluZmluaXRlO1xyXG59XHJcblxyXG4vKiBTYWZhcmkgKi9cclxuQC13ZWJraXQta2V5ZnJhbWVzIHNwaW4ge1xyXG4gIDAlIHsgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKTsgfVxyXG4gIDEwMCUgeyAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDM2MGRlZyk7IH1cclxufVxyXG5cclxuQGtleWZyYW1lcyBzcGluIHtcclxuICAwJSB7IHRyYW5zZm9ybTogcm90YXRlKDBkZWcpOyB9XHJcbiAgMTAwJSB7IHRyYW5zZm9ybTogcm90YXRlKDM2MGRlZyk7IH1cclxufVxyXG5cclxuXHJcbmRpdi5idWxsZXQxIGlucHV0IHtcclxuICBiYWNrZ3JvdW5kOiB1cmwoJ3NyYy9hcHAvaW5mb3MvaW1hZ2VzL291dGxpbmUta2V5Ym9hcmRfYXJyb3dfcmlnaHQtMjRweC5zdmcnKSBuby1yZXBlYXQ7XHJcbiAgY3Vyc29yOiBwb2ludGVyO1xyXG4gIHdpZHRoOiA1MHB4O1xyXG4gIGhlaWdodDogNTBweDtcclxuICBib3JkZXI6IG5vbmU7XHJcbn1cclxuXHJcbnNwYW4uYnVsbGV0MSB7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHRvcDogMzJweDtcclxuICBsZWZ0OiAycHg7XHJcbn1cclxuZGl2LmJ1bGxldDEge1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbn1cclxuZGl2Lmljb24xIGlucHV0IHtcclxuICBiYWNrZ3JvdW5kOiB1cmwoJ3NyYy9hcHAvaW5mb3MvaW1hZ2VzL3NoYXJwLWxvY2FsX2RpbmluZy0yNHB4LnN2ZycpIG5vLXJlcGVhdDtcclxuICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgd2lkdGg6IDUwcHg7XHJcbiAgaGVpZ2h0OiA1MHB4O1xyXG4gIGJvcmRlcjogbm9uZTtcclxufVxyXG5cclxuc3Bhbi5pY29uMSB7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHRvcDogMzJweDtcclxuICBsZWZ0OiAycHg7XHJcbn1cclxuZGl2Lmljb24xIHtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG59XHJcblxyXG5cclxuXHJcblxyXG5kaXYuaWNvbjIgaW5wdXQge1xyXG4gIGJhY2tncm91bmQ6IHVybCgnc3JjL2FwcC9pbmZvcy9pbWFnZXMvdHdvdG9uZS13YXRjaF9sYXRlci0yNHB4LnN2ZycpIG5vLXJlcGVhdDtcclxuICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgd2lkdGg6IDUwcHg7XHJcbiAgaGVpZ2h0OiA1MHB4O1xyXG4gIGJvcmRlcjogbm9uZTtcclxufVxyXG5cclxuc3Bhbi5pY29uMiB7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHRvcDogMzJweDtcclxuICBsZWZ0OiAycHg7XHJcbn1cclxuZGl2Lmljb24yIHtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG59XHJcblxyXG5cclxuI3JvdW5kIHtcclxuICBib3JkZXItcmFkaXVzOiAxNXB4IDUwcHggMzBweDtcclxuIC8qIGJvcmRlci1yYWRpdXM6IDE1cHggNTBweCAzMHB4O1xyXG4gIGJhY2tncm91bmQ6ICM3M0FEMjE7Ki9cclxuICBwYWRkaW5nOiAwcHg7IFxyXG4gIHdpZHRoOiA5MHB4O1xyXG4gIGhlaWdodDogNzBweDsgXHJcbn1cclxuXHJcblxyXG4jbW92ZSB7XHJcbiBcclxuICBwYWRkaW5nLWxlZnQ6IDE2MHB4OyBcclxuICBcclxufVxyXG5cclxuXHJcbiNuYW1laWQge1xyXG4gXHJcbiAgcGFkZGluZy1sZWZ0OiAxNDVweDsgXHJcbiBcclxuICBcclxufVxyXG5cclxuIl19 */"

/***/ }),

/***/ "./src/app/calendar/calendar.component.html":
/*!**************************************************!*\
  !*** ./src/app/calendar/calendar.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n\n<link  href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css\"  rel=\"stylesheet\">\n\n\n<!--\n<div id=\"main\">\n <nav id=\"header\" class=\"navbar navbar-expand-lg navbar-dark bg-dark\">\n  <a class=\"navbar-brand\" href=\"#\">\n    FANI\n  </a>\n\n  <button class=\"navbar-toggler\" type=\"button\"\n          (click)=\"toggleNavbar()\">\n    <span class=\"navbar-toggler-icon\"></span>\n  </button>\n\n\n  <div class=\"collapse navbar-collapse\"\n       [ngClass]=\"{ 'show': navbarOpen }\">\n    <ul class=\"navbar-nav mr-auto\">\n      <li class=\"nav-item\">\n        <a class=\"nav-link\"routerLink=\"/signup\">calendar</a>\n      </li>\n\n      <li class=\"nav-item\">\n        <a class=\"nav-link\" href=\"#\">logout</a>\n      </li>\n    </ul>\n  </div>\n </nav>\n\n</div>-->\n\n\n\n <!-- <div class=\"container\">\n            \n      <div class=\"row\">\n          \n         <div class=\"col-sm-12\" style=\"\">\n            <div id=\"myProgress\">\n                <div id=\"myBar\"></div>\n              </div>\n          </div>\n          </div>\n    </div>-->\n\n    <div class=\"startdate\">\n\n        <h1 id=\"headcalendar\" align=\"center\" style=\"color:#262626;\">Weekly plan</h1>\n\n      </div>\n\n      <div class=\"container\">\n      <div  id=\"barall\" class=\"row\">\n          <div class=\"col-md-12\">\n            <div  align=\"center\"   id=\"loading\"> Loading...</div>\n             <!-- <div id=\"myProgress\">\n                 \n                 <div id=\"myBar\"></div>\n              </div>-->\n              <div id=\"loader\" align=\"center\" class=\"loader\"></div>\n          </div>\n        </div>\n      </div>\n\n<div id=\"headercalendar\">\n    <div class=\"headercalendar\" >\n     \n    <div class=\"day1\" id=\"dayall\">\n        <span id=\"day1\" class=\"day1\">Day1  </span>\n        <input id=\"buttona\" type=\"button\"  (click)=\"submitted=false\" (click)=\"goToPage('/', 'calendar','monday')\">\n      \n    </div>\n\n\n\n    <div class=\"day2\">\n      <span id=\"day2\" class=\"day2\">Day2  </span>\n      <input id=\"buttonb\" type=\"button\"  (click)=\"submitted=false\" (click)=\"goToPage('/', 'main','tuesday')\">\n    \n    </div>\n\n\n    <div class=\"day3\">\n      <span id=\"day3\" class=\"day3\">Day3  </span>\n      <input id=\"buttonc\" type=\"button\"  (click)=\"submitted=false\" (click)=\"goToPage('/', 'main','wednesday')\">\n    \n    </div>\n\n\n\n\n    <div class=\"day4\">\n      <span id=\"day4\" class=\"day4\">Day4  </span>\n      <input id=\"buttond\" type=\"button\"  (click)=\"submitted=false\" (click)=\"goToPage('/', 'main','thursday')\">\n    \n  </div>\n\n\n  <div class=\"day5\">\n    <span id=\"day5\" class=\"day5\">Day5  </span>\n    <input id=\"buttone\" type=\"button\"  (click)=\"submitted=false\" (click)=\"goToPage('/', 'main','friday')\">\n  \n </div>\n  \n    </div>\n</div>    \n  \n<div id=\"maincontenthome\">\n  \n\n    <div id=\"menu\"> <th>Menu</th></div>\n\n<div class=\"container\">\n  <!--  <ul class=\"list\">\n        <cdk-virtual-scroll-viewport  style=\"height: 500px\" itemSize=\"90\" >\n          <ng-container   *cdkVirtualFor=\"let n of data\" >\n              <h3 style=\"color:blue;\">{{n._id}}</h3>\n              <li> {{\n             \n              \n                n.name\n              \n              }} </li>\n\n          \n          </ng-container>\n        \n          \n        </cdk-virtual-scroll-viewport>\n      </ul>-->\n    <table class=\"table table-striped\">\n      <thead>\n          <tr>\n          \n           \n           \n          </tr>\n      </thead>\n      <tbody>\n        <tr *ngFor =\"let d of data; \"  (click)=\"onSelect(d.name)\">\n            \n          <td>\n\n              <div class=\"container\">\n                  <div class=\"row\">\n                    <div class=\"col-md\">\n                      \n                    </div>\n                    <div class=\"col-md\">\n                        \n                    </div>\n                   \n                   \n                  </div>\n              </div>\n             \n             \n             \n              <div class=\"container\">\n                <div class=\"row\">\n                  <div class=\"col-xs\">\n                    <img align=\"left\" id=\"round\"  src={{d.imageurl}}>\n                  </div>\n                  <div class=\"col-xs\">\n                      <h6 style=\"color:blue;\">{{d.type}}</h6>\n                    <h5 id=\"nameid\"   style=\"color:grey;\"> {{d.name}} </h5>\n                     \n\n\n                   \n\n\n                    <div class=\"container\" id=\"move\">\n                        <div class=\"row\">\n\n                            <div class=\"col-xs\">\n                                \n                              \n                              </div>\n                          \n                            <div class=\"col-xs\">\n                                <div id=\"bullets\">\n                                    <div class=\"icon2\">\n                                        <span id=\"icon2\" class=\"icon2\">  </span>\n                                         <input id=\"icon2\" type=\"button\" width=\"48\" height=\"48\" >\n                                    </div>\n                  \n                  \n                  \n                                </div> \n                              </div>\n                    <div class=\"col-xs\">\n                        <div id=\"bullets\" >\n                            <div class=\"icon1\">\n                                <span id=\"icon1\" class=\"icon1\">  </span>\n                                 <input id=\"icon1\" type=\"button\" width=\"48\" height=\"48\" >\n                            </div>\n          \n      \n                        </div>  \n                      </div>\n                      <div class=\"col-xs\">\n                         \n                      </div>\n  \n\n                        </div>\n                    </div>\n                  </div>\n                  \n                </div>\n              </div>\n               \n            \n               \n           \n          \n          \n         \n           \n        </tr>\n      </tbody>\n    </table>\n  </div>\n  \n</div>\n\n\n<div id=\"all\">\n\n  \n    <div class=\"home\">\n        <span id=\"home\" class=\"home\">Home  </span>\n        <input id=\"button1\" type=\"button\"  (click)=\"submitted=false\" (click)=\"goToPageMenu('/', 'main')\">\n      \n    </div>\n\n     <div class=\"analytic\">\n      <span id=\"analytic\" class=\"analytic\">Analytics  </span>\n      <input id=\"button2\" type=\"button\" alt=\"Submit\" width=\"48\" height=\"48\" (click)=\"submitted=false\" (click)=\"goToPageMenu('/', 'analytic')\">\n  \n    </div>\n\n    <div class=\"notification\">\n        <span id=\"notification\" class=\"notification\">Calendar  </span>\n        <input id=\"button3\" type=\"button\" alt=\"Submit\" width=\"48\" height=\"48\" (click)=\"submitted=false\" (click)=\"goToPageMenu('/', 'calendar')\">\n    \n      </div>\n    \n      <div class=\"setting\">\n          <span id=\"setting\" class=\"setting\">Settings  </span>\n          <input id=\"button4\" type=\"button\" alt=\"Submit\" width=\"48\" height=\"48\" (click)=\"submitted=false\" (click)=\"goToPageMenu('/', 'settings')\">\n      \n        </div>\n   \n     \n \n</div>\n\n\n<!--\n<div class=\"container\">\n  <div class=\"row\">\n    <div class=\"col-sm\">\n      <button class=\"button\">Green</button>\n    </div>\n    <div class=\"col-sm\">\n      <button class=\"button\">Green</button>\n    </div>\n    <div class=\"col-sm\">\n        <button class=\"button\">Green</button>\n     \n    </div>\n\n    <div class=\"col-sm\">\n        <button class=\"button\">Green</button>\n     \n    </div>\n\n   \n  </div>\n</div>\n-->\n\n\n"

/***/ }),

/***/ "./src/app/calendar/calendar.component.ts":
/*!************************************************!*\
  !*** ./src/app/calendar/calendar.component.ts ***!
  \************************************************/
/*! exports provided: CalendarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CalendarComponent", function() { return CalendarComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./src/node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _calendar_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./calendar.service */ "./src/app/calendar/calendar.service.ts");






var CalendarComponent = /** @class */ (function () {
    function CalendarComponent(http, router, CalendarService) {
        this.http = http;
        this.router = router;
        this.CalendarService = CalendarService;
        this.url = "http://139.91.200.103:3000/api/things/getmenu/breakfast/";
        this.urlinter1 = "http://139.91.200.103:3000/api/things/getmenu/inter1/";
        this.urlunch = "http://139.91.200.103:3000/api/things/getmenu/lunch/";
        this.urlinter2 = "http://139.91.200.103:3000/api/things/getmenu/inter2/";
        this.urldinner = "http://139.91.200.103:3000/api/things/getmenu/dinner/";
        this.numbers = [];
        this.navbarOpen = false;
        for (var index = 0; index < 10000; index++) {
            this.numbers.push(index);
        }
    }
    CalendarComponent.prototype.onSelect = function (hero) {
        //alert("hier"+hero);
        this.selectedHero = hero;
        // $("#barall").show();
        this.goToPageMenu('/infos', '&name=' + hero + '');
        /*  if(hero=="Cod fresh"){
            //alert("hier"+hero);
            this.goToPageMenu('/infos','&name=Cod fresh');
      
          }else if(hero=="Biscuit"){
            this.goToPageMenu('/infos','&name=Biscuit');
      
          }else if(hero=="Beef"){
      
            this.goToPageMenu('/infos','&name=Beef');
      
      
          }else if(hero=="Naan bread "){
          //  alert("hier"+hero);
            this.goToPageMenu('/infos','&name=Naan bread ');
      
          }else if(hero=="Muesli"){
           // alert("hier"+hero);
            this.goToPageMenu('/infos','&name=Muesli');
      
          }else if(hero=="Jaffa cake"){
            // alert("hier"+String(hero).trim());
             this.goToPageMenu('/infos','&name=Jaffa cake');
       
           }else if(hero=="Vanilla"){
            // alert("hier"+String(hero).trim());
             this.goToPageMenu('/infos','&name=Vanilla');
       
           }else if(hero=="Chapatis"){
            // alert("hier"+String(hero).trim());
             this.goToPageMenu('/infos','&name=Chapatis');
       
           }else if(hero=="Bananas"){
            // alert("hier"+String(hero).trim());
             this.goToPageMenu('/infos','&name=Bananas');
       
           }else if(hero=="Cornflakes with milk"){
            // alert("hier"+String(hero).trim());
             this.goToPageMenu('/infos','&name=Cornflakes with milk');
       
           }else if(hero=="Nuts"){
            // alert("hier"+String(hero).trim());
             this.goToPageMenu('/infos','&name=Nuts');
       
           }else if(hero=="Chicken"){
            // alert("hier"+String(hero).trim());
             this.goToPageMenu('/infos','&name=Chicken');
       
           }else if(hero=="Mashrooms"){
            // alert("hier"+String(hero).trim());
             this.goToPageMenu('/infos','&name=Mashrooms');
       
           }else if(hero=="Potatoes"){
            // alert("hier"+String(hero).trim());
             this.goToPageMenu('/infos','&name=Potatoes');
       
           }*/
    };
    CalendarComponent.prototype.ngOnInit = function () {
        var _this = this;
        jquery__WEBPACK_IMPORTED_MODULE_3__("#headercalendar").hide();
        jquery__WEBPACK_IMPORTED_MODULE_3__("#menu").hide();
        // $("#headcalendar").hide();
        //$("#session").hide();
        //$("#back").show();
        //$("#date").hide();
        document.getElementById("notification").style.backgroundColor = "#696969";
        function delay(ms) {
            return new Promise(function (resolve) { return setTimeout(resolve, ms); });
        }
        //document.getElementById("day1").style.color = "black";
        var width = 1;
        // alert("datieee are hier!");
        // alert(this.username);
        // alert(this.email);
        // alert(this.password);
        // alert(this.height);
        // alert(this.weight);
        /*var elem = document.getElementById("myBar");
         
        var id = setInterval(frame, 10);
        function frame() {
          //alert("frame");
          if (width >= 100) {
          //  alert("called");
            clearInterval(id);
           
           // alert("alles");
           
            
          } else {
            //alert("Kabled width functionarie");
            width++;
            elem.style.width = width + '%';
          }
        }*/
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        var dayv = +dd;
        var daysv = dayv;
        var todaysv = mm + '/' + daysv + '/' + yyyy;
        var day1 = String(dayv);
        var day2 = String(dayv + 1);
        var day3 = String(dayv + 2);
        var day4 = String(dayv + 3);
        var day5 = String(dayv + 4);
        var day6 = String(dayv + 5);
        jquery__WEBPACK_IMPORTED_MODULE_3__('#alllist').show();
        for (var index = 0; index < 10000; index++) {
            this.numbers.push(index);
        }
        document.getElementById("day1").style.color = "#000000";
        document.getElementById("day1").textContent = "Mo";
        document.getElementById("day2").style.color = "#ff0000";
        document.getElementById("day2").textContent = "Tu";
        document.getElementById("day3").style.color = "#ff0000";
        document.getElementById("day3").textContent = "We";
        document.getElementById("day4").style.color = "#ff0000";
        document.getElementById("day4").textContent = "Th";
        document.getElementById("day5").style.color = "#ff0000";
        document.getElementById("day5").textContent = "Fr";
        //document.getElementById("day5").style.color = "#ff0000";
        //document.getElementById("day5").textContent=day1;
        /* this.http.get('http://127.0.0.1:3000/api/things',{responseType:"json"}).subscribe(
           response => {
               this.data = response;
               console.log("data :"+response);
               var sample=JSON.stringify(response);
          });*/
        // http://localhost:3000/api/things/getsensorvalues/niko
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        var day = +dd;
        var days = day;
        var todays = mm + '/' + days + '/' + yyyy;
        //  headcalendar
        document.getElementById("headcalendar").textContent = todays;
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var name;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(1000)];
                    case 1:
                        _a.sent();
                        jquery__WEBPACK_IMPORTED_MODULE_3__("#barall").hide();
                        jquery__WEBPACK_IMPORTED_MODULE_3__("#headercalendar").show();
                        jquery__WEBPACK_IMPORTED_MODULE_3__("#menu").show();
                        name = document.getElementById("session").innerHTML = sessionStorage.getItem("lastname");
                        this.http.get("http://139.91.200.103:3000/api/things/getcalendar/" + name + "/" + 'monday', { responseType: "json" }).subscribe(function (response) {
                            _this.data = response;
                            console.log("data :" + response);
                            var sample = JSON.stringify(response);
                            var js = JSON.parse(sample);
                            // alert(js[0].imageurl);
                            // alert(sample);
                        });
                        return [2 /*return*/];
                }
            });
        }); })();
    };
    CalendarComponent.prototype.toggleNavbar = function () {
        this.navbarOpen = !this.navbarOpen;
    };
    CalendarComponent.prototype.goToPageMenu = function (pagename, parameter) {
        this.router.navigate([pagename, parameter]);
    };
    CalendarComponent.prototype.goToPage = function (pagename, parameter, day) {
        var _this = this;
        var name = sessionStorage.getItem("lastname");
        var daymenu = day;
        if (day == "monday") {
            // alert("kablid");
            jquery__WEBPACK_IMPORTED_MODULE_3__("#day").hide();
            document.getElementById("day1").style.color = "#000000";
            document.getElementById("day2").style.color = "#ff0000";
            document.getElementById("day3").style.color = "#ff0000";
            document.getElementById("day4").style.color = "#ff0000";
            document.getElementById("day5").style.color = "#ff0000";
            // document.getElementById("day1").textContent.italics="Mxsxso"
            this.http.get("http://139.91.200.103:3000/api/things/getcalendar/" + name + "/" + daymenu, { responseType: "json" }).subscribe(function (response) {
                _this.data = response;
                console.log("data :" + response);
                var sample = JSON.stringify(response);
            });
        }
        else if (day == "tuesday") {
            document.getElementById("day1").style.color = "#ff0000";
            document.getElementById("day2").style.color = "#000000";
            document.getElementById("day3").style.color = "#ff0000";
            document.getElementById("day4").style.color = "#ff0000";
            document.getElementById("day5").style.color = "#ff0000";
            this.http.get("http://139.91.200.103:3000/api/things/getcalendar/" + name + "/tuesday", { responseType: "json" }).subscribe(function (response) {
                _this.data = response;
                console.log("data :" + response);
                var sample = JSON.stringify(response);
            });
        }
        else if (day == "wednesday") {
            document.getElementById("day1").style.color = "#ff0000";
            document.getElementById("day2").style.color = "#ff0000";
            document.getElementById("day3").style.color = "#000000";
            document.getElementById("day4").style.color = "#ff0000";
            document.getElementById("day5").style.color = "#ff0000";
            this.http.get("http://139.91.200.103:3000/api/things/getcalendar/" + name + "/wednesday", { responseType: "json" }).subscribe(function (response) {
                _this.data = response;
                console.log("data :" + response);
                var sample = JSON.stringify(response);
            });
        }
        else if (day == "thursday") {
            document.getElementById("day1").style.color = "#ff0000";
            document.getElementById("day2").style.color = "#ff0000";
            document.getElementById("day3").style.color = "#ff0000";
            document.getElementById("day4").style.color = "#000000";
            document.getElementById("day5").style.color = "#ff0000";
            this.http.get("http://139.91.200.103:3000/api/things/getcalendar/" + name + "/thursday", { responseType: "json" }).subscribe(function (response) {
                _this.data = response;
                console.log("data :" + response);
                var sample = JSON.stringify(response);
            });
        }
        else if (day == "friday") {
            document.getElementById("day1").style.color = "#ff0000";
            document.getElementById("day2").style.color = "#ff0000";
            document.getElementById("day3").style.color = "#ff0000";
            document.getElementById("day4").style.color = "#ff0000";
            document.getElementById("day5").style.color = "#000000";
            this.http.get("http://139.91.200.103:3000/api/things/getcalendar/" + name + "/friday", { responseType: "json" }).subscribe(function (response) {
                _this.data = response;
                console.log("data :" + response);
                var sample = JSON.stringify(response);
            });
        }
        //this.router.navigate([pagename, parameter]);
    };
    CalendarComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-calendar',
            template: __webpack_require__(/*! ./calendar.component.html */ "./src/app/calendar/calendar.component.html"),
            styles: [__webpack_require__(/*! ./calendar.component.css */ "./src/app/calendar/calendar.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _calendar_service__WEBPACK_IMPORTED_MODULE_5__["CalendarService"]])
    ], CalendarComponent);
    return CalendarComponent;
}());



/***/ }),

/***/ "./src/app/calendar/calendar.service.ts":
/*!**********************************************!*\
  !*** ./src/app/calendar/calendar.service.ts ***!
  \**********************************************/
/*! exports provided: CalendarService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CalendarService", function() { return CalendarService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./src/node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");



var CalendarService = /** @class */ (function () {
    function CalendarService(httpClient) {
        this.httpClient = httpClient;
        this.apiURL = 'http://139.91.200.103:3000/api/things/setcalendarmenu';
        this.apiURLDELETE = 'http://139.91.200.103:3000/api/things/';
    }
    CalendarService.prototype.delete = function (username) {
        var data = {
            "usename": "Emily"
        };
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
            })
        };
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
            .set('username', "Emily");
        var options = {
            httpOptions: httpOptions,
            //   params,
            withCredentials: false
        };
        var name = "Emily";
        return this.httpClient.delete("http://127.0.0.1:3000/api/things/Emily");
    };
    CalendarService.prototype.setTempCalendar = function (username, name, type, energycontent, wayofcooking, nutrition, calories, kg, info, date, imageurl) {
        var data = {
            "name": "fr"
        };
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
            })
        };
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
            .set('username', String(username))
            .set('name', String(name))
            .set('type', String(type))
            .set('energycontent', String(energycontent))
            .set('wayofcooking', String(wayofcooking))
            .set('nutrition', String(nutrition))
            .set('calories', String(calories))
            .set('kg', String(kg))
            .set('info', String(info))
            .set('date', String(date))
            .set('imageurl', String(imageurl));
        var options = {
            httpOptions: httpOptions,
            params: params,
            withCredentials: false
        };
        return this.httpClient.post("" + this.apiURL, null, options);
    };
    CalendarService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], CalendarService);
    return CalendarService;
}());



/***/ }),

/***/ "./src/app/error/error.component.css":
/*!*******************************************!*\
  !*** ./src/app/error/error.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@import url('https://fonts.googleapis.com/css?family=Cabin+Sketch');\r\n\r\nhtml {\r\n\theight: 100%;\r\n}\r\n\r\nbody {\r\n\tmin-height: 100%;\r\n}\r\n\r\nbody {\r\n\tdisplay: flex;\r\n}\r\n\r\nh1 {\r\n\tfont-family: 'Cabin Sketch', cursive;\r\n\tfont-size: 3em;\r\n\ttext-align: center;\r\n\topacity: .8;\r\n\torder: 1;\r\n}\r\n\r\nh1 small {\r\n\tdisplay: block;\r\n}\r\n\r\n.site {\r\n\tdisplay: flex;\r\n align-items: center;\r\n\tflex-direction: column;\r\n\tmargin: 0 auto;\r\n\tjustify-content: center;\r\n}\r\n\r\n.sketch {\r\n\tposition: relative;\r\n\theight: 400px;\r\n\tmin-width: 400px;\r\n\tmargin: 0;\r\n\toverflow: visible;\r\n\torder: 2;\r\n\t\r\n}\r\n\r\n.bee-sketch {\r\n\theight: 100%;\r\n\twidth: 100%;\r\n\tposition: absolute;\r\n\ttop: 0;\r\n\tleft: 0;\r\n}\r\n\r\n.red {\r\n\tbackground: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/red-1.png) no-repeat center center;\r\n\topacity: 1;\r\n\t-webkit-animation: red 3s linear infinite, opacityRed 5s linear alternate infinite;\r\n\t        animation: red 3s linear infinite, opacityRed 5s linear alternate infinite;\r\n}\r\n\r\n.blue {\r\n\tbackground: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/blue-1.png) no-repeat center center;\r\n\topacity: 0;\r\n\t-webkit-animation: blue 3s linear infinite, opacityBlue 5s linear alternate infinite;\r\n\t        animation: blue 3s linear infinite, opacityBlue 5s linear alternate infinite;\r\n}\r\n\r\n@media only screen and (min-width: 780px) {\r\n  .site {\r\n\t\tflex-direction: row;\r\n\t\tpadding: 1em 3em 1em 0em;\r\n\t}\r\n\t\r\n\th1 {\r\n\t\ttext-align: right;\r\n\t\torder: 2;\r\n\t\tpadding-bottom: 2em;\r\n\t\tpadding-left: 2em;\r\n\r\n\t}\r\n\t\r\n\t.sketch {\r\n\t\torder: 1;\r\n\t}\r\n}\r\n\r\n@-webkit-keyframes blue {\r\n\t0% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/blue-1.png) \r\n  }\r\n\t9.09% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/blue-2.png) \r\n  }\r\n\t27.27% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/blue-3.png) \r\n  }\r\n\t36.36% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/blue-4.png) \r\n  }\r\n\t45.45% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/blue-5.png) \r\n  }\r\n\t54.54% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/blue-6.png) \r\n  }\r\n\t63.63% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/blue-7.png) \r\n  }\r\n\t72.72% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/blue-8.png) \r\n  }\r\n\t81.81% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/blue-9.png) \r\n  }\r\n\t100% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/blue-1.png) \r\n  }\r\n}\r\n\r\n@keyframes blue {\r\n\t0% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/blue-1.png) \r\n  }\r\n\t9.09% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/blue-2.png) \r\n  }\r\n\t27.27% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/blue-3.png) \r\n  }\r\n\t36.36% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/blue-4.png) \r\n  }\r\n\t45.45% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/blue-5.png) \r\n  }\r\n\t54.54% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/blue-6.png) \r\n  }\r\n\t63.63% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/blue-7.png) \r\n  }\r\n\t72.72% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/blue-8.png) \r\n  }\r\n\t81.81% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/blue-9.png) \r\n  }\r\n\t100% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/blue-1.png) \r\n  }\r\n}\r\n\r\n@-webkit-keyframes red {\r\n\t0% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/red-1.png) \r\n  }\r\n\t9.09% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/red-2.png) \r\n  }\r\n\t27.27% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/red-3.png) \r\n  }\r\n\t36.36% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/red-4.png) \r\n  }\r\n\t45.45% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/red-5.png) \r\n  }\r\n\t54.54% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/red-6.png) \r\n  }\r\n\t63.63% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/red-7.png) \r\n  }\r\n\t72.72% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/red-8.png) \r\n  }\r\n\t81.81% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/red-9.png) \r\n  }\r\n\t100% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/red-1.png) \r\n  }\r\n}\r\n\r\n@keyframes red {\r\n\t0% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/red-1.png) \r\n  }\r\n\t9.09% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/red-2.png) \r\n  }\r\n\t27.27% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/red-3.png) \r\n  }\r\n\t36.36% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/red-4.png) \r\n  }\r\n\t45.45% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/red-5.png) \r\n  }\r\n\t54.54% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/red-6.png) \r\n  }\r\n\t63.63% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/red-7.png) \r\n  }\r\n\t72.72% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/red-8.png) \r\n  }\r\n\t81.81% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/red-9.png) \r\n  }\r\n\t100% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/red-1.png) \r\n  }\r\n}\r\n\r\n@-webkit-keyframes opacityBlue {\r\n\tfrom {\r\n\t\topacity: 0\r\n\t}\r\n\t25% {\r\n\t\topacity: 0\r\n\t}\r\n\t75% {\r\n\t\topacity: 1\r\n\t}\r\n\tto {\r\n\t\topacity: 1\r\n\t}\r\n}\r\n\r\n@keyframes opacityBlue {\r\n\tfrom {\r\n\t\topacity: 0\r\n\t}\r\n\t25% {\r\n\t\topacity: 0\r\n\t}\r\n\t75% {\r\n\t\topacity: 1\r\n\t}\r\n\tto {\r\n\t\topacity: 1\r\n\t}\r\n}\r\n\r\n@-webkit-keyframes opacityRed {\r\n\tfrom {\r\n\t\topacity: 1\r\n\t}\r\n\t25% {\r\n\t\topacity: 1\r\n\t}\r\n\t75% {\r\n\t\topacity: .3\r\n\t}\r\n\tto {\r\n\t\topacity: .3\r\n\t}\r\n}\r\n\r\n@keyframes opacityRed {\r\n\tfrom {\r\n\t\topacity: 1\r\n\t}\r\n\t25% {\r\n\t\topacity: 1\r\n\t}\r\n\t75% {\r\n\t\topacity: .3\r\n\t}\r\n\tto {\r\n\t\topacity: .3\r\n\t}\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9lcnJvci9lcnJvci5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLG1FQUFtRTs7QUFFbkU7Q0FDQyxZQUFZO0FBQ2I7O0FBRUE7Q0FDQyxnQkFBZ0I7QUFDakI7O0FBRUE7Q0FDQyxhQUFhO0FBQ2Q7O0FBRUE7Q0FDQyxvQ0FBb0M7Q0FDcEMsY0FBYztDQUNkLGtCQUFrQjtDQUNsQixXQUFXO0NBQ1gsUUFBUTtBQUNUOztBQUVBO0NBQ0MsY0FBYztBQUNmOztBQUVBO0NBSUMsYUFBYTtDQUlaLG1CQUFtQjtDQUNwQixzQkFBc0I7Q0FDdEIsY0FBYztDQUlkLHVCQUF1QjtBQUN4Qjs7QUFHQTtDQUNDLGtCQUFrQjtDQUNsQixhQUFhO0NBQ2IsZ0JBQWdCO0NBQ2hCLFNBQVM7Q0FDVCxpQkFBaUI7Q0FDakIsUUFBUTs7QUFFVDs7QUFFQTtDQUNDLFlBQVk7Q0FDWixXQUFXO0NBQ1gsa0JBQWtCO0NBQ2xCLE1BQU07Q0FDTixPQUFPO0FBQ1I7O0FBRUE7Q0FDQyxzR0FBc0c7Q0FDdEcsVUFBVTtDQUNWLGtGQUEwRTtTQUExRSwwRUFBMEU7QUFDM0U7O0FBRUE7Q0FDQyx1R0FBdUc7Q0FDdkcsVUFBVTtDQUNWLG9GQUE0RTtTQUE1RSw0RUFBNEU7QUFDN0U7O0FBR0E7RUFDRTtFQUNBLG1CQUFtQjtFQUNuQix3QkFBd0I7Q0FDekI7O0NBRUE7RUFDQyxpQkFBaUI7RUFDakIsUUFBUTtFQUNSLG1CQUFtQjtFQUNuQixpQkFBaUI7O0NBRWxCOztDQUVBO0VBQ0MsUUFBUTtDQUNUO0FBQ0Q7O0FBR0E7Q0FDQztFQUNDO0VBQ0E7Q0FDRDtFQUNDO0VBQ0E7Q0FDRDtFQUNDO0VBQ0E7Q0FDRDtFQUNDO0VBQ0E7Q0FDRDtFQUNDO0VBQ0E7Q0FDRDtFQUNDO0VBQ0E7Q0FDRDtFQUNDO0VBQ0E7Q0FDRDtFQUNDO0VBQ0E7Q0FDRDtFQUNDO0VBQ0E7Q0FDRDtFQUNDO0VBQ0E7QUFDRjs7QUEvQkE7Q0FDQztFQUNDO0VBQ0E7Q0FDRDtFQUNDO0VBQ0E7Q0FDRDtFQUNDO0VBQ0E7Q0FDRDtFQUNDO0VBQ0E7Q0FDRDtFQUNDO0VBQ0E7Q0FDRDtFQUNDO0VBQ0E7Q0FDRDtFQUNDO0VBQ0E7Q0FDRDtFQUNDO0VBQ0E7Q0FDRDtFQUNDO0VBQ0E7Q0FDRDtFQUNDO0VBQ0E7QUFDRjs7QUFFQTtDQUNDO0VBQ0M7RUFDQTtDQUNEO0VBQ0M7RUFDQTtDQUNEO0VBQ0M7RUFDQTtDQUNEO0VBQ0M7RUFDQTtDQUNEO0VBQ0M7RUFDQTtDQUNEO0VBQ0M7RUFDQTtDQUNEO0VBQ0M7RUFDQTtDQUNEO0VBQ0M7RUFDQTtDQUNEO0VBQ0M7RUFDQTtDQUNEO0VBQ0M7RUFDQTtBQUNGOztBQS9CQTtDQUNDO0VBQ0M7RUFDQTtDQUNEO0VBQ0M7RUFDQTtDQUNEO0VBQ0M7RUFDQTtDQUNEO0VBQ0M7RUFDQTtDQUNEO0VBQ0M7RUFDQTtDQUNEO0VBQ0M7RUFDQTtDQUNEO0VBQ0M7RUFDQTtDQUNEO0VBQ0M7RUFDQTtDQUNEO0VBQ0M7RUFDQTtDQUNEO0VBQ0M7RUFDQTtBQUNGOztBQUVBO0NBQ0M7RUFDQztDQUNEO0NBQ0E7RUFDQztDQUNEO0NBQ0E7RUFDQztDQUNEO0NBQ0E7RUFDQztDQUNEO0FBQ0Q7O0FBYkE7Q0FDQztFQUNDO0NBQ0Q7Q0FDQTtFQUNDO0NBQ0Q7Q0FDQTtFQUNDO0NBQ0Q7Q0FDQTtFQUNDO0NBQ0Q7QUFDRDs7QUFFQTtDQUNDO0VBQ0M7Q0FDRDtDQUNBO0VBQ0M7Q0FDRDtDQUNBO0VBQ0M7Q0FDRDtDQUNBO0VBQ0M7Q0FDRDtBQUNEOztBQWJBO0NBQ0M7RUFDQztDQUNEO0NBQ0E7RUFDQztDQUNEO0NBQ0E7RUFDQztDQUNEO0NBQ0E7RUFDQztDQUNEO0FBQ0QiLCJmaWxlIjoiYXBwL2Vycm9yL2Vycm9yLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAaW1wb3J0IHVybCgnaHR0cHM6Ly9mb250cy5nb29nbGVhcGlzLmNvbS9jc3M/ZmFtaWx5PUNhYmluK1NrZXRjaCcpO1xyXG5cclxuaHRtbCB7XHJcblx0aGVpZ2h0OiAxMDAlO1xyXG59XHJcblxyXG5ib2R5IHtcclxuXHRtaW4taGVpZ2h0OiAxMDAlO1xyXG59XHJcblxyXG5ib2R5IHtcclxuXHRkaXNwbGF5OiBmbGV4O1xyXG59XHJcblxyXG5oMSB7XHJcblx0Zm9udC1mYW1pbHk6ICdDYWJpbiBTa2V0Y2gnLCBjdXJzaXZlO1xyXG5cdGZvbnQtc2l6ZTogM2VtO1xyXG5cdHRleHQtYWxpZ246IGNlbnRlcjtcclxuXHRvcGFjaXR5OiAuODtcclxuXHRvcmRlcjogMTtcclxufVxyXG5cclxuaDEgc21hbGwge1xyXG5cdGRpc3BsYXk6IGJsb2NrO1xyXG59XHJcblxyXG4uc2l0ZSB7XHJcblx0ZGlzcGxheTogLXdlYmtpdC1ib3g7XHJcblx0ZGlzcGxheTogLXdlYmtpdC1mbGV4O1xyXG5cdGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xyXG5cdGRpc3BsYXk6IGZsZXg7XHJcblx0LXdlYmtpdC1ib3gtYWxpZ246IGNlbnRlcjtcclxuXHQtd2Via2l0LWFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcblx0LW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG5cdGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcblx0bWFyZ2luOiAwIGF1dG87XHJcblx0LXdlYmtpdC1ib3gtcGFjazogY2VudGVyO1xyXG5cdC13ZWJraXQtanVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcblx0LW1zLWZsZXgtcGFjazogY2VudGVyO1xyXG5cdGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG59XHJcblxyXG5cclxuLnNrZXRjaCB7XHJcblx0cG9zaXRpb246IHJlbGF0aXZlO1xyXG5cdGhlaWdodDogNDAwcHg7XHJcblx0bWluLXdpZHRoOiA0MDBweDtcclxuXHRtYXJnaW46IDA7XHJcblx0b3ZlcmZsb3c6IHZpc2libGU7XHJcblx0b3JkZXI6IDI7XHJcblx0XHJcbn1cclxuXHJcbi5iZWUtc2tldGNoIHtcclxuXHRoZWlnaHQ6IDEwMCU7XHJcblx0d2lkdGg6IDEwMCU7XHJcblx0cG9zaXRpb246IGFic29sdXRlO1xyXG5cdHRvcDogMDtcclxuXHRsZWZ0OiAwO1xyXG59XHJcblxyXG4ucmVkIHtcclxuXHRiYWNrZ3JvdW5kOiB1cmwoaHR0cHM6Ly9zMy11cy13ZXN0LTIuYW1hem9uYXdzLmNvbS9zLmNkcG4uaW8vMTk4NTU0L3JlZC0xLnBuZykgbm8tcmVwZWF0IGNlbnRlciBjZW50ZXI7XHJcblx0b3BhY2l0eTogMTtcclxuXHRhbmltYXRpb246IHJlZCAzcyBsaW5lYXIgaW5maW5pdGUsIG9wYWNpdHlSZWQgNXMgbGluZWFyIGFsdGVybmF0ZSBpbmZpbml0ZTtcclxufVxyXG5cclxuLmJsdWUge1xyXG5cdGJhY2tncm91bmQ6IHVybChodHRwczovL3MzLXVzLXdlc3QtMi5hbWF6b25hd3MuY29tL3MuY2Rwbi5pby8xOTg1NTQvYmx1ZS0xLnBuZykgbm8tcmVwZWF0IGNlbnRlciBjZW50ZXI7XHJcblx0b3BhY2l0eTogMDtcclxuXHRhbmltYXRpb246IGJsdWUgM3MgbGluZWFyIGluZmluaXRlLCBvcGFjaXR5Qmx1ZSA1cyBsaW5lYXIgYWx0ZXJuYXRlIGluZmluaXRlO1xyXG59XHJcblxyXG5cclxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOiA3ODBweCkge1xyXG4gIC5zaXRlIHtcclxuXHRcdGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcblx0XHRwYWRkaW5nOiAxZW0gM2VtIDFlbSAwZW07XHJcblx0fVxyXG5cdFxyXG5cdGgxIHtcclxuXHRcdHRleHQtYWxpZ246IHJpZ2h0O1xyXG5cdFx0b3JkZXI6IDI7XHJcblx0XHRwYWRkaW5nLWJvdHRvbTogMmVtO1xyXG5cdFx0cGFkZGluZy1sZWZ0OiAyZW07XHJcblxyXG5cdH1cclxuXHRcclxuXHQuc2tldGNoIHtcclxuXHRcdG9yZGVyOiAxO1xyXG5cdH1cclxufVxyXG5cclxuXHJcbkBrZXlmcmFtZXMgYmx1ZSB7XHJcblx0MCUge1xyXG5cdFx0YmFja2dyb3VuZC1pbWFnZTogdXJsKGh0dHBzOi8vczMtdXMtd2VzdC0yLmFtYXpvbmF3cy5jb20vcy5jZHBuLmlvLzE5ODU1NC9ibHVlLTEucG5nKSBcclxuICB9XHJcblx0OS4wOSUge1xyXG5cdFx0YmFja2dyb3VuZC1pbWFnZTogdXJsKGh0dHBzOi8vczMtdXMtd2VzdC0yLmFtYXpvbmF3cy5jb20vcy5jZHBuLmlvLzE5ODU1NC9ibHVlLTIucG5nKSBcclxuICB9XHJcblx0MjcuMjclIHtcclxuXHRcdGJhY2tncm91bmQtaW1hZ2U6IHVybChodHRwczovL3MzLXVzLXdlc3QtMi5hbWF6b25hd3MuY29tL3MuY2Rwbi5pby8xOTg1NTQvYmx1ZS0zLnBuZykgXHJcbiAgfVxyXG5cdDM2LjM2JSB7XHJcblx0XHRiYWNrZ3JvdW5kLWltYWdlOiB1cmwoaHR0cHM6Ly9zMy11cy13ZXN0LTIuYW1hem9uYXdzLmNvbS9zLmNkcG4uaW8vMTk4NTU0L2JsdWUtNC5wbmcpIFxyXG4gIH1cclxuXHQ0NS40NSUge1xyXG5cdFx0YmFja2dyb3VuZC1pbWFnZTogdXJsKGh0dHBzOi8vczMtdXMtd2VzdC0yLmFtYXpvbmF3cy5jb20vcy5jZHBuLmlvLzE5ODU1NC9ibHVlLTUucG5nKSBcclxuICB9XHJcblx0NTQuNTQlIHtcclxuXHRcdGJhY2tncm91bmQtaW1hZ2U6IHVybChodHRwczovL3MzLXVzLXdlc3QtMi5hbWF6b25hd3MuY29tL3MuY2Rwbi5pby8xOTg1NTQvYmx1ZS02LnBuZykgXHJcbiAgfVxyXG5cdDYzLjYzJSB7XHJcblx0XHRiYWNrZ3JvdW5kLWltYWdlOiB1cmwoaHR0cHM6Ly9zMy11cy13ZXN0LTIuYW1hem9uYXdzLmNvbS9zLmNkcG4uaW8vMTk4NTU0L2JsdWUtNy5wbmcpIFxyXG4gIH1cclxuXHQ3Mi43MiUge1xyXG5cdFx0YmFja2dyb3VuZC1pbWFnZTogdXJsKGh0dHBzOi8vczMtdXMtd2VzdC0yLmFtYXpvbmF3cy5jb20vcy5jZHBuLmlvLzE5ODU1NC9ibHVlLTgucG5nKSBcclxuICB9XHJcblx0ODEuODElIHtcclxuXHRcdGJhY2tncm91bmQtaW1hZ2U6IHVybChodHRwczovL3MzLXVzLXdlc3QtMi5hbWF6b25hd3MuY29tL3MuY2Rwbi5pby8xOTg1NTQvYmx1ZS05LnBuZykgXHJcbiAgfVxyXG5cdDEwMCUge1xyXG5cdFx0YmFja2dyb3VuZC1pbWFnZTogdXJsKGh0dHBzOi8vczMtdXMtd2VzdC0yLmFtYXpvbmF3cy5jb20vcy5jZHBuLmlvLzE5ODU1NC9ibHVlLTEucG5nKSBcclxuICB9XHJcbn1cclxuXHJcbkBrZXlmcmFtZXMgcmVkIHtcclxuXHQwJSB7XHJcblx0XHRiYWNrZ3JvdW5kLWltYWdlOiB1cmwoaHR0cHM6Ly9zMy11cy13ZXN0LTIuYW1hem9uYXdzLmNvbS9zLmNkcG4uaW8vMTk4NTU0L3JlZC0xLnBuZykgXHJcbiAgfVxyXG5cdDkuMDklIHtcclxuXHRcdGJhY2tncm91bmQtaW1hZ2U6IHVybChodHRwczovL3MzLXVzLXdlc3QtMi5hbWF6b25hd3MuY29tL3MuY2Rwbi5pby8xOTg1NTQvcmVkLTIucG5nKSBcclxuICB9XHJcblx0MjcuMjclIHtcclxuXHRcdGJhY2tncm91bmQtaW1hZ2U6IHVybChodHRwczovL3MzLXVzLXdlc3QtMi5hbWF6b25hd3MuY29tL3MuY2Rwbi5pby8xOTg1NTQvcmVkLTMucG5nKSBcclxuICB9XHJcblx0MzYuMzYlIHtcclxuXHRcdGJhY2tncm91bmQtaW1hZ2U6IHVybChodHRwczovL3MzLXVzLXdlc3QtMi5hbWF6b25hd3MuY29tL3MuY2Rwbi5pby8xOTg1NTQvcmVkLTQucG5nKSBcclxuICB9XHJcblx0NDUuNDUlIHtcclxuXHRcdGJhY2tncm91bmQtaW1hZ2U6IHVybChodHRwczovL3MzLXVzLXdlc3QtMi5hbWF6b25hd3MuY29tL3MuY2Rwbi5pby8xOTg1NTQvcmVkLTUucG5nKSBcclxuICB9XHJcblx0NTQuNTQlIHtcclxuXHRcdGJhY2tncm91bmQtaW1hZ2U6IHVybChodHRwczovL3MzLXVzLXdlc3QtMi5hbWF6b25hd3MuY29tL3MuY2Rwbi5pby8xOTg1NTQvcmVkLTYucG5nKSBcclxuICB9XHJcblx0NjMuNjMlIHtcclxuXHRcdGJhY2tncm91bmQtaW1hZ2U6IHVybChodHRwczovL3MzLXVzLXdlc3QtMi5hbWF6b25hd3MuY29tL3MuY2Rwbi5pby8xOTg1NTQvcmVkLTcucG5nKSBcclxuICB9XHJcblx0NzIuNzIlIHtcclxuXHRcdGJhY2tncm91bmQtaW1hZ2U6IHVybChodHRwczovL3MzLXVzLXdlc3QtMi5hbWF6b25hd3MuY29tL3MuY2Rwbi5pby8xOTg1NTQvcmVkLTgucG5nKSBcclxuICB9XHJcblx0ODEuODElIHtcclxuXHRcdGJhY2tncm91bmQtaW1hZ2U6IHVybChodHRwczovL3MzLXVzLXdlc3QtMi5hbWF6b25hd3MuY29tL3MuY2Rwbi5pby8xOTg1NTQvcmVkLTkucG5nKSBcclxuICB9XHJcblx0MTAwJSB7XHJcblx0XHRiYWNrZ3JvdW5kLWltYWdlOiB1cmwoaHR0cHM6Ly9zMy11cy13ZXN0LTIuYW1hem9uYXdzLmNvbS9zLmNkcG4uaW8vMTk4NTU0L3JlZC0xLnBuZykgXHJcbiAgfVxyXG59XHJcblxyXG5Aa2V5ZnJhbWVzIG9wYWNpdHlCbHVlIHtcclxuXHRmcm9tIHtcclxuXHRcdG9wYWNpdHk6IDBcclxuXHR9XHJcblx0MjUlIHtcclxuXHRcdG9wYWNpdHk6IDBcclxuXHR9XHJcblx0NzUlIHtcclxuXHRcdG9wYWNpdHk6IDFcclxuXHR9XHJcblx0dG8ge1xyXG5cdFx0b3BhY2l0eTogMVxyXG5cdH1cclxufVxyXG5cclxuQGtleWZyYW1lcyBvcGFjaXR5UmVkIHtcclxuXHRmcm9tIHtcclxuXHRcdG9wYWNpdHk6IDFcclxuXHR9XHJcblx0MjUlIHtcclxuXHRcdG9wYWNpdHk6IDFcclxuXHR9XHJcblx0NzUlIHtcclxuXHRcdG9wYWNpdHk6IC4zXHJcblx0fVxyXG5cdHRvIHtcclxuXHRcdG9wYWNpdHk6IC4zXHJcblx0fVxyXG59Il19 */"

/***/ }),

/***/ "./src/app/error/error.component.html":
/*!********************************************!*\
  !*** ./src/app/error/error.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div class=\"site\">\n\t<div class=\"sketch\">\n\t\t<div class=\"bee-sketch red\"></div>\n\t\t<div class=\"bee-sketch blue\"></div>\n\t</div>\n\n<h1>404:\n\t<small>Users Not Found</small></h1>\n\n\t<h1>\n\t\t<small routerLink=\"/signup\" id=\"previous\"> <-  </small></h1>\n\t\n</div>"

/***/ }),

/***/ "./src/app/error/error.component.ts":
/*!******************************************!*\
  !*** ./src/app/error/error.component.ts ***!
  \******************************************/
/*! exports provided: ErrorComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ErrorComponent", function() { return ErrorComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./src/node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var ErrorComponent = /** @class */ (function () {
    function ErrorComponent(router) {
        this.router = router;
    }
    ErrorComponent.prototype.ngOnInit = function () {
        //$("#headercalendar").hide();
        jquery__WEBPACK_IMPORTED_MODULE_2__("#header").hide();
        // $("#headcalendar").hide();
    };
    ErrorComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-error',
            template: __webpack_require__(/*! ./error.component.html */ "./src/app/error/error.component.html"),
            styles: [__webpack_require__(/*! ./error.component.css */ "./src/app/error/error.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], ErrorComponent);
    return ErrorComponent;
}());



/***/ }),

/***/ "./src/app/header/diagrams/diagrams.component.css":
/*!********************************************************!*\
  !*** ./src/app/header/diagrams/diagrams.component.css ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAvaGVhZGVyL2RpYWdyYW1zL2RpYWdyYW1zLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/header/diagrams/diagrams.component.html":
/*!*********************************************************!*\
  !*** ./src/app/header/diagrams/diagrams.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div id=\"maincontenthome\">\n  \n  <div id=\"jquery-script-menu\">\n    <div class=\"jquery-script-center\">\n    \n    <div class=\"jquery-script-ads\"><script type=\"text/javascript\">\n    /* jQuery_demo */\n    google_ad_slot = \"2780937993\";\n    google_ad_width = 728;\n    google_ad_height = 90;\n    //-->\n    </script>\n    <script type=\"text/javascript\"\n    src=\"http://pagead2.googlesyndication.com/pagead/show_ads.js\">\n    </script></div>\n    <div class=\"jquery-script-clear\"></div>\n    </div>\n    </div>\n            <div class=\"container\">\n            <h1>Current Calories</h1>\n                <div class=\"row\">\n                    <div class=\"col-sm-4 col-sm-offset-4\">\n                        <div class=\"my-progress-bar\"></div>\n                    </div>\n                </div>\n            </div>\n    \n           <script src=\"https://code.jquery.com/jquery-3.2.1.min.js\"></script>\n           \n            <script type=\"text/javascript\">\n    \n      var _gaq = _gaq || [];\n      _gaq.push(['_setAccount', 'UA-36251023-1']);\n      _gaq.push(['_setDomainName', 'jqueryscript.net']);\n      _gaq.push(['_trackPageview']);\n    \n      (function() {\n        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;\n        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';\n        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);\n      })();\n    \n    </script>\n  \n</div>"

/***/ }),

/***/ "./src/app/header/diagrams/diagrams.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/header/diagrams/diagrams.component.ts ***!
  \*******************************************************/
/*! exports provided: DiagramsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DiagramsComponent", function() { return DiagramsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./src/node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_2__);



var DiagramsComponent = /** @class */ (function () {
    function DiagramsComponent() {
    }
    DiagramsComponent.prototype.ngOnInit = function () {
        /*  $(document).ready(function(){
            $("button").click(function(){
              $("p").hide();
      
      
            });
          });*/
        //
        var global_settings = { percentage: "a", text: "a", starting_position: 1,
            ending_position: 1, width: 1, height: 1, percent: 0, color: "a", line_width: 1,
            counter_clockwise: true
        };
        (function ($) {
            var methods = {
                init: function (options) {
                    // This is the easiest way to have default options.
                    var settings = $.extend({
                        // These are the defaults.
                        color: "#000000",
                        height: "300px",
                        width: "300px",
                        line_width: 8,
                        starting_position: 25,
                        percent: 100,
                        counter_clockwise: false,
                        percentage: true,
                        text: ''
                    }, options);
                    global_settings = settings;
                    // Create percentage
                    var percentage = $("<div class='progress-percentage'></div>");
                    if (!global_settings.percentage) {
                        percentage.text(global_settings.percentage);
                    }
                    $(this).append(percentage);
                    // Create text
                    var text = $("<div class='progress-text'></div>");
                    // Custom text
                    if (global_settings.text != "percent") {
                        text.text(global_settings.text);
                    }
                    $(this).append(text);
                    // Correct any invalid values
                    if (global_settings.starting_position != 100) {
                        global_settings.starting_position = global_settings.starting_position % 100;
                    }
                    if (global_settings.ending_position != 100) {
                        global_settings.ending_position = global_settings.ending_position % 100;
                    }
                    // No 'px' or '%', add 'px'
                    appendUnit(global_settings.width);
                    appendUnit(global_settings.height);
                    // Apply global_settings
                    $(this).css({
                        "height": global_settings.height,
                        "width": global_settings.width
                    });
                    $(this).addClass("circular-progress-bar");
                    // Remove old canvas
                    $(this).find("canvas").remove();
                    // Put canvas inside this
                    $(this).append(createCanvas($(this)));
                    // Return allows for chaining
                    return this;
                },
                percent: function (value) {
                    // Change percent
                    global_settings.percent = value;
                    // Apply global_settings
                    $(this).css({
                        "height": global_settings.height,
                        "width": global_settings.width
                    });
                    // Remove old canvas
                    $(this).children("canvas").remove();
                    // Put canvas inside this
                    $(this).append(createCanvas($(this)));
                    // Return allows for chaining
                    return this;
                },
                animate: function (value, time) {
                    // Apply global_settings
                    // Number of intervals, 10ms interval
                    var num_of_steps = time / 10;
                    // Amount of change each step
                    var percent_change = (value - global_settings.percent) / num_of_steps;
                    // Variable conflict, rename this
                    var scope = $(this);
                    var theInterval = setInterval(function () {
                        if (global_settings.percent < value) {
                            // Remove old canvas
                            scope.children("canvas").remove();
                            // Increment percent
                            global_settings.percent += percent_change;
                            // Put canvas inside this
                            scope.append(createCanvas(scope));
                        }
                        else {
                            clearInterval(theInterval);
                        }
                    }, 10);
                    // Return allows for chaining
                    return this;
                }
            };
            $.fn.circularProgress = function (methodOrOptions) {
                if (methods[methodOrOptions]) {
                    // Method found
                    return methods[methodOrOptions].apply(this, Array.prototype.slice.call(arguments, 1));
                }
                else if (typeof methodOrOptions === 'object' || !methodOrOptions) {
                    // Default to "init", object passed in or nothing passed in
                    return methods.init.apply(this, arguments);
                }
                else {
                    $.error('Method ' + methodOrOptions + ' does not exist.');
                }
            };
            /* =========================================================================
                PRIVATE FUNCTIONS
            ========================================================================= */
            // return string without 'px' or '%'
            function removeUnit(apples) {
                if (apples.indexOf("px")) {
                    return apples.substring(0, apples.length - 2);
                }
            }
            ;
            // return string with 'px'
            function appendUnit(apples) {
                if (apples.toString().indexOf("px") < -1 && apples.toString().indexOf("%") < -1) {
                    return apples += "px";
                }
            }
            ;
            // calculate starting position on canvas
            function calcPos(apples, percent) {
                if (percent < 0) {
                    // Calculate starting position
                    var starting_degree = (parseInt(apples) / 100) * 360;
                    var starting_radian = starting_degree * (Math.PI / 180);
                    return starting_radian - (Math.PI / 2);
                }
                else {
                    // Calculate ending position
                    var ending_degree = ((parseInt(apples) + parseInt(percent)) / 100) * 360;
                    var ending_radian = ending_degree * (Math.PI / 180);
                    return ending_radian - (Math.PI / 2);
                }
            }
            ;
            // Put percentage or custom text inside progress circle
            function insertText(scope) {
                $(".progress-percentage").text(Math.round(global_settings.percent) + "%");
            }
            // create canvas
            function createCanvas(scope) {
                // Remove 'px' or '%'
                var canvas_height = removeUnit(global_settings.height.toString());
                var canvas_width = removeUnit(global_settings.width.toString());
                // Create canvas
                var canvas = document.createElement("canvas");
                canvas.height = canvas_height;
                canvas.width = canvas_width;
                // Create drawable canvas and apply properties
                var ctx = canvas.getContext("2d");
                ctx.strokeStyle = global_settings.color;
                ctx.lineWidth = global_settings.line_width;
                // Draw arc
                ctx.beginPath();
                // Calculate starting and ending positions
                var starting_radian = calcPos(global_settings.starting_position, -1);
                var ending_radian = calcPos(global_settings.starting_position, global_settings.percent);
                // Calculate radius and x,y coordinates
                var radius = 0;
                var xcoord = canvas_width / 2;
                var ycoord = canvas_height / 2;
                // Height or width greater
                if (canvas_height >= canvas_width) {
                    radius = canvas_width * 0.9 / 2 - (global_settings.line_width * 2);
                }
                else {
                    radius = canvas_height * 0.9 / 2 - (global_settings.line_width * 2);
                }
                /*
                    x coordinate
                    y coordinate
                    radius of circle
                    starting angle in radians
                    ending angle in radians
                    clockwise (false, default) or counter-clockwise (true)
                */
                ctx.arc(xcoord, ycoord, radius, starting_radian, ending_radian, global_settings.counter_clockwise);
                ctx.stroke();
                // Add text
                if (global_settings.percentage) {
                    insertText(scope);
                }
                return canvas;
            }
            ;
        }(jquery__WEBPACK_IMPORTED_MODULE_2__));
        var a = 60;
        jquery__WEBPACK_IMPORTED_MODULE_2__(".my-progress-bar").circularProgress({
            line_width: 9,
            color: "#1E90FF",
            starting_position: 0,
            percent: 0,
            percentage: true,
            text: "1600/2000 Kcal"
        }).circularProgress('animate', a, 5000);
        //
    };
    DiagramsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-diagrams',
            template: __webpack_require__(/*! ./diagrams.component.html */ "./src/app/header/diagrams/diagrams.component.html"),
            styles: [__webpack_require__(/*! ./diagrams.component.css */ "./src/app/header/diagrams/diagrams.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], DiagramsComponent);
    return DiagramsComponent;
}());



/***/ }),

/***/ "./src/app/header/header.component.css":
/*!*********************************************!*\
  !*** ./src/app/header/header.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/*\r\n#myProgress {\r\n  width: 100%;\r\n  background-color: #ddd;\r\n}\r\n\r\n#myBar {\r\n  width: 1%;\r\n  height: 30px;\r\n  background-color: #4CAF50;\r\n}*/\r\n#button1 {\r\n    background-color: #b2b2b2\t; \r\n    border: none;\r\n    color: white;\r\n    padding-bottom: 25px 22px;\r\n    text-align: center;\r\n    text-decoration: none;\r\n    display: inline-block;\r\n    font-size: 36px;\r\n    margin: 4px 18px;\r\n    cursor: pointer;\r\n  }\r\n#button2 {\r\n   background-color: #b2b2b2\t; \r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n#button3 {\r\n   background-color: #b2b2b2\t; \r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n#button4 {\r\n   background-color: #b2b2b2\t;\r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n#all{\r\n     text-align: center;\r\n     position: fixed;\r\n     left: 0;\r\n     bottom: 0;\r\n     width: 100%;\r\n     background-color: \t\t#b2b2b2;\r\n     color: white;\r\n     text-align: center;\r\n  }\r\ndiv.home {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\ndiv.home input {\r\n    background: url('home.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\ndiv.analytic {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\ndiv.analytic input {\r\n    background: url('analytic.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\ndiv.notification {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\ndiv.notification input {\r\n    background: url('outline-today-24px.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\ndiv.setting {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\ndiv.setting input {\r\n    background: url('settings.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\nspan.home {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 6px;\r\n  }\r\nspan.analytic {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 2px;\r\n  }\r\nspan.notification {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 2px;\r\n  }\r\nspan.setting {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 12px;\r\n  }\r\n\r\n\r\n  \r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9oZWFkZXIvaGVhZGVyLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7Ozs7Ozs7RUFVRTtBQUNGO0lBQ0ksMEJBQTBCO0lBQzFCLFlBQVk7SUFDWixZQUFZO0lBQ1oseUJBQXlCO0lBQ3pCLGtCQUFrQjtJQUNsQixxQkFBcUI7SUFDckIscUJBQXFCO0lBQ3JCLGVBQWU7SUFDZixnQkFBZ0I7SUFDaEIsZUFBZTtFQUNqQjtBQUdGO0dBQ0csMEJBQTBCO0dBQzFCLFlBQVk7R0FDWixZQUFZO0dBQ1oseUJBQXlCO0dBQ3pCLGtCQUFrQjtHQUNsQixxQkFBcUI7R0FDckIscUJBQXFCO0dBQ3JCLGVBQWU7R0FDZixnQkFBZ0I7R0FDaEIsZUFBZTtDQUNqQjtBQUNBO0dBQ0UsMEJBQTBCO0dBQzFCLFlBQVk7R0FDWixZQUFZO0dBQ1oseUJBQXlCO0dBQ3pCLGtCQUFrQjtHQUNsQixxQkFBcUI7R0FDckIscUJBQXFCO0dBQ3JCLGVBQWU7R0FDZixnQkFBZ0I7R0FDaEIsZUFBZTtDQUNqQjtBQUVBO0dBQ0UsMEJBQTBCO0dBQzFCLFlBQVk7R0FDWixZQUFZO0dBQ1oseUJBQXlCO0dBQ3pCLGtCQUFrQjtHQUNsQixxQkFBcUI7R0FDckIscUJBQXFCO0dBQ3JCLGVBQWU7R0FDZixnQkFBZ0I7R0FDaEIsZUFBZTtDQUNqQjtBQUVDO0tBQ0csa0JBQWtCO0tBQ2xCLGVBQWU7S0FDZixPQUFPO0tBQ1AsU0FBUztLQUNULFdBQVc7S0FDWCwyQkFBMkI7S0FDM0IsWUFBWTtLQUNaLGtCQUFrQjtFQUNyQjtBQUVBO0lBQ0Usa0JBQWtCO0lBQ2xCLHFCQUFxQjtFQUN2QjtBQUVBO0lBQ0UscUNBQTJEO0lBQzNELGVBQWU7SUFDZixXQUFXO0lBQ1gsWUFBWTtJQUNaLFlBQVk7RUFDZDtBQUNBO0lBQ0Usa0JBQWtCO0lBQ2xCLHFCQUFxQjtFQUN2QjtBQUVBO0lBQ0UseUNBQStEO0lBQy9ELGVBQWU7SUFDZixXQUFXO0lBQ1gsWUFBWTtJQUNaLFlBQVk7RUFDZDtBQUVBO0lBQ0Usa0JBQWtCO0lBQ2xCLHFCQUFxQjtFQUN2QjtBQUVBO0lBQ0UsbURBQTJFO0lBQzNFLGVBQWU7SUFDZixXQUFXO0lBQ1gsWUFBWTtJQUNaLFlBQVk7RUFDZDtBQUVBO0lBQ0Usa0JBQWtCO0lBQ2xCLHFCQUFxQjtFQUN2QjtBQUVBO0lBQ0UseUNBQStEO0lBQy9ELGVBQWU7SUFDZixXQUFXO0lBQ1gsWUFBWTtJQUNaLFlBQVk7RUFDZDtBQUNBO0lBQ0Usa0JBQWtCO0lBQ2xCLFNBQVM7SUFDVCxTQUFTO0VBQ1g7QUFFQTtJQUNFLGtCQUFrQjtJQUNsQixTQUFTO0lBQ1QsU0FBUztFQUNYO0FBRUE7SUFDRSxrQkFBa0I7SUFDbEIsU0FBUztJQUNULFNBQVM7RUFDWDtBQUVBO0lBQ0Usa0JBQWtCO0lBQ2xCLFNBQVM7SUFDVCxVQUFVO0VBQ1oiLCJmaWxlIjoiYXBwL2hlYWRlci9oZWFkZXIuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbiNteVByb2dyZXNzIHtcclxuICB3aWR0aDogMTAwJTtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZGRkO1xyXG59XHJcblxyXG4jbXlCYXIge1xyXG4gIHdpZHRoOiAxJTtcclxuICBoZWlnaHQ6IDMwcHg7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzRDQUY1MDtcclxufSovXHJcbiNidXR0b24xIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNiMmIyYjJcdDsgXHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMjVweCAyMnB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgZm9udC1zaXplOiAzNnB4O1xyXG4gICAgbWFyZ2luOiA0cHggMThweDtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICB9XHJcblxyXG4gIFxyXG4jYnV0dG9uMiB7XHJcbiAgIGJhY2tncm91bmQtY29sb3I6ICNiMmIyYjJcdDsgXHJcbiAgIGJvcmRlcjogbm9uZTtcclxuICAgY29sb3I6IHdoaXRlO1xyXG4gICBwYWRkaW5nLWJvdHRvbTogMjVweCAyMnB4O1xyXG4gICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICBmb250LXNpemU6IDM2cHg7XHJcbiAgIG1hcmdpbjogNHB4IDE4cHg7XHJcbiAgIGN1cnNvcjogcG9pbnRlcjtcclxuIH1cclxuICNidXR0b24zIHtcclxuICAgYmFja2dyb3VuZC1jb2xvcjogI2IyYjJiMlx0OyBcclxuICAgYm9yZGVyOiBub25lO1xyXG4gICBjb2xvcjogd2hpdGU7XHJcbiAgIHBhZGRpbmctYm90dG9tOiAyNXB4IDIycHg7XHJcbiAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgIGZvbnQtc2l6ZTogMzZweDtcclxuICAgbWFyZ2luOiA0cHggMThweDtcclxuICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gfVxyXG5cclxuICNidXR0b240IHtcclxuICAgYmFja2dyb3VuZC1jb2xvcjogI2IyYjJiMlx0O1xyXG4gICBib3JkZXI6IG5vbmU7XHJcbiAgIGNvbG9yOiB3aGl0ZTtcclxuICAgcGFkZGluZy1ib3R0b206IDI1cHggMjJweDtcclxuICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgZm9udC1zaXplOiAzNnB4O1xyXG4gICBtYXJnaW46IDRweCAxOHB4O1xyXG4gICBjdXJzb3I6IHBvaW50ZXI7XHJcbiB9XHJcbiBcclxuICAjYWxse1xyXG4gICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgICAgbGVmdDogMDtcclxuICAgICBib3R0b206IDA7XHJcbiAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgYmFja2dyb3VuZC1jb2xvcjogXHRcdCNiMmIyYjI7XHJcbiAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICB9XHJcblxyXG4gIGRpdi5ob21lIHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICB9XHJcblxyXG4gIGRpdi5ob21lIGlucHV0IHtcclxuICAgIGJhY2tncm91bmQ6IHVybCgnc3JjL2FwcC9oZWFkZXIvaW1hZ2VzL2hvbWUuc3ZnJykgbm8tcmVwZWF0O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgd2lkdGg6IDUwcHg7XHJcbiAgICBoZWlnaHQ6IDUwcHg7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgfVxyXG4gIGRpdi5hbmFseXRpYyB7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgfVxyXG5cclxuICBkaXYuYW5hbHl0aWMgaW5wdXQge1xyXG4gICAgYmFja2dyb3VuZDogdXJsKCdzcmMvYXBwL2hlYWRlci9pbWFnZXMvYW5hbHl0aWMuc3ZnJykgbm8tcmVwZWF0O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgd2lkdGg6IDUwcHg7XHJcbiAgICBoZWlnaHQ6IDUwcHg7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgfVxyXG5cclxuICBkaXYubm90aWZpY2F0aW9uIHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICB9XHJcblxyXG4gIGRpdi5ub3RpZmljYXRpb24gaW5wdXQge1xyXG4gICAgYmFja2dyb3VuZDogdXJsKCdzcmMvYXBwL2NhbGVuZGFyL2ltYWdlcy9vdXRsaW5lLXRvZGF5LTI0cHguc3ZnJykgbm8tcmVwZWF0O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgd2lkdGg6IDUwcHg7XHJcbiAgICBoZWlnaHQ6IDUwcHg7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgfVxyXG4gIFxyXG4gIGRpdi5zZXR0aW5nIHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICB9XHJcblxyXG4gIGRpdi5zZXR0aW5nIGlucHV0IHtcclxuICAgIGJhY2tncm91bmQ6IHVybCgnc3JjL2FwcC9oZWFkZXIvaW1hZ2VzL3NldHRpbmdzLnN2ZycpIG5vLXJlcGVhdDtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIHdpZHRoOiA1MHB4O1xyXG4gICAgaGVpZ2h0OiA1MHB4O1xyXG4gICAgYm9yZGVyOiBub25lO1xyXG4gIH1cclxuICBzcGFuLmhvbWUge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAzMnB4O1xyXG4gICAgbGVmdDogNnB4O1xyXG4gIH1cclxuICBcclxuICBzcGFuLmFuYWx5dGljIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogMzJweDtcclxuICAgIGxlZnQ6IDJweDtcclxuICB9XHJcblxyXG4gIHNwYW4ubm90aWZpY2F0aW9uIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogMzJweDtcclxuICAgIGxlZnQ6IDJweDtcclxuICB9XHJcblxyXG4gIHNwYW4uc2V0dGluZyB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDMycHg7XHJcbiAgICBsZWZ0OiAxMnB4O1xyXG4gIH1cclxuXHJcblxyXG4gICJdfQ== */"

/***/ }),

/***/ "./src/app/header/header.component.html":
/*!**********************************************!*\
  !*** ./src/app/header/header.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n\n<link  href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css\"  rel=\"stylesheet\">\n\n\n<div id=\"button3\">   <div  class=\"col-sm-12\"   align=\"center\" style=\"background-color:red;\"  >  <button id=\"not3\" (click)=\"notification3()\">Notifications</button>  </div></div>\n\n\n   <div id=\"button\">   <div  class=\"col-sm-12\"   align=\"center\" style=\"background-color:red;\"  >  <button id=\"not\" (click)=\"notification()\">Notifications</button>  </div></div>\n\n\n   <div id=\"button2\">   <div  class=\"col-sm-12\"   align=\"center\" style=\"background-color:red;\"  >  <button id=\"not2\" (click)=\"notification2()\">Notifications</button>  </div></div>\n\n   <!--\n<div id=\"main\">\n <nav id=\"header\" class=\"navbar navbar-expand-lg navbar-dark bg-dark\">\n  <a class=\"navbar-brand\" href=\"#\">\n    FANI\n  </a>\n\n  <button class=\"navbar-toggler\" type=\"button\"\n          (click)=\"toggleNavbar()\">\n    <span class=\"navbar-toggler-icon\"></span>\n  </button>\n\n\n  <div class=\"collapse navbar-collapse\"\n       [ngClass]=\"{ 'show': navbarOpen }\">\n    <ul class=\"navbar-nav mr-auto\">\n      <li class=\"nav-item\">\n        <a class=\"nav-link\"routerLink=\"/signup\">calendar</a>\n      </li>\n\n      <li class=\"nav-item\">\n        <a class=\"nav-link\" href=\"#\">logout</a>\n      </li>\n    </ul>\n  </div>\n </nav>\n\n</div>-->\n\n\n\n <!-- <div class=\"container\">\n            \n      <div class=\"row\">\n          \n         <div class=\"col-sm-12\" style=\"\">\n            <div id=\"myProgress\">\n                <div id=\"myBar\"></div>\n              </div>\n          </div>\n          </div>\n    </div>-->\n  \n<div id=\"maincontenthome\">\n  \n\n  <div id=\"jquery-script-menu\">\n    <div class=\"jquery-script-center\">\n    \n    <div class=\"jquery-script-ads\"><script type=\"text/javascript\">\n    /* jQuery_demo */\n    google_ad_slot = \"2780937993\";\n    google_ad_width = 728;\n    google_ad_height = 90;\n    //-->\n    </script>\n    <script type=\"text/javascript\"\n    src=\"http://pagead2.googlesyndication.com/pagead/show_ads.js\">\n    </script></div>\n    <div class=\"jquery-script-clear\"></div>\n  \n    \n      </div>\n           <div class=\"container\">\n            \n                <div class=\"row\">\n                    \n                   <!-- <div class=\"col-sm-12\" style=\"\">\n                        <h1>Current Calories</h1>\n                    </div>-->\n                   \n                   \n                    <div class=\"col-sm-12 \">\n                        <h1  align=\"center\" style=\"color:#262626;\">Today plan</h1>\n                        <div class=\"my-progress-bar\"></div>\n                    </div>\n                </div>\n            </div>\n\n            <div class=\"container\">\n           \n                <div class=\"row\">\n                  <div class=\"col-sm-12\" >\n                      <h6 id=\"complete\" col align=\"center \" style=\"color:#262626;\"></h6>\n                   \n                  </div>\n                  \n                </div>\n              </div>\n\n       \n           \n    \n           <script src=\"https://code.jquery.com/jquery-3.2.1.min.js\"></script>\n           \n            <script type=\"text/javascript\">\n    \n      var _gaq = _gaq || [];\n      _gaq.push(['_setAccount', 'UA-36251023-1']);\n      _gaq.push(['_setDomainName', 'jqueryscript.net']);\n      _gaq.push(['_trackPageview']);\n    \n      (function() {\n        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;\n        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';\n        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);\n      })();\n    \n    </script>\n  \n</div>\n\n\n<div id=\"all\">\n\n  \n    <div class=\"home\">\n        <span id=\"home\" class=\"home\">Home  </span>\n        <input id=\"button1\" type=\"button\"  (click)=\"submitted=false\" (click)=\"goToPage('/', 'main')\">\n      \n    </div>\n\n     <div class=\"analytic\">\n      <span id=\"analytic\" class=\"analytic\">Analytics  </span>\n      <input id=\"button2\" type=\"button\" alt=\"Submit\" width=\"48\" height=\"48\" (click)=\"submitted=false\" (click)=\"goToPage('/', 'analytic')\">\n  \n    </div>\n\n    <div class=\"notification\">\n        <span id=\"notification\" class=\"notification\">Calendar  </span>\n        <input id=\"button3\" type=\"button\" alt=\"Submit\" width=\"48\" height=\"48\" (click)=\"submitted=false\" (click)=\"goToPage('/', 'calendar')\">\n    \n      </div>\n    \n      <div class=\"setting\">\n          <span id=\"setting\" class=\"setting\">Settings  </span>\n          <input id=\"button4\" type=\"button\" alt=\"Submit\" width=\"48\" height=\"48\" (click)=\"submitted=false\" (click)=\"goToPage('/', 'settings')\">\n      \n        </div>\n   \n     \n \n</div>\n\n\n<!--\n<div class=\"container\">\n  <div class=\"row\">\n    <div class=\"col-sm\">\n      <button class=\"button\">Green</button>\n    </div>\n    <div class=\"col-sm\">\n      <button class=\"button\">Green</button>\n    </div>\n    <div class=\"col-sm\">\n        <button class=\"button\">Green</button>\n     \n    </div>\n\n    <div class=\"col-sm\">\n        <button class=\"button\">Green</button>\n     \n    </div>\n\n   \n  </div>\n</div>\n-->\n\n\n"

/***/ }),

/***/ "./src/app/header/header.component.ts":
/*!********************************************!*\
  !*** ./src/app/header/header.component.ts ***!
  \********************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./src/node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_push_notifications__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-push-notifications */ "./node_modules/ngx-push-notifications/fesm5/ngx-push-notifications.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _header_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./header.service */ "./src/app/header/header.service.ts");
/* harmony import */ var _sockets_chat_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../sockets/chat.service */ "./src/app/sockets/chat.service.ts");









var HeaderComponent = /** @class */ (function () {
    function HeaderComponent(chat, _pushNotificationService, router, http, HeaderService) {
        this.chat = chat;
        this._pushNotificationService = _pushNotificationService;
        this.router = router;
        this.http = http;
        this.HeaderService = HeaderService;
        this.glo = 0;
        this.timeLeft = 60;
        this.navbarOpen = false;
    }
    HeaderComponent.prototype.getClustering = function (energycontent) {
        var _this = this;
        this.HeaderService.getClusteringValues("Emily", /*"low"*/ energycontent).subscribe(function (res) {
            var i = JSON.stringify(res);
            var stringify = JSON.parse(i);
            stringify.forEach(function (element) {
                //  alert("Hier is is:"+i);
                // alert(element.username)
                // alert(element.name)
                //alert(element.type)
                // alert(element.wayofcooking)
                // alert(element.calories)
                //alert(element.nutrition)  
                //alert("Energycontent"+element.energycontent);
                _this.HeaderService.setBayssian(element.username, element.name, element.energycontent, element.nutrition, element.calories, element.wayofcooking).subscribe(function (res) {
                    var i = JSON.stringify(res);
                    var stringify = JSON.parse(i);
                    alert("Posted");
                }, function (error) {
                    alert("error hier");
                });
            });
            ///////////////////////////////////////////////////////////
            /*
            this.HeaderService.setBayssian("Emily","XA","XA2","XA3","XA4").subscribe((res)=>{
              var i=JSON.stringify(res);
              var stringify = JSON.parse(i);
              stringify.forEach(element => {
                alert(element.username)
                alert(element.name)
                alert(element.type)
                alert(element.wayofcooking)
                alert(element.calories)
                alert(element.nutrition)
              });
            
            
             
            }, (error) =>
            {
                alert("error hier");
               
            });
            */
            ////////////////////////////////////////////////////////////  
        }, function (error) {
            alert("error hier");
        });
    };
    HeaderComponent.prototype.getClustering2 = function (energycontent) {
        var _this = this;
        this.HeaderService.getClusteringValues("Emily", /*"low"*/ energycontent).subscribe(function (res) {
            var i = JSON.stringify(res);
            var stringify = JSON.parse(i);
            stringify.forEach(function (element) {
                //  alert("Hier is is:"+i);
                // alert(element.username)
                // alert(element.name)
                //alert(element.type)
                // alert(element.wayofcooking)
                // alert(element.calories)
                //alert(element.nutrition)  
                //alert("Energycontent"+element.energycontent);
                _this.HeaderService.setBayssian2(element.username, element.name, element.energycontent, element.nutrition, element.calories, element.wayofcooking).subscribe(function (res) {
                    var i = JSON.stringify(res);
                    var stringify = JSON.parse(i);
                    alert("Posted");
                }, function (error) {
                    alert("error hier");
                });
            });
            ///////////////////////////////////////////////////////////
            /*
            this.HeaderService.setBayssian("Emily","XA","XA2","XA3","XA4").subscribe((res)=>{
              var i=JSON.stringify(res);
              var stringify = JSON.parse(i);
              stringify.forEach(element => {
                alert(element.username)
                alert(element.name)
                alert(element.type)
                alert(element.wayofcooking)
                alert(element.calories)
                alert(element.nutrition)
              });
            
            
             
            }, (error) =>
            {
                alert("error hier");
               
            });
            */
            ////////////////////////////////////////////////////////////  
        }, function (error) {
            alert("error hier");
        });
    };
    HeaderComponent.prototype.notification3 = function () {
        var _this = this;
        this.getClustering("low");
        var d = new Date(); // for now
        d.getHours(); // => 9
        d.getMinutes(); // =>  30
        d.getSeconds(); // => 51
        alert("Todays" + d.getDay());
        var e = String(this.days(/*d.getDay()*/ d.getDay() - 1) + " at " + d.getHours() + ":" + d.getMinutes());
        this.http.get('http://139.91.200.103:3000/api/things/getFood/Emily', { responseType: "json" }).subscribe(function (res) {
            var flag = 0;
            var i = JSON.stringify(res[0]);
            var stringify = JSON.parse(i);
            alert("Stringify" + stringify.food1);
            alert("Day breakfast is:" + String(e));
            _this.HeaderService.setHistory("emily", stringify.food1, stringify.food2, "-", "-", "monday", "high", String(e), "Breakfast").subscribe(function (res) {
                //alert("Heys setHistory");
                var i = JSON.stringify(res);
                var stringify = JSON.parse(i);
            }, function (error) {
                alert("error hier");
            });
        }, function (error) {
        });
        /*
        
        
        .subscribe((res)=>{
             // alert("took");
              var flag=0;
              
             // alert("continue");
              //alert(JSON.stringify(res));
        
        
              
              //console.log("called");
              //alert("Clled");
              //alert("cabled"+res[0].info);
              //  console.log(res);
            }, (error) =>
            {
              //alert("error hier");
            //  alert(error);
                // if the request fails, this callback will be invoked
            });
        
        
        
        */
        function test() {
            // this.glo==0;
            // alert("finalaa");
            //alert(location.host);
            //alert(location.hostname);
            // alert("concat");
            // alert(location.host.toString().concat("/main"));
            var i = location.host.toString().concat("/notification").toString().trim();
            // alert(i);
            var all = "http://".concat(i);
            // alert("all are");
            //alert(all);
            window.location.href = all; // i;//'http://www.google.com';
        }
        var title = 'Meal Recommendations';
        var options = new ngx_push_notifications__WEBPACK_IMPORTED_MODULE_2__["PushNotificationOptions"]();
        options.body = 'Its time for breakfast now';
        options.icon = 'https://banner2.kisspng.com/20180219/zqq/kisspng-hamburger-fast-food-restaurant-junk-food-kfc-cartoon-french-fries-5a8b6353d9a269.5412864515190843718914.jpg'; //adding an icon
        this._pushNotificationService.create(title, options).subscribe(function (notif) {
            if (notif.event.type === 'show') {
                console.log('onshow');
                setTimeout(function () {
                }, 3000);
            }
            if (notif.event.type === 'click') {
                console.log('click');
                test();
                notif.notification.close();
            }
            if (notif.event.type === 'close') {
                console.log('close');
            }
        }, function (err) {
            console.log(err);
        });
    };
    HeaderComponent.prototype.notification2 = function () {
        var _this = this;
        alert("Not 2 called");
        this.getClustering("high");
        /////////////////////////////////////////////////////////////////////////////////////
        //this.getClustering("high");
        var d = new Date(); // for now
        d.getHours(); // => 9
        d.getMinutes(); // =>  30
        d.getSeconds(); // => 51
        var e = String(this.days(d.getDay() - 1) + " at " + d.getHours() + ":" + d.getMinutes());
        var name = sessionStorage.getItem("lastname").toString().trim();
        this.http.get("http://139.91.200.103:3000/api/things/getFood/" + name, { responseType: "json" }).subscribe(function (res) {
            // alert("took");
            var flag = 0;
            var i = JSON.stringify(res[0]);
            var stringify = JSON.parse(i);
            alert("Stringify" + stringify.food1);
            _this.HeaderService.setHistory("emily", stringify.food1, stringify.food2, "-", "-", "monday", "high", String(e), "Dinner").subscribe(function (res) {
                //alert("Heys setHistory");
                var i = JSON.stringify(res);
                var stringify = JSON.parse(i);
            }, function (error) {
                alert("error hier");
            });
        }, function (error) {
        });
        /////////////////////////////////////////////////////////////////////////////
        function test() {
            // this.glo==0;
            // alert("finalaa");
            //alert(location.host);
            //alert(location.hostname);
            // alert("concat");
            // alert(location.host.toString().concat("/main"));
            var i = location.host.toString().concat("/notification").toString().trim();
            // alert(i);
            var all = "http://".concat(i);
            // alert("all are");
            //alert(all);
            window.location.href = all; // i;//'http://www.google.com';
        }
        var title = 'Meal Recommendations';
        var options = new ngx_push_notifications__WEBPACK_IMPORTED_MODULE_2__["PushNotificationOptions"]();
        options.body = 'Its time for dinner now';
        options.icon = 'https://banner2.kisspng.com/20180219/zqq/kisspng-hamburger-fast-food-restaurant-junk-food-kfc-cartoon-french-fries-5a8b6353d9a269.5412864515190843718914.jpg'; //adding an icon
        this._pushNotificationService.create(title, options).subscribe(function (notif) {
            if (notif.event.type === 'show') {
                console.log('onshow');
                setTimeout(function () {
                }, 3000);
            }
            if (notif.event.type === 'click') {
                console.log('click');
                test();
                notif.notification.close();
            }
            if (notif.event.type === 'close') {
                console.log('close');
            }
        }, function (err) {
            console.log(err);
        });
    };
    HeaderComponent.prototype.notification = function () {
        var _this = this;
        this.getClustering("medium");
        /////////////////////////////////////////////////////////////////////////////////////
        //this.getClustering("high");
        var d = new Date(); // for now
        d.getHours(); // => 9
        d.getMinutes(); // =>  30
        d.getSeconds(); // => 51
        var e = String(this.days(d.getDay() - 1) + " at " + d.getHours() + ":" + d.getMinutes());
        var name = sessionStorage.getItem("lastname").toString().trim();
        this.http.get("http://139.91.200.103:3000/api/things/getFood/" + name, { responseType: "json" }).subscribe(function (res) {
            // alert("took");
            var flag = 0;
            var i = JSON.stringify(res[0]);
            var stringify = JSON.parse(i);
            _this.HeaderService.setHistory("emily", stringify.food1, stringify.food2, "-", "-", "monday", "medium", String(e), "Lunch").subscribe(function (res) {
                //alert("Heys setHistory");
                var i = JSON.stringify(res);
                var stringify = JSON.parse(i);
            }, function (error) {
                alert("error hier");
            });
        }, function (error) {
        });
        function test() {
            // this.glo==0;
            // alert("finalaa");
            //alert(location.host);
            //alert(location.hostname);
            // alert("concat");
            // alert(location.host.toString().concat("/main"));
            var i = location.host.toString().concat("/notification").toString().trim();
            // alert(i);
            var all = "http://".concat(i);
            // alert("all are");
            //alert(all);
            window.location.href = all; // i;//'http://www.google.com';
        }
        var title = 'Meal Recommendations';
        var options = new ngx_push_notifications__WEBPACK_IMPORTED_MODULE_2__["PushNotificationOptions"]();
        options.body = 'Its time for lunch now';
        options.icon = 'https://banner2.kisspng.com/20180219/zqq/kisspng-hamburger-fast-food-restaurant-junk-food-kfc-cartoon-french-fries-5a8b6353d9a269.5412864515190843718914.jpg'; //adding an icon
        this._pushNotificationService.create(title, options).subscribe(function (notif) {
            if (notif.event.type === 'show') {
                console.log('onshow');
                setTimeout(function () {
                }, 3000);
            }
            if (notif.event.type === 'click') {
                console.log('click');
                test();
                notif.notification.close();
            }
            if (notif.event.type === 'close') {
                console.log('close');
            }
        }, function (err) {
            console.log(err);
        });
    };
    HeaderComponent.prototype.oberserableTimer = function () {
        var _this = this;
        var source = Object(rxjs__WEBPACK_IMPORTED_MODULE_6__["timer"])(1000, 20000);
        var abc = source.subscribe(function (val) {
            console.log(val, '-');
            console.log(val);
            var today = new Date();
            alert("in http");
            var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
            console.log("Now time time" + time);
            var name = document.getElementById("session").innerHTML = sessionStorage.getItem("lastname");
            _this.http.get("http://localhost:3000/api/things/getsensorvalues/" + name, { responseType: "json" }).subscribe(function (response) {
                var sample = JSON.parse(JSON.stringify(response));
                var a = String(sample.energycontent);
                var b = (sample.time);
                console.log("cur time" + time + "glo time" + b);
                if ((a == "high") /*&&(time==b)*/) {
                    _this.glo++;
                    document.getElementById("not3").click();
                    // alert("Scenario called"+a);
                    //  setInterval(function () {document.getElementById("not").click();}, 10000);
                }
                else if ((a == "medium") /*&&(time==b)*/) {
                    _this.glo++;
                    document.getElementById("not2").click();
                    // alert("Scenario called"+a);
                    //  setInterval(function () {document.getElementById("not").click();}, 10000);
                }
                else if ((a == "low") /*&&(time==b)*/) {
                    _this.glo++;
                    document.getElementById("not").click();
                }
            }); ///hier end calendar
            _this.subscribeTimer = _this.timeLeft - val;
        });
        ///////////////////////////////SENSOR2 BA;UES HIER
    };
    HeaderComponent.prototype.days = function (number) {
        if (number == 1) {
            return "Monday";
        }
        else if (number == 2) {
            return "Tuesday";
        }
        else if (number == 3) {
            return "Wednesday";
        }
        else if (number == 4) {
            return "Thursday";
        }
        else if (number == 5) {
            return "Friday";
        }
    };
    HeaderComponent.prototype.BmrAlgorithm = function (gender, weight, height, age) {
        //  alert("Values Bmr are:"+gender+weight+height+age);
        // String:s="";
        var bmr = 0;
        if (gender == "male") {
            bmr = 66.5 + (13.75 * Number(weight)) + (5.003 * Number(height)) - (6.755 * Number(age));
        }
        else if (gender == "female") {
            bmr = 655.1 + (9.563 * Number(weight)) + (1.850 * Number(height)) - (4.676 * Number(age));
        }
        return bmr;
    };
    HeaderComponent.prototype.ngOnInit = function () {
        var _this = this;
        //var name= document.getElementById("session").innerHTML = sessionStorage.getItem("lastname");
        // $('#session').show();
        jquery__WEBPACK_IMPORTED_MODULE_3__('#alllist').hide();
        //$('#main').show();
        jquery__WEBPACK_IMPORTED_MODULE_3__('#button').hide();
        jquery__WEBPACK_IMPORTED_MODULE_3__('#button2').hide();
        jquery__WEBPACK_IMPORTED_MODULE_3__('#button3').hide();
        // $('#session').show();
        // $('#back').hide();
        // $('#date').hide();
        ////////////////////
        this.chat.messages.subscribe(function (msg) {
            console.log("Messages are:");
            alert("not Cllledddddd" + msg.text);
            console.log(msg.text);
            if (String(msg.text) === "inhouse/notbeats/sitting/undefined") {
                alert("not3");
                document.getElementById("not3").click();
            }
            else if (msg.text == "inhouse/beats/notsitting/undefined") {
                alert("not2");
                document.getElementById("not2").click();
            }
            else if (msg.text == "inhouse/notbeats/notsitting/undefined") {
                document.getElementById("not").click();
            }
            else {
                alert("eb want fdak");
            }
            // document.getElementById("not3").click();
            if (msg.text == "not") {
                //this.oberserableTimer();
            }
        });
        /////////////////////
        // this.oberserableTimer();
        function delay(ms) {
            return new Promise(function (resolve) { return setTimeout(resolve, ms); });
        }
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(1000)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        }); })();
        //setTimeout(notification(), 2000);
        // $('#maincontenthome').hide();
        //maincontenthome
        /*  var elem = document.getElementById("myBar");
          var width = 1;
          var id = setInterval(frame, 10);
          function frame() {
            //alert("frame");
            if (width >= 100) {
              alert("called");
              clearInterval(id);
              $('#maincontenthome').show();
              alert("alles");
             // this.goToPage('/', 'main');
            } else {
              width++;
              elem.style.width = width + '%';
            }
          }*/
        document.getElementById("home").style.backgroundColor = "#696969";
        //document.getElementById("button1").style.backgroundColor="#FFFFFF";
        jquery__WEBPACK_IMPORTED_MODULE_3__(document).ready(function () {
            // $("#maincontenthome").hide();
            // $("#maincontentanalytic").hide();
            //alert("Clicked");
            /*
                  $("button").click(function(){
                  
                  });
                  */
        });
        /*  $(document).ready(function(){
            $("button").click(function(){
              $("p").hide();
      
      
            });
          });*/
        //
        var global_settings = { percentage: "a", text: "a", starting_position: 1,
            ending_position: 1, width: 1, height: 1, percent: 0, color: "a", line_width: 1,
            counter_clockwise: true
        };
        (function ($) {
            var methods = {
                init: function (options) {
                    // This is the easiest way to have default options.
                    var settings = $.extend({
                        // These are the defaults.
                        color: "#000000",
                        height: "300px",
                        width: "300px",
                        line_width: 8,
                        starting_position: 25,
                        percent: 100,
                        counter_clockwise: false,
                        percentage: true,
                        text: ''
                    }, options);
                    global_settings = settings;
                    // Create percentage
                    var percentage = $("<div class='progress-percentage'></div>");
                    if (!global_settings.percentage) {
                        percentage.text(global_settings.percentage);
                    }
                    $(this).append(percentage);
                    // Create text
                    var text = $("<div class='progress-text'></div>");
                    // Custom text
                    if (global_settings.text != "percent") {
                        text.text(global_settings.text);
                    }
                    $(this).append(text);
                    // Correct any invalid values
                    if (global_settings.starting_position != 100) {
                        global_settings.starting_position = global_settings.starting_position % 100;
                    }
                    if (global_settings.ending_position != 100) {
                        global_settings.ending_position = global_settings.ending_position % 100;
                    }
                    // No 'px' or '%', add 'px'
                    appendUnit(global_settings.width);
                    appendUnit(global_settings.height);
                    // Apply global_settings
                    $(this).css({
                        "height": global_settings.height,
                        "width": global_settings.width
                    });
                    $(this).addClass("circular-progress-bar");
                    // Remove old canvas
                    $(this).find("canvas").remove();
                    // Put canvas inside this
                    $(this).append(createCanvas($(this)));
                    // Return allows for chaining
                    return this;
                },
                percent: function (value) {
                    // Change percent
                    global_settings.percent = value;
                    // Apply global_settings
                    $(this).css({
                        "height": global_settings.height,
                        "width": global_settings.width
                    });
                    // Remove old canvas
                    $(this).children("canvas").remove();
                    // Put canvas inside this
                    $(this).append(createCanvas($(this)));
                    // Return allows for chaining
                    return this;
                },
                animate: function (value, time) {
                    // Apply global_settings
                    // Number of intervals, 10ms interval
                    var num_of_steps = time / 10;
                    // Amount of change each step
                    var percent_change = (value - global_settings.percent) / num_of_steps;
                    // Variable conflict, rename this
                    var scope = $(this);
                    var theInterval = setInterval(function () {
                        if (global_settings.percent < value) {
                            // Remove old canvas
                            scope.children("canvas").remove();
                            // Increment percent
                            global_settings.percent += percent_change;
                            // Put canvas inside this
                            scope.append(createCanvas(scope));
                        }
                        else {
                            clearInterval(theInterval);
                        }
                    }, 10);
                    // Return allows for chaining
                    return this;
                }
            };
            $.fn.circularProgress = function (methodOrOptions) {
                if (methods[methodOrOptions]) {
                    // Method found
                    return methods[methodOrOptions].apply(this, Array.prototype.slice.call(arguments, 1));
                }
                else if (typeof methodOrOptions === 'object' || !methodOrOptions) {
                    // Default to "init", object passed in or nothing passed in
                    return methods.init.apply(this, arguments);
                }
                else {
                    $.error('Method ' + methodOrOptions + ' does not exist.');
                }
            };
            /* =========================================================================
                PRIVATE FUNCTIONS
            ========================================================================= */
            // return string without 'px' or '%'
            function removeUnit(apples) {
                if (apples.indexOf("px")) {
                    return apples.substring(0, apples.length - 2);
                }
            }
            ;
            // return string with 'px'
            function appendUnit(apples) {
                if (apples.toString().indexOf("px") < -1 && apples.toString().indexOf("%") < -1) {
                    return apples += "px";
                }
            }
            ;
            // calculate starting position on canvas
            function calcPos(apples, percent) {
                if (percent < 0) {
                    // Calculate starting position
                    var starting_degree = (parseInt(apples) / 100) * 360;
                    var starting_radian = starting_degree * (Math.PI / 180);
                    return starting_radian - (Math.PI / 2);
                }
                else {
                    // Calculate ending position
                    var ending_degree = ((parseInt(apples) + parseInt(percent)) / 100) * 360;
                    var ending_radian = ending_degree * (Math.PI / 180);
                    return ending_radian - (Math.PI / 2);
                }
            }
            ;
            // Put percentage or custom text inside progress circle
            function insertText(scope) {
                $(".progress-percentage").text(Math.round(global_settings.percent) + "%");
            }
            // create canvas
            function createCanvas(scope) {
                // Remove 'px' or '%'
                var canvas_height = removeUnit(global_settings.height.toString());
                var canvas_width = removeUnit(global_settings.width.toString());
                // Create canvas
                var canvas = document.createElement("canvas");
                canvas.height = canvas_height;
                canvas.width = canvas_width;
                // Create drawable canvas and apply properties
                var ctx = canvas.getContext("2d");
                ctx.strokeStyle = global_settings.color;
                ctx.lineWidth = global_settings.line_width;
                // Draw arc
                ctx.beginPath();
                // Calculate starting and ending positions
                var starting_radian = calcPos(global_settings.starting_position, -1);
                var ending_radian = calcPos(global_settings.starting_position, global_settings.percent);
                // Calculate radius and x,y coordinates
                var radius = 0;
                var xcoord = canvas_width / 2;
                var ycoord = canvas_height / 2;
                // Height or width greater
                if (canvas_height >= canvas_width) {
                    radius = canvas_width * 0.9 / 2 - (global_settings.line_width * 2);
                }
                else {
                    radius = canvas_height * 0.9 / 2 - (global_settings.line_width * 2);
                }
                /*
                    x coordinate
                    y coordinate
                    radius of circle
                    starting angle in radians
                    ending angle in radians
                    clockwise (false, default) or counter-clockwise (true)
                */
                ctx.arc(xcoord, ycoord, radius, starting_radian, ending_radian, global_settings.counter_clockwise);
                ctx.stroke();
                // Add text
                if (global_settings.percentage) {
                    insertText(scope);
                }
                return canvas;
            }
            ;
        }(jquery__WEBPACK_IMPORTED_MODULE_3__));
        ///////////////////////////////
        var name = sessionStorage.getItem("lastname");
        var calories = 0;
        this.HeaderService.getBmrValues(name).subscribe(function (res) {
            //  alert(this.username+this.password+this.age+this.weight);
            var sample = JSON.stringify(res);
            var js = JSON.parse(sample);
            //alert("js is:"+js[0].username);
            //alert("js is:"+js[0].password);
            //alert("js is:"+js[0].gender);
            //alert("js is:"+js[0].age);
            //alert("js is:"+js[0].height);
            //alert("js is:"+js[0].weight);
            //gender:String,weight:String,height:String,age:String
            //this.BmrAlgorithm(js[0].gender,js[0].weight,js[0].height,js[0].age);
            /*if sedentary BMR*1.2
              IF LIGHTLY ACTIVE(SPOSRTS 1-3 WEEK)bmr*1.375
              IF MODERATELY active (3-5 DAYS PER WEEK) bmr*1.55
              IF VERY ACTIVE( 6-7 days paer week)  BMR*1725
              IF EXTRA ACTIVE(VERY HARD EXERCISE PSYSICAL JOB AND TRAINING) BMR*1.9
         
            */
            var flag = 0;
            // alert("HeaderService bmr"+this.BmrAlgorithm(js[0].gender,js[0].weight,js[0].height,js[0].age)*1.375);
            calories = Math.round(_this.BmrAlgorithm(js[0].gender, js[0].weight, js[0].height, js[0].age) * 1.375);
            var final = "1600/" + calories + ' Kcal';
            var sentence = "You should complete " + calories + " Kcal";
            ////////////////////////////////
            //this.BmrAlgorithm("","","","");
            //alert("Bmr out is:"+this.BmrAlgorithm("","","",""));
            document.getElementById("complete").textContent = sentence;
            var a = 20;
            jquery__WEBPACK_IMPORTED_MODULE_3__(".my-progress-bar").circularProgress({
                line_width: 9,
                color: "#ff8000",
                starting_position: 0,
                percent: 0,
                percentage: true,
                text: final
            }).circularProgress('animate', a, 5000);
            //alert(res);
        }, function (error) {
            alert("error hier");
        });
        //`${this.apiURL}${'/'}${username}${'/'}${password}${'/'}${age}${'/'}${weight}`
        //var final='1600/"" Kcal'
        //
    };
    HeaderComponent.prototype.toggleNavbar = function () {
        this.navbarOpen = !this.navbarOpen;
    };
    HeaderComponent.prototype.goToPage = function (pagename, parameter) {
        this.router.navigate([pagename, parameter]);
    };
    HeaderComponent.prototype.submit1 = function () {
        //alert("cliecked");
        //alert("Called it hier1!");
        //alert(this.email);
        // document.body.style.backgroundColor = "red";
        document.getElementById("home").style.backgroundColor = "#696969";
        // $("#maincontenthome").show();
        // this.router.navigate(["http://localhost:8080/main", ""]);
        //window.location.href = "/main";
        // alert("Called submit");
    };
    HeaderComponent.prototype.submit2 = function () {
        //alert("Called it hier1!");
        //alert(this.email);
        // document.body.style.backgroundColor = "red";
        //document.getElementById("button2").style.backgroundColor="#FFFFFF";
        //$("#maincontentanalytic").show();
        //alert("Called submit");
    };
    HeaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-header',
            template: __webpack_require__(/*! ./header.component.html */ "./src/app/header/header.component.html"),
            styles: [__webpack_require__(/*! ./header.component.css */ "./src/app/header/header.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_sockets_chat_service__WEBPACK_IMPORTED_MODULE_8__["ChatService"], ngx_push_notifications__WEBPACK_IMPORTED_MODULE_2__["PushNotificationService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"], _header_service__WEBPACK_IMPORTED_MODULE_7__["HeaderService"]])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/header/header.service.ts":
/*!******************************************!*\
  !*** ./src/app/header/header.service.ts ***!
  \******************************************/
/*! exports provided: HeaderService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderService", function() { return HeaderService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./src/node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");



var HeaderService = /** @class */ (function () {
    function HeaderService(httpClient) {
        this.httpClient = httpClient;
        this.apiURL = 'http://139.91.200.103:3000/api/things/sethistoryvalue/';
        this.apiURLClass = 'http://139.91.200.103:3000/api/things/getclassification/';
        this.apiURLClassBays = 'http://139.91.200.103:3000/api/things/setsensorsvalues/';
        this.apiURLClassBays2 = 'http://139.91.200.103:3000/api/things/setsensorsvalues2/';
    }
    HeaderService.prototype.getBmrValues = function (username) {
        var data = {
            "name": "fr"
        };
        return this.httpClient.get("http://139.91.200.103:3000/api/things/getBmrValues/" + username, { responseType: "json" });
    };
    HeaderService.prototype.setHistory = function (username, name, name2, name3, name4, day, energycontent, date, type) {
        var data = {
            "name": "fr"
        };
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
            })
        };
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
            .set('username', String(username))
            .set('name', String(name))
            .set('name2', String(name2))
            .set('name3', String(name3))
            .set('name4', String(name4))
            .set('day', String(day))
            .set('energycontent', String(energycontent))
            .set('date', String(date))
            .set('type', String(type));
        var options = {
            httpOptions: httpOptions,
            params: params,
            withCredentials: false
        };
        return this.httpClient.post("" + this.apiURL, null, options);
    };
    HeaderService.prototype.getClusteringValues = function (username, energycontent) {
        var data = {
            "name": "fr"
        };
        return this.httpClient.get("" + this.apiURLClass + '/' + username + '/' + energycontent, { responseType: "json" });
    };
    HeaderService.prototype.setBayssian = function (username, name, energycontent, nutrition, calories, wayofcooking) {
        var data = {
            "name": "fr"
        };
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
            })
        };
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
            .set('username', String(username))
            .set('energycontent', String(energycontent))
            .set('nutrition', String(nutrition))
            .set('calories', String(calories))
            .set('wayofcooking', String(wayofcooking))
            .set('name', String(name));
        var options = {
            httpOptions: httpOptions,
            params: params,
            withCredentials: false
        };
        return this.httpClient.post("" + this.apiURLClassBays, null, options);
    };
    HeaderService.prototype.setBayssian2 = function (username, name, energycontent, nutrition, calories, wayofcooking) {
        var data = {
            "name": "fr"
        };
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
            })
        };
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
            .set('username', String(username))
            .set('energycontent', String(energycontent))
            .set('nutrition', String(nutrition))
            .set('calories', String(calories))
            .set('wayofcooking', String(wayofcooking))
            .set('name', String(name));
        var options = {
            httpOptions: httpOptions,
            params: params,
            withCredentials: false
        };
        return this.httpClient.post("" + this.apiURLClassBays2, null, options);
    };
    HeaderService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], HeaderService);
    return HeaderService;
}());



/***/ }),

/***/ "./src/app/infos/infos.component.css":
/*!*******************************************!*\
  !*** ./src/app/infos/infos.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#button1 {\r\n    background-color: #b2b2b2\t; \r\n    border: none;\r\n    color: white;\r\n    padding-bottom: 25px 22px;\r\n    text-align: center;\r\n    text-decoration: none;\r\n    display: inline-block;\r\n    font-size: 36px;\r\n    margin: 4px 18px;\r\n    cursor: pointer;\r\n    \r\n  }\r\n\r\n  \r\n#button2 {\r\n   background-color: #b2b2b2\t; \r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n\r\n  \r\n#button3 {\r\n   background-color: #b2b2b2\t; \r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n\r\n  \r\n#button4 {\r\n   background-color: #b2b2b2\t;\r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n\r\n  \r\n#buttona {\r\n    border-radius: 25px;\r\n    background-color: #b2b2b2\t; \r\n    border: none;\r\n    color: white;\r\n    padding-bottom: 25px 22px;\r\n    text-align: center;\r\n    text-decoration: none;\r\n    display: inline-block;\r\n    font-size: 36px;\r\n    margin: 4px 18px;\r\n    cursor: pointer;\r\n    \r\n  }\r\n\r\n  \r\n#buttonb {\r\n    border-radius: 25px;\r\n   background-color: #b2b2b2\t; \r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n\r\n  \r\n#buttonc {\r\n    border-radius: 25px;\r\n   background-color: #b2b2b2\t; \r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n\r\n  \r\n#buttond {\r\n    border-radius: 25px;\r\n   background-color: #b2b2b2\t;\r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n\r\n  \r\n#buttone {\r\n    border-radius: 25px;\r\n    background-color: #b2b2b2\t;\r\n    border: none;\r\n    color: white;\r\n    padding-bottom: 25px 22px;\r\n    text-align: center;\r\n    text-decoration: none;\r\n    display: inline-block;\r\n    font-size: 36px;\r\n    margin: 4px 18px;\r\n    cursor: pointer;\r\n  }\r\n\r\n  \r\n#all{\r\n     text-align: center;\r\n     position: fixed;\r\n     left: 0;\r\n     bottom: 0;\r\n     width: 100%;\r\n     background-color: \t\t#b2b2b2;\r\n     color: white;\r\n     text-align: center;\r\n  }\r\n\r\n  \r\ndiv.home {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\n\r\n  \r\ndiv.home input {\r\n    background: url('home.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\n\r\n  \r\ndiv.analytic {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\n\r\n  \r\ndiv.analytic input {\r\n    background: url('analytic.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\n\r\n  \r\ndiv.notification {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\n\r\n  \r\ndiv.notification input {\r\n    background: url('outline-today-24px.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\n\r\n  \r\ndiv.setting {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\n\r\n  \r\ndiv.setting input {\r\n    background: url('settings.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\n\r\n  \r\nspan.home {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 6px;\r\n  }\r\n\r\n  \r\nspan.analytic {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 2px;\r\n  }\r\n\r\n  \r\nspan.notification {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 2px;\r\n  }\r\n\r\n  \r\nspan.setting {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 12px;\r\n  }\r\n\r\n  \r\nimg {\r\n    width: 100%;\r\n    height: auto;\r\n    max-height: 400px;\r\n   /* background: url(loading.gif) 50% no-repeat;*/\r\n   /* border: 1px solid black;*/\r\n   /* border-radius: 5px;*/\r\n  }\r\n\r\n  \r\n.grid-container {\r\n    display: grid;\r\n    grid-template-columns: auto auto ;\r\n    grid-gap: 10px;\r\n    background-color: #ffffff;\r\n    padding: 10px;\r\n  }\r\n\r\n  \r\n.grid-container > div {\r\n    background-color: rgba(255, 255, 255, 0.8);\r\n    text-align: center;\r\n    padding: 20px 1;\r\n    font-size: 30px;\r\n  }\r\n\r\n  \r\n.grid-container-last {\r\n    display: grid;\r\n    grid-template-columns: auto ;\r\n    grid-gap: 10px;\r\n    background-color: #FFFFFF;\r\n    padding: 10px;\r\n  }\r\n\r\n  \r\n.grid-container-last > div {\r\n    background-color: rgba(255, 255, 255, 0.8);\r\n    text-align: center;\r\n    padding: 20px 1;\r\n    font-size: 30px;\r\n  }\r\n\r\n  \r\n#header{\r\n     \r\n      color:grey;\r\n\r\n\r\n  }\r\n\r\n  \r\n#subheader{\r\n   \r\n    color:#00ff40;\r\n\r\n\r\n}\r\n\r\n  \r\n/*\r\n  #bullets{\r\n  \r\n   \r\n\r\n}*/\r\n\r\n  \r\n/*  #third{\r\n    text-align: left\r\n\r\n  }\r\n\r\n  #fifth{\r\n    text-align: left\r\n  }\r\n*/\r\n\r\n  \r\ndiv.bullet1 input {\r\n    background: url('round-brightness_1-24px.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\n\r\n  \r\nspan.bullet1 {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 2px;\r\n  }\r\n\r\n  \r\ndiv.bullet1 {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\n\r\n  \r\n.loader {\r\n  border: 16px solid #f3f3f3;\r\n  border-radius: 50%;\r\n  border-top: 16px solid #ff8000;\r\n  margin: auto;\r\n  width: 120px;\r\n  height: 120px;\r\n  -webkit-animation: spin 2s linear infinite; /* Safari */\r\n  animation: spin 2s linear infinite;\r\n}\r\n\r\n  \r\n/* Safari */\r\n\r\n  \r\n@-webkit-keyframes spin {\r\n  0% { -webkit-transform: rotate(0deg); }\r\n  100% { -webkit-transform: rotate(360deg); }\r\n}\r\n\r\n  \r\n@keyframes spin {\r\n  0% { transform: rotate(0deg); }\r\n  100% { transform: rotate(360deg); }\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9pbmZvcy9pbmZvcy5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksMEJBQTBCO0lBQzFCLFlBQVk7SUFDWixZQUFZO0lBQ1oseUJBQXlCO0lBQ3pCLGtCQUFrQjtJQUNsQixxQkFBcUI7SUFDckIscUJBQXFCO0lBQ3JCLGVBQWU7SUFDZixnQkFBZ0I7SUFDaEIsZUFBZTs7RUFFakI7OztBQUdGO0dBQ0csMEJBQTBCO0dBQzFCLFlBQVk7R0FDWixZQUFZO0dBQ1oseUJBQXlCO0dBQ3pCLGtCQUFrQjtHQUNsQixxQkFBcUI7R0FDckIscUJBQXFCO0dBQ3JCLGVBQWU7R0FDZixnQkFBZ0I7R0FDaEIsZUFBZTtDQUNqQjs7O0FBQ0E7R0FDRSwwQkFBMEI7R0FDMUIsWUFBWTtHQUNaLFlBQVk7R0FDWix5QkFBeUI7R0FDekIsa0JBQWtCO0dBQ2xCLHFCQUFxQjtHQUNyQixxQkFBcUI7R0FDckIsZUFBZTtHQUNmLGdCQUFnQjtHQUNoQixlQUFlO0NBQ2pCOzs7QUFFQTtHQUNFLDBCQUEwQjtHQUMxQixZQUFZO0dBQ1osWUFBWTtHQUNaLHlCQUF5QjtHQUN6QixrQkFBa0I7R0FDbEIscUJBQXFCO0dBQ3JCLHFCQUFxQjtHQUNyQixlQUFlO0dBQ2YsZ0JBQWdCO0dBQ2hCLGVBQWU7Q0FDakI7OztBQUtBO0lBQ0csbUJBQW1CO0lBQ25CLDBCQUEwQjtJQUMxQixZQUFZO0lBQ1osWUFBWTtJQUNaLHlCQUF5QjtJQUN6QixrQkFBa0I7SUFDbEIscUJBQXFCO0lBQ3JCLHFCQUFxQjtJQUNyQixlQUFlO0lBQ2YsZ0JBQWdCO0lBQ2hCLGVBQWU7O0VBRWpCOzs7QUFHRjtJQUNJLG1CQUFtQjtHQUNwQiwwQkFBMEI7R0FDMUIsWUFBWTtHQUNaLFlBQVk7R0FDWix5QkFBeUI7R0FDekIsa0JBQWtCO0dBQ2xCLHFCQUFxQjtHQUNyQixxQkFBcUI7R0FDckIsZUFBZTtHQUNmLGdCQUFnQjtHQUNoQixlQUFlO0NBQ2pCOzs7QUFDQTtJQUNHLG1CQUFtQjtHQUNwQiwwQkFBMEI7R0FDMUIsWUFBWTtHQUNaLFlBQVk7R0FDWix5QkFBeUI7R0FDekIsa0JBQWtCO0dBQ2xCLHFCQUFxQjtHQUNyQixxQkFBcUI7R0FDckIsZUFBZTtHQUNmLGdCQUFnQjtHQUNoQixlQUFlO0NBQ2pCOzs7QUFFQTtJQUNHLG1CQUFtQjtHQUNwQiwwQkFBMEI7R0FDMUIsWUFBWTtHQUNaLFlBQVk7R0FDWix5QkFBeUI7R0FDekIsa0JBQWtCO0dBQ2xCLHFCQUFxQjtHQUNyQixxQkFBcUI7R0FDckIsZUFBZTtHQUNmLGdCQUFnQjtHQUNoQixlQUFlO0NBQ2pCOzs7QUFDQTtJQUNHLG1CQUFtQjtJQUNuQiwwQkFBMEI7SUFDMUIsWUFBWTtJQUNaLFlBQVk7SUFDWix5QkFBeUI7SUFDekIsa0JBQWtCO0lBQ2xCLHFCQUFxQjtJQUNyQixxQkFBcUI7SUFDckIsZUFBZTtJQUNmLGdCQUFnQjtJQUNoQixlQUFlO0VBQ2pCOzs7QUFHQTtLQUNHLGtCQUFrQjtLQUNsQixlQUFlO0tBQ2YsT0FBTztLQUNQLFNBQVM7S0FDVCxXQUFXO0tBQ1gsMkJBQTJCO0tBQzNCLFlBQVk7S0FDWixrQkFBa0I7RUFDckI7OztBQUVBO0lBQ0Usa0JBQWtCO0lBQ2xCLHFCQUFxQjtFQUN2Qjs7O0FBRUE7SUFDRSxxQ0FBMkQ7SUFDM0QsZUFBZTtJQUNmLFdBQVc7SUFDWCxZQUFZO0lBQ1osWUFBWTtFQUNkOzs7QUFDQTtJQUNFLGtCQUFrQjtJQUNsQixxQkFBcUI7RUFDdkI7OztBQUVBO0lBQ0UseUNBQStEO0lBQy9ELGVBQWU7SUFDZixXQUFXO0lBQ1gsWUFBWTtJQUNaLFlBQVk7RUFDZDs7O0FBRUE7SUFDRSxrQkFBa0I7SUFDbEIscUJBQXFCO0VBQ3ZCOzs7QUFFQTtJQUNFLG1EQUEyRTtJQUMzRSxlQUFlO0lBQ2YsV0FBVztJQUNYLFlBQVk7SUFDWixZQUFZO0VBQ2Q7OztBQUVBO0lBQ0Usa0JBQWtCO0lBQ2xCLHFCQUFxQjtFQUN2Qjs7O0FBRUE7SUFDRSx5Q0FBK0Q7SUFDL0QsZUFBZTtJQUNmLFdBQVc7SUFDWCxZQUFZO0lBQ1osWUFBWTtFQUNkOzs7QUFDQTtJQUNFLGtCQUFrQjtJQUNsQixTQUFTO0lBQ1QsU0FBUztFQUNYOzs7QUFFQTtJQUNFLGtCQUFrQjtJQUNsQixTQUFTO0lBQ1QsU0FBUztFQUNYOzs7QUFFQTtJQUNFLGtCQUFrQjtJQUNsQixTQUFTO0lBQ1QsU0FBUztFQUNYOzs7QUFFQTtJQUNFLGtCQUFrQjtJQUNsQixTQUFTO0lBQ1QsVUFBVTtFQUNaOzs7QUFJQTtJQUNFLFdBQVc7SUFDWCxZQUFZO0lBQ1osaUJBQWlCO0dBQ2xCLCtDQUErQztHQUMvQyw0QkFBNEI7R0FDNUIsdUJBQXVCO0VBQ3hCOzs7QUFFQTtJQUNFLGFBQWE7SUFDYixpQ0FBaUM7SUFDakMsY0FBYztJQUNkLHlCQUF5QjtJQUN6QixhQUFhO0VBQ2Y7OztBQUVBO0lBQ0UsMENBQTBDO0lBQzFDLGtCQUFrQjtJQUNsQixlQUFlO0lBQ2YsZUFBZTtFQUNqQjs7O0FBR0E7SUFDRSxhQUFhO0lBQ2IsNEJBQTRCO0lBQzVCLGNBQWM7SUFDZCx5QkFBeUI7SUFDekIsYUFBYTtFQUNmOzs7QUFFQTtJQUNFLDBDQUEwQztJQUMxQyxrQkFBa0I7SUFDbEIsZUFBZTtJQUNmLGVBQWU7RUFDakI7OztBQUVBOztNQUVJLFVBQVU7OztFQUdkOzs7QUFFQTs7SUFFRSxhQUFhOzs7QUFHakI7OztBQUdBOzs7OztFQUtFOzs7QUFFRjs7Ozs7Ozs7Q0FRQzs7O0FBRUM7SUFDRSx3REFBNkU7SUFDN0UsZUFBZTtJQUNmLFdBQVc7SUFDWCxZQUFZO0lBQ1osWUFBWTtFQUNkOzs7QUFFQTtJQUNFLGtCQUFrQjtJQUNsQixTQUFTO0lBQ1QsU0FBUztFQUNYOzs7QUFDQTtJQUNFLGtCQUFrQjtJQUNsQixxQkFBcUI7RUFDdkI7OztBQUdGO0VBQ0UsMEJBQTBCO0VBQzFCLGtCQUFrQjtFQUNsQiw4QkFBOEI7RUFDOUIsWUFBWTtFQUNaLFlBQVk7RUFDWixhQUFhO0VBQ2IsMENBQTBDLEVBQUUsV0FBVztFQUN2RCxrQ0FBa0M7QUFDcEM7OztBQUNBLFdBQVc7OztBQUNYO0VBQ0UsS0FBSywrQkFBK0IsRUFBRTtFQUN0QyxPQUFPLGlDQUFpQyxFQUFFO0FBQzVDOzs7QUFFQTtFQUNFLEtBQUssdUJBQXVCLEVBQUU7RUFDOUIsT0FBTyx5QkFBeUIsRUFBRTtBQUNwQyIsImZpbGUiOiJhcHAvaW5mb3MvaW5mb3MuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIiNidXR0b24xIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNiMmIyYjJcdDsgXHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMjVweCAyMnB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgZm9udC1zaXplOiAzNnB4O1xyXG4gICAgbWFyZ2luOiA0cHggMThweDtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIFxyXG4gIH1cclxuXHJcbiAgXHJcbiNidXR0b24yIHtcclxuICAgYmFja2dyb3VuZC1jb2xvcjogI2IyYjJiMlx0OyBcclxuICAgYm9yZGVyOiBub25lO1xyXG4gICBjb2xvcjogd2hpdGU7XHJcbiAgIHBhZGRpbmctYm90dG9tOiAyNXB4IDIycHg7XHJcbiAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgIGZvbnQtc2l6ZTogMzZweDtcclxuICAgbWFyZ2luOiA0cHggMThweDtcclxuICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gfVxyXG4gI2J1dHRvbjMge1xyXG4gICBiYWNrZ3JvdW5kLWNvbG9yOiAjYjJiMmIyXHQ7IFxyXG4gICBib3JkZXI6IG5vbmU7XHJcbiAgIGNvbG9yOiB3aGl0ZTtcclxuICAgcGFkZGluZy1ib3R0b206IDI1cHggMjJweDtcclxuICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgZm9udC1zaXplOiAzNnB4O1xyXG4gICBtYXJnaW46IDRweCAxOHB4O1xyXG4gICBjdXJzb3I6IHBvaW50ZXI7XHJcbiB9XHJcblxyXG4gI2J1dHRvbjQge1xyXG4gICBiYWNrZ3JvdW5kLWNvbG9yOiAjYjJiMmIyXHQ7XHJcbiAgIGJvcmRlcjogbm9uZTtcclxuICAgY29sb3I6IHdoaXRlO1xyXG4gICBwYWRkaW5nLWJvdHRvbTogMjVweCAyMnB4O1xyXG4gICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICBmb250LXNpemU6IDM2cHg7XHJcbiAgIG1hcmdpbjogNHB4IDE4cHg7XHJcbiAgIGN1cnNvcjogcG9pbnRlcjtcclxuIH1cclxuXHJcblxyXG5cclxuXHJcbiAjYnV0dG9uYSB7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2IyYjJiMlx0OyBcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIHBhZGRpbmctYm90dG9tOiAyNXB4IDIycHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBmb250LXNpemU6IDM2cHg7XHJcbiAgICBtYXJnaW46IDRweCAxOHB4O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgXHJcbiAgfVxyXG5cclxuICBcclxuI2J1dHRvbmIge1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcclxuICAgYmFja2dyb3VuZC1jb2xvcjogI2IyYjJiMlx0OyBcclxuICAgYm9yZGVyOiBub25lO1xyXG4gICBjb2xvcjogd2hpdGU7XHJcbiAgIHBhZGRpbmctYm90dG9tOiAyNXB4IDIycHg7XHJcbiAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgIGZvbnQtc2l6ZTogMzZweDtcclxuICAgbWFyZ2luOiA0cHggMThweDtcclxuICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gfVxyXG4gI2J1dHRvbmMge1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcclxuICAgYmFja2dyb3VuZC1jb2xvcjogI2IyYjJiMlx0OyBcclxuICAgYm9yZGVyOiBub25lO1xyXG4gICBjb2xvcjogd2hpdGU7XHJcbiAgIHBhZGRpbmctYm90dG9tOiAyNXB4IDIycHg7XHJcbiAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgIGZvbnQtc2l6ZTogMzZweDtcclxuICAgbWFyZ2luOiA0cHggMThweDtcclxuICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gfVxyXG5cclxuICNidXR0b25kIHtcclxuICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbiAgIGJhY2tncm91bmQtY29sb3I6ICNiMmIyYjJcdDtcclxuICAgYm9yZGVyOiBub25lO1xyXG4gICBjb2xvcjogd2hpdGU7XHJcbiAgIHBhZGRpbmctYm90dG9tOiAyNXB4IDIycHg7XHJcbiAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgIGZvbnQtc2l6ZTogMzZweDtcclxuICAgbWFyZ2luOiA0cHggMThweDtcclxuICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gfVxyXG4gI2J1dHRvbmUge1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNiMmIyYjJcdDtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIHBhZGRpbmctYm90dG9tOiAyNXB4IDIycHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBmb250LXNpemU6IDM2cHg7XHJcbiAgICBtYXJnaW46IDRweCAxOHB4O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gIH1cclxuICBcclxuIFxyXG4gICNhbGx7XHJcbiAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgIHBvc2l0aW9uOiBmaXhlZDtcclxuICAgICBsZWZ0OiAwO1xyXG4gICAgIGJvdHRvbTogMDtcclxuICAgICB3aWR0aDogMTAwJTtcclxuICAgICBiYWNrZ3JvdW5kLWNvbG9yOiBcdFx0I2IyYjJiMjtcclxuICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIH1cclxuXHJcbiAgZGl2LmhvbWUge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIH1cclxuXHJcbiAgZGl2LmhvbWUgaW5wdXQge1xyXG4gICAgYmFja2dyb3VuZDogdXJsKCdzcmMvYXBwL2hlYWRlci9pbWFnZXMvaG9tZS5zdmcnKSBuby1yZXBlYXQ7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICB3aWR0aDogNTBweDtcclxuICAgIGhlaWdodDogNTBweDtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICB9XHJcbiAgZGl2LmFuYWx5dGljIHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICB9XHJcblxyXG4gIGRpdi5hbmFseXRpYyBpbnB1dCB7XHJcbiAgICBiYWNrZ3JvdW5kOiB1cmwoJ3NyYy9hcHAvaGVhZGVyL2ltYWdlcy9hbmFseXRpYy5zdmcnKSBuby1yZXBlYXQ7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICB3aWR0aDogNTBweDtcclxuICAgIGhlaWdodDogNTBweDtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICB9XHJcblxyXG4gIGRpdi5ub3RpZmljYXRpb24ge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIH1cclxuXHJcbiAgZGl2Lm5vdGlmaWNhdGlvbiBpbnB1dCB7XHJcbiAgICBiYWNrZ3JvdW5kOiB1cmwoJ3NyYy9hcHAvY2FsZW5kYXIvaW1hZ2VzL291dGxpbmUtdG9kYXktMjRweC5zdmcnKSBuby1yZXBlYXQ7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICB3aWR0aDogNTBweDtcclxuICAgIGhlaWdodDogNTBweDtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICB9XHJcbiAgXHJcbiAgZGl2LnNldHRpbmcge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIH1cclxuXHJcbiAgZGl2LnNldHRpbmcgaW5wdXQge1xyXG4gICAgYmFja2dyb3VuZDogdXJsKCdzcmMvYXBwL2hlYWRlci9pbWFnZXMvc2V0dGluZ3Muc3ZnJykgbm8tcmVwZWF0O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgd2lkdGg6IDUwcHg7XHJcbiAgICBoZWlnaHQ6IDUwcHg7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgfVxyXG4gIHNwYW4uaG9tZSB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDMycHg7XHJcbiAgICBsZWZ0OiA2cHg7XHJcbiAgfVxyXG4gIFxyXG4gIHNwYW4uYW5hbHl0aWMge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAzMnB4O1xyXG4gICAgbGVmdDogMnB4O1xyXG4gIH1cclxuXHJcbiAgc3Bhbi5ub3RpZmljYXRpb24ge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAzMnB4O1xyXG4gICAgbGVmdDogMnB4O1xyXG4gIH1cclxuXHJcbiAgc3Bhbi5zZXR0aW5nIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogMzJweDtcclxuICAgIGxlZnQ6IDEycHg7XHJcbiAgfVxyXG5cclxuXHJcbiAgXHJcbiAgaW1nIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiBhdXRvO1xyXG4gICAgbWF4LWhlaWdodDogNDAwcHg7XHJcbiAgIC8qIGJhY2tncm91bmQ6IHVybChsb2FkaW5nLmdpZikgNTAlIG5vLXJlcGVhdDsqL1xyXG4gICAvKiBib3JkZXI6IDFweCBzb2xpZCBibGFjazsqL1xyXG4gICAvKiBib3JkZXItcmFkaXVzOiA1cHg7Ki9cclxuICB9XHJcblxyXG4gIC5ncmlkLWNvbnRhaW5lciB7XHJcbiAgICBkaXNwbGF5OiBncmlkO1xyXG4gICAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiBhdXRvIGF1dG8gO1xyXG4gICAgZ3JpZC1nYXA6IDEwcHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xyXG4gICAgcGFkZGluZzogMTBweDtcclxuICB9XHJcbiAgXHJcbiAgLmdyaWQtY29udGFpbmVyID4gZGl2IHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC44KTtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIHBhZGRpbmc6IDIwcHggMTtcclxuICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICB9XHJcblxyXG5cclxuICAuZ3JpZC1jb250YWluZXItbGFzdCB7XHJcbiAgICBkaXNwbGF5OiBncmlkO1xyXG4gICAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiBhdXRvIDtcclxuICAgIGdyaWQtZ2FwOiAxMHB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0ZGRkZGRjtcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgfVxyXG4gIFxyXG4gIC5ncmlkLWNvbnRhaW5lci1sYXN0ID4gZGl2IHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC44KTtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIHBhZGRpbmc6IDIwcHggMTtcclxuICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICB9XHJcblxyXG4gICNoZWFkZXJ7XHJcbiAgICAgXHJcbiAgICAgIGNvbG9yOmdyZXk7XHJcblxyXG5cclxuICB9XHJcbiAgXHJcbiAgI3N1YmhlYWRlcntcclxuICAgXHJcbiAgICBjb2xvcjojMDBmZjQwO1xyXG5cclxuXHJcbn1cclxuXHJcblxyXG4vKlxyXG4gICNidWxsZXRze1xyXG4gIFxyXG4gICBcclxuXHJcbn0qL1xyXG5cclxuLyogICN0aGlyZHtcclxuICAgIHRleHQtYWxpZ246IGxlZnRcclxuXHJcbiAgfVxyXG5cclxuICAjZmlmdGh7XHJcbiAgICB0ZXh0LWFsaWduOiBsZWZ0XHJcbiAgfVxyXG4qL1xyXG5cclxuICBkaXYuYnVsbGV0MSBpbnB1dCB7XHJcbiAgICBiYWNrZ3JvdW5kOiB1cmwoJ3NyYy9hcHAvaW5mb3MvaW1hZ2VzL3JvdW5kLWJyaWdodG5lc3NfMS0yNHB4LnN2ZycpIG5vLXJlcGVhdDtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIHdpZHRoOiA1MHB4O1xyXG4gICAgaGVpZ2h0OiA1MHB4O1xyXG4gICAgYm9yZGVyOiBub25lO1xyXG4gIH1cclxuXHJcbiAgc3Bhbi5idWxsZXQxIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogMzJweDtcclxuICAgIGxlZnQ6IDJweDtcclxuICB9XHJcbiAgZGl2LmJ1bGxldDEge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIH1cclxuXHJcbiAgXHJcbi5sb2FkZXIge1xyXG4gIGJvcmRlcjogMTZweCBzb2xpZCAjZjNmM2YzO1xyXG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICBib3JkZXItdG9wOiAxNnB4IHNvbGlkICNmZjgwMDA7XHJcbiAgbWFyZ2luOiBhdXRvO1xyXG4gIHdpZHRoOiAxMjBweDtcclxuICBoZWlnaHQ6IDEyMHB4O1xyXG4gIC13ZWJraXQtYW5pbWF0aW9uOiBzcGluIDJzIGxpbmVhciBpbmZpbml0ZTsgLyogU2FmYXJpICovXHJcbiAgYW5pbWF0aW9uOiBzcGluIDJzIGxpbmVhciBpbmZpbml0ZTtcclxufVxyXG4vKiBTYWZhcmkgKi9cclxuQC13ZWJraXQta2V5ZnJhbWVzIHNwaW4ge1xyXG4gIDAlIHsgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKTsgfVxyXG4gIDEwMCUgeyAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDM2MGRlZyk7IH1cclxufVxyXG5cclxuQGtleWZyYW1lcyBzcGluIHtcclxuICAwJSB7IHRyYW5zZm9ybTogcm90YXRlKDBkZWcpOyB9XHJcbiAgMTAwJSB7IHRyYW5zZm9ybTogcm90YXRlKDM2MGRlZyk7IH1cclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/infos/infos.component.html":
/*!********************************************!*\
  !*** ./src/app/infos/infos.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<link  href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css\"  rel=\"stylesheet\">\n<div id=\"maincontenthome\">\n\n    <img >\n    \n\n  <!--  <div class=\"container\">\n        <div class=\"row\">\n          <div class=\"col-sm\">\n            1/3\n          </div>\n          <div class=\"col-sm\">\n            1/3\n          </div>\n         \n        </div>\n    </div>\n  -->\n  <div class=\"container\" >\n     \n      \n      <!--<div id=\"allbars\" class=\"col-md-12\">\n      <div id=\"baralls\">\n          <div  align=\"center\"   id=\"loading\"> Loading...</div>\n           <div id=\"myProgress\">\n               \n               <div id=\"myBar\"></div>\n            </div>\n            <div id=\"loader\" align=\"center\" class=\"loader\"></div>\n        </div>\n      </div>-->\n      <h3 id=\"headmaster\"></h3>\n          <div class=\"grid-container\">\n\n\n            \n              <div  id=\"header\"  >Calories per meal</div>\n              <div id=\"subheader\"></div>\n              <div id=\"bullets\"> \n\n                 <div class=\"bullet1\">\n                  <span id=\"bullet1\" class=\"bullet1\">  </span>\n                   <input id=\"bullet1\" type=\"button\" width=\"48\" height=\"48\" >\n                 </div>\n\n              </div>  \n              <div id=\"bulletsway\"></div>\n              <div id=\"bullets\">\n                  <div class=\"bullet1\">\n                      <span id=\"bullet1\" class=\"bullet1\">  </span>\n                       <input id=\"bullet1\" type=\"button\" width=\"48\" height=\"48\" >\n                  </div>\n\n\n\n              </div>  \n              <div id=\"bulletsnut\"></div>\n            \n            \n          </div>\n      \n      \n          <div class=\"grid-container-last\">\n              <h3>Description</h3>\n              <div  id=\"bulletsdesc\"></div>\n            \n          </div>\n          \n      \n  </div>\n\n   \n\n</div>\n\n\n<div id=\"all\">\n\n  \n    <div class=\"home\">\n        <span id=\"home\" class=\"home\">Home  </span>\n        <input id=\"button1\" type=\"button\"  (click)=\"submitted=false\" (click)=\"goToPage('/', 'main')\">\n      \n    </div>\n\n     <div class=\"analytic\">\n      <span id=\"analytic\" class=\"analytic\">Analytics  </span>\n      <input id=\"button2\" type=\"button\" alt=\"Submit\" width=\"48\" height=\"48\" (click)=\"submitted=false\" (click)=\"goToPage('/', 'analytic')\">\n  \n    </div>\n\n    <div class=\"notification\">\n        <span id=\"notification\" class=\"notification\">Calendar  </span>\n        <input id=\"button3\" type=\"button\" alt=\"Submit\" width=\"48\" height=\"48\" (click)=\"submitted=false\" (click)=\"goToPage('/', 'calendar')\">\n    \n      </div>\n    \n      <div class=\"setting\">\n          <span id=\"setting\" class=\"setting\">Settings  </span>\n          <input id=\"button4\" type=\"button\" alt=\"Submit\" width=\"48\" height=\"48\" (click)=\"submitted=false\" (click)=\"goToPage('/', 'analytic')\">\n      \n        </div>\n   \n     \n \n</div>"

/***/ }),

/***/ "./src/app/infos/infos.component.ts":
/*!******************************************!*\
  !*** ./src/app/infos/infos.component.ts ***!
  \******************************************/
/*! exports provided: InfosComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InfosComponent", function() { return InfosComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./src/node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_4__);





var InfosComponent = /** @class */ (function () {
    function InfosComponent(http, router) {
        this.http = http;
        this.router = router;
    }
    InfosComponent.prototype.ngOnInit = function () {
        jquery__WEBPACK_IMPORTED_MODULE_4__("#baralls").show();
        // var i=location.host.para
        var c = location.pathname.toString();
        // alert("pathname is"+c);
        var param = c.split('&name%3D')[1];
        //var param2=c.split('&day%3D')[1]
        // alert("xsxs are"+param);
        // alert("Parameter is"+param);
        // downloadingImage.src = "http://an.image/to/aynchrounously/download.jpg";
        function delay(ms) {
            return new Promise(function (resolve) { return setTimeout(resolve, ms); });
        }
        jquery__WEBPACK_IMPORTED_MODULE_4__("#date").hide();
        jquery__WEBPACK_IMPORTED_MODULE_4__("#session").hide();
        jquery__WEBPACK_IMPORTED_MODULE_4__("#back").show();
        jquery__WEBPACK_IMPORTED_MODULE_4__("#maincontenthome").hide();
        jquery__WEBPACK_IMPORTED_MODULE_4__("#baralls").show();
        //alert("cabled");
        this.setInfos(param);
    };
    InfosComponent.prototype.setInfos = function (meal) {
        // alert("Set infos called");
        var name = sessionStorage.getItem("lastname");
        this.http.get("http://139.91.200.103:3000/api/things/getcalendarinfo/" + name + "/" + meal, { responseType: "json" }).subscribe(function (response) {
            var sample = JSON.stringify(response);
            var res = JSON.parse(sample);
            //alert(res[0].name);
            // alert(res[0].wayofcooking);
            document.getElementById("headmaster").textContent = res[0].name;
            document.getElementById("bulletsdesc").textContent = res[0].info;
            document.getElementById("subheader").textContent = res[0].calories;
            document.getElementById("bulletsway").textContent = res[0].wayofcooking;
            document.getElementById("bulletsnut").textContent = res[0].nutrition;
            // headmaster
            var image = document.images[0];
            var downloadingImage = new Image();
            image.src = res[0].imageurl;
            jquery__WEBPACK_IMPORTED_MODULE_4__("#maincontenthome").show();
            //$("#baralls").hide();
        });
    };
    InfosComponent.prototype.goToPage = function (pagename, parameter) {
        this.router.navigate([pagename, parameter]);
    };
    InfosComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-infos',
            template: __webpack_require__(/*! ./infos.component.html */ "./src/app/infos/infos.component.html"),
            styles: [__webpack_require__(/*! ./infos.component.css */ "./src/app/infos/infos.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], InfosComponent);
    return InfosComponent;
}());



/***/ }),

/***/ "./src/app/notification/notification.component.css":
/*!*********************************************************!*\
  !*** ./src/app/notification/notification.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#button1 {\r\n    background-color: #b2b2b2\t; \r\n    border: none;\r\n    color: white;\r\n    padding-bottom: 25px 22px;\r\n    text-align: center;\r\n    text-decoration: none;\r\n    display: inline-block;\r\n    font-size: 36px;\r\n    margin: 4px 18px;\r\n    cursor: pointer;\r\n    \r\n  }\r\n\r\n  \r\n#button2 {\r\n   background-color: #b2b2b2\t; \r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n\r\n  \r\n#button3 {\r\n   background-color: #b2b2b2\t; \r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n\r\n  \r\n#button4 {\r\n   background-color: #b2b2b2\t;\r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n\r\n  \r\n#buttona {\r\n    border-radius: 25px;\r\n    background-color: #b2b2b2\t; \r\n    border: none;\r\n    color: white;\r\n    padding-bottom: 25px 22px;\r\n    text-align: center;\r\n    text-decoration: none;\r\n    display: inline-block;\r\n    font-size: 36px;\r\n    margin: 4px 18px;\r\n    cursor: pointer;\r\n    \r\n  }\r\n\r\n  \r\n#buttonb {\r\n    border-radius: 25px;\r\n   background-color: #b2b2b2\t; \r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n\r\n  \r\n#buttonc {\r\n    border-radius: 25px;\r\n   background-color: #b2b2b2\t; \r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n\r\n  \r\n#buttond {\r\n    border-radius: 25px;\r\n   background-color: #b2b2b2\t;\r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n\r\n  \r\n#buttone {\r\n    border-radius: 25px;\r\n    background-color: #b2b2b2\t;\r\n    border: none;\r\n    color: white;\r\n    padding-bottom: 25px 22px;\r\n    text-align: center;\r\n    text-decoration: none;\r\n    display: inline-block;\r\n    font-size: 36px;\r\n    margin: 4px 18px;\r\n    cursor: pointer;\r\n  }\r\n\r\n  \r\n#all{\r\n     text-align: center;\r\n     position: fixed;\r\n     left: 0;\r\n     bottom: 0;\r\n     width: 100%;\r\n     background-color: \t\t#b2b2b2;\r\n     color: white;\r\n     text-align: center;\r\n  }\r\n\r\n  \r\ndiv.home {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\n\r\n  \r\ndiv.home input {\r\n    background: url('home.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\n\r\n  \r\ndiv.analytic {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\n\r\n  \r\ndiv.analytic input {\r\n    background: url('analytic.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\n\r\n  \r\ndiv.notification {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\n\r\n  \r\ndiv.notification input {\r\n    background: url('outline-today-24px.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\n\r\n  \r\ndiv.setting {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\n\r\n  \r\ndiv.setting input {\r\n    background: url('settings.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\n\r\n  \r\nspan.home {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 6px;\r\n  }\r\n\r\n  \r\nspan.analytic {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 2px;\r\n  }\r\n\r\n  \r\nspan.notification {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 2px;\r\n  }\r\n\r\n  \r\nspan.setting {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 12px;\r\n  }\r\n\r\n  \r\n#buttonround1 {\r\n    border-radius: 25px;\r\n    background: #cccccc;\r\n    padding: 5px; \r\n    width: 100px;\r\n    height: 50px;  \r\n  }\r\n\r\n  \r\n#rcorners1 {\r\n    border-radius: 25px;\r\n    background: #e7e0e0;\r\n    padding: 20px; \r\n    width: 100%;\r\n    height: auto;  \r\n  }\r\n\r\n  \r\n#tableid {\r\n    border-radius: 25px;\r\n    background: #FFFFFF;\r\n    padding: 20px; \r\n   \r\n    width: 100%;\r\n    height: auto;  \r\n  }\r\n\r\n  \r\n#myProgress {\r\n    width: 100%;\r\n    background-color: #ddd;\r\n  }\r\n\r\n  \r\n#myBar {\r\n    width: 1%;\r\n    height: 30px;\r\n    background-color: #ff8000;\r\n  }\r\n\r\n  \r\n#barall {\r\n  padding-top:130px;\r\n \r\n}\r\n\r\n  \r\n#menu {\r\n  padding-bottom:0px;\r\n \r\n}\r\n\r\n  \r\n.loader {\r\n  border: 16px solid #f3f3f3;\r\n  border-radius: 50%;\r\n  border-top: 16px solid #ff8000;\r\n  margin: auto;\r\n  width: 120px;\r\n  height: 120px;\r\n  -webkit-animation: spin 2s linear infinite; /* Safari */\r\n  animation: spin 2s linear infinite;\r\n}\r\n\r\n  \r\n/* Safari */\r\n\r\n  \r\n@-webkit-keyframes spin {\r\n  0% { -webkit-transform: rotate(0deg); }\r\n  100% { -webkit-transform: rotate(360deg); }\r\n}\r\n\r\n  \r\n@keyframes spin {\r\n  0% { transform: rotate(0deg); }\r\n  100% { transform: rotate(360deg); }\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSwwQkFBMEI7SUFDMUIsWUFBWTtJQUNaLFlBQVk7SUFDWix5QkFBeUI7SUFDekIsa0JBQWtCO0lBQ2xCLHFCQUFxQjtJQUNyQixxQkFBcUI7SUFDckIsZUFBZTtJQUNmLGdCQUFnQjtJQUNoQixlQUFlOztFQUVqQjs7O0FBR0Y7R0FDRywwQkFBMEI7R0FDMUIsWUFBWTtHQUNaLFlBQVk7R0FDWix5QkFBeUI7R0FDekIsa0JBQWtCO0dBQ2xCLHFCQUFxQjtHQUNyQixxQkFBcUI7R0FDckIsZUFBZTtHQUNmLGdCQUFnQjtHQUNoQixlQUFlO0NBQ2pCOzs7QUFDQTtHQUNFLDBCQUEwQjtHQUMxQixZQUFZO0dBQ1osWUFBWTtHQUNaLHlCQUF5QjtHQUN6QixrQkFBa0I7R0FDbEIscUJBQXFCO0dBQ3JCLHFCQUFxQjtHQUNyQixlQUFlO0dBQ2YsZ0JBQWdCO0dBQ2hCLGVBQWU7Q0FDakI7OztBQUVBO0dBQ0UsMEJBQTBCO0dBQzFCLFlBQVk7R0FDWixZQUFZO0dBQ1oseUJBQXlCO0dBQ3pCLGtCQUFrQjtHQUNsQixxQkFBcUI7R0FDckIscUJBQXFCO0dBQ3JCLGVBQWU7R0FDZixnQkFBZ0I7R0FDaEIsZUFBZTtDQUNqQjs7O0FBS0E7SUFDRyxtQkFBbUI7SUFDbkIsMEJBQTBCO0lBQzFCLFlBQVk7SUFDWixZQUFZO0lBQ1oseUJBQXlCO0lBQ3pCLGtCQUFrQjtJQUNsQixxQkFBcUI7SUFDckIscUJBQXFCO0lBQ3JCLGVBQWU7SUFDZixnQkFBZ0I7SUFDaEIsZUFBZTs7RUFFakI7OztBQUdGO0lBQ0ksbUJBQW1CO0dBQ3BCLDBCQUEwQjtHQUMxQixZQUFZO0dBQ1osWUFBWTtHQUNaLHlCQUF5QjtHQUN6QixrQkFBa0I7R0FDbEIscUJBQXFCO0dBQ3JCLHFCQUFxQjtHQUNyQixlQUFlO0dBQ2YsZ0JBQWdCO0dBQ2hCLGVBQWU7Q0FDakI7OztBQUNBO0lBQ0csbUJBQW1CO0dBQ3BCLDBCQUEwQjtHQUMxQixZQUFZO0dBQ1osWUFBWTtHQUNaLHlCQUF5QjtHQUN6QixrQkFBa0I7R0FDbEIscUJBQXFCO0dBQ3JCLHFCQUFxQjtHQUNyQixlQUFlO0dBQ2YsZ0JBQWdCO0dBQ2hCLGVBQWU7Q0FDakI7OztBQUVBO0lBQ0csbUJBQW1CO0dBQ3BCLDBCQUEwQjtHQUMxQixZQUFZO0dBQ1osWUFBWTtHQUNaLHlCQUF5QjtHQUN6QixrQkFBa0I7R0FDbEIscUJBQXFCO0dBQ3JCLHFCQUFxQjtHQUNyQixlQUFlO0dBQ2YsZ0JBQWdCO0dBQ2hCLGVBQWU7Q0FDakI7OztBQUNBO0lBQ0csbUJBQW1CO0lBQ25CLDBCQUEwQjtJQUMxQixZQUFZO0lBQ1osWUFBWTtJQUNaLHlCQUF5QjtJQUN6QixrQkFBa0I7SUFDbEIscUJBQXFCO0lBQ3JCLHFCQUFxQjtJQUNyQixlQUFlO0lBQ2YsZ0JBQWdCO0lBQ2hCLGVBQWU7RUFDakI7OztBQUdBO0tBQ0csa0JBQWtCO0tBQ2xCLGVBQWU7S0FDZixPQUFPO0tBQ1AsU0FBUztLQUNULFdBQVc7S0FDWCwyQkFBMkI7S0FDM0IsWUFBWTtLQUNaLGtCQUFrQjtFQUNyQjs7O0FBRUE7SUFDRSxrQkFBa0I7SUFDbEIscUJBQXFCO0VBQ3ZCOzs7QUFFQTtJQUNFLHFDQUEyRDtJQUMzRCxlQUFlO0lBQ2YsV0FBVztJQUNYLFlBQVk7SUFDWixZQUFZO0VBQ2Q7OztBQUNBO0lBQ0Usa0JBQWtCO0lBQ2xCLHFCQUFxQjtFQUN2Qjs7O0FBRUE7SUFDRSx5Q0FBK0Q7SUFDL0QsZUFBZTtJQUNmLFdBQVc7SUFDWCxZQUFZO0lBQ1osWUFBWTtFQUNkOzs7QUFFQTtJQUNFLGtCQUFrQjtJQUNsQixxQkFBcUI7RUFDdkI7OztBQUVBO0lBQ0UsbURBQTJFO0lBQzNFLGVBQWU7SUFDZixXQUFXO0lBQ1gsWUFBWTtJQUNaLFlBQVk7RUFDZDs7O0FBRUE7SUFDRSxrQkFBa0I7SUFDbEIscUJBQXFCO0VBQ3ZCOzs7QUFFQTtJQUNFLHlDQUErRDtJQUMvRCxlQUFlO0lBQ2YsV0FBVztJQUNYLFlBQVk7SUFDWixZQUFZO0VBQ2Q7OztBQUNBO0lBQ0Usa0JBQWtCO0lBQ2xCLFNBQVM7SUFDVCxTQUFTO0VBQ1g7OztBQUVBO0lBQ0Usa0JBQWtCO0lBQ2xCLFNBQVM7SUFDVCxTQUFTO0VBQ1g7OztBQUVBO0lBQ0Usa0JBQWtCO0lBQ2xCLFNBQVM7SUFDVCxTQUFTO0VBQ1g7OztBQUVBO0lBQ0Usa0JBQWtCO0lBQ2xCLFNBQVM7SUFDVCxVQUFVO0VBQ1o7OztBQUVBO0lBQ0UsbUJBQW1CO0lBQ25CLG1CQUFtQjtJQUNuQixZQUFZO0lBQ1osWUFBWTtJQUNaLFlBQVk7RUFDZDs7O0FBRUE7SUFDRSxtQkFBbUI7SUFDbkIsbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixXQUFXO0lBQ1gsWUFBWTtFQUNkOzs7QUFFQTtJQUNFLG1CQUFtQjtJQUNuQixtQkFBbUI7SUFDbkIsYUFBYTs7SUFFYixXQUFXO0lBQ1gsWUFBWTtFQUNkOzs7QUFJRjtJQUNJLFdBQVc7SUFDWCxzQkFBc0I7RUFDeEI7OztBQUVGO0lBQ0ksU0FBUztJQUNULFlBQVk7SUFDWix5QkFBeUI7RUFDM0I7OztBQUdGO0VBQ0UsaUJBQWlCOztBQUVuQjs7O0FBRUE7RUFDRSxrQkFBa0I7O0FBRXBCOzs7QUFDQTtFQUNFLDBCQUEwQjtFQUMxQixrQkFBa0I7RUFDbEIsOEJBQThCO0VBQzlCLFlBQVk7RUFDWixZQUFZO0VBQ1osYUFBYTtFQUNiLDBDQUEwQyxFQUFFLFdBQVc7RUFDdkQsa0NBQWtDO0FBQ3BDOzs7QUFFQSxXQUFXOzs7QUFDWDtFQUNFLEtBQUssK0JBQStCLEVBQUU7RUFDdEMsT0FBTyxpQ0FBaUMsRUFBRTtBQUM1Qzs7O0FBRUE7RUFDRSxLQUFLLHVCQUF1QixFQUFFO0VBQzlCLE9BQU8seUJBQXlCLEVBQUU7QUFDcEMiLCJmaWxlIjoiYXBwL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIiNidXR0b24xIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNiMmIyYjJcdDsgXHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMjVweCAyMnB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgZm9udC1zaXplOiAzNnB4O1xyXG4gICAgbWFyZ2luOiA0cHggMThweDtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIFxyXG4gIH1cclxuXHJcbiAgXHJcbiNidXR0b24yIHtcclxuICAgYmFja2dyb3VuZC1jb2xvcjogI2IyYjJiMlx0OyBcclxuICAgYm9yZGVyOiBub25lO1xyXG4gICBjb2xvcjogd2hpdGU7XHJcbiAgIHBhZGRpbmctYm90dG9tOiAyNXB4IDIycHg7XHJcbiAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgIGZvbnQtc2l6ZTogMzZweDtcclxuICAgbWFyZ2luOiA0cHggMThweDtcclxuICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gfVxyXG4gI2J1dHRvbjMge1xyXG4gICBiYWNrZ3JvdW5kLWNvbG9yOiAjYjJiMmIyXHQ7IFxyXG4gICBib3JkZXI6IG5vbmU7XHJcbiAgIGNvbG9yOiB3aGl0ZTtcclxuICAgcGFkZGluZy1ib3R0b206IDI1cHggMjJweDtcclxuICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgZm9udC1zaXplOiAzNnB4O1xyXG4gICBtYXJnaW46IDRweCAxOHB4O1xyXG4gICBjdXJzb3I6IHBvaW50ZXI7XHJcbiB9XHJcblxyXG4gI2J1dHRvbjQge1xyXG4gICBiYWNrZ3JvdW5kLWNvbG9yOiAjYjJiMmIyXHQ7XHJcbiAgIGJvcmRlcjogbm9uZTtcclxuICAgY29sb3I6IHdoaXRlO1xyXG4gICBwYWRkaW5nLWJvdHRvbTogMjVweCAyMnB4O1xyXG4gICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICBmb250LXNpemU6IDM2cHg7XHJcbiAgIG1hcmdpbjogNHB4IDE4cHg7XHJcbiAgIGN1cnNvcjogcG9pbnRlcjtcclxuIH1cclxuXHJcblxyXG5cclxuXHJcbiAjYnV0dG9uYSB7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2IyYjJiMlx0OyBcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIHBhZGRpbmctYm90dG9tOiAyNXB4IDIycHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBmb250LXNpemU6IDM2cHg7XHJcbiAgICBtYXJnaW46IDRweCAxOHB4O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgXHJcbiAgfVxyXG5cclxuICBcclxuI2J1dHRvbmIge1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcclxuICAgYmFja2dyb3VuZC1jb2xvcjogI2IyYjJiMlx0OyBcclxuICAgYm9yZGVyOiBub25lO1xyXG4gICBjb2xvcjogd2hpdGU7XHJcbiAgIHBhZGRpbmctYm90dG9tOiAyNXB4IDIycHg7XHJcbiAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgIGZvbnQtc2l6ZTogMzZweDtcclxuICAgbWFyZ2luOiA0cHggMThweDtcclxuICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gfVxyXG4gI2J1dHRvbmMge1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcclxuICAgYmFja2dyb3VuZC1jb2xvcjogI2IyYjJiMlx0OyBcclxuICAgYm9yZGVyOiBub25lO1xyXG4gICBjb2xvcjogd2hpdGU7XHJcbiAgIHBhZGRpbmctYm90dG9tOiAyNXB4IDIycHg7XHJcbiAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgIGZvbnQtc2l6ZTogMzZweDtcclxuICAgbWFyZ2luOiA0cHggMThweDtcclxuICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gfVxyXG5cclxuICNidXR0b25kIHtcclxuICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbiAgIGJhY2tncm91bmQtY29sb3I6ICNiMmIyYjJcdDtcclxuICAgYm9yZGVyOiBub25lO1xyXG4gICBjb2xvcjogd2hpdGU7XHJcbiAgIHBhZGRpbmctYm90dG9tOiAyNXB4IDIycHg7XHJcbiAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgIGZvbnQtc2l6ZTogMzZweDtcclxuICAgbWFyZ2luOiA0cHggMThweDtcclxuICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gfVxyXG4gI2J1dHRvbmUge1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNiMmIyYjJcdDtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIHBhZGRpbmctYm90dG9tOiAyNXB4IDIycHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBmb250LXNpemU6IDM2cHg7XHJcbiAgICBtYXJnaW46IDRweCAxOHB4O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gIH1cclxuICBcclxuIFxyXG4gICNhbGx7XHJcbiAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgIHBvc2l0aW9uOiBmaXhlZDtcclxuICAgICBsZWZ0OiAwO1xyXG4gICAgIGJvdHRvbTogMDtcclxuICAgICB3aWR0aDogMTAwJTtcclxuICAgICBiYWNrZ3JvdW5kLWNvbG9yOiBcdFx0I2IyYjJiMjtcclxuICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIH1cclxuXHJcbiAgZGl2LmhvbWUge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIH1cclxuXHJcbiAgZGl2LmhvbWUgaW5wdXQge1xyXG4gICAgYmFja2dyb3VuZDogdXJsKCdzcmMvYXBwL2hlYWRlci9pbWFnZXMvaG9tZS5zdmcnKSBuby1yZXBlYXQ7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICB3aWR0aDogNTBweDtcclxuICAgIGhlaWdodDogNTBweDtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICB9XHJcbiAgZGl2LmFuYWx5dGljIHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICB9XHJcblxyXG4gIGRpdi5hbmFseXRpYyBpbnB1dCB7XHJcbiAgICBiYWNrZ3JvdW5kOiB1cmwoJ3NyYy9hcHAvaGVhZGVyL2ltYWdlcy9hbmFseXRpYy5zdmcnKSBuby1yZXBlYXQ7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICB3aWR0aDogNTBweDtcclxuICAgIGhlaWdodDogNTBweDtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICB9XHJcblxyXG4gIGRpdi5ub3RpZmljYXRpb24ge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIH1cclxuXHJcbiAgZGl2Lm5vdGlmaWNhdGlvbiBpbnB1dCB7XHJcbiAgICBiYWNrZ3JvdW5kOiB1cmwoJ3NyYy9hcHAvY2FsZW5kYXIvaW1hZ2VzL291dGxpbmUtdG9kYXktMjRweC5zdmcnKSBuby1yZXBlYXQ7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICB3aWR0aDogNTBweDtcclxuICAgIGhlaWdodDogNTBweDtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICB9XHJcbiAgXHJcbiAgZGl2LnNldHRpbmcge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIH1cclxuXHJcbiAgZGl2LnNldHRpbmcgaW5wdXQge1xyXG4gICAgYmFja2dyb3VuZDogdXJsKCdzcmMvYXBwL2hlYWRlci9pbWFnZXMvc2V0dGluZ3Muc3ZnJykgbm8tcmVwZWF0O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgd2lkdGg6IDUwcHg7XHJcbiAgICBoZWlnaHQ6IDUwcHg7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgfVxyXG4gIHNwYW4uaG9tZSB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDMycHg7XHJcbiAgICBsZWZ0OiA2cHg7XHJcbiAgfVxyXG4gIFxyXG4gIHNwYW4uYW5hbHl0aWMge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAzMnB4O1xyXG4gICAgbGVmdDogMnB4O1xyXG4gIH1cclxuXHJcbiAgc3Bhbi5ub3RpZmljYXRpb24ge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAzMnB4O1xyXG4gICAgbGVmdDogMnB4O1xyXG4gIH1cclxuXHJcbiAgc3Bhbi5zZXR0aW5nIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogMzJweDtcclxuICAgIGxlZnQ6IDEycHg7XHJcbiAgfVxyXG5cclxuICAjYnV0dG9ucm91bmQxIHtcclxuICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbiAgICBiYWNrZ3JvdW5kOiAjY2NjY2NjO1xyXG4gICAgcGFkZGluZzogNXB4OyBcclxuICAgIHdpZHRoOiAxMDBweDtcclxuICAgIGhlaWdodDogNTBweDsgIFxyXG4gIH1cclxuXHJcbiAgI3Jjb3JuZXJzMSB7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xyXG4gICAgYmFja2dyb3VuZDogI2U3ZTBlMDtcclxuICAgIHBhZGRpbmc6IDIwcHg7IFxyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IGF1dG87ICBcclxuICB9XHJcbiAgXHJcbiAgI3RhYmxlaWQge1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcclxuICAgIGJhY2tncm91bmQ6ICNGRkZGRkY7XHJcbiAgICBwYWRkaW5nOiAyMHB4OyBcclxuICAgXHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogYXV0bzsgIFxyXG4gIH1cclxuXHJcblxyXG5cclxuI215UHJvZ3Jlc3Mge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZGRkO1xyXG4gIH1cclxuICBcclxuI215QmFyIHtcclxuICAgIHdpZHRoOiAxJTtcclxuICAgIGhlaWdodDogMzBweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZjgwMDA7XHJcbiAgfVxyXG4gIFxyXG4gICAgXHJcbiNiYXJhbGwge1xyXG4gIHBhZGRpbmctdG9wOjEzMHB4O1xyXG4gXHJcbn1cclxuICAgXHJcbiNtZW51IHtcclxuICBwYWRkaW5nLWJvdHRvbTowcHg7XHJcbiBcclxufVxyXG4ubG9hZGVyIHtcclxuICBib3JkZXI6IDE2cHggc29saWQgI2YzZjNmMztcclxuICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgYm9yZGVyLXRvcDogMTZweCBzb2xpZCAjZmY4MDAwO1xyXG4gIG1hcmdpbjogYXV0bztcclxuICB3aWR0aDogMTIwcHg7XHJcbiAgaGVpZ2h0OiAxMjBweDtcclxuICAtd2Via2l0LWFuaW1hdGlvbjogc3BpbiAycyBsaW5lYXIgaW5maW5pdGU7IC8qIFNhZmFyaSAqL1xyXG4gIGFuaW1hdGlvbjogc3BpbiAycyBsaW5lYXIgaW5maW5pdGU7XHJcbn1cclxuXHJcbi8qIFNhZmFyaSAqL1xyXG5ALXdlYmtpdC1rZXlmcmFtZXMgc3BpbiB7XHJcbiAgMCUgeyAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDBkZWcpOyB9XHJcbiAgMTAwJSB7IC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMzYwZGVnKTsgfVxyXG59XHJcblxyXG5Aa2V5ZnJhbWVzIHNwaW4ge1xyXG4gIDAlIHsgdHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7IH1cclxuICAxMDAlIHsgdHJhbnNmb3JtOiByb3RhdGUoMzYwZGVnKTsgfVxyXG59Il19 */"

/***/ }),

/***/ "./src/app/notification/notification.component.html":
/*!**********************************************************!*\
  !*** ./src/app/notification/notification.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<link  href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css\"  rel=\"stylesheet\">\n<div id=\"all\">\n \n  \n  <div class=\"home\">\n      <span id=\"home\" class=\"home\">Home  </span>\n      <input id=\"button1\" type=\"button\"  (click)=\"submitted=false\" (click)=\"goToPageMenu('/', 'main')\">\n    \n  </div>\n\n   <div class=\"analytic\">\n    <span id=\"analytic\" class=\"analytic\">Analytics  </span>\n    <input id=\"button2\" type=\"button\" alt=\"Submit\" width=\"48\" height=\"48\" (click)=\"submitted=false\" (click)=\"goToPageMenu('/', 'analytic')\">\n\n  </div>\n\n  <div class=\"notification\">\n      <span id=\"notification\" class=\"notification\">Calendar  </span>\n      <input id=\"button3\" type=\"button\" alt=\"Submit\" width=\"48\" height=\"48\" (click)=\"submitted=false\" (click)=\"goToPageMenu('/', 'calendar')\">\n  \n    </div>\n  \n    <div class=\"setting\">\n        <span id=\"setting\" class=\"setting\">Settings  </span>\n        <input id=\"button4\" type=\"button\" alt=\"Submit\" width=\"48\" height=\"48\" (click)=\"submitted=false\" (click)=\"goToPageMenu('/', 'settings')\">\n    \n      </div>\n \n   \n\n</div>\n\n<div id=\"maincontenthome\">\n  \n\n  \n\n  <div class=\"container\">\n    <!--  <ul class=\"list\">\n          <cdk-virtual-scroll-viewport  style=\"height: 500px\" itemSize=\"90\" >\n            <ng-container   *cdkVirtualFor=\"let n of data\" >\n                <h3 style=\"color:blue;\">{{n._id}}</h3>\n                <li> {{\n               \n                \n                  n.name\n                \n                }} </li>\n  \n            \n            </ng-container>\n          \n            \n          </cdk-virtual-scroll-viewport>\n        </ul>-->\n        <div id=\"menu\"> <th>Menu</th></div>\n\n        <div  id=\"barall\" class=\"row\">\n            <div class=\"col-md-12\">\n              <div  align=\"center\"   id=\"loading\"> Loading...</div>\n              <!--  <div id=\"myProgress\">\n                   \n                   <div id=\"myBar\"></div>\n               </div>-->\n\n               <div id=\"loader\" align=\"center\" class=\"loader\"></div>\n            \n            </div>\n          </div>\n      <table class=\"table table-striped\">\n        <thead>\n            <tr>\n            \n             \n       \n\n            </tr>\n        </thead>\n        <tbody id=\"tableid\">\n          <tr *ngFor =\"let d of data;  \" >\n              <!--<h6 style=\"color:blue;\">{{d.type}}</h6>-->\n          <!--  <td>{{d.name}} </td>\n            <td>{{d.type}}</td>\n          -->\n\n\n          \n            <div id=\"rcorners1\" class=\"container\">\n                <div class=\"row\">\n                    <div class=\"col-xs-12\">\n                        <td>Meal Recomendation</td>\n                    </div>\n                </div>\n              <div class=\"row\">\n                  <div class=\"col-xs-6\">\n                      <img src=\"https://getdigitalradio.com/wp-content/uploads/2017/09/tick-1.png\" alt=\"recommend\" width=\"40\" height=\"30\">\n                  </div>\n                  <div class=\"col-xs-6\">\n                      <h5 style=\"color:blue;\">{{d.type}}</h5>\n                    </div>\n                </div>\n                <div class=\"row\">\n                   \n                    <div class=\"col-xs-12\">\n                        <h6  style=\"color:black;\">{{d.date}}</h6>\n                    </div>\n  \n                </div>\n              <div class=\"row\">\n                <div class=\"col-xs-3\">\n                \n                  <button id=\"buttonround1\" class=\"tab-button\" (click)=\"onSelect(d.type)\">{{d.name}}</button>\n                </div>\n                <div class=\"col-xs-3\">\n                 \n                  <button id=\"buttonround1\" class=\"tab-button\" (click)=\"onSelect(d.type)\">{{d.name2}}</button>\n                </div>\n                <div class=\"col-xs-3\">\n                 \n                  <button id=\"buttonround1\" class=\"tab-button\" (click)=\"onSelect(d.type)\">{{d.name3}}</button>\n                </div>\n\n                <div class=\"col-xs-3\">\n                 \n                  <button id=\"buttonround1\" class=\"tab-button\" (click)=\"onSelect(d.type)\">{{d.name4}}</button>\n                </div>\n              </div>\n\n            </div>\n          </tr>\n        </tbody>\n      </table>\n    </div>\n    \n  </div>\n  "

/***/ }),

/***/ "./src/app/notification/notification.component.ts":
/*!********************************************************!*\
  !*** ./src/app/notification/notification.component.ts ***!
  \********************************************************/
/*! exports provided: NotificationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationComponent", function() { return NotificationComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./src/node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_4__);





var NotificationComponent = /** @class */ (function () {
    function NotificationComponent(http, router) {
        this.http = http;
        this.router = router;
    }
    NotificationComponent.prototype.goToPageMenu = function (pagename, parameter) {
        this.router.navigate([pagename, parameter]);
    };
    NotificationComponent.prototype.onSelect = function (hero) {
        alert("called" + hero);
    };
    NotificationComponent.prototype.onSelect2 = function (hero) {
        alert("called2" + hero);
    };
    NotificationComponent.prototype.ngOnInit = function () {
        var _this = this;
        jquery__WEBPACK_IMPORTED_MODULE_4__("#session").hide();
        jquery__WEBPACK_IMPORTED_MODULE_4__("#date").hide();
        jquery__WEBPACK_IMPORTED_MODULE_4__("#back").show();
        function delay(ms) {
            return new Promise(function (resolve) { return setTimeout(resolve, ms); });
        }
        // alert("Cabled");
        //var width = 1;
        // alert("datieee are hier!");
        // alert(this.username);
        // alert(this.email);
        // alert(this.password);
        // alert(this.height);
        // alert(this.weight);
        /* var elem = document.getElementById("myBar");
          
         var id = setInterval(frame, 10);
         function frame() {
           //alert("frame");
           if (width >= 100) {
           //  alert("called");
             clearInterval(id);
            
            // alert("alles");
            
             
           } else {
             //alert("Kabled width functionarie");
             width++;
             elem.style.width = width + '%';
           }
         }*/
        //$("#menu").hide();
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var name;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(1000)];
                    case 1:
                        _a.sent();
                        jquery__WEBPACK_IMPORTED_MODULE_4__("#barall").hide();
                        name = sessionStorage.getItem("lastname").toLowerCase();
                        this.http.get("http://139.91.200.103:3000/api/things/getcalendar2/" + name + "/monday", { responseType: "json" }).subscribe(
                        // this.http.get(`http://localhost:3000/api/things/getcalendar/Emily/${'tuesday'}`,{responseType:"json"}).subscribe(
                        function (response) {
                            _this.data = response;
                            // console.log("data :"+response);
                            // var sample=JSON.stringify(response);
                        });
                        return [2 /*return*/];
                }
            });
        }); })();
    };
    NotificationComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-notification',
            template: __webpack_require__(/*! ./notification.component.html */ "./src/app/notification/notification.component.html"),
            styles: [__webpack_require__(/*! ./notification.component.css */ "./src/app/notification/notification.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], NotificationComponent);
    return NotificationComponent;
}());



/***/ }),

/***/ "./src/app/qrcode/qrcode.component.css":
/*!*********************************************!*\
  !*** ./src/app/qrcode/qrcode.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#button1 {\r\n    background-color: #b2b2b2\t; \r\n    border: none;\r\n    color: white;\r\n    padding-bottom: 25px 22px;\r\n    text-align: center;\r\n    text-decoration: none;\r\n    display: inline-block;\r\n    font-size: 36px;\r\n    margin: 4px 18px;\r\n    cursor: pointer;\r\n    \r\n  }\r\n\r\n  \r\n#button2 {\r\n   background-color: #b2b2b2\t; \r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n\r\n  \r\n#button3 {\r\n   background-color: #b2b2b2\t; \r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n\r\n  \r\n#button4 {\r\n   background-color: #b2b2b2\t;\r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n\r\n  \r\n#buttona {\r\n    border-radius: 25px;\r\n    background-color: #b2b2b2\t; \r\n    border: none;\r\n    color: white;\r\n    padding-bottom: 25px 22px;\r\n    text-align: center;\r\n    text-decoration: none;\r\n    display: inline-block;\r\n    font-size: 36px;\r\n    margin: 4px 18px;\r\n    cursor: pointer;\r\n    \r\n  }\r\n\r\n  \r\n#buttonb {\r\n    border-radius: 25px;\r\n   background-color: #b2b2b2\t; \r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n\r\n  \r\n#buttonc {\r\n    border-radius: 25px;\r\n   background-color: #b2b2b2\t; \r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n\r\n  \r\n#buttond {\r\n    border-radius: 25px;\r\n   background-color: #b2b2b2\t;\r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n\r\n  \r\n#buttone {\r\n    border-radius: 25px;\r\n    background-color: #b2b2b2\t;\r\n    border: none;\r\n    color: white;\r\n    padding-bottom: 25px 22px;\r\n    text-align: center;\r\n    text-decoration: none;\r\n    display: inline-block;\r\n    font-size: 36px;\r\n    margin: 4px 18px;\r\n    cursor: pointer;\r\n  }\r\n\r\n  \r\n#all{\r\n     text-align: center;\r\n     position: fixed;\r\n     left: 0;\r\n     bottom: 0;\r\n     width: 100%;\r\n     background-color: \t\t#b2b2b2;\r\n     color: white;\r\n     text-align: center;\r\n  }\r\n\r\n  \r\ndiv.home {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\n\r\n  \r\ndiv.home input {\r\n    background: url('home.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\n\r\n  \r\ndiv.analytic {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\n\r\n  \r\ndiv.analytic input {\r\n    background: url('analytic.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\n\r\n  \r\ndiv.notification {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\n\r\n  \r\ndiv.notification input {\r\n    background: url('outline-today-24px.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\n\r\n  \r\ndiv.setting {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\n\r\n  \r\ndiv.setting input {\r\n    background: url('settings.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\n\r\n  \r\nspan.home {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 6px;\r\n  }\r\n\r\n  \r\nspan.analytic {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 2px;\r\n  }\r\n\r\n  \r\nspan.notification {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 2px;\r\n  }\r\n\r\n  \r\nspan.setting {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 12px;\r\n  }\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9xcmNvZGUvcXJjb2RlLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSwwQkFBMEI7SUFDMUIsWUFBWTtJQUNaLFlBQVk7SUFDWix5QkFBeUI7SUFDekIsa0JBQWtCO0lBQ2xCLHFCQUFxQjtJQUNyQixxQkFBcUI7SUFDckIsZUFBZTtJQUNmLGdCQUFnQjtJQUNoQixlQUFlOztFQUVqQjs7O0FBR0Y7R0FDRywwQkFBMEI7R0FDMUIsWUFBWTtHQUNaLFlBQVk7R0FDWix5QkFBeUI7R0FDekIsa0JBQWtCO0dBQ2xCLHFCQUFxQjtHQUNyQixxQkFBcUI7R0FDckIsZUFBZTtHQUNmLGdCQUFnQjtHQUNoQixlQUFlO0NBQ2pCOzs7QUFDQTtHQUNFLDBCQUEwQjtHQUMxQixZQUFZO0dBQ1osWUFBWTtHQUNaLHlCQUF5QjtHQUN6QixrQkFBa0I7R0FDbEIscUJBQXFCO0dBQ3JCLHFCQUFxQjtHQUNyQixlQUFlO0dBQ2YsZ0JBQWdCO0dBQ2hCLGVBQWU7Q0FDakI7OztBQUVBO0dBQ0UsMEJBQTBCO0dBQzFCLFlBQVk7R0FDWixZQUFZO0dBQ1oseUJBQXlCO0dBQ3pCLGtCQUFrQjtHQUNsQixxQkFBcUI7R0FDckIscUJBQXFCO0dBQ3JCLGVBQWU7R0FDZixnQkFBZ0I7R0FDaEIsZUFBZTtDQUNqQjs7O0FBS0E7SUFDRyxtQkFBbUI7SUFDbkIsMEJBQTBCO0lBQzFCLFlBQVk7SUFDWixZQUFZO0lBQ1oseUJBQXlCO0lBQ3pCLGtCQUFrQjtJQUNsQixxQkFBcUI7SUFDckIscUJBQXFCO0lBQ3JCLGVBQWU7SUFDZixnQkFBZ0I7SUFDaEIsZUFBZTs7RUFFakI7OztBQUdGO0lBQ0ksbUJBQW1CO0dBQ3BCLDBCQUEwQjtHQUMxQixZQUFZO0dBQ1osWUFBWTtHQUNaLHlCQUF5QjtHQUN6QixrQkFBa0I7R0FDbEIscUJBQXFCO0dBQ3JCLHFCQUFxQjtHQUNyQixlQUFlO0dBQ2YsZ0JBQWdCO0dBQ2hCLGVBQWU7Q0FDakI7OztBQUNBO0lBQ0csbUJBQW1CO0dBQ3BCLDBCQUEwQjtHQUMxQixZQUFZO0dBQ1osWUFBWTtHQUNaLHlCQUF5QjtHQUN6QixrQkFBa0I7R0FDbEIscUJBQXFCO0dBQ3JCLHFCQUFxQjtHQUNyQixlQUFlO0dBQ2YsZ0JBQWdCO0dBQ2hCLGVBQWU7Q0FDakI7OztBQUVBO0lBQ0csbUJBQW1CO0dBQ3BCLDBCQUEwQjtHQUMxQixZQUFZO0dBQ1osWUFBWTtHQUNaLHlCQUF5QjtHQUN6QixrQkFBa0I7R0FDbEIscUJBQXFCO0dBQ3JCLHFCQUFxQjtHQUNyQixlQUFlO0dBQ2YsZ0JBQWdCO0dBQ2hCLGVBQWU7Q0FDakI7OztBQUNBO0lBQ0csbUJBQW1CO0lBQ25CLDBCQUEwQjtJQUMxQixZQUFZO0lBQ1osWUFBWTtJQUNaLHlCQUF5QjtJQUN6QixrQkFBa0I7SUFDbEIscUJBQXFCO0lBQ3JCLHFCQUFxQjtJQUNyQixlQUFlO0lBQ2YsZ0JBQWdCO0lBQ2hCLGVBQWU7RUFDakI7OztBQUdBO0tBQ0csa0JBQWtCO0tBQ2xCLGVBQWU7S0FDZixPQUFPO0tBQ1AsU0FBUztLQUNULFdBQVc7S0FDWCwyQkFBMkI7S0FDM0IsWUFBWTtLQUNaLGtCQUFrQjtFQUNyQjs7O0FBRUE7SUFDRSxrQkFBa0I7SUFDbEIscUJBQXFCO0VBQ3ZCOzs7QUFFQTtJQUNFLHFDQUEyRDtJQUMzRCxlQUFlO0lBQ2YsV0FBVztJQUNYLFlBQVk7SUFDWixZQUFZO0VBQ2Q7OztBQUNBO0lBQ0Usa0JBQWtCO0lBQ2xCLHFCQUFxQjtFQUN2Qjs7O0FBRUE7SUFDRSx5Q0FBK0Q7SUFDL0QsZUFBZTtJQUNmLFdBQVc7SUFDWCxZQUFZO0lBQ1osWUFBWTtFQUNkOzs7QUFFQTtJQUNFLGtCQUFrQjtJQUNsQixxQkFBcUI7RUFDdkI7OztBQUVBO0lBQ0UsbURBQTJFO0lBQzNFLGVBQWU7SUFDZixXQUFXO0lBQ1gsWUFBWTtJQUNaLFlBQVk7RUFDZDs7O0FBRUE7SUFDRSxrQkFBa0I7SUFDbEIscUJBQXFCO0VBQ3ZCOzs7QUFFQTtJQUNFLHlDQUErRDtJQUMvRCxlQUFlO0lBQ2YsV0FBVztJQUNYLFlBQVk7SUFDWixZQUFZO0VBQ2Q7OztBQUNBO0lBQ0Usa0JBQWtCO0lBQ2xCLFNBQVM7SUFDVCxTQUFTO0VBQ1g7OztBQUVBO0lBQ0Usa0JBQWtCO0lBQ2xCLFNBQVM7SUFDVCxTQUFTO0VBQ1g7OztBQUVBO0lBQ0Usa0JBQWtCO0lBQ2xCLFNBQVM7SUFDVCxTQUFTO0VBQ1g7OztBQUVBO0lBQ0Usa0JBQWtCO0lBQ2xCLFNBQVM7SUFDVCxVQUFVO0VBQ1oiLCJmaWxlIjoiYXBwL3FyY29kZS9xcmNvZGUuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIiNidXR0b24xIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNiMmIyYjJcdDsgXHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMjVweCAyMnB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgZm9udC1zaXplOiAzNnB4O1xyXG4gICAgbWFyZ2luOiA0cHggMThweDtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIFxyXG4gIH1cclxuXHJcbiAgXHJcbiNidXR0b24yIHtcclxuICAgYmFja2dyb3VuZC1jb2xvcjogI2IyYjJiMlx0OyBcclxuICAgYm9yZGVyOiBub25lO1xyXG4gICBjb2xvcjogd2hpdGU7XHJcbiAgIHBhZGRpbmctYm90dG9tOiAyNXB4IDIycHg7XHJcbiAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgIGZvbnQtc2l6ZTogMzZweDtcclxuICAgbWFyZ2luOiA0cHggMThweDtcclxuICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gfVxyXG4gI2J1dHRvbjMge1xyXG4gICBiYWNrZ3JvdW5kLWNvbG9yOiAjYjJiMmIyXHQ7IFxyXG4gICBib3JkZXI6IG5vbmU7XHJcbiAgIGNvbG9yOiB3aGl0ZTtcclxuICAgcGFkZGluZy1ib3R0b206IDI1cHggMjJweDtcclxuICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgZm9udC1zaXplOiAzNnB4O1xyXG4gICBtYXJnaW46IDRweCAxOHB4O1xyXG4gICBjdXJzb3I6IHBvaW50ZXI7XHJcbiB9XHJcblxyXG4gI2J1dHRvbjQge1xyXG4gICBiYWNrZ3JvdW5kLWNvbG9yOiAjYjJiMmIyXHQ7XHJcbiAgIGJvcmRlcjogbm9uZTtcclxuICAgY29sb3I6IHdoaXRlO1xyXG4gICBwYWRkaW5nLWJvdHRvbTogMjVweCAyMnB4O1xyXG4gICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICBmb250LXNpemU6IDM2cHg7XHJcbiAgIG1hcmdpbjogNHB4IDE4cHg7XHJcbiAgIGN1cnNvcjogcG9pbnRlcjtcclxuIH1cclxuXHJcblxyXG5cclxuXHJcbiAjYnV0dG9uYSB7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2IyYjJiMlx0OyBcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIHBhZGRpbmctYm90dG9tOiAyNXB4IDIycHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBmb250LXNpemU6IDM2cHg7XHJcbiAgICBtYXJnaW46IDRweCAxOHB4O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgXHJcbiAgfVxyXG5cclxuICBcclxuI2J1dHRvbmIge1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcclxuICAgYmFja2dyb3VuZC1jb2xvcjogI2IyYjJiMlx0OyBcclxuICAgYm9yZGVyOiBub25lO1xyXG4gICBjb2xvcjogd2hpdGU7XHJcbiAgIHBhZGRpbmctYm90dG9tOiAyNXB4IDIycHg7XHJcbiAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgIGZvbnQtc2l6ZTogMzZweDtcclxuICAgbWFyZ2luOiA0cHggMThweDtcclxuICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gfVxyXG4gI2J1dHRvbmMge1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcclxuICAgYmFja2dyb3VuZC1jb2xvcjogI2IyYjJiMlx0OyBcclxuICAgYm9yZGVyOiBub25lO1xyXG4gICBjb2xvcjogd2hpdGU7XHJcbiAgIHBhZGRpbmctYm90dG9tOiAyNXB4IDIycHg7XHJcbiAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgIGZvbnQtc2l6ZTogMzZweDtcclxuICAgbWFyZ2luOiA0cHggMThweDtcclxuICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gfVxyXG5cclxuICNidXR0b25kIHtcclxuICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbiAgIGJhY2tncm91bmQtY29sb3I6ICNiMmIyYjJcdDtcclxuICAgYm9yZGVyOiBub25lO1xyXG4gICBjb2xvcjogd2hpdGU7XHJcbiAgIHBhZGRpbmctYm90dG9tOiAyNXB4IDIycHg7XHJcbiAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgIGZvbnQtc2l6ZTogMzZweDtcclxuICAgbWFyZ2luOiA0cHggMThweDtcclxuICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gfVxyXG4gI2J1dHRvbmUge1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNiMmIyYjJcdDtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIHBhZGRpbmctYm90dG9tOiAyNXB4IDIycHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBmb250LXNpemU6IDM2cHg7XHJcbiAgICBtYXJnaW46IDRweCAxOHB4O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gIH1cclxuICBcclxuIFxyXG4gICNhbGx7XHJcbiAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgIHBvc2l0aW9uOiBmaXhlZDtcclxuICAgICBsZWZ0OiAwO1xyXG4gICAgIGJvdHRvbTogMDtcclxuICAgICB3aWR0aDogMTAwJTtcclxuICAgICBiYWNrZ3JvdW5kLWNvbG9yOiBcdFx0I2IyYjJiMjtcclxuICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIH1cclxuXHJcbiAgZGl2LmhvbWUge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIH1cclxuXHJcbiAgZGl2LmhvbWUgaW5wdXQge1xyXG4gICAgYmFja2dyb3VuZDogdXJsKCdzcmMvYXBwL2hlYWRlci9pbWFnZXMvaG9tZS5zdmcnKSBuby1yZXBlYXQ7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICB3aWR0aDogNTBweDtcclxuICAgIGhlaWdodDogNTBweDtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICB9XHJcbiAgZGl2LmFuYWx5dGljIHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICB9XHJcblxyXG4gIGRpdi5hbmFseXRpYyBpbnB1dCB7XHJcbiAgICBiYWNrZ3JvdW5kOiB1cmwoJ3NyYy9hcHAvaGVhZGVyL2ltYWdlcy9hbmFseXRpYy5zdmcnKSBuby1yZXBlYXQ7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICB3aWR0aDogNTBweDtcclxuICAgIGhlaWdodDogNTBweDtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICB9XHJcblxyXG4gIGRpdi5ub3RpZmljYXRpb24ge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIH1cclxuXHJcbiAgZGl2Lm5vdGlmaWNhdGlvbiBpbnB1dCB7XHJcbiAgICBiYWNrZ3JvdW5kOiB1cmwoJ3NyYy9hcHAvY2FsZW5kYXIvaW1hZ2VzL291dGxpbmUtdG9kYXktMjRweC5zdmcnKSBuby1yZXBlYXQ7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICB3aWR0aDogNTBweDtcclxuICAgIGhlaWdodDogNTBweDtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICB9XHJcbiAgXHJcbiAgZGl2LnNldHRpbmcge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIH1cclxuXHJcbiAgZGl2LnNldHRpbmcgaW5wdXQge1xyXG4gICAgYmFja2dyb3VuZDogdXJsKCdzcmMvYXBwL2hlYWRlci9pbWFnZXMvc2V0dGluZ3Muc3ZnJykgbm8tcmVwZWF0O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgd2lkdGg6IDUwcHg7XHJcbiAgICBoZWlnaHQ6IDUwcHg7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgfVxyXG4gIHNwYW4uaG9tZSB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDMycHg7XHJcbiAgICBsZWZ0OiA2cHg7XHJcbiAgfVxyXG4gIFxyXG4gIHNwYW4uYW5hbHl0aWMge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAzMnB4O1xyXG4gICAgbGVmdDogMnB4O1xyXG4gIH1cclxuXHJcbiAgc3Bhbi5ub3RpZmljYXRpb24ge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAzMnB4O1xyXG4gICAgbGVmdDogMnB4O1xyXG4gIH1cclxuXHJcbiAgc3Bhbi5zZXR0aW5nIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogMzJweDtcclxuICAgIGxlZnQ6IDEycHg7XHJcbiAgfVxyXG5cclxuIl19 */"

/***/ }),

/***/ "./src/app/qrcode/qrcode.component.html":
/*!**********************************************!*\
  !*** ./src/app/qrcode/qrcode.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p align=\"center\">\n  Bonus and work out!\n</p>\n<div id=\"all2\" align=\"center\">\n<ngx-qrcode qrc-element-type=\"url\" [qrc-value]=\"ngxQrcode2\" ></ngx-qrcode>\n<ngx-qrcode qrc-element-type=\"url\" [qrc-value]=\"techiediaries\" ></ngx-qrcode>\n<ngx-qrcode qrc-element-type=\"url\" [qrc-value]=\"letsboot\" ></ngx-qrcode>\n</div>\n\n\n\n<div id=\"all\">\n\n  \n  <div class=\"home\">\n      <span id=\"home\" class=\"home\">Home  </span>\n      <input id=\"button1\" type=\"button\"  (click)=\"submitted=false\" (click)=\"goToPageMenu('/', 'main')\">\n    \n  </div>\n\n   <div class=\"analytic\">\n    <span id=\"analytic\" class=\"analytic\">Analytics  </span>\n    <input id=\"button2\" type=\"button\" alt=\"Submit\" width=\"48\" height=\"48\" (click)=\"submitted=false\" (click)=\"goToPageMenu('/', 'analytic')\">\n\n  </div>\n\n  <div class=\"notification\">\n      <span id=\"notification\" class=\"notification\">Calendar  </span>\n      <input id=\"button3\" type=\"button\" alt=\"Submit\" width=\"48\" height=\"48\" (click)=\"submitted=false\" (click)=\"goToPageMenu('/', 'calendar')\">\n  \n    </div>\n  \n    <div class=\"setting\">\n        <span id=\"setting\" class=\"setting\">Settings  </span>\n        <input id=\"button4\" type=\"button\" alt=\"Submit\" width=\"48\" height=\"48\" (click)=\"submitted=false\" (click)=\"goToPageMenu('/', 'settings')\">\n    \n      </div>\n \n   \n\n</div>\n"

/***/ }),

/***/ "./src/app/qrcode/qrcode.component.ts":
/*!********************************************!*\
  !*** ./src/app/qrcode/qrcode.component.ts ***!
  \********************************************/
/*! exports provided: QrcodeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QrcodeComponent", function() { return QrcodeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./src/node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var qrcode__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! qrcode */ "./src/node_modules/qrcode/lib/browser.js");
/* harmony import */ var qrcode__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(qrcode__WEBPACK_IMPORTED_MODULE_2__);



var QrcodeComponent = /** @class */ (function () {
    function QrcodeComponent() {
        this.ngxQrcode2 = 'https://www.npmjs.com/package/ngx-qrcode2';
        this.techiediaries = 'https://www.npmjs.com/~techiediaries';
        this.letsboot = 'https://www.letsboot.com/';
    }
    QrcodeComponent.prototype.ngOnInit = function () {
        //var image="https://upload.wikimedia.org/wikipedia/commons/thumb/1/1c/Kyriakos_Mitsotakis_%28cropped%29.jpg/800px-Kyriakos_Mitsotakis_%28cropped%29.jpg"
        var _this = this;
        // With promises
        qrcode__WEBPACK_IMPORTED_MODULE_2___default.a.toDataURL('I am a pony!')
            .then(function (url) {
            console.log(url);
            alert("utl is:" + url);
        })
            .catch(function (err) {
            console.error(err);
        });
        // With async/await
        var generateQR = function (text) { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var _a, err_1;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        _a = alert;
                        return [4 /*yield*/, qrcode__WEBPACK_IMPORTED_MODULE_2___default.a.toDataURL(text)];
                    case 1:
                        _a.apply(void 0, [_b.sent()]);
                        return [3 /*break*/, 3];
                    case 2:
                        err_1 = _b.sent();
                        console.error(err_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        }); };
    };
    QrcodeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-qrcode',
            template: __webpack_require__(/*! ./qrcode.component.html */ "./src/app/qrcode/qrcode.component.html"),
            styles: [__webpack_require__(/*! ./qrcode.component.css */ "./src/app/qrcode/qrcode.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], QrcodeComponent);
    return QrcodeComponent;
}());



/***/ }),

/***/ "./src/app/settings/settings.component.css":
/*!*************************************************!*\
  !*** ./src/app/settings/settings.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#button1 {\r\n    background-color: #b2b2b2\t; \r\n    border: none;\r\n    color: white;\r\n    padding-bottom: 25px 22px;\r\n    text-align: center;\r\n    text-decoration: none;\r\n    display: inline-block;\r\n    font-size: 36px;\r\n    margin: 4px 18px;\r\n    cursor: pointer;\r\n    \r\n  }\r\n\r\n  \r\n#button2 {\r\n   background-color: #b2b2b2\t; \r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n\r\n  \r\n#button3 {\r\n   background-color: #b2b2b2\t; \r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n\r\n  \r\n#button4 {\r\n   background-color: #b2b2b2\t;\r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n\r\n  \r\n#buttona {\r\n    border-radius: 25px;\r\n    background-color: #b2b2b2\t; \r\n    border: none;\r\n    color: white;\r\n    padding-bottom: 25px 22px;\r\n    text-align: center;\r\n    text-decoration: none;\r\n    display: inline-block;\r\n    font-size: 36px;\r\n    margin: 4px 18px;\r\n    cursor: pointer;\r\n    \r\n  }\r\n\r\n  \r\n#buttonb {\r\n    border-radius: 25px;\r\n   background-color: #b2b2b2\t; \r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n\r\n  \r\n#buttonc {\r\n    border-radius: 25px;\r\n   background-color: #b2b2b2\t; \r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n\r\n  \r\n#buttond {\r\n    border-radius: 25px;\r\n   background-color: #b2b2b2\t;\r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n\r\n  \r\n#buttone {\r\n    border-radius: 25px;\r\n    background-color: #b2b2b2\t;\r\n    border: none;\r\n    color: white;\r\n    padding-bottom: 25px 22px;\r\n    text-align: center;\r\n    text-decoration: none;\r\n    display: inline-block;\r\n    font-size: 36px;\r\n    margin: 4px 18px;\r\n    cursor: pointer;\r\n  }\r\n\r\n  \r\n#all{\r\n     text-align: center;\r\n     position: fixed;\r\n     left: 0;\r\n     bottom: 0;\r\n     width: 100%;\r\n     background-color: \t\t#b2b2b2;\r\n     color: white;\r\n     text-align: center;\r\n  }\r\n\r\n  \r\ndiv.home {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\n\r\n  \r\ndiv.home input {\r\n    background: url('home.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\n\r\n  \r\ndiv.analytic {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\n\r\n  \r\ndiv.analytic input {\r\n    background: url('analytic.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\n\r\n  \r\ndiv.notification {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\n\r\n  \r\ndiv.notification input {\r\n    background: url('outline-today-24px.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\n\r\n  \r\ndiv.setting {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\n\r\n  \r\ndiv.setting input {\r\n    background: url('settings.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\n\r\n  \r\nspan.home {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 6px;\r\n  }\r\n\r\n  \r\nspan.analytic {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 2px;\r\n  }\r\n\r\n  \r\nspan.notification {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 2px;\r\n  }\r\n\r\n  \r\nspan.setting {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 12px;\r\n  }\r\n\r\n  \r\n#maincontenthome {\r\n   \r\n    background: #FFFFFF;\r\n    padding: 10px; \r\n    width: 100%;\r\n  \r\n    \r\n    height: 100%;  \r\n  }\r\n\r\n  \r\n#in {\r\n    border-radius: 25px;\r\n    background: #E8E8E8;\r\n   \r\n    width: 100%;\r\n  \r\n    \r\n    height: 100%;  \r\n  }\r\n\r\n  \r\nimg {\r\n    border-radius: 50%;\r\n    padding:15px;\r\n  }\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zZXR0aW5ncy9zZXR0aW5ncy5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksMEJBQTBCO0lBQzFCLFlBQVk7SUFDWixZQUFZO0lBQ1oseUJBQXlCO0lBQ3pCLGtCQUFrQjtJQUNsQixxQkFBcUI7SUFDckIscUJBQXFCO0lBQ3JCLGVBQWU7SUFDZixnQkFBZ0I7SUFDaEIsZUFBZTs7RUFFakI7OztBQUdGO0dBQ0csMEJBQTBCO0dBQzFCLFlBQVk7R0FDWixZQUFZO0dBQ1oseUJBQXlCO0dBQ3pCLGtCQUFrQjtHQUNsQixxQkFBcUI7R0FDckIscUJBQXFCO0dBQ3JCLGVBQWU7R0FDZixnQkFBZ0I7R0FDaEIsZUFBZTtDQUNqQjs7O0FBQ0E7R0FDRSwwQkFBMEI7R0FDMUIsWUFBWTtHQUNaLFlBQVk7R0FDWix5QkFBeUI7R0FDekIsa0JBQWtCO0dBQ2xCLHFCQUFxQjtHQUNyQixxQkFBcUI7R0FDckIsZUFBZTtHQUNmLGdCQUFnQjtHQUNoQixlQUFlO0NBQ2pCOzs7QUFFQTtHQUNFLDBCQUEwQjtHQUMxQixZQUFZO0dBQ1osWUFBWTtHQUNaLHlCQUF5QjtHQUN6QixrQkFBa0I7R0FDbEIscUJBQXFCO0dBQ3JCLHFCQUFxQjtHQUNyQixlQUFlO0dBQ2YsZ0JBQWdCO0dBQ2hCLGVBQWU7Q0FDakI7OztBQUtBO0lBQ0csbUJBQW1CO0lBQ25CLDBCQUEwQjtJQUMxQixZQUFZO0lBQ1osWUFBWTtJQUNaLHlCQUF5QjtJQUN6QixrQkFBa0I7SUFDbEIscUJBQXFCO0lBQ3JCLHFCQUFxQjtJQUNyQixlQUFlO0lBQ2YsZ0JBQWdCO0lBQ2hCLGVBQWU7O0VBRWpCOzs7QUFHRjtJQUNJLG1CQUFtQjtHQUNwQiwwQkFBMEI7R0FDMUIsWUFBWTtHQUNaLFlBQVk7R0FDWix5QkFBeUI7R0FDekIsa0JBQWtCO0dBQ2xCLHFCQUFxQjtHQUNyQixxQkFBcUI7R0FDckIsZUFBZTtHQUNmLGdCQUFnQjtHQUNoQixlQUFlO0NBQ2pCOzs7QUFDQTtJQUNHLG1CQUFtQjtHQUNwQiwwQkFBMEI7R0FDMUIsWUFBWTtHQUNaLFlBQVk7R0FDWix5QkFBeUI7R0FDekIsa0JBQWtCO0dBQ2xCLHFCQUFxQjtHQUNyQixxQkFBcUI7R0FDckIsZUFBZTtHQUNmLGdCQUFnQjtHQUNoQixlQUFlO0NBQ2pCOzs7QUFFQTtJQUNHLG1CQUFtQjtHQUNwQiwwQkFBMEI7R0FDMUIsWUFBWTtHQUNaLFlBQVk7R0FDWix5QkFBeUI7R0FDekIsa0JBQWtCO0dBQ2xCLHFCQUFxQjtHQUNyQixxQkFBcUI7R0FDckIsZUFBZTtHQUNmLGdCQUFnQjtHQUNoQixlQUFlO0NBQ2pCOzs7QUFDQTtJQUNHLG1CQUFtQjtJQUNuQiwwQkFBMEI7SUFDMUIsWUFBWTtJQUNaLFlBQVk7SUFDWix5QkFBeUI7SUFDekIsa0JBQWtCO0lBQ2xCLHFCQUFxQjtJQUNyQixxQkFBcUI7SUFDckIsZUFBZTtJQUNmLGdCQUFnQjtJQUNoQixlQUFlO0VBQ2pCOzs7QUFHQTtLQUNHLGtCQUFrQjtLQUNsQixlQUFlO0tBQ2YsT0FBTztLQUNQLFNBQVM7S0FDVCxXQUFXO0tBQ1gsMkJBQTJCO0tBQzNCLFlBQVk7S0FDWixrQkFBa0I7RUFDckI7OztBQUVBO0lBQ0Usa0JBQWtCO0lBQ2xCLHFCQUFxQjtFQUN2Qjs7O0FBRUE7SUFDRSxxQ0FBMkQ7SUFDM0QsZUFBZTtJQUNmLFdBQVc7SUFDWCxZQUFZO0lBQ1osWUFBWTtFQUNkOzs7QUFDQTtJQUNFLGtCQUFrQjtJQUNsQixxQkFBcUI7RUFDdkI7OztBQUVBO0lBQ0UseUNBQStEO0lBQy9ELGVBQWU7SUFDZixXQUFXO0lBQ1gsWUFBWTtJQUNaLFlBQVk7RUFDZDs7O0FBRUE7SUFDRSxrQkFBa0I7SUFDbEIscUJBQXFCO0VBQ3ZCOzs7QUFFQTtJQUNFLG1EQUEyRTtJQUMzRSxlQUFlO0lBQ2YsV0FBVztJQUNYLFlBQVk7SUFDWixZQUFZO0VBQ2Q7OztBQUVBO0lBQ0Usa0JBQWtCO0lBQ2xCLHFCQUFxQjtFQUN2Qjs7O0FBRUE7SUFDRSx5Q0FBK0Q7SUFDL0QsZUFBZTtJQUNmLFdBQVc7SUFDWCxZQUFZO0lBQ1osWUFBWTtFQUNkOzs7QUFDQTtJQUNFLGtCQUFrQjtJQUNsQixTQUFTO0lBQ1QsU0FBUztFQUNYOzs7QUFFQTtJQUNFLGtCQUFrQjtJQUNsQixTQUFTO0lBQ1QsU0FBUztFQUNYOzs7QUFFQTtJQUNFLGtCQUFrQjtJQUNsQixTQUFTO0lBQ1QsU0FBUztFQUNYOzs7QUFFQTtJQUNFLGtCQUFrQjtJQUNsQixTQUFTO0lBQ1QsVUFBVTtFQUNaOzs7QUFFQTs7SUFFRSxtQkFBbUI7SUFDbkIsYUFBYTtJQUNiLFdBQVc7OztJQUdYLFlBQVk7RUFDZDs7O0FBQ0E7SUFDRSxtQkFBbUI7SUFDbkIsbUJBQW1COztJQUVuQixXQUFXOzs7SUFHWCxZQUFZO0VBQ2Q7OztBQUVBO0lBQ0Usa0JBQWtCO0lBQ2xCLFlBQVk7RUFDZCIsImZpbGUiOiJhcHAvc2V0dGluZ3Mvc2V0dGluZ3MuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIiNidXR0b24xIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNiMmIyYjJcdDsgXHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMjVweCAyMnB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgZm9udC1zaXplOiAzNnB4O1xyXG4gICAgbWFyZ2luOiA0cHggMThweDtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIFxyXG4gIH1cclxuXHJcbiAgXHJcbiNidXR0b24yIHtcclxuICAgYmFja2dyb3VuZC1jb2xvcjogI2IyYjJiMlx0OyBcclxuICAgYm9yZGVyOiBub25lO1xyXG4gICBjb2xvcjogd2hpdGU7XHJcbiAgIHBhZGRpbmctYm90dG9tOiAyNXB4IDIycHg7XHJcbiAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgIGZvbnQtc2l6ZTogMzZweDtcclxuICAgbWFyZ2luOiA0cHggMThweDtcclxuICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gfVxyXG4gI2J1dHRvbjMge1xyXG4gICBiYWNrZ3JvdW5kLWNvbG9yOiAjYjJiMmIyXHQ7IFxyXG4gICBib3JkZXI6IG5vbmU7XHJcbiAgIGNvbG9yOiB3aGl0ZTtcclxuICAgcGFkZGluZy1ib3R0b206IDI1cHggMjJweDtcclxuICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgZm9udC1zaXplOiAzNnB4O1xyXG4gICBtYXJnaW46IDRweCAxOHB4O1xyXG4gICBjdXJzb3I6IHBvaW50ZXI7XHJcbiB9XHJcblxyXG4gI2J1dHRvbjQge1xyXG4gICBiYWNrZ3JvdW5kLWNvbG9yOiAjYjJiMmIyXHQ7XHJcbiAgIGJvcmRlcjogbm9uZTtcclxuICAgY29sb3I6IHdoaXRlO1xyXG4gICBwYWRkaW5nLWJvdHRvbTogMjVweCAyMnB4O1xyXG4gICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICBmb250LXNpemU6IDM2cHg7XHJcbiAgIG1hcmdpbjogNHB4IDE4cHg7XHJcbiAgIGN1cnNvcjogcG9pbnRlcjtcclxuIH1cclxuXHJcblxyXG5cclxuXHJcbiAjYnV0dG9uYSB7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2IyYjJiMlx0OyBcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIHBhZGRpbmctYm90dG9tOiAyNXB4IDIycHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBmb250LXNpemU6IDM2cHg7XHJcbiAgICBtYXJnaW46IDRweCAxOHB4O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgXHJcbiAgfVxyXG5cclxuICBcclxuI2J1dHRvbmIge1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcclxuICAgYmFja2dyb3VuZC1jb2xvcjogI2IyYjJiMlx0OyBcclxuICAgYm9yZGVyOiBub25lO1xyXG4gICBjb2xvcjogd2hpdGU7XHJcbiAgIHBhZGRpbmctYm90dG9tOiAyNXB4IDIycHg7XHJcbiAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgIGZvbnQtc2l6ZTogMzZweDtcclxuICAgbWFyZ2luOiA0cHggMThweDtcclxuICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gfVxyXG4gI2J1dHRvbmMge1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcclxuICAgYmFja2dyb3VuZC1jb2xvcjogI2IyYjJiMlx0OyBcclxuICAgYm9yZGVyOiBub25lO1xyXG4gICBjb2xvcjogd2hpdGU7XHJcbiAgIHBhZGRpbmctYm90dG9tOiAyNXB4IDIycHg7XHJcbiAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgIGZvbnQtc2l6ZTogMzZweDtcclxuICAgbWFyZ2luOiA0cHggMThweDtcclxuICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gfVxyXG5cclxuICNidXR0b25kIHtcclxuICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbiAgIGJhY2tncm91bmQtY29sb3I6ICNiMmIyYjJcdDtcclxuICAgYm9yZGVyOiBub25lO1xyXG4gICBjb2xvcjogd2hpdGU7XHJcbiAgIHBhZGRpbmctYm90dG9tOiAyNXB4IDIycHg7XHJcbiAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgIGZvbnQtc2l6ZTogMzZweDtcclxuICAgbWFyZ2luOiA0cHggMThweDtcclxuICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gfVxyXG4gI2J1dHRvbmUge1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNiMmIyYjJcdDtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIHBhZGRpbmctYm90dG9tOiAyNXB4IDIycHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBmb250LXNpemU6IDM2cHg7XHJcbiAgICBtYXJnaW46IDRweCAxOHB4O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gIH1cclxuICBcclxuIFxyXG4gICNhbGx7XHJcbiAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgIHBvc2l0aW9uOiBmaXhlZDtcclxuICAgICBsZWZ0OiAwO1xyXG4gICAgIGJvdHRvbTogMDtcclxuICAgICB3aWR0aDogMTAwJTtcclxuICAgICBiYWNrZ3JvdW5kLWNvbG9yOiBcdFx0I2IyYjJiMjtcclxuICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIH1cclxuXHJcbiAgZGl2LmhvbWUge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIH1cclxuXHJcbiAgZGl2LmhvbWUgaW5wdXQge1xyXG4gICAgYmFja2dyb3VuZDogdXJsKCdzcmMvYXBwL2hlYWRlci9pbWFnZXMvaG9tZS5zdmcnKSBuby1yZXBlYXQ7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICB3aWR0aDogNTBweDtcclxuICAgIGhlaWdodDogNTBweDtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICB9XHJcbiAgZGl2LmFuYWx5dGljIHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICB9XHJcblxyXG4gIGRpdi5hbmFseXRpYyBpbnB1dCB7XHJcbiAgICBiYWNrZ3JvdW5kOiB1cmwoJ3NyYy9hcHAvaGVhZGVyL2ltYWdlcy9hbmFseXRpYy5zdmcnKSBuby1yZXBlYXQ7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICB3aWR0aDogNTBweDtcclxuICAgIGhlaWdodDogNTBweDtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICB9XHJcblxyXG4gIGRpdi5ub3RpZmljYXRpb24ge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIH1cclxuXHJcbiAgZGl2Lm5vdGlmaWNhdGlvbiBpbnB1dCB7XHJcbiAgICBiYWNrZ3JvdW5kOiB1cmwoJ3NyYy9hcHAvY2FsZW5kYXIvaW1hZ2VzL291dGxpbmUtdG9kYXktMjRweC5zdmcnKSBuby1yZXBlYXQ7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICB3aWR0aDogNTBweDtcclxuICAgIGhlaWdodDogNTBweDtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICB9XHJcbiAgXHJcbiAgZGl2LnNldHRpbmcge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIH1cclxuXHJcbiAgZGl2LnNldHRpbmcgaW5wdXQge1xyXG4gICAgYmFja2dyb3VuZDogdXJsKCdzcmMvYXBwL2hlYWRlci9pbWFnZXMvc2V0dGluZ3Muc3ZnJykgbm8tcmVwZWF0O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgd2lkdGg6IDUwcHg7XHJcbiAgICBoZWlnaHQ6IDUwcHg7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgfVxyXG4gIHNwYW4uaG9tZSB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDMycHg7XHJcbiAgICBsZWZ0OiA2cHg7XHJcbiAgfVxyXG4gIFxyXG4gIHNwYW4uYW5hbHl0aWMge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAzMnB4O1xyXG4gICAgbGVmdDogMnB4O1xyXG4gIH1cclxuXHJcbiAgc3Bhbi5ub3RpZmljYXRpb24ge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAzMnB4O1xyXG4gICAgbGVmdDogMnB4O1xyXG4gIH1cclxuXHJcbiAgc3Bhbi5zZXR0aW5nIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogMzJweDtcclxuICAgIGxlZnQ6IDEycHg7XHJcbiAgfVxyXG5cclxuICAjbWFpbmNvbnRlbnRob21lIHtcclxuICAgXHJcbiAgICBiYWNrZ3JvdW5kOiAjRkZGRkZGO1xyXG4gICAgcGFkZGluZzogMTBweDsgXHJcbiAgICB3aWR0aDogMTAwJTtcclxuICBcclxuICAgIFxyXG4gICAgaGVpZ2h0OiAxMDAlOyAgXHJcbiAgfVxyXG4gICNpbiB7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xyXG4gICAgYmFja2dyb3VuZDogI0U4RThFODtcclxuICAgXHJcbiAgICB3aWR0aDogMTAwJTtcclxuICBcclxuICAgIFxyXG4gICAgaGVpZ2h0OiAxMDAlOyAgXHJcbiAgfVxyXG5cclxuICBpbWcge1xyXG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgcGFkZGluZzoxNXB4O1xyXG4gIH0iXX0= */"

/***/ }),

/***/ "./src/app/settings/settings.component.html":
/*!**************************************************!*\
  !*** ./src/app/settings/settings.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"maincontenthome\">\n    <br>\n    <br>\n  <div > <h1 id=\"nameheader\"  align=\"center\">Fanis Alevizakis</h1> </div>\n  <br>\n  <br>\n  <br>\n  <div id=\"edit\"> <h4  align=\"center\">edit person</h4> </div>\n  <br>\n  <br>\n  <div class=\"container\" >\n      <div class=\"row\">\n  \n         <div id=\"thanks\" class=\"col-sm-12 \">\n            <h4  align=\"center\">Thanks lot</h4>\n           </div>\n        </div>\n    </div>\n\n\n\n  <div class=\"container\" >\n    <div class=\"row\">\n\n       <div class=\"col-sm-12 \">\n          <div align=\"center\" id=\"in\">\n            \n              <img align=\"left\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAMAAACahl6sAAAAM1BMVEUKME7///+El6bw8vQZPVlHZHpmfpHCy9Ojsbzg5ekpSmTR2N44V29XcYayvsd2i5yTpLFbvRYnAAAJcklEQVR4nO2d17arOgxFs+kkofz/154Qmg0uKsuQccddT/vhnOCJLclFMo+//4gedzcApf9B4srrusk+GsqPpj+ypq7zVE9LAdLWWVU+Hx69y2FMwAMGyfusLHwIpooyw9IAQfK+8naDp3OGHvZ0FMhrfPMgVnVjC2kABOQ1MLvi0DEIFj1ILu0LU2WjNRgtSF3pKb4qqtd9IHmjGlJHlc09IHlGcrQcPeUjTAySAGNSkQlRhCCJMGaUC0HSYUx6SmxFAtJDTdylsr4ApC1TY0yquKbCBkk7qnYVzPHFBHkBojhVJWviwgPJrsP4qBgTgbQXdsesjm4pDJDmIuswVZDdFx0ENTtkihoeqSDXD6tVxOFFBHndMKxWvUnzexpIcx/Gg2goJJDhVo6PCMGRAnKTmZuKm3wcJO/upphUqUHy29yVrRhJDORXOKIkEZDf4YiRhEF+iSNCEgb5KY4wSRDkB/yurUEG8nMcocgYABnvbrVL3nMIP0h/d5udKnwzSC/InfPdkJ6eWb0PJE++dyVVyQP5iQmWW27X5QG5druEKafBu0Hqu9saVOHa8HKC/K6BzHKZiRMEZCDF0Nd1/ZfXI/fcOibHOssFgokg9uFA20BhztHEAZIjIohrD/o1wljeFBDEwBo8YUt5Ir/rNLjOIACPFdy/AbEcPdcJBOCxytjeYAM4Kzp6rhOIPhRGNzwmFP3rOoTFI0irtnQKx6fj1Zt+h9njEUS9mKJxfFRrX5lt7wcQtaWTOfTHeIXVJQcQrRW+OYex2j0a66XZINoO8a7fPH2iHF2mC7ZBtB3Czb5QvjizSx7A3308mRzqAwujSywQbYfwc0iU8zqjS0yQ6ztEHX9332KCaGNIYB/Qq1z3yN0oDZBWyeFYJBCkm2sXLhDtpKFwNDMu5TnrZpYGiHbK4Nlwikg5DrYV1g6iPoJmzE5MKd/fOp53EPUaQZaLqH3u+vo2ELWp3wSyWuYGoj9EEIJoV3L9AUS/ZLsJpLNBXmqOu0CW6P5A/dx9IL0FAji/FYKot9EqE0Tvs6QBUe/2CxMEkZAlBNGPhdoAQWyTSmbxUwvUygwQyMmniAPgLt87CODXHuftWJIQgzrfQDC5AfwSgz9MmmG/gWCOqDgZ4JsQeTvZBoJJDhAFEsSDyxUEEUUekk0UEMhjBcEcGsoWVpBU3NcCgkkPkJWrKbdRZvULCMTWhYEdMrayBQRyqHcnSLmAIH7LcWJ8Hch7BsHEdWFpJsZjziCgFBpZ9TPm4e0XBJTTJKt9xjy8RoLI4gimPLP5goCSgWTrEcyzsy8IqmZVMo0H5bJiQToBCOjZ5RcElhjLN3dU7uQMAvoxwQkJZKI1CQzCthJYEigahHuDDi4rFwzCPQ7F1fiDQZgTR5iJwEGYRgIsiECD8BwwMAEfDcIaW8CRBQdhjS1kJQEchDEFhiRKr4KDFPS9FGQNVwEHoW83QjsEHdkfnuIOl6C1NjMItiaCaCWgbdpFJXQ9soh2uoB9aJcCxFdgZwlcrTmvENGlrITBBdpK25Qhd1F2RScq8CKu/gsCL8qN5THjy+Rr5E6joYgPxpdl518QrCf8Kpgjn6C8HLkbb+vt7ZM8wdVvy258khsRfHaS5DalDnlidZT7Erk+SXV5Bj1D3LS29XyhVJuoKHs9Q8S6reK11oUc7vPcr9uswP3SLiDINefXOF5rwCuGzVT6zVkVPfh2wWmHcz4wAwba2cgN1/Tsvleu7//i69CgVyt1GwjOs2+XK3rtbl151Tg3vOeioG40Mz2V+6pQ4xbJHOZj6g0EMxk93tV7fuedvVZpQSPhbwNBGInrymGrwNh1GXmL8F+lAaJ+NU/fzcmvJqvKj7177+1v1GY/GiBKI1Fdy/2XK6upXwaIJpI8B/399W0mH9zzafKaeCF9J0WF+jyCuFusTGzZKhFH8dVLZql2brxgcdVBKb7KG/7UZTmB3XJ6uL/QYT5ScRI74FcHEJ7feopyfGkaeaGlPoCw/BbjZmSBWIvINQNmTxdjWJqwUI8sztR4nYPuIPSTSUnOCZOE3ierqRoJfNSQxDjLEYs8i91eqgFCDSWiFHiuqAN9CwEGCPEISVjvwhS7Mfx6dtX8kC5aqvneGBOEFN2v6RBiYwr3DQOkLhEW6fHFbIwFQnkLiWYmZxE220z/aedPx99C+hiyKR4OzNFhg8S75CJTnxQ1dyugHTLaY10iu9dBpmhQtMz1ABLrkgtHVnRsPUO3OcU25i8cWdGxZbflCBKJqBdMs3aF/dYhNexU9RFcYEmLXYQKghyWdufyldBSU3KpjkKhZclxTXQGCTkL/HZDUIH5+Gkt4SgoCtj7pSYSNJLTK3VVRnmXZxebSMBIzmHABeIdXBebiN9eHYtUZ62ab3BdGkUm+SKJw1bdRXeewaX7qqdAnljg2sVxg3guAk3baofcg9yZ2eZpnHNvSFrEqhB9YPjesmt0pt6Xc8hl7W5L9Q4Xx09ctsrd5VhWeF6nF8SRrZdw49qns//0xTK/AZ8vGr3caTliuzeFNeCJTgafpKlhHd2WP1sy1LqDF798gjKJPLqDr9keoTd43+NyNzC1CI8Xy2lcPtOaVBI5IiAWyQ3e125AcKoXs2Djhy5eVc3KiBxREIPkhjBiLhIjU++4T91IbggjRiCJLSEIwWGddkEaxlVN5KCArPHk8mXVpHk8FHH7JL3n5dPA7C90q7XkeFJucacNmGXeRfswLE71HA79efaGiCN/Ofjmfmtcp8X10tIsqCacV5xfRWjNUiXGYbovWgyFYHcQLak15K9oM5zqmgaeKsHJetbSHfSPzXOiw/rxE9YH4CXaUpsZ0ztemFurP95Jpyvrd29YTpIZr7cEJHqfc7Wl0PFm2+yJR70udaokKFtGPTdm8WdQe24+HmVLlueboWQquBcYYVH2vEzfh8kCks1p90eWsLCyZ8qK7E86Oe+3XYFnBuiWdth20UqZR5SvMoyPg3WNauJipi0LMTQgVq5xUUlZcrPsopPHJ926z8pm7xyFLrH/PxpHSoXKdWgXsLn1scZn1ZDd/2vszN3lt254qkE+qu3yoqLM+ghN3Qz2qcVzUC/ZMFsK/alU6l0OWV/bQz6v6yYbyuN5BaZ4A7Y30vs/PPksS2+qzlvfF7OQmzzcL7W+xa7OIfRuVdtn/tdvdFLnL4OTKcm2W16PmWc4FWWXNSlWM2n3D+uPxuyrcfo74aP+Ac30a82+oLmfAAAAAElFTkSuQmCC\" alt=\"Avatar\" style=\"width:200px\">\n              \n              <div class=\"container\">\n            \n                  <div class=\"row\">\n                   \n                      <div class=\"col-sm-12 \">\n                          <div class=\"wrap-input100 validate-input\" >\n                              <input class=\"input100\" type=\"text\" name=\"username\" [(ngModel)]=\"username\" >\n                              <span class=\"focus-input100\" data-placeholder=\"username\" ></span>\n                            </div>\n                         \n                      </div>\n\n\n                      <div class=\"col-sm-12 \">\n                         \n                          <div class=\"wrap-input100 validate-input\" data-validate=\"Enter password\" >\n                              <span class=\"btn-show-pass\">\n                                <i class=\"zmdi zmdi-eye\"></i>\n                              </span>\n                              <input class=\"input100\" type=\"password\" name=\"password\" [(ngModel)]=\"password\" >\n                              <div align=\"right\" id=\"passworderror\"></div>\n                              <span class=\"focus-input100\" data-placeholder=\"password\"></span>\n                          </div>\n                             \n                      </div>\n\n                          \n\n                    <!--  <div class=\"col-sm-12 \">\n                          <div class=\"wrap-input100 validate-input\" >\n                              <input class=\"input100\" type=\"text\" name=\"age\" [(ngModel)]=\"age\" >\n                              <span class=\"focus-input100\" data-placeholder=\"age\"></span>\n                            </div>\n                         \n                      </div>-->\n                     \n                      <div class=\"wrap-input100 validate-input\" >\n                          <input class=\"input100\" type=\"text\" name=\"weight\" [(ngModel)]=\"weight\" >\n                          <span class=\"focus-input100\" data-placeholder=\"weight (kg)\"></span>\n                          <div align=\"right\" id=\"weighterror\"></div>\n                        </div>\n                       \n\n                   </div>\n                   <div class=\"col-sm-12 \">\n                     \n                    \n                    <button id=\"settingbutton\" color=\"blue\" class=\"login100-form-btn\"  (click)=\"submitted=false\" (click)=\"Submit()\"  >\n                       submit\n                    </button>\n                  </div>\n\n              \n                 \n                </div>\n              \n\n             \n                \n             \n           \n              </div>\n        </div>\n     \n   </div>\n\n  </div>\n\n  \n</div>\n\n\n\n<div id=\"all\">\n\n  \n  <div class=\"home\">\n      <span id=\"home\" class=\"home\">Home  </span>\n      <input id=\"button1\" type=\"button\"  (click)=\"submitted=false\" (click)=\"goToPageMenu('/', 'main')\">\n    \n  </div>\n\n   <div class=\"analytic\">\n    <span id=\"analytic\" class=\"analytic\">Analytics  </span>\n    <input id=\"button2\" type=\"button\" alt=\"Submit\" width=\"48\" height=\"48\" (click)=\"submitted=false\" (click)=\"goToPageMenu('/', 'analytic')\">\n\n  </div>\n\n  <div class=\"notification\">\n      <span id=\"notification\" class=\"notification\">Calendar  </span>\n      <input id=\"button3\" type=\"button\" alt=\"Submit\" width=\"48\" height=\"48\" (click)=\"submitted=false\" (click)=\"goToPageMenu('/', 'calendar')\">\n  \n    </div>\n  \n    <div class=\"setting\">\n        <span id=\"setting\" class=\"setting\">Settings  </span>\n        <input id=\"button4\" type=\"button\" alt=\"Submit\" width=\"48\" height=\"48\" (click)=\"submitted=false\" (click)=\"goToPageMenu('/', 'analytic')\">\n    \n      </div>\n \n   \n\n</div>"

/***/ }),

/***/ "./src/app/settings/settings.component.ts":
/*!************************************************!*\
  !*** ./src/app/settings/settings.component.ts ***!
  \************************************************/
/*! exports provided: SettingsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingsComponent", function() { return SettingsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./src/node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _settings_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./settings.service */ "./src/app/settings/settings.service.ts");






var SettingsComponent = /** @class */ (function () {
    function SettingsComponent(http, router, SettingsService) {
        this.http = http;
        this.router = router;
        this.SettingsService = SettingsService;
    }
    SettingsComponent.prototype.goToPageMenu = function (pagename, parameter) {
        this.router.navigate([pagename, parameter]);
    };
    SettingsComponent.prototype.Submit = function () {
        // alert("weight is :"+this.weight);
        //alert(this.username+this.password+this.age+this.weight);
        if (this.password == '') {
            jquery__WEBPACK_IMPORTED_MODULE_4__("#passworderror").text("please correct required");
            jquery__WEBPACK_IMPORTED_MODULE_4__("#passworderror").css("color", "red");
            return;
        }
        else if (Number(this.weight) != parseInt(this.weight, 10)) {
            //  $("#weighterror").show();
            jquery__WEBPACK_IMPORTED_MODULE_4__("#weighterror").text("please correct required");
            jquery__WEBPACK_IMPORTED_MODULE_4__("#weighterror").css("color", "red");
            return;
        }
        else if (this.weight == '') {
            jquery__WEBPACK_IMPORTED_MODULE_4__("#weighterror").text("please correct required");
            jquery__WEBPACK_IMPORTED_MODULE_4__("#weighterror").css("color", "red");
            return;
        }
        jquery__WEBPACK_IMPORTED_MODULE_4__("#in").hide();
        jquery__WEBPACK_IMPORTED_MODULE_4__("#edit").hide();
        jquery__WEBPACK_IMPORTED_MODULE_4__("#thanks").show();
        alert("username tooken is:" + this.age);
        this.SettingsService.updateUsers(this.username, this.password, this.age, this.weight).subscribe(function (res) {
            //   alert(this.username+this.password+this.age+this.weight);
            var flag = 0;
        }, function (error) {
            alert("error hier");
        });
    };
    SettingsComponent.prototype.ngOnInit = function () {
        // Check browser support
        if (typeof (Storage) !== "undefined") {
            // Store
            //sessionStorage.setItem("lastname", "Smith");
            // Retrieve
            document.getElementById("nameheader").innerHTML = sessionStorage.getItem("lastname");
        }
        else {
            document.getElementById("nameheader").innerHTML = "Sorry, your browser does not support Web Storage...";
        }
        jquery__WEBPACK_IMPORTED_MODULE_4__("#thanks").hide();
        document.getElementById("setting").style.backgroundColor = "#696969";
        document.getElementById("settingbutton").style.backgroundColor = "#696969";
        jquery__WEBPACK_IMPORTED_MODULE_4__("#session").hide();
        jquery__WEBPACK_IMPORTED_MODULE_4__("#back").show();
        jquery__WEBPACK_IMPORTED_MODULE_4__("#date").hide();
    };
    SettingsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-settings',
            template: __webpack_require__(/*! ./settings.component.html */ "./src/app/settings/settings.component.html"),
            styles: [__webpack_require__(/*! ./settings.component.css */ "./src/app/settings/settings.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _settings_service__WEBPACK_IMPORTED_MODULE_5__["SettingsService"]])
    ], SettingsComponent);
    return SettingsComponent;
}());



/***/ }),

/***/ "./src/app/settings/settings.service.ts":
/*!**********************************************!*\
  !*** ./src/app/settings/settings.service.ts ***!
  \**********************************************/
/*! exports provided: SettingsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingsService", function() { return SettingsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./src/node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");



var SettingsService = /** @class */ (function () {
    function SettingsService(httpClient) {
        this.httpClient = httpClient;
        this.apiURL = 'http://139.91.200.103:3000/api/things/setusers/';
        this.url = 'http://139.91.200.103:3000/api/things/getusers/';
    }
    SettingsService.prototype.updateUsers = function (username, password, age, weight) {
        var data = {
            "name": "fr"
        };
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
            })
        };
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
            .set('username', String(username));
        var options = {
            httpOptions: httpOptions,
            withCredentials: false
        };
        alert("age in is" + age);
        return this.httpClient.post("" + this.apiURL + username + '/' + password + '/' + age + '/' + weight, null, options);
    };
    SettingsService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], SettingsService);
    return SettingsService;
}());



/***/ }),

/***/ "./src/app/signin/signin.component.css":
/*!*********************************************!*\
  !*** ./src/app/signin/signin.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#signin{\r\n    background-color: \t#303030;\r\n\r\n\r\n}\r\n\r\n\r\n#myProgress {\r\n    width: 100%;\r\n    background-color: #ddd;\r\n  }\r\n\r\n\r\n#myBar {\r\n    width: 1%;\r\n    height: 30px;\r\n    background-color: #ff8000;\r\n  }\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zaWduaW4vc2lnbmluLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSwwQkFBMEI7OztBQUc5Qjs7O0FBR0E7SUFDSSxXQUFXO0lBQ1gsc0JBQXNCO0VBQ3hCOzs7QUFFQTtJQUNFLFNBQVM7SUFDVCxZQUFZO0lBQ1oseUJBQXlCO0VBQzNCIiwiZmlsZSI6ImFwcC9zaWduaW4vc2lnbmluLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjc2lnbmlue1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogXHQjMzAzMDMwO1xyXG5cclxuXHJcbn1cclxuXHJcblxyXG4jbXlQcm9ncmVzcyB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNkZGQ7XHJcbiAgfVxyXG4gIFxyXG4gICNteUJhciB7XHJcbiAgICB3aWR0aDogMSU7XHJcbiAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmY4MDAwO1xyXG4gIH0iXX0= */"

/***/ }),

/***/ "./src/app/signin/signin.component.html":
/*!**********************************************!*\
  !*** ./src/app/signin/signin.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<!---->\n<!--\n<div class=\"container\">\n  <table class=\"table table-striped\">\n    <thead>\n        <tr>\n          <th>Actor Name</th>\n          <th>Character Name</th>\n         \n        </tr>\n    </thead>\n    <tbody>\n      <tr *ngFor =\"let d of data;\">\n        <td>{{d.name}}</td>\n        <td>{{d.info}}</td>\n        \n      </tr>\n    </tbody>\n  </table>\n</div>\n-->\n<div class=\"limiter\">\n  <div class=\"container-login100\">\n    <div class=\"wrap-login100\">\n      <form class=\"login100-form validate-form\" id=\"loadbar\">\n        <span class=\"login100-form-title p-b-26\">\n          Welcome\n        </span>\n        <span class=\"login100-form-title p-b-48\">\n          <i class=\"zmdi zmdi-font\"></i>\n        </span>\n\n        <div class=\"wrap-input100 validate-input\" >\n          <input class=\"input100\" type=\"text\" name=\"username\" [(ngModel)]=\"username\" >\n          <span class=\"focus-input100\" data-placeholder=\"Username\"></span>\n        </div>\n\n        <div class=\"wrap-input100 validate-input\" data-validate=\"Enter password\" >\n          <span class=\"btn-show-pass\">\n            <i class=\"zmdi zmdi-eye\"></i>\n          </span>\n          <input class=\"input100\" type=\"password\" name=\"pass\" [(ngModel)]=\"password\" >\n          <span class=\"focus-input100\" data-placeholder=\"Password\"></span>\n        </div>\n\n        <div class=\"container-login100-form-btn\">\n          <div class=\"wrap-login100-form-btn\">\n            <div class=\"login100-form-bgbtn\"></div>\n         \n            \n            \n            <button id=\"signin\" class=\"login100-form-btn\"  (click)=\"submitted=false\" (click)=\"submit2()\"  >\n              Login\n            </button>\n            \n          </div>\n\n\n         \n          \n        </div>\n\n        \n       \n\n        <div class=\"text-center p-t-115\">\n          <span class=\"txt1\">\n            Don’t have an account?\n          </span>\n          <a   onclick=\"myFunction()\" id=\"all\" routerLink=\"/signup\">Sign Up</a>\n          \n        </div>\n      </form>\n\n     <div id=\"loading\"> Loading...</div>\n      <div id=\"myProgress\">\n          \n          <div id=\"myBar\"></div>\n      </div>\n    </div>\n  </div>\n</div>\n\n\n<div id=\"dropDownSelect1\"></div>\n"

/***/ }),

/***/ "./src/app/signin/signin.component.ts":
/*!********************************************!*\
  !*** ./src/app/signin/signin.component.ts ***!
  \********************************************/
/*! exports provided: SigninComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SigninComponent", function() { return SigninComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./src/node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _signin_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./signin.service */ "./src/app/signin/signin.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");






var SigninComponent = /** @class */ (function () {
    function SigninComponent(http, SigninService, router) {
        this.http = http;
        this.SigninService = SigninService;
        this.router = router;
    }
    ;
    SigninComponent.prototype.ngOnInit = function () {
        //$('#main').hide();
        jquery__WEBPACK_IMPORTED_MODULE_4__('#myProgress').hide();
        jquery__WEBPACK_IMPORTED_MODULE_4__('#loading').hide();
        //loading
        /*   this.http.get('http://127.0.0.1:3000/api/things/',{responseType:"json"}).subscribe(
             response => {
               alert("cabled");
                 this.data = response;
                 console.log("data :"+response);
                 var sample=JSON.stringify(response);
            });*/
        //  loadbar
        /* this.SigninService.getCustomers("aa").subscribe((res)=>{
          // console.log("called");
           alert("Heys guysaa");
           //alert(JSON.parse(res));
           alert(JSON.stringify(res));
           var i=JSON.stringify(res);
           var stringify = JSON.parse(i);
           alert("Hier vars"+stringify.name);
           alert(stringify['name']);
         //  for (var si = 0; si < stringify.length; si++) {
            // alert("Alles gutten");
            // alert(stringify[si]['name']);
          // }
         //  alert("cabled"+res[0].info);
            // console.log(res);
     
            /*response => {
             alert("cabled");
               this.data = response;
               console.log("data :"+response);
               var sample=JSON.stringify(response);
          }*/
        /*}, (error) =>
        {
            alert("error hier");
           //alert(error);
            // if the request fails, this callback will be invoked
        });
    
    ///////////////////////////////////////////////
    
    
    
    this.SigninService.getCustomersB("aa").subscribe((res)=>{
       alert("Heys guysaa customera");
       var i=JSON.stringify(res);
       var stringify = JSON.parse(i);
    
       //var data = res.json();
       alert(stringify[0].name);
      
       
     
     }, (error) =>
     {
         alert("error hier");
        //alert(error);
         // if the request fails, this callback will be invoked
     });
    
    
    
    
    */
        //////////////////////////////////////////////////////
    };
    SigninComponent.prototype.submit2 = function (formData) {
        var _this = this;
        jquery__WEBPACK_IMPORTED_MODULE_4__('#loadbar').hide();
        jquery__WEBPACK_IMPORTED_MODULE_4__('#myProgress').show();
        jquery__WEBPACK_IMPORTED_MODULE_4__('#loading').show();
        function delay(ms) {
            return new Promise(function (resolve) { return setTimeout(resolve, ms); });
        }
        // alert("Username is"+this.username);
        //alert("Passowrd  is"+this.password);
        //  alert(this.username);
        var width = 1;
        this.SigninService.getLogin(this.username).subscribe(function (res) {
            var i = JSON.stringify(res);
            var stringify = JSON.parse(i);
            // alert(res);
            //alert("Heys guysaa hier is nikolas"+this.username);
            // alert(stringify[0]);
            if ((stringify[0]) != undefined || (String(stringify[0]) != null || (String(stringify[0]) != "undefined"))) {
                // alert("in string"+String(stringify[0]));
                if (String(stringify[0]) == "undefined") {
                    // alert("zero hier");
                    _this.goToPage("/", "error");
                    return;
                }
                if (String(stringify[0].username) === _this.username && String(stringify[0].password) === _this.password) {
                    // alert("Kablooo");
                    //  alert(width);
                    /*  if (typeof(Storage) !== "undefined") {
                        // Store
                        sessionStorage.setItem("lastname", String(stringify[0].username));
                        // Retrieve
                         document.getElementById("session").innerHTML = sessionStorage.getItem("lastname");
                      } else {
                         document.getElementById("session").innerHTML = "Sorry, your browser does not support Web Storage...";
                      }
                
                        (async () => {
                          await delay(2000);
                       
                          test();
                      })();*/
                    (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
                        return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                            switch (_a.label) {
                                case 0: return [4 /*yield*/, delay(2000)];
                                case 1:
                                    _a.sent();
                                    if (typeof (Storage) !== "undefined") {
                                        // Store
                                        sessionStorage.setItem("lastname", String(this.username));
                                        // Retrieve
                                        // document.getElementById("session").innerHTML = sessionStorage.getItem("lastname");
                                    }
                                    else {
                                        //document.getElementById("session").innerHTML = "Sorry, your browser does not support Web Storage...";
                                    }
                                    test();
                                    return [2 /*return*/];
                            }
                        });
                    }); })();
                }
                else {
                    (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
                        return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                            switch (_a.label) {
                                case 0: return [4 /*yield*/, delay(2000)];
                                case 1:
                                    _a.sent();
                                    //alert("Not in hier nouli");
                                    this.goToPage("/", "error");
                                    return [2 /*return*/];
                            }
                        });
                    }); })();
                }
            }
        }, function (error) {
            alert("error hier" + error.message);
        });
        // }
        // alert("alles2");
        // this.goToPage('/', 'main');
        // this.goToPage('/', 'main');
        //alert("Called it hier1!");
        //alert(this.email);
        //  alert("Called submit");
        function test() {
            // alert("finalaa");
            //alert(location.host);
            //alert(location.hostname);
            // alert("concat");
            // alert(location.host.toString().concat("/main"));
            var i = location.host.toString().concat("/main").toString().trim();
            // alert(i);
            var all = "http://".concat(i);
            // alert("all are");
            //alert(all);
            window.location.href = all; // i;//'http://www.google.com';
        }
        var elem = document.getElementById("myBar");
        var id = setInterval(frame, 10);
        function frame() {
            //alert("frame");
            if (width >= 100) {
                //  alert("called");
                clearInterval(id);
                // alert("alles");
            }
            else {
                width++;
                elem.style.width = width + '%';
            }
        }
        this.SigninService.getCustomers(this.email).subscribe(function (res) {
            // alert("took");
            var flag = 0;
            // alert("continue");
            //alert(JSON.stringify(res));
            //console.log("called");
            //alert("Clled");
            //alert("cabled"+res[0].info);
            //  console.log(res);  
        }, function (error) {
            //alert("error hier");
            //  alert(error);
            // if the request fails, this callback will be invoked
        });
    };
    SigninComponent.prototype.goToPage = function (pagename, parameter) {
        this.router.navigate([pagename, parameter]);
    };
    SigninComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-signin',
            template: __webpack_require__(/*! ./signin.component.html */ "./src/app/signin/signin.component.html"),
            styles: [__webpack_require__(/*! ./signin.component.css */ "./src/app/signin/signin.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"], _signin_service__WEBPACK_IMPORTED_MODULE_2__["SigninService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], SigninComponent);
    return SigninComponent;
}());



/***/ }),

/***/ "./src/app/signin/signin.service.ts":
/*!******************************************!*\
  !*** ./src/app/signin/signin.service.ts ***!
  \******************************************/
/*! exports provided: SigninService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SigninService", function() { return SigninService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./src/node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");



var SigninService = /** @class */ (function () {
    function SigninService(httpClient) {
        this.httpClient = httpClient;
        this.apiURL = 'http://139.91.200.103:3000/api/things/';
        this.url = 'http://139.91.200.103:3000/api/things/getuser/';
    }
    SigninService.prototype.getCustomers = function (input) {
        var data = {
            "name": "fr"
        };
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
            })
        };
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
            .set('name', String(input))
            .set('info', String(input));
        var options = {
            httpOptions: httpOptions,
            params: params,
            withCredentials: false
        };
        return this.httpClient.post("" + this.apiURL, null, options);
    };
    SigninService.prototype.getCustomersB = function (input) {
        var data = {
            "name": "fr"
        };
        return this.httpClient.get('http://139.91.200.103:3000/api/things/', { responseType: "json" });
    };
    SigninService.prototype.getLogin = function (input) {
        var data = {
            "name": "fr"
        };
        return this.httpClient.get("" + this.url + input, { responseType: "json" });
    };
    SigninService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], SigninService);
    return SigninService;
}());



/***/ }),

/***/ "./src/app/signup/signup.component.css":
/*!*********************************************!*\
  !*** ./src/app/signup/signup.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#signup{\r\n    background-color: \t#303030;\r\n\r\n\r\n}\r\n\r\n#myProgress {\r\n    width: 100%;\r\n    background-color: #ddd;\r\n  }\r\n\r\n#myBar {\r\n    width: 1%;\r\n    height: 30px;\r\n    background-color: #ff8000;\r\n  }\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zaWdudXAvc2lnbnVwLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSwwQkFBMEI7OztBQUc5Qjs7QUFFQTtJQUNJLFdBQVc7SUFDWCxzQkFBc0I7RUFDeEI7O0FBRUY7SUFDSSxTQUFTO0lBQ1QsWUFBWTtJQUNaLHlCQUF5QjtFQUMzQiIsImZpbGUiOiJhcHAvc2lnbnVwL3NpZ251cC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiI3NpZ251cHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IFx0IzMwMzAzMDtcclxuXHJcblxyXG59XHJcblxyXG4jbXlQcm9ncmVzcyB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNkZGQ7XHJcbiAgfVxyXG4gIFxyXG4jbXlCYXIge1xyXG4gICAgd2lkdGg6IDElO1xyXG4gICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmODAwMDtcclxuICB9Il19 */"

/***/ }),

/***/ "./src/app/signup/signup.component.html":
/*!**********************************************!*\
  !*** ./src/app/signup/signup.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\n\n<div class=\"limiter\" id=\"fanis\">\n  <div class=\"container-login100\">\n    <div class=\"wrap-login100\">\n      \n      <form class=\"login100-form validate-form\"  id=\"loadbar\">\n\n\n        \n        <span class=\"login100-form-title p-b-26\">\n          Welcome\n        </span>\n        <span class=\"login100-form-title p-b-48\">\n          <i class=\"zmdi zmdi-font\"></i>\n        </span>\n\n        <div class=\"wrap-input100 validate-input\" >\n            <input id=\"username\" class=\"input100\" type=\"text\"  name=\"username\" [(ngModel)]=\"username\" >\n            <span class=\"focus-input100\" data-placeholder=\"Username\"></span>\n            <div align=\"right\" id=\"usernameerror\"></div>\n        </div>\n  \n\n        <div  class=\"wrap-input100 validate-input\" data-validate = \"Valid email is: a@b.c\">\n          <input id=\"emailup\" class=\"input100\" type=\"text\" name=\"emailup\"  [(ngModel)]=\"email\"  >\n          <span  class=\"focus-input100\" data-placeholder=\"Email\"></span>\n          <div align=\"right\" id=\"emailerror\"></div>\n        </div>\n        \n        <div class=\"wrap-input100 validate-input\" data-validate=\"Enter password\">\n          <span class=\"btn-show-pass\">\n            <i class=\"zmdi zmdi-eye\"></i>\n          </span>\n          <input class=\"input100\" type=\"password\" name=\"password\"  [(ngModel)]=\"password\">\n          <span class=\"focus-input100\" data-placeholder=\"Password\"></span>\n          <div align=\"right\" id=\"passworderror\"></div>\n        </div>\n\n        \n\n        <div class=\"wrap-input100 validate-input\" >\n            <input class=\"input100\" type=\"text\" name=\"age\" [(ngModel)]=\"age\" >\n            <span class=\"focus-input100\" data-placeholder=\"weight\" data-placeholder=\"Age\"></span>\n            <div align=\"right\" id=\"ageerror\"></div>\n        </div>\n\n         <div class=\"wrap-input100 validate-input\" >\n          <input class=\"input100\" type=\"text\"  name=\"height\" [(ngModel)]=\"height\" >\n          <span class=\"focus-input100\" data-placeholder=\"height\" data-placeholder=\"Height(cm)\"></span>\n          <div align=\"right\" id=\"heighterror\"></div>\n         </div>\n\n        <div class=\"wrap-input100 validate-input\" >\n            <input class=\"input100\" type=\"text\" name=\"weight\" name=\"weight\" [(ngModel)]=\"weight\" >\n            <span class=\"focus-input100\" data-placeholder=\"weight\" data-placeholder=\"Weight(kg)\"></span>\n            <div align=\"right\" id=\"weighterror\"></div>\n        </div>\n\n        \n      \n        \n         \n        <input type=\"radio\" name=\"gender\" value=\"male\" [(ngModel)]=\"male\" checked > Male<br>\n        <input type=\"radio\" name=\"gender\" value=\"female\" [(ngModel)]=\"female\"> Female<br>\n\n      \n        \n        <div class=\"container-login100-form-btn\">\n          <div class=\"wrap-login100-form-btn\">\n            <div class=\"login100-form-bgbtn\"></div>\n            <button id=\"signup\" class=\"login100-form-btn\" (click)=\"submitted=false\" (click)=\"submit()\">\n              Register\n            </button>\n          </div>\n        </div>\n\n\n\n        <div class=\"text-center p-t-115\">\n          <span class=\"txt1\">\n            Don’t have an account?\n          </span>\n<a    routerLink=\"/signin\">Sign In</a>\n         \n        </div>\n      </form>\n\n      <div id=\"loading\"> Loading...</div>\n       <div id=\"myProgress\">\n          \n          <div id=\"myBar\"></div>\n       </div>\n\n\n    </div>\n  </div>\n</div>\n\n\n\n\n<div id=\"dropDownSelect1\"></div>\n\n"

/***/ }),

/***/ "./src/app/signup/signup.component.ts":
/*!********************************************!*\
  !*** ./src/app/signup/signup.component.ts ***!
  \********************************************/
/*! exports provided: SignupComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupComponent", function() { return SignupComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./src/node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _signup__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./signup */ "./src/app/signup/signup.ts");
/* harmony import */ var _signup_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./signup.service */ "./src/app/signup/signup.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");







var SignupComponent = /** @class */ (function () {
    function SignupComponent(http, SignupService, router) {
        this.http = http;
        this.SignupService = SignupService;
        this.router = router;
    }
    SignupComponent.prototype.validateEmail = function (email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    };
    SignupComponent.prototype.ngOnInit = function () {
        jquery__WEBPACK_IMPORTED_MODULE_2__('#main').hide();
        // $('#main').hide();
        jquery__WEBPACK_IMPORTED_MODULE_2__('#myProgress').hide();
        jquery__WEBPACK_IMPORTED_MODULE_2__('#loading').hide();
        // usernameerror
        /*  $('#username').on('blur', function() {
            //alert( "Handler for .blur() called." );
          // alert(this.username)
            $("#usernameerror").text("username required");
            $("#usernameerror").css("color","red");
        });
           
            // alert("Emial validator is:"+this.validateEmail("fanis@gmail.com"));
            $('#emailup').on('blur', function() {
              //alert( "Handler for .blur() called." );
              $("#emailerror").text("Ops should inclu");
              $("#emailerror").css("color","red");
          });*/
        // $("#emailerror").text("Ops should inclu");
        // $("#emailerror").css("color","red");
    };
    SignupComponent.prototype.submit = function (formData) {
        var _this = this;
        //alert("val is"+this.validateEmail(this.email));
        if (this.username == '') {
            jquery__WEBPACK_IMPORTED_MODULE_2__("#usernameerror").text("username required");
            jquery__WEBPACK_IMPORTED_MODULE_2__("#usernameerror").css("color", "red");
            return;
        }
        if (this.validateEmail(this.email) == false) {
            //  alert("vali hier");
            jquery__WEBPACK_IMPORTED_MODULE_2__("#emailerror").text("email required");
            jquery__WEBPACK_IMPORTED_MODULE_2__("#emailerror").css("color", "red");
            return;
        }
        else if (this.validateEmail(this.email) == true) {
            jquery__WEBPACK_IMPORTED_MODULE_2__("#emailerror").hide();
        }
        if (this.password == '') {
            jquery__WEBPACK_IMPORTED_MODULE_2__("#passworderror").text("password required");
            jquery__WEBPACK_IMPORTED_MODULE_2__("#passworderror").css("color", "red");
            return;
        }
        else {
            jquery__WEBPACK_IMPORTED_MODULE_2__("#passworderror").hide();
        }
        if (Number(this.age) === parseInt(this.age, 10)) {
            jquery__WEBPACK_IMPORTED_MODULE_2__("#ageerror").hide();
            // return;
        }
        else {
            // ageerror
            jquery__WEBPACK_IMPORTED_MODULE_2__("#ageerror").text("please correct required");
            jquery__WEBPACK_IMPORTED_MODULE_2__("#ageerror").css("color", "red");
            //  alert("data is not an integer")
            return;
        }
        if (Number(this.age) < 0 || Number(this.age) > 100) {
            jquery__WEBPACK_IMPORTED_MODULE_2__("#ageerror").text("please correct required");
            jquery__WEBPACK_IMPORTED_MODULE_2__("#ageerror").css("color", "red");
            return;
        }
        if (Number(this.height) < 150 || Number(this.height) > 220) {
            jquery__WEBPACK_IMPORTED_MODULE_2__("#heighterror").text("please in cm required");
            jquery__WEBPACK_IMPORTED_MODULE_2__("#heighterror").css("color", "red");
            return;
        }
        else if (Number(this.height) > 150 || Number(this.height) < 220) {
            jquery__WEBPACK_IMPORTED_MODULE_2__("#heighterror").hide();
        }
        if (Number(this.weight) === parseInt(this.weight, 10)) {
            jquery__WEBPACK_IMPORTED_MODULE_2__("#weighterror").hide();
            // return;
        }
        else {
            // ageerror
            jquery__WEBPACK_IMPORTED_MODULE_2__("#weighterror").text("please correct required");
            jquery__WEBPACK_IMPORTED_MODULE_2__("#weighterror").css("color", "red");
            //  alert("data is not an integer")
            return;
        }
        jquery__WEBPACK_IMPORTED_MODULE_2__('#loadbar').hide();
        jquery__WEBPACK_IMPORTED_MODULE_2__('#myProgress').show();
        jquery__WEBPACK_IMPORTED_MODULE_2__('#loading').show();
        function delay(ms) {
            return new Promise(function (resolve) { return setTimeout(resolve, ms); });
        }
        var width = 1;
        var elem = document.getElementById("myBar");
        var id = setInterval(frame, 10);
        function frame() {
            if (width >= 100) {
                clearInterval(id);
            }
            else {
                width++;
                elem.style.width = width + '%';
            }
        }
        var gender = jquery__WEBPACK_IMPORTED_MODULE_2__("input[name='gender']:checked").val();
        new _signup__WEBPACK_IMPORTED_MODULE_3__["Signup"]().setSignup(this.username, this.email, gender, this.password, this.weight, this.height);
        this.SignupService.setSignup(this.username, this.password, this.email, gender, this.height, this.weight, this.age).subscribe(function (res) {
            alert("Heys guysaa");
            alert(JSON.stringify(res));
            var i = JSON.stringify(res);
            var stringify = JSON.parse(i);
            //alert("Hier vars"+stringify.name);
            // alert(stringify['name']);
            (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
                return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, delay(2000)];
                        case 1:
                            _a.sent();
                            if (typeof (Storage) !== "undefined") {
                                // Store
                                sessionStorage.setItem("lastname", String(this.username));
                                // Retrieve
                                document.getElementById("session").innerHTML = sessionStorage.getItem("lastname");
                            }
                            else {
                                document.getElementById("session").innerHTML = "Sorry, your browser does not support Web Storage...";
                            }
                            test();
                            return [2 /*return*/];
                    }
                });
            }); })();
        }, function (error) {
            alert("error hier" + error.message);
        });
        function test() {
            var i = location.host.toString().concat("/calendar").toString().trim();
            var all = "http://".concat(i);
            window.location.href = all;
        }
    };
    SignupComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-signup',
            template: __webpack_require__(/*! ./signup.component.html */ "./src/app/signup/signup.component.html"),
            styles: [__webpack_require__(/*! ./signup.component.css */ "./src/app/signup/signup.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClient"], _signup_service__WEBPACK_IMPORTED_MODULE_4__["SignupService"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]])
    ], SignupComponent);
    return SignupComponent;
}());



/***/ }),

/***/ "./src/app/signup/signup.service.ts":
/*!******************************************!*\
  !*** ./src/app/signup/signup.service.ts ***!
  \******************************************/
/*! exports provided: SignupService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupService", function() { return SignupService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./src/node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");



var SignupService = /** @class */ (function () {
    function SignupService(httpClient) {
        this.httpClient = httpClient;
        this.apiURL = 'http://139.91.200.103:3000/api/things/signup';
    }
    SignupService.prototype.setSignup = function (username, password, email, gender, height, weight, age) {
        var data = {
            "name": "fr"
        };
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
            })
        };
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
            .set('username', String(username))
            .set('password', String(password))
            .set('email', String(email))
            .set('gender', String(gender))
            .set('age', String(age))
            .set('height', String(height))
            .set('weight', String(weight));
        var options = {
            httpOptions: httpOptions,
            params: params,
            withCredentials: false
        };
        return this.httpClient.post("" + this.apiURL, null, options);
    };
    SignupService.prototype.setLastHistory = function (username, password, email, gender, height, weight, age) {
        var data = {
            "name": "fr"
        };
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
            })
        };
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
            .set('username', String(username))
            .set('password', String(password))
            .set('email', String(email))
            .set('gender', String(gender))
            .set('age', String(age))
            .set('height', String(height))
            .set('weight', String(weight));
        var options = {
            httpOptions: httpOptions,
            params: params,
            withCredentials: false
        };
        return this.httpClient.post("" + this.apiURL, null, options);
    };
    SignupService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], SignupService);
    return SignupService;
}());



/***/ }),

/***/ "./src/app/signup/signup.ts":
/*!**********************************!*\
  !*** ./src/app/signup/signup.ts ***!
  \**********************************/
/*! exports provided: Signup */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Signup", function() { return Signup; });
var Signup = /** @class */ (function () {
    function Signup() {
    }
    Signup.prototype.setSignup = function (username, email, gender, password, weight, height) {
        this.username = username;
        this.email = email;
        this.gender = gender;
        this.password = password;
        this.weight = weight;
        this.height = height;
        //alert("In class");
        // alert(this.username);
        // alert(this.email);
        // alert(this.gender);
        // alert(this.password);
    };
    return Signup;
}());



/***/ }),

/***/ "./src/app/sockets/chat.service.ts":
/*!*****************************************!*\
  !*** ./src/app/sockets/chat.service.ts ***!
  \*****************************************/
/*! exports provided: ChatService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatService", function() { return ChatService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./src/node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _websocket_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./websocket.service */ "./src/app/sockets/websocket.service.ts");



var ChatService = /** @class */ (function () {
    // Our constructor calls our wsService connect method
    function ChatService(wsService) {
        this.wsService = wsService;
        this.messages = wsService
            .connect()
            .map(function (response) {
            return response;
        });
    }
    // Our simplified interface for sending
    // messages back to our socket.io server
    ChatService.prototype.sendMsg = function (msg) {
        this.messages.next(msg);
    };
    ChatService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_websocket_service__WEBPACK_IMPORTED_MODULE_2__["WebsocketService"]])
    ], ChatService);
    return ChatService;
}());



/***/ }),

/***/ "./src/app/sockets/sockets.component.css":
/*!***********************************************!*\
  !*** ./src/app/sockets/sockets.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAvc29ja2V0cy9zb2NrZXRzLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/sockets/sockets.component.html":
/*!************************************************!*\
  !*** ./src/app/sockets/sockets.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"limiter\">\n  <div class=\"container-login100\">\n      <div class=\"wrap-login100\">\n          <form class=\"login100-form validate-form\" id=\"loadbar\">\n              <span class=\"login100-form-title p-b-48\">\n        <i class=\"zmdi zmdi-font\"></i>\n      </span>\n              <div class=\"container-login100-form-btn\">\n                  <div class=\"wrap-login100-form-btn\">\n                      <div class=\"login100-form-bgbtn\"></div>\n                      <button id=\"signin\" class=\"login100-form-btn\" (click)=\"submitted=false\" (click)=\"sendMessage()\">\n                          tick socket\n                      </button>\n                  </div>\n              </div>\n          </form>\n      </div>\n  </div>\n</div>\n<div id=\"dropDownSelect1\"></div>"

/***/ }),

/***/ "./src/app/sockets/sockets.component.ts":
/*!**********************************************!*\
  !*** ./src/app/sockets/sockets.component.ts ***!
  \**********************************************/
/*! exports provided: SocketsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SocketsComponent", function() { return SocketsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./src/node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _chat_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./chat.service */ "./src/app/sockets/chat.service.ts");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_3__);




var SocketsComponent = /** @class */ (function () {
    function SocketsComponent(chat) {
        this.chat = chat;
    }
    SocketsComponent.prototype.ngOnInit = function () {
        jquery__WEBPACK_IMPORTED_MODULE_3__("#main").hide();
        this.chat.messages.subscribe(function (msg) {
            console.log("Messages are:");
            console.log(msg.text);
        });
    };
    SocketsComponent.prototype.sendMessage = function () {
        //alert("clicked send");
        this.chat.sendMsg("Test Message");
    };
    SocketsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-sockets',
            template: __webpack_require__(/*! ./sockets.component.html */ "./src/app/sockets/sockets.component.html"),
            styles: [__webpack_require__(/*! ./sockets.component.css */ "./src/app/sockets/sockets.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_chat_service__WEBPACK_IMPORTED_MODULE_2__["ChatService"]])
    ], SocketsComponent);
    return SocketsComponent;
}());



/***/ }),

/***/ "./src/app/sockets/websocket.service.ts":
/*!**********************************************!*\
  !*** ./src/app/sockets/websocket.service.ts ***!
  \**********************************************/
/*! exports provided: WebsocketService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WebsocketService", function() { return WebsocketService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./src/node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var socket_io_client__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! socket.io-client */ "./node_modules/socket.io-client/lib/index.js");
/* harmony import */ var socket_io_client__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(socket_io_client__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var rxjs_Observable__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/Observable */ "./node_modules/rxjs-compat/_esm5/Observable.js");
/* harmony import */ var rxjs_Rx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/Rx */ "./node_modules/rxjs-compat/_esm5/Rx.js");





var WebsocketService = /** @class */ (function () {
    function WebsocketService() {
    }
    WebsocketService.prototype.connect = function () {
        var _this = this;
        // If you aren't familiar with environment variables then
        // you can hard code `environment.ws_url` as `http://localhost:5000`
        this.socket = socket_io_client__WEBPACK_IMPORTED_MODULE_2__('http://localhost:5000/');
        // We define our observable which will observe any incoming messages
        // from our socket.io server.
        var observable = new rxjs_Observable__WEBPACK_IMPORTED_MODULE_3__["Observable"](function (observer) {
            _this.socket.on('message', function (data) {
                console.log("Received message from Websocket Server");
                observer.next(data);
            });
            return function () {
                _this.socket.disconnect();
            };
        });
        // We define our Observer which will listen to messages
        // from our other components and send messages back to our
        // socket server whenever the `next()` method is called.
        var observer = {
            next: function (data) {
                _this.socket.emit('message', JSON.stringify(data));
            },
        };
        // we return our Rx.Subject which is a combination
        // of both an observer and observable.
        return rxjs_Rx__WEBPACK_IMPORTED_MODULE_4__["Subject"].create(observer, observable);
    };
    WebsocketService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], WebsocketService);
    return WebsocketService;
}());



/***/ }),

/***/ "./src/app/start/start.component.css":
/*!*******************************************!*\
  !*** ./src/app/start/start.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAvc3RhcnQvc3RhcnQuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/start/start.component.html":
/*!********************************************!*\
  !*** ./src/app/start/start.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  start works!\n</p>\n"

/***/ }),

/***/ "./src/app/start/start.component.ts":
/*!******************************************!*\
  !*** ./src/app/start/start.component.ts ***!
  \******************************************/
/*! exports provided: StartComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StartComponent", function() { return StartComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./src/node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var StartComponent = /** @class */ (function () {
    function StartComponent() {
    }
    StartComponent.prototype.ngOnInit = function () {
    };
    StartComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-start',
            template: __webpack_require__(/*! ./start.component.html */ "./src/app/start/start.component.html"),
            styles: [__webpack_require__(/*! ./start.component.css */ "./src/app/start/start.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], StartComponent);
    return StartComponent;
}());



/***/ }),

/***/ "./src/app/submit-form/submit-form.component.css":
/*!*******************************************************!*\
  !*** ./src/app/submit-form/submit-form.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\npre {margin: 45px 0 60px;}\r\nh1 {margin: 60px 0 60px 0;}\r\np {margin-bottom: 10px;}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zdWJtaXQtZm9ybS9zdWJtaXQtZm9ybS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFDQSxLQUFLLG1CQUFtQixDQUFDO0FBQ3pCLElBQUkscUJBQXFCLENBQUM7QUFDMUIsR0FBRyxtQkFBbUIsQ0FBQyIsImZpbGUiOiJhcHAvc3VibWl0LWZvcm0vc3VibWl0LWZvcm0uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG5wcmUge21hcmdpbjogNDVweCAwIDYwcHg7fVxyXG5oMSB7bWFyZ2luOiA2MHB4IDAgNjBweCAwO31cclxucCB7bWFyZ2luLWJvdHRvbTogMTBweDt9XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/submit-form/submit-form.component.html":
/*!********************************************************!*\
  !*** ./src/app/submit-form/submit-form.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<link  href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css\"  rel=\"stylesheet\">\n<div class=\"container\">\n    <div class=\"site-index\">\n        <div class=\"body-content\">\n\n            <div class=\"row\">\n                <div class=\"col-md-12\">\n                    <h1>Default setting of wizard</h1>\n                    <form id=\"wizard_example\" action=\"\">\n                        <fieldset>\n                            <legend>Basic information</legend>\n                            <div class=\"row\">\n                                <div class=\"col-lg-6\">\n                                    <div class=\"form-group\">\n                                        <label for=\"exampleInputEmail1\">Email address</label>\n                                        <input type=\"email\" class=\"form-control\" id=\"exampleInputEmail1\"\n                                               name=\"exampleInputEmail1\" placeholder=\"Enter email\">\n                                    </div>\n                                    <div class=\"form-group\">\n                                        <label for=\"exampleInputPassword1\">Password</label>\n                                        <input type=\"password\" class=\"form-control\" id=\"exampleInputPassword1\"\n                                               name=\"exampleInputPassword1\" placeholder=\"Password\">\n                                    </div>\n                                    <div class=\"form-group\">\n                                        <label for=\"exampleInputPassword1\">Password again</label>\n                                        <input type=\"password\" class=\"form-control\" id=\"exampleInputPasswordAgain1\"\n                                               name=\"exampleInputPasswordAgain1\" placeholder=\"Password\">\n                                    </div>\n                                </div>\n                                <div class=\"col-lg-6\">\n                                    <div class=\"form-group\">\n                                        <label for=\"exampleInputPassword1\">Favorite number</label>\n                                        <select class=\"form-control\" name=\"favoriteNumber\">\n                                            <option value=\"1\">1</option>\n                                            <option value=\"2\">2</option>\n                                            <option value=\"3\">3</option>\n                                            <option value=\"4\">4</option>\n                                            <option value=\"5\">5</option>\n                                            <option value=\"6\">6</option>\n                                            <option value=\"7\">7</option>\n                                            <option value=\"8\">8</option>\n                                            <option value=\"9\">9</option>\n                                            <option value=\"10\">10</option>\n                                        </select>\n                                    </div>\n                                    <div class=\"form-group\">\n                                        <label>Own animals</label>\n\n                                        <div class=\"checkbox\">\n                                            <label>\n                                                <input type=\"checkbox\" name=\"animal[]\" value=\"goat\" data-sf-text=\"Koza\"> Goat\n                                            </label>\n                                        </div>\n                                        <div class=\"checkbox\">\n                                            <label>\n                                                <input type=\"checkbox\" name=\"animal[]\" value=\"cow\"> Cow\n                                            </label>\n                                        </div>\n                                        <div class=\"checkbox\">\n                                            <label>\n                                                <input type=\"checkbox\" name=\"animal[]\" value=\"rooster\" data-sf-text=\"Kohout\"> Rooster\n                                            </label>\n                                        </div>\n                                        <div class=\"checkbox\">\n                                            <label>\n                                                <input type=\"checkbox\" name=\"animal[]\" value=\"crocodile\"> Crocodile\n                                            </label>\n                                        </div>\n                                    </div>\n                                </div>\n                                <div class=\"col-lg-12\">\n                                    In publishing and graphic design, lorem ipsum is common placeholder text used to\n                                    demonstrate the graphic elements of a document or visual presentation, such as web\n                                    pages, typography, and graphical layout. It is a form of \"greeking\".\n                                </div>\n                            </div>\n                        </fieldset>\n                        <fieldset>\n                            <legend>Condition</legend>\n                            In publishing and graphic design, lorem ipsum is common placeholder text used to demonstrate\n                            the graphic elements of a document or visual presentation, such as web pages, typography,\n                            and graphical layout. It is a form of \"greeking\".\n                            Even though using \"lorem ipsum\" often arouses curiosity due to its resemblance to classical\n                            Latin, it is not intended to have meaning. Where text is visible in a document, people tend\n                            to focus on the textual content rather than upon overall presentation, so publishers use\n                            lorem ipsum when displaying a typeface or design in order to direct the focus to\n                            presentation. \"Lorem ipsum\" also approximates a typical distribution of letters in English.\n                            <div class=\"radio\">\n                                <label>\n                                    <input type=\"radio\" name=\"optionsRadios\" value=\"option1\" checked>\n                                    Yes, it is totaly right.\n                                </label>\n                            </div>\n                            <div class=\"radio\">\n                                <label>\n                                    <input type=\"radio\" name=\"optionsRadios\" value=\"option2\">\n                                    No, I check it twice and it is not right.\n                                </label>\n                            </div>\n                        </fieldset>\n                        <fieldset>\n                            <legend>Final step</legend>\n                            <div class=\"row\">\n                                <div class=\"col-lg-12\">\n                                    <p>\n                                        Even though using \"lorem ipsum\" often arouses curiosity due to its resemblance\n                                        to classical Latin, it is not intended to have meaning. Where text is visible in\n                                        a document, people tend to focus on the textual content rather than upon overall\n                                        presentation, so publishers use lorem ipsum when displaying a typeface or design\n                                        in order to direct the focus to presentation. \"Lorem ipsum\" also approximates a\n                                        typical distribution of letters in English.\n                                    </p>\n                                </div>\n                                <div class=\"col-lg-12\">\n                                    <div class=\"row\">\n                                        <div class=\"col-lg-6\">\n                                            <div class=\"form-group\">\n                                                <label class=\"sr-only\" for=\"exampleInputName1\">Your name</label>\n                                                <input type=\"text\" class=\"form-control\" id=\"exampleInputName1\"\n                                                       name=\"exampleInputName1\" placeholder=\"Your name\">\n                                            </div>\n                                            <div class=\"form-group\">\n                                                <label class=\"sr-only\" for=\"exampleInputCat1\">Name of your cat</label>\n                                                <input type=\"text\" class=\"form-control\" id=\"exampleInputCat1\"\n                                                       name=\"exampleInputCat1\" placeholder=\"Name of your cat\">\n                                            </div>\n                                        </div>\n                                        <div class=\"col-lg-6\">\n                                            <div class=\"form-group\">\n                                                <label class=\"sr-only\" for=\"exampleInputHam1\"></label>\n                                                <input type=\"text\" class=\"form-control\" id=\"exampleInputHam1\"\n                                                       name=\"exampleInputHam1\" placeholder=\"Name of your hamster\">\n                                            </div>\n                                        </div>\n                                        <div class=\"col-lg-6\">\n                                            <div class=\"form-group\">\n                                                <label class=\"sr-only\" for=\"exampleInputGrocer1\"></label>\n                                                <input type=\"text\" class=\"form-control\" id=\"exampleInputGrocer1\"\n                                                       name=\"exampleInputGrocer1\"\n                                                       placeholder=\"Name of your grocery seller\">\n                                            </div>\n                                        </div>\n                                    </div>\n                                </div>\n                                <div class=\"col-lg-12\">\n                                    <div class=\"row\">\n                                        <div class=\"col-lg-6\">\n                                            <div class=\"radio\">\n                                                <label>\n                                                    <input type=\"radio\" name=\"optionsRadios2\" value=\"option3\" checked>\n                                                    Option one is this and that&mdash;be sure to include why it's great\n                                                </label>\n                                            </div>\n                                            <div class=\"radio\">\n                                                <label>\n                                                    <input type=\"radio\" name=\"optionsRadios2\" value=\"option4\">\n                                                    Option two can be something else and selecting it will deselect\n                                                    option one\n                                                </label>\n                                            </div>\n                                        </div>\n                                        <div class=\"col-lg-6\">\n                                            <div class=\"checkbox\">\n                                                <label>\n                                                    <input type=\"checkbox\" name=\"superPower\"> I want have super-power\n                                                </label>\n                                            </div>\n                                            <div class=\"checkbox\">\n                                                <label>\n                                                    <input type=\"checkbox\" name=\"moreSuperPower\"> I have one or more\n                                                    super-power already\n                                                </label>\n                                            </div>\n                                        </div>\n                                    </div>\n                                </div>\n                                <div class=\"col-lg-12\">\n                                    <p>\n                                        Even though using \"lorem ipsum\" often arouses curiosity due to its resemblance\n                                        to classical Latin, it is not intended to have meaning. Where text is visible in\n                                        a document, people tend to focus on the textual content rather than upon overall\n                                        presentation, so publishers use lorem ipsum when displaying a typeface or design\n                                        in order to direct the focus to presentation. \"Lorem ipsum\" also approximates a\n                                        typical distribution of letters in English.\n                                    </p>\n\n                                    <p>\n                                        Using \"lorem ipsum\" often arouses curiosity due to its resemblance to classical\n                                        Latin, it is not intended to have meaning. Where text is visible in a document,\n                                        people tend to focus on the textual content rather than upon overall\n                                        presentation, so publishers use lorem ipsum when displaying a typeface or design\n                                        in order to direct the focus to presentation. \"Lorem ipsum\" also approximates a\n                                        typical distribution of letters in English.\n                                    </p>\n                                </div>\n                                <noscript>\n                                    <input class=\"nocsript-finish-btn sf-right nocsript-sf-btn\" type=\"submit\"\n                                           name=\"no-js-clicked\" value=\"finish\"/>\n                                </noscript>\n                            </div>\n                        </fieldset>\n                    </form>\n                </div>\n            </div>\n           \n           \n\n            <footer class=\"footer\">\n                <div class=\"container\">\n                    <p class=\"pull-left\">© ajoke.cz/wizard 2015</p>\n                </div>\n            </footer>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/submit-form/submit-form.component.ts":
/*!******************************************************!*\
  !*** ./src/app/submit-form/submit-form.component.ts ***!
  \******************************************************/
/*! exports provided: SubmitFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SubmitFormComponent", function() { return SubmitFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./src/node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var SubmitFormComponent = /** @class */ (function () {
    function SubmitFormComponent() {
    }
    SubmitFormComponent.prototype.ngOnInit = function () {
    };
    SubmitFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-submit-form',
            template: __webpack_require__(/*! ./submit-form.component.html */ "./src/app/submit-form/submit-form.component.html"),
            styles: [__webpack_require__(/*! ./submit-form.component.css */ "./src/app/submit-form/submit-form.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], SubmitFormComponent);
    return SubmitFormComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! G:\GymPlanManager\src\front-end\src\main.ts */"./src/main.ts");


/***/ }),

/***/ 1:
/*!********************!*\
  !*** ws (ignored) ***!
  \********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map