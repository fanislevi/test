(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/admin/admin.component.css":
/*!*******************************************!*\
  !*** ./src/app/admin/admin.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL2FkbWluLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/admin/admin.component.html":
/*!********************************************!*\
  !*** ./src/app/admin/admin.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<link  href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css\"  rel=\"stylesheet\">\n\n\n<div class=\"container-fluid\">\n   \n  \n  <div class=\"row\">\n     \n    \n        \n      <div class=\"col-sm-12\" align=\"center\" style=\"background-color:blue;\"  >   <input type=\"radio\" name=\"Emily\" value=\"Emily\" [(ngModel)]=\"Emily\" checked >Emily Breakfast<br> </div>\n      \n      <div class=\"col-sm-12\" align=\"center\" style=\"background-color:blue;\"  >   <input type=\"text\" name=\"usr_time\" [(ngModel)]=\"usr_time\"><br> </div> \n   \n      <div class=\"col-sm-6\" style=\"background-color:blue;\"><input type=\"radio\" name=\"sitting\" value=\"sitting\" [(ngModel)]=\"sitting\" checked >WakeUp<br></div>\n      <div class=\"col-sm-6\" style=\"background-color:blue;\"><input type=\"radio\" name=\"sitting\" value=\"notsitting\" [(ngModel)]=\"notsitting\">WakeUp<br></div>\n\n      <div class=\"col-sm-6\" style=\"background-color:blue;\"><input type=\"radio\" name=\"house\" value=\"inhouse\" [(ngModel)]=\"inhouse\" checked > Inhousesensor<br></div>\n      <div class=\"col-sm-6\" style=\"background-color:blue;\"><input type=\"radio\" name=\"house\" value=\"outhouse\" [(ngModel)]=\"outhouse\"> Outhousesensor<br></div>\n\n      <div class=\"col-sm-6\" style=\"background-color:blue;\"><input type=\"radio\" name=\"heart\" value=\"beats\" [(ngModel)]=\"beats\" checked > Heart beats<br></div>\n      <div class=\"col-sm-6\" style=\"background-color:blue;\"><input type=\"radio\" name=\"heart\" value=\"notbeats\" [(ngModel)]=\"notbeats\"> Heart not beats<br></div>\n  \n    \n\n      <div class=\"col-sm-12\"   align=\"center\" style=\"background-color:red;\"  > <button type=\"button\"   style=\"background-color:red;\"  (click)=\"submitted=false\" (click)=\"sensorValues()\">SENSORS</button>  </div>\n   \n\n\n\n      \n      \n      <div class=\"col-sm-12\"  align=\"center\"  style=\"background-color:red;\"><input id=\"file\" type=\"file\" accept=\".csv\" (change)=\"fileUpload($event.target.files)\"></div>\n   \n   \n      <h1>\n         {{title}}\n      </h1>\n     \n     <!--\n      <div class=\"col-sm-12\"   align=\"center\" style=\"background-color:red;\"  >  <button (click)=\"notification()\">Notification</button>  </div>-->\n      <div class=\"col-sm-12\"   align=\"center\" style=\"background-color:red;\"  > <button type=\"button\"   style=\"background-color:red;\"  (click)=\"submitted=false\" (click)=\"submit()\">Menu</button>  </div>\n   \n      <div class=\"col-sm-12\"   align=\"center\" style=\"background-color:red;\"  > <button type=\"button\"   style=\"background-color:red;\"  (click)=\"submitted=false\" (click)=\"makeProgram()\">Program Make</button>  </div>\n    </div>\n  </div>\n \n\n\n"

/***/ }),

/***/ "./src/app/admin/admin.component.ts":
/*!******************************************!*\
  !*** ./src/app/admin/admin.component.ts ***!
  \******************************************/
/*! exports provided: AdminComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminComponent", function() { return AdminComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _admin_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./admin.service */ "./src/app/admin/admin.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _calendar_calendar_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../calendar/calendar.service */ "./src/app/calendar/calendar.service.ts");
/* harmony import */ var ngx_push_notifications__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-push-notifications */ "./node_modules/ngx-push-notifications/fesm5/ngx-push-notifications.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");








var AdminComponent = /** @class */ (function () {
    function AdminComponent(_pushNotificationService, AdminService, CalendarService, http, router) {
        this._pushNotificationService = _pushNotificationService;
        this.AdminService = AdminService;
        this.CalendarService = CalendarService;
        this.http = http;
        this.router = router;
        this.url = "http://localhost:3000/api/things/getmenu/breakfast/";
        this.urlinter1 = "http://localhost:3000/api/things/getmenu/inter1/";
        this.urlunch = "http://localhost:3000/api/things/getmenu/lunch/";
        this.urlinter2 = "http://localhost:3000/api/things/getmenu/inter2/";
        this.urldinner = "http://localhost:3000/api/things/getmenu/dinner/";
    }
    AdminComponent.prototype.sensorValues = function () {
        this.sensorhouse = jquery__WEBPACK_IMPORTED_MODULE_2__("input[name='house']:checked").val();
        this.sensorheart = jquery__WEBPACK_IMPORTED_MODULE_2__("input[name='heart']:checked").val();
        this.sensorsitting = jquery__WEBPACK_IMPORTED_MODULE_2__("input[name='sitting']:checked").val();
        //alert("Sensor is:"+this.sensorhouse+this.sensorheart+this.sensorsitting+this.usr_time);
        this.AdminService.setSensors(this.sensorhouse, this.sensorheart, this.sensorsitting, this.usr_time).subscribe(function (res) {
            // alert("Sensor called");
            //alert("Data post each time");
            // alert(JSON.stringify(res));
            //var i=JSON.stringify(res);
            // var stringify = JSON.parse(i);
            //alert("Hier vars"+stringify.name);
            //alert(stringify['name']);
        }, function (error) {
            alert("error hier");
        });
    };
    AdminComponent.prototype.notification = function () {
        function test() {
            // alert("finalaa");
            //alert(location.host);
            //alert(location.hostname);
            // alert("concat");
            // alert(location.host.toString().concat("/main"));
            var i = location.host.toString().concat("/notification").toString().trim();
            // alert(i);
            var all = "http://".concat(i);
            // alert("all are");
            //alert(all);
            window.location.href = all; // i;//'http://www.google.com';
        }
        var title = 'Meal Recommendation';
        var options = new ngx_push_notifications__WEBPACK_IMPORTED_MODULE_6__["PushNotificationOptions"]();
        options.body = 'Its time for meal now';
        options.icon = 'https://banner2.kisspng.com/20180219/zqq/kisspng-hamburger-fast-food-restaurant-junk-food-kfc-cartoon-french-fries-5a8b6353d9a269.5412864515190843718914.jpg'; //adding an icon
        this._pushNotificationService.create(title, options).subscribe(function (notif) {
            if (notif.event.type === 'show') {
                console.log('onshow');
                setTimeout(function () {
                    // notif.notification.close();
                }, 9000);
            }
            if (notif.event.type === 'click') {
                console.log('click');
                alert("Clicked");
                //notif.notification.close();
                // this.goToPage("/","calendar");
                test();
                notif.notification.close();
            }
            if (notif.event.type === 'close') {
                console.log('close');
            }
        }, function (err) {
            console.log(err);
        });
    };
    AdminComponent.prototype.goToPage = function (pagename, parameter) {
        this.router.navigate([pagename, parameter]);
    };
    AdminComponent.prototype.fileUpload = function (files) {
        var _this = this;
        /* this.sensorhouse = $("input[name='house']:checked").val();
         this.sensorheart = $("input[name='heart']:checked").val();
         this.sensorsitting=$("input[name='sitting']:checked").val();
        // this.sensoralarm=$("input[name='usr_time']:checked").val();*/
        alert("Sensor is:" + this.sensorhouse + this.sensorheart + this.sensorsitting + this.usr_time);
        // alert("file upload called");
        if (files && files.length > 0) {
            var file = files.item(0);
            var reader_1 = new FileReader();
            reader_1.readAsText(file);
            reader_1.onload = function (e) {
                var res = reader_1.result; // This variable contains your file as text
                var lines = res.split('\n'); // Splits you file into lines
                var date = [];
                var minuteslightlyactive = [];
                var minutesfairlyactive = [];
                var minutesverylyactive = [];
                lines.forEach(function (line) {
                    date.push(line.split(',')[3]);
                    minuteslightlyactive.push(line.split(',')[0]);
                    minutesfairlyactive.push(line.split(',')[1]);
                    minutesverylyactive.push(line.split(',')[2]);
                    ///////////////////////////////////////////////////////////////
                    var name = jquery__WEBPACK_IMPORTED_MODULE_2__("input[name='Emily']:checked").val();
                    //alert("name is"+name);
                    _this.AdminService.setWearable(name, line.split(',')[3], line.split(',')[0], String((line.split(',')[1])), String((line.split(',')[2])), "", "", "", "").subscribe(function (res) {
                        //alert("Data post each time");
                        // alert(JSON.stringify(res));
                        //var i=JSON.stringify(res);
                        // var stringify = JSON.parse(i);
                        //alert("Hier vars"+stringify.name);
                        //alert(stringify['name']);
                    }, function (error) {
                        alert("error hier");
                    });
                    ////////////////////////////////////////////////////////////////
                });
                // alert(date);
                // alert(minutesfairlyactive);
                // alert(minutesfairlyactive);
                // alert(minutesverylyactive);
                //console.log(minuteslightlyactive);
            };
        }
    };
    AdminComponent.prototype.makeProgram = function () {
        var _this = this;
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        var day = +dd;
        var days = day;
        var todays = mm + '/' + days + '/' + yyyy;
        function delay(ms) {
            return new Promise(function (resolve) { return setTimeout(resolve, ms); });
        }
        var daymenu = "monday";
        this.http.get("" + this.url + daymenu, { responseType: "json" }).subscribe(function (response) {
            alert("Response for one value is" + response[0].energycontent);
            _this.CalendarService.setTempCalendar("Emily", response[0].name, response[0].type, response[0].energycontent, response[0].nutrition, response[0].wayofcooking, response[0].calories, response[0].kg, response[0].info, daymenu, response[0].imageurl).subscribe(function (res) {
                // alert("Set temp calendar hier");
            }, function (error) {
                alert("error hier");
            });
        });
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(1000)];
                    case 1:
                        _a.sent();
                        this.http.get("" + this.urlinter1 + daymenu, { responseType: "json" }).subscribe(function (response) {
                            alert("Response for one value is" + response[0].energycontent);
                            _this.CalendarService.setTempCalendar("Emily", response[0].name, response[0].type, response[0].energycontent, response[0].nutrition, response[0].wayofcooking, response[0].calories, response[0].kg, response[0].info, daymenu, response[0].imageurl).subscribe(function (res) {
                                // alert("Set temp calendar hier");
                            }, function (error) {
                                alert("error hier");
                            });
                        });
                        return [2 /*return*/];
                }
            });
        }); })();
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(2000)];
                    case 1:
                        _a.sent();
                        this.http.get("" + this.urlunch + daymenu, { responseType: "json" }).subscribe(function (response) {
                            alert("Response for one value is" + response[0].energycontent);
                            _this.CalendarService.setTempCalendar("Emily", response[0].name, response[0].type, response[0].energycontent, response[0].nutrition, response[0].wayofcooking, response[0].calories, response[0].kg, response[0].info, daymenu, response[0].imageurl).subscribe(function (res) {
                                // alert("Set temp calendar hier");
                            }, function (error) {
                                alert("error hier");
                            });
                        });
                        return [2 /*return*/];
                }
            });
        }); })();
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(3000)];
                    case 1:
                        _a.sent();
                        this.http.get("" + this.urlinter2 + daymenu, { responseType: "json" }).subscribe(function (response) {
                            alert("Response for one value is" + response[0].energycontent);
                            _this.CalendarService.setTempCalendar("Emily", response[0].name, response[0].type, response[0].energycontent, response[0].nutrition, response[0].wayofcooking, response[0].calories, response[0].kg, response[0].info, daymenu, response[0].imageurl).subscribe(function (res) {
                                // alert("Set temp calendar hier");
                            }, function (error) {
                                alert("error hier");
                            });
                        });
                        return [2 /*return*/];
                }
            });
        }); })();
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(4000)];
                    case 1:
                        _a.sent();
                        this.http.get("" + this.urldinner + daymenu, { responseType: "json" }).subscribe(function (response) {
                            alert("Response for one value is" + response[0].energycontent);
                            _this.CalendarService.setTempCalendar("Emily", response[0].name, response[0].type, response[0].energycontent, response[0].nutrition, response[0].wayofcooking, response[0].calories, response[0].kg, response[0].info, daymenu, response[0].imageurl).subscribe(function (res) {
                                // alert("Set temp calendar hier");
                            }, function (error) {
                                alert("error hier");
                            });
                        });
                        return [2 /*return*/];
                }
            });
        }); })();
        var daymenu2 = "tuesday";
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(5000)];
                    case 1:
                        _a.sent();
                        this.http.get("" + this.url + daymenu2, { responseType: "json" }).subscribe(function (response) {
                            alert("Response for one value is" + response[0].energycontent);
                            //alert("Response for one value is"+response[0].name);
                            _this.CalendarService.setTempCalendar("Emily", response[0].name, response[0].type, response[0].energycontent, response[0].nutrition, response[0].wayofcooking, response[0].calories, response[0].kg, response[0].info, daymenu2, response[0].imageurl).subscribe(function (res) {
                                // alert("Set temp calendar hier");
                            }, function (error) {
                                alert("error hier");
                            });
                        });
                        return [2 /*return*/];
                }
            });
        }); })();
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(6000)];
                    case 1:
                        _a.sent();
                        this.http.get("" + this.urlinter1 + daymenu2, { responseType: "json" }).subscribe(function (response) {
                            // alert("Response for one value is"+response[0].name);
                            _this.CalendarService.setTempCalendar("Emily", response[0].name, response[0].type, response[0].energycontent, response[0].nutrition, response[0].wayofcooking, response[0].calories, response[0].kg, response[0].info, daymenu2, response[0].imageurl).subscribe(function (res) {
                                // alert("Set temp calendar hier");
                            }, function (error) {
                                alert("error hier");
                            });
                        });
                        return [2 /*return*/];
                }
            });
        }); })();
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(7000)];
                    case 1:
                        _a.sent();
                        this.http.get("" + this.urlunch + daymenu2, { responseType: "json" }).subscribe(function (response) {
                            alert("Breakfast hier" + response[0].type);
                            //alert("Response for one value is"+response[0].name);
                            _this.CalendarService.setTempCalendar("Emily", response[0].name, response[0].type, response[0].energycontent, response[0].nutrition, response[0].wayofcooking, response[0].calories, response[0].kg, response[0].info, daymenu2, response[0].imageurl).subscribe(function (res) {
                                alert("Kableddd");
                            }, function (error) {
                                alert("error hier");
                            });
                        });
                        return [2 /*return*/];
                }
            });
        }); })();
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(8000)];
                    case 1:
                        _a.sent();
                        this.http.get("" + this.urlinter2 + daymenu2, { responseType: "json" }).subscribe(function (response) {
                            // alert("Response for one value is"+response[0].name);
                            _this.CalendarService.setTempCalendar("Emily", response[0].name, response[0].type, response[0].energycontent, response[0].nutrition, response[0].wayofcooking, response[0].calories, response[0].kg, response[0].info, daymenu2, response[0].imageurl).subscribe(function (res) {
                                // alert("Set temp calendar hier");
                            }, function (error) {
                                alert("error hier");
                            });
                        });
                        return [2 /*return*/];
                }
            });
        }); })();
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(9000)];
                    case 1:
                        _a.sent();
                        this.http.get("" + this.urldinner + daymenu2, { responseType: "json" }).subscribe(function (response) {
                            // alert("Response for one value is"+response[0].name);
                            _this.CalendarService.setTempCalendar("Emily", response[0].name, response[0].type, response[0].energycontent, response[0].nutrition, response[0].wayofcooking, response[0].calories, response[0].kg, response[0].info, daymenu2, response[0].imageurl).subscribe(function (res) {
                                // alert("Set temp calendar hier");
                            }, function (error) {
                                alert("error hier");
                            });
                        });
                        return [2 /*return*/];
                }
            });
        }); })();
        ///////////////////////////
        var daymenu3 = "wednesday";
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(10000)];
                    case 1:
                        _a.sent();
                        this.http.get("" + this.url + daymenu3, { responseType: "json" }).subscribe(function (response) {
                            alert("Breakfast hier" + response[0].type);
                            //alert("Response for one value is"+response[0].name);
                            _this.CalendarService.setTempCalendar("Emily", response[0].name, response[0].type, response[0].energycontent, response[0].nutrition, response[0].wayofcooking, response[0].calories, response[0].kg, response[0].info, daymenu3, response[0].imageurl).subscribe(function (res) {
                                // alert("Set temp calendar hier");
                            }, function (error) {
                                alert("error hier");
                            });
                        });
                        return [2 /*return*/];
                }
            });
        }); })();
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(11000)];
                    case 1:
                        _a.sent();
                        this.http.get("" + this.urlinter1 + daymenu3, { responseType: "json" }).subscribe(function (response) {
                            // alert("Response for one value is"+response[0].name);
                            _this.CalendarService.setTempCalendar("Emily", response[0].name, response[0].type, response[0].energycontent, response[0].nutrition, response[0].wayofcooking, response[0].calories, response[0].kg, response[0].info, daymenu3, response[0].imageurl).subscribe(function (res) {
                                // alert("Set temp calendar hier");
                            }, function (error) {
                                alert("error hier");
                            });
                        });
                        return [2 /*return*/];
                }
            });
        }); })();
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(12000)];
                    case 1:
                        _a.sent();
                        this.http.get("" + this.urlunch + daymenu3, { responseType: "json" }).subscribe(function (response) {
                            alert("Breakfast hier" + response[0].type);
                            //alert("Response for one value is"+response[0].name);
                            _this.CalendarService.setTempCalendar("Emily", response[0].name, response[0].type, response[0].energycontent, response[0].nutrition, response[0].wayofcooking, response[0].calories, response[0].kg, response[0].info, daymenu3, response[0].imageurl).subscribe(function (res) {
                                alert("Kableddd");
                            }, function (error) {
                                alert("error hier");
                            });
                        });
                        return [2 /*return*/];
                }
            });
        }); })();
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(13000)];
                    case 1:
                        _a.sent();
                        this.http.get("" + this.urlinter2 + daymenu3, { responseType: "json" }).subscribe(function (response) {
                            // alert("Response for one value is"+response[0].name);
                            _this.CalendarService.setTempCalendar("Emily", response[0].name, response[0].type, response[0].energycontent, response[0].nutrition, response[0].wayofcooking, response[0].calories, response[0].kg, response[0].info, daymenu3, response[0].imageurl).subscribe(function (res) {
                                // alert("Set temp calendar hier");
                            }, function (error) {
                                alert("error hier");
                            });
                        });
                        return [2 /*return*/];
                }
            });
        }); })();
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(14000)];
                    case 1:
                        _a.sent();
                        this.http.get("" + this.urldinner + daymenu3, { responseType: "json" }).subscribe(function (response) {
                            // alert("Response for one value is"+response[0].name);
                            _this.CalendarService.setTempCalendar("Emily", response[0].name, response[0].type, response[0].energycontent, response[0].nutrition, response[0].wayofcooking, response[0].calories, response[0].kg, response[0].info, daymenu3, response[0].imageurl).subscribe(function (res) {
                                // alert("Set temp calendar hier");
                            }, function (error) {
                                alert("error hier");
                            });
                        });
                        return [2 /*return*/];
                }
            });
        }); })();
        var daymenu4 = "thursday";
        /////////////////////////////////////////////
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(15000)];
                    case 1:
                        _a.sent();
                        this.http.get("" + this.url + daymenu4, { responseType: "json" }).subscribe(function (response) {
                            alert("Breakfast hier" + response[0].type);
                            //alert("Response for one value is"+response[0].name);
                            _this.CalendarService.setTempCalendar("Emily", response[0].name, response[0].type, response[0].energycontent, response[0].nutrition, response[0].wayofcooking, response[0].calories, response[0].kg, response[0].info, daymenu4, response[0].imageurl).subscribe(function (res) {
                                // alert("Set temp calendar hier");
                            }, function (error) {
                                alert("error hier");
                            });
                        });
                        return [2 /*return*/];
                }
            });
        }); })();
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(16000)];
                    case 1:
                        _a.sent();
                        this.http.get("" + this.urlinter1 + daymenu4, { responseType: "json" }).subscribe(function (response) {
                            // alert("Response for one value is"+response[0].name);
                            _this.CalendarService.setTempCalendar("Emily", response[0].name, response[0].type, response[0].energycontent, response[0].nutrition, response[0].wayofcooking, response[0].calories, response[0].kg, response[0].info, daymenu4, response[0].imageurl).subscribe(function (res) {
                                // alert("Set temp calendar hier");
                            }, function (error) {
                                alert("error hier");
                            });
                        });
                        return [2 /*return*/];
                }
            });
        }); })();
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(17000)];
                    case 1:
                        _a.sent();
                        this.http.get("" + this.urlunch + daymenu4, { responseType: "json" }).subscribe(function (response) {
                            alert("Breakfast hier" + response[0].type);
                            //alert("Response for one value is"+response[0].name);
                            _this.CalendarService.setTempCalendar("Emily", response[0].name, response[0].type, response[0].wayofcooking, response[0].nutrition, response[0].wayofcooking, response[0].calories, response[0].kg, response[0].info, daymenu4, response[0].imageurl).subscribe(function (res) {
                                alert("Kableddd");
                            }, function (error) {
                                alert("error hier");
                            });
                        });
                        return [2 /*return*/];
                }
            });
        }); })();
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(18000)];
                    case 1:
                        _a.sent();
                        this.http.get("" + this.urlinter2 + daymenu4, { responseType: "json" }).subscribe(function (response) {
                            // alert("Response for one value is"+response[0].name);
                            _this.CalendarService.setTempCalendar("Emily", response[0].name, response[0].type, response[0].wayofcooking, response[0].nutrition, response[0].wayofcooking, response[0].calories, response[0].kg, response[0].info, daymenu4, response[0].imageurl).subscribe(function (res) {
                                // alert("Set temp calendar hier");
                            }, function (error) {
                                alert("error hier");
                            });
                        });
                        return [2 /*return*/];
                }
            });
        }); })();
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(19000)];
                    case 1:
                        _a.sent();
                        this.http.get("" + this.urldinner + daymenu4, { responseType: "json" }).subscribe(function (response) {
                            // alert("Response for one value is"+response[0].name);
                            _this.CalendarService.setTempCalendar("Emily", response[0].name, response[0].type, response[0].wayofcooking, response[0].nutrition, response[0].wayofcooking, response[0].calories, response[0].kg, response[0].info, daymenu4, response[0].imageurl).subscribe(function (res) {
                                // alert("Set temp calendar hier");
                            }, function (error) {
                                alert("error hier");
                            });
                        });
                        return [2 /*return*/];
                }
            });
        }); })();
        ////////////////////////////////////////////////
        var daymenu5 = "friday";
        /////////////////////////////////////////////
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(20000)];
                    case 1:
                        _a.sent();
                        this.http.get("" + this.url + daymenu5, { responseType: "json" }).subscribe(function (response) {
                            alert("Breakfast hier" + response[0].type);
                            //alert("Response for one value is"+response[0].name);
                            _this.CalendarService.setTempCalendar("Emily", response[0].name, response[0].type, response[0].wayofcooking, response[0].nutrition, response[0].wayofcooking, response[0].calories, response[0].kg, response[0].info, daymenu5, response[0].imageurl).subscribe(function (res) {
                                // alert("Set temp calendar hier");
                            }, function (error) {
                                alert("error hier");
                            });
                        });
                        return [2 /*return*/];
                }
            });
        }); })();
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(21000)];
                    case 1:
                        _a.sent();
                        this.http.get("" + this.urlinter1 + daymenu5, { responseType: "json" }).subscribe(function (response) {
                            // alert("Response for one value is"+response[0].name);
                            _this.CalendarService.setTempCalendar("Emily", response[0].name, response[0].type, response[0].wayofcooking, response[0].nutrition, response[0].wayofcooking, response[0].calories, response[0].kg, response[0].info, daymenu5, response[0].imageurl).subscribe(function (res) {
                                // alert("Set temp calendar hier");
                            }, function (error) {
                                alert("error hier");
                            });
                        });
                        return [2 /*return*/];
                }
            });
        }); })();
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(22000)];
                    case 1:
                        _a.sent();
                        this.http.get("" + this.urlunch + daymenu5, { responseType: "json" }).subscribe(function (response) {
                            alert("Breakfast hier" + response[0].type);
                            //alert("Response for one value is"+response[0].name);
                            _this.CalendarService.setTempCalendar("Emily", response[0].name, response[0].type, response[0].wayofcooking, response[0].nutrition, response[0].wayofcooking, response[0].calories, response[0].kg, response[0].info, daymenu5, response[0].imageurl).subscribe(function (res) {
                                alert("Kableddd");
                            }, function (error) {
                                alert("error hier");
                            });
                        });
                        return [2 /*return*/];
                }
            });
        }); })();
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(23000)];
                    case 1:
                        _a.sent();
                        this.http.get("" + this.urlinter2 + daymenu5, { responseType: "json" }).subscribe(function (response) {
                            // alert("Response for one value is"+response[0].name);
                            _this.CalendarService.setTempCalendar("Emily", response[0].name, response[0].type, response[0].wayofcooking, response[0].nutrition, response[0].wayofcooking, response[0].calories, response[0].kg, response[0].info, daymenu5, response[0].imageurl).subscribe(function (res) {
                                // alert("Set temp calendar hier");
                            }, function (error) {
                                alert("error hier");
                            });
                        });
                        return [2 /*return*/];
                }
            });
        }); })();
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(24000)];
                    case 1:
                        _a.sent();
                        this.http.get("" + this.urldinner + daymenu5, { responseType: "json" }).subscribe(function (response) {
                            // alert("Response for one value is"+response[0].name);
                            _this.CalendarService.setTempCalendar("Emily", response[0].name, response[0].type, response[0].wayofcooking, response[0].nutrition, response[0].wayofcooking, response[0].calories, response[0].kg, response[0].info, daymenu5, response[0].imageurl).subscribe(function (res) {
                                // alert("Set temp calendar hier");
                            }, function (error) {
                                alert("error hier");
                            });
                        });
                        return [2 /*return*/];
                }
            });
        }); })();
        /////////////////////////////////////
    };
    AdminComponent.prototype.submit = function () {
        var _this = this;
        alert("Clicked");
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        var day = +dd;
        var days = day;
        var todays = mm + '/' + days + '/' + yyyy;
        this.AdminService.getRecipes("").subscribe(function (res) {
            for (var i in res) {
                alert("new image url is");
                alert(res[i].imageurl);
                _this.AdminService.setMenu(res[i].name, res[i].type, res[i].day, res[i].energycontent, res[i].wayofcooking, res[i].nutrition, res[i].calories, res[i].kg, res[i].info, res[i].imageurl).subscribe(function (res) {
                }, function (error) {
                    alert("error hier");
                });
            }
            function delay(ms) {
                return new Promise(function (resolve) { return setTimeout(resolve, ms); });
            }
            /*(async () => {
              await delay(1000);
            this.http.get('http://localhost:3000/api/things/getsensorvalues/niko',{responseType:"json"}).subscribe(
              response => {
              var sample=JSON.parse(JSON.stringify(response));
              var a=String(sample.status);
            
            
            });///hier end calendar
              
            
            })();*/
            //////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////
        }, function (error) {
            alert("error hier");
        });
    };
    AdminComponent.prototype.ngOnInit = function () {
        jquery__WEBPACK_IMPORTED_MODULE_2__("#main").hide();
        this._pushNotificationService.requestPermission();
        /* this.AdminService.getRecipes("").subscribe((res)=>{
            alert("resipes respose is:");
           for(var i in res){
              alert(res[i].name);
              alert(res[i].type);
           }
          }, (error) =>
          {
              alert("error hier");
             
          });*/
    };
    AdminComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-admin',
            template: __webpack_require__(/*! ./admin.component.html */ "./src/app/admin/admin.component.html"),
            styles: [__webpack_require__(/*! ./admin.component.css */ "./src/app/admin/admin.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [ngx_push_notifications__WEBPACK_IMPORTED_MODULE_6__["PushNotificationService"], _admin_service__WEBPACK_IMPORTED_MODULE_3__["AdminService"], _calendar_calendar_service__WEBPACK_IMPORTED_MODULE_5__["CalendarService"], _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"]])
    ], AdminComponent);
    return AdminComponent;
}());



/***/ }),

/***/ "./src/app/admin/admin.service.ts":
/*!****************************************!*\
  !*** ./src/app/admin/admin.service.ts ***!
  \****************************************/
/*! exports provided: AdminService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminService", function() { return AdminService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");



var AdminService = /** @class */ (function () {
    function AdminService(httpClient) {
        this.httpClient = httpClient;
        this.apiURL = 'http://127.0.0.1:3000/api/things/setwearable';
        this.apiURLMENU = 'http://127.0.0.1:3000/api/things/setmenu';
        this.apiURLCAL = 'http://127.0.0.1:3000/api/things/setcalendarmenu';
        this.apiURLSEN = 'http://127.0.0.1:3000/api/things/setsensor/';
    }
    AdminService.prototype.setTempCalendar2 = function (username, name, type, energycontent, content, wayofcooking, nutrition, calories, kg, info, date) {
        var data = {
            "name": "fr"
        };
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
            })
        };
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
            .set('username', String(username))
            .set('name', String(name))
            .set('type', String(type))
            .set('food', String(content))
            .set('energycontent', String(energycontent))
            .set('wayofcooking', String(wayofcooking))
            .set('nutrition', String(nutrition))
            .set('calories', String(calories))
            .set('kg', String(kg))
            .set('info', String(info))
            .set('date', String(date));
        var options = {
            httpOptions: httpOptions,
            params: params,
            withCredentials: false
        };
        return this.httpClient.post("" + this.apiURLCAL, null, options);
    };
    AdminService.prototype.setWearable = function (username, date, minuteslightlyactive, minutesfairlyactive, minutesveryactive, sensor, sensor2, sensor3, sensor4) {
        var data = {
            "name": "fr"
        };
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
            })
        };
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
            .set('username', String(username))
            .set('date', String(date))
            .set('minuteslightlyactive', String(minuteslightlyactive))
            .set('minutesfairlyactive', String(minutesfairlyactive))
            .set('minutesveryactive', String(minutesveryactive))
            .set('sensorhouse', String(sensor))
            .set('sensorheart', String(sensor2))
            .set('sensorsitting', String(sensor3))
            .set('sensoralarm', String(sensor4));
        var options = {
            httpOptions: httpOptions,
            params: params,
            withCredentials: false
        };
        return this.httpClient.post("" + this.apiURL, null, options);
    };
    AdminService.prototype.setTempCalendar = function (username, name, type, energycontent, wayofcooking, nutrition, calories, kg, info, date) {
        var data = {
            "name": "fr"
        };
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
            })
        };
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
            .set('username', String(username))
            .set('name', String(name))
            .set('type', String(type))
            .set('energycontent', String(energycontent))
            .set('wayofcooking', String(wayofcooking))
            .set('nutrition', String(nutrition))
            .set('calories', String(calories))
            .set('kg', String(kg))
            .set('info', String(info))
            .set('date', String(date));
        var options = {
            httpOptions: httpOptions,
            params: params,
            withCredentials: false
        };
        return this.httpClient.post("" + this.apiURLCAL, null, options);
    };
    AdminService.prototype.setSensors = function (sensor, sensor2, sensor3, sensor4) {
        //  alert("Called setsensor");
        var data = {
            "name": "fr"
        };
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
            })
        };
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
            .set('sensorhouse', String(sensor))
            .set('sensorheart', String(sensor2))
            .set('sensorsitting', String(sensor3))
            .set('sensoralarm', String(sensor4));
        var options = {
            httpOptions: httpOptions,
            params: params,
            withCredentials: false
        };
        return this.httpClient.post("http://127.0.0.1:3000/api/things/setsensorsvalue/", null, options);
    };
    AdminService.prototype.setMenu = function (name, type, day, energycontent, wayofcooking, nutrition, calories, kg, info, imageurl) {
        var data = {
            "name": "fr"
        };
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
            })
        };
        // alert("energycontent"+String(energycontent));
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
            .set('name', String(name))
            .set('type', String(type))
            .set('day', String(day))
            .set('energycontent', String(energycontent))
            .set('wayofcooking', String(wayofcooking))
            .set('nutrition', String(nutrition))
            .set('calories', String(calories))
            .set('kg', String(kg))
            .set('info', String(info))
            .set('imageurl', String(imageurl));
        var options = {
            httpOptions: httpOptions,
            params: params,
            withCredentials: false
        };
        return this.httpClient.post("" + this.apiURLMENU, null, options);
    };
    AdminService.prototype.getRecipes = function (input) {
        var data = {
            "name": "fr"
        };
        return this.httpClient.get("http://localhost:3000/api/things/recipes", { responseType: "json" });
    };
    AdminService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], AdminService);
    return AdminService;
}());



/***/ }),

/***/ "./src/app/analytic/analytic.component.css":
/*!*************************************************!*\
  !*** ./src/app/analytic/analytic.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "body{background:black}\r\n#progress-container{width:80%;height:40px;background:#edebee;position:absolute;top:50%;left:50%;-webkit-transform:translate(-50%,-50%);transform:translate(-50%,-50%)}\r\n#progress-bar{width:2%;height:100%;background:red;color:white;text-align:center;line-height:40px}\r\n#button1 {\r\n    background-color: #b2b2b2\t; \r\n    border: none;\r\n    color: white;\r\n    padding-bottom: 25px 22px;\r\n    text-align: center;\r\n    text-decoration: none;\r\n    display: inline-block;\r\n    font-size: 36px;\r\n    margin: 4px 18px;\r\n    cursor: pointer;\r\n  }\r\n#button2 {\r\n   background-color: #b2b2b2\t; \r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n#button3 {\r\n   background-color: #b2b2b2\t; \r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n#button4 {\r\n   background-color: #b2b2b2\t;\r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n#all{\r\n     text-align: center;\r\n     position: fixed;\r\n     left: 0;\r\n     bottom: 0;\r\n     width: 100%;\r\n     background-color: \t\t#b2b2b2;\r\n     color: white;\r\n     text-align: center;\r\n  }\r\ndiv.home {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\ndiv.home input {\r\n    background: url('home.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\ndiv.analytic {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\ndiv.analytic input {\r\n    background: url('analytic.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\ndiv.notification {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\ndiv.notification input {\r\n    background: url('outline-today-24px.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\ndiv.setting {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\ndiv.setting input {\r\n    background: url('settings.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\nspan.home {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 6px;\r\n  }\r\nspan.analytic {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 2px;\r\n  }\r\nspan.notification {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 2px;\r\n  }\r\nspan.setting {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 12px;\r\n  }\r\n#myProgress {\r\n    margin-top: 60px;\r\n    \r\n    border-radius: 45px;\r\n    width: 100%;\r\n    \r\n    background-color: #ddd;\r\n   \r\n  }\r\n#myBar {\r\n    margin-top: 60px;\r\n    \r\n    border-radius: 45px;\r\n    padding-top: 15px;\r\n    width: 1%;\r\n    height: 15px;\r\n    background-color: \t#0000FF;\r\n   \r\n  }\r\n* {\r\n      /* border: 1px solid red; */\r\n  }\r\n:root {\r\n      --font: \"Bitter\", serif;\r\n      --title-size: 36px;\r\n      --sub-size: 18px;\r\n  }\r\nbody {\r\n      background: #e5e5e5;\r\n  }\r\n.title h1 {\r\n      margin: 4px;\r\n      font-family: var(--font);\r\n      font-size: var(--title-size);\r\n      color: #333;\r\n  }\r\n.title p {\r\n      margin: 4px;\r\n      padding-bottom: 25px;\r\n      font-family: var(--font);\r\n      font-size: var(--sub-size);\r\n      color: #888;\r\n  }\r\n.container {\r\n      text-align: center;\r\n  }\r\n.github {\r\n      margin: 40px;\r\n  }\r\n.progress {\r\n      display: inline-block;\r\n      width: 400px;\r\n      height: 50px;\r\n      margin: 35px;\r\n      border-radius: 20px;\r\n      background: #f9f9f9;\r\n  }\r\n.bar {\r\n      border-radius: 20px;\r\n      width: 0%;\r\n      height: 100%;\r\n      transition: width;\r\n      transition-duration: 1s;\r\n      transition-timing-function: cubic-bezier(0.36, 0.55, 0.63, 0.48);\r\n  }\r\n.mobile {\r\n      display: none;\r\n  }\r\n.shadow {\r\n      /* 25 50 */\r\n      box-shadow: 0px 45px 50px rgba(0, 0, 0, 0.25);\r\n  }\r\n.crosses {\r\n      background-color: #dfdbe5;\r\n      background-image: url(\"data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='25' height='25' viewBox='0 0 40 40'%3E%3Cg fill-rule='evenodd'%3E%3Cg fill='%239C92AC' fill-opacity='0.4'%3E%3Cpath d='M0 38.59l2.83-2.83 1.41 1.41L1.41 40H0v-1.41zM0 1.4l2.83 2.83 1.41-1.41L1.41 0H0v1.41zM38.59 40l-2.83-2.83 1.41-1.41L40 38.59V40h-1.41zM40 1.41l-2.83 2.83-1.41-1.41L38.59 0H40v1.41zM20 18.6l2.83-2.83 1.41 1.41L21.41 20l2.83 2.83-1.41 1.41L20 21.41l-2.83 2.83-1.41-1.41L18.59 20l-2.83-2.83 1.41-1.41L20 18.59z'/%3E%3C/g%3E%3C/g%3E%3C/svg%3E\");\r\n  }\r\n.jupiter {\r\n      background-color: #f395a5;\r\n      background-image: url(\"data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='52' height='52' viewBox='0 0 52 52'%3E%3Cpath fill='%23e6cca5' fill-opacity='0.4' d='M0 17.83V0h17.83a3 3 0 0 1-5.66 2H5.9A5 5 0 0 1 2 5.9v6.27a3 3 0 0 1-2 5.66zm0 18.34a3 3 0 0 1 2 5.66v6.27A5 5 0 0 1 5.9 52h6.27a3 3 0 0 1 5.66 0H0V36.17zM36.17 52a3 3 0 0 1 5.66 0h6.27a5 5 0 0 1 3.9-3.9v-6.27a3 3 0 0 1 0-5.66V52H36.17zM0 31.93v-9.78a5 5 0 0 1 3.8.72l4.43-4.43a3 3 0 1 1 1.42 1.41L5.2 24.28a5 5 0 0 1 0 5.52l4.44 4.43a3 3 0 1 1-1.42 1.42L3.8 31.2a5 5 0 0 1-3.8.72zm52-14.1a3 3 0 0 1 0-5.66V5.9A5 5 0 0 1 48.1 2h-6.27a3 3 0 0 1-5.66-2H52v17.83zm0 14.1a4.97 4.97 0 0 1-1.72-.72l-4.43 4.44a3 3 0 1 1-1.41-1.42l4.43-4.43a5 5 0 0 1 0-5.52l-4.43-4.43a3 3 0 1 1 1.41-1.41l4.43 4.43c.53-.35 1.12-.6 1.72-.72v9.78zM22.15 0h9.78a5 5 0 0 1-.72 3.8l4.44 4.43a3 3 0 1 1-1.42 1.42L29.8 5.2a5 5 0 0 1-5.52 0l-4.43 4.44a3 3 0 1 1-1.41-1.42l4.43-4.43a5 5 0 0 1-.72-3.8zm0 52c.13-.6.37-1.19.72-1.72l-4.43-4.43a3 3 0 1 1 1.41-1.41l4.43 4.43a5 5 0 0 1 5.52 0l4.43-4.43a3 3 0 1 1 1.42 1.41l-4.44 4.43c.36.53.6 1.12.72 1.72h-9.78zm9.75-24a5 5 0 0 1-3.9 3.9v6.27a3 3 0 1 1-2 0V31.9a5 5 0 0 1-3.9-3.9h-6.27a3 3 0 1 1 0-2h6.27a5 5 0 0 1 3.9-3.9v-6.27a3 3 0 1 1 2 0v6.27a5 5 0 0 1 3.9 3.9h6.27a3 3 0 1 1 0 2H31.9z'%3E%3C/path%3E%3C/svg%3E\");\r\n  }\r\n.piano {\r\n      background-color: #cccccc;\r\n      background-image: url(\"data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='70' height='46' viewBox='0 0 70 46'%3E%3Cg fill-rule='evenodd'%3E%3Cg fill='%23333333' fill-opacity='0.4'%3E%3Cpolygon points='68 44 62 44 62 46 56 46 56 44 52 44 52 46 46 46 46 44 40 44 40 46 38 46 38 44 32 44 32 46 26 46 26 44 22 44 22 46 16 46 16 44 12 44 12 46 6 46 6 44 0 44 0 42 8 42 8 28 6 28 6 0 12 0 12 28 10 28 10 42 18 42 18 28 16 28 16 0 22 0 22 28 20 28 20 42 28 42 28 28 26 28 26 0 32 0 32 28 30 28 30 42 38 42 38 0 40 0 40 42 48 42 48 28 46 28 46 0 52 0 52 28 50 28 50 42 58 42 58 28 56 28 56 0 62 0 62 28 60 28 60 42 68 42 68 0 70 0 70 46 68 46'/%3E%3C/g%3E%3C/g%3E%3C/svg%3E\");\r\n  }\r\n.dominos {\r\n      background-color: #fff6bd;\r\n      background-image: url(\"data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='126' height='84' viewBox='0 0 126 84'%3E%3Cg fill-rule='evenodd'%3E%3Cg fill='%23333333' fill-opacity='0.4'%3E%3Cpath d='M126 83v1H0v-2h40V42H0v-2h40V0h2v40h40V0h2v40h40V0h2v83zm-2-1V42H84v40h40zM82 42H42v40h40V42zm-50-6a4 4 0 1 1 0-8 4 4 0 0 1 0 8zM8 12a4 4 0 1 1 0-8 4 4 0 0 1 0 8zm96 12a4 4 0 1 1 0-8 4 4 0 0 1 0 8zm-42 0a4 4 0 1 1 0-8 4 4 0 0 1 0 8zm30-12a4 4 0 1 1 0-8 4 4 0 0 1 0 8zM20 54a4 4 0 1 1 0-8 4 4 0 0 1 0 8zm12 24a4 4 0 1 1 0-8 4 4 0 0 1 0 8zM8 54a4 4 0 1 1 0-8 4 4 0 0 1 0 8zm24 0a4 4 0 1 1 0-8 4 4 0 0 1 0 8zM8 78a4 4 0 1 1 0-8 4 4 0 0 1 0 8zm12 0a4 4 0 1 1 0-8 4 4 0 0 1 0 8zm54 0a4 4 0 1 1 0-8 4 4 0 0 1 0 8zM50 54a4 4 0 1 1 0-8 4 4 0 0 1 0 8zm24 0a4 4 0 1 1 0-8 4 4 0 0 1 0 8zM50 78a4 4 0 1 1 0-8 4 4 0 0 1 0 8zm54-12a4 4 0 1 1 0-8 4 4 0 0 1 0 8zm12 12a4 4 0 1 1 0-8 4 4 0 0 1 0 8zM92 54a4 4 0 1 1 0-8 4 4 0 0 1 0 8zm24 0a4 4 0 1 1 0-8 4 4 0 0 1 0 8zM92 78a4 4 0 1 1 0-8 4 4 0 0 1 0 8zm24-42a4 4 0 1 1 0-8 4 4 0 0 1 0 8z'/%3E%3C/g%3E%3C/g%3E%3C/svg%3E\");\r\n  }\r\n.pie {\r\n      background-color: #faaca8;\r\n      background-image: url(\"data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='50' height='50' viewBox='0 0 60 60'%3E%3Cg fill-rule='evenodd'%3E%3Cg fill='%23f55b53' fill-opacity='0.4' fill-rule='nonzero'%3E%3Cpath d='M29 58.58l7.38-7.39A30.95 30.95 0 0 1 29 37.84a30.95 30.95 0 0 1-7.38 13.36l7.37 7.38zm1.4 1.41l.01.01h-2.84l-7.37-7.38A30.95 30.95 0 0 1 6.84 60H0v-1.02a28.9 28.9 0 0 0 18.79-7.78L0 32.41v-4.84L18.78 8.79A28.9 28.9 0 0 0 0 1.02V0h6.84a30.95 30.95 0 0 1 13.35 7.38L27.57 0h2.84l7.39 7.38A30.95 30.95 0 0 1 51.16 0H60v27.58-.01V60h-8.84a30.95 30.95 0 0 1-13.37-7.4L30.4 60zM29 1.41l-7.4 7.38A30.95 30.95 0 0 1 29 22.16 30.95 30.95 0 0 1 36.38 8.8L29 1.4zM58 1A28.9 28.9 0 0 0 39.2 8.8L58 27.58V1.02zm-20.2 9.2A28.9 28.9 0 0 0 30.02 29h26.56L37.8 10.21zM30.02 31a28.9 28.9 0 0 0 7.77 18.79l18.79-18.79H30.02zm9.18 20.2A28.9 28.9 0 0 0 58 59V32.4L39.2 51.19zm-19-1.4a28.9 28.9 0 0 0 7.78-18.8H1.41l18.8 18.8zm7.78-20.8A28.9 28.9 0 0 0 20.2 10.2L1.41 29h26.57z'/%3E%3C/g%3E%3C/g%3E%3C/svg%3E\");\r\n  }\r\n.bees {\r\n      background-color: #fcc846;\r\n      background-image: url(\"data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='28' height='49' viewBox='0 0 28 49'%3E%3Cg fill-rule='evenodd'%3E%3Cg id='hexagons' fill='%237a4948' fill-opacity='0.83' fill-rule='nonzero'%3E%3Cpath d='M13.99 9.25l13 7.5v15l-13 7.5L1 31.75v-15l12.99-7.5zM3 17.9v12.7l10.99 6.34 11-6.35V17.9l-11-6.34L3 17.9zM0 15l12.98-7.5V0h-2v6.35L0 12.69v2.3zm0 18.5L12.98 41v8h-2v-6.85L0 35.81v-2.3zM15 0v7.5L27.99 15H28v-2.31h-.01L17 6.35V0h-2zm0 49v-8l12.99-7.5H28v2.31h-.01L17 42.15V49h-2z'/%3E%3C/g%3E%3C/g%3E%3C/svg%3E\");\r\n  }\r\n.food {\r\n      background-color: #ffcb05;\r\n      background-image: url(\"data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='120' height='120' viewBox='0 0 260 260'%3E%3Cg fill-rule='evenodd'%3E%3Cg fill='%23ec008c' fill-opacity='0.83'%3E%3Cpath d='M24.37 16c.2.65.39 1.32.54 2H21.17l1.17 2.34.45.9-.24.11V28a5 5 0 0 1-2.23 8.94l-.02.06a8 8 0 0 1-7.75 6h-20a8 8 0 0 1-7.74-6l-.02-.06A5 5 0 0 1-17.45 28v-6.76l-.79-1.58-.44-.9.9-.44.63-.32H-20a23.01 23.01 0 0 1 44.37-2zm-36.82 2a1 1 0 0 0-.44.1l-3.1 1.56.89 1.79 1.31-.66a3 3 0 0 1 2.69 0l2.2 1.1a1 1 0 0 0 .9 0l2.21-1.1a3 3 0 0 1 2.69 0l2.2 1.1a1 1 0 0 0 .9 0l2.21-1.1a3 3 0 0 1 2.69 0l2.2 1.1a1 1 0 0 0 .86.02l2.88-1.27a3 3 0 0 1 2.43 0l2.88 1.27a1 1 0 0 0 .85-.02l3.1-1.55-.89-1.79-1.42.71a3 3 0 0 1-2.56.06l-2.77-1.23a1 1 0 0 0-.4-.09h-.01a1 1 0 0 0-.4.09l-2.78 1.23a3 3 0 0 1-2.56-.06l-2.3-1.15a1 1 0 0 0-.45-.11h-.01a1 1 0 0 0-.44.1L.9 19.22a3 3 0 0 1-2.69 0l-2.2-1.1a1 1 0 0 0-.45-.11h-.01a1 1 0 0 0-.44.1l-2.21 1.11a3 3 0 0 1-2.69 0l-2.2-1.1a1 1 0 0 0-.45-.11h-.01zm0-2h-4.9a21.01 21.01 0 0 1 39.61 0h-2.09l-.06-.13-.26.13h-32.31zm30.35 7.68l1.36-.68h1.3v2h-36v-1.15l.34-.17 1.36-.68h2.59l1.36.68a3 3 0 0 0 2.69 0l1.36-.68h2.59l1.36.68a3 3 0 0 0 2.69 0L2.26 23h2.59l1.36.68a3 3 0 0 0 2.56.06l1.67-.74h3.23l1.67.74a3 3 0 0 0 2.56-.06zM-13.82 27l16.37 4.91L18.93 27h-32.75zm-.63 2h.34l16.66 5 16.67-5h.33a3 3 0 1 1 0 6h-34a3 3 0 1 1 0-6zm1.35 8a6 6 0 0 0 5.65 4h20a6 6 0 0 0 5.66-4H-13.1z'/%3E%3Cpath id='path6_fill-copy' d='M284.37 16c.2.65.39 1.32.54 2H281.17l1.17 2.34.45.9-.24.11V28a5 5 0 0 1-2.23 8.94l-.02.06a8 8 0 0 1-7.75 6h-20a8 8 0 0 1-7.74-6l-.02-.06a5 5 0 0 1-2.24-8.94v-6.76l-.79-1.58-.44-.9.9-.44.63-.32H240a23.01 23.01 0 0 1 44.37-2zm-36.82 2a1 1 0 0 0-.44.1l-3.1 1.56.89 1.79 1.31-.66a3 3 0 0 1 2.69 0l2.2 1.1a1 1 0 0 0 .9 0l2.21-1.1a3 3 0 0 1 2.69 0l2.2 1.1a1 1 0 0 0 .9 0l2.21-1.1a3 3 0 0 1 2.69 0l2.2 1.1a1 1 0 0 0 .86.02l2.88-1.27a3 3 0 0 1 2.43 0l2.88 1.27a1 1 0 0 0 .85-.02l3.1-1.55-.89-1.79-1.42.71a3 3 0 0 1-2.56.06l-2.77-1.23a1 1 0 0 0-.4-.09h-.01a1 1 0 0 0-.4.09l-2.78 1.23a3 3 0 0 1-2.56-.06l-2.3-1.15a1 1 0 0 0-.45-.11h-.01a1 1 0 0 0-.44.1l-2.21 1.11a3 3 0 0 1-2.69 0l-2.2-1.1a1 1 0 0 0-.45-.11h-.01a1 1 0 0 0-.44.1l-2.21 1.11a3 3 0 0 1-2.69 0l-2.2-1.1a1 1 0 0 0-.45-.11h-.01zm0-2h-4.9a21.01 21.01 0 0 1 39.61 0h-2.09l-.06-.13-.26.13h-32.31zm30.35 7.68l1.36-.68h1.3v2h-36v-1.15l.34-.17 1.36-.68h2.59l1.36.68a3 3 0 0 0 2.69 0l1.36-.68h2.59l1.36.68a3 3 0 0 0 2.69 0l1.36-.68h2.59l1.36.68a3 3 0 0 0 2.56.06l1.67-.74h3.23l1.67.74a3 3 0 0 0 2.56-.06zM246.18 27l16.37 4.91L278.93 27h-32.75zm-.63 2h.34l16.66 5 16.67-5h.33a3 3 0 1 1 0 6h-34a3 3 0 1 1 0-6zm1.35 8a6 6 0 0 0 5.65 4h20a6 6 0 0 0 5.66-4H246.9z'/%3E%3Cpath d='M159.5 21.02A9 9 0 0 0 151 15h-42a9 9 0 0 0-8.5 6.02 6 6 0 0 0 .02 11.96A8.99 8.99 0 0 0 109 45h42a9 9 0 0 0 8.48-12.02 6 6 0 0 0 .02-11.96zM151 17h-42a7 7 0 0 0-6.33 4h54.66a7 7 0 0 0-6.33-4zm-9.34 26a8.98 8.98 0 0 0 3.34-7h-2a7 7 0 0 1-7 7h-4.34a8.98 8.98 0 0 0 3.34-7h-2a7 7 0 0 1-7 7h-4.34a8.98 8.98 0 0 0 3.34-7h-2a7 7 0 0 1-7 7h-7a7 7 0 1 1 0-14h42a7 7 0 1 1 0 14h-9.34zM109 27a9 9 0 0 0-7.48 4H101a4 4 0 1 1 0-8h58a4 4 0 0 1 0 8h-.52a9 9 0 0 0-7.48-4h-42z'/%3E%3Cpath d='M39 115a8 8 0 1 0 0-16 8 8 0 0 0 0 16zm6-8a6 6 0 1 1-12 0 6 6 0 0 1 12 0zm-3-29v-2h8v-6H40a4 4 0 0 0-4 4v10H22l-1.33 4-.67 2h2.19L26 130h26l3.81-40H58l-.67-2L56 84H42v-6zm-4-4v10h2V74h8v-2h-8a2 2 0 0 0-2 2zm2 12h14.56l.67 2H22.77l.67-2H40zm13.8 4H24.2l3.62 38h22.36l3.62-38z'/%3E%3Cpath d='M129 92h-6v4h-6v4h-6v14h-3l.24 2 3.76 32h36l3.76-32 .24-2h-3v-14h-6v-4h-6v-4h-8zm18 22v-12h-4v4h3v8h1zm-3 0v-6h-4v6h4zm-6 6v-16h-4v19.17c1.6-.7 2.97-1.8 4-3.17zm-6 3.8V100h-4v23.8a10.04 10.04 0 0 0 4 0zm-6-.63V104h-4v16a10.04 10.04 0 0 0 4 3.17zm-6-9.17v-6h-4v6h4zm-6 0v-8h3v-4h-4v12h1zm27-12v-4h-4v4h3v4h1v-4zm-6 0v-8h-4v4h3v4h1zm-6-4v-4h-4v8h1v-4h3zm-6 4v-4h-4v8h1v-4h3zm7 24a12 12 0 0 0 11.83-10h7.92l-3.53 30h-32.44l-3.53-30h7.92A12 12 0 0 0 130 126z'/%3E%3Cpath d='M212 86v2h-4v-2h4zm4 0h-2v2h2v-2zm-20 0v.1a5 5 0 0 0-.56 9.65l.06.25 1.12 4.48a2 2 0 0 0 1.94 1.52h.01l7.02 24.55a2 2 0 0 0 1.92 1.45h4.98a2 2 0 0 0 1.92-1.45l7.02-24.55a2 2 0 0 0 1.95-1.52L224.5 96l.06-.25a5 5 0 0 0-.56-9.65V86a14 14 0 0 0-28 0zm4 0h6v2h-9a3 3 0 1 0 0 6H223a3 3 0 1 0 0-6H220v-2h2a12 12 0 1 0-24 0h2zm-1.44 14l-1-4h24.88l-1 4h-22.88zm8.95 26l-6.86-24h18.7l-6.86 24h-4.98zM150 242a22 22 0 1 0 0-44 22 22 0 0 0 0 44zm24-22a24 24 0 1 1-48 0 24 24 0 0 1 48 0zm-28.38 17.73l2.04-.87a6 6 0 0 1 4.68 0l2.04.87a2 2 0 0 0 2.5-.82l1.14-1.9a6 6 0 0 1 3.79-2.75l2.15-.5a2 2 0 0 0 1.54-2.12l-.19-2.2a6 6 0 0 1 1.45-4.46l1.45-1.67a2 2 0 0 0 0-2.62l-1.45-1.67a6 6 0 0 1-1.45-4.46l.2-2.2a2 2 0 0 0-1.55-2.13l-2.15-.5a6 6 0 0 1-3.8-2.75l-1.13-1.9a2 2 0 0 0-2.5-.8l-2.04.86a6 6 0 0 1-4.68 0l-2.04-.87a2 2 0 0 0-2.5.82l-1.14 1.9a6 6 0 0 1-3.79 2.75l-2.15.5a2 2 0 0 0-1.54 2.12l.19 2.2a6 6 0 0 1-1.45 4.46l-1.45 1.67a2 2 0 0 0 0 2.62l1.45 1.67a6 6 0 0 1 1.45 4.46l-.2 2.2a2 2 0 0 0 1.55 2.13l2.15.5a6 6 0 0 1 3.8 2.75l1.13 1.9a2 2 0 0 0 2.5.8zm2.82.97a4 4 0 0 1 3.12 0l2.04.87a4 4 0 0 0 4.99-1.62l1.14-1.9a4 4 0 0 1 2.53-1.84l2.15-.5a4 4 0 0 0 3.09-4.24l-.2-2.2a4 4 0 0 1 .97-2.98l1.45-1.67a4 4 0 0 0 0-5.24l-1.45-1.67a4 4 0 0 1-.97-2.97l.2-2.2a4 4 0 0 0-3.09-4.25l-2.15-.5a4 4 0 0 1-2.53-1.84l-1.14-1.9a4 4 0 0 0-5-1.62l-2.03.87a4 4 0 0 1-3.12 0l-2.04-.87a4 4 0 0 0-4.99 1.62l-1.14 1.9a4 4 0 0 1-2.53 1.84l-2.15.5a4 4 0 0 0-3.09 4.24l.2 2.2a4 4 0 0 1-.97 2.98l-1.45 1.67a4 4 0 0 0 0 5.24l1.45 1.67a4 4 0 0 1 .97 2.97l-.2 2.2a4 4 0 0 0 3.09 4.25l2.15.5a4 4 0 0 1 2.53 1.84l1.14 1.9a4 4 0 0 0 5 1.62l2.03-.87zM152 207a1 1 0 1 1 2 0 1 1 0 0 1-2 0zm6 2a1 1 0 1 1 2 0 1 1 0 0 1-2 0zm-11 1a1 1 0 1 1 2 0 1 1 0 0 1-2 0zm-6 0a1 1 0 1 1 2 0 1 1 0 0 1-2 0zm3-5a1 1 0 1 1 2 0 1 1 0 0 1-2 0zm-8 8a1 1 0 1 1 2 0 1 1 0 0 1-2 0zm3 6a1 1 0 1 1 2 0 1 1 0 0 1-2 0zm0 6a1 1 0 1 1 2 0 1 1 0 0 1-2 0zm4 7a1 1 0 1 1 2 0 1 1 0 0 1-2 0zm5-2a1 1 0 1 1 2 0 1 1 0 0 1-2 0zm5 4a1 1 0 1 1 2 0 1 1 0 0 1-2 0zm4-6a1 1 0 1 1 2 0 1 1 0 0 1-2 0zm6-4a1 1 0 1 1 2 0 1 1 0 0 1-2 0zm-4-3a1 1 0 1 1 2 0 1 1 0 0 1-2 0zm4-3a1 1 0 1 1 2 0 1 1 0 0 1-2 0zm-5-4a1 1 0 1 1 2 0 1 1 0 0 1-2 0zm-24 6a1 1 0 1 1 2 0 1 1 0 0 1-2 0zm16 5a5 5 0 1 0 0-10 5 5 0 0 0 0 10zm7-5a7 7 0 1 1-14 0 7 7 0 0 1 14 0zm86-29a1 1 0 0 0 0 2h2a1 1 0 0 0 0-2h-2zm19 9a1 1 0 0 1 1-1h2a1 1 0 0 1 0 2h-2a1 1 0 0 1-1-1zm-14 5a1 1 0 0 0 0 2h2a1 1 0 0 0 0-2h-2zm-25 1a1 1 0 0 0 0 2h2a1 1 0 0 0 0-2h-2zm5 4a1 1 0 0 0 0 2h2a1 1 0 0 0 0-2h-2zm9 0a1 1 0 0 1 1-1h2a1 1 0 0 1 0 2h-2a1 1 0 0 1-1-1zm15 1a1 1 0 0 1 1-1h2a1 1 0 0 1 0 2h-2a1 1 0 0 1-1-1zm12-2a1 1 0 0 0 0 2h2a1 1 0 0 0 0-2h-2zm-11-14a1 1 0 0 1 1-1h2a1 1 0 0 1 0 2h-2a1 1 0 0 1-1-1zm-19 0a1 1 0 0 0 0 2h2a1 1 0 0 0 0-2h-2zm6 5a1 1 0 0 1 1-1h2a1 1 0 0 1 0 2h-2a1 1 0 0 1-1-1zm-25 15c0-.47.01-.94.03-1.4a5 5 0 0 1-1.7-8 3.99 3.99 0 0 1 1.88-5.18 5 5 0 0 1 3.4-6.22 3 3 0 0 1 1.46-1.05 5 5 0 0 1 7.76-3.27A30.86 30.86 0 0 1 246 184c6.79 0 13.06 2.18 18.17 5.88a5 5 0 0 1 7.76 3.27 3 3 0 0 1 1.47 1.05 5 5 0 0 1 3.4 6.22 4 4 0 0 1 1.87 5.18 4.98 4.98 0 0 1-1.7 8c.02.46.03.93.03 1.4v1h-62v-1zm.83-7.17a30.9 30.9 0 0 0-.62 3.57 3 3 0 0 1-.61-4.2c.37.28.78.49 1.23.63zm1.49-4.61c-.36.87-.68 1.76-.96 2.68a2 2 0 0 1-.21-3.71c.33.4.73.75 1.17 1.03zm2.32-4.54c-.54.86-1.03 1.76-1.49 2.68a3 3 0 0 1-.07-4.67 3 3 0 0 0 1.56 1.99zm1.14-1.7c.35-.5.72-.98 1.1-1.46a1 1 0 1 0-1.1 1.45zm5.34-5.77c-1.03.86-2 1.79-2.9 2.77a3 3 0 0 0-1.11-.77 3 3 0 0 1 4-2zm42.66 2.77c-.9-.98-1.87-1.9-2.9-2.77a3 3 0 0 1 4.01 2 3 3 0 0 0-1.1.77zm1.34 1.54c.38.48.75.96 1.1 1.45a1 1 0 1 0-1.1-1.45zm3.73 5.84c-.46-.92-.95-1.82-1.5-2.68a3 3 0 0 0 1.57-1.99 3 3 0 0 1-.07 4.67zm1.8 4.53c-.29-.9-.6-1.8-.97-2.67.44-.28.84-.63 1.17-1.03a2 2 0 0 1-.2 3.7zm1.14 5.51c-.14-1.21-.35-2.4-.62-3.57.45-.14.86-.35 1.23-.63a2.99 2.99 0 0 1-.6 4.2zM275 214a29 29 0 0 0-57.97 0h57.96zM72.33 198.12c-.21-.32-.34-.7-.34-1.12v-12h-2v12a4.01 4.01 0 0 0 7.09 2.54c.57-.69.91-1.57.91-2.54v-12h-2v12a1.99 1.99 0 0 1-2 2 2 2 0 0 1-1.66-.88zM75 176c.38 0 .74-.04 1.1-.12a4 4 0 0 0 6.19 2.4A13.94 13.94 0 0 1 84 185v24a6 6 0 0 1-6 6h-3v9a5 5 0 1 1-10 0v-9h-3a6 6 0 0 1-6-6v-24a14 14 0 0 1 14-14 5 5 0 0 0 5 5zm-17 15v12a1.99 1.99 0 0 0 1.22 1.84 2 2 0 0 0 2.44-.72c.21-.32.34-.7.34-1.12v-12h2v12a3.98 3.98 0 0 1-5.35 3.77 3.98 3.98 0 0 1-.65-.3V209a4 4 0 0 0 4 4h16a4 4 0 0 0 4-4v-24c.01-1.53-.23-2.88-.72-4.17-.43.1-.87.16-1.28.17a6 6 0 0 1-5.2-3 7 7 0 0 1-6.47-4.88A12 12 0 0 0 58 185v6zm9 24v9a3 3 0 1 0 6 0v-9h-6z'/%3E%3Cpath d='M-17 191a1 1 0 0 0 0 2h2a1 1 0 0 0 0-2h-2zm19 9a1 1 0 0 1 1-1h2a1 1 0 0 1 0 2H3a1 1 0 0 1-1-1zm-14 5a1 1 0 0 0 0 2h2a1 1 0 0 0 0-2h-2zm-25 1a1 1 0 0 0 0 2h2a1 1 0 0 0 0-2h-2zm5 4a1 1 0 0 0 0 2h2a1 1 0 0 0 0-2h-2zm9 0a1 1 0 0 1 1-1h2a1 1 0 0 1 0 2h-2a1 1 0 0 1-1-1zm15 1a1 1 0 0 1 1-1h2a1 1 0 0 1 0 2h-2a1 1 0 0 1-1-1zm12-2a1 1 0 0 0 0 2h2a1 1 0 0 0 0-2H4zm-11-14a1 1 0 0 1 1-1h2a1 1 0 0 1 0 2h-2a1 1 0 0 1-1-1zm-19 0a1 1 0 0 0 0 2h2a1 1 0 0 0 0-2h-2zm6 5a1 1 0 0 1 1-1h2a1 1 0 0 1 0 2h-2a1 1 0 0 1-1-1zm-25 15c0-.47.01-.94.03-1.4a5 5 0 0 1-1.7-8 3.99 3.99 0 0 1 1.88-5.18 5 5 0 0 1 3.4-6.22 3 3 0 0 1 1.46-1.05 5 5 0 0 1 7.76-3.27A30.86 30.86 0 0 1-14 184c6.79 0 13.06 2.18 18.17 5.88a5 5 0 0 1 7.76 3.27 3 3 0 0 1 1.47 1.05 5 5 0 0 1 3.4 6.22 4 4 0 0 1 1.87 5.18 4.98 4.98 0 0 1-1.7 8c.02.46.03.93.03 1.4v1h-62v-1zm.83-7.17a30.9 30.9 0 0 0-.62 3.57 3 3 0 0 1-.61-4.2c.37.28.78.49 1.23.63zm1.49-4.61c-.36.87-.68 1.76-.96 2.68a2 2 0 0 1-.21-3.71c.33.4.73.75 1.17 1.03zm2.32-4.54c-.54.86-1.03 1.76-1.49 2.68a3 3 0 0 1-.07-4.67 3 3 0 0 0 1.56 1.99zm1.14-1.7c.35-.5.72-.98 1.1-1.46a1 1 0 1 0-1.1 1.45zm5.34-5.77c-1.03.86-2 1.79-2.9 2.77a3 3 0 0 0-1.11-.77 3 3 0 0 1 4-2zm42.66 2.77c-.9-.98-1.87-1.9-2.9-2.77a3 3 0 0 1 4.01 2 3 3 0 0 0-1.1.77zm1.34 1.54c.38.48.75.96 1.1 1.45a1 1 0 1 0-1.1-1.45zm3.73 5.84c-.46-.92-.95-1.82-1.5-2.68a3 3 0 0 0 1.57-1.99 3 3 0 0 1-.07 4.67zm1.8 4.53c-.29-.9-.6-1.8-.97-2.67.44-.28.84-.63 1.17-1.03a2 2 0 0 1-.2 3.7zm1.14 5.51c-.14-1.21-.35-2.4-.62-3.57.45-.14.86-.35 1.23-.63a2.99 2.99 0 0 1-.6 4.2zM15 214a29 29 0 0 0-57.97 0h57.96z'/%3E%3C/g%3E%3C/g%3E%3C/svg%3E\");\r\n  }\r\n.floor {\r\n      background-color: #00b9f2;\r\n      background-image: url(\"data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='20' height='20' viewBox='0 0 80 80'%3E%3Cg fill='%2392278f' fill-opacity='0.71'%3E%3Cpath fill-rule='evenodd' d='M0 0h40v40H0V0zm40 40h40v40H40V40zm0-40h2l-2 2V0zm0 4l4-4h2l-6 6V4zm0 4l8-8h2L40 10V8zm0 4L52 0h2L40 14v-2zm0 4L56 0h2L40 18v-2zm0 4L60 0h2L40 22v-2zm0 4L64 0h2L40 26v-2zm0 4L68 0h2L40 30v-2zm0 4L72 0h2L40 34v-2zm0 4L76 0h2L40 38v-2zm0 4L80 0v2L42 40h-2zm4 0L80 4v2L46 40h-2zm4 0L80 8v2L50 40h-2zm4 0l28-28v2L54 40h-2zm4 0l24-24v2L58 40h-2zm4 0l20-20v2L62 40h-2zm4 0l16-16v2L66 40h-2zm4 0l12-12v2L70 40h-2zm4 0l8-8v2l-6 6h-2zm4 0l4-4v2l-2 2h-2z'/%3E%3C/g%3E%3C/svg%3E\");\r\n  }\r\n.wiggle {\r\n      background-color: #dbbef9;\r\n      background-image: url(\"data:image/svg+xml,%3Csvg width='52' height='26' viewBox='0 0 52 26' xmlns='http://www.w3.org/2000/svg'%3E%3Cg fill='none' fill-rule='evenodd'%3E%3Cg fill='%23ff7c7c' fill-opacity='1'%3E%3Cpath d='M10 10c0-2.21-1.79-4-4-4-3.314 0-6-2.686-6-6h2c0 2.21 1.79 4 4 4 3.314 0 6 2.686 6 6 0 2.21 1.79 4 4 4 3.314 0 6 2.686 6 6 0 2.21 1.79 4 4 4v2c-3.314 0-6-2.686-6-6 0-2.21-1.79-4-4-4-3.314 0-6-2.686-6-6zm25.464-1.95l8.486 8.486-1.414 1.414-8.486-8.486 1.414-1.414z' /%3E%3C/g%3E%3C/g%3E%3C/svg%3E\");\r\n  }\r\n.bars {\r\n      background-color: #ffe67c;\r\n      background-image: url(\"data:image/svg+xml,%3Csvg width='40' height='40' viewBox='0 0 40 40' xmlns='http://www.w3.org/2000/svg'%3E%3Cg fill='%238fe1e7' fill-opacity='1' fill-rule='evenodd'%3E%3Cpath d='M0 40L40 0H20L0 20M40 40V20L20 40'/%3E%3C/g%3E%3C/svg%3E\");\r\n  }\r\n.bubbles {\r\n      background-color: #beffc2;\r\n      background-image: url(\"data:image/svg+xml,%3Csvg width='100' height='100' viewBox='0 0 100 100' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M11 18c3.866 0 7-3.134 7-7s-3.134-7-7-7-7 3.134-7 7 3.134 7 7 7zm48 25c3.866 0 7-3.134 7-7s-3.134-7-7-7-7 3.134-7 7 3.134 7 7 7zm-43-7c1.657 0 3-1.343 3-3s-1.343-3-3-3-3 1.343-3 3 1.343 3 3 3zm63 31c1.657 0 3-1.343 3-3s-1.343-3-3-3-3 1.343-3 3 1.343 3 3 3zM34 90c1.657 0 3-1.343 3-3s-1.343-3-3-3-3 1.343-3 3 1.343 3 3 3zm56-76c1.657 0 3-1.343 3-3s-1.343-3-3-3-3 1.343-3 3 1.343 3 3 3zM12 86c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm28-65c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm23-11c2.76 0 5-2.24 5-5s-2.24-5-5-5-5 2.24-5 5 2.24 5 5 5zm-6 60c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm29 22c2.76 0 5-2.24 5-5s-2.24-5-5-5-5 2.24-5 5 2.24 5 5 5zM32 63c2.76 0 5-2.24 5-5s-2.24-5-5-5-5 2.24-5 5 2.24 5 5 5zm57-13c2.76 0 5-2.24 5-5s-2.24-5-5-5-5 2.24-5 5 2.24 5 5 5zm-9-21c1.105 0 2-.895 2-2s-.895-2-2-2-2 .895-2 2 .895 2 2 2zM60 91c1.105 0 2-.895 2-2s-.895-2-2-2-2 .895-2 2 .895 2 2 2zM35 41c1.105 0 2-.895 2-2s-.895-2-2-2-2 .895-2 2 .895 2 2 2zM12 60c1.105 0 2-.895 2-2s-.895-2-2-2-2 .895-2 2 .895 2 2 2z' fill='%23e6afff' fill-opacity='1' fill-rule='evenodd'/%3E%3C/svg%3E\");\r\n  }\r\n.ticTac {\r\n      background-color: #ffefaa;\r\n      background-image: url(\"data:image/svg+xml,%3Csvg width='76' height='76' viewBox='0 0 64 64' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M8 16c4.418 0 8-3.582 8-8s-3.582-8-8-8-8 3.582-8 8 3.582 8 8 8zm0-2c3.314 0 6-2.686 6-6s-2.686-6-6-6-6 2.686-6 6 2.686 6 6 6zm33.414-6l5.95-5.95L45.95.636 40 6.586 34.05.636 32.636 2.05 38.586 8l-5.95 5.95 1.414 1.414L40 9.414l5.95 5.95 1.414-1.414L41.414 8zM40 48c4.418 0 8-3.582 8-8s-3.582-8-8-8-8 3.582-8 8 3.582 8 8 8zm0-2c3.314 0 6-2.686 6-6s-2.686-6-6-6-6 2.686-6 6 2.686 6 6 6zM9.414 40l5.95-5.95-1.414-1.414L8 38.586l-5.95-5.95L.636 34.05 6.586 40l-5.95 5.95 1.414 1.414L8 41.414l5.95 5.95 1.414-1.414L9.414 40z' fill='%23ffadad' fill-opacity='0.84' fill-rule='evenodd'/%3E%3C/svg%3E\");\r\n  }\r\n.zigZag {\r\n      background-color: #00dac3;\r\n      background-image: url(\"data:image/svg+xml,%3Csvg width='40' height='12' viewBox='0 0 40 12' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M0 6.172L6.172 0h5.656L0 11.828V6.172zm40 5.656L28.172 0h5.656L40 6.172v5.656zM6.172 12l12-12h3.656l12 12h-5.656L20 3.828 11.828 12H6.172zm12 0L20 10.172 21.828 12h-3.656z' fill='%23008386' fill-opacity='0.7' fill-rule='evenodd'/%3E%3C/svg%3E\");\r\n  }\r\n.stripes {\r\n      background-color: #ffffff;\r\n      background-image: url(\"data:image/svg+xml,%3Csvg width='25' height='1' viewBox='0 0 40 1' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M0 0h20v1H0z' fill='%23d09af3' fill-opacity='0.54' fill-rule='evenodd'/%3E%3C/svg%3E\");\r\n  }\r\n.clouds {\r\n      background-color: #959bb5;\r\n      background-image: url(\"data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 56 28' width='56' height='28'%3E%3Cpath fill='%23f0d519' fill-opacity='0.89' d='M56 26v2h-7.75c2.3-1.27 4.94-2 7.75-2zm-26 2a2 2 0 1 0-4 0h-4.09A25.98 25.98 0 0 0 0 16v-2c.67 0 1.34.02 2 .07V14a2 2 0 0 0-2-2v-2a4 4 0 0 1 3.98 3.6 28.09 28.09 0 0 1 2.8-3.86A8 8 0 0 0 0 6V4a9.99 9.99 0 0 1 8.17 4.23c.94-.95 1.96-1.83 3.03-2.63A13.98 13.98 0 0 0 0 0h7.75c2 1.1 3.73 2.63 5.1 4.45 1.12-.72 2.3-1.37 3.53-1.93A20.1 20.1 0 0 0 14.28 0h2.7c.45.56.88 1.14 1.29 1.74 1.3-.48 2.63-.87 4-1.15-.11-.2-.23-.4-.36-.59H26v.07a28.4 28.4 0 0 1 4 0V0h4.09l-.37.59c1.38.28 2.72.67 4.01 1.15.4-.6.84-1.18 1.3-1.74h2.69a20.1 20.1 0 0 0-2.1 2.52c1.23.56 2.41 1.2 3.54 1.93A16.08 16.08 0 0 1 48.25 0H56c-4.58 0-8.65 2.2-11.2 5.6 1.07.8 2.09 1.68 3.03 2.63A9.99 9.99 0 0 1 56 4v2a8 8 0 0 0-6.77 3.74c1.03 1.2 1.97 2.5 2.79 3.86A4 4 0 0 1 56 10v2a2 2 0 0 0-2 2.07 28.4 28.4 0 0 1 2-.07v2c-9.2 0-17.3 4.78-21.91 12H30zM7.75 28H0v-2c2.81 0 5.46.73 7.75 2zM56 20v2c-5.6 0-10.65 2.3-14.28 6h-2.7c4.04-4.89 10.15-8 16.98-8zm-39.03 8h-2.69C10.65 24.3 5.6 22 0 22v-2c6.83 0 12.94 3.11 16.97 8zm15.01-.4a28.09 28.09 0 0 1 2.8-3.86 8 8 0 0 0-13.55 0c1.03 1.2 1.97 2.5 2.79 3.86a4 4 0 0 1 7.96 0zm14.29-11.86c1.3-.48 2.63-.87 4-1.15a25.99 25.99 0 0 0-44.55 0c1.38.28 2.72.67 4.01 1.15a21.98 21.98 0 0 1 36.54 0zm-5.43 2.71c1.13-.72 2.3-1.37 3.54-1.93a19.98 19.98 0 0 0-32.76 0c1.23.56 2.41 1.2 3.54 1.93a15.98 15.98 0 0 1 25.68 0zm-4.67 3.78c.94-.95 1.96-1.83 3.03-2.63a13.98 13.98 0 0 0-22.4 0c1.07.8 2.09 1.68 3.03 2.63a9.99 9.99 0 0 1 16.34 0z'%3E%3C/path%3E%3C/svg%3E\");\r\n  }\r\n.aztec {\r\n      background-color: #d59242;\r\n      background-image: url(\"data:image/svg+xml,%3Csvg width='100' height='20' viewBox='0 0 100 20' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M21.184 20c.357-.13.72-.264 1.088-.402l1.768-.661C33.64 15.347 39.647 14 50 14c10.271 0 15.362 1.222 24.629 4.928.955.383 1.869.74 2.75 1.072h6.225c-2.51-.73-5.139-1.691-8.233-2.928C65.888 13.278 60.562 12 50 12c-10.626 0-16.855 1.397-26.66 5.063l-1.767.662c-2.475.923-4.66 1.674-6.724 2.275h6.335zm0-20C13.258 2.892 8.077 4 0 4V2c5.744 0 9.951-.574 14.85-2h6.334zM77.38 0C85.239 2.966 90.502 4 100 4V2c-6.842 0-11.386-.542-16.396-2h-6.225zM0 14c8.44 0 13.718-1.21 22.272-4.402l1.768-.661C33.64 5.347 39.647 4 50 4c10.271 0 15.362 1.222 24.629 4.928C84.112 12.722 89.438 14 100 14v-2c-10.271 0-15.362-1.222-24.629-4.928C65.888 3.278 60.562 2 50 2 39.374 2 33.145 3.397 23.34 7.063l-1.767.662C13.223 10.84 8.163 12 0 12v2z' fill='%230d37c2' fill-opacity='0.4' fill-rule='evenodd'/%3E%3C/svg%3E\");\r\n  }\r\n.circuit {\r\n      background-color: #00b497;\r\n      background-image: url(\"data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 304 304' width='150' height='150'%3E%3Cpath fill='%23333333' fill-opacity='0.4' d='M44.1 224a5 5 0 1 1 0 2H0v-2h44.1zm160 48a5 5 0 1 1 0 2H82v-2h122.1zm57.8-46a5 5 0 1 1 0-2H304v2h-42.1zm0 16a5 5 0 1 1 0-2H304v2h-42.1zm6.2-114a5 5 0 1 1 0 2h-86.2a5 5 0 1 1 0-2h86.2zm-256-48a5 5 0 1 1 0 2H0v-2h12.1zm185.8 34a5 5 0 1 1 0-2h86.2a5 5 0 1 1 0 2h-86.2zM258 12.1a5 5 0 1 1-2 0V0h2v12.1zm-64 208a5 5 0 1 1-2 0v-54.2a5 5 0 1 1 2 0v54.2zm48-198.2V80h62v2h-64V21.9a5 5 0 1 1 2 0zm16 16V64h46v2h-48V37.9a5 5 0 1 1 2 0zm-128 96V208h16v12.1a5 5 0 1 1-2 0V210h-16v-76.1a5 5 0 1 1 2 0zm-5.9-21.9a5 5 0 1 1 0 2H114v48H85.9a5 5 0 1 1 0-2H112v-48h12.1zm-6.2 130a5 5 0 1 1 0-2H176v-74.1a5 5 0 1 1 2 0V242h-60.1zm-16-64a5 5 0 1 1 0-2H114v48h10.1a5 5 0 1 1 0 2H112v-48h-10.1zM66 284.1a5 5 0 1 1-2 0V274H50v30h-2v-32h18v12.1zM236.1 176a5 5 0 1 1 0 2H226v94h48v32h-2v-30h-48v-98h12.1zm25.8-30a5 5 0 1 1 0-2H274v44.1a5 5 0 1 1-2 0V146h-10.1zm-64 96a5 5 0 1 1 0-2H208v-80h16v-14h-42.1a5 5 0 1 1 0-2H226v18h-16v80h-12.1zm86.2-210a5 5 0 1 1 0 2H272V0h2v32h10.1zM98 101.9V146H53.9a5 5 0 1 1 0-2H96v-42.1a5 5 0 1 1 2 0zM53.9 34a5 5 0 1 1 0-2H80V0h2v34H53.9zm60.1 3.9V66H82v64H69.9a5 5 0 1 1 0-2H80V64h32V37.9a5 5 0 1 1 2 0zM101.9 82a5 5 0 1 1 0-2H128V37.9a5 5 0 1 1 2 0V82h-28.1zm16-64a5 5 0 1 1 0-2H146v44.1a5 5 0 1 1-2 0V18h-26.1zm102.2 270a5 5 0 1 1 0 2H98v14h-2v-16h124.1zM242 149.9V160h16v34h-16v62h48v48h-2v-46h-48v-66h16v-30h-16v-12.1a5 5 0 1 1 2 0zM53.9 18a5 5 0 1 1 0-2H64V2H48V0h18v18H53.9zm112 32a5 5 0 1 1 0-2H192V0h50v2h-48v48h-28.1zm-48-48a5 5 0 0 1-9.8-2h2.07a3 3 0 1 0 5.66 0H178v34h-18V21.9a5 5 0 1 1 2 0V32h14V2h-58.1zm0 96a5 5 0 1 1 0-2H137l32-32h39V21.9a5 5 0 1 1 2 0V66h-40.17l-32 32H117.9zm28.1 90.1a5 5 0 1 1-2 0v-76.51L175.59 80H224V21.9a5 5 0 1 1 2 0V82h-49.59L146 112.41v75.69zm16 32a5 5 0 1 1-2 0v-99.51L184.59 96H300.1a5 5 0 0 1 3.9-3.9v2.07a3 3 0 0 0 0 5.66v2.07a5 5 0 0 1-3.9-3.9H185.41L162 121.41v98.69zm-144-64a5 5 0 1 1-2 0v-3.51l48-48V48h32V0h2v50H66v55.41l-48 48v2.69zM50 53.9v43.51l-48 48V208h26.1a5 5 0 1 1 0 2H0v-65.41l48-48V53.9a5 5 0 1 1 2 0zm-16 16V89.41l-34 34v-2.82l32-32V69.9a5 5 0 1 1 2 0zM12.1 32a5 5 0 1 1 0 2H9.41L0 43.41V40.6L8.59 32h3.51zm265.8 18a5 5 0 1 1 0-2h18.69l7.41-7.41v2.82L297.41 50H277.9zm-16 160a5 5 0 1 1 0-2H288v-71.41l16-16v2.82l-14 14V210h-28.1zm-208 32a5 5 0 1 1 0-2H64v-22.59L40.59 194H21.9a5 5 0 1 1 0-2H41.41L66 216.59V242H53.9zm150.2 14a5 5 0 1 1 0 2H96v-56.6L56.6 162H37.9a5 5 0 1 1 0-2h19.5L98 200.6V256h106.1zm-150.2 2a5 5 0 1 1 0-2H80v-46.59L48.59 178H21.9a5 5 0 1 1 0-2H49.41L82 208.59V258H53.9zM34 39.8v1.61L9.41 66H0v-2h8.59L32 40.59V0h2v39.8zM2 300.1a5 5 0 0 1 3.9 3.9H3.83A3 3 0 0 0 0 302.17V256h18v48h-2v-46H2v42.1zM34 241v63h-2v-62H0v-2h34v1zM17 18H0v-2h16V0h2v18h-1zm273-2h14v2h-16V0h2v16zm-32 273v15h-2v-14h-14v14h-2v-16h18v1zM0 92.1A5.02 5.02 0 0 1 6 97a5 5 0 0 1-6 4.9v-2.07a3 3 0 1 0 0-5.66V92.1zM80 272h2v32h-2v-32zm37.9 32h-2.07a3 3 0 0 0-5.66 0h-2.07a5 5 0 0 1 9.8 0zM5.9 0A5.02 5.02 0 0 1 0 5.9V3.83A3 3 0 0 0 3.83 0H5.9zm294.2 0h2.07A3 3 0 0 0 304 3.83V5.9a5 5 0 0 1-3.9-5.9zm3.9 300.1v2.07a3 3 0 0 0-1.83 1.83h-2.07a5 5 0 0 1 3.9-3.9zM97 100a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm0-16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16 16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16 16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm0 16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-48 32a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16 16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm32 48a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-16 16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm32-16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm0-32a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16 32a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm32 16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm0-16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-16-64a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16 96a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm0 16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16 16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16-144a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm0 32a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16-32a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16-16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-96 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm0 16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16-32a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm96 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-16-64a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16-16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-32 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm0-16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-16 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-16 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-16 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zM49 36a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-32 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm32 16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zM33 68a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16-48a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm0 240a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16 32a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-16-64a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm0 16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-16-32a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm80-176a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-16-16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm32 48a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16-16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm0-32a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm112 176a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-16 16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm0 16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm0 16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zM17 180a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm0 16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm0-32a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zM17 84a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm32 64a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16-16a3 3 0 1 0 0-6 3 3 0 0 0 0 6z'%3E%3C/path%3E%3C/svg%3E\");\r\n  }\r\n.dots {\r\n      background-color: #ffcb05;\r\n      background-image: url(\"data:image/svg+xml,%3Csvg width='16' height='16' viewBox='0 0 20 20' xmlns='http://www.w3.org/2000/svg'%3E%3Cg fill='%2300a99d' fill-opacity='0.71' fill-rule='evenodd'%3E%3Ccircle cx='3' cy='3' r='3'/%3E%3Ccircle cx='13' cy='13' r='3'/%3E%3C/g%3E%3C/svg%3E\");\r\n  }\r\n.lines {\r\n      background-color: #efefef;\r\n      background-image: url(\"data:image/svg+xml,%3Csvg width='80' height='80' viewBox='0 0 120 120' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M9 0h2v20H9V0zm25.134.84l1.732 1-10 17.32-1.732-1 10-17.32zm-20 20l1.732 1-10 17.32-1.732-1 10-17.32zM58.16 4.134l1 1.732-17.32 10-1-1.732 17.32-10zm-40 40l1 1.732-17.32 10-1-1.732 17.32-10zM80 9v2H60V9h20zM20 69v2H0v-2h20zm79.32-55l-1 1.732-17.32-10L82 4l17.32 10zm-80 80l-1 1.732-17.32-10L2 84l17.32 10zm96.546-75.84l-1.732 1-10-17.32 1.732-1 10 17.32zm-100 100l-1.732 1-10-17.32 1.732-1 10 17.32zM38.16 24.134l1 1.732-17.32 10-1-1.732 17.32-10zM60 29v2H40v-2h20zm19.32 5l-1 1.732-17.32-10L62 24l17.32 10zm16.546 4.16l-1.732 1-10-17.32 1.732-1 10 17.32zM111 40h-2V20h2v20zm3.134.84l1.732 1-10 17.32-1.732-1 10-17.32zM40 49v2H20v-2h20zm19.32 5l-1 1.732-17.32-10L42 44l17.32 10zm16.546 4.16l-1.732 1-10-17.32 1.732-1 10 17.32zM91 60h-2V40h2v20zm3.134.84l1.732 1-10 17.32-1.732-1 10-17.32zm24.026 3.294l1 1.732-17.32 10-1-1.732 17.32-10zM39.32 74l-1 1.732-17.32-10L22 64l17.32 10zm16.546 4.16l-1.732 1-10-17.32 1.732-1 10 17.32zM71 80h-2V60h2v20zm3.134.84l1.732 1-10 17.32-1.732-1 10-17.32zm24.026 3.294l1 1.732-17.32 10-1-1.732 17.32-10zM120 89v2h-20v-2h20zm-84.134 9.16l-1.732 1-10-17.32 1.732-1 10 17.32zM51 100h-2V80h2v20zm3.134.84l1.732 1-10 17.32-1.732-1 10-17.32zm24.026 3.294l1 1.732-17.32 10-1-1.732 17.32-10zM100 109v2H80v-2h20zm19.32 5l-1 1.732-17.32-10 1-1.732 17.32 10zM31 120h-2v-20h2v20z' fill='%23efb4a3' fill-opacity='0.84' fill-rule='evenodd'/%3E%3C/svg%3E\");\r\n  }\r\n.leaf {\r\n      background-color: #b5ccbf;\r\n      background-image: url(\"data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 80 40' width='60' height='30'%3E%3Cpath fill='%2340584a' fill-opacity='0.47' d='M0 40a19.96 19.96 0 0 1 5.9-14.11 20.17 20.17 0 0 1 19.44-5.2A20 20 0 0 1 20.2 40H0zM65.32.75A20.02 20.02 0 0 1 40.8 25.26 20.02 20.02 0 0 1 65.32.76zM.07 0h20.1l-.08.07A20.02 20.02 0 0 1 .75 5.25 20.08 20.08 0 0 1 .07 0zm1.94 40h2.53l4.26-4.24v-9.78A17.96 17.96 0 0 0 2 40zm5.38 0h9.8a17.98 17.98 0 0 0 6.67-16.42L7.4 40zm3.43-15.42v9.17l11.62-11.59c-3.97-.5-8.08.3-11.62 2.42zm32.86-.78A18 18 0 0 0 63.85 3.63L43.68 23.8zm7.2-19.17v9.15L62.43 2.22c-3.96-.5-8.05.3-11.57 2.4zm-3.49 2.72c-4.1 4.1-5.81 9.69-5.13 15.03l6.61-6.6V6.02c-.51.41-1 .85-1.48 1.33zM17.18 0H7.42L3.64 3.78A18 18 0 0 0 17.18 0zM2.08 0c-.01.8.04 1.58.14 2.37L4.59 0H2.07z'%3E%3C/path%3E%3C/svg%3E\");\r\n  }\r\n.overlap {\r\n      background-color: #ffadff;\r\n      background-image: url(\"data:image/svg+xml,%3Csvg width='60' height='60' viewBox='0 0 80 80' xmlns='http://www.w3.org/2000/svg'%3E%3Cg fill='none' fill-rule='evenodd'%3E%3Cg fill='%232e78ff' fill-opacity='0.82'%3E%3Cpath d='M50 50c0-5.523 4.477-10 10-10s10 4.477 10 10-4.477 10-10 10c0 5.523-4.477 10-10 10s-10-4.477-10-10 4.477-10 10-10zM10 10c0-5.523 4.477-10 10-10s10 4.477 10 10-4.477 10-10 10c0 5.523-4.477 10-10 10S0 25.523 0 20s4.477-10 10-10zm10 8c4.418 0 8-3.582 8-8s-3.582-8-8-8-8 3.582-8 8 3.582 8 8 8zm40 40c4.418 0 8-3.582 8-8s-3.582-8-8-8-8 3.582-8 8 3.582 8 8 8z' /%3E%3C/g%3E%3C/g%3E%3C/svg%3E\");\r\n  }\r\n@media screen and (max-width: 500px) {\r\n      .progress {\r\n          width: calc(100vw * 0.7);\r\n          /* height: calc(100vw * 0.7 * 0.145); */\r\n          height: 40px;\r\n          margin: 25px;\r\n      }\r\n  \r\n      .github {\r\n          margin: 20px;\r\n      }\r\n  \r\n      .desktop {\r\n          display: none;\r\n      }\r\n  \r\n      .mobile {\r\n          display: inherit;\r\n      }\r\n  }\r\n/* Indeterminate Classes and Animations */\r\n.indeterminate-16 {\r\n      -webkit-animation: indeterminate-16 0.25s linear infinite;\r\n              animation: indeterminate-16 0.25s linear infinite;\r\n  }\r\n.indeterminate-20 {\r\n      -webkit-animation: indeterminate-20 0.5s linear infinite;\r\n              animation: indeterminate-20 0.5s linear infinite;\r\n  }\r\n.indeterminate-25 {\r\n      -webkit-animation: indeterminate-25 0.5s linear infinite;\r\n              animation: indeterminate-25 0.5s linear infinite;\r\n  }\r\n.indeterminate-28 {\r\n      -webkit-animation: indeterminate-28 0.25s linear infinite;\r\n              animation: indeterminate-28 0.25s linear infinite;\r\n  }\r\n.indeterminate-40 {\r\n      -webkit-animation: indeterminate-40 0.5s linear infinite;\r\n              animation: indeterminate-40 0.5s linear infinite;\r\n  }\r\n.indeterminate-52 {\r\n      -webkit-animation: indeterminate-52 0.5s linear infinite;\r\n              animation: indeterminate-52 0.5s linear infinite;\r\n  }\r\n.indeterminate-56 {\r\n      -webkit-animation: indeterminate-56 0.5s linear infinite;\r\n              animation: indeterminate-56 0.5s linear infinite;\r\n  }\r\n.indeterminate-50 {\r\n      -webkit-animation: indeterminate-50 0.5s linear infinite;\r\n              animation: indeterminate-50 0.5s linear infinite;\r\n  }\r\n.indeterminate-60 {\r\n      -webkit-animation: indeterminate-60 0.5s linear infinite;\r\n              animation: indeterminate-60 0.5s linear infinite;\r\n  }\r\n.indeterminate-70 {\r\n      -webkit-animation: indeterminate-70 0.5s linear infinite;\r\n              animation: indeterminate-70 0.5s linear infinite;\r\n  }\r\n.indeterminate-76 {\r\n      -webkit-animation: indeterminate-76 0.5s linear infinite;\r\n              animation: indeterminate-76 0.5s linear infinite;\r\n  }\r\n.indeterminate-80 {\r\n      -webkit-animation: indeterminate-80 0.5s linear infinite;\r\n              animation: indeterminate-80 0.5s linear infinite;\r\n  }\r\n.indeterminate-100 {\r\n      -webkit-animation: indeterminate-100 1s linear infinite;\r\n              animation: indeterminate-100 1s linear infinite;\r\n  }\r\n.indeterminate-120 {\r\n      -webkit-animation: indeterminate-120 1s linear infinite;\r\n              animation: indeterminate-120 1s linear infinite;\r\n  }\r\n.indeterminate-126 {\r\n      -webkit-animation: indeterminate-126 1s linear infinite;\r\n              animation: indeterminate-126 1s linear infinite;\r\n  }\r\n.indeterminate-150 {\r\n      -webkit-animation: indeterminate-150 1s linear infinite;\r\n              animation: indeterminate-150 1s linear infinite;\r\n  }\r\n@-webkit-keyframes indeterminate-16 {\r\n      from {\r\n          background-position: 16px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@keyframes indeterminate-16 {\r\n      from {\r\n          background-position: 16px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@-webkit-keyframes indeterminate-20 {\r\n      from {\r\n          background-position: 20px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@keyframes indeterminate-20 {\r\n      from {\r\n          background-position: 20px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@-webkit-keyframes indeterminate-25 {\r\n      from {\r\n          background-position: 25px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@keyframes indeterminate-25 {\r\n      from {\r\n          background-position: 25px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@-webkit-keyframes indeterminate-28 {\r\n      from {\r\n          background-position: 28px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@keyframes indeterminate-28 {\r\n      from {\r\n          background-position: 28px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@-webkit-keyframes indeterminate-40 {\r\n      from {\r\n          background-position: 40px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@keyframes indeterminate-40 {\r\n      from {\r\n          background-position: 40px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@-webkit-keyframes indeterminate-50 {\r\n      from {\r\n          background-position: 50px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@keyframes indeterminate-50 {\r\n      from {\r\n          background-position: 50px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@-webkit-keyframes indeterminate-52 {\r\n      from {\r\n          background-position: 52px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@keyframes indeterminate-52 {\r\n      from {\r\n          background-position: 52px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@-webkit-keyframes indeterminate-56 {\r\n      from {\r\n          background-position: 56px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@keyframes indeterminate-56 {\r\n      from {\r\n          background-position: 56px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@-webkit-keyframes indeterminate-60 {\r\n      from {\r\n          background-position: 60px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@keyframes indeterminate-60 {\r\n      from {\r\n          background-position: 60px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@-webkit-keyframes indeterminate-70 {\r\n      from {\r\n          background-position: 70px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@keyframes indeterminate-70 {\r\n      from {\r\n          background-position: 70px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@-webkit-keyframes indeterminate-76 {\r\n      from {\r\n          background-position: 76px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@keyframes indeterminate-76 {\r\n      from {\r\n          background-position: 76px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@-webkit-keyframes indeterminate-80 {\r\n      from {\r\n          background-position: 80px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@keyframes indeterminate-80 {\r\n      from {\r\n          background-position: 80px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@-webkit-keyframes indeterminate-100 {\r\n      from {\r\n          background-position: 100px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@keyframes indeterminate-100 {\r\n      from {\r\n          background-position: 100px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@-webkit-keyframes indeterminate-120 {\r\n      from {\r\n          background-position: 120px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@keyframes indeterminate-120 {\r\n      from {\r\n          background-position: 120px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@-webkit-keyframes indeterminate-126 {\r\n      from {\r\n          background-position: 126px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@keyframes indeterminate-126 {\r\n      from {\r\n          background-position: 126px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@-webkit-keyframes indeterminate-150 {\r\n      from {\r\n          background-position: 150px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n@keyframes indeterminate-150 {\r\n      from {\r\n          background-position: 150px 0;\r\n      }\r\n      to {\r\n          background-position: 0 0;\r\n      }\r\n  }\r\n  \r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYW5hbHl0aWMvYW5hbHl0aWMuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxLQUFLLGdCQUFnQjtBQUNyQixvQkFBb0IsU0FBUyxDQUFDLFdBQVcsQ0FBQyxrQkFBa0IsQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLHNDQUE2QixDQUE3Qiw4QkFBOEI7QUFFOUgsY0FBYyxRQUFRLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsaUJBQWlCLENBQUMsZ0JBQWdCO0FBRWhHO0lBQ0ksMEJBQTBCO0lBQzFCLFlBQVk7SUFDWixZQUFZO0lBQ1oseUJBQXlCO0lBQ3pCLGtCQUFrQjtJQUNsQixxQkFBcUI7SUFDckIscUJBQXFCO0lBQ3JCLGVBQWU7SUFDZixnQkFBZ0I7SUFDaEIsZUFBZTtFQUNqQjtBQUdGO0dBQ0csMEJBQTBCO0dBQzFCLFlBQVk7R0FDWixZQUFZO0dBQ1oseUJBQXlCO0dBQ3pCLGtCQUFrQjtHQUNsQixxQkFBcUI7R0FDckIscUJBQXFCO0dBQ3JCLGVBQWU7R0FDZixnQkFBZ0I7R0FDaEIsZUFBZTtDQUNqQjtBQUNBO0dBQ0UsMEJBQTBCO0dBQzFCLFlBQVk7R0FDWixZQUFZO0dBQ1oseUJBQXlCO0dBQ3pCLGtCQUFrQjtHQUNsQixxQkFBcUI7R0FDckIscUJBQXFCO0dBQ3JCLGVBQWU7R0FDZixnQkFBZ0I7R0FDaEIsZUFBZTtDQUNqQjtBQUVBO0dBQ0UsMEJBQTBCO0dBQzFCLFlBQVk7R0FDWixZQUFZO0dBQ1oseUJBQXlCO0dBQ3pCLGtCQUFrQjtHQUNsQixxQkFBcUI7R0FDckIscUJBQXFCO0dBQ3JCLGVBQWU7R0FDZixnQkFBZ0I7R0FDaEIsZUFBZTtDQUNqQjtBQUVDO0tBQ0csa0JBQWtCO0tBQ2xCLGVBQWU7S0FDZixPQUFPO0tBQ1AsU0FBUztLQUNULFdBQVc7S0FDWCwyQkFBMkI7S0FDM0IsWUFBWTtLQUNaLGtCQUFrQjtFQUNyQjtBQUVBO0lBQ0Usa0JBQWtCO0lBQ2xCLHFCQUFxQjtFQUN2QjtBQUVBO0lBQ0UscUNBQTJEO0lBQzNELGVBQWU7SUFDZixXQUFXO0lBQ1gsWUFBWTtJQUNaLFlBQVk7RUFDZDtBQUNBO0lBQ0Usa0JBQWtCO0lBQ2xCLHFCQUFxQjtFQUN2QjtBQUVBO0lBQ0UseUNBQStEO0lBQy9ELGVBQWU7SUFDZixXQUFXO0lBQ1gsWUFBWTtJQUNaLFlBQVk7RUFDZDtBQUVBO0lBQ0Usa0JBQWtCO0lBQ2xCLHFCQUFxQjtFQUN2QjtBQUVBO0lBQ0UsbURBQTJFO0lBQzNFLGVBQWU7SUFDZixXQUFXO0lBQ1gsWUFBWTtJQUNaLFlBQVk7RUFDZDtBQUVBO0lBQ0Usa0JBQWtCO0lBQ2xCLHFCQUFxQjtFQUN2QjtBQUVBO0lBQ0UseUNBQStEO0lBQy9ELGVBQWU7SUFDZixXQUFXO0lBQ1gsWUFBWTtJQUNaLFlBQVk7RUFDZDtBQUNBO0lBQ0Usa0JBQWtCO0lBQ2xCLFNBQVM7SUFDVCxTQUFTO0VBQ1g7QUFFQTtJQUNFLGtCQUFrQjtJQUNsQixTQUFTO0lBQ1QsU0FBUztFQUNYO0FBRUE7SUFDRSxrQkFBa0I7SUFDbEIsU0FBUztJQUNULFNBQVM7RUFDWDtBQUVBO0lBQ0Usa0JBQWtCO0lBQ2xCLFNBQVM7SUFDVCxVQUFVO0VBQ1o7QUFHQTtJQUNFLGdCQUFnQjs7SUFFaEIsbUJBQW1CO0lBQ25CLFdBQVc7O0lBRVgsc0JBQXNCOztFQUV4QjtBQUVBO0lBQ0UsZ0JBQWdCOztJQUVoQixtQkFBbUI7SUFDbkIsaUJBQWlCO0lBQ2pCLFNBQVM7SUFDVCxZQUFZO0lBQ1osMEJBQTBCOztFQUU1QjtBQU9BO01BQ0ksMkJBQTJCO0VBQy9CO0FBRUE7TUFDSSx1QkFBdUI7TUFDdkIsa0JBQWtCO01BQ2xCLGdCQUFnQjtFQUNwQjtBQUVBO01BQ0ksbUJBQW1CO0VBQ3ZCO0FBRUE7TUFDSSxXQUFXO01BQ1gsd0JBQXdCO01BQ3hCLDRCQUE0QjtNQUM1QixXQUFXO0VBQ2Y7QUFFQTtNQUNJLFdBQVc7TUFDWCxvQkFBb0I7TUFDcEIsd0JBQXdCO01BQ3hCLDBCQUEwQjtNQUMxQixXQUFXO0VBQ2Y7QUFFQTtNQUNJLGtCQUFrQjtFQUN0QjtBQUVBO01BQ0ksWUFBWTtFQUNoQjtBQUVBO01BQ0kscUJBQXFCO01BQ3JCLFlBQVk7TUFDWixZQUFZO01BQ1osWUFBWTtNQUNaLG1CQUFtQjtNQUNuQixtQkFBbUI7RUFDdkI7QUFFQTtNQUNJLG1CQUFtQjtNQUNuQixTQUFTO01BQ1QsWUFBWTtNQUNaLGlCQUFpQjtNQUNqQix1QkFBdUI7TUFDdkIsZ0VBQWdFO0VBQ3BFO0FBRUE7TUFDSSxhQUFhO0VBQ2pCO0FBRUE7TUFDSSxVQUFVO01BQ1YsNkNBQTZDO0VBQ2pEO0FBRUE7TUFDSSx5QkFBeUI7TUFDekIsdWlCQUF1aUI7RUFDM2lCO0FBRUE7TUFDSSx5QkFBeUI7TUFDekIsMnhDQUEyeEM7RUFDL3hDO0FBRUE7TUFDSSx5QkFBeUI7TUFDekIsNHFCQUE0cUI7RUFDaHJCO0FBRUE7TUFDSSx5QkFBeUI7TUFDekIseWlDQUF5aUM7RUFDN2lDO0FBRUE7TUFDSSx5QkFBeUI7TUFDekIsMi9CQUEyL0I7RUFDLy9CO0FBRUE7TUFDSSx5QkFBeUI7TUFDekIsMmlCQUEyaUI7RUFDL2lCO0FBRUE7TUFDSSx5QkFBeUI7TUFDekIsMnRUQUEydFQ7RUFDL3RUO0FBRUE7TUFDSSx5QkFBeUI7TUFDekIsaXFCQUFpcUI7RUFDcnFCO0FBRUE7TUFDSSx5QkFBeUI7TUFDekIscWdCQUFxZ0I7RUFDemdCO0FBRUE7TUFDSSx5QkFBeUI7TUFDekIsbVFBQW1RO0VBQ3ZRO0FBRUE7TUFDSSx5QkFBeUI7TUFDekIsb3VDQUFvdUM7RUFDeHVDO0FBRUE7TUFDSSx5QkFBeUI7TUFDekIsNnRCQUE2dEI7RUFDanVCO0FBRUE7TUFDSSx5QkFBeUI7TUFDekIsZ1lBQWdZO0VBQ3BZO0FBRUE7TUFDSSx5QkFBeUI7TUFDekIsZ09BQWdPO0VBQ3BPO0FBRUE7TUFDSSx5QkFBeUI7TUFDekIscW1EQUFxbUQ7RUFDem1EO0FBRUE7TUFDSSx5QkFBeUI7TUFDekIsdzZCQUF3NkI7RUFDNTZCO0FBRUE7TUFDSSx5QkFBeUI7TUFDekIsMHVLQUEwdUs7RUFDOXVLO0FBRUE7TUFDSSx5QkFBeUI7TUFDekIseVJBQXlSO0VBQzdSO0FBRUE7TUFDSSx5QkFBeUI7TUFDekIsNitDQUE2K0M7RUFDai9DO0FBRUE7TUFDSSx5QkFBeUI7TUFDekIsKzBCQUErMEI7RUFDbjFCO0FBRUE7TUFDSSx5QkFBeUI7TUFDekIsa21CQUFrbUI7RUFDdG1CO0FBRUE7TUFDSTtVQUNJLHdCQUF3QjtVQUN4Qix1Q0FBdUM7VUFDdkMsWUFBWTtVQUNaLFlBQVk7TUFDaEI7O01BRUE7VUFDSSxZQUFZO01BQ2hCOztNQUVBO1VBQ0ksYUFBYTtNQUNqQjs7TUFFQTtVQUNJLGdCQUFnQjtNQUNwQjtFQUNKO0FBR0EseUNBQXlDO0FBRXpDO01BQ0kseURBQWlEO2NBQWpELGlEQUFpRDtFQUNyRDtBQUVBO01BQ0ksd0RBQWdEO2NBQWhELGdEQUFnRDtFQUNwRDtBQUVBO01BQ0ksd0RBQWdEO2NBQWhELGdEQUFnRDtFQUNwRDtBQUVBO01BQ0kseURBQWlEO2NBQWpELGlEQUFpRDtFQUNyRDtBQUVBO01BQ0ksd0RBQWdEO2NBQWhELGdEQUFnRDtFQUNwRDtBQUVBO01BQ0ksd0RBQWdEO2NBQWhELGdEQUFnRDtFQUNwRDtBQUVBO01BQ0ksd0RBQWdEO2NBQWhELGdEQUFnRDtFQUNwRDtBQUVBO01BQ0ksd0RBQWdEO2NBQWhELGdEQUFnRDtFQUNwRDtBQUVBO01BQ0ksd0RBQWdEO2NBQWhELGdEQUFnRDtFQUNwRDtBQUVBO01BQ0ksd0RBQWdEO2NBQWhELGdEQUFnRDtFQUNwRDtBQUVBO01BQ0ksd0RBQWdEO2NBQWhELGdEQUFnRDtFQUNwRDtBQUVBO01BQ0ksd0RBQWdEO2NBQWhELGdEQUFnRDtFQUNwRDtBQUVBO01BQ0ksdURBQStDO2NBQS9DLCtDQUErQztFQUNuRDtBQUVBO01BQ0ksdURBQStDO2NBQS9DLCtDQUErQztFQUNuRDtBQUVBO01BQ0ksdURBQStDO2NBQS9DLCtDQUErQztFQUNuRDtBQUVBO01BQ0ksdURBQStDO2NBQS9DLCtDQUErQztFQUNuRDtBQUVBO01BQ0k7VUFDSSwyQkFBMkI7TUFDL0I7TUFDQTtVQUNJLHdCQUF3QjtNQUM1QjtFQUNKO0FBUEE7TUFDSTtVQUNJLDJCQUEyQjtNQUMvQjtNQUNBO1VBQ0ksd0JBQXdCO01BQzVCO0VBQ0o7QUFFQTtNQUNJO1VBQ0ksMkJBQTJCO01BQy9CO01BQ0E7VUFDSSx3QkFBd0I7TUFDNUI7RUFDSjtBQVBBO01BQ0k7VUFDSSwyQkFBMkI7TUFDL0I7TUFDQTtVQUNJLHdCQUF3QjtNQUM1QjtFQUNKO0FBRUE7TUFDSTtVQUNJLDJCQUEyQjtNQUMvQjtNQUNBO1VBQ0ksd0JBQXdCO01BQzVCO0VBQ0o7QUFQQTtNQUNJO1VBQ0ksMkJBQTJCO01BQy9CO01BQ0E7VUFDSSx3QkFBd0I7TUFDNUI7RUFDSjtBQUVBO01BQ0k7VUFDSSwyQkFBMkI7TUFDL0I7TUFDQTtVQUNJLHdCQUF3QjtNQUM1QjtFQUNKO0FBUEE7TUFDSTtVQUNJLDJCQUEyQjtNQUMvQjtNQUNBO1VBQ0ksd0JBQXdCO01BQzVCO0VBQ0o7QUFFQTtNQUNJO1VBQ0ksMkJBQTJCO01BQy9CO01BQ0E7VUFDSSx3QkFBd0I7TUFDNUI7RUFDSjtBQVBBO01BQ0k7VUFDSSwyQkFBMkI7TUFDL0I7TUFDQTtVQUNJLHdCQUF3QjtNQUM1QjtFQUNKO0FBRUE7TUFDSTtVQUNJLDJCQUEyQjtNQUMvQjtNQUNBO1VBQ0ksd0JBQXdCO01BQzVCO0VBQ0o7QUFQQTtNQUNJO1VBQ0ksMkJBQTJCO01BQy9CO01BQ0E7VUFDSSx3QkFBd0I7TUFDNUI7RUFDSjtBQUVBO01BQ0k7VUFDSSwyQkFBMkI7TUFDL0I7TUFDQTtVQUNJLHdCQUF3QjtNQUM1QjtFQUNKO0FBUEE7TUFDSTtVQUNJLDJCQUEyQjtNQUMvQjtNQUNBO1VBQ0ksd0JBQXdCO01BQzVCO0VBQ0o7QUFFQTtNQUNJO1VBQ0ksMkJBQTJCO01BQy9CO01BQ0E7VUFDSSx3QkFBd0I7TUFDNUI7RUFDSjtBQVBBO01BQ0k7VUFDSSwyQkFBMkI7TUFDL0I7TUFDQTtVQUNJLHdCQUF3QjtNQUM1QjtFQUNKO0FBRUE7TUFDSTtVQUNJLDJCQUEyQjtNQUMvQjtNQUNBO1VBQ0ksd0JBQXdCO01BQzVCO0VBQ0o7QUFQQTtNQUNJO1VBQ0ksMkJBQTJCO01BQy9CO01BQ0E7VUFDSSx3QkFBd0I7TUFDNUI7RUFDSjtBQUVBO01BQ0k7VUFDSSwyQkFBMkI7TUFDL0I7TUFDQTtVQUNJLHdCQUF3QjtNQUM1QjtFQUNKO0FBUEE7TUFDSTtVQUNJLDJCQUEyQjtNQUMvQjtNQUNBO1VBQ0ksd0JBQXdCO01BQzVCO0VBQ0o7QUFFQTtNQUNJO1VBQ0ksMkJBQTJCO01BQy9CO01BQ0E7VUFDSSx3QkFBd0I7TUFDNUI7RUFDSjtBQVBBO01BQ0k7VUFDSSwyQkFBMkI7TUFDL0I7TUFDQTtVQUNJLHdCQUF3QjtNQUM1QjtFQUNKO0FBRUE7TUFDSTtVQUNJLDJCQUEyQjtNQUMvQjtNQUNBO1VBQ0ksd0JBQXdCO01BQzVCO0VBQ0o7QUFQQTtNQUNJO1VBQ0ksMkJBQTJCO01BQy9CO01BQ0E7VUFDSSx3QkFBd0I7TUFDNUI7RUFDSjtBQUVBO01BQ0k7VUFDSSw0QkFBNEI7TUFDaEM7TUFDQTtVQUNJLHdCQUF3QjtNQUM1QjtFQUNKO0FBUEE7TUFDSTtVQUNJLDRCQUE0QjtNQUNoQztNQUNBO1VBQ0ksd0JBQXdCO01BQzVCO0VBQ0o7QUFFQTtNQUNJO1VBQ0ksNEJBQTRCO01BQ2hDO01BQ0E7VUFDSSx3QkFBd0I7TUFDNUI7RUFDSjtBQVBBO01BQ0k7VUFDSSw0QkFBNEI7TUFDaEM7TUFDQTtVQUNJLHdCQUF3QjtNQUM1QjtFQUNKO0FBRUE7TUFDSTtVQUNJLDRCQUE0QjtNQUNoQztNQUNBO1VBQ0ksd0JBQXdCO01BQzVCO0VBQ0o7QUFQQTtNQUNJO1VBQ0ksNEJBQTRCO01BQ2hDO01BQ0E7VUFDSSx3QkFBd0I7TUFDNUI7RUFDSjtBQUVBO01BQ0k7VUFDSSw0QkFBNEI7TUFDaEM7TUFDQTtVQUNJLHdCQUF3QjtNQUM1QjtFQUNKO0FBUEE7TUFDSTtVQUNJLDRCQUE0QjtNQUNoQztNQUNBO1VBQ0ksd0JBQXdCO01BQzVCO0VBQ0oiLCJmaWxlIjoic3JjL2FwcC9hbmFseXRpYy9hbmFseXRpYy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiYm9keXtiYWNrZ3JvdW5kOmJsYWNrfVxyXG4jcHJvZ3Jlc3MtY29udGFpbmVye3dpZHRoOjgwJTtoZWlnaHQ6NDBweDtiYWNrZ3JvdW5kOiNlZGViZWU7cG9zaXRpb246YWJzb2x1dGU7dG9wOjUwJTtsZWZ0OjUwJTt0cmFuc2Zvcm06dHJhbnNsYXRlKC01MCUsLTUwJSl9XHJcblxyXG4jcHJvZ3Jlc3MtYmFye3dpZHRoOjIlO2hlaWdodDoxMDAlO2JhY2tncm91bmQ6cmVkO2NvbG9yOndoaXRlO3RleHQtYWxpZ246Y2VudGVyO2xpbmUtaGVpZ2h0OjQwcHh9XHJcblxyXG4jYnV0dG9uMSB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjYjJiMmIyXHQ7IFxyXG4gICAgYm9yZGVyOiBub25lO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgcGFkZGluZy1ib3R0b206IDI1cHggMjJweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIGZvbnQtc2l6ZTogMzZweDtcclxuICAgIG1hcmdpbjogNHB4IDE4cHg7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgfVxyXG5cclxuICBcclxuI2J1dHRvbjIge1xyXG4gICBiYWNrZ3JvdW5kLWNvbG9yOiAjYjJiMmIyXHQ7IFxyXG4gICBib3JkZXI6IG5vbmU7XHJcbiAgIGNvbG9yOiB3aGl0ZTtcclxuICAgcGFkZGluZy1ib3R0b206IDI1cHggMjJweDtcclxuICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgZm9udC1zaXplOiAzNnB4O1xyXG4gICBtYXJnaW46IDRweCAxOHB4O1xyXG4gICBjdXJzb3I6IHBvaW50ZXI7XHJcbiB9XHJcbiAjYnV0dG9uMyB7XHJcbiAgIGJhY2tncm91bmQtY29sb3I6ICNiMmIyYjJcdDsgXHJcbiAgIGJvcmRlcjogbm9uZTtcclxuICAgY29sb3I6IHdoaXRlO1xyXG4gICBwYWRkaW5nLWJvdHRvbTogMjVweCAyMnB4O1xyXG4gICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICBmb250LXNpemU6IDM2cHg7XHJcbiAgIG1hcmdpbjogNHB4IDE4cHg7XHJcbiAgIGN1cnNvcjogcG9pbnRlcjtcclxuIH1cclxuXHJcbiAjYnV0dG9uNCB7XHJcbiAgIGJhY2tncm91bmQtY29sb3I6ICNiMmIyYjJcdDtcclxuICAgYm9yZGVyOiBub25lO1xyXG4gICBjb2xvcjogd2hpdGU7XHJcbiAgIHBhZGRpbmctYm90dG9tOiAyNXB4IDIycHg7XHJcbiAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgIGZvbnQtc2l6ZTogMzZweDtcclxuICAgbWFyZ2luOiA0cHggMThweDtcclxuICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gfVxyXG4gXHJcbiAgI2FsbHtcclxuICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgcG9zaXRpb246IGZpeGVkO1xyXG4gICAgIGxlZnQ6IDA7XHJcbiAgICAgYm90dG9tOiAwO1xyXG4gICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgIGJhY2tncm91bmQtY29sb3I6IFx0XHQjYjJiMmIyO1xyXG4gICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgfVxyXG5cclxuICBkaXYuaG9tZSB7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgfVxyXG5cclxuICBkaXYuaG9tZSBpbnB1dCB7XHJcbiAgICBiYWNrZ3JvdW5kOiB1cmwoJ3NyYy9hcHAvaGVhZGVyL2ltYWdlcy9ob21lLnN2ZycpIG5vLXJlcGVhdDtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIHdpZHRoOiA1MHB4O1xyXG4gICAgaGVpZ2h0OiA1MHB4O1xyXG4gICAgYm9yZGVyOiBub25lO1xyXG4gIH1cclxuICBkaXYuYW5hbHl0aWMge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIH1cclxuXHJcbiAgZGl2LmFuYWx5dGljIGlucHV0IHtcclxuICAgIGJhY2tncm91bmQ6IHVybCgnc3JjL2FwcC9oZWFkZXIvaW1hZ2VzL2FuYWx5dGljLnN2ZycpIG5vLXJlcGVhdDtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIHdpZHRoOiA1MHB4O1xyXG4gICAgaGVpZ2h0OiA1MHB4O1xyXG4gICAgYm9yZGVyOiBub25lO1xyXG4gIH1cclxuXHJcbiAgZGl2Lm5vdGlmaWNhdGlvbiB7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgfVxyXG5cclxuICBkaXYubm90aWZpY2F0aW9uIGlucHV0IHtcclxuICAgIGJhY2tncm91bmQ6IHVybCgnc3JjL2FwcC9jYWxlbmRhci9pbWFnZXMvb3V0bGluZS10b2RheS0yNHB4LnN2ZycpIG5vLXJlcGVhdDtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIHdpZHRoOiA1MHB4O1xyXG4gICAgaGVpZ2h0OiA1MHB4O1xyXG4gICAgYm9yZGVyOiBub25lO1xyXG4gIH1cclxuICBcclxuICBkaXYuc2V0dGluZyB7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgfVxyXG5cclxuICBkaXYuc2V0dGluZyBpbnB1dCB7XHJcbiAgICBiYWNrZ3JvdW5kOiB1cmwoJ3NyYy9hcHAvaGVhZGVyL2ltYWdlcy9zZXR0aW5ncy5zdmcnKSBuby1yZXBlYXQ7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICB3aWR0aDogNTBweDtcclxuICAgIGhlaWdodDogNTBweDtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICB9XHJcbiAgc3Bhbi5ob21lIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogMzJweDtcclxuICAgIGxlZnQ6IDZweDtcclxuICB9XHJcbiAgXHJcbiAgc3Bhbi5hbmFseXRpYyB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDMycHg7XHJcbiAgICBsZWZ0OiAycHg7XHJcbiAgfVxyXG5cclxuICBzcGFuLm5vdGlmaWNhdGlvbiB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDMycHg7XHJcbiAgICBsZWZ0OiAycHg7XHJcbiAgfVxyXG5cclxuICBzcGFuLnNldHRpbmcge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAzMnB4O1xyXG4gICAgbGVmdDogMTJweDtcclxuICB9XHJcblxyXG5cclxuICAjbXlQcm9ncmVzcyB7XHJcbiAgICBtYXJnaW4tdG9wOiA2MHB4O1xyXG4gICAgXHJcbiAgICBib3JkZXItcmFkaXVzOiA0NXB4O1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNkZGQ7XHJcbiAgIFxyXG4gIH1cclxuICBcclxuICAjbXlCYXIge1xyXG4gICAgbWFyZ2luLXRvcDogNjBweDtcclxuICAgIFxyXG4gICAgYm9yZGVyLXJhZGl1czogNDVweDtcclxuICAgIHBhZGRpbmctdG9wOiAxNXB4O1xyXG4gICAgd2lkdGg6IDElO1xyXG4gICAgaGVpZ2h0OiAxNXB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogXHQjMDAwMEZGO1xyXG4gICBcclxuICB9XHJcblxyXG5cclxuXHJcblxyXG4gIEBpbXBvcnQgdXJsKFwiaHR0cHM6Ly9mb250cy5nb29nbGVhcGlzLmNvbS9jc3M/ZmFtaWx5PUJpdHRlclwiKTtcclxuXHJcbiAgKiB7XHJcbiAgICAgIC8qIGJvcmRlcjogMXB4IHNvbGlkIHJlZDsgKi9cclxuICB9XHJcbiAgXHJcbiAgOnJvb3Qge1xyXG4gICAgICAtLWZvbnQ6IFwiQml0dGVyXCIsIHNlcmlmO1xyXG4gICAgICAtLXRpdGxlLXNpemU6IDM2cHg7XHJcbiAgICAgIC0tc3ViLXNpemU6IDE4cHg7XHJcbiAgfVxyXG4gIFxyXG4gIGJvZHkge1xyXG4gICAgICBiYWNrZ3JvdW5kOiAjZTVlNWU1O1xyXG4gIH1cclxuICBcclxuICAudGl0bGUgaDEge1xyXG4gICAgICBtYXJnaW46IDRweDtcclxuICAgICAgZm9udC1mYW1pbHk6IHZhcigtLWZvbnQpO1xyXG4gICAgICBmb250LXNpemU6IHZhcigtLXRpdGxlLXNpemUpO1xyXG4gICAgICBjb2xvcjogIzMzMztcclxuICB9XHJcbiAgXHJcbiAgLnRpdGxlIHAge1xyXG4gICAgICBtYXJnaW46IDRweDtcclxuICAgICAgcGFkZGluZy1ib3R0b206IDI1cHg7XHJcbiAgICAgIGZvbnQtZmFtaWx5OiB2YXIoLS1mb250KTtcclxuICAgICAgZm9udC1zaXplOiB2YXIoLS1zdWItc2l6ZSk7XHJcbiAgICAgIGNvbG9yOiAjODg4O1xyXG4gIH1cclxuICBcclxuICAuY29udGFpbmVyIHtcclxuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIH1cclxuICBcclxuICAuZ2l0aHViIHtcclxuICAgICAgbWFyZ2luOiA0MHB4O1xyXG4gIH1cclxuICBcclxuICAucHJvZ3Jlc3Mge1xyXG4gICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICAgIHdpZHRoOiA0MDBweDtcclxuICAgICAgaGVpZ2h0OiA1MHB4O1xyXG4gICAgICBtYXJnaW46IDM1cHg7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDIwcHg7XHJcbiAgICAgIGJhY2tncm91bmQ6ICNmOWY5Zjk7XHJcbiAgfVxyXG4gIFxyXG4gIC5iYXIge1xyXG4gICAgICBib3JkZXItcmFkaXVzOiAyMHB4O1xyXG4gICAgICB3aWR0aDogMCU7XHJcbiAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgdHJhbnNpdGlvbjogd2lkdGg7XHJcbiAgICAgIHRyYW5zaXRpb24tZHVyYXRpb246IDFzO1xyXG4gICAgICB0cmFuc2l0aW9uLXRpbWluZy1mdW5jdGlvbjogY3ViaWMtYmV6aWVyKDAuMzYsIDAuNTUsIDAuNjMsIDAuNDgpO1xyXG4gIH1cclxuICBcclxuICAubW9iaWxlIHtcclxuICAgICAgZGlzcGxheTogbm9uZTtcclxuICB9XHJcbiAgXHJcbiAgLnNoYWRvdyB7XHJcbiAgICAgIC8qIDI1IDUwICovXHJcbiAgICAgIGJveC1zaGFkb3c6IDBweCA0NXB4IDUwcHggcmdiYSgwLCAwLCAwLCAwLjI1KTtcclxuICB9XHJcbiAgXHJcbiAgLmNyb3NzZXMge1xyXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZGZkYmU1O1xyXG4gICAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJkYXRhOmltYWdlL3N2Zyt4bWwsJTNDc3ZnIHhtbG5zPSdodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Zycgd2lkdGg9JzI1JyBoZWlnaHQ9JzI1JyB2aWV3Qm94PScwIDAgNDAgNDAnJTNFJTNDZyBmaWxsLXJ1bGU9J2V2ZW5vZGQnJTNFJTNDZyBmaWxsPSclMjM5QzkyQUMnIGZpbGwtb3BhY2l0eT0nMC40JyUzRSUzQ3BhdGggZD0nTTAgMzguNTlsMi44My0yLjgzIDEuNDEgMS40MUwxLjQxIDQwSDB2LTEuNDF6TTAgMS40bDIuODMgMi44MyAxLjQxLTEuNDFMMS40MSAwSDB2MS40MXpNMzguNTkgNDBsLTIuODMtMi44MyAxLjQxLTEuNDFMNDAgMzguNTlWNDBoLTEuNDF6TTQwIDEuNDFsLTIuODMgMi44My0xLjQxLTEuNDFMMzguNTkgMEg0MHYxLjQxek0yMCAxOC42bDIuODMtMi44MyAxLjQxIDEuNDFMMjEuNDEgMjBsMi44MyAyLjgzLTEuNDEgMS40MUwyMCAyMS40MWwtMi44MyAyLjgzLTEuNDEtMS40MUwxOC41OSAyMGwtMi44My0yLjgzIDEuNDEtMS40MUwyMCAxOC41OXonLyUzRSUzQy9nJTNFJTNDL2clM0UlM0Mvc3ZnJTNFXCIpO1xyXG4gIH1cclxuICBcclxuICAuanVwaXRlciB7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmMzk1YTU7XHJcbiAgICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybChcImRhdGE6aW1hZ2Uvc3ZnK3htbCwlM0NzdmcgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJyB3aWR0aD0nNTInIGhlaWdodD0nNTInIHZpZXdCb3g9JzAgMCA1MiA1MiclM0UlM0NwYXRoIGZpbGw9JyUyM2U2Y2NhNScgZmlsbC1vcGFjaXR5PScwLjQnIGQ9J00wIDE3LjgzVjBoMTcuODNhMyAzIDAgMCAxLTUuNjYgMkg1LjlBNSA1IDAgMCAxIDIgNS45djYuMjdhMyAzIDAgMCAxLTIgNS42NnptMCAxOC4zNGEzIDMgMCAwIDEgMiA1LjY2djYuMjdBNSA1IDAgMCAxIDUuOSA1Mmg2LjI3YTMgMyAwIDAgMSA1LjY2IDBIMFYzNi4xN3pNMzYuMTcgNTJhMyAzIDAgMCAxIDUuNjYgMGg2LjI3YTUgNSAwIDAgMSAzLjktMy45di02LjI3YTMgMyAwIDAgMSAwLTUuNjZWNTJIMzYuMTd6TTAgMzEuOTN2LTkuNzhhNSA1IDAgMCAxIDMuOC43Mmw0LjQzLTQuNDNhMyAzIDAgMSAxIDEuNDIgMS40MUw1LjIgMjQuMjhhNSA1IDAgMCAxIDAgNS41Mmw0LjQ0IDQuNDNhMyAzIDAgMSAxLTEuNDIgMS40MkwzLjggMzEuMmE1IDUgMCAwIDEtMy44Ljcyem01Mi0xNC4xYTMgMyAwIDAgMSAwLTUuNjZWNS45QTUgNSAwIDAgMSA0OC4xIDJoLTYuMjdhMyAzIDAgMCAxLTUuNjYtMkg1MnYxNy44M3ptMCAxNC4xYTQuOTcgNC45NyAwIDAgMS0xLjcyLS43MmwtNC40MyA0LjQ0YTMgMyAwIDEgMS0xLjQxLTEuNDJsNC40My00LjQzYTUgNSAwIDAgMSAwLTUuNTJsLTQuNDMtNC40M2EzIDMgMCAxIDEgMS40MS0xLjQxbDQuNDMgNC40M2MuNTMtLjM1IDEuMTItLjYgMS43Mi0uNzJ2OS43OHpNMjIuMTUgMGg5Ljc4YTUgNSAwIDAgMS0uNzIgMy44bDQuNDQgNC40M2EzIDMgMCAxIDEtMS40MiAxLjQyTDI5LjggNS4yYTUgNSAwIDAgMS01LjUyIDBsLTQuNDMgNC40NGEzIDMgMCAxIDEtMS40MS0xLjQybDQuNDMtNC40M2E1IDUgMCAwIDEtLjcyLTMuOHptMCA1MmMuMTMtLjYuMzctMS4xOS43Mi0xLjcybC00LjQzLTQuNDNhMyAzIDAgMSAxIDEuNDEtMS40MWw0LjQzIDQuNDNhNSA1IDAgMCAxIDUuNTIgMGw0LjQzLTQuNDNhMyAzIDAgMSAxIDEuNDIgMS40MWwtNC40NCA0LjQzYy4zNi41My42IDEuMTIuNzIgMS43MmgtOS43OHptOS43NS0yNGE1IDUgMCAwIDEtMy45IDMuOXY2LjI3YTMgMyAwIDEgMS0yIDBWMzEuOWE1IDUgMCAwIDEtMy45LTMuOWgtNi4yN2EzIDMgMCAxIDEgMC0yaDYuMjdhNSA1IDAgMCAxIDMuOS0zLjl2LTYuMjdhMyAzIDAgMSAxIDIgMHY2LjI3YTUgNSAwIDAgMSAzLjkgMy45aDYuMjdhMyAzIDAgMSAxIDAgMkgzMS45eiclM0UlM0MvcGF0aCUzRSUzQy9zdmclM0VcIik7XHJcbiAgfVxyXG4gIFxyXG4gIC5waWFubyB7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICNjY2NjY2M7XHJcbiAgICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybChcImRhdGE6aW1hZ2Uvc3ZnK3htbCwlM0NzdmcgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJyB3aWR0aD0nNzAnIGhlaWdodD0nNDYnIHZpZXdCb3g9JzAgMCA3MCA0NiclM0UlM0NnIGZpbGwtcnVsZT0nZXZlbm9kZCclM0UlM0NnIGZpbGw9JyUyMzMzMzMzMycgZmlsbC1vcGFjaXR5PScwLjQnJTNFJTNDcG9seWdvbiBwb2ludHM9JzY4IDQ0IDYyIDQ0IDYyIDQ2IDU2IDQ2IDU2IDQ0IDUyIDQ0IDUyIDQ2IDQ2IDQ2IDQ2IDQ0IDQwIDQ0IDQwIDQ2IDM4IDQ2IDM4IDQ0IDMyIDQ0IDMyIDQ2IDI2IDQ2IDI2IDQ0IDIyIDQ0IDIyIDQ2IDE2IDQ2IDE2IDQ0IDEyIDQ0IDEyIDQ2IDYgNDYgNiA0NCAwIDQ0IDAgNDIgOCA0MiA4IDI4IDYgMjggNiAwIDEyIDAgMTIgMjggMTAgMjggMTAgNDIgMTggNDIgMTggMjggMTYgMjggMTYgMCAyMiAwIDIyIDI4IDIwIDI4IDIwIDQyIDI4IDQyIDI4IDI4IDI2IDI4IDI2IDAgMzIgMCAzMiAyOCAzMCAyOCAzMCA0MiAzOCA0MiAzOCAwIDQwIDAgNDAgNDIgNDggNDIgNDggMjggNDYgMjggNDYgMCA1MiAwIDUyIDI4IDUwIDI4IDUwIDQyIDU4IDQyIDU4IDI4IDU2IDI4IDU2IDAgNjIgMCA2MiAyOCA2MCAyOCA2MCA0MiA2OCA0MiA2OCAwIDcwIDAgNzAgNDYgNjggNDYnLyUzRSUzQy9nJTNFJTNDL2clM0UlM0Mvc3ZnJTNFXCIpO1xyXG4gIH1cclxuICBcclxuICAuZG9taW5vcyB7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmY2YmQ7XHJcbiAgICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybChcImRhdGE6aW1hZ2Uvc3ZnK3htbCwlM0NzdmcgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJyB3aWR0aD0nMTI2JyBoZWlnaHQ9Jzg0JyB2aWV3Qm94PScwIDAgMTI2IDg0JyUzRSUzQ2cgZmlsbC1ydWxlPSdldmVub2RkJyUzRSUzQ2cgZmlsbD0nJTIzMzMzMzMzJyBmaWxsLW9wYWNpdHk9JzAuNCclM0UlM0NwYXRoIGQ9J00xMjYgODN2MUgwdi0yaDQwVjQySDB2LTJoNDBWMGgydjQwaDQwVjBoMnY0MGg0MFYwaDJ2ODN6bS0yLTFWNDJIODR2NDBoNDB6TTgyIDQySDQydjQwaDQwVjQyem0tNTAtNmE0IDQgMCAxIDEgMC04IDQgNCAwIDAgMSAwIDh6TTggMTJhNCA0IDAgMSAxIDAtOCA0IDQgMCAwIDEgMCA4em05NiAxMmE0IDQgMCAxIDEgMC04IDQgNCAwIDAgMSAwIDh6bS00MiAwYTQgNCAwIDEgMSAwLTggNCA0IDAgMCAxIDAgOHptMzAtMTJhNCA0IDAgMSAxIDAtOCA0IDQgMCAwIDEgMCA4ek0yMCA1NGE0IDQgMCAxIDEgMC04IDQgNCAwIDAgMSAwIDh6bTEyIDI0YTQgNCAwIDEgMSAwLTggNCA0IDAgMCAxIDAgOHpNOCA1NGE0IDQgMCAxIDEgMC04IDQgNCAwIDAgMSAwIDh6bTI0IDBhNCA0IDAgMSAxIDAtOCA0IDQgMCAwIDEgMCA4ek04IDc4YTQgNCAwIDEgMSAwLTggNCA0IDAgMCAxIDAgOHptMTIgMGE0IDQgMCAxIDEgMC04IDQgNCAwIDAgMSAwIDh6bTU0IDBhNCA0IDAgMSAxIDAtOCA0IDQgMCAwIDEgMCA4ek01MCA1NGE0IDQgMCAxIDEgMC04IDQgNCAwIDAgMSAwIDh6bTI0IDBhNCA0IDAgMSAxIDAtOCA0IDQgMCAwIDEgMCA4ek01MCA3OGE0IDQgMCAxIDEgMC04IDQgNCAwIDAgMSAwIDh6bTU0LTEyYTQgNCAwIDEgMSAwLTggNCA0IDAgMCAxIDAgOHptMTIgMTJhNCA0IDAgMSAxIDAtOCA0IDQgMCAwIDEgMCA4ek05MiA1NGE0IDQgMCAxIDEgMC04IDQgNCAwIDAgMSAwIDh6bTI0IDBhNCA0IDAgMSAxIDAtOCA0IDQgMCAwIDEgMCA4ek05MiA3OGE0IDQgMCAxIDEgMC04IDQgNCAwIDAgMSAwIDh6bTI0LTQyYTQgNCAwIDEgMSAwLTggNCA0IDAgMCAxIDAgOHonLyUzRSUzQy9nJTNFJTNDL2clM0UlM0Mvc3ZnJTNFXCIpO1xyXG4gIH1cclxuICBcclxuICAucGllIHtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZhYWNhODtcclxuICAgICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiZGF0YTppbWFnZS9zdmcreG1sLCUzQ3N2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPSc1MCcgaGVpZ2h0PSc1MCcgdmlld0JveD0nMCAwIDYwIDYwJyUzRSUzQ2cgZmlsbC1ydWxlPSdldmVub2RkJyUzRSUzQ2cgZmlsbD0nJTIzZjU1YjUzJyBmaWxsLW9wYWNpdHk9JzAuNCcgZmlsbC1ydWxlPSdub256ZXJvJyUzRSUzQ3BhdGggZD0nTTI5IDU4LjU4bDcuMzgtNy4zOUEzMC45NSAzMC45NSAwIDAgMSAyOSAzNy44NGEzMC45NSAzMC45NSAwIDAgMS03LjM4IDEzLjM2bDcuMzcgNy4zOHptMS40IDEuNDFsLjAxLjAxaC0yLjg0bC03LjM3LTcuMzhBMzAuOTUgMzAuOTUgMCAwIDEgNi44NCA2MEgwdi0xLjAyYTI4LjkgMjguOSAwIDAgMCAxOC43OS03Ljc4TDAgMzIuNDF2LTQuODRMMTguNzggOC43OUEyOC45IDI4LjkgMCAwIDAgMCAxLjAyVjBoNi44NGEzMC45NSAzMC45NSAwIDAgMSAxMy4zNSA3LjM4TDI3LjU3IDBoMi44NGw3LjM5IDcuMzhBMzAuOTUgMzAuOTUgMCAwIDEgNTEuMTYgMEg2MHYyNy41OC0uMDFWNjBoLTguODRhMzAuOTUgMzAuOTUgMCAwIDEtMTMuMzctNy40TDMwLjQgNjB6TTI5IDEuNDFsLTcuNCA3LjM4QTMwLjk1IDMwLjk1IDAgMCAxIDI5IDIyLjE2IDMwLjk1IDMwLjk1IDAgMCAxIDM2LjM4IDguOEwyOSAxLjR6TTU4IDFBMjguOSAyOC45IDAgMCAwIDM5LjIgOC44TDU4IDI3LjU4VjEuMDJ6bS0yMC4yIDkuMkEyOC45IDI4LjkgMCAwIDAgMzAuMDIgMjloMjYuNTZMMzcuOCAxMC4yMXpNMzAuMDIgMzFhMjguOSAyOC45IDAgMCAwIDcuNzcgMTguNzlsMTguNzktMTguNzlIMzAuMDJ6bTkuMTggMjAuMkEyOC45IDI4LjkgMCAwIDAgNTggNTlWMzIuNEwzOS4yIDUxLjE5em0tMTktMS40YTI4LjkgMjguOSAwIDAgMCA3Ljc4LTE4LjhIMS40MWwxOC44IDE4Ljh6bTcuNzgtMjAuOEEyOC45IDI4LjkgMCAwIDAgMjAuMiAxMC4yTDEuNDEgMjloMjYuNTd6Jy8lM0UlM0MvZyUzRSUzQy9nJTNFJTNDL3N2ZyUzRVwiKTtcclxuICB9XHJcbiAgXHJcbiAgLmJlZXMge1xyXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmNjODQ2O1xyXG4gICAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJkYXRhOmltYWdlL3N2Zyt4bWwsJTNDc3ZnIHhtbG5zPSdodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Zycgd2lkdGg9JzI4JyBoZWlnaHQ9JzQ5JyB2aWV3Qm94PScwIDAgMjggNDknJTNFJTNDZyBmaWxsLXJ1bGU9J2V2ZW5vZGQnJTNFJTNDZyBpZD0naGV4YWdvbnMnIGZpbGw9JyUyMzdhNDk0OCcgZmlsbC1vcGFjaXR5PScwLjgzJyBmaWxsLXJ1bGU9J25vbnplcm8nJTNFJTNDcGF0aCBkPSdNMTMuOTkgOS4yNWwxMyA3LjV2MTVsLTEzIDcuNUwxIDMxLjc1di0xNWwxMi45OS03LjV6TTMgMTcuOXYxMi43bDEwLjk5IDYuMzQgMTEtNi4zNVYxNy45bC0xMS02LjM0TDMgMTcuOXpNMCAxNWwxMi45OC03LjVWMGgtMnY2LjM1TDAgMTIuNjl2Mi4zem0wIDE4LjVMMTIuOTggNDF2OGgtMnYtNi44NUwwIDM1Ljgxdi0yLjN6TTE1IDB2Ny41TDI3Ljk5IDE1SDI4di0yLjMxaC0uMDFMMTcgNi4zNVYwaC0yem0wIDQ5di04bDEyLjk5LTcuNUgyOHYyLjMxaC0uMDFMMTcgNDIuMTVWNDloLTJ6Jy8lM0UlM0MvZyUzRSUzQy9nJTNFJTNDL3N2ZyUzRVwiKTtcclxuICB9XHJcbiAgXHJcbiAgLmZvb2Qge1xyXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZjYjA1O1xyXG4gICAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJkYXRhOmltYWdlL3N2Zyt4bWwsJTNDc3ZnIHhtbG5zPSdodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Zycgd2lkdGg9JzEyMCcgaGVpZ2h0PScxMjAnIHZpZXdCb3g9JzAgMCAyNjAgMjYwJyUzRSUzQ2cgZmlsbC1ydWxlPSdldmVub2RkJyUzRSUzQ2cgZmlsbD0nJTIzZWMwMDhjJyBmaWxsLW9wYWNpdHk9JzAuODMnJTNFJTNDcGF0aCBkPSdNMjQuMzcgMTZjLjIuNjUuMzkgMS4zMi41NCAySDIxLjE3bDEuMTcgMi4zNC40NS45LS4yNC4xMVYyOGE1IDUgMCAwIDEtMi4yMyA4Ljk0bC0uMDIuMDZhOCA4IDAgMCAxLTcuNzUgNmgtMjBhOCA4IDAgMCAxLTcuNzQtNmwtLjAyLS4wNkE1IDUgMCAwIDEtMTcuNDUgMjh2LTYuNzZsLS43OS0xLjU4LS40NC0uOS45LS40NC42My0uMzJILTIwYTIzLjAxIDIzLjAxIDAgMCAxIDQ0LjM3LTJ6bS0zNi44MiAyYTEgMSAwIDAgMC0uNDQuMWwtMy4xIDEuNTYuODkgMS43OSAxLjMxLS42NmEzIDMgMCAwIDEgMi42OSAwbDIuMiAxLjFhMSAxIDAgMCAwIC45IDBsMi4yMS0xLjFhMyAzIDAgMCAxIDIuNjkgMGwyLjIgMS4xYTEgMSAwIDAgMCAuOSAwbDIuMjEtMS4xYTMgMyAwIDAgMSAyLjY5IDBsMi4yIDEuMWExIDEgMCAwIDAgLjg2LjAybDIuODgtMS4yN2EzIDMgMCAwIDEgMi40MyAwbDIuODggMS4yN2ExIDEgMCAwIDAgLjg1LS4wMmwzLjEtMS41NS0uODktMS43OS0xLjQyLjcxYTMgMyAwIDAgMS0yLjU2LjA2bC0yLjc3LTEuMjNhMSAxIDAgMCAwLS40LS4wOWgtLjAxYTEgMSAwIDAgMC0uNC4wOWwtMi43OCAxLjIzYTMgMyAwIDAgMS0yLjU2LS4wNmwtMi4zLTEuMTVhMSAxIDAgMCAwLS40NS0uMTFoLS4wMWExIDEgMCAwIDAtLjQ0LjFMLjkgMTkuMjJhMyAzIDAgMCAxLTIuNjkgMGwtMi4yLTEuMWExIDEgMCAwIDAtLjQ1LS4xMWgtLjAxYTEgMSAwIDAgMC0uNDQuMWwtMi4yMSAxLjExYTMgMyAwIDAgMS0yLjY5IDBsLTIuMi0xLjFhMSAxIDAgMCAwLS40NS0uMTFoLS4wMXptMC0yaC00LjlhMjEuMDEgMjEuMDEgMCAwIDEgMzkuNjEgMGgtMi4wOWwtLjA2LS4xMy0uMjYuMTNoLTMyLjMxem0zMC4zNSA3LjY4bDEuMzYtLjY4aDEuM3YyaC0zNnYtMS4xNWwuMzQtLjE3IDEuMzYtLjY4aDIuNTlsMS4zNi42OGEzIDMgMCAwIDAgMi42OSAwbDEuMzYtLjY4aDIuNTlsMS4zNi42OGEzIDMgMCAwIDAgMi42OSAwTDIuMjYgMjNoMi41OWwxLjM2LjY4YTMgMyAwIDAgMCAyLjU2LjA2bDEuNjctLjc0aDMuMjNsMS42Ny43NGEzIDMgMCAwIDAgMi41Ni0uMDZ6TS0xMy44MiAyN2wxNi4zNyA0LjkxTDE4LjkzIDI3aC0zMi43NXptLS42MyAyaC4zNGwxNi42NiA1IDE2LjY3LTVoLjMzYTMgMyAwIDEgMSAwIDZoLTM0YTMgMyAwIDEgMSAwLTZ6bTEuMzUgOGE2IDYgMCAwIDAgNS42NSA0aDIwYTYgNiAwIDAgMCA1LjY2LTRILTEzLjF6Jy8lM0UlM0NwYXRoIGlkPSdwYXRoNl9maWxsLWNvcHknIGQ9J00yODQuMzcgMTZjLjIuNjUuMzkgMS4zMi41NCAySDI4MS4xN2wxLjE3IDIuMzQuNDUuOS0uMjQuMTFWMjhhNSA1IDAgMCAxLTIuMjMgOC45NGwtLjAyLjA2YTggOCAwIDAgMS03Ljc1IDZoLTIwYTggOCAwIDAgMS03Ljc0LTZsLS4wMi0uMDZhNSA1IDAgMCAxLTIuMjQtOC45NHYtNi43NmwtLjc5LTEuNTgtLjQ0LS45LjktLjQ0LjYzLS4zMkgyNDBhMjMuMDEgMjMuMDEgMCAwIDEgNDQuMzctMnptLTM2LjgyIDJhMSAxIDAgMCAwLS40NC4xbC0zLjEgMS41Ni44OSAxLjc5IDEuMzEtLjY2YTMgMyAwIDAgMSAyLjY5IDBsMi4yIDEuMWExIDEgMCAwIDAgLjkgMGwyLjIxLTEuMWEzIDMgMCAwIDEgMi42OSAwbDIuMiAxLjFhMSAxIDAgMCAwIC45IDBsMi4yMS0xLjFhMyAzIDAgMCAxIDIuNjkgMGwyLjIgMS4xYTEgMSAwIDAgMCAuODYuMDJsMi44OC0xLjI3YTMgMyAwIDAgMSAyLjQzIDBsMi44OCAxLjI3YTEgMSAwIDAgMCAuODUtLjAybDMuMS0xLjU1LS44OS0xLjc5LTEuNDIuNzFhMyAzIDAgMCAxLTIuNTYuMDZsLTIuNzctMS4yM2ExIDEgMCAwIDAtLjQtLjA5aC0uMDFhMSAxIDAgMCAwLS40LjA5bC0yLjc4IDEuMjNhMyAzIDAgMCAxLTIuNTYtLjA2bC0yLjMtMS4xNWExIDEgMCAwIDAtLjQ1LS4xMWgtLjAxYTEgMSAwIDAgMC0uNDQuMWwtMi4yMSAxLjExYTMgMyAwIDAgMS0yLjY5IDBsLTIuMi0xLjFhMSAxIDAgMCAwLS40NS0uMTFoLS4wMWExIDEgMCAwIDAtLjQ0LjFsLTIuMjEgMS4xMWEzIDMgMCAwIDEtMi42OSAwbC0yLjItMS4xYTEgMSAwIDAgMC0uNDUtLjExaC0uMDF6bTAtMmgtNC45YTIxLjAxIDIxLjAxIDAgMCAxIDM5LjYxIDBoLTIuMDlsLS4wNi0uMTMtLjI2LjEzaC0zMi4zMXptMzAuMzUgNy42OGwxLjM2LS42OGgxLjN2MmgtMzZ2LTEuMTVsLjM0LS4xNyAxLjM2LS42OGgyLjU5bDEuMzYuNjhhMyAzIDAgMCAwIDIuNjkgMGwxLjM2LS42OGgyLjU5bDEuMzYuNjhhMyAzIDAgMCAwIDIuNjkgMGwxLjM2LS42OGgyLjU5bDEuMzYuNjhhMyAzIDAgMCAwIDIuNTYuMDZsMS42Ny0uNzRoMy4yM2wxLjY3Ljc0YTMgMyAwIDAgMCAyLjU2LS4wNnpNMjQ2LjE4IDI3bDE2LjM3IDQuOTFMMjc4LjkzIDI3aC0zMi43NXptLS42MyAyaC4zNGwxNi42NiA1IDE2LjY3LTVoLjMzYTMgMyAwIDEgMSAwIDZoLTM0YTMgMyAwIDEgMSAwLTZ6bTEuMzUgOGE2IDYgMCAwIDAgNS42NSA0aDIwYTYgNiAwIDAgMCA1LjY2LTRIMjQ2Ljl6Jy8lM0UlM0NwYXRoIGQ9J00xNTkuNSAyMS4wMkE5IDkgMCAwIDAgMTUxIDE1aC00MmE5IDkgMCAwIDAtOC41IDYuMDIgNiA2IDAgMCAwIC4wMiAxMS45NkE4Ljk5IDguOTkgMCAwIDAgMTA5IDQ1aDQyYTkgOSAwIDAgMCA4LjQ4LTEyLjAyIDYgNiAwIDAgMCAuMDItMTEuOTZ6TTE1MSAxN2gtNDJhNyA3IDAgMCAwLTYuMzMgNGg1NC42NmE3IDcgMCAwIDAtNi4zMy00em0tOS4zNCAyNmE4Ljk4IDguOTggMCAwIDAgMy4zNC03aC0yYTcgNyAwIDAgMS03IDdoLTQuMzRhOC45OCA4Ljk4IDAgMCAwIDMuMzQtN2gtMmE3IDcgMCAwIDEtNyA3aC00LjM0YTguOTggOC45OCAwIDAgMCAzLjM0LTdoLTJhNyA3IDAgMCAxLTcgN2gtN2E3IDcgMCAxIDEgMC0xNGg0MmE3IDcgMCAxIDEgMCAxNGgtOS4zNHpNMTA5IDI3YTkgOSAwIDAgMC03LjQ4IDRIMTAxYTQgNCAwIDEgMSAwLThoNThhNCA0IDAgMCAxIDAgOGgtLjUyYTkgOSAwIDAgMC03LjQ4LTRoLTQyeicvJTNFJTNDcGF0aCBkPSdNMzkgMTE1YTggOCAwIDEgMCAwLTE2IDggOCAwIDAgMCAwIDE2em02LThhNiA2IDAgMSAxLTEyIDAgNiA2IDAgMCAxIDEyIDB6bS0zLTI5di0yaDh2LTZINDBhNCA0IDAgMCAwLTQgNHYxMEgyMmwtMS4zMyA0LS42NyAyaDIuMTlMMjYgMTMwaDI2bDMuODEtNDBINThsLS42Ny0yTDU2IDg0SDQydi02em0tNC00djEwaDJWNzRoOHYtMmgtOGEyIDIgMCAwIDAtMiAyem0yIDEyaDE0LjU2bC42NyAySDIyLjc3bC42Ny0ySDQwem0xMy44IDRIMjQuMmwzLjYyIDM4aDIyLjM2bDMuNjItMzh6Jy8lM0UlM0NwYXRoIGQ9J00xMjkgOTJoLTZ2NGgtNnY0aC02djE0aC0zbC4yNCAyIDMuNzYgMzJoMzZsMy43Ni0zMiAuMjQtMmgtM3YtMTRoLTZ2LTRoLTZ2LTRoLTh6bTE4IDIydi0xMmgtNHY0aDN2OGgxem0tMyAwdi02aC00djZoNHptLTYgNnYtMTZoLTR2MTkuMTdjMS42LS43IDIuOTctMS44IDQtMy4xN3ptLTYgMy44VjEwMGgtNHYyMy44YTEwLjA0IDEwLjA0IDAgMCAwIDQgMHptLTYtLjYzVjEwNGgtNHYxNmExMC4wNCAxMC4wNCAwIDAgMCA0IDMuMTd6bS02LTkuMTd2LTZoLTR2Nmg0em0tNiAwdi04aDN2LTRoLTR2MTJoMXptMjctMTJ2LTRoLTR2NGgzdjRoMXYtNHptLTYgMHYtOGgtNHY0aDN2NGgxem0tNi00di00aC00djhoMXYtNGgzem0tNiA0di00aC00djhoMXYtNGgzem03IDI0YTEyIDEyIDAgMCAwIDExLjgzLTEwaDcuOTJsLTMuNTMgMzBoLTMyLjQ0bC0zLjUzLTMwaDcuOTJBMTIgMTIgMCAwIDAgMTMwIDEyNnonLyUzRSUzQ3BhdGggZD0nTTIxMiA4NnYyaC00di0yaDR6bTQgMGgtMnYyaDJ2LTJ6bS0yMCAwdi4xYTUgNSAwIDAgMC0uNTYgOS42NWwuMDYuMjUgMS4xMiA0LjQ4YTIgMiAwIDAgMCAxLjk0IDEuNTJoLjAxbDcuMDIgMjQuNTVhMiAyIDAgMCAwIDEuOTIgMS40NWg0Ljk4YTIgMiAwIDAgMCAxLjkyLTEuNDVsNy4wMi0yNC41NWEyIDIgMCAwIDAgMS45NS0xLjUyTDIyNC41IDk2bC4wNi0uMjVhNSA1IDAgMCAwLS41Ni05LjY1Vjg2YTE0IDE0IDAgMCAwLTI4IDB6bTQgMGg2djJoLTlhMyAzIDAgMSAwIDAgNkgyMjNhMyAzIDAgMSAwIDAtNkgyMjB2LTJoMmExMiAxMiAwIDEgMC0yNCAwaDJ6bS0xLjQ0IDE0bC0xLTRoMjQuODhsLTEgNGgtMjIuODh6bTguOTUgMjZsLTYuODYtMjRoMTguN2wtNi44NiAyNGgtNC45OHpNMTUwIDI0MmEyMiAyMiAwIDEgMCAwLTQ0IDIyIDIyIDAgMCAwIDAgNDR6bTI0LTIyYTI0IDI0IDAgMSAxLTQ4IDAgMjQgMjQgMCAwIDEgNDggMHptLTI4LjM4IDE3LjczbDIuMDQtLjg3YTYgNiAwIDAgMSA0LjY4IDBsMi4wNC44N2EyIDIgMCAwIDAgMi41LS44MmwxLjE0LTEuOWE2IDYgMCAwIDEgMy43OS0yLjc1bDIuMTUtLjVhMiAyIDAgMCAwIDEuNTQtMi4xMmwtLjE5LTIuMmE2IDYgMCAwIDEgMS40NS00LjQ2bDEuNDUtMS42N2EyIDIgMCAwIDAgMC0yLjYybC0xLjQ1LTEuNjdhNiA2IDAgMCAxLTEuNDUtNC40NmwuMi0yLjJhMiAyIDAgMCAwLTEuNTUtMi4xM2wtMi4xNS0uNWE2IDYgMCAwIDEtMy44LTIuNzVsLTEuMTMtMS45YTIgMiAwIDAgMC0yLjUtLjhsLTIuMDQuODZhNiA2IDAgMCAxLTQuNjggMGwtMi4wNC0uODdhMiAyIDAgMCAwLTIuNS44MmwtMS4xNCAxLjlhNiA2IDAgMCAxLTMuNzkgMi43NWwtMi4xNS41YTIgMiAwIDAgMC0xLjU0IDIuMTJsLjE5IDIuMmE2IDYgMCAwIDEtMS40NSA0LjQ2bC0xLjQ1IDEuNjdhMiAyIDAgMCAwIDAgMi42MmwxLjQ1IDEuNjdhNiA2IDAgMCAxIDEuNDUgNC40NmwtLjIgMi4yYTIgMiAwIDAgMCAxLjU1IDIuMTNsMi4xNS41YTYgNiAwIDAgMSAzLjggMi43NWwxLjEzIDEuOWEyIDIgMCAwIDAgMi41Ljh6bTIuODIuOTdhNCA0IDAgMCAxIDMuMTIgMGwyLjA0Ljg3YTQgNCAwIDAgMCA0Ljk5LTEuNjJsMS4xNC0xLjlhNCA0IDAgMCAxIDIuNTMtMS44NGwyLjE1LS41YTQgNCAwIDAgMCAzLjA5LTQuMjRsLS4yLTIuMmE0IDQgMCAwIDEgLjk3LTIuOThsMS40NS0xLjY3YTQgNCAwIDAgMCAwLTUuMjRsLTEuNDUtMS42N2E0IDQgMCAwIDEtLjk3LTIuOTdsLjItMi4yYTQgNCAwIDAgMC0zLjA5LTQuMjVsLTIuMTUtLjVhNCA0IDAgMCAxLTIuNTMtMS44NGwtMS4xNC0xLjlhNCA0IDAgMCAwLTUtMS42MmwtMi4wMy44N2E0IDQgMCAwIDEtMy4xMiAwbC0yLjA0LS44N2E0IDQgMCAwIDAtNC45OSAxLjYybC0xLjE0IDEuOWE0IDQgMCAwIDEtMi41MyAxLjg0bC0yLjE1LjVhNCA0IDAgMCAwLTMuMDkgNC4yNGwuMiAyLjJhNCA0IDAgMCAxLS45NyAyLjk4bC0xLjQ1IDEuNjdhNCA0IDAgMCAwIDAgNS4yNGwxLjQ1IDEuNjdhNCA0IDAgMCAxIC45NyAyLjk3bC0uMiAyLjJhNCA0IDAgMCAwIDMuMDkgNC4yNWwyLjE1LjVhNCA0IDAgMCAxIDIuNTMgMS44NGwxLjE0IDEuOWE0IDQgMCAwIDAgNSAxLjYybDIuMDMtLjg3ek0xNTIgMjA3YTEgMSAwIDEgMSAyIDAgMSAxIDAgMCAxLTIgMHptNiAyYTEgMSAwIDEgMSAyIDAgMSAxIDAgMCAxLTIgMHptLTExIDFhMSAxIDAgMSAxIDIgMCAxIDEgMCAwIDEtMiAwem0tNiAwYTEgMSAwIDEgMSAyIDAgMSAxIDAgMCAxLTIgMHptMy01YTEgMSAwIDEgMSAyIDAgMSAxIDAgMCAxLTIgMHptLTggOGExIDEgMCAxIDEgMiAwIDEgMSAwIDAgMS0yIDB6bTMgNmExIDEgMCAxIDEgMiAwIDEgMSAwIDAgMS0yIDB6bTAgNmExIDEgMCAxIDEgMiAwIDEgMSAwIDAgMS0yIDB6bTQgN2ExIDEgMCAxIDEgMiAwIDEgMSAwIDAgMS0yIDB6bTUtMmExIDEgMCAxIDEgMiAwIDEgMSAwIDAgMS0yIDB6bTUgNGExIDEgMCAxIDEgMiAwIDEgMSAwIDAgMS0yIDB6bTQtNmExIDEgMCAxIDEgMiAwIDEgMSAwIDAgMS0yIDB6bTYtNGExIDEgMCAxIDEgMiAwIDEgMSAwIDAgMS0yIDB6bS00LTNhMSAxIDAgMSAxIDIgMCAxIDEgMCAwIDEtMiAwem00LTNhMSAxIDAgMSAxIDIgMCAxIDEgMCAwIDEtMiAwem0tNS00YTEgMSAwIDEgMSAyIDAgMSAxIDAgMCAxLTIgMHptLTI0IDZhMSAxIDAgMSAxIDIgMCAxIDEgMCAwIDEtMiAwem0xNiA1YTUgNSAwIDEgMCAwLTEwIDUgNSAwIDAgMCAwIDEwem03LTVhNyA3IDAgMSAxLTE0IDAgNyA3IDAgMCAxIDE0IDB6bTg2LTI5YTEgMSAwIDAgMCAwIDJoMmExIDEgMCAwIDAgMC0yaC0yem0xOSA5YTEgMSAwIDAgMSAxLTFoMmExIDEgMCAwIDEgMCAyaC0yYTEgMSAwIDAgMS0xLTF6bS0xNCA1YTEgMSAwIDAgMCAwIDJoMmExIDEgMCAwIDAgMC0yaC0yem0tMjUgMWExIDEgMCAwIDAgMCAyaDJhMSAxIDAgMCAwIDAtMmgtMnptNSA0YTEgMSAwIDAgMCAwIDJoMmExIDEgMCAwIDAgMC0yaC0yem05IDBhMSAxIDAgMCAxIDEtMWgyYTEgMSAwIDAgMSAwIDJoLTJhMSAxIDAgMCAxLTEtMXptMTUgMWExIDEgMCAwIDEgMS0xaDJhMSAxIDAgMCAxIDAgMmgtMmExIDEgMCAwIDEtMS0xem0xMi0yYTEgMSAwIDAgMCAwIDJoMmExIDEgMCAwIDAgMC0yaC0yem0tMTEtMTRhMSAxIDAgMCAxIDEtMWgyYTEgMSAwIDAgMSAwIDJoLTJhMSAxIDAgMCAxLTEtMXptLTE5IDBhMSAxIDAgMCAwIDAgMmgyYTEgMSAwIDAgMCAwLTJoLTJ6bTYgNWExIDEgMCAwIDEgMS0xaDJhMSAxIDAgMCAxIDAgMmgtMmExIDEgMCAwIDEtMS0xem0tMjUgMTVjMC0uNDcuMDEtLjk0LjAzLTEuNGE1IDUgMCAwIDEtMS43LTggMy45OSAzLjk5IDAgMCAxIDEuODgtNS4xOCA1IDUgMCAwIDEgMy40LTYuMjIgMyAzIDAgMCAxIDEuNDYtMS4wNSA1IDUgMCAwIDEgNy43Ni0zLjI3QTMwLjg2IDMwLjg2IDAgMCAxIDI0NiAxODRjNi43OSAwIDEzLjA2IDIuMTggMTguMTcgNS44OGE1IDUgMCAwIDEgNy43NiAzLjI3IDMgMyAwIDAgMSAxLjQ3IDEuMDUgNSA1IDAgMCAxIDMuNCA2LjIyIDQgNCAwIDAgMSAxLjg3IDUuMTggNC45OCA0Ljk4IDAgMCAxLTEuNyA4Yy4wMi40Ni4wMy45My4wMyAxLjR2MWgtNjJ2LTF6bS44My03LjE3YTMwLjkgMzAuOSAwIDAgMC0uNjIgMy41NyAzIDMgMCAwIDEtLjYxLTQuMmMuMzcuMjguNzguNDkgMS4yMy42M3ptMS40OS00LjYxYy0uMzYuODctLjY4IDEuNzYtLjk2IDIuNjhhMiAyIDAgMCAxLS4yMS0zLjcxYy4zMy40LjczLjc1IDEuMTcgMS4wM3ptMi4zMi00LjU0Yy0uNTQuODYtMS4wMyAxLjc2LTEuNDkgMi42OGEzIDMgMCAwIDEtLjA3LTQuNjcgMyAzIDAgMCAwIDEuNTYgMS45OXptMS4xNC0xLjdjLjM1LS41LjcyLS45OCAxLjEtMS40NmExIDEgMCAxIDAtMS4xIDEuNDV6bTUuMzQtNS43N2MtMS4wMy44Ni0yIDEuNzktMi45IDIuNzdhMyAzIDAgMCAwLTEuMTEtLjc3IDMgMyAwIDAgMSA0LTJ6bTQyLjY2IDIuNzdjLS45LS45OC0xLjg3LTEuOS0yLjktMi43N2EzIDMgMCAwIDEgNC4wMSAyIDMgMyAwIDAgMC0xLjEuNzd6bTEuMzQgMS41NGMuMzguNDguNzUuOTYgMS4xIDEuNDVhMSAxIDAgMSAwLTEuMS0xLjQ1em0zLjczIDUuODRjLS40Ni0uOTItLjk1LTEuODItMS41LTIuNjhhMyAzIDAgMCAwIDEuNTctMS45OSAzIDMgMCAwIDEtLjA3IDQuNjd6bTEuOCA0LjUzYy0uMjktLjktLjYtMS44LS45Ny0yLjY3LjQ0LS4yOC44NC0uNjMgMS4xNy0xLjAzYTIgMiAwIDAgMS0uMiAzLjd6bTEuMTQgNS41MWMtLjE0LTEuMjEtLjM1LTIuNC0uNjItMy41Ny40NS0uMTQuODYtLjM1IDEuMjMtLjYzYTIuOTkgMi45OSAwIDAgMS0uNiA0LjJ6TTI3NSAyMTRhMjkgMjkgMCAwIDAtNTcuOTcgMGg1Ny45NnpNNzIuMzMgMTk4LjEyYy0uMjEtLjMyLS4zNC0uNy0uMzQtMS4xMnYtMTJoLTJ2MTJhNC4wMSA0LjAxIDAgMCAwIDcuMDkgMi41NGMuNTctLjY5LjkxLTEuNTcuOTEtMi41NHYtMTJoLTJ2MTJhMS45OSAxLjk5IDAgMCAxLTIgMiAyIDIgMCAwIDEtMS42Ni0uODh6TTc1IDE3NmMuMzggMCAuNzQtLjA0IDEuMS0uMTJhNCA0IDAgMCAwIDYuMTkgMi40QTEzLjk0IDEzLjk0IDAgMCAxIDg0IDE4NXYyNGE2IDYgMCAwIDEtNiA2aC0zdjlhNSA1IDAgMSAxLTEwIDB2LTloLTNhNiA2IDAgMCAxLTYtNnYtMjRhMTQgMTQgMCAwIDEgMTQtMTQgNSA1IDAgMCAwIDUgNXptLTE3IDE1djEyYTEuOTkgMS45OSAwIDAgMCAxLjIyIDEuODQgMiAyIDAgMCAwIDIuNDQtLjcyYy4yMS0uMzIuMzQtLjcuMzQtMS4xMnYtMTJoMnYxMmEzLjk4IDMuOTggMCAwIDEtNS4zNSAzLjc3IDMuOTggMy45OCAwIDAgMS0uNjUtLjNWMjA5YTQgNCAwIDAgMCA0IDRoMTZhNCA0IDAgMCAwIDQtNHYtMjRjLjAxLTEuNTMtLjIzLTIuODgtLjcyLTQuMTctLjQzLjEtLjg3LjE2LTEuMjguMTdhNiA2IDAgMCAxLTUuMi0zIDcgNyAwIDAgMS02LjQ3LTQuODhBMTIgMTIgMCAwIDAgNTggMTg1djZ6bTkgMjR2OWEzIDMgMCAxIDAgNiAwdi05aC02eicvJTNFJTNDcGF0aCBkPSdNLTE3IDE5MWExIDEgMCAwIDAgMCAyaDJhMSAxIDAgMCAwIDAtMmgtMnptMTkgOWExIDEgMCAwIDEgMS0xaDJhMSAxIDAgMCAxIDAgMkgzYTEgMSAwIDAgMS0xLTF6bS0xNCA1YTEgMSAwIDAgMCAwIDJoMmExIDEgMCAwIDAgMC0yaC0yem0tMjUgMWExIDEgMCAwIDAgMCAyaDJhMSAxIDAgMCAwIDAtMmgtMnptNSA0YTEgMSAwIDAgMCAwIDJoMmExIDEgMCAwIDAgMC0yaC0yem05IDBhMSAxIDAgMCAxIDEtMWgyYTEgMSAwIDAgMSAwIDJoLTJhMSAxIDAgMCAxLTEtMXptMTUgMWExIDEgMCAwIDEgMS0xaDJhMSAxIDAgMCAxIDAgMmgtMmExIDEgMCAwIDEtMS0xem0xMi0yYTEgMSAwIDAgMCAwIDJoMmExIDEgMCAwIDAgMC0ySDR6bS0xMS0xNGExIDEgMCAwIDEgMS0xaDJhMSAxIDAgMCAxIDAgMmgtMmExIDEgMCAwIDEtMS0xem0tMTkgMGExIDEgMCAwIDAgMCAyaDJhMSAxIDAgMCAwIDAtMmgtMnptNiA1YTEgMSAwIDAgMSAxLTFoMmExIDEgMCAwIDEgMCAyaC0yYTEgMSAwIDAgMS0xLTF6bS0yNSAxNWMwLS40Ny4wMS0uOTQuMDMtMS40YTUgNSAwIDAgMS0xLjctOCAzLjk5IDMuOTkgMCAwIDEgMS44OC01LjE4IDUgNSAwIDAgMSAzLjQtNi4yMiAzIDMgMCAwIDEgMS40Ni0xLjA1IDUgNSAwIDAgMSA3Ljc2LTMuMjdBMzAuODYgMzAuODYgMCAwIDEtMTQgMTg0YzYuNzkgMCAxMy4wNiAyLjE4IDE4LjE3IDUuODhhNSA1IDAgMCAxIDcuNzYgMy4yNyAzIDMgMCAwIDEgMS40NyAxLjA1IDUgNSAwIDAgMSAzLjQgNi4yMiA0IDQgMCAwIDEgMS44NyA1LjE4IDQuOTggNC45OCAwIDAgMS0xLjcgOGMuMDIuNDYuMDMuOTMuMDMgMS40djFoLTYydi0xem0uODMtNy4xN2EzMC45IDMwLjkgMCAwIDAtLjYyIDMuNTcgMyAzIDAgMCAxLS42MS00LjJjLjM3LjI4Ljc4LjQ5IDEuMjMuNjN6bTEuNDktNC42MWMtLjM2Ljg3LS42OCAxLjc2LS45NiAyLjY4YTIgMiAwIDAgMS0uMjEtMy43MWMuMzMuNC43My43NSAxLjE3IDEuMDN6bTIuMzItNC41NGMtLjU0Ljg2LTEuMDMgMS43Ni0xLjQ5IDIuNjhhMyAzIDAgMCAxLS4wNy00LjY3IDMgMyAwIDAgMCAxLjU2IDEuOTl6bTEuMTQtMS43Yy4zNS0uNS43Mi0uOTggMS4xLTEuNDZhMSAxIDAgMSAwLTEuMSAxLjQ1em01LjM0LTUuNzdjLTEuMDMuODYtMiAxLjc5LTIuOSAyLjc3YTMgMyAwIDAgMC0xLjExLS43NyAzIDMgMCAwIDEgNC0yem00Mi42NiAyLjc3Yy0uOS0uOTgtMS44Ny0xLjktMi45LTIuNzdhMyAzIDAgMCAxIDQuMDEgMiAzIDMgMCAwIDAtMS4xLjc3em0xLjM0IDEuNTRjLjM4LjQ4Ljc1Ljk2IDEuMSAxLjQ1YTEgMSAwIDEgMC0xLjEtMS40NXptMy43MyA1Ljg0Yy0uNDYtLjkyLS45NS0xLjgyLTEuNS0yLjY4YTMgMyAwIDAgMCAxLjU3LTEuOTkgMyAzIDAgMCAxLS4wNyA0LjY3em0xLjggNC41M2MtLjI5LS45LS42LTEuOC0uOTctMi42Ny40NC0uMjguODQtLjYzIDEuMTctMS4wM2EyIDIgMCAwIDEtLjIgMy43em0xLjE0IDUuNTFjLS4xNC0xLjIxLS4zNS0yLjQtLjYyLTMuNTcuNDUtLjE0Ljg2LS4zNSAxLjIzLS42M2EyLjk5IDIuOTkgMCAwIDEtLjYgNC4yek0xNSAyMTRhMjkgMjkgMCAwIDAtNTcuOTcgMGg1Ny45NnonLyUzRSUzQy9nJTNFJTNDL2clM0UlM0Mvc3ZnJTNFXCIpO1xyXG4gIH1cclxuICBcclxuICAuZmxvb3Ige1xyXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDBiOWYyO1xyXG4gICAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJkYXRhOmltYWdlL3N2Zyt4bWwsJTNDc3ZnIHhtbG5zPSdodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Zycgd2lkdGg9JzIwJyBoZWlnaHQ9JzIwJyB2aWV3Qm94PScwIDAgODAgODAnJTNFJTNDZyBmaWxsPSclMjM5MjI3OGYnIGZpbGwtb3BhY2l0eT0nMC43MSclM0UlM0NwYXRoIGZpbGwtcnVsZT0nZXZlbm9kZCcgZD0nTTAgMGg0MHY0MEgwVjB6bTQwIDQwaDQwdjQwSDQwVjQwem0wLTQwaDJsLTIgMlYwem0wIDRsNC00aDJsLTYgNlY0em0wIDRsOC04aDJMNDAgMTBWOHptMCA0TDUyIDBoMkw0MCAxNHYtMnptMCA0TDU2IDBoMkw0MCAxOHYtMnptMCA0TDYwIDBoMkw0MCAyMnYtMnptMCA0TDY0IDBoMkw0MCAyNnYtMnptMCA0TDY4IDBoMkw0MCAzMHYtMnptMCA0TDcyIDBoMkw0MCAzNHYtMnptMCA0TDc2IDBoMkw0MCAzOHYtMnptMCA0TDgwIDB2Mkw0MiA0MGgtMnptNCAwTDgwIDR2Mkw0NiA0MGgtMnptNCAwTDgwIDh2Mkw1MCA0MGgtMnptNCAwbDI4LTI4djJMNTQgNDBoLTJ6bTQgMGwyNC0yNHYyTDU4IDQwaC0yem00IDBsMjAtMjB2Mkw2MiA0MGgtMnptNCAwbDE2LTE2djJMNjYgNDBoLTJ6bTQgMGwxMi0xMnYyTDcwIDQwaC0yem00IDBsOC04djJsLTYgNmgtMnptNCAwbDQtNHYybC0yIDJoLTJ6Jy8lM0UlM0MvZyUzRSUzQy9zdmclM0VcIik7XHJcbiAgfVxyXG4gIFxyXG4gIC53aWdnbGUge1xyXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZGJiZWY5O1xyXG4gICAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJkYXRhOmltYWdlL3N2Zyt4bWwsJTNDc3ZnIHdpZHRoPSc1MicgaGVpZ2h0PScyNicgdmlld0JveD0nMCAwIDUyIDI2JyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnJTNFJTNDZyBmaWxsPSdub25lJyBmaWxsLXJ1bGU9J2V2ZW5vZGQnJTNFJTNDZyBmaWxsPSclMjNmZjdjN2MnIGZpbGwtb3BhY2l0eT0nMSclM0UlM0NwYXRoIGQ9J00xMCAxMGMwLTIuMjEtMS43OS00LTQtNC0zLjMxNCAwLTYtMi42ODYtNi02aDJjMCAyLjIxIDEuNzkgNCA0IDQgMy4zMTQgMCA2IDIuNjg2IDYgNiAwIDIuMjEgMS43OSA0IDQgNCAzLjMxNCAwIDYgMi42ODYgNiA2IDAgMi4yMSAxLjc5IDQgNCA0djJjLTMuMzE0IDAtNi0yLjY4Ni02LTYgMC0yLjIxLTEuNzktNC00LTQtMy4zMTQgMC02LTIuNjg2LTYtNnptMjUuNDY0LTEuOTVsOC40ODYgOC40ODYtMS40MTQgMS40MTQtOC40ODYtOC40ODYgMS40MTQtMS40MTR6JyAvJTNFJTNDL2clM0UlM0MvZyUzRSUzQy9zdmclM0VcIik7XHJcbiAgfVxyXG4gIFxyXG4gIC5iYXJzIHtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZTY3YztcclxuICAgICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiZGF0YTppbWFnZS9zdmcreG1sLCUzQ3N2ZyB3aWR0aD0nNDAnIGhlaWdodD0nNDAnIHZpZXdCb3g9JzAgMCA0MCA0MCcgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJyUzRSUzQ2cgZmlsbD0nJTIzOGZlMWU3JyBmaWxsLW9wYWNpdHk9JzEnIGZpbGwtcnVsZT0nZXZlbm9kZCclM0UlM0NwYXRoIGQ9J00wIDQwTDQwIDBIMjBMMCAyME00MCA0MFYyMEwyMCA0MCcvJTNFJTNDL2clM0UlM0Mvc3ZnJTNFXCIpO1xyXG4gIH1cclxuICBcclxuICAuYnViYmxlcyB7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICNiZWZmYzI7XHJcbiAgICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybChcImRhdGE6aW1hZ2Uvc3ZnK3htbCwlM0Nzdmcgd2lkdGg9JzEwMCcgaGVpZ2h0PScxMDAnIHZpZXdCb3g9JzAgMCAxMDAgMTAwJyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnJTNFJTNDcGF0aCBkPSdNMTEgMThjMy44NjYgMCA3LTMuMTM0IDctN3MtMy4xMzQtNy03LTctNyAzLjEzNC03IDcgMy4xMzQgNyA3IDd6bTQ4IDI1YzMuODY2IDAgNy0zLjEzNCA3LTdzLTMuMTM0LTctNy03LTcgMy4xMzQtNyA3IDMuMTM0IDcgNyA3em0tNDMtN2MxLjY1NyAwIDMtMS4zNDMgMy0zcy0xLjM0My0zLTMtMy0zIDEuMzQzLTMgMyAxLjM0MyAzIDMgM3ptNjMgMzFjMS42NTcgMCAzLTEuMzQzIDMtM3MtMS4zNDMtMy0zLTMtMyAxLjM0My0zIDMgMS4zNDMgMyAzIDN6TTM0IDkwYzEuNjU3IDAgMy0xLjM0MyAzLTNzLTEuMzQzLTMtMy0zLTMgMS4zNDMtMyAzIDEuMzQzIDMgMyAzem01Ni03NmMxLjY1NyAwIDMtMS4zNDMgMy0zcy0xLjM0My0zLTMtMy0zIDEuMzQzLTMgMyAxLjM0MyAzIDMgM3pNMTIgODZjMi4yMSAwIDQtMS43OSA0LTRzLTEuNzktNC00LTQtNCAxLjc5LTQgNCAxLjc5IDQgNCA0em0yOC02NWMyLjIxIDAgNC0xLjc5IDQtNHMtMS43OS00LTQtNC00IDEuNzktNCA0IDEuNzkgNCA0IDR6bTIzLTExYzIuNzYgMCA1LTIuMjQgNS01cy0yLjI0LTUtNS01LTUgMi4yNC01IDUgMi4yNCA1IDUgNXptLTYgNjBjMi4yMSAwIDQtMS43OSA0LTRzLTEuNzktNC00LTQtNCAxLjc5LTQgNCAxLjc5IDQgNCA0em0yOSAyMmMyLjc2IDAgNS0yLjI0IDUtNXMtMi4yNC01LTUtNS01IDIuMjQtNSA1IDIuMjQgNSA1IDV6TTMyIDYzYzIuNzYgMCA1LTIuMjQgNS01cy0yLjI0LTUtNS01LTUgMi4yNC01IDUgMi4yNCA1IDUgNXptNTctMTNjMi43NiAwIDUtMi4yNCA1LTVzLTIuMjQtNS01LTUtNSAyLjI0LTUgNSAyLjI0IDUgNSA1em0tOS0yMWMxLjEwNSAwIDItLjg5NSAyLTJzLS44OTUtMi0yLTItMiAuODk1LTIgMiAuODk1IDIgMiAyek02MCA5MWMxLjEwNSAwIDItLjg5NSAyLTJzLS44OTUtMi0yLTItMiAuODk1LTIgMiAuODk1IDIgMiAyek0zNSA0MWMxLjEwNSAwIDItLjg5NSAyLTJzLS44OTUtMi0yLTItMiAuODk1LTIgMiAuODk1IDIgMiAyek0xMiA2MGMxLjEwNSAwIDItLjg5NSAyLTJzLS44OTUtMi0yLTItMiAuODk1LTIgMiAuODk1IDIgMiAyeicgZmlsbD0nJTIzZTZhZmZmJyBmaWxsLW9wYWNpdHk9JzEnIGZpbGwtcnVsZT0nZXZlbm9kZCcvJTNFJTNDL3N2ZyUzRVwiKTtcclxuICB9XHJcbiAgXHJcbiAgLnRpY1RhYyB7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmVmYWE7XHJcbiAgICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybChcImRhdGE6aW1hZ2Uvc3ZnK3htbCwlM0Nzdmcgd2lkdGg9Jzc2JyBoZWlnaHQ9Jzc2JyB2aWV3Qm94PScwIDAgNjQgNjQnIHhtbG5zPSdodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyclM0UlM0NwYXRoIGQ9J004IDE2YzQuNDE4IDAgOC0zLjU4MiA4LThzLTMuNTgyLTgtOC04LTggMy41ODItOCA4IDMuNTgyIDggOCA4em0wLTJjMy4zMTQgMCA2LTIuNjg2IDYtNnMtMi42ODYtNi02LTYtNiAyLjY4Ni02IDYgMi42ODYgNiA2IDZ6bTMzLjQxNC02bDUuOTUtNS45NUw0NS45NS42MzYgNDAgNi41ODYgMzQuMDUuNjM2IDMyLjYzNiAyLjA1IDM4LjU4NiA4bC01Ljk1IDUuOTUgMS40MTQgMS40MTRMNDAgOS40MTRsNS45NSA1Ljk1IDEuNDE0LTEuNDE0TDQxLjQxNCA4ek00MCA0OGM0LjQxOCAwIDgtMy41ODIgOC04cy0zLjU4Mi04LTgtOC04IDMuNTgyLTggOCAzLjU4MiA4IDggOHptMC0yYzMuMzE0IDAgNi0yLjY4NiA2LTZzLTIuNjg2LTYtNi02LTYgMi42ODYtNiA2IDIuNjg2IDYgNiA2ek05LjQxNCA0MGw1Ljk1LTUuOTUtMS40MTQtMS40MTRMOCAzOC41ODZsLTUuOTUtNS45NUwuNjM2IDM0LjA1IDYuNTg2IDQwbC01Ljk1IDUuOTUgMS40MTQgMS40MTRMOCA0MS40MTRsNS45NSA1Ljk1IDEuNDE0LTEuNDE0TDkuNDE0IDQweicgZmlsbD0nJTIzZmZhZGFkJyBmaWxsLW9wYWNpdHk9JzAuODQnIGZpbGwtcnVsZT0nZXZlbm9kZCcvJTNFJTNDL3N2ZyUzRVwiKTtcclxuICB9XHJcbiAgXHJcbiAgLnppZ1phZyB7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICMwMGRhYzM7XHJcbiAgICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybChcImRhdGE6aW1hZ2Uvc3ZnK3htbCwlM0Nzdmcgd2lkdGg9JzQwJyBoZWlnaHQ9JzEyJyB2aWV3Qm94PScwIDAgNDAgMTInIHhtbG5zPSdodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyclM0UlM0NwYXRoIGQ9J00wIDYuMTcyTDYuMTcyIDBoNS42NTZMMCAxMS44MjhWNi4xNzJ6bTQwIDUuNjU2TDI4LjE3MiAwaDUuNjU2TDQwIDYuMTcydjUuNjU2ek02LjE3MiAxMmwxMi0xMmgzLjY1NmwxMiAxMmgtNS42NTZMMjAgMy44MjggMTEuODI4IDEySDYuMTcyem0xMiAwTDIwIDEwLjE3MiAyMS44MjggMTJoLTMuNjU2eicgZmlsbD0nJTIzMDA4Mzg2JyBmaWxsLW9wYWNpdHk9JzAuNycgZmlsbC1ydWxlPSdldmVub2RkJy8lM0UlM0Mvc3ZnJTNFXCIpO1xyXG4gIH1cclxuICBcclxuICAuc3RyaXBlcyB7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XHJcbiAgICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybChcImRhdGE6aW1hZ2Uvc3ZnK3htbCwlM0Nzdmcgd2lkdGg9JzI1JyBoZWlnaHQ9JzEnIHZpZXdCb3g9JzAgMCA0MCAxJyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnJTNFJTNDcGF0aCBkPSdNMCAwaDIwdjFIMHonIGZpbGw9JyUyM2QwOWFmMycgZmlsbC1vcGFjaXR5PScwLjU0JyBmaWxsLXJ1bGU9J2V2ZW5vZGQnLyUzRSUzQy9zdmclM0VcIik7XHJcbiAgfVxyXG4gIFxyXG4gIC5jbG91ZHMge1xyXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjOTU5YmI1O1xyXG4gICAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJkYXRhOmltYWdlL3N2Zyt4bWwsJTNDc3ZnIHhtbG5zPSdodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Zycgdmlld0JveD0nMCAwIDU2IDI4JyB3aWR0aD0nNTYnIGhlaWdodD0nMjgnJTNFJTNDcGF0aCBmaWxsPSclMjNmMGQ1MTknIGZpbGwtb3BhY2l0eT0nMC44OScgZD0nTTU2IDI2djJoLTcuNzVjMi4zLTEuMjcgNC45NC0yIDcuNzUtMnptLTI2IDJhMiAyIDAgMSAwLTQgMGgtNC4wOUEyNS45OCAyNS45OCAwIDAgMCAwIDE2di0yYy42NyAwIDEuMzQuMDIgMiAuMDdWMTRhMiAyIDAgMCAwLTItMnYtMmE0IDQgMCAwIDEgMy45OCAzLjYgMjguMDkgMjguMDkgMCAwIDEgMi44LTMuODZBOCA4IDAgMCAwIDAgNlY0YTkuOTkgOS45OSAwIDAgMSA4LjE3IDQuMjNjLjk0LS45NSAxLjk2LTEuODMgMy4wMy0yLjYzQTEzLjk4IDEzLjk4IDAgMCAwIDAgMGg3Ljc1YzIgMS4xIDMuNzMgMi42MyA1LjEgNC40NSAxLjEyLS43MiAyLjMtMS4zNyAzLjUzLTEuOTNBMjAuMSAyMC4xIDAgMCAwIDE0LjI4IDBoMi43Yy40NS41Ni44OCAxLjE0IDEuMjkgMS43NCAxLjMtLjQ4IDIuNjMtLjg3IDQtMS4xNS0uMTEtLjItLjIzLS40LS4zNi0uNTlIMjZ2LjA3YTI4LjQgMjguNCAwIDAgMSA0IDBWMGg0LjA5bC0uMzcuNTljMS4zOC4yOCAyLjcyLjY3IDQuMDEgMS4xNS40LS42Ljg0LTEuMTggMS4zLTEuNzRoMi42OWEyMC4xIDIwLjEgMCAwIDAtMi4xIDIuNTJjMS4yMy41NiAyLjQxIDEuMiAzLjU0IDEuOTNBMTYuMDggMTYuMDggMCAwIDEgNDguMjUgMEg1NmMtNC41OCAwLTguNjUgMi4yLTExLjIgNS42IDEuMDcuOCAyLjA5IDEuNjggMy4wMyAyLjYzQTkuOTkgOS45OSAwIDAgMSA1NiA0djJhOCA4IDAgMCAwLTYuNzcgMy43NGMxLjAzIDEuMiAxLjk3IDIuNSAyLjc5IDMuODZBNCA0IDAgMCAxIDU2IDEwdjJhMiAyIDAgMCAwLTIgMi4wNyAyOC40IDI4LjQgMCAwIDEgMi0uMDd2MmMtOS4yIDAtMTcuMyA0Ljc4LTIxLjkxIDEySDMwek03Ljc1IDI4SDB2LTJjMi44MSAwIDUuNDYuNzMgNy43NSAyek01NiAyMHYyYy01LjYgMC0xMC42NSAyLjMtMTQuMjggNmgtMi43YzQuMDQtNC44OSAxMC4xNS04IDE2Ljk4LTh6bS0zOS4wMyA4aC0yLjY5QzEwLjY1IDI0LjMgNS42IDIyIDAgMjJ2LTJjNi44MyAwIDEyLjk0IDMuMTEgMTYuOTcgOHptMTUuMDEtLjRhMjguMDkgMjguMDkgMCAwIDEgMi44LTMuODYgOCA4IDAgMCAwLTEzLjU1IDBjMS4wMyAxLjIgMS45NyAyLjUgMi43OSAzLjg2YTQgNCAwIDAgMSA3Ljk2IDB6bTE0LjI5LTExLjg2YzEuMy0uNDggMi42My0uODcgNC0xLjE1YTI1Ljk5IDI1Ljk5IDAgMCAwLTQ0LjU1IDBjMS4zOC4yOCAyLjcyLjY3IDQuMDEgMS4xNWEyMS45OCAyMS45OCAwIDAgMSAzNi41NCAwem0tNS40MyAyLjcxYzEuMTMtLjcyIDIuMy0xLjM3IDMuNTQtMS45M2ExOS45OCAxOS45OCAwIDAgMC0zMi43NiAwYzEuMjMuNTYgMi40MSAxLjIgMy41NCAxLjkzYTE1Ljk4IDE1Ljk4IDAgMCAxIDI1LjY4IDB6bS00LjY3IDMuNzhjLjk0LS45NSAxLjk2LTEuODMgMy4wMy0yLjYzYTEzLjk4IDEzLjk4IDAgMCAwLTIyLjQgMGMxLjA3LjggMi4wOSAxLjY4IDMuMDMgMi42M2E5Ljk5IDkuOTkgMCAwIDEgMTYuMzQgMHonJTNFJTNDL3BhdGglM0UlM0Mvc3ZnJTNFXCIpO1xyXG4gIH1cclxuICBcclxuICAuYXp0ZWMge1xyXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZDU5MjQyO1xyXG4gICAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJkYXRhOmltYWdlL3N2Zyt4bWwsJTNDc3ZnIHdpZHRoPScxMDAnIGhlaWdodD0nMjAnIHZpZXdCb3g9JzAgMCAxMDAgMjAnIHhtbG5zPSdodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyclM0UlM0NwYXRoIGQ9J00yMS4xODQgMjBjLjM1Ny0uMTMuNzItLjI2NCAxLjA4OC0uNDAybDEuNzY4LS42NjFDMzMuNjQgMTUuMzQ3IDM5LjY0NyAxNCA1MCAxNGMxMC4yNzEgMCAxNS4zNjIgMS4yMjIgMjQuNjI5IDQuOTI4Ljk1NS4zODMgMS44NjkuNzQgMi43NSAxLjA3Mmg2LjIyNWMtMi41MS0uNzMtNS4xMzktMS42OTEtOC4yMzMtMi45MjhDNjUuODg4IDEzLjI3OCA2MC41NjIgMTIgNTAgMTJjLTEwLjYyNiAwLTE2Ljg1NSAxLjM5Ny0yNi42NiA1LjA2M2wtMS43NjcuNjYyYy0yLjQ3NS45MjMtNC42NiAxLjY3NC02LjcyNCAyLjI3NWg2LjMzNXptMC0yMEMxMy4yNTggMi44OTIgOC4wNzcgNCAwIDRWMmM1Ljc0NCAwIDkuOTUxLS41NzQgMTQuODUtMmg2LjMzNHpNNzcuMzggMEM4NS4yMzkgMi45NjYgOTAuNTAyIDQgMTAwIDRWMmMtNi44NDIgMC0xMS4zODYtLjU0Mi0xNi4zOTYtMmgtNi4yMjV6TTAgMTRjOC40NCAwIDEzLjcxOC0xLjIxIDIyLjI3Mi00LjQwMmwxLjc2OC0uNjYxQzMzLjY0IDUuMzQ3IDM5LjY0NyA0IDUwIDRjMTAuMjcxIDAgMTUuMzYyIDEuMjIyIDI0LjYyOSA0LjkyOEM4NC4xMTIgMTIuNzIyIDg5LjQzOCAxNCAxMDAgMTR2LTJjLTEwLjI3MSAwLTE1LjM2Mi0xLjIyMi0yNC42MjktNC45MjhDNjUuODg4IDMuMjc4IDYwLjU2MiAyIDUwIDIgMzkuMzc0IDIgMzMuMTQ1IDMuMzk3IDIzLjM0IDcuMDYzbC0xLjc2Ny42NjJDMTMuMjIzIDEwLjg0IDguMTYzIDEyIDAgMTJ2MnonIGZpbGw9JyUyMzBkMzdjMicgZmlsbC1vcGFjaXR5PScwLjQnIGZpbGwtcnVsZT0nZXZlbm9kZCcvJTNFJTNDL3N2ZyUzRVwiKTtcclxuICB9XHJcbiAgXHJcbiAgLmNpcmN1aXQge1xyXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDBiNDk3O1xyXG4gICAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJkYXRhOmltYWdlL3N2Zyt4bWwsJTNDc3ZnIHhtbG5zPSdodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Zycgdmlld0JveD0nMCAwIDMwNCAzMDQnIHdpZHRoPScxNTAnIGhlaWdodD0nMTUwJyUzRSUzQ3BhdGggZmlsbD0nJTIzMzMzMzMzJyBmaWxsLW9wYWNpdHk9JzAuNCcgZD0nTTQ0LjEgMjI0YTUgNSAwIDEgMSAwIDJIMHYtMmg0NC4xem0xNjAgNDhhNSA1IDAgMSAxIDAgMkg4MnYtMmgxMjIuMXptNTcuOC00NmE1IDUgMCAxIDEgMC0ySDMwNHYyaC00Mi4xem0wIDE2YTUgNSAwIDEgMSAwLTJIMzA0djJoLTQyLjF6bTYuMi0xMTRhNSA1IDAgMSAxIDAgMmgtODYuMmE1IDUgMCAxIDEgMC0yaDg2LjJ6bS0yNTYtNDhhNSA1IDAgMSAxIDAgMkgwdi0yaDEyLjF6bTE4NS44IDM0YTUgNSAwIDEgMSAwLTJoODYuMmE1IDUgMCAxIDEgMCAyaC04Ni4yek0yNTggMTIuMWE1IDUgMCAxIDEtMiAwVjBoMnYxMi4xem0tNjQgMjA4YTUgNSAwIDEgMS0yIDB2LTU0LjJhNSA1IDAgMSAxIDIgMHY1NC4yem00OC0xOTguMlY4MGg2MnYyaC02NFYyMS45YTUgNSAwIDEgMSAyIDB6bTE2IDE2VjY0aDQ2djJoLTQ4VjM3LjlhNSA1IDAgMSAxIDIgMHptLTEyOCA5NlYyMDhoMTZ2MTIuMWE1IDUgMCAxIDEtMiAwVjIxMGgtMTZ2LTc2LjFhNSA1IDAgMSAxIDIgMHptLTUuOS0yMS45YTUgNSAwIDEgMSAwIDJIMTE0djQ4SDg1LjlhNSA1IDAgMSAxIDAtMkgxMTJ2LTQ4aDEyLjF6bS02LjIgMTMwYTUgNSAwIDEgMSAwLTJIMTc2di03NC4xYTUgNSAwIDEgMSAyIDBWMjQyaC02MC4xem0tMTYtNjRhNSA1IDAgMSAxIDAtMkgxMTR2NDhoMTAuMWE1IDUgMCAxIDEgMCAySDExMnYtNDhoLTEwLjF6TTY2IDI4NC4xYTUgNSAwIDEgMS0yIDBWMjc0SDUwdjMwaC0ydi0zMmgxOHYxMi4xek0yMzYuMSAxNzZhNSA1IDAgMSAxIDAgMkgyMjZ2OTRoNDh2MzJoLTJ2LTMwaC00OHYtOThoMTIuMXptMjUuOC0zMGE1IDUgMCAxIDEgMC0ySDI3NHY0NC4xYTUgNSAwIDEgMS0yIDBWMTQ2aC0xMC4xem0tNjQgOTZhNSA1IDAgMSAxIDAtMkgyMDh2LTgwaDE2di0xNGgtNDIuMWE1IDUgMCAxIDEgMC0ySDIyNnYxOGgtMTZ2ODBoLTEyLjF6bTg2LjItMjEwYTUgNSAwIDEgMSAwIDJIMjcyVjBoMnYzMmgxMC4xek05OCAxMDEuOVYxNDZINTMuOWE1IDUgMCAxIDEgMC0ySDk2di00Mi4xYTUgNSAwIDEgMSAyIDB6TTUzLjkgMzRhNSA1IDAgMSAxIDAtMkg4MFYwaDJ2MzRINTMuOXptNjAuMSAzLjlWNjZIODJ2NjRINjkuOWE1IDUgMCAxIDEgMC0ySDgwVjY0aDMyVjM3LjlhNSA1IDAgMSAxIDIgMHpNMTAxLjkgODJhNSA1IDAgMSAxIDAtMkgxMjhWMzcuOWE1IDUgMCAxIDEgMiAwVjgyaC0yOC4xem0xNi02NGE1IDUgMCAxIDEgMC0ySDE0NnY0NC4xYTUgNSAwIDEgMS0yIDBWMThoLTI2LjF6bTEwMi4yIDI3MGE1IDUgMCAxIDEgMCAySDk4djE0aC0ydi0xNmgxMjQuMXpNMjQyIDE0OS45VjE2MGgxNnYzNGgtMTZ2NjJoNDh2NDhoLTJ2LTQ2aC00OHYtNjZoMTZ2LTMwaC0xNnYtMTIuMWE1IDUgMCAxIDEgMiAwek01My45IDE4YTUgNSAwIDEgMSAwLTJINjRWMkg0OFYwaDE4djE4SDUzLjl6bTExMiAzMmE1IDUgMCAxIDEgMC0ySDE5MlYwaDUwdjJoLTQ4djQ4aC0yOC4xem0tNDgtNDhhNSA1IDAgMCAxLTkuOC0yaDIuMDdhMyAzIDAgMSAwIDUuNjYgMEgxNzh2MzRoLTE4VjIxLjlhNSA1IDAgMSAxIDIgMFYzMmgxNFYyaC01OC4xem0wIDk2YTUgNSAwIDEgMSAwLTJIMTM3bDMyLTMyaDM5VjIxLjlhNSA1IDAgMSAxIDIgMFY2NmgtNDAuMTdsLTMyIDMySDExNy45em0yOC4xIDkwLjFhNSA1IDAgMSAxLTIgMHYtNzYuNTFMMTc1LjU5IDgwSDIyNFYyMS45YTUgNSAwIDEgMSAyIDBWODJoLTQ5LjU5TDE0NiAxMTIuNDF2NzUuNjl6bTE2IDMyYTUgNSAwIDEgMS0yIDB2LTk5LjUxTDE4NC41OSA5NkgzMDAuMWE1IDUgMCAwIDEgMy45LTMuOXYyLjA3YTMgMyAwIDAgMCAwIDUuNjZ2Mi4wN2E1IDUgMCAwIDEtMy45LTMuOUgxODUuNDFMMTYyIDEyMS40MXY5OC42OXptLTE0NC02NGE1IDUgMCAxIDEtMiAwdi0zLjUxbDQ4LTQ4VjQ4aDMyVjBoMnY1MEg2NnY1NS40MWwtNDggNDh2Mi42OXpNNTAgNTMuOXY0My41MWwtNDggNDhWMjA4aDI2LjFhNSA1IDAgMSAxIDAgMkgwdi02NS40MWw0OC00OFY1My45YTUgNSAwIDEgMSAyIDB6bS0xNiAxNlY4OS40MWwtMzQgMzR2LTIuODJsMzItMzJWNjkuOWE1IDUgMCAxIDEgMiAwek0xMi4xIDMyYTUgNSAwIDEgMSAwIDJIOS40MUwwIDQzLjQxVjQwLjZMOC41OSAzMmgzLjUxem0yNjUuOCAxOGE1IDUgMCAxIDEgMC0yaDE4LjY5bDcuNDEtNy40MXYyLjgyTDI5Ny40MSA1MEgyNzcuOXptLTE2IDE2MGE1IDUgMCAxIDEgMC0ySDI4OHYtNzEuNDFsMTYtMTZ2Mi44MmwtMTQgMTRWMjEwaC0yOC4xem0tMjA4IDMyYTUgNSAwIDEgMSAwLTJINjR2LTIyLjU5TDQwLjU5IDE5NEgyMS45YTUgNSAwIDEgMSAwLTJINDEuNDFMNjYgMjE2LjU5VjI0Mkg1My45em0xNTAuMiAxNGE1IDUgMCAxIDEgMCAySDk2di01Ni42TDU2LjYgMTYySDM3LjlhNSA1IDAgMSAxIDAtMmgxOS41TDk4IDIwMC42VjI1NmgxMDYuMXptLTE1MC4yIDJhNSA1IDAgMSAxIDAtMkg4MHYtNDYuNTlMNDguNTkgMTc4SDIxLjlhNSA1IDAgMSAxIDAtMkg0OS40MUw4MiAyMDguNTlWMjU4SDUzLjl6TTM0IDM5Ljh2MS42MUw5LjQxIDY2SDB2LTJoOC41OUwzMiA0MC41OVYwaDJ2MzkuOHpNMiAzMDAuMWE1IDUgMCAwIDEgMy45IDMuOUgzLjgzQTMgMyAwIDAgMCAwIDMwMi4xN1YyNTZoMTh2NDhoLTJ2LTQ2SDJ2NDIuMXpNMzQgMjQxdjYzaC0ydi02Mkgwdi0yaDM0djF6TTE3IDE4SDB2LTJoMTZWMGgydjE4aC0xem0yNzMtMmgxNHYyaC0xNlYwaDJ2MTZ6bS0zMiAyNzN2MTVoLTJ2LTE0aC0xNHYxNGgtMnYtMTZoMTh2MXpNMCA5Mi4xQTUuMDIgNS4wMiAwIDAgMSA2IDk3YTUgNSAwIDAgMS02IDQuOXYtMi4wN2EzIDMgMCAxIDAgMC01LjY2VjkyLjF6TTgwIDI3MmgydjMyaC0ydi0zMnptMzcuOSAzMmgtMi4wN2EzIDMgMCAwIDAtNS42NiAwaC0yLjA3YTUgNSAwIDAgMSA5LjggMHpNNS45IDBBNS4wMiA1LjAyIDAgMCAxIDAgNS45VjMuODNBMyAzIDAgMCAwIDMuODMgMEg1Ljl6bTI5NC4yIDBoMi4wN0EzIDMgMCAwIDAgMzA0IDMuODNWNS45YTUgNSAwIDAgMS0zLjktNS45em0zLjkgMzAwLjF2Mi4wN2EzIDMgMCAwIDAtMS44MyAxLjgzaC0yLjA3YTUgNSAwIDAgMSAzLjktMy45ek05NyAxMDBhMyAzIDAgMSAwIDAtNiAzIDMgMCAwIDAgMCA2em0wLTE2YTMgMyAwIDEgMCAwLTYgMyAzIDAgMCAwIDAgNnptMTYgMTZhMyAzIDAgMSAwIDAtNiAzIDMgMCAwIDAgMCA2em0xNiAxNmEzIDMgMCAxIDAgMC02IDMgMyAwIDAgMCAwIDZ6bTAgMTZhMyAzIDAgMSAwIDAtNiAzIDMgMCAwIDAgMCA2em0tNDggMzJhMyAzIDAgMSAwIDAtNiAzIDMgMCAwIDAgMCA2em0xNiAxNmEzIDMgMCAxIDAgMC02IDMgMyAwIDAgMCAwIDZ6bTMyIDQ4YTMgMyAwIDEgMCAwLTYgMyAzIDAgMCAwIDAgNnptLTE2IDE2YTMgMyAwIDEgMCAwLTYgMyAzIDAgMCAwIDAgNnptMzItMTZhMyAzIDAgMSAwIDAtNiAzIDMgMCAwIDAgMCA2em0wLTMyYTMgMyAwIDEgMCAwLTYgMyAzIDAgMCAwIDAgNnptMTYgMzJhMyAzIDAgMSAwIDAtNiAzIDMgMCAwIDAgMCA2em0zMiAxNmEzIDMgMCAxIDAgMC02IDMgMyAwIDAgMCAwIDZ6bTAtMTZhMyAzIDAgMSAwIDAtNiAzIDMgMCAwIDAgMCA2em0tMTYtNjRhMyAzIDAgMSAwIDAtNiAzIDMgMCAwIDAgMCA2em0xNiAwYTMgMyAwIDEgMCAwLTYgMyAzIDAgMCAwIDAgNnptMTYgOTZhMyAzIDAgMSAwIDAtNiAzIDMgMCAwIDAgMCA2em0wIDE2YTMgMyAwIDEgMCAwLTYgMyAzIDAgMCAwIDAgNnptMTYgMTZhMyAzIDAgMSAwIDAtNiAzIDMgMCAwIDAgMCA2em0xNi0xNDRhMyAzIDAgMSAwIDAtNiAzIDMgMCAwIDAgMCA2em0wIDMyYTMgMyAwIDEgMCAwLTYgMyAzIDAgMCAwIDAgNnptMTYtMzJhMyAzIDAgMSAwIDAtNiAzIDMgMCAwIDAgMCA2em0xNi0xNmEzIDMgMCAxIDAgMC02IDMgMyAwIDAgMCAwIDZ6bS05NiAwYTMgMyAwIDEgMCAwLTYgMyAzIDAgMCAwIDAgNnptMCAxNmEzIDMgMCAxIDAgMC02IDMgMyAwIDAgMCAwIDZ6bTE2LTMyYTMgMyAwIDEgMCAwLTYgMyAzIDAgMCAwIDAgNnptOTYgMGEzIDMgMCAxIDAgMC02IDMgMyAwIDAgMCAwIDZ6bS0xNi02NGEzIDMgMCAxIDAgMC02IDMgMyAwIDAgMCAwIDZ6bTE2LTE2YTMgMyAwIDEgMCAwLTYgMyAzIDAgMCAwIDAgNnptLTMyIDBhMyAzIDAgMSAwIDAtNiAzIDMgMCAwIDAgMCA2em0wLTE2YTMgMyAwIDEgMCAwLTYgMyAzIDAgMCAwIDAgNnptLTE2IDBhMyAzIDAgMSAwIDAtNiAzIDMgMCAwIDAgMCA2em0tMTYgMGEzIDMgMCAxIDAgMC02IDMgMyAwIDAgMCAwIDZ6bS0xNiAwYTMgMyAwIDEgMCAwLTYgMyAzIDAgMCAwIDAgNnpNNDkgMzZhMyAzIDAgMSAwIDAtNiAzIDMgMCAwIDAgMCA2em0tMzIgMGEzIDMgMCAxIDAgMC02IDMgMyAwIDAgMCAwIDZ6bTMyIDE2YTMgMyAwIDEgMCAwLTYgMyAzIDAgMCAwIDAgNnpNMzMgNjhhMyAzIDAgMSAwIDAtNiAzIDMgMCAwIDAgMCA2em0xNi00OGEzIDMgMCAxIDAgMC02IDMgMyAwIDAgMCAwIDZ6bTAgMjQwYTMgMyAwIDEgMCAwLTYgMyAzIDAgMCAwIDAgNnptMTYgMzJhMyAzIDAgMSAwIDAtNiAzIDMgMCAwIDAgMCA2em0tMTYtNjRhMyAzIDAgMSAwIDAtNiAzIDMgMCAwIDAgMCA2em0wIDE2YTMgMyAwIDEgMCAwLTYgMyAzIDAgMCAwIDAgNnptLTE2LTMyYTMgMyAwIDEgMCAwLTYgMyAzIDAgMCAwIDAgNnptODAtMTc2YTMgMyAwIDEgMCAwLTYgMyAzIDAgMCAwIDAgNnptMTYgMGEzIDMgMCAxIDAgMC02IDMgMyAwIDAgMCAwIDZ6bS0xNi0xNmEzIDMgMCAxIDAgMC02IDMgMyAwIDAgMCAwIDZ6bTMyIDQ4YTMgMyAwIDEgMCAwLTYgMyAzIDAgMCAwIDAgNnptMTYtMTZhMyAzIDAgMSAwIDAtNiAzIDMgMCAwIDAgMCA2em0wLTMyYTMgMyAwIDEgMCAwLTYgMyAzIDAgMCAwIDAgNnptMTEyIDE3NmEzIDMgMCAxIDAgMC02IDMgMyAwIDAgMCAwIDZ6bS0xNiAxNmEzIDMgMCAxIDAgMC02IDMgMyAwIDAgMCAwIDZ6bTAgMTZhMyAzIDAgMSAwIDAtNiAzIDMgMCAwIDAgMCA2em0wIDE2YTMgMyAwIDEgMCAwLTYgMyAzIDAgMCAwIDAgNnpNMTcgMTgwYTMgMyAwIDEgMCAwLTYgMyAzIDAgMCAwIDAgNnptMCAxNmEzIDMgMCAxIDAgMC02IDMgMyAwIDAgMCAwIDZ6bTAtMzJhMyAzIDAgMSAwIDAtNiAzIDMgMCAwIDAgMCA2em0xNiAwYTMgMyAwIDEgMCAwLTYgMyAzIDAgMCAwIDAgNnpNMTcgODRhMyAzIDAgMSAwIDAtNiAzIDMgMCAwIDAgMCA2em0zMiA2NGEzIDMgMCAxIDAgMC02IDMgMyAwIDAgMCAwIDZ6bTE2LTE2YTMgMyAwIDEgMCAwLTYgMyAzIDAgMCAwIDAgNnonJTNFJTNDL3BhdGglM0UlM0Mvc3ZnJTNFXCIpO1xyXG4gIH1cclxuICBcclxuICAuZG90cyB7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmNiMDU7XHJcbiAgICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybChcImRhdGE6aW1hZ2Uvc3ZnK3htbCwlM0Nzdmcgd2lkdGg9JzE2JyBoZWlnaHQ9JzE2JyB2aWV3Qm94PScwIDAgMjAgMjAnIHhtbG5zPSdodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyclM0UlM0NnIGZpbGw9JyUyMzAwYTk5ZCcgZmlsbC1vcGFjaXR5PScwLjcxJyBmaWxsLXJ1bGU9J2V2ZW5vZGQnJTNFJTNDY2lyY2xlIGN4PSczJyBjeT0nMycgcj0nMycvJTNFJTNDY2lyY2xlIGN4PScxMycgY3k9JzEzJyByPSczJy8lM0UlM0MvZyUzRSUzQy9zdmclM0VcIik7XHJcbiAgfVxyXG4gIFxyXG4gIC5saW5lcyB7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICNlZmVmZWY7XHJcbiAgICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybChcImRhdGE6aW1hZ2Uvc3ZnK3htbCwlM0Nzdmcgd2lkdGg9JzgwJyBoZWlnaHQ9JzgwJyB2aWV3Qm94PScwIDAgMTIwIDEyMCcgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJyUzRSUzQ3BhdGggZD0nTTkgMGgydjIwSDlWMHptMjUuMTM0Ljg0bDEuNzMyIDEtMTAgMTcuMzItMS43MzItMSAxMC0xNy4zMnptLTIwIDIwbDEuNzMyIDEtMTAgMTcuMzItMS43MzItMSAxMC0xNy4zMnpNNTguMTYgNC4xMzRsMSAxLjczMi0xNy4zMiAxMC0xLTEuNzMyIDE3LjMyLTEwem0tNDAgNDBsMSAxLjczMi0xNy4zMiAxMC0xLTEuNzMyIDE3LjMyLTEwek04MCA5djJINjBWOWgyMHpNMjAgNjl2Mkgwdi0yaDIwem03OS4zMi01NWwtMSAxLjczMi0xNy4zMi0xMEw4MiA0bDE3LjMyIDEwem0tODAgODBsLTEgMS43MzItMTcuMzItMTBMMiA4NGwxNy4zMiAxMHptOTYuNTQ2LTc1Ljg0bC0xLjczMiAxLTEwLTE3LjMyIDEuNzMyLTEgMTAgMTcuMzJ6bS0xMDAgMTAwbC0xLjczMiAxLTEwLTE3LjMyIDEuNzMyLTEgMTAgMTcuMzJ6TTM4LjE2IDI0LjEzNGwxIDEuNzMyLTE3LjMyIDEwLTEtMS43MzIgMTcuMzItMTB6TTYwIDI5djJINDB2LTJoMjB6bTE5LjMyIDVsLTEgMS43MzItMTcuMzItMTBMNjIgMjRsMTcuMzIgMTB6bTE2LjU0NiA0LjE2bC0xLjczMiAxLTEwLTE3LjMyIDEuNzMyLTEgMTAgMTcuMzJ6TTExMSA0MGgtMlYyMGgydjIwem0zLjEzNC44NGwxLjczMiAxLTEwIDE3LjMyLTEuNzMyLTEgMTAtMTcuMzJ6TTQwIDQ5djJIMjB2LTJoMjB6bTE5LjMyIDVsLTEgMS43MzItMTcuMzItMTBMNDIgNDRsMTcuMzIgMTB6bTE2LjU0NiA0LjE2bC0xLjczMiAxLTEwLTE3LjMyIDEuNzMyLTEgMTAgMTcuMzJ6TTkxIDYwaC0yVjQwaDJ2MjB6bTMuMTM0Ljg0bDEuNzMyIDEtMTAgMTcuMzItMS43MzItMSAxMC0xNy4zMnptMjQuMDI2IDMuMjk0bDEgMS43MzItMTcuMzIgMTAtMS0xLjczMiAxNy4zMi0xMHpNMzkuMzIgNzRsLTEgMS43MzItMTcuMzItMTBMMjIgNjRsMTcuMzIgMTB6bTE2LjU0NiA0LjE2bC0xLjczMiAxLTEwLTE3LjMyIDEuNzMyLTEgMTAgMTcuMzJ6TTcxIDgwaC0yVjYwaDJ2MjB6bTMuMTM0Ljg0bDEuNzMyIDEtMTAgMTcuMzItMS43MzItMSAxMC0xNy4zMnptMjQuMDI2IDMuMjk0bDEgMS43MzItMTcuMzIgMTAtMS0xLjczMiAxNy4zMi0xMHpNMTIwIDg5djJoLTIwdi0yaDIwem0tODQuMTM0IDkuMTZsLTEuNzMyIDEtMTAtMTcuMzIgMS43MzItMSAxMCAxNy4zMnpNNTEgMTAwaC0yVjgwaDJ2MjB6bTMuMTM0Ljg0bDEuNzMyIDEtMTAgMTcuMzItMS43MzItMSAxMC0xNy4zMnptMjQuMDI2IDMuMjk0bDEgMS43MzItMTcuMzIgMTAtMS0xLjczMiAxNy4zMi0xMHpNMTAwIDEwOXYySDgwdi0yaDIwem0xOS4zMiA1bC0xIDEuNzMyLTE3LjMyLTEwIDEtMS43MzIgMTcuMzIgMTB6TTMxIDEyMGgtMnYtMjBoMnYyMHonIGZpbGw9JyUyM2VmYjRhMycgZmlsbC1vcGFjaXR5PScwLjg0JyBmaWxsLXJ1bGU9J2V2ZW5vZGQnLyUzRSUzQy9zdmclM0VcIik7XHJcbiAgfVxyXG4gIFxyXG4gIC5sZWFmIHtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2I1Y2NiZjtcclxuICAgICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiZGF0YTppbWFnZS9zdmcreG1sLCUzQ3N2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHZpZXdCb3g9JzAgMCA4MCA0MCcgd2lkdGg9JzYwJyBoZWlnaHQ9JzMwJyUzRSUzQ3BhdGggZmlsbD0nJTIzNDA1ODRhJyBmaWxsLW9wYWNpdHk9JzAuNDcnIGQ9J00wIDQwYTE5Ljk2IDE5Ljk2IDAgMCAxIDUuOS0xNC4xMSAyMC4xNyAyMC4xNyAwIDAgMSAxOS40NC01LjJBMjAgMjAgMCAwIDEgMjAuMiA0MEgwek02NS4zMi43NUEyMC4wMiAyMC4wMiAwIDAgMSA0MC44IDI1LjI2IDIwLjAyIDIwLjAyIDAgMCAxIDY1LjMyLjc2ek0uMDcgMGgyMC4xbC0uMDguMDdBMjAuMDIgMjAuMDIgMCAwIDEgLjc1IDUuMjUgMjAuMDggMjAuMDggMCAwIDEgLjA3IDB6bTEuOTQgNDBoMi41M2w0LjI2LTQuMjR2LTkuNzhBMTcuOTYgMTcuOTYgMCAwIDAgMiA0MHptNS4zOCAwaDkuOGExNy45OCAxNy45OCAwIDAgMCA2LjY3LTE2LjQyTDcuNCA0MHptMy40My0xNS40MnY5LjE3bDExLjYyLTExLjU5Yy0zLjk3LS41LTguMDguMy0xMS42MiAyLjQyem0zMi44Ni0uNzhBMTggMTggMCAwIDAgNjMuODUgMy42M0w0My42OCAyMy44em03LjItMTkuMTd2OS4xNUw2Mi40MyAyLjIyYy0zLjk2LS41LTguMDUuMy0xMS41NyAyLjR6bS0zLjQ5IDIuNzJjLTQuMSA0LjEtNS44MSA5LjY5LTUuMTMgMTUuMDNsNi42MS02LjZWNi4wMmMtLjUxLjQxLTEgLjg1LTEuNDggMS4zM3pNMTcuMTggMEg3LjQyTDMuNjQgMy43OEExOCAxOCAwIDAgMCAxNy4xOCAwek0yLjA4IDBjLS4wMS44LjA0IDEuNTguMTQgMi4zN0w0LjU5IDBIMi4wN3onJTNFJTNDL3BhdGglM0UlM0Mvc3ZnJTNFXCIpO1xyXG4gIH1cclxuICBcclxuICAub3ZlcmxhcCB7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmFkZmY7XHJcbiAgICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybChcImRhdGE6aW1hZ2Uvc3ZnK3htbCwlM0Nzdmcgd2lkdGg9JzYwJyBoZWlnaHQ9JzYwJyB2aWV3Qm94PScwIDAgODAgODAnIHhtbG5zPSdodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyclM0UlM0NnIGZpbGw9J25vbmUnIGZpbGwtcnVsZT0nZXZlbm9kZCclM0UlM0NnIGZpbGw9JyUyMzJlNzhmZicgZmlsbC1vcGFjaXR5PScwLjgyJyUzRSUzQ3BhdGggZD0nTTUwIDUwYzAtNS41MjMgNC40NzctMTAgMTAtMTBzMTAgNC40NzcgMTAgMTAtNC40NzcgMTAtMTAgMTBjMCA1LjUyMy00LjQ3NyAxMC0xMCAxMHMtMTAtNC40NzctMTAtMTAgNC40NzctMTAgMTAtMTB6TTEwIDEwYzAtNS41MjMgNC40NzctMTAgMTAtMTBzMTAgNC40NzcgMTAgMTAtNC40NzcgMTAtMTAgMTBjMCA1LjUyMy00LjQ3NyAxMC0xMCAxMFMwIDI1LjUyMyAwIDIwczQuNDc3LTEwIDEwLTEwem0xMCA4YzQuNDE4IDAgOC0zLjU4MiA4LThzLTMuNTgyLTgtOC04LTggMy41ODItOCA4IDMuNTgyIDggOCA4em00MCA0MGM0LjQxOCAwIDgtMy41ODIgOC04cy0zLjU4Mi04LTgtOC04IDMuNTgyLTggOCAzLjU4MiA4IDggOHonIC8lM0UlM0MvZyUzRSUzQy9nJTNFJTNDL3N2ZyUzRVwiKTtcclxuICB9XHJcbiAgXHJcbiAgQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNTAwcHgpIHtcclxuICAgICAgLnByb2dyZXNzIHtcclxuICAgICAgICAgIHdpZHRoOiBjYWxjKDEwMHZ3ICogMC43KTtcclxuICAgICAgICAgIC8qIGhlaWdodDogY2FsYygxMDB2dyAqIDAuNyAqIDAuMTQ1KTsgKi9cclxuICAgICAgICAgIGhlaWdodDogNDBweDtcclxuICAgICAgICAgIG1hcmdpbjogMjVweDtcclxuICAgICAgfVxyXG4gIFxyXG4gICAgICAuZ2l0aHViIHtcclxuICAgICAgICAgIG1hcmdpbjogMjBweDtcclxuICAgICAgfVxyXG4gIFxyXG4gICAgICAuZGVza3RvcCB7XHJcbiAgICAgICAgICBkaXNwbGF5OiBub25lO1xyXG4gICAgICB9XHJcbiAgXHJcbiAgICAgIC5tb2JpbGUge1xyXG4gICAgICAgICAgZGlzcGxheTogaW5oZXJpdDtcclxuICAgICAgfVxyXG4gIH1cclxuICBcclxuICBcclxuICAvKiBJbmRldGVybWluYXRlIENsYXNzZXMgYW5kIEFuaW1hdGlvbnMgKi9cclxuICBcclxuICAuaW5kZXRlcm1pbmF0ZS0xNiB7XHJcbiAgICAgIGFuaW1hdGlvbjogaW5kZXRlcm1pbmF0ZS0xNiAwLjI1cyBsaW5lYXIgaW5maW5pdGU7XHJcbiAgfVxyXG4gIFxyXG4gIC5pbmRldGVybWluYXRlLTIwIHtcclxuICAgICAgYW5pbWF0aW9uOiBpbmRldGVybWluYXRlLTIwIDAuNXMgbGluZWFyIGluZmluaXRlO1xyXG4gIH1cclxuICBcclxuICAuaW5kZXRlcm1pbmF0ZS0yNSB7XHJcbiAgICAgIGFuaW1hdGlvbjogaW5kZXRlcm1pbmF0ZS0yNSAwLjVzIGxpbmVhciBpbmZpbml0ZTtcclxuICB9XHJcbiAgXHJcbiAgLmluZGV0ZXJtaW5hdGUtMjgge1xyXG4gICAgICBhbmltYXRpb246IGluZGV0ZXJtaW5hdGUtMjggMC4yNXMgbGluZWFyIGluZmluaXRlO1xyXG4gIH1cclxuICBcclxuICAuaW5kZXRlcm1pbmF0ZS00MCB7XHJcbiAgICAgIGFuaW1hdGlvbjogaW5kZXRlcm1pbmF0ZS00MCAwLjVzIGxpbmVhciBpbmZpbml0ZTtcclxuICB9XHJcbiAgXHJcbiAgLmluZGV0ZXJtaW5hdGUtNTIge1xyXG4gICAgICBhbmltYXRpb246IGluZGV0ZXJtaW5hdGUtNTIgMC41cyBsaW5lYXIgaW5maW5pdGU7XHJcbiAgfVxyXG4gIFxyXG4gIC5pbmRldGVybWluYXRlLTU2IHtcclxuICAgICAgYW5pbWF0aW9uOiBpbmRldGVybWluYXRlLTU2IDAuNXMgbGluZWFyIGluZmluaXRlO1xyXG4gIH1cclxuICBcclxuICAuaW5kZXRlcm1pbmF0ZS01MCB7XHJcbiAgICAgIGFuaW1hdGlvbjogaW5kZXRlcm1pbmF0ZS01MCAwLjVzIGxpbmVhciBpbmZpbml0ZTtcclxuICB9XHJcbiAgXHJcbiAgLmluZGV0ZXJtaW5hdGUtNjAge1xyXG4gICAgICBhbmltYXRpb246IGluZGV0ZXJtaW5hdGUtNjAgMC41cyBsaW5lYXIgaW5maW5pdGU7XHJcbiAgfVxyXG4gIFxyXG4gIC5pbmRldGVybWluYXRlLTcwIHtcclxuICAgICAgYW5pbWF0aW9uOiBpbmRldGVybWluYXRlLTcwIDAuNXMgbGluZWFyIGluZmluaXRlO1xyXG4gIH1cclxuICBcclxuICAuaW5kZXRlcm1pbmF0ZS03NiB7XHJcbiAgICAgIGFuaW1hdGlvbjogaW5kZXRlcm1pbmF0ZS03NiAwLjVzIGxpbmVhciBpbmZpbml0ZTtcclxuICB9XHJcbiAgXHJcbiAgLmluZGV0ZXJtaW5hdGUtODAge1xyXG4gICAgICBhbmltYXRpb246IGluZGV0ZXJtaW5hdGUtODAgMC41cyBsaW5lYXIgaW5maW5pdGU7XHJcbiAgfVxyXG4gIFxyXG4gIC5pbmRldGVybWluYXRlLTEwMCB7XHJcbiAgICAgIGFuaW1hdGlvbjogaW5kZXRlcm1pbmF0ZS0xMDAgMXMgbGluZWFyIGluZmluaXRlO1xyXG4gIH1cclxuICBcclxuICAuaW5kZXRlcm1pbmF0ZS0xMjAge1xyXG4gICAgICBhbmltYXRpb246IGluZGV0ZXJtaW5hdGUtMTIwIDFzIGxpbmVhciBpbmZpbml0ZTtcclxuICB9XHJcbiAgXHJcbiAgLmluZGV0ZXJtaW5hdGUtMTI2IHtcclxuICAgICAgYW5pbWF0aW9uOiBpbmRldGVybWluYXRlLTEyNiAxcyBsaW5lYXIgaW5maW5pdGU7XHJcbiAgfVxyXG4gIFxyXG4gIC5pbmRldGVybWluYXRlLTE1MCB7XHJcbiAgICAgIGFuaW1hdGlvbjogaW5kZXRlcm1pbmF0ZS0xNTAgMXMgbGluZWFyIGluZmluaXRlO1xyXG4gIH1cclxuICBcclxuICBAa2V5ZnJhbWVzIGluZGV0ZXJtaW5hdGUtMTYge1xyXG4gICAgICBmcm9tIHtcclxuICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IDE2cHggMDtcclxuICAgICAgfVxyXG4gICAgICB0byB7XHJcbiAgICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiAwIDA7XHJcbiAgICAgIH1cclxuICB9XHJcbiAgXHJcbiAgQGtleWZyYW1lcyBpbmRldGVybWluYXRlLTIwIHtcclxuICAgICAgZnJvbSB7XHJcbiAgICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiAyMHB4IDA7XHJcbiAgICAgIH1cclxuICAgICAgdG8ge1xyXG4gICAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogMCAwO1xyXG4gICAgICB9XHJcbiAgfVxyXG4gIFxyXG4gIEBrZXlmcmFtZXMgaW5kZXRlcm1pbmF0ZS0yNSB7XHJcbiAgICAgIGZyb20ge1xyXG4gICAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogMjVweCAwO1xyXG4gICAgICB9XHJcbiAgICAgIHRvIHtcclxuICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IDAgMDtcclxuICAgICAgfVxyXG4gIH1cclxuICBcclxuICBAa2V5ZnJhbWVzIGluZGV0ZXJtaW5hdGUtMjgge1xyXG4gICAgICBmcm9tIHtcclxuICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IDI4cHggMDtcclxuICAgICAgfVxyXG4gICAgICB0byB7XHJcbiAgICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiAwIDA7XHJcbiAgICAgIH1cclxuICB9XHJcbiAgXHJcbiAgQGtleWZyYW1lcyBpbmRldGVybWluYXRlLTQwIHtcclxuICAgICAgZnJvbSB7XHJcbiAgICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiA0MHB4IDA7XHJcbiAgICAgIH1cclxuICAgICAgdG8ge1xyXG4gICAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogMCAwO1xyXG4gICAgICB9XHJcbiAgfVxyXG4gIFxyXG4gIEBrZXlmcmFtZXMgaW5kZXRlcm1pbmF0ZS01MCB7XHJcbiAgICAgIGZyb20ge1xyXG4gICAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogNTBweCAwO1xyXG4gICAgICB9XHJcbiAgICAgIHRvIHtcclxuICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IDAgMDtcclxuICAgICAgfVxyXG4gIH1cclxuICBcclxuICBAa2V5ZnJhbWVzIGluZGV0ZXJtaW5hdGUtNTIge1xyXG4gICAgICBmcm9tIHtcclxuICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IDUycHggMDtcclxuICAgICAgfVxyXG4gICAgICB0byB7XHJcbiAgICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiAwIDA7XHJcbiAgICAgIH1cclxuICB9XHJcbiAgXHJcbiAgQGtleWZyYW1lcyBpbmRldGVybWluYXRlLTU2IHtcclxuICAgICAgZnJvbSB7XHJcbiAgICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiA1NnB4IDA7XHJcbiAgICAgIH1cclxuICAgICAgdG8ge1xyXG4gICAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogMCAwO1xyXG4gICAgICB9XHJcbiAgfVxyXG4gIFxyXG4gIEBrZXlmcmFtZXMgaW5kZXRlcm1pbmF0ZS02MCB7XHJcbiAgICAgIGZyb20ge1xyXG4gICAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogNjBweCAwO1xyXG4gICAgICB9XHJcbiAgICAgIHRvIHtcclxuICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IDAgMDtcclxuICAgICAgfVxyXG4gIH1cclxuICBcclxuICBAa2V5ZnJhbWVzIGluZGV0ZXJtaW5hdGUtNzAge1xyXG4gICAgICBmcm9tIHtcclxuICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IDcwcHggMDtcclxuICAgICAgfVxyXG4gICAgICB0byB7XHJcbiAgICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiAwIDA7XHJcbiAgICAgIH1cclxuICB9XHJcbiAgXHJcbiAgQGtleWZyYW1lcyBpbmRldGVybWluYXRlLTc2IHtcclxuICAgICAgZnJvbSB7XHJcbiAgICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiA3NnB4IDA7XHJcbiAgICAgIH1cclxuICAgICAgdG8ge1xyXG4gICAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogMCAwO1xyXG4gICAgICB9XHJcbiAgfVxyXG4gIFxyXG4gIEBrZXlmcmFtZXMgaW5kZXRlcm1pbmF0ZS04MCB7XHJcbiAgICAgIGZyb20ge1xyXG4gICAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogODBweCAwO1xyXG4gICAgICB9XHJcbiAgICAgIHRvIHtcclxuICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IDAgMDtcclxuICAgICAgfVxyXG4gIH1cclxuICBcclxuICBAa2V5ZnJhbWVzIGluZGV0ZXJtaW5hdGUtMTAwIHtcclxuICAgICAgZnJvbSB7XHJcbiAgICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiAxMDBweCAwO1xyXG4gICAgICB9XHJcbiAgICAgIHRvIHtcclxuICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IDAgMDtcclxuICAgICAgfVxyXG4gIH1cclxuICBcclxuICBAa2V5ZnJhbWVzIGluZGV0ZXJtaW5hdGUtMTIwIHtcclxuICAgICAgZnJvbSB7XHJcbiAgICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiAxMjBweCAwO1xyXG4gICAgICB9XHJcbiAgICAgIHRvIHtcclxuICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IDAgMDtcclxuICAgICAgfVxyXG4gIH1cclxuICBcclxuICBAa2V5ZnJhbWVzIGluZGV0ZXJtaW5hdGUtMTI2IHtcclxuICAgICAgZnJvbSB7XHJcbiAgICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiAxMjZweCAwO1xyXG4gICAgICB9XHJcbiAgICAgIHRvIHtcclxuICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IDAgMDtcclxuICAgICAgfVxyXG4gIH1cclxuICBcclxuICBAa2V5ZnJhbWVzIGluZGV0ZXJtaW5hdGUtMTUwIHtcclxuICAgICAgZnJvbSB7XHJcbiAgICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiAxNTBweCAwO1xyXG4gICAgICB9XHJcbiAgICAgIHRvIHtcclxuICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IDAgMDtcclxuICAgICAgfVxyXG4gIH1cclxuICAiXX0= */"

/***/ }),

/***/ "./src/app/analytic/analytic.component.html":
/*!**************************************************!*\
  !*** ./src/app/analytic/analytic.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<link  href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css\"  rel=\"stylesheet\">\n\n\n\n\n\n\n\n\n<!--\n<div id=\"main\">\n <nav id=\"header\" class=\"navbar navbar-expand-lg navbar-dark bg-dark\">\n  <a class=\"navbar-brand\" href=\"#\">\n    FANI\n  </a>\n\n  <button class=\"navbar-toggler\" type=\"button\"\n          (click)=\"toggleNavbar()\">\n    <span class=\"navbar-toggler-icon\"></span>\n  </button>\n\n\n  <div class=\"collapse navbar-collapse\"\n       [ngClass]=\"{ 'show': navbarOpen }\">\n    <ul class=\"navbar-nav mr-auto\">\n      <li class=\"nav-item\">\n        <a class=\"nav-link\"routerLink=\"/signup\">calendar</a>\n      </li>\n\n      <li class=\"nav-item\">\n        <a class=\"nav-link\" href=\"#\">logout</a>\n      </li>\n    </ul>\n  </div>\n </nav>\n\n</div>-->\n\n<div id=\"maincontenthome\">\n\n  \n<!--style=\"background-color:rgb(245, 245, 245);\"-->\n\n<div class=\"container\">\n  <div class=\"row\">\n    <div class=\"col-md-12\" >\n        <h2  align=\"left\" style=\"color:#808080;\">Last week total</h2>\n      <!-- <h4  align=\"right\" style=\"color:#13101b;\">Today </h4>-->\n    <!--  <div id=\"myProgress\">\n           <div id=\"myBar\"></div>\n      </div>-->\n     <!-- <div id=\"txt\"></div>-->\n      <!--<h6  align=\"center\" style=\"color:#13101b;\"> <div id=\"calories\"></div></h6>-->\n    </div>\n\n    <div class=\"col-md-12\" >\n        <h6 id=\"day1\" align=\"right\" style=\"color:#13101b;\"> </h6>\n        <div  align=\"right\" id=\"txt\"></div>\n        \n        <div class=\"progress\">\n          \n            <div class=\"bar shadow indeterminate-60 leaf\"></div>\n            \n        </div> \n        <h6 id=\"day2\"  align=\"right\" style=\"color:#13101b;\"> </h6>\n        <div  align=\"right\" id=\"txt2\"></div>\n        <div class=\"progress\">\n            <div class=\"bar shadow indeterminate-40 bars\"></div>\n        </div>\n        <h6 id=\"day3\"  align=\"right\" style=\"color:#13101b;\"> </h6>\n        <div  align=\"right\" id=\"txt3\"></div>\n        <div class=\"progress\">\n            <div class=\"bar shadow indeterminate-150 circuit\"></div>\n        </div>\n        <h6 id=\"day4\"  align=\"right\" style=\"color:#13101b;\"> </h6>\n        <div  align=\"right\" id=\"txt4\"></div>\n        <div class=\"progress\">\n            <div class=\"bar shadow indeterminate-100 aztec\"></div>\n        </div>\n\n     \n        \n      \n    </div>\n\n   \n    <div class=\"col-md-12\" >\n        \n    \n      <div class=\"container\">\n         \n        <!--  <div id=\"bar1\" class=\"progress\">\n              <div class=\"bar shadow indeterminate-60 overlap\"></div>\n          </div>-->\n         \n        \n        <!--  <div class=\"progress\">\n              <div class=\"bar shadow indeterminate-40 bars\"></div>\n          </div>-->\n        <!--  <div class=\"progress\">\n              <div class=\"bar shadow indeterminate-80 lines\"></div>\n          </div>\n          <div class=\"progress\">\n              <div class=\"bar shadow indeterminate-52 wiggle\"></div>\n          </div>\n          <div class=\"progress\">\n              <div class=\"bar shadow indeterminate-16 dots\"></div>\n          </div>\n          <div class=\"progress\">\n              <div class=\"bar shadow indeterminate-150 circuit\"></div>\n          </div>\n          <div class=\"progress\">\n              <div class=\"bar shadow indeterminate-100 aztec\"></div>\n          </div>\n          <div class=\"progress\">\n              <div class=\"bar shadow indeterminate-28 bees\"></div>\n          </div>\n          <div class=\"progress\">\n              <div class=\"bar shadow indeterminate-120 food\"></div>\n          </div>\n          <div class=\"progress\">\n              <div class=\"bar shadow indeterminate-56 clouds\"></div>\n          </div>\n          <div class=\"progress\">\n              <div class=\"bar shadow indeterminate-25 stripes\"></div>\n          </div>\n          <div class=\"progress\">\n              <div class=\"bar shadow indeterminate-25 crosses\"></div>\n          </div>\n          <div class=\"progress\">\n              <div class=\"bar shadow indeterminate-52 jupiter\"></div>\n          </div>\n          <div class=\"progress\">\n              <div class=\"bar shadow indeterminate-70 piano\"></div>\n          </div>\n          <div class=\"progress\">\n              <div class=\"bar shadow indeterminate-126 dominos\"></div>\n          </div>\n          <div class=\"progress\">\n              <div class=\"bar shadow indeterminate-50 pie\"></div>\n          </div>\n          <div class=\"progress\">\n              <div class=\"bar shadow indeterminate-20 floor\"></div>\n          </div>\n          <div class=\"progress\">\n              <div class=\"bar shadow indeterminate-100 bubbles\"></div>\n          </div>\n          <div class=\"progress\">\n              <div class=\"bar shadow indeterminate-76 ticTac\"></div>\n          </div>\n          <div class=\"progress\">\n              <div class=\"bar shadow indeterminate-40 zigZag\"></div>\n          </div>-->\n      </div>\n    </div>\n  </div>\n</div>\n</div>\n\n\n<div id=\"all\">\n\n  \n    <div class=\"home\">\n        <span id=\"home\" class=\"home\">Home  </span>\n        <input id=\"button1\" type=\"button\"  (click)=\"submitted=false\"  (click)=\"goToPage('/', 'main')\">\n      \n    </div>\n\n     <div class=\"analytic\">\n      <span id=\"analytic\" class=\"analytic\">Analytics  </span>\n      <input id=\"button2\" type=\"button\" alt=\"Submit\" width=\"48\" height=\"48\" (click)=\"submitted=false\" (click)=\"goToPage('/', 'analytic')\">\n  \n    </div>\n\n    <div class=\"notification\">\n        <span id=\"notification\" class=\"notification\">Calendar  </span>\n        <input id=\"button3\" type=\"button\" alt=\"Submit\" width=\"48\" height=\"48\" (click)=\"submitted=false\" (click)=\"goToPage('/', 'calendar')\">\n    \n      </div>\n    \n      <div class=\"setting\">\n          <span id=\"setting\" class=\"setting\">Settings  </span>\n          <input id=\"button4\" type=\"button\" alt=\"Submit\" width=\"48\" height=\"48\" (click)=\"submitted=false\" (click)=\"goToPage('/', 'settings')\">\n      \n        </div>\n   \n \n \n</div>\n\n\n<!--\n<div class=\"container\">\n  <div class=\"row\">\n    <div class=\"col-sm\">\n      <button class=\"button\">Green</button>\n    </div>\n    <div class=\"col-sm\">\n      <button class=\"button\">Green</button>\n    </div>\n    <div class=\"col-sm\">\n        <button class=\"button\">Green</button>\n     \n    </div>\n\n    <div class=\"col-sm\">\n        <button class=\"button\">Green</button>\n     \n    </div>\n\n   \n  </div>\n</div>\n-->\n\n\n"

/***/ }),

/***/ "./src/app/analytic/analytic.component.ts":
/*!************************************************!*\
  !*** ./src/app/analytic/analytic.component.ts ***!
  \************************************************/
/*! exports provided: AnalyticComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AnalyticComponent", function() { return AnalyticComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _analytic__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./analytic */ "./src/app/analytic/analytic.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");






var AnalyticComponent = /** @class */ (function () {
    function AnalyticComponent(router, httpClient) {
        this.router = router;
        this.httpClient = httpClient;
        this.apiURL = 'http://127.0.0.1:3000/api/things/setlasthistoryinfo/';
        this.apiURLHistory = 'http://localhost:3000/api/things/gethistoryvalue/';
        this.navbarOpen = false;
    }
    AnalyticComponent.prototype.bars = function (arg, value) {
        // arg="0";
        //alert(arg);
        //alert(value);
        switch (arg) {
            case "0": {
                var a = 100;
                var bars = document.querySelectorAll('.bar')[arg];
                var progress = document.querySelectorAll('.progress');
                var bar1 = document.getElementById("bar1");
                var randomWidth = Math.floor((Math.random() * 65) + 10);
                bars.style.width = randomWidth + "%";
                // progress[arg].addEventListener('mouseover', () => {
                var randomTiming = Math.floor((Math.random() * 2) + 2);
                //  alert(randomTiming);
                console.log(randomTiming);
                bars.style.transitionDuration = randomTiming + "s";
                bars.style.width = value.toString() + "%";
                // });
            }
            case "1": {
                var a = 20;
                var bars = document.querySelectorAll('.bar')[1];
                var progress = document.querySelectorAll('.progress');
                var bar1 = document.getElementById("bar1");
                var randomWidth = Math.floor((Math.random() * 65) + 10);
                bars.style.width = randomWidth + "%";
                // progress[1].addEventListener('mouseover', () => {
                var randomTiming = Math.floor((Math.random() * 2) + 2);
                console.log(randomTiming);
                bars.style.transitionDuration = randomTiming + "s";
                bars.style.width = value.toString() + "%";
                //  });
            }
            case "2": {
                // var a=20;
                var bars = document.querySelectorAll('.bar')[2];
                var progress = document.querySelectorAll('.progress');
                var bar1 = document.getElementById("bar1");
                var randomWidth = Math.floor((Math.random() * 65) + 10);
                bars.style.width = randomWidth + "%";
                //   progress[2].addEventListener('mouseover', () => {
                var randomTiming = Math.floor((Math.random() * 2) + 2);
                console.log(randomTiming);
                bars.style.transitionDuration = randomTiming + "s";
                bars.style.width = value.toString() + "%";
                // });
            }
            case "3": {
                // var a=20;
                var bars = document.querySelectorAll('.bar')[3];
                var progress = document.querySelectorAll('.progress');
                var bar1 = document.getElementById("bar1");
                var randomWidth = Math.floor((Math.random() * 65) + 10);
                bars.style.width = randomWidth + "%";
                //   progress[2].addEventListener('mouseover', () => {
                var randomTiming = Math.floor((Math.random() * 2) + 2);
                console.log(randomTiming);
                bars.style.transitionDuration = randomTiming + "s";
                bars.style.width = value.toString() + "%";
                // });
            }
        }
    };
    AnalyticComponent.prototype.getAnalytic = function (name) {
        var data = {
            "name": "fr"
        };
        return this.httpClient.get("" + this.apiURLHistory + name, { responseType: "json" });
    };
    AnalyticComponent.prototype.ngOnInit = function () {
        var _this = this;
        var day = new Date().getDay().toString();
        var month = new Date().getMonth().toString();
        var year = new Date().getFullYear().toString();
        // alert((new Date()).getDay());
        // alert((new Date()).getMonth());
        // alert((new Date()).getFullYear());
        jquery__WEBPACK_IMPORTED_MODULE_2__("#session").hide();
        jquery__WEBPACK_IMPORTED_MODULE_2__("#back").hide();
        jquery__WEBPACK_IMPORTED_MODULE_2__("#date").show();
        /////////////////////////////////////////
        /* this.getAnalytic("Emily").subscribe((res)=>{
         //  alert("Hi to all analytic");
          // alert(res[0].date);
          // alert(res[0].date);
          }, (error) =>
          {
            //alert("error hier");
          //  alert(error);
              // if the request fails, this callback will be invoked
          });*/
        ///////////////////////////////////////  
        var prevScrollpos = window.pageYOffset;
        window.onscroll = function () {
            var currentScrollPos = window.pageYOffset;
            if (prevScrollpos > currentScrollPos) {
                document.getElementById("navbar").style.top = "0";
            }
            else {
                document.getElementById("navbar").style.top = "0px";
            }
            prevScrollpos = currentScrollPos;
        };
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        var todays = mm + '/' + dd + '/' + yyyy;
        //alert(todays);
        new _analytic__WEBPACK_IMPORTED_MODULE_4__["Analytic"]().setDate(todays, month, year);
        function delay(ms) {
            return new Promise(function (resolve) { return setTimeout(resolve, ms); });
        }
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(1000)];
                    case 1:
                        _a.sent();
                        this.bars("0", 90);
                        this.bars("1", 50);
                        this.bars("2", 80);
                        this.bars("3", 90);
                        new _analytic__WEBPACK_IMPORTED_MODULE_4__["Analytic"]().processPercentage(25, "1100/2400", "1200/2400", "1300/2400", "1400/2400");
                        return [2 /*return*/];
                }
            });
        }); })();
        var num1 = 1250;
        document.getElementById("analytic").style.backgroundColor = "#696969";
        //document.getElementById("calories").textContent="Current "+num1+" calories";
        //this.move();
    };
    AnalyticComponent.prototype.move = function () {
        /* let percent:number = 1;
         var elem = document.getElementById("myBar");
         var perc = document.getElementById("txt");
         var width = 0;
         var percentTotal=72;
         var id = setInterval(frame, 33);
         function frame() {
           if (width >= 90&&width <= 100) {
             document.getElementById("cur").textContent="You need more exercise";
             document.getElementById("myProgress").style.backgroundColor="#8B0000";
             clearInterval(id);
           }else if(width>80&&width<90){
             document.getElementById("myProgress").style.backgroundColor="#7CFC00";
             document.getElementById("cur").textContent="You are good";
             clearInterval(id);
           } else {
             width++;
             percent++;
             percentTotal++;
             perc.textContent=""+percent+"%";
             elem.style.width = width + '%';
           }
         }*/
    };
    AnalyticComponent.prototype.toggleNavbar = function () {
        this.navbarOpen = !this.navbarOpen;
    };
    AnalyticComponent.prototype.goToPage = function (pagename, parameter) {
        this.router.navigate([pagename, parameter]);
    };
    AnalyticComponent.prototype.submit1 = function () {
        document.getElementById("home").style.backgroundColor = "#696969";
    };
    AnalyticComponent.prototype.submit2 = function () {
    };
    AnalyticComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-analytic',
            template: __webpack_require__(/*! ./analytic.component.html */ "./src/app/analytic/analytic.component.html"),
            styles: [__webpack_require__(/*! ./analytic.component.css */ "./src/app/analytic/analytic.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"]])
    ], AnalyticComponent);
    return AnalyticComponent;
}());



/***/ }),

/***/ "./src/app/analytic/analytic.ts":
/*!**************************************!*\
  !*** ./src/app/analytic/analytic.ts ***!
  \**************************************/
/*! exports provided: Analytic */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Analytic", function() { return Analytic; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

var Analytic = /** @class */ (function () {
    function Analytic() {
    }
    Analytic.prototype.processDates = function () {
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        var day = +dd;
        var days = day - 1;
        var todays = mm + '/' + days + '/' + yyyy;
        document.getElementById("day1").textContent = todays;
        var days1 = day - 2;
        var todays1 = mm + '/' + days1 + '/' + yyyy;
        document.getElementById("day2").textContent = todays1;
        var days2 = day - 3;
        var todays2 = mm + '/' + days2 + '/' + yyyy;
        document.getElementById("day3").textContent = todays2;
        var days3 = day - 4;
        var todays3 = mm + '/' + days3 + '/' + yyyy;
        document.getElementById("day4").textContent = todays3;
    };
    Analytic.prototype.processPercentage = function (percentage, value, value2, value3, value4) {
        var _this = this;
        var percent = 1;
        //var elem = document.getElementById("myBar");  
        var perc = document.getElementById("txt");
        var perc2 = document.getElementById("txt2");
        var perc3 = document.getElementById("txt3");
        var perc4 = document.getElementById("txt4");
        var width = 0;
        var percentTotal = 72;
        function delay(ms) {
            return new Promise(function (resolve) { return setTimeout(resolve, ms); });
        }
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(4000)];
                    case 1:
                        _a.sent();
                        perc.textContent = "Your calories " + value + "";
                        perc2.textContent = "Your calories " + value2 + "";
                        perc3.textContent = "Your calories " + value3 + "";
                        perc4.textContent = "Your calories " + value4 + "";
                        return [2 /*return*/];
                }
            });
        }); })();
        //var id = setInterval(frame, 27);
        /* function frame() {
           if (width >= percentage&&width <= 100) {
             perc.textContent="Your calories "+value+"";
             clearInterval(id);
           }else {
             width++;
             percent++;
             percentTotal++;
           
            // elem.style.width = width + '%';
           }
         }*/
    };
    Analytic.prototype.setDate = function (date1, date2, date3) {
        date1 = date1;
        date2 = date2;
        date3 = date3;
        this.processDates();
        //this.processPercentage();
        // alert(date1);
        //alert(date2);
        // alert(date3);
        // let fulldate:any=date1+"/"+date2+"/"+date3;
        //console.log("date1:"+date1);
    };
    return Analytic;
}());



/***/ }),

/***/ "./src/app/animations.ts":
/*!*******************************!*\
  !*** ./src/app/animations.ts ***!
  \*******************************/
/*! exports provided: slideInAnimation */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "slideInAnimation", function() { return slideInAnimation; });
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm5/animations.js");

var slideInAnimation = Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["trigger"])('routeAnimations', [
    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["transition"])('HomePage <=> AboutPage', [
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ position: 'relative' }),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["query"])(':enter, :leave', [
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%'
            })
        ]),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["query"])(':enter', [
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ left: '-100%' })
        ]),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["query"])(':leave', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animateChild"])()),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["group"])([
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["query"])(':leave', [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('300ms ease-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ left: '100%' }))
            ]),
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["query"])(':enter', [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('300ms ease-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ left: '0%' }))
            ])
        ]),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animateChild"])()),
    ]),
    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["transition"])('* <=> FilterPage', [
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ position: 'relative' }),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["query"])(':enter, :leave', [
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%'
            })
        ]),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["query"])(':enter', [
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ left: '-100%' })
        ]),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["query"])(':leave', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animateChild"])()),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["group"])([
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["query"])(':leave', [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('200ms ease-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ left: '100%' }))
            ]),
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["query"])(':enter', [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('300ms ease-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ left: '0%' }))
            ])
        ]),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animateChild"])()),
    ])
]);


/***/ }),

/***/ "./src/app/api.service.ts":
/*!********************************!*\
  !*** ./src/app/api.service.ts ***!
  \********************************/
/*! exports provided: ApiService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApiService", function() { return ApiService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");



//import { Customer } from './customer';
var ApiService = /** @class */ (function () {
    function ApiService(httpClient) {
        this.httpClient = httpClient;
        this.apiURL = 'http://localhost:3000/api/things/';
        this.firstPage = "";
        this.prevPage = "";
        this.nextPage = "";
        this.lastPage = "";
    }
    ApiService.prototype.getCustomers = function () {
        alert("called customers");
        /* return this.httpClient.get(`${this.apiURL}`, {
           params: {
             id: '3'
            
           },
           observe: 'response'
         })*/
        var i = 3;
        var data = {
            name: "dede",
            name2: "nou"
        };
        return this.httpClient.get("" + this.apiURL + JSON.stringify(data));
    };
    ApiService.prototype.createCustomer = function () {
        /* const httpOptions = {
           headers: new HttpHeaders({
             'Content-Type':  'application/json',
             'Authorization': 'my-auth-token'
           })
         };*/
        /*
        this.http.post(
           "http://localhost:3000/contacts",
           JSON.stringify({id: 4, name: 'some'}),
           httpOptions
        ).subscribe();*/
        //var HttpClient = new HttpClient();
        //HttpClient.append('Content-Type', 'application/json');
        //return this.httpClient.post(`${this.apiURL}`,JSON.stringify({name:"okza"}));
        /*const headers={
          'Content-Type':  'application/json'
        
        }*/
        //const headers = new HttpHeaders().set('Content-Type', 'application/json');
        var data = {
            name: "dede"
        };
        var headerDict = {
            'Content-Type': 'application/json',
        };
        var requestOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"](headerDict),
        };
        var res = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]().set("name", "1");
        alert("fani" + JSON.stringify(data));
        return this.httpClient.post("" + this.apiURL, JSON.stringify(data));
        // return this.httpClient.post(`${this.apiURL}`,JSON.stringify(data) )
    };
    ApiService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], ApiService);
    return ApiService;
}());



/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _signup_signup_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./signup/signup.component */ "./src/app/signup/signup.component.ts");
/* harmony import */ var _signin_signin_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./signin/signin.component */ "./src/app/signin/signin.component.ts");
/* harmony import */ var _start_start_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./start/start.component */ "./src/app/start/start.component.ts");
/* harmony import */ var _header_header_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./header/header.component */ "./src/app/header/header.component.ts");
/* harmony import */ var _analytic_analytic_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./analytic/analytic.component */ "./src/app/analytic/analytic.component.ts");
/* harmony import */ var _submit_form_submit_form_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./submit-form/submit-form.component */ "./src/app/submit-form/submit-form.component.ts");
/* harmony import */ var _header_diagrams_diagrams_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./header/diagrams/diagrams.component */ "./src/app/header/diagrams/diagrams.component.ts");
/* harmony import */ var _admin_admin_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./admin/admin.component */ "./src/app/admin/admin.component.ts");
/* harmony import */ var _calendar_calendar_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./calendar/calendar.component */ "./src/app/calendar/calendar.component.ts");
/* harmony import */ var _notification_notification_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./notification/notification.component */ "./src/app/notification/notification.component.ts");
/* harmony import */ var _infos_infos_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./infos/infos.component */ "./src/app/infos/infos.component.ts");
/* harmony import */ var _settings_settings_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./settings/settings.component */ "./src/app/settings/settings.component.ts");
/* harmony import */ var _error_error_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./error/error.component */ "./src/app/error/error.component.ts");
/* harmony import */ var _sockets_sockets_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./sockets/sockets.component */ "./src/app/sockets/sockets.component.ts");

















var routes = [
    { path: 'signup', component: _signup_signup_component__WEBPACK_IMPORTED_MODULE_3__["SignupComponent"] },
    { path: 'signin', component: _signin_signin_component__WEBPACK_IMPORTED_MODULE_4__["SigninComponent"] },
    { path: 'start', component: _start_start_component__WEBPACK_IMPORTED_MODULE_5__["StartComponent"] },
    { path: 'component', component: _submit_form_submit_form_component__WEBPACK_IMPORTED_MODULE_8__["SubmitFormComponent"] },
    { path: 'main', component: _header_header_component__WEBPACK_IMPORTED_MODULE_6__["HeaderComponent"] },
    { path: 'analytic', component: _analytic_analytic_component__WEBPACK_IMPORTED_MODULE_7__["AnalyticComponent"] },
    { path: 'diagrams', component: _header_diagrams_diagrams_component__WEBPACK_IMPORTED_MODULE_9__["DiagramsComponent"] },
    { path: 'calendar', component: _calendar_calendar_component__WEBPACK_IMPORTED_MODULE_11__["CalendarComponent"] },
    { path: 'paneladmin', component: _admin_admin_component__WEBPACK_IMPORTED_MODULE_10__["AdminComponent"] },
    { path: 'notification', component: _notification_notification_component__WEBPACK_IMPORTED_MODULE_12__["NotificationComponent"] },
    { path: 'infos/:name', component: _infos_infos_component__WEBPACK_IMPORTED_MODULE_13__["InfosComponent"] },
    { path: 'settings', component: _settings_settings_component__WEBPACK_IMPORTED_MODULE_14__["SettingsComponent"] },
    { path: 'error', component: _error_error_component__WEBPACK_IMPORTED_MODULE_15__["ErrorComponent"] },
    { path: 'sockets', component: _sockets_sockets_component__WEBPACK_IMPORTED_MODULE_16__["SocketsComponent"] }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/*\r\nul {\r\n    max-width: 500px;\r\n    color: #333;\r\n    list-style: none;\r\n    margin: 50px auto;\r\n    padding: 10px;\r\n}\r\n\r\n.list li {\r\n    padding: 25px;\r\n    background: #FAFAAF;\r\n    border-radius: 5px;\r\n    margin-bottom: 5px;\r\n    text-align: center;\r\n    font-size: 32px;\r\n    font-weight: 600;\r\n}\r\n\r\n.center {\r\n    text-align: center;\r\n}*/\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztFQXFCRSIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxudWwge1xyXG4gICAgbWF4LXdpZHRoOiA1MDBweDtcclxuICAgIGNvbG9yOiAjMzMzO1xyXG4gICAgbGlzdC1zdHlsZTogbm9uZTtcclxuICAgIG1hcmdpbjogNTBweCBhdXRvO1xyXG4gICAgcGFkZGluZzogMTBweDtcclxufVxyXG5cclxuLmxpc3QgbGkge1xyXG4gICAgcGFkZGluZzogMjVweDtcclxuICAgIGJhY2tncm91bmQ6ICNGQUZBQUY7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICBtYXJnaW4tYm90dG9tOiA1cHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBmb250LXNpemU6IDMycHg7XHJcbiAgICBmb250LXdlaWdodDogNjAwO1xyXG59XHJcblxyXG4uY2VudGVyIHtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxufSovXHJcblxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n\n<link  href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css\"  rel=\"stylesheet\">\n\n\n\n<div id=\"main\">\n  \n <nav id=\"header\" class=\"navbar navbar-expand-lg navbar-dark bg-dark\">\n  <a id=\"session\" class=\"navbar-brand\" href=\"#\">\n    \n  </a>\n\n  \n  \n  \n  <a style=\"color:white;\" id=\"back\" class=\"nav-link\"routerLink=\"/main\"><</a>\n\n  <a  style=\"color:white;\" id=\"date\" class=\"nav-link\"></a>\n\n  \n  <button class=\"navbar-toggler\" type=\"button\"\n          (click)=\"toggleNavbar()\">\n    <span class=\"navbar-toggler-icon\"></span>\n  </button>\n\n\n  <div id=\"navbar\"  class=\"collapse navbar-collapse\"\n       [ngClass]=\"{ 'show': navbarOpen }\">\n    <ul id=\"navbar\" class=\"navbar-nav mr-auto\" >\n      <li class=\"nav-item\">\n        <a style=\"color:white;\" class=\"nav-link\"routerLink=\"/notification\">notification</a>\n      </li>\n\n      <li class=\"nav-item\"   >\n        <a style=\"color:white;\" class=\"nav-link\" href=\"/signin\">logout</a>\n      </li>\n    \n      \n    </ul>\n\n    \n  </div>\n\n  \n </nav>\n\n</div>\n\n<router-outlet></router-outlet>\n\n<div id=\"alllist\">\n <!--   <h1 class=\"center\">Virtual Scroll using Angular 7</h1>\n    \n    <ul class=\"list\">\n      <cdk-virtual-scroll-viewport  style=\"height: 500px\" itemSize=\"90\" >\n        <ng-container *cdkVirtualFor=\"let n of numbers\">\n          <li> {{n}} </li>\n        </ng-container>\n      </cdk-virtual-scroll-viewport>\n    </ul>-->\n    </div>\n<!-- You can now use it in app.component.html -->\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./api.service */ "./src/app/api.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _animations__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./animations */ "./src/app/animations.ts");
/* harmony import */ var ng_push__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ng-push */ "./node_modules/ng-push/ng-push.umd.js");
/* harmony import */ var ng_push__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(ng_push__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var ngx_push_notifications__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-push-notifications */ "./node_modules/ngx-push-notifications/fesm5/ngx-push-notifications.js");







//import { PushNotificationService } from 'ngx-push-notifications';

//const socket = io('http://localhost?token=abc');
//import { Socket } from 'ngx-socket-io';
//import { Analytic } from './analytic';
var AppComponent = /** @class */ (function () {
    function AppComponent(_pushNotificationService, _pushNotifications, apiService, router) {
        this._pushNotificationService = _pushNotificationService;
        this._pushNotifications = _pushNotifications;
        this.apiService = apiService;
        this.router = router;
        this.title = 'angularfinal';
        this.numbers = [];
        /* sendMessage() {
          this.chat.sendMsg("Test Message");
         }*/
        this.navbarOpen = false;
        this._pushNotifications.requestPermission();
    }
    ;
    //title = 'Web push Notifications!';
    /* constructor( ) {
       this._pushNotifications.requestPermission();
     }*/
    AppComponent.prototype.myFunction = function () {
        var _this = this;
        var title = 'Hello';
        var options = new ngx_push_notifications__WEBPACK_IMPORTED_MODULE_7__["PushNotificationOptions"]();
        options.body = 'Native Push Notification';
        this._pushNotificationService.create(title, options).subscribe(function (notif) {
            if (notif.event.type === 'show') {
                console.log('onshow');
                setTimeout(function () {
                    notif.notification.close();
                }, 9000);
            }
            if (notif.event.type === 'click') {
                console.log('click');
                alert("Clicked");
                //notif.notification.close();
                _this.goToPage("/", "calendar");
                notif.notification.close();
            }
            if (notif.event.type === 'close') {
                console.log('close');
            }
        }, function (err) {
            console.log(err);
        });
    };
    AppComponent.prototype.notify = function () {
        var options = {
            body: "The c is, I'am Iron Man!",
            href: "http://154.57.7.149:8080/notification",
            icon: "assets/images/ironman.png" //adding an icon
        };
        this._pushNotifications.create('Iron Man', options).subscribe(//creates a notification
        function (//creates a notification
        res) { return console.log(res); }, function (err) { return console.log(err); });
    };
    AppComponent.prototype.ngOnInit = function () {
        /*  this._pushNotificationService.requestPermission();
          this.chat.messages.subscribe(msg => {
            console.log(msg);
          })*/
        // new WebsocketService(null).sendMessage("sasa");
        //   alert( new WebsocketService(null).getMessage());
        //alert("loc");
        // alert(location.pathname);
        // alert(   location.href);
        // new WebsocketService(Socket.arguments("s")).sendMessage("s");
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        var day = +dd;
        var days = day;
        var todays = mm + '/' + days + '/' + yyyy;
        document.getElementById("date").textContent = todays;
        var prevScrollpos = window.pageYOffset;
        window.onscroll = function () {
            var currentScrollPos = window.pageYOffset;
            if (prevScrollpos > currentScrollPos) {
                document.getElementById("navbar").style.top = "0";
            }
            else {
                document.getElementById("navbar").style.top = "0px";
            }
            prevScrollpos = currentScrollPos;
        };
        if (location.pathname.trim() == "/signin") {
            // alert("equal");
            jquery__WEBPACK_IMPORTED_MODULE_4__('#main').hide();
        }
        else if (location.pathname.trim() == "/signup") {
            jquery__WEBPACK_IMPORTED_MODULE_4__('#main').hide();
        }
        else {
            jquery__WEBPACK_IMPORTED_MODULE_4__('#main').show();
        }
        /*  this.apiService.getCustomers().subscribe((res)=>{
             console.log("called");
             alert("cabled"+res[0].info);
               console.log(res);
           });*/
        /*var customer = {
         
           "name": "xaxa",
          
         }
         this.apiService.createCustomer(customer).subscribe((res)=>{
           alert("response nouli is is"+res);
           console.log("Created a customer");
          });*/
    };
    AppComponent.prototype.toggleNavbar = function () {
        this.navbarOpen = !this.navbarOpen;
    };
    AppComponent.prototype.goToPage = function (pagename, parameter) {
        this.router.navigate([pagename, parameter]);
    };
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            animations: [
                _animations__WEBPACK_IMPORTED_MODULE_5__["slideInAnimation"]
                // animation triggers go here
            ],
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [ngx_push_notifications__WEBPACK_IMPORTED_MODULE_7__["PushNotificationService"], ng_push__WEBPACK_IMPORTED_MODULE_6__["PushNotificationsService"], _api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _signin_signin_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./signin/signin.component */ "./src/app/signin/signin.component.ts");
/* harmony import */ var _signup_signup_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./signup/signup.component */ "./src/app/signup/signup.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _start_start_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./start/start.component */ "./src/app/start/start.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _submit_form_submit_form_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./submit-form/submit-form.component */ "./src/app/submit-form/submit-form.component.ts");
/* harmony import */ var _header_header_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./header/header.component */ "./src/app/header/header.component.ts");
/* harmony import */ var _analytic_analytic_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./analytic/analytic.component */ "./src/app/analytic/analytic.component.ts");
/* harmony import */ var _header_diagrams_diagrams_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./header/diagrams/diagrams.component */ "./src/app/header/diagrams/diagrams.component.ts");
/* harmony import */ var ng_circle_progress__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ng-circle-progress */ "./node_modules/ng-circle-progress/index.js");
/* harmony import */ var _calendar_calendar_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./calendar/calendar.component */ "./src/app/calendar/calendar.component.ts");
/* harmony import */ var ng_push__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ng-push */ "./node_modules/ng-push/ng-push.umd.js");
/* harmony import */ var ng_push__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(ng_push__WEBPACK_IMPORTED_MODULE_16__);
/* harmony import */ var _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/cdk/scrolling */ "./node_modules/@angular/cdk/esm5/scrolling.es5.js");
/* harmony import */ var _admin_admin_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./admin/admin.component */ "./src/app/admin/admin.component.ts");
/* harmony import */ var ngx_push_notifications__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ngx-push-notifications */ "./node_modules/ngx-push-notifications/fesm5/ngx-push-notifications.js");
/* harmony import */ var _notification_notification_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./notification/notification.component */ "./src/app/notification/notification.component.ts");
/* harmony import */ var _infos_infos_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./infos/infos.component */ "./src/app/infos/infos.component.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _settings_settings_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./settings/settings.component */ "./src/app/settings/settings.component.ts");
/* harmony import */ var ngx_editor__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ngx-editor */ "./node_modules/ngx-editor/fesm5/ngx-editor.js");
/* harmony import */ var ng6_socket_io__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ng6-socket-io */ "./node_modules/ng6-socket-io/dist/index.js");
/* harmony import */ var ng6_socket_io__WEBPACK_IMPORTED_MODULE_25___default = /*#__PURE__*/__webpack_require__.n(ng6_socket_io__WEBPACK_IMPORTED_MODULE_25__);
/* harmony import */ var _auto_generated_auto_generated_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./auto-generated/auto-generated.component */ "./src/app/auto-generated/auto-generated.component.ts");
/* harmony import */ var _error_error_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./error/error.component */ "./src/app/error/error.component.ts");
/* harmony import */ var _sockets_sockets_component__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./sockets/sockets.component */ "./src/app/sockets/sockets.component.ts");














// Import ng-circle-progress


 //import the module
/////animate//








//...




var config = { url: 'http://localhost:8988', options: {} };
//import { BrowserModule } from '@angular/platform-browser';
/////
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
                _signin_signin_component__WEBPACK_IMPORTED_MODULE_5__["SigninComponent"],
                _signup_signup_component__WEBPACK_IMPORTED_MODULE_6__["SignupComponent"],
                _start_start_component__WEBPACK_IMPORTED_MODULE_8__["StartComponent"],
                _submit_form_submit_form_component__WEBPACK_IMPORTED_MODULE_10__["SubmitFormComponent"],
                _header_header_component__WEBPACK_IMPORTED_MODULE_11__["HeaderComponent"],
                _analytic_analytic_component__WEBPACK_IMPORTED_MODULE_12__["AnalyticComponent"],
                _header_diagrams_diagrams_component__WEBPACK_IMPORTED_MODULE_13__["DiagramsComponent"],
                _calendar_calendar_component__WEBPACK_IMPORTED_MODULE_15__["CalendarComponent"],
                _admin_admin_component__WEBPACK_IMPORTED_MODULE_18__["AdminComponent"],
                _notification_notification_component__WEBPACK_IMPORTED_MODULE_20__["NotificationComponent"],
                _infos_infos_component__WEBPACK_IMPORTED_MODULE_21__["InfosComponent"],
                _settings_settings_component__WEBPACK_IMPORTED_MODULE_23__["SettingsComponent"],
                _auto_generated_auto_generated_component__WEBPACK_IMPORTED_MODULE_26__["AutoGeneratedComponent"],
                _error_error_component__WEBPACK_IMPORTED_MODULE_27__["ErrorComponent"],
                _sockets_sockets_component__WEBPACK_IMPORTED_MODULE_28__["SocketsComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_7__["HttpClientModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_9__["FormsModule"], _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_17__["ScrollingModule"],
                ng_push__WEBPACK_IMPORTED_MODULE_16__["PushNotificationsModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_22__["CommonModule"],
                // Specify ng-circle-progress as an import
                ng_circle_progress__WEBPACK_IMPORTED_MODULE_14__["NgCircleProgressModule"].forRoot({
                    // set defaults here
                    radius: 100,
                    outerStrokeWidth: 16,
                    innerStrokeWidth: 8,
                    outerStrokeColor: "#78C000",
                    innerStrokeColor: "#C7E596",
                    animationDuration: 300,
                }),
                ngx_editor__WEBPACK_IMPORTED_MODULE_24__["NgxEditorModule"],
                ng6_socket_io__WEBPACK_IMPORTED_MODULE_25__["SocketIoModule"].forRoot(config)
            ],
            providers: [ngx_push_notifications__WEBPACK_IMPORTED_MODULE_19__["PushNotificationService"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/auto-generated/auto-generated.component.css":
/*!*************************************************************!*\
  !*** ./src/app/auto-generated/auto-generated.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2F1dG8tZ2VuZXJhdGVkL2F1dG8tZ2VuZXJhdGVkLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/auto-generated/auto-generated.component.html":
/*!**************************************************************!*\
  !*** ./src/app/auto-generated/auto-generated.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  auto-generated works!\n</p>\n"

/***/ }),

/***/ "./src/app/auto-generated/auto-generated.component.ts":
/*!************************************************************!*\
  !*** ./src/app/auto-generated/auto-generated.component.ts ***!
  \************************************************************/
/*! exports provided: AutoGeneratedComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AutoGeneratedComponent", function() { return AutoGeneratedComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AutoGeneratedComponent = /** @class */ (function () {
    function AutoGeneratedComponent() {
    }
    AutoGeneratedComponent.prototype.ngOnInit = function () {
    };
    AutoGeneratedComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-auto-generated',
            template: __webpack_require__(/*! ./auto-generated.component.html */ "./src/app/auto-generated/auto-generated.component.html"),
            styles: [__webpack_require__(/*! ./auto-generated.component.css */ "./src/app/auto-generated/auto-generated.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], AutoGeneratedComponent);
    return AutoGeneratedComponent;
}());



/***/ }),

/***/ "./src/app/calendar/calendar.component.css":
/*!*************************************************!*\
  !*** ./src/app/calendar/calendar.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/*\r\n#myProgress {\r\n  width: 100%;\r\n  background-color: #ddd;\r\n}\r\n\r\n#myBar {\r\n  width: 1%;\r\n  height: 30px;\r\n  background-color: #4CAF50;\r\n}*/\r\n#button1 {\r\n    background-color: #b2b2b2\t; \r\n    border: none;\r\n    color: white;\r\n    padding-bottom: 25px 22px;\r\n    text-align: center;\r\n    text-decoration: none;\r\n    display: inline-block;\r\n    font-size: 36px;\r\n    margin: 4px 18px;\r\n    cursor: pointer;\r\n    \r\n  }\r\n#button2 {\r\n   background-color: #b2b2b2\t; \r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n#button3 {\r\n   background-color: #b2b2b2\t; \r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n#button4 {\r\n   background-color: #b2b2b2\t;\r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n#buttona {\r\n    border-radius: 25px;\r\n    background-color: #b2b2b2\t; \r\n    border: none;\r\n    color: white;\r\n    padding-bottom: 25px 22px;\r\n    text-align: center;\r\n    text-decoration: none;\r\n    display: inline-block;\r\n    font-size: 36px;\r\n    margin: 4px 18px;\r\n    cursor: pointer;\r\n    \r\n  }\r\n#buttonb {\r\n    border-radius: 25px;\r\n   background-color: #b2b2b2\t; \r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n#buttonc {\r\n    border-radius: 25px;\r\n   background-color: #b2b2b2\t; \r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n#buttond {\r\n    border-radius: 25px;\r\n   background-color: #b2b2b2\t;\r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n#buttone {\r\n    border-radius: 25px;\r\n    background-color: #b2b2b2\t;\r\n    border: none;\r\n    color: white;\r\n    padding-bottom: 25px 22px;\r\n    text-align: center;\r\n    text-decoration: none;\r\n    display: inline-block;\r\n    font-size: 36px;\r\n    margin: 4px 18px;\r\n    cursor: pointer;\r\n  }\r\n#all{\r\n     text-align: center;\r\n     position: fixed;\r\n     left: 0;\r\n     bottom: 0;\r\n     width: 100%;\r\n     background-color: \t\t#b2b2b2;\r\n     color: white;\r\n     text-align: center;\r\n  }\r\ndiv.home {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\ndiv.home input {\r\n    background: url('home.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\ndiv.analytic {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\ndiv.analytic input {\r\n    background: url('analytic.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\ndiv.notification {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\ndiv.notification input {\r\n    background: url('outline-today-24px.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\ndiv.setting {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\ndiv.setting input {\r\n    background: url('settings.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\nspan.home {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 6px;\r\n  }\r\nspan.analytic {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 2px;\r\n  }\r\nspan.notification {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 2px;\r\n  }\r\nspan.setting {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 12px;\r\n  }\r\n.headercalendar {\r\n    overflow: hidden;\r\n    background-color: #f2f2f2;\r\n    padding: 10px 50px;\r\n    text-align: center;\r\n    \r\n  }\r\ndiv.day1 {\r\n    position: relative;\r\n    display: inline-block;\r\n    \r\n    \r\n    \r\n  }\r\ndiv.day1 input {\r\n    background: url('outline-today-24px.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 70px;\r\n    border: none;\r\n  }\r\nspan.day1 {\r\n    position: absolute;\r\n    top: 28px;\r\n    left: 28px;\r\n  }\r\ndiv.day2 {\r\n    position: relative;\r\n    display: inline-block;\r\n    \r\n    \r\n    \r\n  }\r\ndiv.day2 input {\r\n    background: url('outline-today-24px.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 70px;\r\n    border: none;\r\n  }\r\nspan.day2 {\r\n    position: absolute;\r\n    top: 28px;\r\n    left: 28px;\r\n  }\r\ndiv.day3 {\r\n    position: relative;\r\n    display: inline-block;\r\n    \r\n    \r\n    \r\n  }\r\ndiv.day3 input {\r\n    background: url('outline-today-24px.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 70px;\r\n    border: none;\r\n  }\r\nspan.day3 {\r\n    position: absolute;\r\n    top: 28px;\r\n    left: 28px;\r\n  }\r\ndiv.day4 {\r\n    position: relative;\r\n    display: inline-block;\r\n    \r\n    \r\n    \r\n  }\r\ndiv.day4 input {\r\n    background: url('outline-today-24px.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 70px;\r\n    border: none;\r\n  }\r\nspan.day4 {\r\n    position: absolute;\r\n    top: 28px;\r\n    left: 28px;\r\n  }\r\ndiv.day5 {\r\n    position: relative;\r\n    display: inline-block;\r\n    \r\n    \r\n    \r\n  }\r\ndiv.day5 input {\r\n    background: url('outline-today-24px.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 70px;\r\n    border: none;\r\n  }\r\nspan.day5 {\r\n    position: absolute;\r\n    top: 28px;\r\n    left: 28px;\r\n  }\r\nul {\r\n    max-width: 600px;\r\n    color: #333;\r\n    list-style: none;\r\n    margin: 50px auto;\r\n    padding: 10px;\r\n}\r\n.list li {\r\n    padding: 25px;\r\n    background: #f2f2f2;\r\n    border-radius: 5px;\r\n    margin-bottom: 5px;\r\n    text-align: center;\r\n    font-size: 32px;\r\n    font-weight: 600;\r\n}\r\n.center {\r\n    text-align: center;\r\n}\r\n/*\r\n#myProgress {\r\n  width: 100%;\r\n  background-color: #ddd;\r\n}\r\n\r\n#myBar {\r\n  width: 1%;\r\n  height: 30px;\r\n  background-color: #ff8000;\r\n}\r\n*/\r\n#barall {\r\npadding-top:130px;\r\n\r\n}\r\n.loader {\r\n  border: 16px solid #f3f3f3;\r\n  border-radius: 50%;\r\n  border-top: 16px solid #ff8000;\r\n  margin: auto;\r\n  width: 120px;\r\n  height: 120px;\r\n  -webkit-animation: spin 2s linear infinite; /* Safari */\r\n  animation: spin 2s linear infinite;\r\n}\r\n/* Safari */\r\n@-webkit-keyframes spin {\r\n  0% { -webkit-transform: rotate(0deg); }\r\n  100% { -webkit-transform: rotate(360deg); }\r\n}\r\n@keyframes spin {\r\n  0% { -webkit-transform: rotate(0deg); transform: rotate(0deg); }\r\n  100% { -webkit-transform: rotate(360deg); transform: rotate(360deg); }\r\n}\r\ndiv.bullet1 input {\r\n  background: url('outline-keyboard_arrow_right-24px.svg') no-repeat;\r\n  cursor: pointer;\r\n  width: 50px;\r\n  height: 50px;\r\n  border: none;\r\n}\r\nspan.bullet1 {\r\n  position: absolute;\r\n  top: 32px;\r\n  left: 2px;\r\n}\r\ndiv.bullet1 {\r\n  position: relative;\r\n  display: inline-block;\r\n}\r\ndiv.icon1 input {\r\n  background: url('sharp-local_dining-24px.svg') no-repeat;\r\n  cursor: pointer;\r\n  width: 50px;\r\n  height: 50px;\r\n  border: none;\r\n}\r\nspan.icon1 {\r\n  position: absolute;\r\n  top: 32px;\r\n  left: 2px;\r\n}\r\ndiv.icon1 {\r\n  position: relative;\r\n  display: inline-block;\r\n}\r\ndiv.icon2 input {\r\n  background: url('twotone-watch_later-24px.svg') no-repeat;\r\n  cursor: pointer;\r\n  width: 50px;\r\n  height: 50px;\r\n  border: none;\r\n}\r\nspan.icon2 {\r\n  position: absolute;\r\n  top: 32px;\r\n  left: 2px;\r\n}\r\ndiv.icon2 {\r\n  position: relative;\r\n  display: inline-block;\r\n}\r\n#round {\r\n  border-radius: 15px 50px 30px;\r\n /* border-radius: 15px 50px 30px;\r\n  background: #73AD21;*/\r\n  padding: 0px; \r\n  width: 90px;\r\n  height: 70px; \r\n}\r\n#move {\r\n \r\n  padding-left: 160px; \r\n  \r\n}\r\n#nameid {\r\n \r\n  padding-left: 145px; \r\n \r\n  \r\n}\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2FsZW5kYXIvY2FsZW5kYXIuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs7OztFQVVFO0FBQ0Y7SUFDSSwwQkFBMEI7SUFDMUIsWUFBWTtJQUNaLFlBQVk7SUFDWix5QkFBeUI7SUFDekIsa0JBQWtCO0lBQ2xCLHFCQUFxQjtJQUNyQixxQkFBcUI7SUFDckIsZUFBZTtJQUNmLGdCQUFnQjtJQUNoQixlQUFlOztFQUVqQjtBQUdGO0dBQ0csMEJBQTBCO0dBQzFCLFlBQVk7R0FDWixZQUFZO0dBQ1oseUJBQXlCO0dBQ3pCLGtCQUFrQjtHQUNsQixxQkFBcUI7R0FDckIscUJBQXFCO0dBQ3JCLGVBQWU7R0FDZixnQkFBZ0I7R0FDaEIsZUFBZTtDQUNqQjtBQUNBO0dBQ0UsMEJBQTBCO0dBQzFCLFlBQVk7R0FDWixZQUFZO0dBQ1oseUJBQXlCO0dBQ3pCLGtCQUFrQjtHQUNsQixxQkFBcUI7R0FDckIscUJBQXFCO0dBQ3JCLGVBQWU7R0FDZixnQkFBZ0I7R0FDaEIsZUFBZTtDQUNqQjtBQUVBO0dBQ0UsMEJBQTBCO0dBQzFCLFlBQVk7R0FDWixZQUFZO0dBQ1oseUJBQXlCO0dBQ3pCLGtCQUFrQjtHQUNsQixxQkFBcUI7R0FDckIscUJBQXFCO0dBQ3JCLGVBQWU7R0FDZixnQkFBZ0I7R0FDaEIsZUFBZTtDQUNqQjtBQUtBO0lBQ0csbUJBQW1CO0lBQ25CLDBCQUEwQjtJQUMxQixZQUFZO0lBQ1osWUFBWTtJQUNaLHlCQUF5QjtJQUN6QixrQkFBa0I7SUFDbEIscUJBQXFCO0lBQ3JCLHFCQUFxQjtJQUNyQixlQUFlO0lBQ2YsZ0JBQWdCO0lBQ2hCLGVBQWU7O0VBRWpCO0FBR0Y7SUFDSSxtQkFBbUI7R0FDcEIsMEJBQTBCO0dBQzFCLFlBQVk7R0FDWixZQUFZO0dBQ1oseUJBQXlCO0dBQ3pCLGtCQUFrQjtHQUNsQixxQkFBcUI7R0FDckIscUJBQXFCO0dBQ3JCLGVBQWU7R0FDZixnQkFBZ0I7R0FDaEIsZUFBZTtDQUNqQjtBQUNBO0lBQ0csbUJBQW1CO0dBQ3BCLDBCQUEwQjtHQUMxQixZQUFZO0dBQ1osWUFBWTtHQUNaLHlCQUF5QjtHQUN6QixrQkFBa0I7R0FDbEIscUJBQXFCO0dBQ3JCLHFCQUFxQjtHQUNyQixlQUFlO0dBQ2YsZ0JBQWdCO0dBQ2hCLGVBQWU7Q0FDakI7QUFFQTtJQUNHLG1CQUFtQjtHQUNwQiwwQkFBMEI7R0FDMUIsWUFBWTtHQUNaLFlBQVk7R0FDWix5QkFBeUI7R0FDekIsa0JBQWtCO0dBQ2xCLHFCQUFxQjtHQUNyQixxQkFBcUI7R0FDckIsZUFBZTtHQUNmLGdCQUFnQjtHQUNoQixlQUFlO0NBQ2pCO0FBQ0E7SUFDRyxtQkFBbUI7SUFDbkIsMEJBQTBCO0lBQzFCLFlBQVk7SUFDWixZQUFZO0lBQ1oseUJBQXlCO0lBQ3pCLGtCQUFrQjtJQUNsQixxQkFBcUI7SUFDckIscUJBQXFCO0lBQ3JCLGVBQWU7SUFDZixnQkFBZ0I7SUFDaEIsZUFBZTtFQUNqQjtBQUdBO0tBQ0csa0JBQWtCO0tBQ2xCLGVBQWU7S0FDZixPQUFPO0tBQ1AsU0FBUztLQUNULFdBQVc7S0FDWCwyQkFBMkI7S0FDM0IsWUFBWTtLQUNaLGtCQUFrQjtFQUNyQjtBQUVBO0lBQ0Usa0JBQWtCO0lBQ2xCLHFCQUFxQjtFQUN2QjtBQUVBO0lBQ0UscUNBQTJEO0lBQzNELGVBQWU7SUFDZixXQUFXO0lBQ1gsWUFBWTtJQUNaLFlBQVk7RUFDZDtBQUNBO0lBQ0Usa0JBQWtCO0lBQ2xCLHFCQUFxQjtFQUN2QjtBQUVBO0lBQ0UseUNBQStEO0lBQy9ELGVBQWU7SUFDZixXQUFXO0lBQ1gsWUFBWTtJQUNaLFlBQVk7RUFDZDtBQUVBO0lBQ0Usa0JBQWtCO0lBQ2xCLHFCQUFxQjtFQUN2QjtBQUVBO0lBQ0UsbURBQTJFO0lBQzNFLGVBQWU7SUFDZixXQUFXO0lBQ1gsWUFBWTtJQUNaLFlBQVk7RUFDZDtBQUVBO0lBQ0Usa0JBQWtCO0lBQ2xCLHFCQUFxQjtFQUN2QjtBQUVBO0lBQ0UseUNBQStEO0lBQy9ELGVBQWU7SUFDZixXQUFXO0lBQ1gsWUFBWTtJQUNaLFlBQVk7RUFDZDtBQUNBO0lBQ0Usa0JBQWtCO0lBQ2xCLFNBQVM7SUFDVCxTQUFTO0VBQ1g7QUFFQTtJQUNFLGtCQUFrQjtJQUNsQixTQUFTO0lBQ1QsU0FBUztFQUNYO0FBRUE7SUFDRSxrQkFBa0I7SUFDbEIsU0FBUztJQUNULFNBQVM7RUFDWDtBQUVBO0lBQ0Usa0JBQWtCO0lBQ2xCLFNBQVM7SUFDVCxVQUFVO0VBQ1o7QUFRQTtJQUNFLGdCQUFnQjtJQUNoQix5QkFBeUI7SUFDekIsa0JBQWtCO0lBQ2xCLGtCQUFrQjs7RUFFcEI7QUFJQTtJQUNFLGtCQUFrQjtJQUNsQixxQkFBcUI7Ozs7RUFJdkI7QUFFQTtJQUNFLG1EQUEyRTtJQUMzRSxlQUFlO0lBQ2YsV0FBVztJQUNYLFlBQVk7SUFDWixZQUFZO0VBQ2Q7QUFFQTtJQUNFLGtCQUFrQjtJQUNsQixTQUFTO0lBQ1QsVUFBVTtFQUNaO0FBTUE7SUFDRSxrQkFBa0I7SUFDbEIscUJBQXFCOzs7O0VBSXZCO0FBRUE7SUFDRSxtREFBMkU7SUFDM0UsZUFBZTtJQUNmLFdBQVc7SUFDWCxZQUFZO0lBQ1osWUFBWTtFQUNkO0FBRUE7SUFDRSxrQkFBa0I7SUFDbEIsU0FBUztJQUNULFVBQVU7RUFDWjtBQUtBO0lBQ0Usa0JBQWtCO0lBQ2xCLHFCQUFxQjs7OztFQUl2QjtBQUVBO0lBQ0UsbURBQTJFO0lBQzNFLGVBQWU7SUFDZixXQUFXO0lBQ1gsWUFBWTtJQUNaLFlBQVk7RUFDZDtBQUVBO0lBQ0Usa0JBQWtCO0lBQ2xCLFNBQVM7SUFDVCxVQUFVO0VBQ1o7QUFLQTtJQUNFLGtCQUFrQjtJQUNsQixxQkFBcUI7Ozs7RUFJdkI7QUFFQTtJQUNFLG1EQUEyRTtJQUMzRSxlQUFlO0lBQ2YsV0FBVztJQUNYLFlBQVk7SUFDWixZQUFZO0VBQ2Q7QUFFQTtJQUNFLGtCQUFrQjtJQUNsQixTQUFTO0lBQ1QsVUFBVTtFQUNaO0FBS0E7SUFDRSxrQkFBa0I7SUFDbEIscUJBQXFCOzs7O0VBSXZCO0FBRUE7SUFDRSxtREFBMkU7SUFDM0UsZUFBZTtJQUNmLFdBQVc7SUFDWCxZQUFZO0lBQ1osWUFBWTtFQUNkO0FBRUE7SUFDRSxrQkFBa0I7SUFDbEIsU0FBUztJQUNULFVBQVU7RUFDWjtBQVVBO0lBQ0UsZ0JBQWdCO0lBQ2hCLFdBQVc7SUFDWCxnQkFBZ0I7SUFDaEIsaUJBQWlCO0lBQ2pCLGFBQWE7QUFDakI7QUFFQTtJQUNJLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsa0JBQWtCO0lBQ2xCLGtCQUFrQjtJQUNsQixrQkFBa0I7SUFDbEIsZUFBZTtJQUNmLGdCQUFnQjtBQUNwQjtBQUVBO0lBQ0ksa0JBQWtCO0FBQ3RCO0FBQ0E7Ozs7Ozs7Ozs7O0NBV0M7QUFFRDtBQUNBLGlCQUFpQjs7QUFFakI7QUFLQTtFQUNFLDBCQUEwQjtFQUMxQixrQkFBa0I7RUFDbEIsOEJBQThCO0VBQzlCLFlBQVk7RUFDWixZQUFZO0VBQ1osYUFBYTtFQUNiLDBDQUEwQyxFQUFFLFdBQVc7RUFDdkQsa0NBQWtDO0FBQ3BDO0FBRUEsV0FBVztBQUNYO0VBQ0UsS0FBSywrQkFBK0IsRUFBRTtFQUN0QyxPQUFPLGlDQUFpQyxFQUFFO0FBQzVDO0FBRUE7RUFDRSxLQUFLLCtCQUF1QixFQUF2Qix1QkFBdUIsRUFBRTtFQUM5QixPQUFPLGlDQUF5QixFQUF6Qix5QkFBeUIsRUFBRTtBQUNwQztBQUdBO0VBQ0Usa0VBQXVGO0VBQ3ZGLGVBQWU7RUFDZixXQUFXO0VBQ1gsWUFBWTtFQUNaLFlBQVk7QUFDZDtBQUVBO0VBQ0Usa0JBQWtCO0VBQ2xCLFNBQVM7RUFDVCxTQUFTO0FBQ1g7QUFDQTtFQUNFLGtCQUFrQjtFQUNsQixxQkFBcUI7QUFDdkI7QUFDQTtFQUNFLHdEQUE2RTtFQUM3RSxlQUFlO0VBQ2YsV0FBVztFQUNYLFlBQVk7RUFDWixZQUFZO0FBQ2Q7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQixTQUFTO0VBQ1QsU0FBUztBQUNYO0FBQ0E7RUFDRSxrQkFBa0I7RUFDbEIscUJBQXFCO0FBQ3ZCO0FBS0E7RUFDRSx5REFBOEU7RUFDOUUsZUFBZTtFQUNmLFdBQVc7RUFDWCxZQUFZO0VBQ1osWUFBWTtBQUNkO0FBRUE7RUFDRSxrQkFBa0I7RUFDbEIsU0FBUztFQUNULFNBQVM7QUFDWDtBQUNBO0VBQ0Usa0JBQWtCO0VBQ2xCLHFCQUFxQjtBQUN2QjtBQUdBO0VBQ0UsNkJBQTZCO0NBQzlCO3VCQUNzQjtFQUNyQixZQUFZO0VBQ1osV0FBVztFQUNYLFlBQVk7QUFDZDtBQUdBOztFQUVFLG1CQUFtQjs7QUFFckI7QUFHQTs7RUFFRSxtQkFBbUI7OztBQUdyQiIsImZpbGUiOiJzcmMvYXBwL2NhbGVuZGFyL2NhbGVuZGFyLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG4jbXlQcm9ncmVzcyB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2RkZDtcclxufVxyXG5cclxuI215QmFyIHtcclxuICB3aWR0aDogMSU7XHJcbiAgaGVpZ2h0OiAzMHB4O1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICM0Q0FGNTA7XHJcbn0qL1xyXG4jYnV0dG9uMSB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjYjJiMmIyXHQ7IFxyXG4gICAgYm9yZGVyOiBub25lO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgcGFkZGluZy1ib3R0b206IDI1cHggMjJweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIGZvbnQtc2l6ZTogMzZweDtcclxuICAgIG1hcmdpbjogNHB4IDE4cHg7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICBcclxuICB9XHJcblxyXG4gIFxyXG4jYnV0dG9uMiB7XHJcbiAgIGJhY2tncm91bmQtY29sb3I6ICNiMmIyYjJcdDsgXHJcbiAgIGJvcmRlcjogbm9uZTtcclxuICAgY29sb3I6IHdoaXRlO1xyXG4gICBwYWRkaW5nLWJvdHRvbTogMjVweCAyMnB4O1xyXG4gICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICBmb250LXNpemU6IDM2cHg7XHJcbiAgIG1hcmdpbjogNHB4IDE4cHg7XHJcbiAgIGN1cnNvcjogcG9pbnRlcjtcclxuIH1cclxuICNidXR0b24zIHtcclxuICAgYmFja2dyb3VuZC1jb2xvcjogI2IyYjJiMlx0OyBcclxuICAgYm9yZGVyOiBub25lO1xyXG4gICBjb2xvcjogd2hpdGU7XHJcbiAgIHBhZGRpbmctYm90dG9tOiAyNXB4IDIycHg7XHJcbiAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgIGZvbnQtc2l6ZTogMzZweDtcclxuICAgbWFyZ2luOiA0cHggMThweDtcclxuICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gfVxyXG5cclxuICNidXR0b240IHtcclxuICAgYmFja2dyb3VuZC1jb2xvcjogI2IyYjJiMlx0O1xyXG4gICBib3JkZXI6IG5vbmU7XHJcbiAgIGNvbG9yOiB3aGl0ZTtcclxuICAgcGFkZGluZy1ib3R0b206IDI1cHggMjJweDtcclxuICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgZm9udC1zaXplOiAzNnB4O1xyXG4gICBtYXJnaW46IDRweCAxOHB4O1xyXG4gICBjdXJzb3I6IHBvaW50ZXI7XHJcbiB9XHJcblxyXG5cclxuXHJcblxyXG4gI2J1dHRvbmEge1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNiMmIyYjJcdDsgXHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMjVweCAyMnB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgZm9udC1zaXplOiAzNnB4O1xyXG4gICAgbWFyZ2luOiA0cHggMThweDtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIFxyXG4gIH1cclxuXHJcbiAgXHJcbiNidXR0b25iIHtcclxuICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbiAgIGJhY2tncm91bmQtY29sb3I6ICNiMmIyYjJcdDsgXHJcbiAgIGJvcmRlcjogbm9uZTtcclxuICAgY29sb3I6IHdoaXRlO1xyXG4gICBwYWRkaW5nLWJvdHRvbTogMjVweCAyMnB4O1xyXG4gICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICBmb250LXNpemU6IDM2cHg7XHJcbiAgIG1hcmdpbjogNHB4IDE4cHg7XHJcbiAgIGN1cnNvcjogcG9pbnRlcjtcclxuIH1cclxuICNidXR0b25jIHtcclxuICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbiAgIGJhY2tncm91bmQtY29sb3I6ICNiMmIyYjJcdDsgXHJcbiAgIGJvcmRlcjogbm9uZTtcclxuICAgY29sb3I6IHdoaXRlO1xyXG4gICBwYWRkaW5nLWJvdHRvbTogMjVweCAyMnB4O1xyXG4gICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICBmb250LXNpemU6IDM2cHg7XHJcbiAgIG1hcmdpbjogNHB4IDE4cHg7XHJcbiAgIGN1cnNvcjogcG9pbnRlcjtcclxuIH1cclxuXHJcbiAjYnV0dG9uZCB7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xyXG4gICBiYWNrZ3JvdW5kLWNvbG9yOiAjYjJiMmIyXHQ7XHJcbiAgIGJvcmRlcjogbm9uZTtcclxuICAgY29sb3I6IHdoaXRlO1xyXG4gICBwYWRkaW5nLWJvdHRvbTogMjVweCAyMnB4O1xyXG4gICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICBmb250LXNpemU6IDM2cHg7XHJcbiAgIG1hcmdpbjogNHB4IDE4cHg7XHJcbiAgIGN1cnNvcjogcG9pbnRlcjtcclxuIH1cclxuICNidXR0b25lIHtcclxuICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjYjJiMmIyXHQ7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMjVweCAyMnB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgZm9udC1zaXplOiAzNnB4O1xyXG4gICAgbWFyZ2luOiA0cHggMThweDtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICB9XHJcbiAgXHJcbiBcclxuICAjYWxse1xyXG4gICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgICAgbGVmdDogMDtcclxuICAgICBib3R0b206IDA7XHJcbiAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgYmFja2dyb3VuZC1jb2xvcjogXHRcdCNiMmIyYjI7XHJcbiAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICB9XHJcblxyXG4gIGRpdi5ob21lIHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICB9XHJcblxyXG4gIGRpdi5ob21lIGlucHV0IHtcclxuICAgIGJhY2tncm91bmQ6IHVybCgnc3JjL2FwcC9oZWFkZXIvaW1hZ2VzL2hvbWUuc3ZnJykgbm8tcmVwZWF0O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgd2lkdGg6IDUwcHg7XHJcbiAgICBoZWlnaHQ6IDUwcHg7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgfVxyXG4gIGRpdi5hbmFseXRpYyB7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgfVxyXG5cclxuICBkaXYuYW5hbHl0aWMgaW5wdXQge1xyXG4gICAgYmFja2dyb3VuZDogdXJsKCdzcmMvYXBwL2hlYWRlci9pbWFnZXMvYW5hbHl0aWMuc3ZnJykgbm8tcmVwZWF0O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgd2lkdGg6IDUwcHg7XHJcbiAgICBoZWlnaHQ6IDUwcHg7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgfVxyXG5cclxuICBkaXYubm90aWZpY2F0aW9uIHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICB9XHJcblxyXG4gIGRpdi5ub3RpZmljYXRpb24gaW5wdXQge1xyXG4gICAgYmFja2dyb3VuZDogdXJsKCdzcmMvYXBwL2NhbGVuZGFyL2ltYWdlcy9vdXRsaW5lLXRvZGF5LTI0cHguc3ZnJykgbm8tcmVwZWF0O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgd2lkdGg6IDUwcHg7XHJcbiAgICBoZWlnaHQ6IDUwcHg7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgfVxyXG4gIFxyXG4gIGRpdi5zZXR0aW5nIHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICB9XHJcblxyXG4gIGRpdi5zZXR0aW5nIGlucHV0IHtcclxuICAgIGJhY2tncm91bmQ6IHVybCgnc3JjL2FwcC9oZWFkZXIvaW1hZ2VzL3NldHRpbmdzLnN2ZycpIG5vLXJlcGVhdDtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIHdpZHRoOiA1MHB4O1xyXG4gICAgaGVpZ2h0OiA1MHB4O1xyXG4gICAgYm9yZGVyOiBub25lO1xyXG4gIH1cclxuICBzcGFuLmhvbWUge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAzMnB4O1xyXG4gICAgbGVmdDogNnB4O1xyXG4gIH1cclxuICBcclxuICBzcGFuLmFuYWx5dGljIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogMzJweDtcclxuICAgIGxlZnQ6IDJweDtcclxuICB9XHJcblxyXG4gIHNwYW4ubm90aWZpY2F0aW9uIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogMzJweDtcclxuICAgIGxlZnQ6IDJweDtcclxuICB9XHJcblxyXG4gIHNwYW4uc2V0dGluZyB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDMycHg7XHJcbiAgICBsZWZ0OiAxMnB4O1xyXG4gIH1cclxuXHJcblxyXG4gIFxyXG5cclxuXHJcblxyXG5cclxuICAuaGVhZGVyY2FsZW5kYXIge1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmMmYyZjI7XHJcbiAgICBwYWRkaW5nOiAxMHB4IDUwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBcclxuICB9XHJcblxyXG5cclxuXHJcbiAgZGl2LmRheTEge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgXHJcbiAgICBcclxuICAgIFxyXG4gIH1cclxuXHJcbiAgZGl2LmRheTEgaW5wdXQge1xyXG4gICAgYmFja2dyb3VuZDogdXJsKCdzcmMvYXBwL2NhbGVuZGFyL2ltYWdlcy9vdXRsaW5lLXRvZGF5LTI0cHguc3ZnJykgbm8tcmVwZWF0O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgd2lkdGg6IDUwcHg7XHJcbiAgICBoZWlnaHQ6IDcwcHg7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgfVxyXG5cclxuICBzcGFuLmRheTEge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAyOHB4O1xyXG4gICAgbGVmdDogMjhweDtcclxuICB9XHJcblxyXG5cclxuXHJcblxyXG4gIFxyXG4gIGRpdi5kYXkyIHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIFxyXG4gICAgXHJcbiAgICBcclxuICB9XHJcblxyXG4gIGRpdi5kYXkyIGlucHV0IHtcclxuICAgIGJhY2tncm91bmQ6IHVybCgnc3JjL2FwcC9jYWxlbmRhci9pbWFnZXMvb3V0bGluZS10b2RheS0yNHB4LnN2ZycpIG5vLXJlcGVhdDtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIHdpZHRoOiA1MHB4O1xyXG4gICAgaGVpZ2h0OiA3MHB4O1xyXG4gICAgYm9yZGVyOiBub25lO1xyXG4gIH1cclxuXHJcbiAgc3Bhbi5kYXkyIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogMjhweDtcclxuICAgIGxlZnQ6IDI4cHg7XHJcbiAgfVxyXG5cclxuXHJcblxyXG4gIFxyXG4gIGRpdi5kYXkzIHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIFxyXG4gICAgXHJcbiAgICBcclxuICB9XHJcblxyXG4gIGRpdi5kYXkzIGlucHV0IHtcclxuICAgIGJhY2tncm91bmQ6IHVybCgnc3JjL2FwcC9jYWxlbmRhci9pbWFnZXMvb3V0bGluZS10b2RheS0yNHB4LnN2ZycpIG5vLXJlcGVhdDtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIHdpZHRoOiA1MHB4O1xyXG4gICAgaGVpZ2h0OiA3MHB4O1xyXG4gICAgYm9yZGVyOiBub25lO1xyXG4gIH1cclxuXHJcbiAgc3Bhbi5kYXkzIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogMjhweDtcclxuICAgIGxlZnQ6IDI4cHg7XHJcbiAgfVxyXG5cclxuXHJcblxyXG4gIFxyXG4gIGRpdi5kYXk0IHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIFxyXG4gICAgXHJcbiAgICBcclxuICB9XHJcblxyXG4gIGRpdi5kYXk0IGlucHV0IHtcclxuICAgIGJhY2tncm91bmQ6IHVybCgnc3JjL2FwcC9jYWxlbmRhci9pbWFnZXMvb3V0bGluZS10b2RheS0yNHB4LnN2ZycpIG5vLXJlcGVhdDtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIHdpZHRoOiA1MHB4O1xyXG4gICAgaGVpZ2h0OiA3MHB4O1xyXG4gICAgYm9yZGVyOiBub25lO1xyXG4gIH1cclxuXHJcbiAgc3Bhbi5kYXk0IHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogMjhweDtcclxuICAgIGxlZnQ6IDI4cHg7XHJcbiAgfVxyXG5cclxuXHJcblxyXG4gXHJcbiAgZGl2LmRheTUge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgXHJcbiAgICBcclxuICAgIFxyXG4gIH1cclxuXHJcbiAgZGl2LmRheTUgaW5wdXQge1xyXG4gICAgYmFja2dyb3VuZDogdXJsKCdzcmMvYXBwL2NhbGVuZGFyL2ltYWdlcy9vdXRsaW5lLXRvZGF5LTI0cHguc3ZnJykgbm8tcmVwZWF0O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgd2lkdGg6IDUwcHg7XHJcbiAgICBoZWlnaHQ6IDcwcHg7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgfVxyXG5cclxuICBzcGFuLmRheTUge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAyOHB4O1xyXG4gICAgbGVmdDogMjhweDtcclxuICB9XHJcblxyXG5cclxuXHJcblxyXG5cclxuXHJcblxyXG5cclxuICBcclxuICB1bCB7XHJcbiAgICBtYXgtd2lkdGg6IDYwMHB4O1xyXG4gICAgY29sb3I6ICMzMzM7XHJcbiAgICBsaXN0LXN0eWxlOiBub25lO1xyXG4gICAgbWFyZ2luOiA1MHB4IGF1dG87XHJcbiAgICBwYWRkaW5nOiAxMHB4O1xyXG59XHJcblxyXG4ubGlzdCBsaSB7XHJcbiAgICBwYWRkaW5nOiAyNXB4O1xyXG4gICAgYmFja2dyb3VuZDogI2YyZjJmMjtcclxuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgIG1hcmdpbi1ib3R0b206IDVweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGZvbnQtc2l6ZTogMzJweDtcclxuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbn1cclxuXHJcbi5jZW50ZXIge1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcbi8qXHJcbiNteVByb2dyZXNzIHtcclxuICB3aWR0aDogMTAwJTtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZGRkO1xyXG59XHJcblxyXG4jbXlCYXIge1xyXG4gIHdpZHRoOiAxJTtcclxuICBoZWlnaHQ6IDMwcHg7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmODAwMDtcclxufVxyXG4qL1xyXG4gIFxyXG4jYmFyYWxsIHtcclxucGFkZGluZy10b3A6MTMwcHg7XHJcblxyXG59XHJcbiAgXHJcblxyXG5cclxuXHJcbi5sb2FkZXIge1xyXG4gIGJvcmRlcjogMTZweCBzb2xpZCAjZjNmM2YzO1xyXG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICBib3JkZXItdG9wOiAxNnB4IHNvbGlkICNmZjgwMDA7XHJcbiAgbWFyZ2luOiBhdXRvO1xyXG4gIHdpZHRoOiAxMjBweDtcclxuICBoZWlnaHQ6IDEyMHB4O1xyXG4gIC13ZWJraXQtYW5pbWF0aW9uOiBzcGluIDJzIGxpbmVhciBpbmZpbml0ZTsgLyogU2FmYXJpICovXHJcbiAgYW5pbWF0aW9uOiBzcGluIDJzIGxpbmVhciBpbmZpbml0ZTtcclxufVxyXG5cclxuLyogU2FmYXJpICovXHJcbkAtd2Via2l0LWtleWZyYW1lcyBzcGluIHtcclxuICAwJSB7IC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7IH1cclxuICAxMDAlIHsgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgzNjBkZWcpOyB9XHJcbn1cclxuXHJcbkBrZXlmcmFtZXMgc3BpbiB7XHJcbiAgMCUgeyB0cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKTsgfVxyXG4gIDEwMCUgeyB0cmFuc2Zvcm06IHJvdGF0ZSgzNjBkZWcpOyB9XHJcbn1cclxuXHJcblxyXG5kaXYuYnVsbGV0MSBpbnB1dCB7XHJcbiAgYmFja2dyb3VuZDogdXJsKCdzcmMvYXBwL2luZm9zL2ltYWdlcy9vdXRsaW5lLWtleWJvYXJkX2Fycm93X3JpZ2h0LTI0cHguc3ZnJykgbm8tcmVwZWF0O1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxuICB3aWR0aDogNTBweDtcclxuICBoZWlnaHQ6IDUwcHg7XHJcbiAgYm9yZGVyOiBub25lO1xyXG59XHJcblxyXG5zcGFuLmJ1bGxldDEge1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICB0b3A6IDMycHg7XHJcbiAgbGVmdDogMnB4O1xyXG59XHJcbmRpdi5idWxsZXQxIHtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG59XHJcbmRpdi5pY29uMSBpbnB1dCB7XHJcbiAgYmFja2dyb3VuZDogdXJsKCdzcmMvYXBwL2luZm9zL2ltYWdlcy9zaGFycC1sb2NhbF9kaW5pbmctMjRweC5zdmcnKSBuby1yZXBlYXQ7XHJcbiAgY3Vyc29yOiBwb2ludGVyO1xyXG4gIHdpZHRoOiA1MHB4O1xyXG4gIGhlaWdodDogNTBweDtcclxuICBib3JkZXI6IG5vbmU7XHJcbn1cclxuXHJcbnNwYW4uaWNvbjEge1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICB0b3A6IDMycHg7XHJcbiAgbGVmdDogMnB4O1xyXG59XHJcbmRpdi5pY29uMSB7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxufVxyXG5cclxuXHJcblxyXG5cclxuZGl2Lmljb24yIGlucHV0IHtcclxuICBiYWNrZ3JvdW5kOiB1cmwoJ3NyYy9hcHAvaW5mb3MvaW1hZ2VzL3R3b3RvbmUtd2F0Y2hfbGF0ZXItMjRweC5zdmcnKSBuby1yZXBlYXQ7XHJcbiAgY3Vyc29yOiBwb2ludGVyO1xyXG4gIHdpZHRoOiA1MHB4O1xyXG4gIGhlaWdodDogNTBweDtcclxuICBib3JkZXI6IG5vbmU7XHJcbn1cclxuXHJcbnNwYW4uaWNvbjIge1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICB0b3A6IDMycHg7XHJcbiAgbGVmdDogMnB4O1xyXG59XHJcbmRpdi5pY29uMiB7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxufVxyXG5cclxuXHJcbiNyb3VuZCB7XHJcbiAgYm9yZGVyLXJhZGl1czogMTVweCA1MHB4IDMwcHg7XHJcbiAvKiBib3JkZXItcmFkaXVzOiAxNXB4IDUwcHggMzBweDtcclxuICBiYWNrZ3JvdW5kOiAjNzNBRDIxOyovXHJcbiAgcGFkZGluZzogMHB4OyBcclxuICB3aWR0aDogOTBweDtcclxuICBoZWlnaHQ6IDcwcHg7IFxyXG59XHJcblxyXG5cclxuI21vdmUge1xyXG4gXHJcbiAgcGFkZGluZy1sZWZ0OiAxNjBweDsgXHJcbiAgXHJcbn1cclxuXHJcblxyXG4jbmFtZWlkIHtcclxuIFxyXG4gIHBhZGRpbmctbGVmdDogMTQ1cHg7IFxyXG4gXHJcbiAgXHJcbn1cclxuXHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/calendar/calendar.component.html":
/*!**************************************************!*\
  !*** ./src/app/calendar/calendar.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n\n<link  href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css\"  rel=\"stylesheet\">\n\n\n<!--\n<div id=\"main\">\n <nav id=\"header\" class=\"navbar navbar-expand-lg navbar-dark bg-dark\">\n  <a class=\"navbar-brand\" href=\"#\">\n    FANI\n  </a>\n\n  <button class=\"navbar-toggler\" type=\"button\"\n          (click)=\"toggleNavbar()\">\n    <span class=\"navbar-toggler-icon\"></span>\n  </button>\n\n\n  <div class=\"collapse navbar-collapse\"\n       [ngClass]=\"{ 'show': navbarOpen }\">\n    <ul class=\"navbar-nav mr-auto\">\n      <li class=\"nav-item\">\n        <a class=\"nav-link\"routerLink=\"/signup\">calendar</a>\n      </li>\n\n      <li class=\"nav-item\">\n        <a class=\"nav-link\" href=\"#\">logout</a>\n      </li>\n    </ul>\n  </div>\n </nav>\n\n</div>-->\n\n\n\n <!-- <div class=\"container\">\n            \n      <div class=\"row\">\n          \n         <div class=\"col-sm-12\" style=\"\">\n            <div id=\"myProgress\">\n                <div id=\"myBar\"></div>\n              </div>\n          </div>\n          </div>\n    </div>-->\n\n    <div class=\"startdate\">\n\n        <h1 id=\"headcalendar\" align=\"center\" style=\"color:#262626;\">Weekly plan</h1>\n\n      </div>\n\n      <div class=\"container\">\n      <div  id=\"barall\" class=\"row\">\n          <div class=\"col-md-12\">\n            <div  align=\"center\"   id=\"loading\"> Loading...</div>\n             <!-- <div id=\"myProgress\">\n                 \n                 <div id=\"myBar\"></div>\n              </div>-->\n              <div id=\"loader\" align=\"center\" class=\"loader\"></div>\n          </div>\n        </div>\n      </div>\n\n<div id=\"headercalendar\">\n    <div class=\"headercalendar\" >\n     \n    <div class=\"day1\" id=\"dayall\">\n        <span id=\"day1\" class=\"day1\">Day1  </span>\n        <input id=\"buttona\" type=\"button\"  (click)=\"submitted=false\" (click)=\"goToPage('/', 'calendar','monday')\">\n      \n    </div>\n\n\n\n    <div class=\"day2\">\n      <span id=\"day2\" class=\"day2\">Day2  </span>\n      <input id=\"buttonb\" type=\"button\"  (click)=\"submitted=false\" (click)=\"goToPage('/', 'main','tuesday')\">\n    \n    </div>\n\n\n    <div class=\"day3\">\n      <span id=\"day3\" class=\"day3\">Day3  </span>\n      <input id=\"buttonc\" type=\"button\"  (click)=\"submitted=false\" (click)=\"goToPage('/', 'main','wednesday')\">\n    \n    </div>\n\n\n\n\n    <div class=\"day4\">\n      <span id=\"day4\" class=\"day4\">Day4  </span>\n      <input id=\"buttond\" type=\"button\"  (click)=\"submitted=false\" (click)=\"goToPage('/', 'main','thursday')\">\n    \n  </div>\n\n\n  <div class=\"day5\">\n    <span id=\"day5\" class=\"day5\">Day5  </span>\n    <input id=\"buttone\" type=\"button\"  (click)=\"submitted=false\" (click)=\"goToPage('/', 'main','friday')\">\n  \n </div>\n  \n    </div>\n</div>    \n  \n<div id=\"maincontenthome\">\n  \n\n    <div id=\"menu\"> <th>Menu</th></div>\n\n<div class=\"container\">\n  <!--  <ul class=\"list\">\n        <cdk-virtual-scroll-viewport  style=\"height: 500px\" itemSize=\"90\" >\n          <ng-container   *cdkVirtualFor=\"let n of data\" >\n              <h3 style=\"color:blue;\">{{n._id}}</h3>\n              <li> {{\n             \n              \n                n.name\n              \n              }} </li>\n\n          \n          </ng-container>\n        \n          \n        </cdk-virtual-scroll-viewport>\n      </ul>-->\n    <table class=\"table table-striped\">\n      <thead>\n          <tr>\n          \n           \n           \n          </tr>\n      </thead>\n      <tbody>\n        <tr *ngFor =\"let d of data; \"  (click)=\"onSelect(d.name)\">\n            \n          <td>\n\n              <div class=\"container\">\n                  <div class=\"row\">\n                    <div class=\"col-md\">\n                      \n                    </div>\n                    <div class=\"col-md\">\n                        \n                    </div>\n                   \n                   \n                  </div>\n              </div>\n             \n             \n             \n              <div class=\"container\">\n                <div class=\"row\">\n                  <div class=\"col-xs\">\n                    <img align=\"left\" id=\"round\"  src={{d.imageurl}}>\n                  </div>\n                  <div class=\"col-xs\">\n                      <h6 style=\"color:blue;\">{{d.type}}</h6>\n                    <h5 id=\"nameid\"   style=\"color:grey;\"> {{d.name}} </h5>\n                     \n\n\n                   \n\n\n                    <div class=\"container\" id=\"move\">\n                        <div class=\"row\">\n\n                            <div class=\"col-xs\">\n                                \n                              \n                              </div>\n                          \n                            <div class=\"col-xs\">\n                                <div id=\"bullets\">\n                                    <div class=\"icon2\">\n                                        <span id=\"icon2\" class=\"icon2\">  </span>\n                                         <input id=\"icon2\" type=\"button\" width=\"48\" height=\"48\" >\n                                    </div>\n                  \n                  \n                  \n                                </div> \n                              </div>\n                    <div class=\"col-xs\">\n                        <div id=\"bullets\" >\n                            <div class=\"icon1\">\n                                <span id=\"icon1\" class=\"icon1\">  </span>\n                                 <input id=\"icon1\" type=\"button\" width=\"48\" height=\"48\" >\n                            </div>\n          \n      \n                        </div>  \n                      </div>\n                      <div class=\"col-xs\">\n                         \n                      </div>\n  \n\n                        </div>\n                    </div>\n                  </div>\n                  \n                </div>\n              </div>\n               \n            \n               \n           \n          \n          \n         \n           \n        </tr>\n      </tbody>\n    </table>\n  </div>\n  \n</div>\n\n\n<div id=\"all\">\n\n  \n    <div class=\"home\">\n        <span id=\"home\" class=\"home\">Home  </span>\n        <input id=\"button1\" type=\"button\"  (click)=\"submitted=false\" (click)=\"goToPageMenu('/', 'main')\">\n      \n    </div>\n\n     <div class=\"analytic\">\n      <span id=\"analytic\" class=\"analytic\">Analytics  </span>\n      <input id=\"button2\" type=\"button\" alt=\"Submit\" width=\"48\" height=\"48\" (click)=\"submitted=false\" (click)=\"goToPageMenu('/', 'analytic')\">\n  \n    </div>\n\n    <div class=\"notification\">\n        <span id=\"notification\" class=\"notification\">Calendar  </span>\n        <input id=\"button3\" type=\"button\" alt=\"Submit\" width=\"48\" height=\"48\" (click)=\"submitted=false\" (click)=\"goToPageMenu('/', 'calendar')\">\n    \n      </div>\n    \n      <div class=\"setting\">\n          <span id=\"setting\" class=\"setting\">Settings  </span>\n          <input id=\"button4\" type=\"button\" alt=\"Submit\" width=\"48\" height=\"48\" (click)=\"submitted=false\" (click)=\"goToPageMenu('/', 'settings')\">\n      \n        </div>\n   \n     \n \n</div>\n\n\n<!--\n<div class=\"container\">\n  <div class=\"row\">\n    <div class=\"col-sm\">\n      <button class=\"button\">Green</button>\n    </div>\n    <div class=\"col-sm\">\n      <button class=\"button\">Green</button>\n    </div>\n    <div class=\"col-sm\">\n        <button class=\"button\">Green</button>\n     \n    </div>\n\n    <div class=\"col-sm\">\n        <button class=\"button\">Green</button>\n     \n    </div>\n\n   \n  </div>\n</div>\n-->\n\n\n"

/***/ }),

/***/ "./src/app/calendar/calendar.component.ts":
/*!************************************************!*\
  !*** ./src/app/calendar/calendar.component.ts ***!
  \************************************************/
/*! exports provided: CalendarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CalendarComponent", function() { return CalendarComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _calendar_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./calendar.service */ "./src/app/calendar/calendar.service.ts");






var CalendarComponent = /** @class */ (function () {
    function CalendarComponent(http, router, CalendarService) {
        this.http = http;
        this.router = router;
        this.CalendarService = CalendarService;
        this.url = "http://localhost:3000/api/things/getmenu/breakfast/";
        this.urlinter1 = "http://localhost:3000/api/things/getmenu/inter1/";
        this.urlunch = "http://localhost:3000/api/things/getmenu/lunch/";
        this.urlinter2 = "http://localhost:3000/api/things/getmenu/inter2/";
        this.urldinner = "http://localhost:3000/api/things/getmenu/dinner/";
        this.numbers = [];
        this.navbarOpen = false;
        for (var index = 0; index < 10000; index++) {
            this.numbers.push(index);
        }
    }
    CalendarComponent.prototype.onSelect = function (hero) {
        //alert("hier"+hero);
        this.selectedHero = hero;
        // $("#barall").show();
        this.goToPageMenu('/infos', '&name=' + hero + '');
        /*  if(hero=="Cod fresh"){
            //alert("hier"+hero);
            this.goToPageMenu('/infos','&name=Cod fresh');
      
          }else if(hero=="Biscuit"){
            this.goToPageMenu('/infos','&name=Biscuit');
      
          }else if(hero=="Beef"){
      
            this.goToPageMenu('/infos','&name=Beef');
      
      
          }else if(hero=="Naan bread "){
          //  alert("hier"+hero);
            this.goToPageMenu('/infos','&name=Naan bread ');
      
          }else if(hero=="Muesli"){
           // alert("hier"+hero);
            this.goToPageMenu('/infos','&name=Muesli');
      
          }else if(hero=="Jaffa cake"){
            // alert("hier"+String(hero).trim());
             this.goToPageMenu('/infos','&name=Jaffa cake');
       
           }else if(hero=="Vanilla"){
            // alert("hier"+String(hero).trim());
             this.goToPageMenu('/infos','&name=Vanilla');
       
           }else if(hero=="Chapatis"){
            // alert("hier"+String(hero).trim());
             this.goToPageMenu('/infos','&name=Chapatis');
       
           }else if(hero=="Bananas"){
            // alert("hier"+String(hero).trim());
             this.goToPageMenu('/infos','&name=Bananas');
       
           }else if(hero=="Cornflakes with milk"){
            // alert("hier"+String(hero).trim());
             this.goToPageMenu('/infos','&name=Cornflakes with milk');
       
           }else if(hero=="Nuts"){
            // alert("hier"+String(hero).trim());
             this.goToPageMenu('/infos','&name=Nuts');
       
           }else if(hero=="Chicken"){
            // alert("hier"+String(hero).trim());
             this.goToPageMenu('/infos','&name=Chicken');
       
           }else if(hero=="Mashrooms"){
            // alert("hier"+String(hero).trim());
             this.goToPageMenu('/infos','&name=Mashrooms');
       
           }else if(hero=="Potatoes"){
            // alert("hier"+String(hero).trim());
             this.goToPageMenu('/infos','&name=Potatoes');
       
           }*/
    };
    CalendarComponent.prototype.ngOnInit = function () {
        var _this = this;
        jquery__WEBPACK_IMPORTED_MODULE_3__("#headercalendar").hide();
        jquery__WEBPACK_IMPORTED_MODULE_3__("#menu").hide();
        // $("#headcalendar").hide();
        jquery__WEBPACK_IMPORTED_MODULE_3__("#session").hide();
        jquery__WEBPACK_IMPORTED_MODULE_3__("#back").show();
        jquery__WEBPACK_IMPORTED_MODULE_3__("#date").hide();
        document.getElementById("notification").style.backgroundColor = "#696969";
        function delay(ms) {
            return new Promise(function (resolve) { return setTimeout(resolve, ms); });
        }
        //document.getElementById("day1").style.color = "black";
        var width = 1;
        // alert("datieee are hier!");
        // alert(this.username);
        // alert(this.email);
        // alert(this.password);
        // alert(this.height);
        // alert(this.weight);
        /*var elem = document.getElementById("myBar");
         
        var id = setInterval(frame, 10);
        function frame() {
          //alert("frame");
          if (width >= 100) {
          //  alert("called");
            clearInterval(id);
           
           // alert("alles");
           
            
          } else {
            //alert("Kabled width functionarie");
            width++;
            elem.style.width = width + '%';
          }
        }*/
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        var dayv = +dd;
        var daysv = dayv;
        var todaysv = mm + '/' + daysv + '/' + yyyy;
        var day1 = String(dayv);
        var day2 = String(dayv + 1);
        var day3 = String(dayv + 2);
        var day4 = String(dayv + 3);
        var day5 = String(dayv + 4);
        var day6 = String(dayv + 5);
        jquery__WEBPACK_IMPORTED_MODULE_3__('#alllist').show();
        for (var index = 0; index < 10000; index++) {
            this.numbers.push(index);
        }
        document.getElementById("day1").style.color = "#000000";
        document.getElementById("day1").textContent = "Mo";
        document.getElementById("day2").style.color = "#ff0000";
        document.getElementById("day2").textContent = "Tu";
        document.getElementById("day3").style.color = "#ff0000";
        document.getElementById("day3").textContent = "We";
        document.getElementById("day4").style.color = "#ff0000";
        document.getElementById("day4").textContent = "Th";
        document.getElementById("day5").style.color = "#ff0000";
        document.getElementById("day5").textContent = "Fr";
        //document.getElementById("day5").style.color = "#ff0000";
        //document.getElementById("day5").textContent=day1;
        /* this.http.get('http://127.0.0.1:3000/api/things',{responseType:"json"}).subscribe(
           response => {
               this.data = response;
               console.log("data :"+response);
               var sample=JSON.stringify(response);
          });*/
        // http://localhost:3000/api/things/getsensorvalues/niko
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        var day = +dd;
        var days = day;
        var todays = mm + '/' + days + '/' + yyyy;
        //  headcalendar
        document.getElementById("headcalendar").textContent = todays;
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var name;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(1000)];
                    case 1:
                        _a.sent();
                        jquery__WEBPACK_IMPORTED_MODULE_3__("#barall").hide();
                        jquery__WEBPACK_IMPORTED_MODULE_3__("#headercalendar").show();
                        jquery__WEBPACK_IMPORTED_MODULE_3__("#menu").show();
                        name = document.getElementById("session").innerHTML = sessionStorage.getItem("lastname");
                        this.http.get("http://localhost:3000/api/things/getcalendar/" + name + "/" + 'monday', { responseType: "json" }).subscribe(function (response) {
                            _this.data = response;
                            console.log("data :" + response);
                            var sample = JSON.stringify(response);
                            var js = JSON.parse(sample);
                            // alert(js[0].imageurl);
                            // alert(sample);
                        });
                        return [2 /*return*/];
                }
            });
        }); })();
    };
    CalendarComponent.prototype.toggleNavbar = function () {
        this.navbarOpen = !this.navbarOpen;
    };
    CalendarComponent.prototype.goToPageMenu = function (pagename, parameter) {
        this.router.navigate([pagename, parameter]);
    };
    CalendarComponent.prototype.goToPage = function (pagename, parameter, day) {
        var _this = this;
        var name = sessionStorage.getItem("lastname");
        var daymenu = day;
        if (day == "monday") {
            // alert("kablid");
            jquery__WEBPACK_IMPORTED_MODULE_3__("#day").hide();
            document.getElementById("day1").style.color = "#000000";
            document.getElementById("day2").style.color = "#ff0000";
            document.getElementById("day3").style.color = "#ff0000";
            document.getElementById("day4").style.color = "#ff0000";
            document.getElementById("day5").style.color = "#ff0000";
            // document.getElementById("day1").textContent.italics="Mxsxso"
            this.http.get("http://localhost:3000/api/things/getcalendar/" + name + "/" + daymenu, { responseType: "json" }).subscribe(function (response) {
                _this.data = response;
                console.log("data :" + response);
                var sample = JSON.stringify(response);
            });
        }
        else if (day == "tuesday") {
            document.getElementById("day1").style.color = "#ff0000";
            document.getElementById("day2").style.color = "#000000";
            document.getElementById("day3").style.color = "#ff0000";
            document.getElementById("day4").style.color = "#ff0000";
            document.getElementById("day5").style.color = "#ff0000";
            this.http.get("http://localhost:3000/api/things/getcalendar/" + name + "/tuesday", { responseType: "json" }).subscribe(function (response) {
                _this.data = response;
                console.log("data :" + response);
                var sample = JSON.stringify(response);
            });
        }
        else if (day == "wednesday") {
            document.getElementById("day1").style.color = "#ff0000";
            document.getElementById("day2").style.color = "#ff0000";
            document.getElementById("day3").style.color = "#000000";
            document.getElementById("day4").style.color = "#ff0000";
            document.getElementById("day5").style.color = "#ff0000";
            this.http.get("http://localhost:3000/api/things/getcalendar/" + name + "/wednesday", { responseType: "json" }).subscribe(function (response) {
                _this.data = response;
                console.log("data :" + response);
                var sample = JSON.stringify(response);
            });
        }
        else if (day == "thursday") {
            document.getElementById("day1").style.color = "#ff0000";
            document.getElementById("day2").style.color = "#ff0000";
            document.getElementById("day3").style.color = "#ff0000";
            document.getElementById("day4").style.color = "#000000";
            document.getElementById("day5").style.color = "#ff0000";
            this.http.get("http://localhost:3000/api/things/getcalendar/" + name + "/thursday", { responseType: "json" }).subscribe(function (response) {
                _this.data = response;
                console.log("data :" + response);
                var sample = JSON.stringify(response);
            });
        }
        else if (day == "friday") {
            document.getElementById("day1").style.color = "#ff0000";
            document.getElementById("day2").style.color = "#ff0000";
            document.getElementById("day3").style.color = "#ff0000";
            document.getElementById("day4").style.color = "#ff0000";
            document.getElementById("day5").style.color = "#000000";
            this.http.get("http://localhost:3000/api/things/getcalendar/" + name + "/friday", { responseType: "json" }).subscribe(function (response) {
                _this.data = response;
                console.log("data :" + response);
                var sample = JSON.stringify(response);
            });
        }
        //this.router.navigate([pagename, parameter]);
    };
    CalendarComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-calendar',
            template: __webpack_require__(/*! ./calendar.component.html */ "./src/app/calendar/calendar.component.html"),
            styles: [__webpack_require__(/*! ./calendar.component.css */ "./src/app/calendar/calendar.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _calendar_service__WEBPACK_IMPORTED_MODULE_5__["CalendarService"]])
    ], CalendarComponent);
    return CalendarComponent;
}());



/***/ }),

/***/ "./src/app/calendar/calendar.service.ts":
/*!**********************************************!*\
  !*** ./src/app/calendar/calendar.service.ts ***!
  \**********************************************/
/*! exports provided: CalendarService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CalendarService", function() { return CalendarService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");



var CalendarService = /** @class */ (function () {
    function CalendarService(httpClient) {
        this.httpClient = httpClient;
        this.apiURL = 'http://127.0.0.1:3000/api/things/setcalendarmenu';
        this.apiURLDELETE = 'http://127.0.0.1:3000/api/things/';
    }
    CalendarService.prototype.delete = function (username) {
        var data = {
            "usename": "Emily"
        };
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
            })
        };
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
            .set('username', "Emily");
        var options = {
            httpOptions: httpOptions,
            //   params,
            withCredentials: false
        };
        var name = "Emily";
        return this.httpClient.delete("http://127.0.0.1:3000/api/things/Emily");
    };
    CalendarService.prototype.setTempCalendar = function (username, name, type, energycontent, wayofcooking, nutrition, calories, kg, info, date, imageurl) {
        var data = {
            "name": "fr"
        };
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
            })
        };
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
            .set('username', String(username))
            .set('name', String(name))
            .set('type', String(type))
            .set('energycontent', String(energycontent))
            .set('wayofcooking', String(wayofcooking))
            .set('nutrition', String(nutrition))
            .set('calories', String(calories))
            .set('kg', String(kg))
            .set('info', String(info))
            .set('date', String(date))
            .set('imageurl', String(imageurl));
        var options = {
            httpOptions: httpOptions,
            params: params,
            withCredentials: false
        };
        return this.httpClient.post("" + this.apiURL, null, options);
    };
    CalendarService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], CalendarService);
    return CalendarService;
}());



/***/ }),

/***/ "./src/app/error/error.component.css":
/*!*******************************************!*\
  !*** ./src/app/error/error.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@import url('https://fonts.googleapis.com/css?family=Cabin+Sketch');\r\n\r\nhtml {\r\n\theight: 100%;\r\n}\r\n\r\nbody {\r\n\tmin-height: 100%;\r\n}\r\n\r\nbody {\r\n\tdisplay: flex;\r\n}\r\n\r\nh1 {\r\n\tfont-family: 'Cabin Sketch', cursive;\r\n\tfont-size: 3em;\r\n\ttext-align: center;\r\n\topacity: .8;\r\n\torder: 1;\r\n}\r\n\r\nh1 small {\r\n\tdisplay: block;\r\n}\r\n\r\n.site {\r\n\tdisplay: flex;\r\n align-items: center;\r\n\tflex-direction: column;\r\n\tmargin: 0 auto;\r\n\tjustify-content: center;\r\n}\r\n\r\n.sketch {\r\n\tposition: relative;\r\n\theight: 400px;\r\n\tmin-width: 400px;\r\n\tmargin: 0;\r\n\toverflow: visible;\r\n\torder: 2;\r\n\t\r\n}\r\n\r\n.bee-sketch {\r\n\theight: 100%;\r\n\twidth: 100%;\r\n\tposition: absolute;\r\n\ttop: 0;\r\n\tleft: 0;\r\n}\r\n\r\n.red {\r\n\tbackground: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/red-1.png) no-repeat center center;\r\n\topacity: 1;\r\n\t-webkit-animation: red 3s linear infinite, opacityRed 5s linear alternate infinite;\r\n\t        animation: red 3s linear infinite, opacityRed 5s linear alternate infinite;\r\n}\r\n\r\n.blue {\r\n\tbackground: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/blue-1.png) no-repeat center center;\r\n\topacity: 0;\r\n\t-webkit-animation: blue 3s linear infinite, opacityBlue 5s linear alternate infinite;\r\n\t        animation: blue 3s linear infinite, opacityBlue 5s linear alternate infinite;\r\n}\r\n\r\n@media only screen and (min-width: 780px) {\r\n  .site {\r\n\t\tflex-direction: row;\r\n\t\tpadding: 1em 3em 1em 0em;\r\n\t}\r\n\t\r\n\th1 {\r\n\t\ttext-align: right;\r\n\t\torder: 2;\r\n\t\tpadding-bottom: 2em;\r\n\t\tpadding-left: 2em;\r\n\r\n\t}\r\n\t\r\n\t.sketch {\r\n\t\torder: 1;\r\n\t}\r\n}\r\n\r\n@-webkit-keyframes blue {\r\n\t0% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/blue-1.png) \r\n  }\r\n\t9.09% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/blue-2.png) \r\n  }\r\n\t27.27% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/blue-3.png) \r\n  }\r\n\t36.36% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/blue-4.png) \r\n  }\r\n\t45.45% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/blue-5.png) \r\n  }\r\n\t54.54% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/blue-6.png) \r\n  }\r\n\t63.63% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/blue-7.png) \r\n  }\r\n\t72.72% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/blue-8.png) \r\n  }\r\n\t81.81% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/blue-9.png) \r\n  }\r\n\t100% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/blue-1.png) \r\n  }\r\n}\r\n\r\n@keyframes blue {\r\n\t0% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/blue-1.png) \r\n  }\r\n\t9.09% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/blue-2.png) \r\n  }\r\n\t27.27% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/blue-3.png) \r\n  }\r\n\t36.36% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/blue-4.png) \r\n  }\r\n\t45.45% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/blue-5.png) \r\n  }\r\n\t54.54% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/blue-6.png) \r\n  }\r\n\t63.63% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/blue-7.png) \r\n  }\r\n\t72.72% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/blue-8.png) \r\n  }\r\n\t81.81% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/blue-9.png) \r\n  }\r\n\t100% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/blue-1.png) \r\n  }\r\n}\r\n\r\n@-webkit-keyframes red {\r\n\t0% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/red-1.png) \r\n  }\r\n\t9.09% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/red-2.png) \r\n  }\r\n\t27.27% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/red-3.png) \r\n  }\r\n\t36.36% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/red-4.png) \r\n  }\r\n\t45.45% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/red-5.png) \r\n  }\r\n\t54.54% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/red-6.png) \r\n  }\r\n\t63.63% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/red-7.png) \r\n  }\r\n\t72.72% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/red-8.png) \r\n  }\r\n\t81.81% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/red-9.png) \r\n  }\r\n\t100% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/red-1.png) \r\n  }\r\n}\r\n\r\n@keyframes red {\r\n\t0% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/red-1.png) \r\n  }\r\n\t9.09% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/red-2.png) \r\n  }\r\n\t27.27% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/red-3.png) \r\n  }\r\n\t36.36% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/red-4.png) \r\n  }\r\n\t45.45% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/red-5.png) \r\n  }\r\n\t54.54% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/red-6.png) \r\n  }\r\n\t63.63% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/red-7.png) \r\n  }\r\n\t72.72% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/red-8.png) \r\n  }\r\n\t81.81% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/red-9.png) \r\n  }\r\n\t100% {\r\n\t\tbackground-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/198554/red-1.png) \r\n  }\r\n}\r\n\r\n@-webkit-keyframes opacityBlue {\r\n\tfrom {\r\n\t\topacity: 0\r\n\t}\r\n\t25% {\r\n\t\topacity: 0\r\n\t}\r\n\t75% {\r\n\t\topacity: 1\r\n\t}\r\n\tto {\r\n\t\topacity: 1\r\n\t}\r\n}\r\n\r\n@keyframes opacityBlue {\r\n\tfrom {\r\n\t\topacity: 0\r\n\t}\r\n\t25% {\r\n\t\topacity: 0\r\n\t}\r\n\t75% {\r\n\t\topacity: 1\r\n\t}\r\n\tto {\r\n\t\topacity: 1\r\n\t}\r\n}\r\n\r\n@-webkit-keyframes opacityRed {\r\n\tfrom {\r\n\t\topacity: 1\r\n\t}\r\n\t25% {\r\n\t\topacity: 1\r\n\t}\r\n\t75% {\r\n\t\topacity: .3\r\n\t}\r\n\tto {\r\n\t\topacity: .3\r\n\t}\r\n}\r\n\r\n@keyframes opacityRed {\r\n\tfrom {\r\n\t\topacity: 1\r\n\t}\r\n\t25% {\r\n\t\topacity: 1\r\n\t}\r\n\t75% {\r\n\t\topacity: .3\r\n\t}\r\n\tto {\r\n\t\topacity: .3\r\n\t}\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZXJyb3IvZXJyb3IuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxtRUFBbUU7O0FBRW5FO0NBQ0MsWUFBWTtBQUNiOztBQUVBO0NBQ0MsZ0JBQWdCO0FBQ2pCOztBQUVBO0NBQ0MsYUFBYTtBQUNkOztBQUVBO0NBQ0Msb0NBQW9DO0NBQ3BDLGNBQWM7Q0FDZCxrQkFBa0I7Q0FDbEIsV0FBVztDQUNYLFFBQVE7QUFDVDs7QUFFQTtDQUNDLGNBQWM7QUFDZjs7QUFFQTtDQUlDLGFBQWE7Q0FJWixtQkFBbUI7Q0FDcEIsc0JBQXNCO0NBQ3RCLGNBQWM7Q0FJZCx1QkFBdUI7QUFDeEI7O0FBR0E7Q0FDQyxrQkFBa0I7Q0FDbEIsYUFBYTtDQUNiLGdCQUFnQjtDQUNoQixTQUFTO0NBQ1QsaUJBQWlCO0NBQ2pCLFFBQVE7O0FBRVQ7O0FBRUE7Q0FDQyxZQUFZO0NBQ1osV0FBVztDQUNYLGtCQUFrQjtDQUNsQixNQUFNO0NBQ04sT0FBTztBQUNSOztBQUVBO0NBQ0Msc0dBQXNHO0NBQ3RHLFVBQVU7Q0FDVixrRkFBMEU7U0FBMUUsMEVBQTBFO0FBQzNFOztBQUVBO0NBQ0MsdUdBQXVHO0NBQ3ZHLFVBQVU7Q0FDVixvRkFBNEU7U0FBNUUsNEVBQTRFO0FBQzdFOztBQUdBO0VBQ0U7RUFDQSxtQkFBbUI7RUFDbkIsd0JBQXdCO0NBQ3pCOztDQUVBO0VBQ0MsaUJBQWlCO0VBQ2pCLFFBQVE7RUFDUixtQkFBbUI7RUFDbkIsaUJBQWlCOztDQUVsQjs7Q0FFQTtFQUNDLFFBQVE7Q0FDVDtBQUNEOztBQUdBO0NBQ0M7RUFDQztFQUNBO0NBQ0Q7RUFDQztFQUNBO0NBQ0Q7RUFDQztFQUNBO0NBQ0Q7RUFDQztFQUNBO0NBQ0Q7RUFDQztFQUNBO0NBQ0Q7RUFDQztFQUNBO0NBQ0Q7RUFDQztFQUNBO0NBQ0Q7RUFDQztFQUNBO0NBQ0Q7RUFDQztFQUNBO0NBQ0Q7RUFDQztFQUNBO0FBQ0Y7O0FBL0JBO0NBQ0M7RUFDQztFQUNBO0NBQ0Q7RUFDQztFQUNBO0NBQ0Q7RUFDQztFQUNBO0NBQ0Q7RUFDQztFQUNBO0NBQ0Q7RUFDQztFQUNBO0NBQ0Q7RUFDQztFQUNBO0NBQ0Q7RUFDQztFQUNBO0NBQ0Q7RUFDQztFQUNBO0NBQ0Q7RUFDQztFQUNBO0NBQ0Q7RUFDQztFQUNBO0FBQ0Y7O0FBRUE7Q0FDQztFQUNDO0VBQ0E7Q0FDRDtFQUNDO0VBQ0E7Q0FDRDtFQUNDO0VBQ0E7Q0FDRDtFQUNDO0VBQ0E7Q0FDRDtFQUNDO0VBQ0E7Q0FDRDtFQUNDO0VBQ0E7Q0FDRDtFQUNDO0VBQ0E7Q0FDRDtFQUNDO0VBQ0E7Q0FDRDtFQUNDO0VBQ0E7Q0FDRDtFQUNDO0VBQ0E7QUFDRjs7QUEvQkE7Q0FDQztFQUNDO0VBQ0E7Q0FDRDtFQUNDO0VBQ0E7Q0FDRDtFQUNDO0VBQ0E7Q0FDRDtFQUNDO0VBQ0E7Q0FDRDtFQUNDO0VBQ0E7Q0FDRDtFQUNDO0VBQ0E7Q0FDRDtFQUNDO0VBQ0E7Q0FDRDtFQUNDO0VBQ0E7Q0FDRDtFQUNDO0VBQ0E7Q0FDRDtFQUNDO0VBQ0E7QUFDRjs7QUFFQTtDQUNDO0VBQ0M7Q0FDRDtDQUNBO0VBQ0M7Q0FDRDtDQUNBO0VBQ0M7Q0FDRDtDQUNBO0VBQ0M7Q0FDRDtBQUNEOztBQWJBO0NBQ0M7RUFDQztDQUNEO0NBQ0E7RUFDQztDQUNEO0NBQ0E7RUFDQztDQUNEO0NBQ0E7RUFDQztDQUNEO0FBQ0Q7O0FBRUE7Q0FDQztFQUNDO0NBQ0Q7Q0FDQTtFQUNDO0NBQ0Q7Q0FDQTtFQUNDO0NBQ0Q7Q0FDQTtFQUNDO0NBQ0Q7QUFDRDs7QUFiQTtDQUNDO0VBQ0M7Q0FDRDtDQUNBO0VBQ0M7Q0FDRDtDQUNBO0VBQ0M7Q0FDRDtDQUNBO0VBQ0M7Q0FDRDtBQUNEIiwiZmlsZSI6InNyYy9hcHAvZXJyb3IvZXJyb3IuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIkBpbXBvcnQgdXJsKCdodHRwczovL2ZvbnRzLmdvb2dsZWFwaXMuY29tL2Nzcz9mYW1pbHk9Q2FiaW4rU2tldGNoJyk7XHJcblxyXG5odG1sIHtcclxuXHRoZWlnaHQ6IDEwMCU7XHJcbn1cclxuXHJcbmJvZHkge1xyXG5cdG1pbi1oZWlnaHQ6IDEwMCU7XHJcbn1cclxuXHJcbmJvZHkge1xyXG5cdGRpc3BsYXk6IGZsZXg7XHJcbn1cclxuXHJcbmgxIHtcclxuXHRmb250LWZhbWlseTogJ0NhYmluIFNrZXRjaCcsIGN1cnNpdmU7XHJcblx0Zm9udC1zaXplOiAzZW07XHJcblx0dGV4dC1hbGlnbjogY2VudGVyO1xyXG5cdG9wYWNpdHk6IC44O1xyXG5cdG9yZGVyOiAxO1xyXG59XHJcblxyXG5oMSBzbWFsbCB7XHJcblx0ZGlzcGxheTogYmxvY2s7XHJcbn1cclxuXHJcbi5zaXRlIHtcclxuXHRkaXNwbGF5OiAtd2Via2l0LWJveDtcclxuXHRkaXNwbGF5OiAtd2Via2l0LWZsZXg7XHJcblx0ZGlzcGxheTogLW1zLWZsZXhib3g7XHJcblx0ZGlzcGxheTogZmxleDtcclxuXHQtd2Via2l0LWJveC1hbGlnbjogY2VudGVyO1xyXG5cdC13ZWJraXQtYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuXHQtbXMtZmxleC1hbGlnbjogY2VudGVyO1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcblx0ZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuXHRtYXJnaW46IDAgYXV0bztcclxuXHQtd2Via2l0LWJveC1wYWNrOiBjZW50ZXI7XHJcblx0LXdlYmtpdC1qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuXHQtbXMtZmxleC1wYWNrOiBjZW50ZXI7XHJcblx0anVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbn1cclxuXHJcblxyXG4uc2tldGNoIHtcclxuXHRwb3NpdGlvbjogcmVsYXRpdmU7XHJcblx0aGVpZ2h0OiA0MDBweDtcclxuXHRtaW4td2lkdGg6IDQwMHB4O1xyXG5cdG1hcmdpbjogMDtcclxuXHRvdmVyZmxvdzogdmlzaWJsZTtcclxuXHRvcmRlcjogMjtcclxuXHRcclxufVxyXG5cclxuLmJlZS1za2V0Y2gge1xyXG5cdGhlaWdodDogMTAwJTtcclxuXHR3aWR0aDogMTAwJTtcclxuXHRwb3NpdGlvbjogYWJzb2x1dGU7XHJcblx0dG9wOiAwO1xyXG5cdGxlZnQ6IDA7XHJcbn1cclxuXHJcbi5yZWQge1xyXG5cdGJhY2tncm91bmQ6IHVybChodHRwczovL3MzLXVzLXdlc3QtMi5hbWF6b25hd3MuY29tL3MuY2Rwbi5pby8xOTg1NTQvcmVkLTEucG5nKSBuby1yZXBlYXQgY2VudGVyIGNlbnRlcjtcclxuXHRvcGFjaXR5OiAxO1xyXG5cdGFuaW1hdGlvbjogcmVkIDNzIGxpbmVhciBpbmZpbml0ZSwgb3BhY2l0eVJlZCA1cyBsaW5lYXIgYWx0ZXJuYXRlIGluZmluaXRlO1xyXG59XHJcblxyXG4uYmx1ZSB7XHJcblx0YmFja2dyb3VuZDogdXJsKGh0dHBzOi8vczMtdXMtd2VzdC0yLmFtYXpvbmF3cy5jb20vcy5jZHBuLmlvLzE5ODU1NC9ibHVlLTEucG5nKSBuby1yZXBlYXQgY2VudGVyIGNlbnRlcjtcclxuXHRvcGFjaXR5OiAwO1xyXG5cdGFuaW1hdGlvbjogYmx1ZSAzcyBsaW5lYXIgaW5maW5pdGUsIG9wYWNpdHlCbHVlIDVzIGxpbmVhciBhbHRlcm5hdGUgaW5maW5pdGU7XHJcbn1cclxuXHJcblxyXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGg6IDc4MHB4KSB7XHJcbiAgLnNpdGUge1xyXG5cdFx0ZmxleC1kaXJlY3Rpb246IHJvdztcclxuXHRcdHBhZGRpbmc6IDFlbSAzZW0gMWVtIDBlbTtcclxuXHR9XHJcblx0XHJcblx0aDEge1xyXG5cdFx0dGV4dC1hbGlnbjogcmlnaHQ7XHJcblx0XHRvcmRlcjogMjtcclxuXHRcdHBhZGRpbmctYm90dG9tOiAyZW07XHJcblx0XHRwYWRkaW5nLWxlZnQ6IDJlbTtcclxuXHJcblx0fVxyXG5cdFxyXG5cdC5za2V0Y2gge1xyXG5cdFx0b3JkZXI6IDE7XHJcblx0fVxyXG59XHJcblxyXG5cclxuQGtleWZyYW1lcyBibHVlIHtcclxuXHQwJSB7XHJcblx0XHRiYWNrZ3JvdW5kLWltYWdlOiB1cmwoaHR0cHM6Ly9zMy11cy13ZXN0LTIuYW1hem9uYXdzLmNvbS9zLmNkcG4uaW8vMTk4NTU0L2JsdWUtMS5wbmcpIFxyXG4gIH1cclxuXHQ5LjA5JSB7XHJcblx0XHRiYWNrZ3JvdW5kLWltYWdlOiB1cmwoaHR0cHM6Ly9zMy11cy13ZXN0LTIuYW1hem9uYXdzLmNvbS9zLmNkcG4uaW8vMTk4NTU0L2JsdWUtMi5wbmcpIFxyXG4gIH1cclxuXHQyNy4yNyUge1xyXG5cdFx0YmFja2dyb3VuZC1pbWFnZTogdXJsKGh0dHBzOi8vczMtdXMtd2VzdC0yLmFtYXpvbmF3cy5jb20vcy5jZHBuLmlvLzE5ODU1NC9ibHVlLTMucG5nKSBcclxuICB9XHJcblx0MzYuMzYlIHtcclxuXHRcdGJhY2tncm91bmQtaW1hZ2U6IHVybChodHRwczovL3MzLXVzLXdlc3QtMi5hbWF6b25hd3MuY29tL3MuY2Rwbi5pby8xOTg1NTQvYmx1ZS00LnBuZykgXHJcbiAgfVxyXG5cdDQ1LjQ1JSB7XHJcblx0XHRiYWNrZ3JvdW5kLWltYWdlOiB1cmwoaHR0cHM6Ly9zMy11cy13ZXN0LTIuYW1hem9uYXdzLmNvbS9zLmNkcG4uaW8vMTk4NTU0L2JsdWUtNS5wbmcpIFxyXG4gIH1cclxuXHQ1NC41NCUge1xyXG5cdFx0YmFja2dyb3VuZC1pbWFnZTogdXJsKGh0dHBzOi8vczMtdXMtd2VzdC0yLmFtYXpvbmF3cy5jb20vcy5jZHBuLmlvLzE5ODU1NC9ibHVlLTYucG5nKSBcclxuICB9XHJcblx0NjMuNjMlIHtcclxuXHRcdGJhY2tncm91bmQtaW1hZ2U6IHVybChodHRwczovL3MzLXVzLXdlc3QtMi5hbWF6b25hd3MuY29tL3MuY2Rwbi5pby8xOTg1NTQvYmx1ZS03LnBuZykgXHJcbiAgfVxyXG5cdDcyLjcyJSB7XHJcblx0XHRiYWNrZ3JvdW5kLWltYWdlOiB1cmwoaHR0cHM6Ly9zMy11cy13ZXN0LTIuYW1hem9uYXdzLmNvbS9zLmNkcG4uaW8vMTk4NTU0L2JsdWUtOC5wbmcpIFxyXG4gIH1cclxuXHQ4MS44MSUge1xyXG5cdFx0YmFja2dyb3VuZC1pbWFnZTogdXJsKGh0dHBzOi8vczMtdXMtd2VzdC0yLmFtYXpvbmF3cy5jb20vcy5jZHBuLmlvLzE5ODU1NC9ibHVlLTkucG5nKSBcclxuICB9XHJcblx0MTAwJSB7XHJcblx0XHRiYWNrZ3JvdW5kLWltYWdlOiB1cmwoaHR0cHM6Ly9zMy11cy13ZXN0LTIuYW1hem9uYXdzLmNvbS9zLmNkcG4uaW8vMTk4NTU0L2JsdWUtMS5wbmcpIFxyXG4gIH1cclxufVxyXG5cclxuQGtleWZyYW1lcyByZWQge1xyXG5cdDAlIHtcclxuXHRcdGJhY2tncm91bmQtaW1hZ2U6IHVybChodHRwczovL3MzLXVzLXdlc3QtMi5hbWF6b25hd3MuY29tL3MuY2Rwbi5pby8xOTg1NTQvcmVkLTEucG5nKSBcclxuICB9XHJcblx0OS4wOSUge1xyXG5cdFx0YmFja2dyb3VuZC1pbWFnZTogdXJsKGh0dHBzOi8vczMtdXMtd2VzdC0yLmFtYXpvbmF3cy5jb20vcy5jZHBuLmlvLzE5ODU1NC9yZWQtMi5wbmcpIFxyXG4gIH1cclxuXHQyNy4yNyUge1xyXG5cdFx0YmFja2dyb3VuZC1pbWFnZTogdXJsKGh0dHBzOi8vczMtdXMtd2VzdC0yLmFtYXpvbmF3cy5jb20vcy5jZHBuLmlvLzE5ODU1NC9yZWQtMy5wbmcpIFxyXG4gIH1cclxuXHQzNi4zNiUge1xyXG5cdFx0YmFja2dyb3VuZC1pbWFnZTogdXJsKGh0dHBzOi8vczMtdXMtd2VzdC0yLmFtYXpvbmF3cy5jb20vcy5jZHBuLmlvLzE5ODU1NC9yZWQtNC5wbmcpIFxyXG4gIH1cclxuXHQ0NS40NSUge1xyXG5cdFx0YmFja2dyb3VuZC1pbWFnZTogdXJsKGh0dHBzOi8vczMtdXMtd2VzdC0yLmFtYXpvbmF3cy5jb20vcy5jZHBuLmlvLzE5ODU1NC9yZWQtNS5wbmcpIFxyXG4gIH1cclxuXHQ1NC41NCUge1xyXG5cdFx0YmFja2dyb3VuZC1pbWFnZTogdXJsKGh0dHBzOi8vczMtdXMtd2VzdC0yLmFtYXpvbmF3cy5jb20vcy5jZHBuLmlvLzE5ODU1NC9yZWQtNi5wbmcpIFxyXG4gIH1cclxuXHQ2My42MyUge1xyXG5cdFx0YmFja2dyb3VuZC1pbWFnZTogdXJsKGh0dHBzOi8vczMtdXMtd2VzdC0yLmFtYXpvbmF3cy5jb20vcy5jZHBuLmlvLzE5ODU1NC9yZWQtNy5wbmcpIFxyXG4gIH1cclxuXHQ3Mi43MiUge1xyXG5cdFx0YmFja2dyb3VuZC1pbWFnZTogdXJsKGh0dHBzOi8vczMtdXMtd2VzdC0yLmFtYXpvbmF3cy5jb20vcy5jZHBuLmlvLzE5ODU1NC9yZWQtOC5wbmcpIFxyXG4gIH1cclxuXHQ4MS44MSUge1xyXG5cdFx0YmFja2dyb3VuZC1pbWFnZTogdXJsKGh0dHBzOi8vczMtdXMtd2VzdC0yLmFtYXpvbmF3cy5jb20vcy5jZHBuLmlvLzE5ODU1NC9yZWQtOS5wbmcpIFxyXG4gIH1cclxuXHQxMDAlIHtcclxuXHRcdGJhY2tncm91bmQtaW1hZ2U6IHVybChodHRwczovL3MzLXVzLXdlc3QtMi5hbWF6b25hd3MuY29tL3MuY2Rwbi5pby8xOTg1NTQvcmVkLTEucG5nKSBcclxuICB9XHJcbn1cclxuXHJcbkBrZXlmcmFtZXMgb3BhY2l0eUJsdWUge1xyXG5cdGZyb20ge1xyXG5cdFx0b3BhY2l0eTogMFxyXG5cdH1cclxuXHQyNSUge1xyXG5cdFx0b3BhY2l0eTogMFxyXG5cdH1cclxuXHQ3NSUge1xyXG5cdFx0b3BhY2l0eTogMVxyXG5cdH1cclxuXHR0byB7XHJcblx0XHRvcGFjaXR5OiAxXHJcblx0fVxyXG59XHJcblxyXG5Aa2V5ZnJhbWVzIG9wYWNpdHlSZWQge1xyXG5cdGZyb20ge1xyXG5cdFx0b3BhY2l0eTogMVxyXG5cdH1cclxuXHQyNSUge1xyXG5cdFx0b3BhY2l0eTogMVxyXG5cdH1cclxuXHQ3NSUge1xyXG5cdFx0b3BhY2l0eTogLjNcclxuXHR9XHJcblx0dG8ge1xyXG5cdFx0b3BhY2l0eTogLjNcclxuXHR9XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/error/error.component.html":
/*!********************************************!*\
  !*** ./src/app/error/error.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div class=\"site\">\n\t<div class=\"sketch\">\n\t\t<div class=\"bee-sketch red\"></div>\n\t\t<div class=\"bee-sketch blue\"></div>\n\t</div>\n\n<h1>404:\n\t<small>Users Not Found</small></h1>\n\n\t<h1>\n\t\t<small routerLink=\"/signup\" id=\"previous\"> <-  </small></h1>\n\t\n</div>"

/***/ }),

/***/ "./src/app/error/error.component.ts":
/*!******************************************!*\
  !*** ./src/app/error/error.component.ts ***!
  \******************************************/
/*! exports provided: ErrorComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ErrorComponent", function() { return ErrorComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var ErrorComponent = /** @class */ (function () {
    function ErrorComponent(router) {
        this.router = router;
    }
    ErrorComponent.prototype.ngOnInit = function () {
        //$("#headercalendar").hide();
        jquery__WEBPACK_IMPORTED_MODULE_2__("#header").hide();
        // $("#headcalendar").hide();
    };
    ErrorComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-error',
            template: __webpack_require__(/*! ./error.component.html */ "./src/app/error/error.component.html"),
            styles: [__webpack_require__(/*! ./error.component.css */ "./src/app/error/error.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], ErrorComponent);
    return ErrorComponent;
}());



/***/ }),

/***/ "./src/app/header/diagrams/diagrams.component.css":
/*!********************************************************!*\
  !*** ./src/app/header/diagrams/diagrams.component.css ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hlYWRlci9kaWFncmFtcy9kaWFncmFtcy5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/header/diagrams/diagrams.component.html":
/*!*********************************************************!*\
  !*** ./src/app/header/diagrams/diagrams.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div id=\"maincontenthome\">\n  \n  <div id=\"jquery-script-menu\">\n    <div class=\"jquery-script-center\">\n    \n    <div class=\"jquery-script-ads\"><script type=\"text/javascript\">\n    /* jQuery_demo */\n    google_ad_slot = \"2780937993\";\n    google_ad_width = 728;\n    google_ad_height = 90;\n    //-->\n    </script>\n    <script type=\"text/javascript\"\n    src=\"http://pagead2.googlesyndication.com/pagead/show_ads.js\">\n    </script></div>\n    <div class=\"jquery-script-clear\"></div>\n    </div>\n    </div>\n            <div class=\"container\">\n            <h1>Current Calories</h1>\n                <div class=\"row\">\n                    <div class=\"col-sm-4 col-sm-offset-4\">\n                        <div class=\"my-progress-bar\"></div>\n                    </div>\n                </div>\n            </div>\n    \n           <script src=\"https://code.jquery.com/jquery-3.2.1.min.js\"></script>\n           \n            <script type=\"text/javascript\">\n    \n      var _gaq = _gaq || [];\n      _gaq.push(['_setAccount', 'UA-36251023-1']);\n      _gaq.push(['_setDomainName', 'jqueryscript.net']);\n      _gaq.push(['_trackPageview']);\n    \n      (function() {\n        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;\n        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';\n        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);\n      })();\n    \n    </script>\n  \n</div>"

/***/ }),

/***/ "./src/app/header/diagrams/diagrams.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/header/diagrams/diagrams.component.ts ***!
  \*******************************************************/
/*! exports provided: DiagramsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DiagramsComponent", function() { return DiagramsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_2__);



var DiagramsComponent = /** @class */ (function () {
    function DiagramsComponent() {
    }
    DiagramsComponent.prototype.ngOnInit = function () {
        /*  $(document).ready(function(){
            $("button").click(function(){
              $("p").hide();
      
      
            });
          });*/
        //
        var global_settings = { percentage: "a", text: "a", starting_position: 1,
            ending_position: 1, width: 1, height: 1, percent: 0, color: "a", line_width: 1,
            counter_clockwise: true
        };
        (function ($) {
            var methods = {
                init: function (options) {
                    // This is the easiest way to have default options.
                    var settings = $.extend({
                        // These are the defaults.
                        color: "#000000",
                        height: "300px",
                        width: "300px",
                        line_width: 8,
                        starting_position: 25,
                        percent: 100,
                        counter_clockwise: false,
                        percentage: true,
                        text: ''
                    }, options);
                    global_settings = settings;
                    // Create percentage
                    var percentage = $("<div class='progress-percentage'></div>");
                    if (!global_settings.percentage) {
                        percentage.text(global_settings.percentage);
                    }
                    $(this).append(percentage);
                    // Create text
                    var text = $("<div class='progress-text'></div>");
                    // Custom text
                    if (global_settings.text != "percent") {
                        text.text(global_settings.text);
                    }
                    $(this).append(text);
                    // Correct any invalid values
                    if (global_settings.starting_position != 100) {
                        global_settings.starting_position = global_settings.starting_position % 100;
                    }
                    if (global_settings.ending_position != 100) {
                        global_settings.ending_position = global_settings.ending_position % 100;
                    }
                    // No 'px' or '%', add 'px'
                    appendUnit(global_settings.width);
                    appendUnit(global_settings.height);
                    // Apply global_settings
                    $(this).css({
                        "height": global_settings.height,
                        "width": global_settings.width
                    });
                    $(this).addClass("circular-progress-bar");
                    // Remove old canvas
                    $(this).find("canvas").remove();
                    // Put canvas inside this
                    $(this).append(createCanvas($(this)));
                    // Return allows for chaining
                    return this;
                },
                percent: function (value) {
                    // Change percent
                    global_settings.percent = value;
                    // Apply global_settings
                    $(this).css({
                        "height": global_settings.height,
                        "width": global_settings.width
                    });
                    // Remove old canvas
                    $(this).children("canvas").remove();
                    // Put canvas inside this
                    $(this).append(createCanvas($(this)));
                    // Return allows for chaining
                    return this;
                },
                animate: function (value, time) {
                    // Apply global_settings
                    // Number of intervals, 10ms interval
                    var num_of_steps = time / 10;
                    // Amount of change each step
                    var percent_change = (value - global_settings.percent) / num_of_steps;
                    // Variable conflict, rename this
                    var scope = $(this);
                    var theInterval = setInterval(function () {
                        if (global_settings.percent < value) {
                            // Remove old canvas
                            scope.children("canvas").remove();
                            // Increment percent
                            global_settings.percent += percent_change;
                            // Put canvas inside this
                            scope.append(createCanvas(scope));
                        }
                        else {
                            clearInterval(theInterval);
                        }
                    }, 10);
                    // Return allows for chaining
                    return this;
                }
            };
            $.fn.circularProgress = function (methodOrOptions) {
                if (methods[methodOrOptions]) {
                    // Method found
                    return methods[methodOrOptions].apply(this, Array.prototype.slice.call(arguments, 1));
                }
                else if (typeof methodOrOptions === 'object' || !methodOrOptions) {
                    // Default to "init", object passed in or nothing passed in
                    return methods.init.apply(this, arguments);
                }
                else {
                    $.error('Method ' + methodOrOptions + ' does not exist.');
                }
            };
            /* =========================================================================
                PRIVATE FUNCTIONS
            ========================================================================= */
            // return string without 'px' or '%'
            function removeUnit(apples) {
                if (apples.indexOf("px")) {
                    return apples.substring(0, apples.length - 2);
                }
            }
            ;
            // return string with 'px'
            function appendUnit(apples) {
                if (apples.toString().indexOf("px") < -1 && apples.toString().indexOf("%") < -1) {
                    return apples += "px";
                }
            }
            ;
            // calculate starting position on canvas
            function calcPos(apples, percent) {
                if (percent < 0) {
                    // Calculate starting position
                    var starting_degree = (parseInt(apples) / 100) * 360;
                    var starting_radian = starting_degree * (Math.PI / 180);
                    return starting_radian - (Math.PI / 2);
                }
                else {
                    // Calculate ending position
                    var ending_degree = ((parseInt(apples) + parseInt(percent)) / 100) * 360;
                    var ending_radian = ending_degree * (Math.PI / 180);
                    return ending_radian - (Math.PI / 2);
                }
            }
            ;
            // Put percentage or custom text inside progress circle
            function insertText(scope) {
                $(".progress-percentage").text(Math.round(global_settings.percent) + "%");
            }
            // create canvas
            function createCanvas(scope) {
                // Remove 'px' or '%'
                var canvas_height = removeUnit(global_settings.height.toString());
                var canvas_width = removeUnit(global_settings.width.toString());
                // Create canvas
                var canvas = document.createElement("canvas");
                canvas.height = canvas_height;
                canvas.width = canvas_width;
                // Create drawable canvas and apply properties
                var ctx = canvas.getContext("2d");
                ctx.strokeStyle = global_settings.color;
                ctx.lineWidth = global_settings.line_width;
                // Draw arc
                ctx.beginPath();
                // Calculate starting and ending positions
                var starting_radian = calcPos(global_settings.starting_position, -1);
                var ending_radian = calcPos(global_settings.starting_position, global_settings.percent);
                // Calculate radius and x,y coordinates
                var radius = 0;
                var xcoord = canvas_width / 2;
                var ycoord = canvas_height / 2;
                // Height or width greater
                if (canvas_height >= canvas_width) {
                    radius = canvas_width * 0.9 / 2 - (global_settings.line_width * 2);
                }
                else {
                    radius = canvas_height * 0.9 / 2 - (global_settings.line_width * 2);
                }
                /*
                    x coordinate
                    y coordinate
                    radius of circle
                    starting angle in radians
                    ending angle in radians
                    clockwise (false, default) or counter-clockwise (true)
                */
                ctx.arc(xcoord, ycoord, radius, starting_radian, ending_radian, global_settings.counter_clockwise);
                ctx.stroke();
                // Add text
                if (global_settings.percentage) {
                    insertText(scope);
                }
                return canvas;
            }
            ;
        }(jquery__WEBPACK_IMPORTED_MODULE_2__));
        var a = 60;
        jquery__WEBPACK_IMPORTED_MODULE_2__(".my-progress-bar").circularProgress({
            line_width: 9,
            color: "#1E90FF",
            starting_position: 0,
            percent: 0,
            percentage: true,
            text: "1600/2000 Kcal"
        }).circularProgress('animate', a, 5000);
        //
    };
    DiagramsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-diagrams',
            template: __webpack_require__(/*! ./diagrams.component.html */ "./src/app/header/diagrams/diagrams.component.html"),
            styles: [__webpack_require__(/*! ./diagrams.component.css */ "./src/app/header/diagrams/diagrams.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], DiagramsComponent);
    return DiagramsComponent;
}());



/***/ }),

/***/ "./src/app/header/header.component.css":
/*!*********************************************!*\
  !*** ./src/app/header/header.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/*\r\n#myProgress {\r\n  width: 100%;\r\n  background-color: #ddd;\r\n}\r\n\r\n#myBar {\r\n  width: 1%;\r\n  height: 30px;\r\n  background-color: #4CAF50;\r\n}*/\r\n#button1 {\r\n    background-color: #b2b2b2\t; \r\n    border: none;\r\n    color: white;\r\n    padding-bottom: 25px 22px;\r\n    text-align: center;\r\n    text-decoration: none;\r\n    display: inline-block;\r\n    font-size: 36px;\r\n    margin: 4px 18px;\r\n    cursor: pointer;\r\n  }\r\n#button2 {\r\n   background-color: #b2b2b2\t; \r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n#button3 {\r\n   background-color: #b2b2b2\t; \r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n#button4 {\r\n   background-color: #b2b2b2\t;\r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n#all{\r\n     text-align: center;\r\n     position: fixed;\r\n     left: 0;\r\n     bottom: 0;\r\n     width: 100%;\r\n     background-color: \t\t#b2b2b2;\r\n     color: white;\r\n     text-align: center;\r\n  }\r\ndiv.home {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\ndiv.home input {\r\n    background: url('home.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\ndiv.analytic {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\ndiv.analytic input {\r\n    background: url('analytic.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\ndiv.notification {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\ndiv.notification input {\r\n    background: url('outline-today-24px.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\ndiv.setting {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\ndiv.setting input {\r\n    background: url('settings.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\nspan.home {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 6px;\r\n  }\r\nspan.analytic {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 2px;\r\n  }\r\nspan.notification {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 2px;\r\n  }\r\nspan.setting {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 12px;\r\n  }\r\n\r\n\r\n  \r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaGVhZGVyL2hlYWRlci5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7O0VBVUU7QUFDRjtJQUNJLDBCQUEwQjtJQUMxQixZQUFZO0lBQ1osWUFBWTtJQUNaLHlCQUF5QjtJQUN6QixrQkFBa0I7SUFDbEIscUJBQXFCO0lBQ3JCLHFCQUFxQjtJQUNyQixlQUFlO0lBQ2YsZ0JBQWdCO0lBQ2hCLGVBQWU7RUFDakI7QUFHRjtHQUNHLDBCQUEwQjtHQUMxQixZQUFZO0dBQ1osWUFBWTtHQUNaLHlCQUF5QjtHQUN6QixrQkFBa0I7R0FDbEIscUJBQXFCO0dBQ3JCLHFCQUFxQjtHQUNyQixlQUFlO0dBQ2YsZ0JBQWdCO0dBQ2hCLGVBQWU7Q0FDakI7QUFDQTtHQUNFLDBCQUEwQjtHQUMxQixZQUFZO0dBQ1osWUFBWTtHQUNaLHlCQUF5QjtHQUN6QixrQkFBa0I7R0FDbEIscUJBQXFCO0dBQ3JCLHFCQUFxQjtHQUNyQixlQUFlO0dBQ2YsZ0JBQWdCO0dBQ2hCLGVBQWU7Q0FDakI7QUFFQTtHQUNFLDBCQUEwQjtHQUMxQixZQUFZO0dBQ1osWUFBWTtHQUNaLHlCQUF5QjtHQUN6QixrQkFBa0I7R0FDbEIscUJBQXFCO0dBQ3JCLHFCQUFxQjtHQUNyQixlQUFlO0dBQ2YsZ0JBQWdCO0dBQ2hCLGVBQWU7Q0FDakI7QUFFQztLQUNHLGtCQUFrQjtLQUNsQixlQUFlO0tBQ2YsT0FBTztLQUNQLFNBQVM7S0FDVCxXQUFXO0tBQ1gsMkJBQTJCO0tBQzNCLFlBQVk7S0FDWixrQkFBa0I7RUFDckI7QUFFQTtJQUNFLGtCQUFrQjtJQUNsQixxQkFBcUI7RUFDdkI7QUFFQTtJQUNFLHFDQUEyRDtJQUMzRCxlQUFlO0lBQ2YsV0FBVztJQUNYLFlBQVk7SUFDWixZQUFZO0VBQ2Q7QUFDQTtJQUNFLGtCQUFrQjtJQUNsQixxQkFBcUI7RUFDdkI7QUFFQTtJQUNFLHlDQUErRDtJQUMvRCxlQUFlO0lBQ2YsV0FBVztJQUNYLFlBQVk7SUFDWixZQUFZO0VBQ2Q7QUFFQTtJQUNFLGtCQUFrQjtJQUNsQixxQkFBcUI7RUFDdkI7QUFFQTtJQUNFLG1EQUEyRTtJQUMzRSxlQUFlO0lBQ2YsV0FBVztJQUNYLFlBQVk7SUFDWixZQUFZO0VBQ2Q7QUFFQTtJQUNFLGtCQUFrQjtJQUNsQixxQkFBcUI7RUFDdkI7QUFFQTtJQUNFLHlDQUErRDtJQUMvRCxlQUFlO0lBQ2YsV0FBVztJQUNYLFlBQVk7SUFDWixZQUFZO0VBQ2Q7QUFDQTtJQUNFLGtCQUFrQjtJQUNsQixTQUFTO0lBQ1QsU0FBUztFQUNYO0FBRUE7SUFDRSxrQkFBa0I7SUFDbEIsU0FBUztJQUNULFNBQVM7RUFDWDtBQUVBO0lBQ0Usa0JBQWtCO0lBQ2xCLFNBQVM7SUFDVCxTQUFTO0VBQ1g7QUFFQTtJQUNFLGtCQUFrQjtJQUNsQixTQUFTO0lBQ1QsVUFBVTtFQUNaIiwiZmlsZSI6InNyYy9hcHAvaGVhZGVyL2hlYWRlci5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuI215UHJvZ3Jlc3Mge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNkZGQ7XHJcbn1cclxuXHJcbiNteUJhciB7XHJcbiAgd2lkdGg6IDElO1xyXG4gIGhlaWdodDogMzBweDtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNENBRjUwO1xyXG59Ki9cclxuI2J1dHRvbjEge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2IyYjJiMlx0OyBcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIHBhZGRpbmctYm90dG9tOiAyNXB4IDIycHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBmb250LXNpemU6IDM2cHg7XHJcbiAgICBtYXJnaW46IDRweCAxOHB4O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gIH1cclxuXHJcbiAgXHJcbiNidXR0b24yIHtcclxuICAgYmFja2dyb3VuZC1jb2xvcjogI2IyYjJiMlx0OyBcclxuICAgYm9yZGVyOiBub25lO1xyXG4gICBjb2xvcjogd2hpdGU7XHJcbiAgIHBhZGRpbmctYm90dG9tOiAyNXB4IDIycHg7XHJcbiAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgIGZvbnQtc2l6ZTogMzZweDtcclxuICAgbWFyZ2luOiA0cHggMThweDtcclxuICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gfVxyXG4gI2J1dHRvbjMge1xyXG4gICBiYWNrZ3JvdW5kLWNvbG9yOiAjYjJiMmIyXHQ7IFxyXG4gICBib3JkZXI6IG5vbmU7XHJcbiAgIGNvbG9yOiB3aGl0ZTtcclxuICAgcGFkZGluZy1ib3R0b206IDI1cHggMjJweDtcclxuICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgZm9udC1zaXplOiAzNnB4O1xyXG4gICBtYXJnaW46IDRweCAxOHB4O1xyXG4gICBjdXJzb3I6IHBvaW50ZXI7XHJcbiB9XHJcblxyXG4gI2J1dHRvbjQge1xyXG4gICBiYWNrZ3JvdW5kLWNvbG9yOiAjYjJiMmIyXHQ7XHJcbiAgIGJvcmRlcjogbm9uZTtcclxuICAgY29sb3I6IHdoaXRlO1xyXG4gICBwYWRkaW5nLWJvdHRvbTogMjVweCAyMnB4O1xyXG4gICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICBmb250LXNpemU6IDM2cHg7XHJcbiAgIG1hcmdpbjogNHB4IDE4cHg7XHJcbiAgIGN1cnNvcjogcG9pbnRlcjtcclxuIH1cclxuIFxyXG4gICNhbGx7XHJcbiAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgIHBvc2l0aW9uOiBmaXhlZDtcclxuICAgICBsZWZ0OiAwO1xyXG4gICAgIGJvdHRvbTogMDtcclxuICAgICB3aWR0aDogMTAwJTtcclxuICAgICBiYWNrZ3JvdW5kLWNvbG9yOiBcdFx0I2IyYjJiMjtcclxuICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIH1cclxuXHJcbiAgZGl2LmhvbWUge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIH1cclxuXHJcbiAgZGl2LmhvbWUgaW5wdXQge1xyXG4gICAgYmFja2dyb3VuZDogdXJsKCdzcmMvYXBwL2hlYWRlci9pbWFnZXMvaG9tZS5zdmcnKSBuby1yZXBlYXQ7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICB3aWR0aDogNTBweDtcclxuICAgIGhlaWdodDogNTBweDtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICB9XHJcbiAgZGl2LmFuYWx5dGljIHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICB9XHJcblxyXG4gIGRpdi5hbmFseXRpYyBpbnB1dCB7XHJcbiAgICBiYWNrZ3JvdW5kOiB1cmwoJ3NyYy9hcHAvaGVhZGVyL2ltYWdlcy9hbmFseXRpYy5zdmcnKSBuby1yZXBlYXQ7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICB3aWR0aDogNTBweDtcclxuICAgIGhlaWdodDogNTBweDtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICB9XHJcblxyXG4gIGRpdi5ub3RpZmljYXRpb24ge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIH1cclxuXHJcbiAgZGl2Lm5vdGlmaWNhdGlvbiBpbnB1dCB7XHJcbiAgICBiYWNrZ3JvdW5kOiB1cmwoJ3NyYy9hcHAvY2FsZW5kYXIvaW1hZ2VzL291dGxpbmUtdG9kYXktMjRweC5zdmcnKSBuby1yZXBlYXQ7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICB3aWR0aDogNTBweDtcclxuICAgIGhlaWdodDogNTBweDtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICB9XHJcbiAgXHJcbiAgZGl2LnNldHRpbmcge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIH1cclxuXHJcbiAgZGl2LnNldHRpbmcgaW5wdXQge1xyXG4gICAgYmFja2dyb3VuZDogdXJsKCdzcmMvYXBwL2hlYWRlci9pbWFnZXMvc2V0dGluZ3Muc3ZnJykgbm8tcmVwZWF0O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgd2lkdGg6IDUwcHg7XHJcbiAgICBoZWlnaHQ6IDUwcHg7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgfVxyXG4gIHNwYW4uaG9tZSB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDMycHg7XHJcbiAgICBsZWZ0OiA2cHg7XHJcbiAgfVxyXG4gIFxyXG4gIHNwYW4uYW5hbHl0aWMge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAzMnB4O1xyXG4gICAgbGVmdDogMnB4O1xyXG4gIH1cclxuXHJcbiAgc3Bhbi5ub3RpZmljYXRpb24ge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAzMnB4O1xyXG4gICAgbGVmdDogMnB4O1xyXG4gIH1cclxuXHJcbiAgc3Bhbi5zZXR0aW5nIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogMzJweDtcclxuICAgIGxlZnQ6IDEycHg7XHJcbiAgfVxyXG5cclxuXHJcbiAgIl19 */"

/***/ }),

/***/ "./src/app/header/header.component.html":
/*!**********************************************!*\
  !*** ./src/app/header/header.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n\n<link  href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css\"  rel=\"stylesheet\">\n\n\n<div id=\"button3\">   <div  class=\"col-sm-12\"   align=\"center\" style=\"background-color:red;\"  >  <button id=\"not3\" (click)=\"notification3()\">Notifications</button>  </div></div>\n\n\n   <div id=\"button\">   <div  class=\"col-sm-12\"   align=\"center\" style=\"background-color:red;\"  >  <button id=\"not\" (click)=\"notification()\">Notifications</button>  </div></div>\n\n\n   <div id=\"button2\">   <div  class=\"col-sm-12\"   align=\"center\" style=\"background-color:red;\"  >  <button id=\"not2\" (click)=\"notification2()\">Notifications</button>  </div></div>\n\n   <!--\n<div id=\"main\">\n <nav id=\"header\" class=\"navbar navbar-expand-lg navbar-dark bg-dark\">\n  <a class=\"navbar-brand\" href=\"#\">\n    FANI\n  </a>\n\n  <button class=\"navbar-toggler\" type=\"button\"\n          (click)=\"toggleNavbar()\">\n    <span class=\"navbar-toggler-icon\"></span>\n  </button>\n\n\n  <div class=\"collapse navbar-collapse\"\n       [ngClass]=\"{ 'show': navbarOpen }\">\n    <ul class=\"navbar-nav mr-auto\">\n      <li class=\"nav-item\">\n        <a class=\"nav-link\"routerLink=\"/signup\">calendar</a>\n      </li>\n\n      <li class=\"nav-item\">\n        <a class=\"nav-link\" href=\"#\">logout</a>\n      </li>\n    </ul>\n  </div>\n </nav>\n\n</div>-->\n\n\n\n <!-- <div class=\"container\">\n            \n      <div class=\"row\">\n          \n         <div class=\"col-sm-12\" style=\"\">\n            <div id=\"myProgress\">\n                <div id=\"myBar\"></div>\n              </div>\n          </div>\n          </div>\n    </div>-->\n  \n<div id=\"maincontenthome\">\n  \n\n  <div id=\"jquery-script-menu\">\n    <div class=\"jquery-script-center\">\n    \n    <div class=\"jquery-script-ads\"><script type=\"text/javascript\">\n    /* jQuery_demo */\n    google_ad_slot = \"2780937993\";\n    google_ad_width = 728;\n    google_ad_height = 90;\n    //-->\n    </script>\n    <script type=\"text/javascript\"\n    src=\"http://pagead2.googlesyndication.com/pagead/show_ads.js\">\n    </script></div>\n    <div class=\"jquery-script-clear\"></div>\n  \n    \n      </div>\n           <div class=\"container\">\n            \n                <div class=\"row\">\n                    \n                   <!-- <div class=\"col-sm-12\" style=\"\">\n                        <h1>Current Calories</h1>\n                    </div>-->\n                   \n                   \n                    <div class=\"col-sm-12 \">\n                        <h1  align=\"center\" style=\"color:#262626;\">Today plan</h1>\n                        <div class=\"my-progress-bar\"></div>\n                    </div>\n                </div>\n            </div>\n\n            <div class=\"container\">\n           \n                <div class=\"row\">\n                  <div class=\"col-sm-12\" >\n                      <h6 id=\"complete\" col align=\"center \" style=\"color:#262626;\"></h6>\n                   \n                  </div>\n                  \n                </div>\n              </div>\n\n       \n           \n    \n           <script src=\"https://code.jquery.com/jquery-3.2.1.min.js\"></script>\n           \n            <script type=\"text/javascript\">\n    \n      var _gaq = _gaq || [];\n      _gaq.push(['_setAccount', 'UA-36251023-1']);\n      _gaq.push(['_setDomainName', 'jqueryscript.net']);\n      _gaq.push(['_trackPageview']);\n    \n      (function() {\n        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;\n        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';\n        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);\n      })();\n    \n    </script>\n  \n</div>\n\n\n<div id=\"all\">\n\n  \n    <div class=\"home\">\n        <span id=\"home\" class=\"home\">Home  </span>\n        <input id=\"button1\" type=\"button\"  (click)=\"submitted=false\" (click)=\"goToPage('/', 'main')\">\n      \n    </div>\n\n     <div class=\"analytic\">\n      <span id=\"analytic\" class=\"analytic\">Analytics  </span>\n      <input id=\"button2\" type=\"button\" alt=\"Submit\" width=\"48\" height=\"48\" (click)=\"submitted=false\" (click)=\"goToPage('/', 'analytic')\">\n  \n    </div>\n\n    <div class=\"notification\">\n        <span id=\"notification\" class=\"notification\">Calendar  </span>\n        <input id=\"button3\" type=\"button\" alt=\"Submit\" width=\"48\" height=\"48\" (click)=\"submitted=false\" (click)=\"goToPage('/', 'calendar')\">\n    \n      </div>\n    \n      <div class=\"setting\">\n          <span id=\"setting\" class=\"setting\">Settings  </span>\n          <input id=\"button4\" type=\"button\" alt=\"Submit\" width=\"48\" height=\"48\" (click)=\"submitted=false\" (click)=\"goToPage('/', 'settings')\">\n      \n        </div>\n   \n     \n \n</div>\n\n\n<!--\n<div class=\"container\">\n  <div class=\"row\">\n    <div class=\"col-sm\">\n      <button class=\"button\">Green</button>\n    </div>\n    <div class=\"col-sm\">\n      <button class=\"button\">Green</button>\n    </div>\n    <div class=\"col-sm\">\n        <button class=\"button\">Green</button>\n     \n    </div>\n\n    <div class=\"col-sm\">\n        <button class=\"button\">Green</button>\n     \n    </div>\n\n   \n  </div>\n</div>\n-->\n\n\n"

/***/ }),

/***/ "./src/app/header/header.component.ts":
/*!********************************************!*\
  !*** ./src/app/header/header.component.ts ***!
  \********************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_push_notifications__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-push-notifications */ "./node_modules/ngx-push-notifications/fesm5/ngx-push-notifications.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _header_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./header.service */ "./src/app/header/header.service.ts");
/* harmony import */ var _sockets_chat_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../sockets/chat.service */ "./src/app/sockets/chat.service.ts");









var HeaderComponent = /** @class */ (function () {
    function HeaderComponent(chat, _pushNotificationService, router, http, HeaderService) {
        this.chat = chat;
        this._pushNotificationService = _pushNotificationService;
        this.router = router;
        this.http = http;
        this.HeaderService = HeaderService;
        this.glo = 0;
        this.timeLeft = 60;
        this.navbarOpen = false;
    }
    HeaderComponent.prototype.getClustering = function (energycontent) {
        var _this = this;
        this.HeaderService.getClusteringValues("Emily", /*"low"*/ energycontent).subscribe(function (res) {
            var i = JSON.stringify(res);
            var stringify = JSON.parse(i);
            stringify.forEach(function (element) {
                //  alert("Hier is is:"+i);
                // alert(element.username)
                // alert(element.name)
                //alert(element.type)
                // alert(element.wayofcooking)
                // alert(element.calories)
                //alert(element.nutrition)  
                //alert("Energycontent"+element.energycontent);
                _this.HeaderService.setBayssian(element.username, element.name, element.energycontent, element.nutrition, element.calories, element.wayofcooking).subscribe(function (res) {
                    var i = JSON.stringify(res);
                    var stringify = JSON.parse(i);
                    alert("Posted");
                }, function (error) {
                    alert("error hier");
                });
            });
            ///////////////////////////////////////////////////////////
            /*
            this.HeaderService.setBayssian("Emily","XA","XA2","XA3","XA4").subscribe((res)=>{
              var i=JSON.stringify(res);
              var stringify = JSON.parse(i);
              stringify.forEach(element => {
                alert(element.username)
                alert(element.name)
                alert(element.type)
                alert(element.wayofcooking)
                alert(element.calories)
                alert(element.nutrition)
              });
            
            
             
            }, (error) =>
            {
                alert("error hier");
               
            });
            */
            ////////////////////////////////////////////////////////////  
        }, function (error) {
            alert("error hier");
        });
    };
    HeaderComponent.prototype.getClustering2 = function (energycontent) {
        var _this = this;
        this.HeaderService.getClusteringValues("Emily", /*"low"*/ energycontent).subscribe(function (res) {
            var i = JSON.stringify(res);
            var stringify = JSON.parse(i);
            stringify.forEach(function (element) {
                //  alert("Hier is is:"+i);
                // alert(element.username)
                // alert(element.name)
                //alert(element.type)
                // alert(element.wayofcooking)
                // alert(element.calories)
                //alert(element.nutrition)  
                //alert("Energycontent"+element.energycontent);
                _this.HeaderService.setBayssian2(element.username, element.name, element.energycontent, element.nutrition, element.calories, element.wayofcooking).subscribe(function (res) {
                    var i = JSON.stringify(res);
                    var stringify = JSON.parse(i);
                    alert("Posted");
                }, function (error) {
                    alert("error hier");
                });
            });
            ///////////////////////////////////////////////////////////
            /*
            this.HeaderService.setBayssian("Emily","XA","XA2","XA3","XA4").subscribe((res)=>{
              var i=JSON.stringify(res);
              var stringify = JSON.parse(i);
              stringify.forEach(element => {
                alert(element.username)
                alert(element.name)
                alert(element.type)
                alert(element.wayofcooking)
                alert(element.calories)
                alert(element.nutrition)
              });
            
            
             
            }, (error) =>
            {
                alert("error hier");
               
            });
            */
            ////////////////////////////////////////////////////////////  
        }, function (error) {
            alert("error hier");
        });
    };
    HeaderComponent.prototype.notification3 = function () {
        var _this = this;
        this.getClustering("low");
        var d = new Date(); // for now
        d.getHours(); // => 9
        d.getMinutes(); // =>  30
        d.getSeconds(); // => 51
        alert("Todays" + d.getDay());
        var e = String(this.days(/*d.getDay()*/ d.getDay() - 1) + " at " + d.getHours() + ":" + d.getMinutes());
        this.http.get('http://localhost:3000/api/things/getFood/Emily', { responseType: "json" }).subscribe(function (res) {
            var flag = 0;
            var i = JSON.stringify(res[0]);
            var stringify = JSON.parse(i);
            alert("Stringify" + stringify.food1);
            alert("Day breakfast is:" + String(e));
            _this.HeaderService.setHistory("emily", stringify.food1, stringify.food2, "-", "-", "monday", "high", String(e), "Breakfast").subscribe(function (res) {
                //alert("Heys setHistory");
                var i = JSON.stringify(res);
                var stringify = JSON.parse(i);
            }, function (error) {
                alert("error hier");
            });
        }, function (error) {
        });
        /*
        
        
        .subscribe((res)=>{
             // alert("took");
              var flag=0;
              
             // alert("continue");
              //alert(JSON.stringify(res));
        
        
              
              //console.log("called");
              //alert("Clled");
              //alert("cabled"+res[0].info);
              //  console.log(res);
            }, (error) =>
            {
              //alert("error hier");
            //  alert(error);
                // if the request fails, this callback will be invoked
            });
        
        
        
        */
        function test() {
            // this.glo==0;
            // alert("finalaa");
            //alert(location.host);
            //alert(location.hostname);
            // alert("concat");
            // alert(location.host.toString().concat("/main"));
            var i = location.host.toString().concat("/notification").toString().trim();
            // alert(i);
            var all = "http://".concat(i);
            // alert("all are");
            //alert(all);
            window.location.href = all; // i;//'http://www.google.com';
        }
        var title = 'Meal Recommendations';
        var options = new ngx_push_notifications__WEBPACK_IMPORTED_MODULE_2__["PushNotificationOptions"]();
        options.body = 'Its time for breakfast now';
        options.icon = 'https://banner2.kisspng.com/20180219/zqq/kisspng-hamburger-fast-food-restaurant-junk-food-kfc-cartoon-french-fries-5a8b6353d9a269.5412864515190843718914.jpg'; //adding an icon
        this._pushNotificationService.create(title, options).subscribe(function (notif) {
            if (notif.event.type === 'show') {
                console.log('onshow');
                setTimeout(function () {
                }, 3000);
            }
            if (notif.event.type === 'click') {
                console.log('click');
                test();
                notif.notification.close();
            }
            if (notif.event.type === 'close') {
                console.log('close');
            }
        }, function (err) {
            console.log(err);
        });
    };
    HeaderComponent.prototype.notification2 = function () {
        var _this = this;
        alert("Not 2 called");
        this.getClustering("high");
        /////////////////////////////////////////////////////////////////////////////////////
        //this.getClustering("high");
        var d = new Date(); // for now
        d.getHours(); // => 9
        d.getMinutes(); // =>  30
        d.getSeconds(); // => 51
        var e = String(this.days(d.getDay() - 1) + " at " + d.getHours() + ":" + d.getMinutes());
        var name = sessionStorage.getItem("lastname").toString().trim();
        this.http.get("http://localhost:3000/api/things/getFood/" + name, { responseType: "json" }).subscribe(function (res) {
            // alert("took");
            var flag = 0;
            var i = JSON.stringify(res[0]);
            var stringify = JSON.parse(i);
            alert("Stringify" + stringify.food1);
            _this.HeaderService.setHistory("emily", stringify.food1, stringify.food2, "-", "-", "monday", "high", String(e), "Dinner").subscribe(function (res) {
                //alert("Heys setHistory");
                var i = JSON.stringify(res);
                var stringify = JSON.parse(i);
            }, function (error) {
                alert("error hier");
            });
        }, function (error) {
        });
        /////////////////////////////////////////////////////////////////////////////
        function test() {
            // this.glo==0;
            // alert("finalaa");
            //alert(location.host);
            //alert(location.hostname);
            // alert("concat");
            // alert(location.host.toString().concat("/main"));
            var i = location.host.toString().concat("/notification").toString().trim();
            // alert(i);
            var all = "http://".concat(i);
            // alert("all are");
            //alert(all);
            window.location.href = all; // i;//'http://www.google.com';
        }
        var title = 'Meal Recommendations';
        var options = new ngx_push_notifications__WEBPACK_IMPORTED_MODULE_2__["PushNotificationOptions"]();
        options.body = 'Its time for dinner now';
        options.icon = 'https://banner2.kisspng.com/20180219/zqq/kisspng-hamburger-fast-food-restaurant-junk-food-kfc-cartoon-french-fries-5a8b6353d9a269.5412864515190843718914.jpg'; //adding an icon
        this._pushNotificationService.create(title, options).subscribe(function (notif) {
            if (notif.event.type === 'show') {
                console.log('onshow');
                setTimeout(function () {
                }, 3000);
            }
            if (notif.event.type === 'click') {
                console.log('click');
                test();
                notif.notification.close();
            }
            if (notif.event.type === 'close') {
                console.log('close');
            }
        }, function (err) {
            console.log(err);
        });
    };
    HeaderComponent.prototype.notification = function () {
        var _this = this;
        this.getClustering("medium");
        /////////////////////////////////////////////////////////////////////////////////////
        //this.getClustering("high");
        var d = new Date(); // for now
        d.getHours(); // => 9
        d.getMinutes(); // =>  30
        d.getSeconds(); // => 51
        var e = String(this.days(d.getDay() - 1) + " at " + d.getHours() + ":" + d.getMinutes());
        var name = sessionStorage.getItem("lastname").toString().trim();
        this.http.get("http://localhost:3000/api/things/getFood/" + name, { responseType: "json" }).subscribe(function (res) {
            // alert("took");
            var flag = 0;
            var i = JSON.stringify(res[0]);
            var stringify = JSON.parse(i);
            _this.HeaderService.setHistory("emily", stringify.food1, stringify.food2, "-", "-", "monday", "medium", String(e), "Lunch").subscribe(function (res) {
                //alert("Heys setHistory");
                var i = JSON.stringify(res);
                var stringify = JSON.parse(i);
            }, function (error) {
                alert("error hier");
            });
        }, function (error) {
        });
        function test() {
            // this.glo==0;
            // alert("finalaa");
            //alert(location.host);
            //alert(location.hostname);
            // alert("concat");
            // alert(location.host.toString().concat("/main"));
            var i = location.host.toString().concat("/notification").toString().trim();
            // alert(i);
            var all = "http://".concat(i);
            // alert("all are");
            //alert(all);
            window.location.href = all; // i;//'http://www.google.com';
        }
        var title = 'Meal Recommendations';
        var options = new ngx_push_notifications__WEBPACK_IMPORTED_MODULE_2__["PushNotificationOptions"]();
        options.body = 'Its time for lunch now';
        options.icon = 'https://banner2.kisspng.com/20180219/zqq/kisspng-hamburger-fast-food-restaurant-junk-food-kfc-cartoon-french-fries-5a8b6353d9a269.5412864515190843718914.jpg'; //adding an icon
        this._pushNotificationService.create(title, options).subscribe(function (notif) {
            if (notif.event.type === 'show') {
                console.log('onshow');
                setTimeout(function () {
                }, 3000);
            }
            if (notif.event.type === 'click') {
                console.log('click');
                test();
                notif.notification.close();
            }
            if (notif.event.type === 'close') {
                console.log('close');
            }
        }, function (err) {
            console.log(err);
        });
    };
    HeaderComponent.prototype.oberserableTimer = function () {
        var _this = this;
        var source = Object(rxjs__WEBPACK_IMPORTED_MODULE_6__["timer"])(1000, 20000);
        var abc = source.subscribe(function (val) {
            console.log(val, '-');
            console.log(val);
            var today = new Date();
            alert("in http");
            var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
            console.log("Now time time" + time);
            var name = document.getElementById("session").innerHTML = sessionStorage.getItem("lastname");
            _this.http.get("http://localhost:3000/api/things/getsensorvalues/" + name, { responseType: "json" }).subscribe(function (response) {
                var sample = JSON.parse(JSON.stringify(response));
                var a = String(sample.energycontent);
                var b = (sample.time);
                console.log("cur time" + time + "glo time" + b);
                if ((a == "high") /*&&(time==b)*/) {
                    _this.glo++;
                    document.getElementById("not3").click();
                    // alert("Scenario called"+a);
                    //  setInterval(function () {document.getElementById("not").click();}, 10000);
                }
                else if ((a == "medium") /*&&(time==b)*/) {
                    _this.glo++;
                    document.getElementById("not2").click();
                    // alert("Scenario called"+a);
                    //  setInterval(function () {document.getElementById("not").click();}, 10000);
                }
                else if ((a == "low") /*&&(time==b)*/) {
                    _this.glo++;
                    document.getElementById("not").click();
                }
            }); ///hier end calendar
            _this.subscribeTimer = _this.timeLeft - val;
        });
        ///////////////////////////////SENSOR2 BA;UES HIER
    };
    HeaderComponent.prototype.days = function (number) {
        if (number == 1) {
            return "Monday";
        }
        else if (number == 2) {
            return "Tuesday";
        }
        else if (number == 3) {
            return "Wednesday";
        }
        else if (number == 4) {
            return "Thursday";
        }
        else if (number == 5) {
            return "Friday";
        }
    };
    HeaderComponent.prototype.BmrAlgorithm = function (gender, weight, height, age) {
        //  alert("Values Bmr are:"+gender+weight+height+age);
        // String:s="";
        var bmr = 0;
        if (gender == "male") {
            bmr = 66.5 + (13.75 * Number(weight)) + (5.003 * Number(height)) - (6.755 * Number(age));
        }
        else if (gender == "female") {
            bmr = 655.1 + (9.563 * Number(weight)) + (1.850 * Number(height)) - (4.676 * Number(age));
        }
        return bmr;
    };
    HeaderComponent.prototype.ngOnInit = function () {
        var _this = this;
        var name = document.getElementById("session").innerHTML = sessionStorage.getItem("lastname");
        jquery__WEBPACK_IMPORTED_MODULE_3__('#session').show();
        jquery__WEBPACK_IMPORTED_MODULE_3__('#alllist').hide();
        jquery__WEBPACK_IMPORTED_MODULE_3__('#main').show();
        jquery__WEBPACK_IMPORTED_MODULE_3__('#button').hide();
        jquery__WEBPACK_IMPORTED_MODULE_3__('#button2').hide();
        jquery__WEBPACK_IMPORTED_MODULE_3__('#button3').hide();
        jquery__WEBPACK_IMPORTED_MODULE_3__('#session').show();
        jquery__WEBPACK_IMPORTED_MODULE_3__('#back').hide();
        jquery__WEBPACK_IMPORTED_MODULE_3__('#date').hide();
        ////////////////////
        this.chat.messages.subscribe(function (msg) {
            console.log("Messages are:");
            alert("not Cllledddddd" + msg.text);
            console.log(msg.text);
            if (msg.text == "inhouse/notbeats/sitting/undefined") {
                alert("not3");
                document.getElementById("not3").click();
            }
            else if (msg.text == "inhouse/beats/notsitting/undefined") {
                alert("not2");
                document.getElementById("not2").click();
            }
            else if (msg.text == "inhouse/notbeats/notsitting/undefined") {
                document.getElementById("not").click();
            }
            // document.getElementById("not3").click();
            if (msg.text == "not") {
                //this.oberserableTimer();
            }
        });
        /////////////////////
        // this.oberserableTimer();
        function delay(ms) {
            return new Promise(function (resolve) { return setTimeout(resolve, ms); });
        }
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(1000)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        }); })();
        //setTimeout(notification(), 2000);
        // $('#maincontenthome').hide();
        //maincontenthome
        /*  var elem = document.getElementById("myBar");
          var width = 1;
          var id = setInterval(frame, 10);
          function frame() {
            //alert("frame");
            if (width >= 100) {
              alert("called");
              clearInterval(id);
              $('#maincontenthome').show();
              alert("alles");
             // this.goToPage('/', 'main');
            } else {
              width++;
              elem.style.width = width + '%';
            }
          }*/
        document.getElementById("home").style.backgroundColor = "#696969";
        //document.getElementById("button1").style.backgroundColor="#FFFFFF";
        jquery__WEBPACK_IMPORTED_MODULE_3__(document).ready(function () {
            // $("#maincontenthome").hide();
            // $("#maincontentanalytic").hide();
            //alert("Clicked");
            /*
                  $("button").click(function(){
                  
                  });
                  */
        });
        /*  $(document).ready(function(){
            $("button").click(function(){
              $("p").hide();
      
      
            });
          });*/
        //
        var global_settings = { percentage: "a", text: "a", starting_position: 1,
            ending_position: 1, width: 1, height: 1, percent: 0, color: "a", line_width: 1,
            counter_clockwise: true
        };
        (function ($) {
            var methods = {
                init: function (options) {
                    // This is the easiest way to have default options.
                    var settings = $.extend({
                        // These are the defaults.
                        color: "#000000",
                        height: "300px",
                        width: "300px",
                        line_width: 8,
                        starting_position: 25,
                        percent: 100,
                        counter_clockwise: false,
                        percentage: true,
                        text: ''
                    }, options);
                    global_settings = settings;
                    // Create percentage
                    var percentage = $("<div class='progress-percentage'></div>");
                    if (!global_settings.percentage) {
                        percentage.text(global_settings.percentage);
                    }
                    $(this).append(percentage);
                    // Create text
                    var text = $("<div class='progress-text'></div>");
                    // Custom text
                    if (global_settings.text != "percent") {
                        text.text(global_settings.text);
                    }
                    $(this).append(text);
                    // Correct any invalid values
                    if (global_settings.starting_position != 100) {
                        global_settings.starting_position = global_settings.starting_position % 100;
                    }
                    if (global_settings.ending_position != 100) {
                        global_settings.ending_position = global_settings.ending_position % 100;
                    }
                    // No 'px' or '%', add 'px'
                    appendUnit(global_settings.width);
                    appendUnit(global_settings.height);
                    // Apply global_settings
                    $(this).css({
                        "height": global_settings.height,
                        "width": global_settings.width
                    });
                    $(this).addClass("circular-progress-bar");
                    // Remove old canvas
                    $(this).find("canvas").remove();
                    // Put canvas inside this
                    $(this).append(createCanvas($(this)));
                    // Return allows for chaining
                    return this;
                },
                percent: function (value) {
                    // Change percent
                    global_settings.percent = value;
                    // Apply global_settings
                    $(this).css({
                        "height": global_settings.height,
                        "width": global_settings.width
                    });
                    // Remove old canvas
                    $(this).children("canvas").remove();
                    // Put canvas inside this
                    $(this).append(createCanvas($(this)));
                    // Return allows for chaining
                    return this;
                },
                animate: function (value, time) {
                    // Apply global_settings
                    // Number of intervals, 10ms interval
                    var num_of_steps = time / 10;
                    // Amount of change each step
                    var percent_change = (value - global_settings.percent) / num_of_steps;
                    // Variable conflict, rename this
                    var scope = $(this);
                    var theInterval = setInterval(function () {
                        if (global_settings.percent < value) {
                            // Remove old canvas
                            scope.children("canvas").remove();
                            // Increment percent
                            global_settings.percent += percent_change;
                            // Put canvas inside this
                            scope.append(createCanvas(scope));
                        }
                        else {
                            clearInterval(theInterval);
                        }
                    }, 10);
                    // Return allows for chaining
                    return this;
                }
            };
            $.fn.circularProgress = function (methodOrOptions) {
                if (methods[methodOrOptions]) {
                    // Method found
                    return methods[methodOrOptions].apply(this, Array.prototype.slice.call(arguments, 1));
                }
                else if (typeof methodOrOptions === 'object' || !methodOrOptions) {
                    // Default to "init", object passed in or nothing passed in
                    return methods.init.apply(this, arguments);
                }
                else {
                    $.error('Method ' + methodOrOptions + ' does not exist.');
                }
            };
            /* =========================================================================
                PRIVATE FUNCTIONS
            ========================================================================= */
            // return string without 'px' or '%'
            function removeUnit(apples) {
                if (apples.indexOf("px")) {
                    return apples.substring(0, apples.length - 2);
                }
            }
            ;
            // return string with 'px'
            function appendUnit(apples) {
                if (apples.toString().indexOf("px") < -1 && apples.toString().indexOf("%") < -1) {
                    return apples += "px";
                }
            }
            ;
            // calculate starting position on canvas
            function calcPos(apples, percent) {
                if (percent < 0) {
                    // Calculate starting position
                    var starting_degree = (parseInt(apples) / 100) * 360;
                    var starting_radian = starting_degree * (Math.PI / 180);
                    return starting_radian - (Math.PI / 2);
                }
                else {
                    // Calculate ending position
                    var ending_degree = ((parseInt(apples) + parseInt(percent)) / 100) * 360;
                    var ending_radian = ending_degree * (Math.PI / 180);
                    return ending_radian - (Math.PI / 2);
                }
            }
            ;
            // Put percentage or custom text inside progress circle
            function insertText(scope) {
                $(".progress-percentage").text(Math.round(global_settings.percent) + "%");
            }
            // create canvas
            function createCanvas(scope) {
                // Remove 'px' or '%'
                var canvas_height = removeUnit(global_settings.height.toString());
                var canvas_width = removeUnit(global_settings.width.toString());
                // Create canvas
                var canvas = document.createElement("canvas");
                canvas.height = canvas_height;
                canvas.width = canvas_width;
                // Create drawable canvas and apply properties
                var ctx = canvas.getContext("2d");
                ctx.strokeStyle = global_settings.color;
                ctx.lineWidth = global_settings.line_width;
                // Draw arc
                ctx.beginPath();
                // Calculate starting and ending positions
                var starting_radian = calcPos(global_settings.starting_position, -1);
                var ending_radian = calcPos(global_settings.starting_position, global_settings.percent);
                // Calculate radius and x,y coordinates
                var radius = 0;
                var xcoord = canvas_width / 2;
                var ycoord = canvas_height / 2;
                // Height or width greater
                if (canvas_height >= canvas_width) {
                    radius = canvas_width * 0.9 / 2 - (global_settings.line_width * 2);
                }
                else {
                    radius = canvas_height * 0.9 / 2 - (global_settings.line_width * 2);
                }
                /*
                    x coordinate
                    y coordinate
                    radius of circle
                    starting angle in radians
                    ending angle in radians
                    clockwise (false, default) or counter-clockwise (true)
                */
                ctx.arc(xcoord, ycoord, radius, starting_radian, ending_radian, global_settings.counter_clockwise);
                ctx.stroke();
                // Add text
                if (global_settings.percentage) {
                    insertText(scope);
                }
                return canvas;
            }
            ;
        }(jquery__WEBPACK_IMPORTED_MODULE_3__));
        ///////////////////////////////
        var name = sessionStorage.getItem("lastname");
        var calories = 0;
        this.HeaderService.getBmrValues(name).subscribe(function (res) {
            //  alert(this.username+this.password+this.age+this.weight);
            var sample = JSON.stringify(res);
            var js = JSON.parse(sample);
            //alert("js is:"+js[0].username);
            //alert("js is:"+js[0].password);
            //alert("js is:"+js[0].gender);
            //alert("js is:"+js[0].age);
            //alert("js is:"+js[0].height);
            //alert("js is:"+js[0].weight);
            //gender:String,weight:String,height:String,age:String
            //this.BmrAlgorithm(js[0].gender,js[0].weight,js[0].height,js[0].age);
            /*if sedentary BMR*1.2
              IF LIGHTLY ACTIVE(SPOSRTS 1-3 WEEK)bmr*1.375
              IF MODERATELY active (3-5 DAYS PER WEEK) bmr*1.55
              IF VERY ACTIVE( 6-7 days paer week)  BMR*1725
              IF EXTRA ACTIVE(VERY HARD EXERCISE PSYSICAL JOB AND TRAINING) BMR*1.9
         
            */
            var flag = 0;
            // alert("HeaderService bmr"+this.BmrAlgorithm(js[0].gender,js[0].weight,js[0].height,js[0].age)*1.375);
            calories = Math.round(_this.BmrAlgorithm(js[0].gender, js[0].weight, js[0].height, js[0].age) * 1.375);
            var final = "1600/" + calories + ' Kcal';
            var sentence = "You should complete " + calories + " Kcal";
            ////////////////////////////////
            //this.BmrAlgorithm("","","","");
            //alert("Bmr out is:"+this.BmrAlgorithm("","","",""));
            document.getElementById("complete").textContent = sentence;
            var a = 20;
            jquery__WEBPACK_IMPORTED_MODULE_3__(".my-progress-bar").circularProgress({
                line_width: 9,
                color: "#ff8000",
                starting_position: 0,
                percent: 0,
                percentage: true,
                text: final
            }).circularProgress('animate', a, 5000);
            //alert(res);
        }, function (error) {
            alert("error hier");
        });
        //`${this.apiURL}${'/'}${username}${'/'}${password}${'/'}${age}${'/'}${weight}`
        //var final='1600/"" Kcal'
        //
    };
    HeaderComponent.prototype.toggleNavbar = function () {
        this.navbarOpen = !this.navbarOpen;
    };
    HeaderComponent.prototype.goToPage = function (pagename, parameter) {
        this.router.navigate([pagename, parameter]);
    };
    HeaderComponent.prototype.submit1 = function () {
        //alert("cliecked");
        //alert("Called it hier1!");
        //alert(this.email);
        // document.body.style.backgroundColor = "red";
        document.getElementById("home").style.backgroundColor = "#696969";
        // $("#maincontenthome").show();
        // this.router.navigate(["http://localhost:8080/main", ""]);
        //window.location.href = "/main";
        // alert("Called submit");
    };
    HeaderComponent.prototype.submit2 = function () {
        //alert("Called it hier1!");
        //alert(this.email);
        // document.body.style.backgroundColor = "red";
        //document.getElementById("button2").style.backgroundColor="#FFFFFF";
        //$("#maincontentanalytic").show();
        //alert("Called submit");
    };
    HeaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-header',
            template: __webpack_require__(/*! ./header.component.html */ "./src/app/header/header.component.html"),
            styles: [__webpack_require__(/*! ./header.component.css */ "./src/app/header/header.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_sockets_chat_service__WEBPACK_IMPORTED_MODULE_8__["ChatService"], ngx_push_notifications__WEBPACK_IMPORTED_MODULE_2__["PushNotificationService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"], _header_service__WEBPACK_IMPORTED_MODULE_7__["HeaderService"]])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/header/header.service.ts":
/*!******************************************!*\
  !*** ./src/app/header/header.service.ts ***!
  \******************************************/
/*! exports provided: HeaderService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderService", function() { return HeaderService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");



var HeaderService = /** @class */ (function () {
    function HeaderService(httpClient) {
        this.httpClient = httpClient;
        this.apiURL = 'http://127.0.0.1:3000/api/things/sethistoryvalue/';
        this.apiURLClass = 'http://localhost:3000/api/things/getclassification/';
        this.apiURLClassBays = 'http://127.0.0.1:3000/api/things/setsensorsvalues/';
        this.apiURLClassBays2 = 'http://127.0.0.1:3000/api/things/setsensorsvalues2/';
    }
    HeaderService.prototype.getBmrValues = function (username) {
        var data = {
            "name": "fr"
        };
        return this.httpClient.get("http://localhost:3000/api/things/getBmrValues/" + username, { responseType: "json" });
    };
    HeaderService.prototype.setHistory = function (username, name, name2, name3, name4, day, energycontent, date, type) {
        var data = {
            "name": "fr"
        };
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
            })
        };
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
            .set('username', String(username))
            .set('name', String(name))
            .set('name2', String(name2))
            .set('name3', String(name3))
            .set('name4', String(name4))
            .set('day', String(day))
            .set('energycontent', String(energycontent))
            .set('date', String(date))
            .set('type', String(type));
        var options = {
            httpOptions: httpOptions,
            params: params,
            withCredentials: false
        };
        return this.httpClient.post("" + this.apiURL, null, options);
    };
    HeaderService.prototype.getClusteringValues = function (username, energycontent) {
        var data = {
            "name": "fr"
        };
        return this.httpClient.get("" + this.apiURLClass + '/' + username + '/' + energycontent, { responseType: "json" });
    };
    HeaderService.prototype.setBayssian = function (username, name, energycontent, nutrition, calories, wayofcooking) {
        var data = {
            "name": "fr"
        };
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
            })
        };
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
            .set('username', String(username))
            .set('energycontent', String(energycontent))
            .set('nutrition', String(nutrition))
            .set('calories', String(calories))
            .set('wayofcooking', String(wayofcooking))
            .set('name', String(name));
        var options = {
            httpOptions: httpOptions,
            params: params,
            withCredentials: false
        };
        return this.httpClient.post("" + this.apiURLClassBays, null, options);
    };
    HeaderService.prototype.setBayssian2 = function (username, name, energycontent, nutrition, calories, wayofcooking) {
        var data = {
            "name": "fr"
        };
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
            })
        };
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
            .set('username', String(username))
            .set('energycontent', String(energycontent))
            .set('nutrition', String(nutrition))
            .set('calories', String(calories))
            .set('wayofcooking', String(wayofcooking))
            .set('name', String(name));
        var options = {
            httpOptions: httpOptions,
            params: params,
            withCredentials: false
        };
        return this.httpClient.post("" + this.apiURLClassBays2, null, options);
    };
    HeaderService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], HeaderService);
    return HeaderService;
}());



/***/ }),

/***/ "./src/app/infos/infos.component.css":
/*!*******************************************!*\
  !*** ./src/app/infos/infos.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#button1 {\r\n    background-color: #b2b2b2\t; \r\n    border: none;\r\n    color: white;\r\n    padding-bottom: 25px 22px;\r\n    text-align: center;\r\n    text-decoration: none;\r\n    display: inline-block;\r\n    font-size: 36px;\r\n    margin: 4px 18px;\r\n    cursor: pointer;\r\n    \r\n  }\r\n\r\n  \r\n#button2 {\r\n   background-color: #b2b2b2\t; \r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n\r\n  \r\n#button3 {\r\n   background-color: #b2b2b2\t; \r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n\r\n  \r\n#button4 {\r\n   background-color: #b2b2b2\t;\r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n\r\n  \r\n#buttona {\r\n    border-radius: 25px;\r\n    background-color: #b2b2b2\t; \r\n    border: none;\r\n    color: white;\r\n    padding-bottom: 25px 22px;\r\n    text-align: center;\r\n    text-decoration: none;\r\n    display: inline-block;\r\n    font-size: 36px;\r\n    margin: 4px 18px;\r\n    cursor: pointer;\r\n    \r\n  }\r\n\r\n  \r\n#buttonb {\r\n    border-radius: 25px;\r\n   background-color: #b2b2b2\t; \r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n\r\n  \r\n#buttonc {\r\n    border-radius: 25px;\r\n   background-color: #b2b2b2\t; \r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n\r\n  \r\n#buttond {\r\n    border-radius: 25px;\r\n   background-color: #b2b2b2\t;\r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n\r\n  \r\n#buttone {\r\n    border-radius: 25px;\r\n    background-color: #b2b2b2\t;\r\n    border: none;\r\n    color: white;\r\n    padding-bottom: 25px 22px;\r\n    text-align: center;\r\n    text-decoration: none;\r\n    display: inline-block;\r\n    font-size: 36px;\r\n    margin: 4px 18px;\r\n    cursor: pointer;\r\n  }\r\n\r\n  \r\n#all{\r\n     text-align: center;\r\n     position: fixed;\r\n     left: 0;\r\n     bottom: 0;\r\n     width: 100%;\r\n     background-color: \t\t#b2b2b2;\r\n     color: white;\r\n     text-align: center;\r\n  }\r\n\r\n  \r\ndiv.home {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\n\r\n  \r\ndiv.home input {\r\n    background: url('home.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\n\r\n  \r\ndiv.analytic {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\n\r\n  \r\ndiv.analytic input {\r\n    background: url('analytic.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\n\r\n  \r\ndiv.notification {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\n\r\n  \r\ndiv.notification input {\r\n    background: url('outline-today-24px.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\n\r\n  \r\ndiv.setting {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\n\r\n  \r\ndiv.setting input {\r\n    background: url('settings.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\n\r\n  \r\nspan.home {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 6px;\r\n  }\r\n\r\n  \r\nspan.analytic {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 2px;\r\n  }\r\n\r\n  \r\nspan.notification {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 2px;\r\n  }\r\n\r\n  \r\nspan.setting {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 12px;\r\n  }\r\n\r\n  \r\nimg {\r\n    width: 100%;\r\n    height: auto;\r\n    max-height: 400px;\r\n   /* background: url(loading.gif) 50% no-repeat;*/\r\n   /* border: 1px solid black;*/\r\n   /* border-radius: 5px;*/\r\n  }\r\n\r\n  \r\n.grid-container {\r\n    display: grid;\r\n    grid-template-columns: auto auto ;\r\n    grid-gap: 10px;\r\n    background-color: #ffffff;\r\n    padding: 10px;\r\n  }\r\n\r\n  \r\n.grid-container > div {\r\n    background-color: rgba(255, 255, 255, 0.8);\r\n    text-align: center;\r\n    padding: 20px 1;\r\n    font-size: 30px;\r\n  }\r\n\r\n  \r\n.grid-container-last {\r\n    display: grid;\r\n    grid-template-columns: auto ;\r\n    grid-gap: 10px;\r\n    background-color: #FFFFFF;\r\n    padding: 10px;\r\n  }\r\n\r\n  \r\n.grid-container-last > div {\r\n    background-color: rgba(255, 255, 255, 0.8);\r\n    text-align: center;\r\n    padding: 20px 1;\r\n    font-size: 30px;\r\n  }\r\n\r\n  \r\n#header{\r\n     \r\n      color:grey;\r\n\r\n\r\n  }\r\n\r\n  \r\n#subheader{\r\n   \r\n    color:#00ff40;\r\n\r\n\r\n}\r\n\r\n  \r\n/*\r\n  #bullets{\r\n  \r\n   \r\n\r\n}*/\r\n\r\n  \r\n/*  #third{\r\n    text-align: left\r\n\r\n  }\r\n\r\n  #fifth{\r\n    text-align: left\r\n  }\r\n*/\r\n\r\n  \r\ndiv.bullet1 input {\r\n    background: url('round-brightness_1-24px.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\n\r\n  \r\nspan.bullet1 {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 2px;\r\n  }\r\n\r\n  \r\ndiv.bullet1 {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\n\r\n  \r\n.loader {\r\n  border: 16px solid #f3f3f3;\r\n  border-radius: 50%;\r\n  border-top: 16px solid #ff8000;\r\n  margin: auto;\r\n  width: 120px;\r\n  height: 120px;\r\n  -webkit-animation: spin 2s linear infinite; /* Safari */\r\n  animation: spin 2s linear infinite;\r\n}\r\n\r\n  \r\n/* Safari */\r\n\r\n  \r\n@-webkit-keyframes spin {\r\n  0% { -webkit-transform: rotate(0deg); }\r\n  100% { -webkit-transform: rotate(360deg); }\r\n}\r\n\r\n  \r\n@keyframes spin {\r\n  0% { -webkit-transform: rotate(0deg); transform: rotate(0deg); }\r\n  100% { -webkit-transform: rotate(360deg); transform: rotate(360deg); }\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaW5mb3MvaW5mb3MuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLDBCQUEwQjtJQUMxQixZQUFZO0lBQ1osWUFBWTtJQUNaLHlCQUF5QjtJQUN6QixrQkFBa0I7SUFDbEIscUJBQXFCO0lBQ3JCLHFCQUFxQjtJQUNyQixlQUFlO0lBQ2YsZ0JBQWdCO0lBQ2hCLGVBQWU7O0VBRWpCOzs7QUFHRjtHQUNHLDBCQUEwQjtHQUMxQixZQUFZO0dBQ1osWUFBWTtHQUNaLHlCQUF5QjtHQUN6QixrQkFBa0I7R0FDbEIscUJBQXFCO0dBQ3JCLHFCQUFxQjtHQUNyQixlQUFlO0dBQ2YsZ0JBQWdCO0dBQ2hCLGVBQWU7Q0FDakI7OztBQUNBO0dBQ0UsMEJBQTBCO0dBQzFCLFlBQVk7R0FDWixZQUFZO0dBQ1oseUJBQXlCO0dBQ3pCLGtCQUFrQjtHQUNsQixxQkFBcUI7R0FDckIscUJBQXFCO0dBQ3JCLGVBQWU7R0FDZixnQkFBZ0I7R0FDaEIsZUFBZTtDQUNqQjs7O0FBRUE7R0FDRSwwQkFBMEI7R0FDMUIsWUFBWTtHQUNaLFlBQVk7R0FDWix5QkFBeUI7R0FDekIsa0JBQWtCO0dBQ2xCLHFCQUFxQjtHQUNyQixxQkFBcUI7R0FDckIsZUFBZTtHQUNmLGdCQUFnQjtHQUNoQixlQUFlO0NBQ2pCOzs7QUFLQTtJQUNHLG1CQUFtQjtJQUNuQiwwQkFBMEI7SUFDMUIsWUFBWTtJQUNaLFlBQVk7SUFDWix5QkFBeUI7SUFDekIsa0JBQWtCO0lBQ2xCLHFCQUFxQjtJQUNyQixxQkFBcUI7SUFDckIsZUFBZTtJQUNmLGdCQUFnQjtJQUNoQixlQUFlOztFQUVqQjs7O0FBR0Y7SUFDSSxtQkFBbUI7R0FDcEIsMEJBQTBCO0dBQzFCLFlBQVk7R0FDWixZQUFZO0dBQ1oseUJBQXlCO0dBQ3pCLGtCQUFrQjtHQUNsQixxQkFBcUI7R0FDckIscUJBQXFCO0dBQ3JCLGVBQWU7R0FDZixnQkFBZ0I7R0FDaEIsZUFBZTtDQUNqQjs7O0FBQ0E7SUFDRyxtQkFBbUI7R0FDcEIsMEJBQTBCO0dBQzFCLFlBQVk7R0FDWixZQUFZO0dBQ1oseUJBQXlCO0dBQ3pCLGtCQUFrQjtHQUNsQixxQkFBcUI7R0FDckIscUJBQXFCO0dBQ3JCLGVBQWU7R0FDZixnQkFBZ0I7R0FDaEIsZUFBZTtDQUNqQjs7O0FBRUE7SUFDRyxtQkFBbUI7R0FDcEIsMEJBQTBCO0dBQzFCLFlBQVk7R0FDWixZQUFZO0dBQ1oseUJBQXlCO0dBQ3pCLGtCQUFrQjtHQUNsQixxQkFBcUI7R0FDckIscUJBQXFCO0dBQ3JCLGVBQWU7R0FDZixnQkFBZ0I7R0FDaEIsZUFBZTtDQUNqQjs7O0FBQ0E7SUFDRyxtQkFBbUI7SUFDbkIsMEJBQTBCO0lBQzFCLFlBQVk7SUFDWixZQUFZO0lBQ1oseUJBQXlCO0lBQ3pCLGtCQUFrQjtJQUNsQixxQkFBcUI7SUFDckIscUJBQXFCO0lBQ3JCLGVBQWU7SUFDZixnQkFBZ0I7SUFDaEIsZUFBZTtFQUNqQjs7O0FBR0E7S0FDRyxrQkFBa0I7S0FDbEIsZUFBZTtLQUNmLE9BQU87S0FDUCxTQUFTO0tBQ1QsV0FBVztLQUNYLDJCQUEyQjtLQUMzQixZQUFZO0tBQ1osa0JBQWtCO0VBQ3JCOzs7QUFFQTtJQUNFLGtCQUFrQjtJQUNsQixxQkFBcUI7RUFDdkI7OztBQUVBO0lBQ0UscUNBQTJEO0lBQzNELGVBQWU7SUFDZixXQUFXO0lBQ1gsWUFBWTtJQUNaLFlBQVk7RUFDZDs7O0FBQ0E7SUFDRSxrQkFBa0I7SUFDbEIscUJBQXFCO0VBQ3ZCOzs7QUFFQTtJQUNFLHlDQUErRDtJQUMvRCxlQUFlO0lBQ2YsV0FBVztJQUNYLFlBQVk7SUFDWixZQUFZO0VBQ2Q7OztBQUVBO0lBQ0Usa0JBQWtCO0lBQ2xCLHFCQUFxQjtFQUN2Qjs7O0FBRUE7SUFDRSxtREFBMkU7SUFDM0UsZUFBZTtJQUNmLFdBQVc7SUFDWCxZQUFZO0lBQ1osWUFBWTtFQUNkOzs7QUFFQTtJQUNFLGtCQUFrQjtJQUNsQixxQkFBcUI7RUFDdkI7OztBQUVBO0lBQ0UseUNBQStEO0lBQy9ELGVBQWU7SUFDZixXQUFXO0lBQ1gsWUFBWTtJQUNaLFlBQVk7RUFDZDs7O0FBQ0E7SUFDRSxrQkFBa0I7SUFDbEIsU0FBUztJQUNULFNBQVM7RUFDWDs7O0FBRUE7SUFDRSxrQkFBa0I7SUFDbEIsU0FBUztJQUNULFNBQVM7RUFDWDs7O0FBRUE7SUFDRSxrQkFBa0I7SUFDbEIsU0FBUztJQUNULFNBQVM7RUFDWDs7O0FBRUE7SUFDRSxrQkFBa0I7SUFDbEIsU0FBUztJQUNULFVBQVU7RUFDWjs7O0FBSUE7SUFDRSxXQUFXO0lBQ1gsWUFBWTtJQUNaLGlCQUFpQjtHQUNsQiwrQ0FBK0M7R0FDL0MsNEJBQTRCO0dBQzVCLHVCQUF1QjtFQUN4Qjs7O0FBRUE7SUFDRSxhQUFhO0lBQ2IsaUNBQWlDO0lBQ2pDLGNBQWM7SUFDZCx5QkFBeUI7SUFDekIsYUFBYTtFQUNmOzs7QUFFQTtJQUNFLDBDQUEwQztJQUMxQyxrQkFBa0I7SUFDbEIsZUFBZTtJQUNmLGVBQWU7RUFDakI7OztBQUdBO0lBQ0UsYUFBYTtJQUNiLDRCQUE0QjtJQUM1QixjQUFjO0lBQ2QseUJBQXlCO0lBQ3pCLGFBQWE7RUFDZjs7O0FBRUE7SUFDRSwwQ0FBMEM7SUFDMUMsa0JBQWtCO0lBQ2xCLGVBQWU7SUFDZixlQUFlO0VBQ2pCOzs7QUFFQTs7TUFFSSxVQUFVOzs7RUFHZDs7O0FBRUE7O0lBRUUsYUFBYTs7O0FBR2pCOzs7QUFHQTs7Ozs7RUFLRTs7O0FBRUY7Ozs7Ozs7O0NBUUM7OztBQUVDO0lBQ0Usd0RBQTZFO0lBQzdFLGVBQWU7SUFDZixXQUFXO0lBQ1gsWUFBWTtJQUNaLFlBQVk7RUFDZDs7O0FBRUE7SUFDRSxrQkFBa0I7SUFDbEIsU0FBUztJQUNULFNBQVM7RUFDWDs7O0FBQ0E7SUFDRSxrQkFBa0I7SUFDbEIscUJBQXFCO0VBQ3ZCOzs7QUFHRjtFQUNFLDBCQUEwQjtFQUMxQixrQkFBa0I7RUFDbEIsOEJBQThCO0VBQzlCLFlBQVk7RUFDWixZQUFZO0VBQ1osYUFBYTtFQUNiLDBDQUEwQyxFQUFFLFdBQVc7RUFDdkQsa0NBQWtDO0FBQ3BDOzs7QUFDQSxXQUFXOzs7QUFDWDtFQUNFLEtBQUssK0JBQStCLEVBQUU7RUFDdEMsT0FBTyxpQ0FBaUMsRUFBRTtBQUM1Qzs7O0FBRUE7RUFDRSxLQUFLLCtCQUF1QixFQUF2Qix1QkFBdUIsRUFBRTtFQUM5QixPQUFPLGlDQUF5QixFQUF6Qix5QkFBeUIsRUFBRTtBQUNwQyIsImZpbGUiOiJzcmMvYXBwL2luZm9zL2luZm9zLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjYnV0dG9uMSB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjYjJiMmIyXHQ7IFxyXG4gICAgYm9yZGVyOiBub25lO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgcGFkZGluZy1ib3R0b206IDI1cHggMjJweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIGZvbnQtc2l6ZTogMzZweDtcclxuICAgIG1hcmdpbjogNHB4IDE4cHg7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICBcclxuICB9XHJcblxyXG4gIFxyXG4jYnV0dG9uMiB7XHJcbiAgIGJhY2tncm91bmQtY29sb3I6ICNiMmIyYjJcdDsgXHJcbiAgIGJvcmRlcjogbm9uZTtcclxuICAgY29sb3I6IHdoaXRlO1xyXG4gICBwYWRkaW5nLWJvdHRvbTogMjVweCAyMnB4O1xyXG4gICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICBmb250LXNpemU6IDM2cHg7XHJcbiAgIG1hcmdpbjogNHB4IDE4cHg7XHJcbiAgIGN1cnNvcjogcG9pbnRlcjtcclxuIH1cclxuICNidXR0b24zIHtcclxuICAgYmFja2dyb3VuZC1jb2xvcjogI2IyYjJiMlx0OyBcclxuICAgYm9yZGVyOiBub25lO1xyXG4gICBjb2xvcjogd2hpdGU7XHJcbiAgIHBhZGRpbmctYm90dG9tOiAyNXB4IDIycHg7XHJcbiAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgIGZvbnQtc2l6ZTogMzZweDtcclxuICAgbWFyZ2luOiA0cHggMThweDtcclxuICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gfVxyXG5cclxuICNidXR0b240IHtcclxuICAgYmFja2dyb3VuZC1jb2xvcjogI2IyYjJiMlx0O1xyXG4gICBib3JkZXI6IG5vbmU7XHJcbiAgIGNvbG9yOiB3aGl0ZTtcclxuICAgcGFkZGluZy1ib3R0b206IDI1cHggMjJweDtcclxuICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgZm9udC1zaXplOiAzNnB4O1xyXG4gICBtYXJnaW46IDRweCAxOHB4O1xyXG4gICBjdXJzb3I6IHBvaW50ZXI7XHJcbiB9XHJcblxyXG5cclxuXHJcblxyXG4gI2J1dHRvbmEge1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNiMmIyYjJcdDsgXHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMjVweCAyMnB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgZm9udC1zaXplOiAzNnB4O1xyXG4gICAgbWFyZ2luOiA0cHggMThweDtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIFxyXG4gIH1cclxuXHJcbiAgXHJcbiNidXR0b25iIHtcclxuICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbiAgIGJhY2tncm91bmQtY29sb3I6ICNiMmIyYjJcdDsgXHJcbiAgIGJvcmRlcjogbm9uZTtcclxuICAgY29sb3I6IHdoaXRlO1xyXG4gICBwYWRkaW5nLWJvdHRvbTogMjVweCAyMnB4O1xyXG4gICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICBmb250LXNpemU6IDM2cHg7XHJcbiAgIG1hcmdpbjogNHB4IDE4cHg7XHJcbiAgIGN1cnNvcjogcG9pbnRlcjtcclxuIH1cclxuICNidXR0b25jIHtcclxuICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbiAgIGJhY2tncm91bmQtY29sb3I6ICNiMmIyYjJcdDsgXHJcbiAgIGJvcmRlcjogbm9uZTtcclxuICAgY29sb3I6IHdoaXRlO1xyXG4gICBwYWRkaW5nLWJvdHRvbTogMjVweCAyMnB4O1xyXG4gICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICBmb250LXNpemU6IDM2cHg7XHJcbiAgIG1hcmdpbjogNHB4IDE4cHg7XHJcbiAgIGN1cnNvcjogcG9pbnRlcjtcclxuIH1cclxuXHJcbiAjYnV0dG9uZCB7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xyXG4gICBiYWNrZ3JvdW5kLWNvbG9yOiAjYjJiMmIyXHQ7XHJcbiAgIGJvcmRlcjogbm9uZTtcclxuICAgY29sb3I6IHdoaXRlO1xyXG4gICBwYWRkaW5nLWJvdHRvbTogMjVweCAyMnB4O1xyXG4gICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICBmb250LXNpemU6IDM2cHg7XHJcbiAgIG1hcmdpbjogNHB4IDE4cHg7XHJcbiAgIGN1cnNvcjogcG9pbnRlcjtcclxuIH1cclxuICNidXR0b25lIHtcclxuICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjYjJiMmIyXHQ7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMjVweCAyMnB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgZm9udC1zaXplOiAzNnB4O1xyXG4gICAgbWFyZ2luOiA0cHggMThweDtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICB9XHJcbiAgXHJcbiBcclxuICAjYWxse1xyXG4gICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgICAgbGVmdDogMDtcclxuICAgICBib3R0b206IDA7XHJcbiAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgYmFja2dyb3VuZC1jb2xvcjogXHRcdCNiMmIyYjI7XHJcbiAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICB9XHJcblxyXG4gIGRpdi5ob21lIHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICB9XHJcblxyXG4gIGRpdi5ob21lIGlucHV0IHtcclxuICAgIGJhY2tncm91bmQ6IHVybCgnc3JjL2FwcC9oZWFkZXIvaW1hZ2VzL2hvbWUuc3ZnJykgbm8tcmVwZWF0O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgd2lkdGg6IDUwcHg7XHJcbiAgICBoZWlnaHQ6IDUwcHg7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgfVxyXG4gIGRpdi5hbmFseXRpYyB7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgfVxyXG5cclxuICBkaXYuYW5hbHl0aWMgaW5wdXQge1xyXG4gICAgYmFja2dyb3VuZDogdXJsKCdzcmMvYXBwL2hlYWRlci9pbWFnZXMvYW5hbHl0aWMuc3ZnJykgbm8tcmVwZWF0O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgd2lkdGg6IDUwcHg7XHJcbiAgICBoZWlnaHQ6IDUwcHg7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgfVxyXG5cclxuICBkaXYubm90aWZpY2F0aW9uIHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICB9XHJcblxyXG4gIGRpdi5ub3RpZmljYXRpb24gaW5wdXQge1xyXG4gICAgYmFja2dyb3VuZDogdXJsKCdzcmMvYXBwL2NhbGVuZGFyL2ltYWdlcy9vdXRsaW5lLXRvZGF5LTI0cHguc3ZnJykgbm8tcmVwZWF0O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgd2lkdGg6IDUwcHg7XHJcbiAgICBoZWlnaHQ6IDUwcHg7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgfVxyXG4gIFxyXG4gIGRpdi5zZXR0aW5nIHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICB9XHJcblxyXG4gIGRpdi5zZXR0aW5nIGlucHV0IHtcclxuICAgIGJhY2tncm91bmQ6IHVybCgnc3JjL2FwcC9oZWFkZXIvaW1hZ2VzL3NldHRpbmdzLnN2ZycpIG5vLXJlcGVhdDtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIHdpZHRoOiA1MHB4O1xyXG4gICAgaGVpZ2h0OiA1MHB4O1xyXG4gICAgYm9yZGVyOiBub25lO1xyXG4gIH1cclxuICBzcGFuLmhvbWUge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAzMnB4O1xyXG4gICAgbGVmdDogNnB4O1xyXG4gIH1cclxuICBcclxuICBzcGFuLmFuYWx5dGljIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogMzJweDtcclxuICAgIGxlZnQ6IDJweDtcclxuICB9XHJcblxyXG4gIHNwYW4ubm90aWZpY2F0aW9uIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogMzJweDtcclxuICAgIGxlZnQ6IDJweDtcclxuICB9XHJcblxyXG4gIHNwYW4uc2V0dGluZyB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDMycHg7XHJcbiAgICBsZWZ0OiAxMnB4O1xyXG4gIH1cclxuXHJcblxyXG4gIFxyXG4gIGltZyB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogYXV0bztcclxuICAgIG1heC1oZWlnaHQ6IDQwMHB4O1xyXG4gICAvKiBiYWNrZ3JvdW5kOiB1cmwobG9hZGluZy5naWYpIDUwJSBuby1yZXBlYXQ7Ki9cclxuICAgLyogYm9yZGVyOiAxcHggc29saWQgYmxhY2s7Ki9cclxuICAgLyogYm9yZGVyLXJhZGl1czogNXB4OyovXHJcbiAgfVxyXG5cclxuICAuZ3JpZC1jb250YWluZXIge1xyXG4gICAgZGlzcGxheTogZ3JpZDtcclxuICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogYXV0byBhdXRvIDtcclxuICAgIGdyaWQtZ2FwOiAxMHB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgfVxyXG4gIFxyXG4gIC5ncmlkLWNvbnRhaW5lciA+IGRpdiB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuOCk7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBwYWRkaW5nOiAyMHB4IDE7XHJcbiAgICBmb250LXNpemU6IDMwcHg7XHJcbiAgfVxyXG5cclxuXHJcbiAgLmdyaWQtY29udGFpbmVyLWxhc3Qge1xyXG4gICAgZGlzcGxheTogZ3JpZDtcclxuICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogYXV0byA7XHJcbiAgICBncmlkLWdhcDogMTBweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNGRkZGRkY7XHJcbiAgICBwYWRkaW5nOiAxMHB4O1xyXG4gIH1cclxuICBcclxuICAuZ3JpZC1jb250YWluZXItbGFzdCA+IGRpdiB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuOCk7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBwYWRkaW5nOiAyMHB4IDE7XHJcbiAgICBmb250LXNpemU6IDMwcHg7XHJcbiAgfVxyXG5cclxuICAjaGVhZGVye1xyXG4gICAgIFxyXG4gICAgICBjb2xvcjpncmV5O1xyXG5cclxuXHJcbiAgfVxyXG4gIFxyXG4gICNzdWJoZWFkZXJ7XHJcbiAgIFxyXG4gICAgY29sb3I6IzAwZmY0MDtcclxuXHJcblxyXG59XHJcblxyXG5cclxuLypcclxuICAjYnVsbGV0c3tcclxuICBcclxuICAgXHJcblxyXG59Ki9cclxuXHJcbi8qICAjdGhpcmR7XHJcbiAgICB0ZXh0LWFsaWduOiBsZWZ0XHJcblxyXG4gIH1cclxuXHJcbiAgI2ZpZnRoe1xyXG4gICAgdGV4dC1hbGlnbjogbGVmdFxyXG4gIH1cclxuKi9cclxuXHJcbiAgZGl2LmJ1bGxldDEgaW5wdXQge1xyXG4gICAgYmFja2dyb3VuZDogdXJsKCdzcmMvYXBwL2luZm9zL2ltYWdlcy9yb3VuZC1icmlnaHRuZXNzXzEtMjRweC5zdmcnKSBuby1yZXBlYXQ7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICB3aWR0aDogNTBweDtcclxuICAgIGhlaWdodDogNTBweDtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICB9XHJcblxyXG4gIHNwYW4uYnVsbGV0MSB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDMycHg7XHJcbiAgICBsZWZ0OiAycHg7XHJcbiAgfVxyXG4gIGRpdi5idWxsZXQxIHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICB9XHJcblxyXG4gIFxyXG4ubG9hZGVyIHtcclxuICBib3JkZXI6IDE2cHggc29saWQgI2YzZjNmMztcclxuICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgYm9yZGVyLXRvcDogMTZweCBzb2xpZCAjZmY4MDAwO1xyXG4gIG1hcmdpbjogYXV0bztcclxuICB3aWR0aDogMTIwcHg7XHJcbiAgaGVpZ2h0OiAxMjBweDtcclxuICAtd2Via2l0LWFuaW1hdGlvbjogc3BpbiAycyBsaW5lYXIgaW5maW5pdGU7IC8qIFNhZmFyaSAqL1xyXG4gIGFuaW1hdGlvbjogc3BpbiAycyBsaW5lYXIgaW5maW5pdGU7XHJcbn1cclxuLyogU2FmYXJpICovXHJcbkAtd2Via2l0LWtleWZyYW1lcyBzcGluIHtcclxuICAwJSB7IC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7IH1cclxuICAxMDAlIHsgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgzNjBkZWcpOyB9XHJcbn1cclxuXHJcbkBrZXlmcmFtZXMgc3BpbiB7XHJcbiAgMCUgeyB0cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKTsgfVxyXG4gIDEwMCUgeyB0cmFuc2Zvcm06IHJvdGF0ZSgzNjBkZWcpOyB9XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/infos/infos.component.html":
/*!********************************************!*\
  !*** ./src/app/infos/infos.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<link  href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css\"  rel=\"stylesheet\">\n<div id=\"maincontenthome\">\n\n    <img >\n    \n\n  <!--  <div class=\"container\">\n        <div class=\"row\">\n          <div class=\"col-sm\">\n            1/3\n          </div>\n          <div class=\"col-sm\">\n            1/3\n          </div>\n         \n        </div>\n    </div>\n  -->\n  <div class=\"container\" >\n     \n      \n      <!--<div id=\"allbars\" class=\"col-md-12\">\n      <div id=\"baralls\">\n          <div  align=\"center\"   id=\"loading\"> Loading...</div>\n           <div id=\"myProgress\">\n               \n               <div id=\"myBar\"></div>\n            </div>\n            <div id=\"loader\" align=\"center\" class=\"loader\"></div>\n        </div>\n      </div>-->\n      <h3 id=\"headmaster\"></h3>\n          <div class=\"grid-container\">\n\n\n            \n              <div  id=\"header\"  >Calories per meal</div>\n              <div id=\"subheader\"></div>\n              <div id=\"bullets\"> \n\n                 <div class=\"bullet1\">\n                  <span id=\"bullet1\" class=\"bullet1\">  </span>\n                   <input id=\"bullet1\" type=\"button\" width=\"48\" height=\"48\" >\n                 </div>\n\n              </div>  \n              <div id=\"bulletsway\"></div>\n              <div id=\"bullets\">\n                  <div class=\"bullet1\">\n                      <span id=\"bullet1\" class=\"bullet1\">  </span>\n                       <input id=\"bullet1\" type=\"button\" width=\"48\" height=\"48\" >\n                  </div>\n\n\n\n              </div>  \n              <div id=\"bulletsnut\"></div>\n            \n            \n          </div>\n      \n      \n          <div class=\"grid-container-last\">\n              <h3>Description</h3>\n              <div  id=\"bulletsdesc\"></div>\n            \n          </div>\n          \n      \n  </div>\n\n   \n\n</div>\n\n\n<div id=\"all\">\n\n  \n    <div class=\"home\">\n        <span id=\"home\" class=\"home\">Home  </span>\n        <input id=\"button1\" type=\"button\"  (click)=\"submitted=false\" (click)=\"goToPage('/', 'main')\">\n      \n    </div>\n\n     <div class=\"analytic\">\n      <span id=\"analytic\" class=\"analytic\">Analytics  </span>\n      <input id=\"button2\" type=\"button\" alt=\"Submit\" width=\"48\" height=\"48\" (click)=\"submitted=false\" (click)=\"goToPage('/', 'analytic')\">\n  \n    </div>\n\n    <div class=\"notification\">\n        <span id=\"notification\" class=\"notification\">Calendar  </span>\n        <input id=\"button3\" type=\"button\" alt=\"Submit\" width=\"48\" height=\"48\" (click)=\"submitted=false\" (click)=\"goToPage('/', 'calendar')\">\n    \n      </div>\n    \n      <div class=\"setting\">\n          <span id=\"setting\" class=\"setting\">Settings  </span>\n          <input id=\"button4\" type=\"button\" alt=\"Submit\" width=\"48\" height=\"48\" (click)=\"submitted=false\" (click)=\"goToPage('/', 'analytic')\">\n      \n        </div>\n   \n     \n \n</div>"

/***/ }),

/***/ "./src/app/infos/infos.component.ts":
/*!******************************************!*\
  !*** ./src/app/infos/infos.component.ts ***!
  \******************************************/
/*! exports provided: InfosComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InfosComponent", function() { return InfosComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_4__);





var InfosComponent = /** @class */ (function () {
    function InfosComponent(http, router) {
        this.http = http;
        this.router = router;
    }
    InfosComponent.prototype.ngOnInit = function () {
        jquery__WEBPACK_IMPORTED_MODULE_4__("#baralls").show();
        // var i=location.host.para
        var c = location.pathname.toString();
        // alert("pathname is"+c);
        var param = c.split('&name%3D')[1];
        //var param2=c.split('&day%3D')[1]
        // alert("xsxs are"+param);
        // alert("Parameter is"+param);
        // downloadingImage.src = "http://an.image/to/aynchrounously/download.jpg";
        function delay(ms) {
            return new Promise(function (resolve) { return setTimeout(resolve, ms); });
        }
        jquery__WEBPACK_IMPORTED_MODULE_4__("#date").hide();
        jquery__WEBPACK_IMPORTED_MODULE_4__("#session").hide();
        jquery__WEBPACK_IMPORTED_MODULE_4__("#back").show();
        jquery__WEBPACK_IMPORTED_MODULE_4__("#maincontenthome").hide();
        jquery__WEBPACK_IMPORTED_MODULE_4__("#baralls").show();
        //alert("cabled");
        this.setInfos(param);
    };
    InfosComponent.prototype.setInfos = function (meal) {
        // alert("Set infos called");
        var name = sessionStorage.getItem("lastname");
        this.http.get("http://localhost:3000/api/things/getcalendarinfo/" + name + "/" + meal, { responseType: "json" }).subscribe(function (response) {
            //  $("#suball").show();
            // this.data = response;
            // console.log("data :"+response);
            var sample = JSON.stringify(response);
            var res = JSON.parse(sample);
            //alert(res[0].name);
            // alert(res[0].wayofcooking);
            document.getElementById("headmaster").textContent = res[0].name;
            document.getElementById("bulletsdesc").textContent = res[0].info;
            document.getElementById("subheader").textContent = res[0].calories;
            //  document.getElementById("bulletsdesc").textContent=res[0].info
            document.getElementById("bulletsway").textContent = res[0].wayofcooking;
            document.getElementById("bulletsnut").textContent = res[0].nutrition;
            // headmaster
            var image = document.images[0];
            var downloadingImage = new Image();
            image.src = res[0].imageurl;
            jquery__WEBPACK_IMPORTED_MODULE_4__("#maincontenthome").show();
            //$("#baralls").hide();
        });
    };
    InfosComponent.prototype.goToPage = function (pagename, parameter) {
        this.router.navigate([pagename, parameter]);
    };
    InfosComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-infos',
            template: __webpack_require__(/*! ./infos.component.html */ "./src/app/infos/infos.component.html"),
            styles: [__webpack_require__(/*! ./infos.component.css */ "./src/app/infos/infos.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], InfosComponent);
    return InfosComponent;
}());



/***/ }),

/***/ "./src/app/notification/notification.component.css":
/*!*********************************************************!*\
  !*** ./src/app/notification/notification.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#button1 {\r\n    background-color: #b2b2b2\t; \r\n    border: none;\r\n    color: white;\r\n    padding-bottom: 25px 22px;\r\n    text-align: center;\r\n    text-decoration: none;\r\n    display: inline-block;\r\n    font-size: 36px;\r\n    margin: 4px 18px;\r\n    cursor: pointer;\r\n    \r\n  }\r\n\r\n  \r\n#button2 {\r\n   background-color: #b2b2b2\t; \r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n\r\n  \r\n#button3 {\r\n   background-color: #b2b2b2\t; \r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n\r\n  \r\n#button4 {\r\n   background-color: #b2b2b2\t;\r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n\r\n  \r\n#buttona {\r\n    border-radius: 25px;\r\n    background-color: #b2b2b2\t; \r\n    border: none;\r\n    color: white;\r\n    padding-bottom: 25px 22px;\r\n    text-align: center;\r\n    text-decoration: none;\r\n    display: inline-block;\r\n    font-size: 36px;\r\n    margin: 4px 18px;\r\n    cursor: pointer;\r\n    \r\n  }\r\n\r\n  \r\n#buttonb {\r\n    border-radius: 25px;\r\n   background-color: #b2b2b2\t; \r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n\r\n  \r\n#buttonc {\r\n    border-radius: 25px;\r\n   background-color: #b2b2b2\t; \r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n\r\n  \r\n#buttond {\r\n    border-radius: 25px;\r\n   background-color: #b2b2b2\t;\r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n\r\n  \r\n#buttone {\r\n    border-radius: 25px;\r\n    background-color: #b2b2b2\t;\r\n    border: none;\r\n    color: white;\r\n    padding-bottom: 25px 22px;\r\n    text-align: center;\r\n    text-decoration: none;\r\n    display: inline-block;\r\n    font-size: 36px;\r\n    margin: 4px 18px;\r\n    cursor: pointer;\r\n  }\r\n\r\n  \r\n#all{\r\n     text-align: center;\r\n     position: fixed;\r\n     left: 0;\r\n     bottom: 0;\r\n     width: 100%;\r\n     background-color: \t\t#b2b2b2;\r\n     color: white;\r\n     text-align: center;\r\n  }\r\n\r\n  \r\ndiv.home {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\n\r\n  \r\ndiv.home input {\r\n    background: url('home.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\n\r\n  \r\ndiv.analytic {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\n\r\n  \r\ndiv.analytic input {\r\n    background: url('analytic.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\n\r\n  \r\ndiv.notification {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\n\r\n  \r\ndiv.notification input {\r\n    background: url('outline-today-24px.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\n\r\n  \r\ndiv.setting {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\n\r\n  \r\ndiv.setting input {\r\n    background: url('settings.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\n\r\n  \r\nspan.home {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 6px;\r\n  }\r\n\r\n  \r\nspan.analytic {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 2px;\r\n  }\r\n\r\n  \r\nspan.notification {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 2px;\r\n  }\r\n\r\n  \r\nspan.setting {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 12px;\r\n  }\r\n\r\n  \r\n#buttonround1 {\r\n    border-radius: 25px;\r\n    background: #cccccc;\r\n    padding: 5px; \r\n    width: 100px;\r\n    height: 50px;  \r\n  }\r\n\r\n  \r\n#rcorners1 {\r\n    border-radius: 25px;\r\n    background: #e7e0e0;\r\n    padding: 20px; \r\n    width: 100%;\r\n    height: auto;  \r\n  }\r\n\r\n  \r\n#tableid {\r\n    border-radius: 25px;\r\n    background: #FFFFFF;\r\n    padding: 20px; \r\n   \r\n    width: 100%;\r\n    height: auto;  \r\n  }\r\n\r\n  \r\n#myProgress {\r\n    width: 100%;\r\n    background-color: #ddd;\r\n  }\r\n\r\n  \r\n#myBar {\r\n    width: 1%;\r\n    height: 30px;\r\n    background-color: #ff8000;\r\n  }\r\n\r\n  \r\n#barall {\r\n  padding-top:130px;\r\n \r\n}\r\n\r\n  \r\n#menu {\r\n  padding-bottom:0px;\r\n \r\n}\r\n\r\n  \r\n.loader {\r\n  border: 16px solid #f3f3f3;\r\n  border-radius: 50%;\r\n  border-top: 16px solid #ff8000;\r\n  margin: auto;\r\n  width: 120px;\r\n  height: 120px;\r\n  -webkit-animation: spin 2s linear infinite; /* Safari */\r\n  animation: spin 2s linear infinite;\r\n}\r\n\r\n  \r\n/* Safari */\r\n\r\n  \r\n@-webkit-keyframes spin {\r\n  0% { -webkit-transform: rotate(0deg); }\r\n  100% { -webkit-transform: rotate(360deg); }\r\n}\r\n\r\n  \r\n@keyframes spin {\r\n  0% { -webkit-transform: rotate(0deg); transform: rotate(0deg); }\r\n  100% { -webkit-transform: rotate(360deg); transform: rotate(360deg); }\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksMEJBQTBCO0lBQzFCLFlBQVk7SUFDWixZQUFZO0lBQ1oseUJBQXlCO0lBQ3pCLGtCQUFrQjtJQUNsQixxQkFBcUI7SUFDckIscUJBQXFCO0lBQ3JCLGVBQWU7SUFDZixnQkFBZ0I7SUFDaEIsZUFBZTs7RUFFakI7OztBQUdGO0dBQ0csMEJBQTBCO0dBQzFCLFlBQVk7R0FDWixZQUFZO0dBQ1oseUJBQXlCO0dBQ3pCLGtCQUFrQjtHQUNsQixxQkFBcUI7R0FDckIscUJBQXFCO0dBQ3JCLGVBQWU7R0FDZixnQkFBZ0I7R0FDaEIsZUFBZTtDQUNqQjs7O0FBQ0E7R0FDRSwwQkFBMEI7R0FDMUIsWUFBWTtHQUNaLFlBQVk7R0FDWix5QkFBeUI7R0FDekIsa0JBQWtCO0dBQ2xCLHFCQUFxQjtHQUNyQixxQkFBcUI7R0FDckIsZUFBZTtHQUNmLGdCQUFnQjtHQUNoQixlQUFlO0NBQ2pCOzs7QUFFQTtHQUNFLDBCQUEwQjtHQUMxQixZQUFZO0dBQ1osWUFBWTtHQUNaLHlCQUF5QjtHQUN6QixrQkFBa0I7R0FDbEIscUJBQXFCO0dBQ3JCLHFCQUFxQjtHQUNyQixlQUFlO0dBQ2YsZ0JBQWdCO0dBQ2hCLGVBQWU7Q0FDakI7OztBQUtBO0lBQ0csbUJBQW1CO0lBQ25CLDBCQUEwQjtJQUMxQixZQUFZO0lBQ1osWUFBWTtJQUNaLHlCQUF5QjtJQUN6QixrQkFBa0I7SUFDbEIscUJBQXFCO0lBQ3JCLHFCQUFxQjtJQUNyQixlQUFlO0lBQ2YsZ0JBQWdCO0lBQ2hCLGVBQWU7O0VBRWpCOzs7QUFHRjtJQUNJLG1CQUFtQjtHQUNwQiwwQkFBMEI7R0FDMUIsWUFBWTtHQUNaLFlBQVk7R0FDWix5QkFBeUI7R0FDekIsa0JBQWtCO0dBQ2xCLHFCQUFxQjtHQUNyQixxQkFBcUI7R0FDckIsZUFBZTtHQUNmLGdCQUFnQjtHQUNoQixlQUFlO0NBQ2pCOzs7QUFDQTtJQUNHLG1CQUFtQjtHQUNwQiwwQkFBMEI7R0FDMUIsWUFBWTtHQUNaLFlBQVk7R0FDWix5QkFBeUI7R0FDekIsa0JBQWtCO0dBQ2xCLHFCQUFxQjtHQUNyQixxQkFBcUI7R0FDckIsZUFBZTtHQUNmLGdCQUFnQjtHQUNoQixlQUFlO0NBQ2pCOzs7QUFFQTtJQUNHLG1CQUFtQjtHQUNwQiwwQkFBMEI7R0FDMUIsWUFBWTtHQUNaLFlBQVk7R0FDWix5QkFBeUI7R0FDekIsa0JBQWtCO0dBQ2xCLHFCQUFxQjtHQUNyQixxQkFBcUI7R0FDckIsZUFBZTtHQUNmLGdCQUFnQjtHQUNoQixlQUFlO0NBQ2pCOzs7QUFDQTtJQUNHLG1CQUFtQjtJQUNuQiwwQkFBMEI7SUFDMUIsWUFBWTtJQUNaLFlBQVk7SUFDWix5QkFBeUI7SUFDekIsa0JBQWtCO0lBQ2xCLHFCQUFxQjtJQUNyQixxQkFBcUI7SUFDckIsZUFBZTtJQUNmLGdCQUFnQjtJQUNoQixlQUFlO0VBQ2pCOzs7QUFHQTtLQUNHLGtCQUFrQjtLQUNsQixlQUFlO0tBQ2YsT0FBTztLQUNQLFNBQVM7S0FDVCxXQUFXO0tBQ1gsMkJBQTJCO0tBQzNCLFlBQVk7S0FDWixrQkFBa0I7RUFDckI7OztBQUVBO0lBQ0Usa0JBQWtCO0lBQ2xCLHFCQUFxQjtFQUN2Qjs7O0FBRUE7SUFDRSxxQ0FBMkQ7SUFDM0QsZUFBZTtJQUNmLFdBQVc7SUFDWCxZQUFZO0lBQ1osWUFBWTtFQUNkOzs7QUFDQTtJQUNFLGtCQUFrQjtJQUNsQixxQkFBcUI7RUFDdkI7OztBQUVBO0lBQ0UseUNBQStEO0lBQy9ELGVBQWU7SUFDZixXQUFXO0lBQ1gsWUFBWTtJQUNaLFlBQVk7RUFDZDs7O0FBRUE7SUFDRSxrQkFBa0I7SUFDbEIscUJBQXFCO0VBQ3ZCOzs7QUFFQTtJQUNFLG1EQUEyRTtJQUMzRSxlQUFlO0lBQ2YsV0FBVztJQUNYLFlBQVk7SUFDWixZQUFZO0VBQ2Q7OztBQUVBO0lBQ0Usa0JBQWtCO0lBQ2xCLHFCQUFxQjtFQUN2Qjs7O0FBRUE7SUFDRSx5Q0FBK0Q7SUFDL0QsZUFBZTtJQUNmLFdBQVc7SUFDWCxZQUFZO0lBQ1osWUFBWTtFQUNkOzs7QUFDQTtJQUNFLGtCQUFrQjtJQUNsQixTQUFTO0lBQ1QsU0FBUztFQUNYOzs7QUFFQTtJQUNFLGtCQUFrQjtJQUNsQixTQUFTO0lBQ1QsU0FBUztFQUNYOzs7QUFFQTtJQUNFLGtCQUFrQjtJQUNsQixTQUFTO0lBQ1QsU0FBUztFQUNYOzs7QUFFQTtJQUNFLGtCQUFrQjtJQUNsQixTQUFTO0lBQ1QsVUFBVTtFQUNaOzs7QUFFQTtJQUNFLG1CQUFtQjtJQUNuQixtQkFBbUI7SUFDbkIsWUFBWTtJQUNaLFlBQVk7SUFDWixZQUFZO0VBQ2Q7OztBQUVBO0lBQ0UsbUJBQW1CO0lBQ25CLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsV0FBVztJQUNYLFlBQVk7RUFDZDs7O0FBRUE7SUFDRSxtQkFBbUI7SUFDbkIsbUJBQW1CO0lBQ25CLGFBQWE7O0lBRWIsV0FBVztJQUNYLFlBQVk7RUFDZDs7O0FBSUY7SUFDSSxXQUFXO0lBQ1gsc0JBQXNCO0VBQ3hCOzs7QUFFRjtJQUNJLFNBQVM7SUFDVCxZQUFZO0lBQ1oseUJBQXlCO0VBQzNCOzs7QUFHRjtFQUNFLGlCQUFpQjs7QUFFbkI7OztBQUVBO0VBQ0Usa0JBQWtCOztBQUVwQjs7O0FBQ0E7RUFDRSwwQkFBMEI7RUFDMUIsa0JBQWtCO0VBQ2xCLDhCQUE4QjtFQUM5QixZQUFZO0VBQ1osWUFBWTtFQUNaLGFBQWE7RUFDYiwwQ0FBMEMsRUFBRSxXQUFXO0VBQ3ZELGtDQUFrQztBQUNwQzs7O0FBRUEsV0FBVzs7O0FBQ1g7RUFDRSxLQUFLLCtCQUErQixFQUFFO0VBQ3RDLE9BQU8saUNBQWlDLEVBQUU7QUFDNUM7OztBQUVBO0VBQ0UsS0FBSywrQkFBdUIsRUFBdkIsdUJBQXVCLEVBQUU7RUFDOUIsT0FBTyxpQ0FBeUIsRUFBekIseUJBQXlCLEVBQUU7QUFDcEMiLCJmaWxlIjoic3JjL2FwcC9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjYnV0dG9uMSB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjYjJiMmIyXHQ7IFxyXG4gICAgYm9yZGVyOiBub25lO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgcGFkZGluZy1ib3R0b206IDI1cHggMjJweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIGZvbnQtc2l6ZTogMzZweDtcclxuICAgIG1hcmdpbjogNHB4IDE4cHg7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICBcclxuICB9XHJcblxyXG4gIFxyXG4jYnV0dG9uMiB7XHJcbiAgIGJhY2tncm91bmQtY29sb3I6ICNiMmIyYjJcdDsgXHJcbiAgIGJvcmRlcjogbm9uZTtcclxuICAgY29sb3I6IHdoaXRlO1xyXG4gICBwYWRkaW5nLWJvdHRvbTogMjVweCAyMnB4O1xyXG4gICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICBmb250LXNpemU6IDM2cHg7XHJcbiAgIG1hcmdpbjogNHB4IDE4cHg7XHJcbiAgIGN1cnNvcjogcG9pbnRlcjtcclxuIH1cclxuICNidXR0b24zIHtcclxuICAgYmFja2dyb3VuZC1jb2xvcjogI2IyYjJiMlx0OyBcclxuICAgYm9yZGVyOiBub25lO1xyXG4gICBjb2xvcjogd2hpdGU7XHJcbiAgIHBhZGRpbmctYm90dG9tOiAyNXB4IDIycHg7XHJcbiAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgIGZvbnQtc2l6ZTogMzZweDtcclxuICAgbWFyZ2luOiA0cHggMThweDtcclxuICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gfVxyXG5cclxuICNidXR0b240IHtcclxuICAgYmFja2dyb3VuZC1jb2xvcjogI2IyYjJiMlx0O1xyXG4gICBib3JkZXI6IG5vbmU7XHJcbiAgIGNvbG9yOiB3aGl0ZTtcclxuICAgcGFkZGluZy1ib3R0b206IDI1cHggMjJweDtcclxuICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgZm9udC1zaXplOiAzNnB4O1xyXG4gICBtYXJnaW46IDRweCAxOHB4O1xyXG4gICBjdXJzb3I6IHBvaW50ZXI7XHJcbiB9XHJcblxyXG5cclxuXHJcblxyXG4gI2J1dHRvbmEge1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNiMmIyYjJcdDsgXHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMjVweCAyMnB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgZm9udC1zaXplOiAzNnB4O1xyXG4gICAgbWFyZ2luOiA0cHggMThweDtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIFxyXG4gIH1cclxuXHJcbiAgXHJcbiNidXR0b25iIHtcclxuICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbiAgIGJhY2tncm91bmQtY29sb3I6ICNiMmIyYjJcdDsgXHJcbiAgIGJvcmRlcjogbm9uZTtcclxuICAgY29sb3I6IHdoaXRlO1xyXG4gICBwYWRkaW5nLWJvdHRvbTogMjVweCAyMnB4O1xyXG4gICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICBmb250LXNpemU6IDM2cHg7XHJcbiAgIG1hcmdpbjogNHB4IDE4cHg7XHJcbiAgIGN1cnNvcjogcG9pbnRlcjtcclxuIH1cclxuICNidXR0b25jIHtcclxuICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbiAgIGJhY2tncm91bmQtY29sb3I6ICNiMmIyYjJcdDsgXHJcbiAgIGJvcmRlcjogbm9uZTtcclxuICAgY29sb3I6IHdoaXRlO1xyXG4gICBwYWRkaW5nLWJvdHRvbTogMjVweCAyMnB4O1xyXG4gICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICBmb250LXNpemU6IDM2cHg7XHJcbiAgIG1hcmdpbjogNHB4IDE4cHg7XHJcbiAgIGN1cnNvcjogcG9pbnRlcjtcclxuIH1cclxuXHJcbiAjYnV0dG9uZCB7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xyXG4gICBiYWNrZ3JvdW5kLWNvbG9yOiAjYjJiMmIyXHQ7XHJcbiAgIGJvcmRlcjogbm9uZTtcclxuICAgY29sb3I6IHdoaXRlO1xyXG4gICBwYWRkaW5nLWJvdHRvbTogMjVweCAyMnB4O1xyXG4gICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICBmb250LXNpemU6IDM2cHg7XHJcbiAgIG1hcmdpbjogNHB4IDE4cHg7XHJcbiAgIGN1cnNvcjogcG9pbnRlcjtcclxuIH1cclxuICNidXR0b25lIHtcclxuICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjYjJiMmIyXHQ7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMjVweCAyMnB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgZm9udC1zaXplOiAzNnB4O1xyXG4gICAgbWFyZ2luOiA0cHggMThweDtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICB9XHJcbiAgXHJcbiBcclxuICAjYWxse1xyXG4gICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgICAgbGVmdDogMDtcclxuICAgICBib3R0b206IDA7XHJcbiAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgYmFja2dyb3VuZC1jb2xvcjogXHRcdCNiMmIyYjI7XHJcbiAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICB9XHJcblxyXG4gIGRpdi5ob21lIHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICB9XHJcblxyXG4gIGRpdi5ob21lIGlucHV0IHtcclxuICAgIGJhY2tncm91bmQ6IHVybCgnc3JjL2FwcC9oZWFkZXIvaW1hZ2VzL2hvbWUuc3ZnJykgbm8tcmVwZWF0O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgd2lkdGg6IDUwcHg7XHJcbiAgICBoZWlnaHQ6IDUwcHg7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgfVxyXG4gIGRpdi5hbmFseXRpYyB7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgfVxyXG5cclxuICBkaXYuYW5hbHl0aWMgaW5wdXQge1xyXG4gICAgYmFja2dyb3VuZDogdXJsKCdzcmMvYXBwL2hlYWRlci9pbWFnZXMvYW5hbHl0aWMuc3ZnJykgbm8tcmVwZWF0O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgd2lkdGg6IDUwcHg7XHJcbiAgICBoZWlnaHQ6IDUwcHg7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgfVxyXG5cclxuICBkaXYubm90aWZpY2F0aW9uIHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICB9XHJcblxyXG4gIGRpdi5ub3RpZmljYXRpb24gaW5wdXQge1xyXG4gICAgYmFja2dyb3VuZDogdXJsKCdzcmMvYXBwL2NhbGVuZGFyL2ltYWdlcy9vdXRsaW5lLXRvZGF5LTI0cHguc3ZnJykgbm8tcmVwZWF0O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgd2lkdGg6IDUwcHg7XHJcbiAgICBoZWlnaHQ6IDUwcHg7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgfVxyXG4gIFxyXG4gIGRpdi5zZXR0aW5nIHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICB9XHJcblxyXG4gIGRpdi5zZXR0aW5nIGlucHV0IHtcclxuICAgIGJhY2tncm91bmQ6IHVybCgnc3JjL2FwcC9oZWFkZXIvaW1hZ2VzL3NldHRpbmdzLnN2ZycpIG5vLXJlcGVhdDtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIHdpZHRoOiA1MHB4O1xyXG4gICAgaGVpZ2h0OiA1MHB4O1xyXG4gICAgYm9yZGVyOiBub25lO1xyXG4gIH1cclxuICBzcGFuLmhvbWUge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAzMnB4O1xyXG4gICAgbGVmdDogNnB4O1xyXG4gIH1cclxuICBcclxuICBzcGFuLmFuYWx5dGljIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogMzJweDtcclxuICAgIGxlZnQ6IDJweDtcclxuICB9XHJcblxyXG4gIHNwYW4ubm90aWZpY2F0aW9uIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogMzJweDtcclxuICAgIGxlZnQ6IDJweDtcclxuICB9XHJcblxyXG4gIHNwYW4uc2V0dGluZyB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDMycHg7XHJcbiAgICBsZWZ0OiAxMnB4O1xyXG4gIH1cclxuXHJcbiAgI2J1dHRvbnJvdW5kMSB7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xyXG4gICAgYmFja2dyb3VuZDogI2NjY2NjYztcclxuICAgIHBhZGRpbmc6IDVweDsgXHJcbiAgICB3aWR0aDogMTAwcHg7XHJcbiAgICBoZWlnaHQ6IDUwcHg7ICBcclxuICB9XHJcblxyXG4gICNyY29ybmVyczEge1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcclxuICAgIGJhY2tncm91bmQ6ICNlN2UwZTA7XHJcbiAgICBwYWRkaW5nOiAyMHB4OyBcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiBhdXRvOyAgXHJcbiAgfVxyXG4gIFxyXG4gICN0YWJsZWlkIHtcclxuICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbiAgICBiYWNrZ3JvdW5kOiAjRkZGRkZGO1xyXG4gICAgcGFkZGluZzogMjBweDsgXHJcbiAgIFxyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IGF1dG87ICBcclxuICB9XHJcblxyXG5cclxuXHJcbiNteVByb2dyZXNzIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2RkZDtcclxuICB9XHJcbiAgXHJcbiNteUJhciB7XHJcbiAgICB3aWR0aDogMSU7XHJcbiAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmY4MDAwO1xyXG4gIH1cclxuICBcclxuICAgIFxyXG4jYmFyYWxsIHtcclxuICBwYWRkaW5nLXRvcDoxMzBweDtcclxuIFxyXG59XHJcbiAgIFxyXG4jbWVudSB7XHJcbiAgcGFkZGluZy1ib3R0b206MHB4O1xyXG4gXHJcbn1cclxuLmxvYWRlciB7XHJcbiAgYm9yZGVyOiAxNnB4IHNvbGlkICNmM2YzZjM7XHJcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gIGJvcmRlci10b3A6IDE2cHggc29saWQgI2ZmODAwMDtcclxuICBtYXJnaW46IGF1dG87XHJcbiAgd2lkdGg6IDEyMHB4O1xyXG4gIGhlaWdodDogMTIwcHg7XHJcbiAgLXdlYmtpdC1hbmltYXRpb246IHNwaW4gMnMgbGluZWFyIGluZmluaXRlOyAvKiBTYWZhcmkgKi9cclxuICBhbmltYXRpb246IHNwaW4gMnMgbGluZWFyIGluZmluaXRlO1xyXG59XHJcblxyXG4vKiBTYWZhcmkgKi9cclxuQC13ZWJraXQta2V5ZnJhbWVzIHNwaW4ge1xyXG4gIDAlIHsgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKTsgfVxyXG4gIDEwMCUgeyAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDM2MGRlZyk7IH1cclxufVxyXG5cclxuQGtleWZyYW1lcyBzcGluIHtcclxuICAwJSB7IHRyYW5zZm9ybTogcm90YXRlKDBkZWcpOyB9XHJcbiAgMTAwJSB7IHRyYW5zZm9ybTogcm90YXRlKDM2MGRlZyk7IH1cclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/notification/notification.component.html":
/*!**********************************************************!*\
  !*** ./src/app/notification/notification.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<link  href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css\"  rel=\"stylesheet\">\n<div id=\"all\">\n \n  \n  <div class=\"home\">\n      <span id=\"home\" class=\"home\">Home  </span>\n      <input id=\"button1\" type=\"button\"  (click)=\"submitted=false\" (click)=\"goToPageMenu('/', 'main')\">\n    \n  </div>\n\n   <div class=\"analytic\">\n    <span id=\"analytic\" class=\"analytic\">Analytics  </span>\n    <input id=\"button2\" type=\"button\" alt=\"Submit\" width=\"48\" height=\"48\" (click)=\"submitted=false\" (click)=\"goToPageMenu('/', 'analytic')\">\n\n  </div>\n\n  <div class=\"notification\">\n      <span id=\"notification\" class=\"notification\">Calendar  </span>\n      <input id=\"button3\" type=\"button\" alt=\"Submit\" width=\"48\" height=\"48\" (click)=\"submitted=false\" (click)=\"goToPageMenu('/', 'calendar')\">\n  \n    </div>\n  \n    <div class=\"setting\">\n        <span id=\"setting\" class=\"setting\">Settings  </span>\n        <input id=\"button4\" type=\"button\" alt=\"Submit\" width=\"48\" height=\"48\" (click)=\"submitted=false\" (click)=\"goToPageMenu('/', 'settings')\">\n    \n      </div>\n \n   \n\n</div>\n\n<div id=\"maincontenthome\">\n  \n\n  \n\n  <div class=\"container\">\n    <!--  <ul class=\"list\">\n          <cdk-virtual-scroll-viewport  style=\"height: 500px\" itemSize=\"90\" >\n            <ng-container   *cdkVirtualFor=\"let n of data\" >\n                <h3 style=\"color:blue;\">{{n._id}}</h3>\n                <li> {{\n               \n                \n                  n.name\n                \n                }} </li>\n  \n            \n            </ng-container>\n          \n            \n          </cdk-virtual-scroll-viewport>\n        </ul>-->\n        <div id=\"menu\"> <th>Menu</th></div>\n\n        <div  id=\"barall\" class=\"row\">\n            <div class=\"col-md-12\">\n              <div  align=\"center\"   id=\"loading\"> Loading...</div>\n              <!--  <div id=\"myProgress\">\n                   \n                   <div id=\"myBar\"></div>\n               </div>-->\n\n               <div id=\"loader\" align=\"center\" class=\"loader\"></div>\n            \n            </div>\n          </div>\n      <table class=\"table table-striped\">\n        <thead>\n            <tr>\n            \n             \n       \n\n            </tr>\n        </thead>\n        <tbody id=\"tableid\">\n          <tr *ngFor =\"let d of data;  \" >\n              <!--<h6 style=\"color:blue;\">{{d.type}}</h6>-->\n          <!--  <td>{{d.name}} </td>\n            <td>{{d.type}}</td>\n          -->\n\n\n          \n            <div id=\"rcorners1\" class=\"container\">\n                <div class=\"row\">\n                    <div class=\"col-xs-12\">\n                        <td>Meal Recomendation</td>\n                    </div>\n                </div>\n              <div class=\"row\">\n                  <div class=\"col-xs-6\">\n                      <img src=\"https://getdigitalradio.com/wp-content/uploads/2017/09/tick-1.png\" alt=\"recommend\" width=\"40\" height=\"30\">\n                  </div>\n                  <div class=\"col-xs-6\">\n                      <h5 style=\"color:blue;\">{{d.type}}</h5>\n                    </div>\n                </div>\n                <div class=\"row\">\n                   \n                    <div class=\"col-xs-12\">\n                        <h6  style=\"color:black;\">{{d.date}}</h6>\n                    </div>\n  \n                </div>\n              <div class=\"row\">\n                <div class=\"col-xs-3\">\n                \n                  <button id=\"buttonround1\" class=\"tab-button\" (click)=\"onSelect(d.type)\">{{d.name}}</button>\n                </div>\n                <div class=\"col-xs-3\">\n                 \n                  <button id=\"buttonround1\" class=\"tab-button\" (click)=\"onSelect(d.type)\">{{d.name2}}</button>\n                </div>\n                <div class=\"col-xs-3\">\n                 \n                  <button id=\"buttonround1\" class=\"tab-button\" (click)=\"onSelect(d.type)\">{{d.name3}}</button>\n                </div>\n\n                <div class=\"col-xs-3\">\n                 \n                  <button id=\"buttonround1\" class=\"tab-button\" (click)=\"onSelect(d.type)\">{{d.name4}}</button>\n                </div>\n              </div>\n\n            </div>\n          </tr>\n        </tbody>\n      </table>\n    </div>\n    \n  </div>\n  "

/***/ }),

/***/ "./src/app/notification/notification.component.ts":
/*!********************************************************!*\
  !*** ./src/app/notification/notification.component.ts ***!
  \********************************************************/
/*! exports provided: NotificationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationComponent", function() { return NotificationComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_4__);





var NotificationComponent = /** @class */ (function () {
    function NotificationComponent(http, router) {
        this.http = http;
        this.router = router;
    }
    NotificationComponent.prototype.goToPageMenu = function (pagename, parameter) {
        this.router.navigate([pagename, parameter]);
    };
    NotificationComponent.prototype.onSelect = function (hero) {
        alert("called" + hero);
    };
    NotificationComponent.prototype.onSelect2 = function (hero) {
        alert("called2" + hero);
    };
    NotificationComponent.prototype.ngOnInit = function () {
        var _this = this;
        jquery__WEBPACK_IMPORTED_MODULE_4__("#session").hide();
        jquery__WEBPACK_IMPORTED_MODULE_4__("#date").hide();
        jquery__WEBPACK_IMPORTED_MODULE_4__("#back").show();
        function delay(ms) {
            return new Promise(function (resolve) { return setTimeout(resolve, ms); });
        }
        // alert("Cabled");
        //var width = 1;
        // alert("datieee are hier!");
        // alert(this.username);
        // alert(this.email);
        // alert(this.password);
        // alert(this.height);
        // alert(this.weight);
        /* var elem = document.getElementById("myBar");
          
         var id = setInterval(frame, 10);
         function frame() {
           //alert("frame");
           if (width >= 100) {
           //  alert("called");
             clearInterval(id);
            
            // alert("alles");
            
             
           } else {
             //alert("Kabled width functionarie");
             width++;
             elem.style.width = width + '%';
           }
         }*/
        //$("#menu").hide();
        (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var name;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, delay(1000)];
                    case 1:
                        _a.sent();
                        jquery__WEBPACK_IMPORTED_MODULE_4__("#barall").hide();
                        name = sessionStorage.getItem("lastname").toLowerCase();
                        this.http.get("http://localhost:3000/api/things/getcalendar2/" + name + "/monday", { responseType: "json" }).subscribe(
                        // this.http.get(`http://localhost:3000/api/things/getcalendar/Emily/${'tuesday'}`,{responseType:"json"}).subscribe(
                        function (response) {
                            _this.data = response;
                            // console.log("data :"+response);
                            // var sample=JSON.stringify(response);
                        });
                        return [2 /*return*/];
                }
            });
        }); })();
    };
    NotificationComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-notification',
            template: __webpack_require__(/*! ./notification.component.html */ "./src/app/notification/notification.component.html"),
            styles: [__webpack_require__(/*! ./notification.component.css */ "./src/app/notification/notification.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], NotificationComponent);
    return NotificationComponent;
}());



/***/ }),

/***/ "./src/app/settings/settings.component.css":
/*!*************************************************!*\
  !*** ./src/app/settings/settings.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#button1 {\r\n    background-color: #b2b2b2\t; \r\n    border: none;\r\n    color: white;\r\n    padding-bottom: 25px 22px;\r\n    text-align: center;\r\n    text-decoration: none;\r\n    display: inline-block;\r\n    font-size: 36px;\r\n    margin: 4px 18px;\r\n    cursor: pointer;\r\n    \r\n  }\r\n\r\n  \r\n#button2 {\r\n   background-color: #b2b2b2\t; \r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n\r\n  \r\n#button3 {\r\n   background-color: #b2b2b2\t; \r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n\r\n  \r\n#button4 {\r\n   background-color: #b2b2b2\t;\r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n\r\n  \r\n#buttona {\r\n    border-radius: 25px;\r\n    background-color: #b2b2b2\t; \r\n    border: none;\r\n    color: white;\r\n    padding-bottom: 25px 22px;\r\n    text-align: center;\r\n    text-decoration: none;\r\n    display: inline-block;\r\n    font-size: 36px;\r\n    margin: 4px 18px;\r\n    cursor: pointer;\r\n    \r\n  }\r\n\r\n  \r\n#buttonb {\r\n    border-radius: 25px;\r\n   background-color: #b2b2b2\t; \r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n\r\n  \r\n#buttonc {\r\n    border-radius: 25px;\r\n   background-color: #b2b2b2\t; \r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n\r\n  \r\n#buttond {\r\n    border-radius: 25px;\r\n   background-color: #b2b2b2\t;\r\n   border: none;\r\n   color: white;\r\n   padding-bottom: 25px 22px;\r\n   text-align: center;\r\n   text-decoration: none;\r\n   display: inline-block;\r\n   font-size: 36px;\r\n   margin: 4px 18px;\r\n   cursor: pointer;\r\n }\r\n\r\n  \r\n#buttone {\r\n    border-radius: 25px;\r\n    background-color: #b2b2b2\t;\r\n    border: none;\r\n    color: white;\r\n    padding-bottom: 25px 22px;\r\n    text-align: center;\r\n    text-decoration: none;\r\n    display: inline-block;\r\n    font-size: 36px;\r\n    margin: 4px 18px;\r\n    cursor: pointer;\r\n  }\r\n\r\n  \r\n#all{\r\n     text-align: center;\r\n     position: fixed;\r\n     left: 0;\r\n     bottom: 0;\r\n     width: 100%;\r\n     background-color: \t\t#b2b2b2;\r\n     color: white;\r\n     text-align: center;\r\n  }\r\n\r\n  \r\ndiv.home {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\n\r\n  \r\ndiv.home input {\r\n    background: url('home.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\n\r\n  \r\ndiv.analytic {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\n\r\n  \r\ndiv.analytic input {\r\n    background: url('analytic.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\n\r\n  \r\ndiv.notification {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\n\r\n  \r\ndiv.notification input {\r\n    background: url('outline-today-24px.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\n\r\n  \r\ndiv.setting {\r\n    position: relative;\r\n    display: inline-block;\r\n  }\r\n\r\n  \r\ndiv.setting input {\r\n    background: url('settings.svg') no-repeat;\r\n    cursor: pointer;\r\n    width: 50px;\r\n    height: 50px;\r\n    border: none;\r\n  }\r\n\r\n  \r\nspan.home {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 6px;\r\n  }\r\n\r\n  \r\nspan.analytic {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 2px;\r\n  }\r\n\r\n  \r\nspan.notification {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 2px;\r\n  }\r\n\r\n  \r\nspan.setting {\r\n    position: absolute;\r\n    top: 32px;\r\n    left: 12px;\r\n  }\r\n\r\n  \r\n#maincontenthome {\r\n   \r\n    background: #F0F0F0;\r\n    padding: 10px; \r\n    width: 100%;\r\n  \r\n    \r\n    height: 100%;  \r\n  }\r\n\r\n  \r\n#in {\r\n    border-radius: 25px;\r\n    background: #E8E8E8;\r\n   \r\n    width: 100%;\r\n  \r\n    \r\n    height: 100%;  \r\n  }\r\n\r\n  \r\nimg {\r\n    border-radius: 50%;\r\n    padding:15px;\r\n  }\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2V0dGluZ3Mvc2V0dGluZ3MuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLDBCQUEwQjtJQUMxQixZQUFZO0lBQ1osWUFBWTtJQUNaLHlCQUF5QjtJQUN6QixrQkFBa0I7SUFDbEIscUJBQXFCO0lBQ3JCLHFCQUFxQjtJQUNyQixlQUFlO0lBQ2YsZ0JBQWdCO0lBQ2hCLGVBQWU7O0VBRWpCOzs7QUFHRjtHQUNHLDBCQUEwQjtHQUMxQixZQUFZO0dBQ1osWUFBWTtHQUNaLHlCQUF5QjtHQUN6QixrQkFBa0I7R0FDbEIscUJBQXFCO0dBQ3JCLHFCQUFxQjtHQUNyQixlQUFlO0dBQ2YsZ0JBQWdCO0dBQ2hCLGVBQWU7Q0FDakI7OztBQUNBO0dBQ0UsMEJBQTBCO0dBQzFCLFlBQVk7R0FDWixZQUFZO0dBQ1oseUJBQXlCO0dBQ3pCLGtCQUFrQjtHQUNsQixxQkFBcUI7R0FDckIscUJBQXFCO0dBQ3JCLGVBQWU7R0FDZixnQkFBZ0I7R0FDaEIsZUFBZTtDQUNqQjs7O0FBRUE7R0FDRSwwQkFBMEI7R0FDMUIsWUFBWTtHQUNaLFlBQVk7R0FDWix5QkFBeUI7R0FDekIsa0JBQWtCO0dBQ2xCLHFCQUFxQjtHQUNyQixxQkFBcUI7R0FDckIsZUFBZTtHQUNmLGdCQUFnQjtHQUNoQixlQUFlO0NBQ2pCOzs7QUFLQTtJQUNHLG1CQUFtQjtJQUNuQiwwQkFBMEI7SUFDMUIsWUFBWTtJQUNaLFlBQVk7SUFDWix5QkFBeUI7SUFDekIsa0JBQWtCO0lBQ2xCLHFCQUFxQjtJQUNyQixxQkFBcUI7SUFDckIsZUFBZTtJQUNmLGdCQUFnQjtJQUNoQixlQUFlOztFQUVqQjs7O0FBR0Y7SUFDSSxtQkFBbUI7R0FDcEIsMEJBQTBCO0dBQzFCLFlBQVk7R0FDWixZQUFZO0dBQ1oseUJBQXlCO0dBQ3pCLGtCQUFrQjtHQUNsQixxQkFBcUI7R0FDckIscUJBQXFCO0dBQ3JCLGVBQWU7R0FDZixnQkFBZ0I7R0FDaEIsZUFBZTtDQUNqQjs7O0FBQ0E7SUFDRyxtQkFBbUI7R0FDcEIsMEJBQTBCO0dBQzFCLFlBQVk7R0FDWixZQUFZO0dBQ1oseUJBQXlCO0dBQ3pCLGtCQUFrQjtHQUNsQixxQkFBcUI7R0FDckIscUJBQXFCO0dBQ3JCLGVBQWU7R0FDZixnQkFBZ0I7R0FDaEIsZUFBZTtDQUNqQjs7O0FBRUE7SUFDRyxtQkFBbUI7R0FDcEIsMEJBQTBCO0dBQzFCLFlBQVk7R0FDWixZQUFZO0dBQ1oseUJBQXlCO0dBQ3pCLGtCQUFrQjtHQUNsQixxQkFBcUI7R0FDckIscUJBQXFCO0dBQ3JCLGVBQWU7R0FDZixnQkFBZ0I7R0FDaEIsZUFBZTtDQUNqQjs7O0FBQ0E7SUFDRyxtQkFBbUI7SUFDbkIsMEJBQTBCO0lBQzFCLFlBQVk7SUFDWixZQUFZO0lBQ1oseUJBQXlCO0lBQ3pCLGtCQUFrQjtJQUNsQixxQkFBcUI7SUFDckIscUJBQXFCO0lBQ3JCLGVBQWU7SUFDZixnQkFBZ0I7SUFDaEIsZUFBZTtFQUNqQjs7O0FBR0E7S0FDRyxrQkFBa0I7S0FDbEIsZUFBZTtLQUNmLE9BQU87S0FDUCxTQUFTO0tBQ1QsV0FBVztLQUNYLDJCQUEyQjtLQUMzQixZQUFZO0tBQ1osa0JBQWtCO0VBQ3JCOzs7QUFFQTtJQUNFLGtCQUFrQjtJQUNsQixxQkFBcUI7RUFDdkI7OztBQUVBO0lBQ0UscUNBQTJEO0lBQzNELGVBQWU7SUFDZixXQUFXO0lBQ1gsWUFBWTtJQUNaLFlBQVk7RUFDZDs7O0FBQ0E7SUFDRSxrQkFBa0I7SUFDbEIscUJBQXFCO0VBQ3ZCOzs7QUFFQTtJQUNFLHlDQUErRDtJQUMvRCxlQUFlO0lBQ2YsV0FBVztJQUNYLFlBQVk7SUFDWixZQUFZO0VBQ2Q7OztBQUVBO0lBQ0Usa0JBQWtCO0lBQ2xCLHFCQUFxQjtFQUN2Qjs7O0FBRUE7SUFDRSxtREFBMkU7SUFDM0UsZUFBZTtJQUNmLFdBQVc7SUFDWCxZQUFZO0lBQ1osWUFBWTtFQUNkOzs7QUFFQTtJQUNFLGtCQUFrQjtJQUNsQixxQkFBcUI7RUFDdkI7OztBQUVBO0lBQ0UseUNBQStEO0lBQy9ELGVBQWU7SUFDZixXQUFXO0lBQ1gsWUFBWTtJQUNaLFlBQVk7RUFDZDs7O0FBQ0E7SUFDRSxrQkFBa0I7SUFDbEIsU0FBUztJQUNULFNBQVM7RUFDWDs7O0FBRUE7SUFDRSxrQkFBa0I7SUFDbEIsU0FBUztJQUNULFNBQVM7RUFDWDs7O0FBRUE7SUFDRSxrQkFBa0I7SUFDbEIsU0FBUztJQUNULFNBQVM7RUFDWDs7O0FBRUE7SUFDRSxrQkFBa0I7SUFDbEIsU0FBUztJQUNULFVBQVU7RUFDWjs7O0FBRUE7O0lBRUUsbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixXQUFXOzs7SUFHWCxZQUFZO0VBQ2Q7OztBQUNBO0lBQ0UsbUJBQW1CO0lBQ25CLG1CQUFtQjs7SUFFbkIsV0FBVzs7O0lBR1gsWUFBWTtFQUNkOzs7QUFFQTtJQUNFLGtCQUFrQjtJQUNsQixZQUFZO0VBQ2QiLCJmaWxlIjoic3JjL2FwcC9zZXR0aW5ncy9zZXR0aW5ncy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiI2J1dHRvbjEge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2IyYjJiMlx0OyBcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIHBhZGRpbmctYm90dG9tOiAyNXB4IDIycHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBmb250LXNpemU6IDM2cHg7XHJcbiAgICBtYXJnaW46IDRweCAxOHB4O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgXHJcbiAgfVxyXG5cclxuICBcclxuI2J1dHRvbjIge1xyXG4gICBiYWNrZ3JvdW5kLWNvbG9yOiAjYjJiMmIyXHQ7IFxyXG4gICBib3JkZXI6IG5vbmU7XHJcbiAgIGNvbG9yOiB3aGl0ZTtcclxuICAgcGFkZGluZy1ib3R0b206IDI1cHggMjJweDtcclxuICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgZm9udC1zaXplOiAzNnB4O1xyXG4gICBtYXJnaW46IDRweCAxOHB4O1xyXG4gICBjdXJzb3I6IHBvaW50ZXI7XHJcbiB9XHJcbiAjYnV0dG9uMyB7XHJcbiAgIGJhY2tncm91bmQtY29sb3I6ICNiMmIyYjJcdDsgXHJcbiAgIGJvcmRlcjogbm9uZTtcclxuICAgY29sb3I6IHdoaXRlO1xyXG4gICBwYWRkaW5nLWJvdHRvbTogMjVweCAyMnB4O1xyXG4gICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICBmb250LXNpemU6IDM2cHg7XHJcbiAgIG1hcmdpbjogNHB4IDE4cHg7XHJcbiAgIGN1cnNvcjogcG9pbnRlcjtcclxuIH1cclxuXHJcbiAjYnV0dG9uNCB7XHJcbiAgIGJhY2tncm91bmQtY29sb3I6ICNiMmIyYjJcdDtcclxuICAgYm9yZGVyOiBub25lO1xyXG4gICBjb2xvcjogd2hpdGU7XHJcbiAgIHBhZGRpbmctYm90dG9tOiAyNXB4IDIycHg7XHJcbiAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgIGZvbnQtc2l6ZTogMzZweDtcclxuICAgbWFyZ2luOiA0cHggMThweDtcclxuICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gfVxyXG5cclxuXHJcblxyXG5cclxuICNidXR0b25hIHtcclxuICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjYjJiMmIyXHQ7IFxyXG4gICAgYm9yZGVyOiBub25lO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgcGFkZGluZy1ib3R0b206IDI1cHggMjJweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIGZvbnQtc2l6ZTogMzZweDtcclxuICAgIG1hcmdpbjogNHB4IDE4cHg7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICBcclxuICB9XHJcblxyXG4gIFxyXG4jYnV0dG9uYiB7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xyXG4gICBiYWNrZ3JvdW5kLWNvbG9yOiAjYjJiMmIyXHQ7IFxyXG4gICBib3JkZXI6IG5vbmU7XHJcbiAgIGNvbG9yOiB3aGl0ZTtcclxuICAgcGFkZGluZy1ib3R0b206IDI1cHggMjJweDtcclxuICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgZm9udC1zaXplOiAzNnB4O1xyXG4gICBtYXJnaW46IDRweCAxOHB4O1xyXG4gICBjdXJzb3I6IHBvaW50ZXI7XHJcbiB9XHJcbiAjYnV0dG9uYyB7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xyXG4gICBiYWNrZ3JvdW5kLWNvbG9yOiAjYjJiMmIyXHQ7IFxyXG4gICBib3JkZXI6IG5vbmU7XHJcbiAgIGNvbG9yOiB3aGl0ZTtcclxuICAgcGFkZGluZy1ib3R0b206IDI1cHggMjJweDtcclxuICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgZm9udC1zaXplOiAzNnB4O1xyXG4gICBtYXJnaW46IDRweCAxOHB4O1xyXG4gICBjdXJzb3I6IHBvaW50ZXI7XHJcbiB9XHJcblxyXG4gI2J1dHRvbmQge1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcclxuICAgYmFja2dyb3VuZC1jb2xvcjogI2IyYjJiMlx0O1xyXG4gICBib3JkZXI6IG5vbmU7XHJcbiAgIGNvbG9yOiB3aGl0ZTtcclxuICAgcGFkZGluZy1ib3R0b206IDI1cHggMjJweDtcclxuICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgZm9udC1zaXplOiAzNnB4O1xyXG4gICBtYXJnaW46IDRweCAxOHB4O1xyXG4gICBjdXJzb3I6IHBvaW50ZXI7XHJcbiB9XHJcbiAjYnV0dG9uZSB7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2IyYjJiMlx0O1xyXG4gICAgYm9yZGVyOiBub25lO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgcGFkZGluZy1ib3R0b206IDI1cHggMjJweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIGZvbnQtc2l6ZTogMzZweDtcclxuICAgIG1hcmdpbjogNHB4IDE4cHg7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgfVxyXG4gIFxyXG4gXHJcbiAgI2FsbHtcclxuICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgcG9zaXRpb246IGZpeGVkO1xyXG4gICAgIGxlZnQ6IDA7XHJcbiAgICAgYm90dG9tOiAwO1xyXG4gICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgIGJhY2tncm91bmQtY29sb3I6IFx0XHQjYjJiMmIyO1xyXG4gICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgfVxyXG5cclxuICBkaXYuaG9tZSB7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgfVxyXG5cclxuICBkaXYuaG9tZSBpbnB1dCB7XHJcbiAgICBiYWNrZ3JvdW5kOiB1cmwoJ3NyYy9hcHAvaGVhZGVyL2ltYWdlcy9ob21lLnN2ZycpIG5vLXJlcGVhdDtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIHdpZHRoOiA1MHB4O1xyXG4gICAgaGVpZ2h0OiA1MHB4O1xyXG4gICAgYm9yZGVyOiBub25lO1xyXG4gIH1cclxuICBkaXYuYW5hbHl0aWMge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIH1cclxuXHJcbiAgZGl2LmFuYWx5dGljIGlucHV0IHtcclxuICAgIGJhY2tncm91bmQ6IHVybCgnc3JjL2FwcC9oZWFkZXIvaW1hZ2VzL2FuYWx5dGljLnN2ZycpIG5vLXJlcGVhdDtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIHdpZHRoOiA1MHB4O1xyXG4gICAgaGVpZ2h0OiA1MHB4O1xyXG4gICAgYm9yZGVyOiBub25lO1xyXG4gIH1cclxuXHJcbiAgZGl2Lm5vdGlmaWNhdGlvbiB7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgfVxyXG5cclxuICBkaXYubm90aWZpY2F0aW9uIGlucHV0IHtcclxuICAgIGJhY2tncm91bmQ6IHVybCgnc3JjL2FwcC9jYWxlbmRhci9pbWFnZXMvb3V0bGluZS10b2RheS0yNHB4LnN2ZycpIG5vLXJlcGVhdDtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIHdpZHRoOiA1MHB4O1xyXG4gICAgaGVpZ2h0OiA1MHB4O1xyXG4gICAgYm9yZGVyOiBub25lO1xyXG4gIH1cclxuICBcclxuICBkaXYuc2V0dGluZyB7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgfVxyXG5cclxuICBkaXYuc2V0dGluZyBpbnB1dCB7XHJcbiAgICBiYWNrZ3JvdW5kOiB1cmwoJ3NyYy9hcHAvaGVhZGVyL2ltYWdlcy9zZXR0aW5ncy5zdmcnKSBuby1yZXBlYXQ7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICB3aWR0aDogNTBweDtcclxuICAgIGhlaWdodDogNTBweDtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICB9XHJcbiAgc3Bhbi5ob21lIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogMzJweDtcclxuICAgIGxlZnQ6IDZweDtcclxuICB9XHJcbiAgXHJcbiAgc3Bhbi5hbmFseXRpYyB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDMycHg7XHJcbiAgICBsZWZ0OiAycHg7XHJcbiAgfVxyXG5cclxuICBzcGFuLm5vdGlmaWNhdGlvbiB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDMycHg7XHJcbiAgICBsZWZ0OiAycHg7XHJcbiAgfVxyXG5cclxuICBzcGFuLnNldHRpbmcge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAzMnB4O1xyXG4gICAgbGVmdDogMTJweDtcclxuICB9XHJcblxyXG4gICNtYWluY29udGVudGhvbWUge1xyXG4gICBcclxuICAgIGJhY2tncm91bmQ6ICNGMEYwRjA7XHJcbiAgICBwYWRkaW5nOiAxMHB4OyBcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gIFxyXG4gICAgXHJcbiAgICBoZWlnaHQ6IDEwMCU7ICBcclxuICB9XHJcbiAgI2luIHtcclxuICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbiAgICBiYWNrZ3JvdW5kOiAjRThFOEU4O1xyXG4gICBcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gIFxyXG4gICAgXHJcbiAgICBoZWlnaHQ6IDEwMCU7ICBcclxuICB9XHJcblxyXG4gIGltZyB7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICBwYWRkaW5nOjE1cHg7XHJcbiAgfSJdfQ== */"

/***/ }),

/***/ "./src/app/settings/settings.component.html":
/*!**************************************************!*\
  !*** ./src/app/settings/settings.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"maincontenthome\">\n    <br>\n    <br>\n  <div > <h1 id=\"nameheader\"  align=\"center\">Fanis Alevizakis</h1> </div>\n  <br>\n  <br>\n  <br>\n  <div id=\"edit\"> <h4  align=\"center\">edit person</h4> </div>\n  <br>\n  <br>\n  <div class=\"container\" >\n      <div class=\"row\">\n  \n         <div id=\"thanks\" class=\"col-sm-12 \">\n            <h4  align=\"center\">Thanks lot</h4>\n           </div>\n        </div>\n    </div>\n\n\n\n  <div class=\"container\" >\n    <div class=\"row\">\n\n       <div class=\"col-sm-12 \">\n          <div align=\"center\" id=\"in\">\n            \n              <img align=\"left\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAMAAACahl6sAAAAM1BMVEUKME7///+El6bw8vQZPVlHZHpmfpHCy9Ojsbzg5ekpSmTR2N44V29XcYayvsd2i5yTpLFbvRYnAAAJcklEQVR4nO2d17arOgxFs+kkofz/154Qmg0uKsuQccddT/vhnOCJLclFMo+//4gedzcApf9B4srrusk+GsqPpj+ypq7zVE9LAdLWWVU+Hx69y2FMwAMGyfusLHwIpooyw9IAQfK+8naDp3OGHvZ0FMhrfPMgVnVjC2kABOQ1MLvi0DEIFj1ILu0LU2WjNRgtSF3pKb4qqtd9IHmjGlJHlc09IHlGcrQcPeUjTAySAGNSkQlRhCCJMGaUC0HSYUx6SmxFAtJDTdylsr4ApC1TY0yquKbCBkk7qnYVzPHFBHkBojhVJWviwgPJrsP4qBgTgbQXdsesjm4pDJDmIuswVZDdFx0ENTtkihoeqSDXD6tVxOFFBHndMKxWvUnzexpIcx/Gg2goJJDhVo6PCMGRAnKTmZuKm3wcJO/upphUqUHy29yVrRhJDORXOKIkEZDf4YiRhEF+iSNCEgb5KY4wSRDkB/yurUEG8nMcocgYABnvbrVL3nMIP0h/d5udKnwzSC/InfPdkJ6eWb0PJE++dyVVyQP5iQmWW27X5QG5druEKafBu0Hqu9saVOHa8HKC/K6BzHKZiRMEZCDF0Nd1/ZfXI/fcOibHOssFgokg9uFA20BhztHEAZIjIohrD/o1wljeFBDEwBo8YUt5Ir/rNLjOIACPFdy/AbEcPdcJBOCxytjeYAM4Kzp6rhOIPhRGNzwmFP3rOoTFI0irtnQKx6fj1Zt+h9njEUS9mKJxfFRrX5lt7wcQtaWTOfTHeIXVJQcQrRW+OYex2j0a66XZINoO8a7fPH2iHF2mC7ZBtB3Czb5QvjizSx7A3308mRzqAwujSywQbYfwc0iU8zqjS0yQ6ztEHX9332KCaGNIYB/Qq1z3yN0oDZBWyeFYJBCkm2sXLhDtpKFwNDMu5TnrZpYGiHbK4Nlwikg5DrYV1g6iPoJmzE5MKd/fOp53EPUaQZaLqH3u+vo2ELWp3wSyWuYGoj9EEIJoV3L9AUS/ZLsJpLNBXmqOu0CW6P5A/dx9IL0FAji/FYKot9EqE0Tvs6QBUe/2CxMEkZAlBNGPhdoAQWyTSmbxUwvUygwQyMmniAPgLt87CODXHuftWJIQgzrfQDC5AfwSgz9MmmG/gWCOqDgZ4JsQeTvZBoJJDhAFEsSDyxUEEUUekk0UEMhjBcEcGsoWVpBU3NcCgkkPkJWrKbdRZvULCMTWhYEdMrayBQRyqHcnSLmAIH7LcWJ8Hch7BsHEdWFpJsZjziCgFBpZ9TPm4e0XBJTTJKt9xjy8RoLI4gimPLP5goCSgWTrEcyzsy8IqmZVMo0H5bJiQToBCOjZ5RcElhjLN3dU7uQMAvoxwQkJZKI1CQzCthJYEigahHuDDi4rFwzCPQ7F1fiDQZgTR5iJwEGYRgIsiECD8BwwMAEfDcIaW8CRBQdhjS1kJQEchDEFhiRKr4KDFPS9FGQNVwEHoW83QjsEHdkfnuIOl6C1NjMItiaCaCWgbdpFJXQ9soh2uoB9aJcCxFdgZwlcrTmvENGlrITBBdpK25Qhd1F2RScq8CKu/gsCL8qN5THjy+Rr5E6joYgPxpdl518QrCf8Kpgjn6C8HLkbb+vt7ZM8wdVvy258khsRfHaS5DalDnlidZT7Erk+SXV5Bj1D3LS29XyhVJuoKHs9Q8S6reK11oUc7vPcr9uswP3SLiDINefXOF5rwCuGzVT6zVkVPfh2wWmHcz4wAwba2cgN1/Tsvleu7//i69CgVyt1GwjOs2+XK3rtbl151Tg3vOeioG40Mz2V+6pQ4xbJHOZj6g0EMxk93tV7fuedvVZpQSPhbwNBGInrymGrwNh1GXmL8F+lAaJ+NU/fzcmvJqvKj7177+1v1GY/GiBKI1Fdy/2XK6upXwaIJpI8B/399W0mH9zzafKaeCF9J0WF+jyCuFusTGzZKhFH8dVLZql2brxgcdVBKb7KG/7UZTmB3XJ6uL/QYT5ScRI74FcHEJ7feopyfGkaeaGlPoCw/BbjZmSBWIvINQNmTxdjWJqwUI8sztR4nYPuIPSTSUnOCZOE3ierqRoJfNSQxDjLEYs8i91eqgFCDSWiFHiuqAN9CwEGCPEISVjvwhS7Mfx6dtX8kC5aqvneGBOEFN2v6RBiYwr3DQOkLhEW6fHFbIwFQnkLiWYmZxE220z/aedPx99C+hiyKR4OzNFhg8S75CJTnxQ1dyugHTLaY10iu9dBpmhQtMz1ABLrkgtHVnRsPUO3OcU25i8cWdGxZbflCBKJqBdMs3aF/dYhNexU9RFcYEmLXYQKghyWdufyldBSU3KpjkKhZclxTXQGCTkL/HZDUIH5+Gkt4SgoCtj7pSYSNJLTK3VVRnmXZxebSMBIzmHABeIdXBebiN9eHYtUZ62ab3BdGkUm+SKJw1bdRXeewaX7qqdAnljg2sVxg3guAk3baofcg9yZ2eZpnHNvSFrEqhB9YPjesmt0pt6Xc8hl7W5L9Q4Xx09ctsrd5VhWeF6nF8SRrZdw49qns//0xTK/AZ8vGr3caTliuzeFNeCJTgafpKlhHd2WP1sy1LqDF798gjKJPLqDr9keoTd43+NyNzC1CI8Xy2lcPtOaVBI5IiAWyQ3e125AcKoXs2Djhy5eVc3KiBxREIPkhjBiLhIjU++4T91IbggjRiCJLSEIwWGddkEaxlVN5KCArPHk8mXVpHk8FHH7JL3n5dPA7C90q7XkeFJucacNmGXeRfswLE71HA79efaGiCN/Ofjmfmtcp8X10tIsqCacV5xfRWjNUiXGYbovWgyFYHcQLak15K9oM5zqmgaeKsHJetbSHfSPzXOiw/rxE9YH4CXaUpsZ0ztemFurP95Jpyvrd29YTpIZr7cEJHqfc7Wl0PFm2+yJR70udaokKFtGPTdm8WdQe24+HmVLlueboWQquBcYYVH2vEzfh8kCks1p90eWsLCyZ8qK7E86Oe+3XYFnBuiWdth20UqZR5SvMoyPg3WNauJipi0LMTQgVq5xUUlZcrPsopPHJ926z8pm7xyFLrH/PxpHSoXKdWgXsLn1scZn1ZDd/2vszN3lt254qkE+qu3yoqLM+ghN3Qz2qcVzUC/ZMFsK/alU6l0OWV/bQz6v6yYbyuN5BaZ4A7Y30vs/PPksS2+qzlvfF7OQmzzcL7W+xa7OIfRuVdtn/tdvdFLnL4OTKcm2W16PmWc4FWWXNSlWM2n3D+uPxuyrcfo74aP+Ac30a82+oLmfAAAAAElFTkSuQmCC\" alt=\"Avatar\" style=\"width:200px\">\n              \n              <div class=\"container\">\n            \n                  <div class=\"row\">\n                   \n                      <div class=\"col-sm-12 \">\n                          <div class=\"wrap-input100 validate-input\" >\n                              <input class=\"input100\" type=\"text\" name=\"username\" [(ngModel)]=\"username\" >\n                              <span class=\"focus-input100\" data-placeholder=\"username\" ></span>\n                            </div>\n                         \n                      </div>\n\n\n                      <div class=\"col-sm-12 \">\n                         \n                          <div class=\"wrap-input100 validate-input\" data-validate=\"Enter password\" >\n                              <span class=\"btn-show-pass\">\n                                <i class=\"zmdi zmdi-eye\"></i>\n                              </span>\n                              <input class=\"input100\" type=\"password\" name=\"password\" [(ngModel)]=\"password\" >\n                              <div align=\"right\" id=\"passworderror\"></div>\n                              <span class=\"focus-input100\" data-placeholder=\"password\"></span>\n                          </div>\n                             \n                      </div>\n\n                          \n\n                    <!--  <div class=\"col-sm-12 \">\n                          <div class=\"wrap-input100 validate-input\" >\n                              <input class=\"input100\" type=\"text\" name=\"age\" [(ngModel)]=\"age\" >\n                              <span class=\"focus-input100\" data-placeholder=\"age\"></span>\n                            </div>\n                         \n                      </div>-->\n                     \n                      <div class=\"wrap-input100 validate-input\" >\n                          <input class=\"input100\" type=\"text\" name=\"weight\" [(ngModel)]=\"weight\" >\n                          <span class=\"focus-input100\" data-placeholder=\"weight (kg)\"></span>\n                          <div align=\"right\" id=\"weighterror\"></div>\n                        </div>\n                       \n\n                   </div>\n                   <div class=\"col-sm-12 \">\n                     \n                    \n                    <button id=\"settingbutton\" color=\"blue\" class=\"login100-form-btn\"  (click)=\"submitted=false\" (click)=\"Submit()\"  >\n                       submit\n                    </button>\n                  </div>\n\n              \n                 \n                </div>\n              \n\n             \n                \n             \n           \n              </div>\n        </div>\n     \n   </div>\n\n  </div>\n\n  \n</div>\n\n\n\n<div id=\"all\">\n\n  \n  <div class=\"home\">\n      <span id=\"home\" class=\"home\">Home  </span>\n      <input id=\"button1\" type=\"button\"  (click)=\"submitted=false\" (click)=\"goToPageMenu('/', 'main')\">\n    \n  </div>\n\n   <div class=\"analytic\">\n    <span id=\"analytic\" class=\"analytic\">Analytics  </span>\n    <input id=\"button2\" type=\"button\" alt=\"Submit\" width=\"48\" height=\"48\" (click)=\"submitted=false\" (click)=\"goToPageMenu('/', 'analytic')\">\n\n  </div>\n\n  <div class=\"notification\">\n      <span id=\"notification\" class=\"notification\">Calendar  </span>\n      <input id=\"button3\" type=\"button\" alt=\"Submit\" width=\"48\" height=\"48\" (click)=\"submitted=false\" (click)=\"goToPageMenu('/', 'calendar')\">\n  \n    </div>\n  \n    <div class=\"setting\">\n        <span id=\"setting\" class=\"setting\">Settings  </span>\n        <input id=\"button4\" type=\"button\" alt=\"Submit\" width=\"48\" height=\"48\" (click)=\"submitted=false\" (click)=\"goToPageMenu('/', 'analytic')\">\n    \n      </div>\n \n   \n\n</div>"

/***/ }),

/***/ "./src/app/settings/settings.component.ts":
/*!************************************************!*\
  !*** ./src/app/settings/settings.component.ts ***!
  \************************************************/
/*! exports provided: SettingsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingsComponent", function() { return SettingsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _settings_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./settings.service */ "./src/app/settings/settings.service.ts");






var SettingsComponent = /** @class */ (function () {
    function SettingsComponent(http, router, SettingsService) {
        this.http = http;
        this.router = router;
        this.SettingsService = SettingsService;
    }
    SettingsComponent.prototype.goToPageMenu = function (pagename, parameter) {
        this.router.navigate([pagename, parameter]);
    };
    SettingsComponent.prototype.Submit = function () {
        // alert("weight is :"+this.weight);
        //alert(this.username+this.password+this.age+this.weight);
        if (this.password == '') {
            jquery__WEBPACK_IMPORTED_MODULE_4__("#passworderror").text("please correct required");
            jquery__WEBPACK_IMPORTED_MODULE_4__("#passworderror").css("color", "red");
            return;
        }
        else if (Number(this.weight) != parseInt(this.weight, 10)) {
            //  $("#weighterror").show();
            jquery__WEBPACK_IMPORTED_MODULE_4__("#weighterror").text("please correct required");
            jquery__WEBPACK_IMPORTED_MODULE_4__("#weighterror").css("color", "red");
            return;
        }
        else if (this.weight == '') {
            jquery__WEBPACK_IMPORTED_MODULE_4__("#weighterror").text("please correct required");
            jquery__WEBPACK_IMPORTED_MODULE_4__("#weighterror").css("color", "red");
            return;
        }
        jquery__WEBPACK_IMPORTED_MODULE_4__("#in").hide();
        jquery__WEBPACK_IMPORTED_MODULE_4__("#edit").hide();
        jquery__WEBPACK_IMPORTED_MODULE_4__("#thanks").show();
        this.SettingsService.updateUsers(this.username, this.password, this.age, this.weight).subscribe(function (res) {
            //   alert(this.username+this.password+this.age+this.weight);
            var flag = 0;
        }, function (error) {
            alert("error hier");
        });
    };
    SettingsComponent.prototype.ngOnInit = function () {
        // Check browser support
        if (typeof (Storage) !== "undefined") {
            // Store
            //sessionStorage.setItem("lastname", "Smith");
            // Retrieve
            document.getElementById("nameheader").innerHTML = sessionStorage.getItem("lastname");
        }
        else {
            document.getElementById("nameheader").innerHTML = "Sorry, your browser does not support Web Storage...";
        }
        jquery__WEBPACK_IMPORTED_MODULE_4__("#thanks").hide();
        document.getElementById("setting").style.backgroundColor = "#696969";
        document.getElementById("settingbutton").style.backgroundColor = "#696969";
        jquery__WEBPACK_IMPORTED_MODULE_4__("#session").hide();
        jquery__WEBPACK_IMPORTED_MODULE_4__("#back").show();
        jquery__WEBPACK_IMPORTED_MODULE_4__("#date").hide();
    };
    SettingsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-settings',
            template: __webpack_require__(/*! ./settings.component.html */ "./src/app/settings/settings.component.html"),
            styles: [__webpack_require__(/*! ./settings.component.css */ "./src/app/settings/settings.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _settings_service__WEBPACK_IMPORTED_MODULE_5__["SettingsService"]])
    ], SettingsComponent);
    return SettingsComponent;
}());



/***/ }),

/***/ "./src/app/settings/settings.service.ts":
/*!**********************************************!*\
  !*** ./src/app/settings/settings.service.ts ***!
  \**********************************************/
/*! exports provided: SettingsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingsService", function() { return SettingsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");



var SettingsService = /** @class */ (function () {
    function SettingsService(httpClient) {
        this.httpClient = httpClient;
        this.apiURL = 'http://127.0.0.1:3000/api/things/setusers/';
        this.url = 'http://localhost:3000/api/things/getusers/';
    }
    SettingsService.prototype.updateUsers = function (username, password, age, weight) {
        var data = {
            "name": "fr"
        };
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
            })
        };
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
            .set('username', String(username));
        var options = {
            httpOptions: httpOptions,
            withCredentials: false
        };
        return this.httpClient.post("" + this.apiURL + '/' + username + '/' + password + '/' + age + '/' + weight, null, options);
    };
    SettingsService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], SettingsService);
    return SettingsService;
}());



/***/ }),

/***/ "./src/app/signin/signin.component.css":
/*!*********************************************!*\
  !*** ./src/app/signin/signin.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#signin{\r\n    background-color: \t#303030;\r\n\r\n\r\n}\r\n\r\n\r\n#myProgress {\r\n    width: 100%;\r\n    background-color: #ddd;\r\n  }\r\n\r\n\r\n#myBar {\r\n    width: 1%;\r\n    height: 30px;\r\n    background-color: #ff8000;\r\n  }\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2lnbmluL3NpZ25pbi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksMEJBQTBCOzs7QUFHOUI7OztBQUdBO0lBQ0ksV0FBVztJQUNYLHNCQUFzQjtFQUN4Qjs7O0FBRUE7SUFDRSxTQUFTO0lBQ1QsWUFBWTtJQUNaLHlCQUF5QjtFQUMzQiIsImZpbGUiOiJzcmMvYXBwL3NpZ25pbi9zaWduaW4uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIiNzaWduaW57XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBcdCMzMDMwMzA7XHJcblxyXG5cclxufVxyXG5cclxuXHJcbiNteVByb2dyZXNzIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2RkZDtcclxuICB9XHJcbiAgXHJcbiAgI215QmFyIHtcclxuICAgIHdpZHRoOiAxJTtcclxuICAgIGhlaWdodDogMzBweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZjgwMDA7XHJcbiAgfSJdfQ== */"

/***/ }),

/***/ "./src/app/signin/signin.component.html":
/*!**********************************************!*\
  !*** ./src/app/signin/signin.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<!---->\n<!--\n<div class=\"container\">\n  <table class=\"table table-striped\">\n    <thead>\n        <tr>\n          <th>Actor Name</th>\n          <th>Character Name</th>\n         \n        </tr>\n    </thead>\n    <tbody>\n      <tr *ngFor =\"let d of data;\">\n        <td>{{d.name}}</td>\n        <td>{{d.info}}</td>\n        \n      </tr>\n    </tbody>\n  </table>\n</div>\n-->\n<div class=\"limiter\">\n  <div class=\"container-login100\">\n    <div class=\"wrap-login100\">\n      <form class=\"login100-form validate-form\" id=\"loadbar\">\n        <span class=\"login100-form-title p-b-26\">\n          Welcome\n        </span>\n        <span class=\"login100-form-title p-b-48\">\n          <i class=\"zmdi zmdi-font\"></i>\n        </span>\n\n        <div class=\"wrap-input100 validate-input\" >\n          <input class=\"input100\" type=\"text\" name=\"username\" [(ngModel)]=\"username\" >\n          <span class=\"focus-input100\" data-placeholder=\"Username\"></span>\n        </div>\n\n        <div class=\"wrap-input100 validate-input\" data-validate=\"Enter password\" >\n          <span class=\"btn-show-pass\">\n            <i class=\"zmdi zmdi-eye\"></i>\n          </span>\n          <input class=\"input100\" type=\"password\" name=\"pass\" [(ngModel)]=\"password\" >\n          <span class=\"focus-input100\" data-placeholder=\"Password\"></span>\n        </div>\n\n        <div class=\"container-login100-form-btn\">\n          <div class=\"wrap-login100-form-btn\">\n            <div class=\"login100-form-bgbtn\"></div>\n         \n            \n            \n            <button id=\"signin\" class=\"login100-form-btn\"  (click)=\"submitted=false\" (click)=\"submit2()\"  >\n              Login\n            </button>\n            \n          </div>\n\n\n         \n          \n        </div>\n\n        \n       \n\n        <div class=\"text-center p-t-115\">\n          <span class=\"txt1\">\n            Don’t have an account?\n          </span>\n          <a   onclick=\"myFunction()\" id=\"all\" routerLink=\"/signup\">Sign Up</a>\n          \n        </div>\n      </form>\n\n     <div id=\"loading\"> Loading...</div>\n      <div id=\"myProgress\">\n          \n          <div id=\"myBar\"></div>\n      </div>\n    </div>\n  </div>\n</div>\n\n\n<div id=\"dropDownSelect1\"></div>\n"

/***/ }),

/***/ "./src/app/signin/signin.component.ts":
/*!********************************************!*\
  !*** ./src/app/signin/signin.component.ts ***!
  \********************************************/
/*! exports provided: SigninComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SigninComponent", function() { return SigninComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _signin_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./signin.service */ "./src/app/signin/signin.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");






var SigninComponent = /** @class */ (function () {
    function SigninComponent(http, SigninService, router) {
        this.http = http;
        this.SigninService = SigninService;
        this.router = router;
    }
    ;
    SigninComponent.prototype.ngOnInit = function () {
        jquery__WEBPACK_IMPORTED_MODULE_4__('#main').hide();
        jquery__WEBPACK_IMPORTED_MODULE_4__('#myProgress').hide();
        jquery__WEBPACK_IMPORTED_MODULE_4__('#loading').hide();
        //loading
        /*   this.http.get('http://127.0.0.1:3000/api/things/',{responseType:"json"}).subscribe(
             response => {
               alert("cabled");
                 this.data = response;
                 console.log("data :"+response);
                 var sample=JSON.stringify(response);
            });*/
        //  loadbar
        /* this.SigninService.getCustomers("aa").subscribe((res)=>{
          // console.log("called");
           alert("Heys guysaa");
           //alert(JSON.parse(res));
           alert(JSON.stringify(res));
           var i=JSON.stringify(res);
           var stringify = JSON.parse(i);
           alert("Hier vars"+stringify.name);
           alert(stringify['name']);
         //  for (var si = 0; si < stringify.length; si++) {
            // alert("Alles gutten");
            // alert(stringify[si]['name']);
          // }
         //  alert("cabled"+res[0].info);
            // console.log(res);
     
            /*response => {
             alert("cabled");
               this.data = response;
               console.log("data :"+response);
               var sample=JSON.stringify(response);
          }*/
        /*}, (error) =>
        {
            alert("error hier");
           //alert(error);
            // if the request fails, this callback will be invoked
        });
    
    ///////////////////////////////////////////////
    
    
    
    this.SigninService.getCustomersB("aa").subscribe((res)=>{
       alert("Heys guysaa customera");
       var i=JSON.stringify(res);
       var stringify = JSON.parse(i);
    
       //var data = res.json();
       alert(stringify[0].name);
      
       
     
     }, (error) =>
     {
         alert("error hier");
        //alert(error);
         // if the request fails, this callback will be invoked
     });
    
    
    
    
    */
        //////////////////////////////////////////////////////
    };
    SigninComponent.prototype.submit2 = function (formData) {
        var _this = this;
        jquery__WEBPACK_IMPORTED_MODULE_4__('#loadbar').hide();
        jquery__WEBPACK_IMPORTED_MODULE_4__('#myProgress').show();
        jquery__WEBPACK_IMPORTED_MODULE_4__('#loading').show();
        function delay(ms) {
            return new Promise(function (resolve) { return setTimeout(resolve, ms); });
        }
        // alert("Username is"+this.username);
        //alert("Passowrd  is"+this.password);
        //  alert(this.username);
        var width = 1;
        this.SigninService.getLogin(this.username).subscribe(function (res) {
            var i = JSON.stringify(res);
            var stringify = JSON.parse(i);
            // alert(res);
            //alert("Heys guysaa hier is nikolas"+this.username);
            // alert(stringify[0]);
            if ((stringify[0]) != undefined || (String(stringify[0]) != null || (String(stringify[0]) != "undefined"))) {
                // alert("in string"+String(stringify[0]));
                if (String(stringify[0]) == "undefined") {
                    // alert("zero hier");
                    _this.goToPage("/", "error");
                    return;
                }
                if (String(stringify[0].username) === _this.username && String(stringify[0].password) === _this.password) {
                    // alert("Kablooo");
                    //  alert(width);
                    /*  if (typeof(Storage) !== "undefined") {
                        // Store
                        sessionStorage.setItem("lastname", String(stringify[0].username));
                        // Retrieve
                         document.getElementById("session").innerHTML = sessionStorage.getItem("lastname");
                      } else {
                         document.getElementById("session").innerHTML = "Sorry, your browser does not support Web Storage...";
                      }
                
                        (async () => {
                          await delay(2000);
                       
                          test();
                      })();*/
                    (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
                        return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                            switch (_a.label) {
                                case 0: return [4 /*yield*/, delay(2000)];
                                case 1:
                                    _a.sent();
                                    if (typeof (Storage) !== "undefined") {
                                        // Store
                                        sessionStorage.setItem("lastname", String(this.username));
                                        // Retrieve
                                        document.getElementById("session").innerHTML = sessionStorage.getItem("lastname");
                                    }
                                    else {
                                        document.getElementById("session").innerHTML = "Sorry, your browser does not support Web Storage...";
                                    }
                                    test();
                                    return [2 /*return*/];
                            }
                        });
                    }); })();
                }
                else {
                    (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
                        return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                            switch (_a.label) {
                                case 0: return [4 /*yield*/, delay(2000)];
                                case 1:
                                    _a.sent();
                                    //alert("Not in hier nouli");
                                    this.goToPage("/", "error");
                                    return [2 /*return*/];
                            }
                        });
                    }); })();
                }
            }
        }, function (error) {
            alert("error hier");
        });
        // }
        // alert("alles2");
        // this.goToPage('/', 'main');
        // this.goToPage('/', 'main');
        //alert("Called it hier1!");
        //alert(this.email);
        //  alert("Called submit");
        function test() {
            // alert("finalaa");
            //alert(location.host);
            //alert(location.hostname);
            // alert("concat");
            // alert(location.host.toString().concat("/main"));
            var i = location.host.toString().concat("/main").toString().trim();
            // alert(i);
            var all = "http://".concat(i);
            // alert("all are");
            //alert(all);
            window.location.href = all; // i;//'http://www.google.com';
        }
        var elem = document.getElementById("myBar");
        var id = setInterval(frame, 10);
        function frame() {
            //alert("frame");
            if (width >= 100) {
                //  alert("called");
                clearInterval(id);
                // alert("alles");
            }
            else {
                width++;
                elem.style.width = width + '%';
            }
        }
        this.SigninService.getCustomers(this.email).subscribe(function (res) {
            // alert("took");
            var flag = 0;
            // alert("continue");
            //alert(JSON.stringify(res));
            //console.log("called");
            //alert("Clled");
            //alert("cabled"+res[0].info);
            //  console.log(res);  
        }, function (error) {
            //alert("error hier");
            //  alert(error);
            // if the request fails, this callback will be invoked
        });
    };
    SigninComponent.prototype.goToPage = function (pagename, parameter) {
        this.router.navigate([pagename, parameter]);
    };
    SigninComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-signin',
            template: __webpack_require__(/*! ./signin.component.html */ "./src/app/signin/signin.component.html"),
            styles: [__webpack_require__(/*! ./signin.component.css */ "./src/app/signin/signin.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"], _signin_service__WEBPACK_IMPORTED_MODULE_2__["SigninService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], SigninComponent);
    return SigninComponent;
}());



/***/ }),

/***/ "./src/app/signin/signin.service.ts":
/*!******************************************!*\
  !*** ./src/app/signin/signin.service.ts ***!
  \******************************************/
/*! exports provided: SigninService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SigninService", function() { return SigninService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");



var SigninService = /** @class */ (function () {
    function SigninService(httpClient) {
        this.httpClient = httpClient;
        this.apiURL = 'http://127.0.0.1:3000/api/things/';
        this.url = 'http://localhost:3000/api/things/getuser/';
    }
    SigninService.prototype.getCustomers = function (input) {
        var data = {
            "name": "fr"
        };
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
            })
        };
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
            .set('name', String(input))
            .set('info', String(input));
        var options = {
            httpOptions: httpOptions,
            params: params,
            withCredentials: false
        };
        return this.httpClient.post("" + this.apiURL, null, options);
    };
    SigninService.prototype.getCustomersB = function (input) {
        var data = {
            "name": "fr"
        };
        return this.httpClient.get('http://localhost:3000/api/things/', { responseType: "json" });
    };
    SigninService.prototype.getLogin = function (input) {
        var data = {
            "name": "fr"
        };
        return this.httpClient.get("" + this.url + input, { responseType: "json" });
    };
    SigninService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], SigninService);
    return SigninService;
}());



/***/ }),

/***/ "./src/app/signup/signup.component.css":
/*!*********************************************!*\
  !*** ./src/app/signup/signup.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#signup{\r\n    background-color: \t#303030;\r\n\r\n\r\n}\r\n\r\n#myProgress {\r\n    width: 100%;\r\n    background-color: #ddd;\r\n  }\r\n\r\n#myBar {\r\n    width: 1%;\r\n    height: 30px;\r\n    background-color: #ff8000;\r\n  }\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2lnbnVwL3NpZ251cC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksMEJBQTBCOzs7QUFHOUI7O0FBRUE7SUFDSSxXQUFXO0lBQ1gsc0JBQXNCO0VBQ3hCOztBQUVGO0lBQ0ksU0FBUztJQUNULFlBQVk7SUFDWix5QkFBeUI7RUFDM0IiLCJmaWxlIjoic3JjL2FwcC9zaWdudXAvc2lnbnVwLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjc2lnbnVwe1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogXHQjMzAzMDMwO1xyXG5cclxuXHJcbn1cclxuXHJcbiNteVByb2dyZXNzIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2RkZDtcclxuICB9XHJcbiAgXHJcbiNteUJhciB7XHJcbiAgICB3aWR0aDogMSU7XHJcbiAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmY4MDAwO1xyXG4gIH0iXX0= */"

/***/ }),

/***/ "./src/app/signup/signup.component.html":
/*!**********************************************!*\
  !*** ./src/app/signup/signup.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\n\n<div class=\"limiter\" id=\"fanis\">\n  <div class=\"container-login100\">\n    <div class=\"wrap-login100\">\n      \n      <form class=\"login100-form validate-form\"  id=\"loadbar\">\n\n\n        \n        <span class=\"login100-form-title p-b-26\">\n          Welcome\n        </span>\n        <span class=\"login100-form-title p-b-48\">\n          <i class=\"zmdi zmdi-font\"></i>\n        </span>\n\n        <div class=\"wrap-input100 validate-input\" >\n            <input id=\"username\" class=\"input100\" type=\"text\"  name=\"username\" [(ngModel)]=\"username\" >\n            <span class=\"focus-input100\" data-placeholder=\"Username\"></span>\n            <div align=\"right\" id=\"usernameerror\"></div>\n        </div>\n  \n\n        <div  class=\"wrap-input100 validate-input\" data-validate = \"Valid email is: a@b.c\">\n          <input id=\"emailup\" class=\"input100\" type=\"text\" name=\"emailup\"  [(ngModel)]=\"email\"  >\n          <span  class=\"focus-input100\" data-placeholder=\"Email\"></span>\n          <div align=\"right\" id=\"emailerror\"></div>\n        </div>\n        \n        <div class=\"wrap-input100 validate-input\" data-validate=\"Enter password\">\n          <span class=\"btn-show-pass\">\n            <i class=\"zmdi zmdi-eye\"></i>\n          </span>\n          <input class=\"input100\" type=\"password\" name=\"password\"  [(ngModel)]=\"password\">\n          <span class=\"focus-input100\" data-placeholder=\"Password\"></span>\n          <div align=\"right\" id=\"passworderror\"></div>\n        </div>\n\n        \n\n        <div class=\"wrap-input100 validate-input\" >\n            <input class=\"input100\" type=\"text\" name=\"age\" [(ngModel)]=\"age\" >\n            <span class=\"focus-input100\" data-placeholder=\"weight\" data-placeholder=\"Age\"></span>\n            <div align=\"right\" id=\"ageerror\"></div>\n        </div>\n\n         <div class=\"wrap-input100 validate-input\" >\n          <input class=\"input100\" type=\"text\"  name=\"height\" [(ngModel)]=\"height\" >\n          <span class=\"focus-input100\" data-placeholder=\"height\" data-placeholder=\"Height(cm)\"></span>\n          <div align=\"right\" id=\"heighterror\"></div>\n         </div>\n\n        <div class=\"wrap-input100 validate-input\" >\n            <input class=\"input100\" type=\"text\" name=\"weight\" name=\"weight\" [(ngModel)]=\"weight\" >\n            <span class=\"focus-input100\" data-placeholder=\"weight\" data-placeholder=\"Weight(kg)\"></span>\n            <div align=\"right\" id=\"weighterror\"></div>\n        </div>\n\n        \n      \n        \n         \n        <input type=\"radio\" name=\"gender\" value=\"male\" [(ngModel)]=\"male\" checked > Male<br>\n        <input type=\"radio\" name=\"gender\" value=\"female\" [(ngModel)]=\"female\"> Female<br>\n\n      \n        \n        <div class=\"container-login100-form-btn\">\n          <div class=\"wrap-login100-form-btn\">\n            <div class=\"login100-form-bgbtn\"></div>\n            <button id=\"signup\" class=\"login100-form-btn\" (click)=\"submitted=false\" (click)=\"submit()\">\n              Register\n            </button>\n          </div>\n        </div>\n\n\n\n        <div class=\"text-center p-t-115\">\n          <span class=\"txt1\">\n            Don’t have an account?\n          </span>\n<a    routerLink=\"/signin\">Sign In</a>\n         \n        </div>\n      </form>\n\n      <div id=\"loading\"> Loading...</div>\n       <div id=\"myProgress\">\n          \n          <div id=\"myBar\"></div>\n       </div>\n\n\n    </div>\n  </div>\n</div>\n\n\n\n\n<div id=\"dropDownSelect1\"></div>\n\n"

/***/ }),

/***/ "./src/app/signup/signup.component.ts":
/*!********************************************!*\
  !*** ./src/app/signup/signup.component.ts ***!
  \********************************************/
/*! exports provided: SignupComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupComponent", function() { return SignupComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _signup__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./signup */ "./src/app/signup/signup.ts");
/* harmony import */ var _signup_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./signup.service */ "./src/app/signup/signup.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");







var SignupComponent = /** @class */ (function () {
    function SignupComponent(http, SignupService, router) {
        this.http = http;
        this.SignupService = SignupService;
        this.router = router;
    }
    SignupComponent.prototype.validateEmail = function (email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    };
    SignupComponent.prototype.ngOnInit = function () {
        jquery__WEBPACK_IMPORTED_MODULE_2__('#main').hide();
        // $('#main').hide();
        jquery__WEBPACK_IMPORTED_MODULE_2__('#myProgress').hide();
        jquery__WEBPACK_IMPORTED_MODULE_2__('#loading').hide();
        // usernameerror
        /*  $('#username').on('blur', function() {
            //alert( "Handler for .blur() called." );
          // alert(this.username)
            $("#usernameerror").text("username required");
            $("#usernameerror").css("color","red");
        });
           
            // alert("Emial validator is:"+this.validateEmail("fanis@gmail.com"));
            $('#emailup').on('blur', function() {
              //alert( "Handler for .blur() called." );
              $("#emailerror").text("Ops should inclu");
              $("#emailerror").css("color","red");
          });*/
        // $("#emailerror").text("Ops should inclu");
        // $("#emailerror").css("color","red");
    };
    SignupComponent.prototype.submit = function (formData) {
        var _this = this;
        //alert("val is"+this.validateEmail(this.email));
        if (this.username == '') {
            jquery__WEBPACK_IMPORTED_MODULE_2__("#usernameerror").text("username required");
            jquery__WEBPACK_IMPORTED_MODULE_2__("#usernameerror").css("color", "red");
            return;
        }
        if (this.validateEmail(this.email) == false) {
            //  alert("vali hier");
            jquery__WEBPACK_IMPORTED_MODULE_2__("#emailerror").text("email required");
            jquery__WEBPACK_IMPORTED_MODULE_2__("#emailerror").css("color", "red");
            return;
        }
        else if (this.validateEmail(this.email) == true) {
            jquery__WEBPACK_IMPORTED_MODULE_2__("#emailerror").hide();
        }
        if (this.password == '') {
            jquery__WEBPACK_IMPORTED_MODULE_2__("#passworderror").text("password required");
            jquery__WEBPACK_IMPORTED_MODULE_2__("#passworderror").css("color", "red");
            return;
        }
        else {
            jquery__WEBPACK_IMPORTED_MODULE_2__("#passworderror").hide();
        }
        if (Number(this.age) === parseInt(this.age, 10)) {
            jquery__WEBPACK_IMPORTED_MODULE_2__("#ageerror").hide();
            // return;
        }
        else {
            // ageerror
            jquery__WEBPACK_IMPORTED_MODULE_2__("#ageerror").text("please correct required");
            jquery__WEBPACK_IMPORTED_MODULE_2__("#ageerror").css("color", "red");
            //  alert("data is not an integer")
            return;
        }
        if (Number(this.age) < 0 || Number(this.age) > 100) {
            jquery__WEBPACK_IMPORTED_MODULE_2__("#ageerror").text("please correct required");
            jquery__WEBPACK_IMPORTED_MODULE_2__("#ageerror").css("color", "red");
            return;
        }
        if (Number(this.height) < 150 || Number(this.height) > 220) {
            jquery__WEBPACK_IMPORTED_MODULE_2__("#heighterror").text("please in cm required");
            jquery__WEBPACK_IMPORTED_MODULE_2__("#heighterror").css("color", "red");
            return;
        }
        else if (Number(this.height) > 150 || Number(this.height) < 220) {
            jquery__WEBPACK_IMPORTED_MODULE_2__("#heighterror").hide();
        }
        if (Number(this.weight) === parseInt(this.weight, 10)) {
            jquery__WEBPACK_IMPORTED_MODULE_2__("#weighterror").hide();
            // return;
        }
        else {
            // ageerror
            jquery__WEBPACK_IMPORTED_MODULE_2__("#weighterror").text("please correct required");
            jquery__WEBPACK_IMPORTED_MODULE_2__("#weighterror").css("color", "red");
            //  alert("data is not an integer")
            return;
        }
        /*else if(this.validateEmail(this.email)=="false"){
          alert("falsed");
          return;
    
          
        }*/
        jquery__WEBPACK_IMPORTED_MODULE_2__('#loadbar').hide();
        jquery__WEBPACK_IMPORTED_MODULE_2__('#myProgress').show();
        jquery__WEBPACK_IMPORTED_MODULE_2__('#loading').show();
        function delay(ms) {
            return new Promise(function (resolve) { return setTimeout(resolve, ms); });
        }
        var width = 1;
        // alert("datieee are hier!");
        // alert(this.username);
        // alert(this.email);
        // alert(this.password);
        // alert(this.height);
        // alert(this.weight);
        var elem = document.getElementById("myBar");
        var id = setInterval(frame, 10);
        function frame() {
            //alert("frame");
            if (width >= 100) {
                //  alert("called");
                clearInterval(id);
                // alert("alles");
            }
            else {
                //alert("Kabled width functionarie");
                width++;
                elem.style.width = width + '%';
            }
        }
        var gender = jquery__WEBPACK_IMPORTED_MODULE_2__("input[name='gender']:checked").val();
        // alert(gender);
        new _signup__WEBPACK_IMPORTED_MODULE_3__["Signup"]().setSignup(this.username, this.email, gender, this.password, this.weight, this.height);
        this.SignupService.setSignup(this.username, this.password, this.email, gender, this.height, this.weight, this.age).subscribe(function (res) {
            // alert("Heys guysaa");
            // alert(JSON.stringify(res));
            var i = JSON.stringify(res);
            var stringify = JSON.parse(i);
            //alert("Hier vars"+stringify.name);
            // alert(stringify['name']);
            (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
                return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, delay(2000)];
                        case 1:
                            _a.sent();
                            if (typeof (Storage) !== "undefined") {
                                // Store
                                sessionStorage.setItem("lastname", String(this.username));
                                // Retrieve
                                document.getElementById("session").innerHTML = sessionStorage.getItem("lastname");
                            }
                            else {
                                document.getElementById("session").innerHTML = "Sorry, your browser does not support Web Storage...";
                            }
                            test();
                            return [2 /*return*/];
                    }
                });
            }); })();
        }, function (error) {
            alert("error hier");
        });
        function test() {
            // alert("finalaa");
            //alert(location.host);
            //alert(location.hostname);
            // alert("concat");
            // alert(location.host.toString().concat("/main"));
            var i = location.host.toString().concat("/calendar").toString().trim();
            // alert(i);
            var all = "http://".concat(i);
            // alert("all are");
            //alert(all);
            window.location.href = all; // i;//'http://www.google.com';
        }
    };
    SignupComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-signup',
            template: __webpack_require__(/*! ./signup.component.html */ "./src/app/signup/signup.component.html"),
            styles: [__webpack_require__(/*! ./signup.component.css */ "./src/app/signup/signup.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClient"], _signup_service__WEBPACK_IMPORTED_MODULE_4__["SignupService"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]])
    ], SignupComponent);
    return SignupComponent;
}());



/***/ }),

/***/ "./src/app/signup/signup.service.ts":
/*!******************************************!*\
  !*** ./src/app/signup/signup.service.ts ***!
  \******************************************/
/*! exports provided: SignupService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupService", function() { return SignupService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");



var SignupService = /** @class */ (function () {
    function SignupService(httpClient) {
        this.httpClient = httpClient;
        this.apiURL = 'http://127.0.0.1:3000/api/things/';
    }
    SignupService.prototype.setSignup = function (username, password, email, gender, height, weight, age) {
        var data = {
            "name": "fr"
        };
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
            })
        };
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
            .set('username', String(username))
            .set('password', String(password))
            .set('email', String(email))
            .set('gender', String(gender))
            .set('age', String(age))
            .set('height', String(height))
            .set('weight', String(weight));
        var options = {
            httpOptions: httpOptions,
            params: params,
            withCredentials: false
        };
        return this.httpClient.post("" + this.apiURL, null, options);
    };
    SignupService.prototype.setLastHistory = function (username, password, email, gender, height, weight, age) {
        var data = {
            "name": "fr"
        };
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
            })
        };
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
            .set('username', String(username))
            .set('password', String(password))
            .set('email', String(email))
            .set('gender', String(gender))
            .set('age', String(age))
            .set('height', String(height))
            .set('weight', String(weight));
        var options = {
            httpOptions: httpOptions,
            params: params,
            withCredentials: false
        };
        return this.httpClient.post("" + this.apiURL, null, options);
    };
    SignupService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], SignupService);
    return SignupService;
}());



/***/ }),

/***/ "./src/app/signup/signup.ts":
/*!**********************************!*\
  !*** ./src/app/signup/signup.ts ***!
  \**********************************/
/*! exports provided: Signup */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Signup", function() { return Signup; });
var Signup = /** @class */ (function () {
    function Signup() {
    }
    Signup.prototype.setSignup = function (username, email, gender, password, weight, height) {
        this.username = username;
        this.email = email;
        this.gender = gender;
        this.password = password;
        this.weight = weight;
        this.height = height;
        //alert("In class");
        // alert(this.username);
        // alert(this.email);
        // alert(this.gender);
        // alert(this.password);
    };
    return Signup;
}());



/***/ }),

/***/ "./src/app/sockets/chat.service.ts":
/*!*****************************************!*\
  !*** ./src/app/sockets/chat.service.ts ***!
  \*****************************************/
/*! exports provided: ChatService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatService", function() { return ChatService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _websocket_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./websocket.service */ "./src/app/sockets/websocket.service.ts");



var ChatService = /** @class */ (function () {
    // Our constructor calls our wsService connect method
    function ChatService(wsService) {
        this.wsService = wsService;
        this.messages = wsService
            .connect()
            .map(function (response) {
            return response;
        });
    }
    // Our simplified interface for sending
    // messages back to our socket.io server
    ChatService.prototype.sendMsg = function (msg) {
        this.messages.next(msg);
    };
    ChatService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_websocket_service__WEBPACK_IMPORTED_MODULE_2__["WebsocketService"]])
    ], ChatService);
    return ChatService;
}());



/***/ }),

/***/ "./src/app/sockets/sockets.component.css":
/*!***********************************************!*\
  !*** ./src/app/sockets/sockets.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NvY2tldHMvc29ja2V0cy5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/sockets/sockets.component.html":
/*!************************************************!*\
  !*** ./src/app/sockets/sockets.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  sockets works!\n</p>\n\n\n\n<div class=\"limiter\">\n  <div class=\"container-login100\">\n    <div class=\"wrap-login100\">\n      <form class=\"login100-form validate-form\" id=\"loadbar\">\n        <span class=\"login100-form-title p-b-26\">\n          Welcome\n        </span>\n        <span class=\"login100-form-title p-b-48\">\n          <i class=\"zmdi zmdi-font\"></i>\n        </span>\n\n\n        <div class=\"container-login100-form-btn\">\n          <div class=\"wrap-login100-form-btn\">\n            <div class=\"login100-form-bgbtn\"></div>\n         \n            \n            \n            <button id=\"signin\" class=\"login100-form-btn\"  (click)=\"submitted=false\" (click)=\"sendMessage()\"  >\n              Login\n            </button>\n            \n          </div>\n\n\n         \n          \n        </div>\n\n        \n       \n\n        <div class=\"text-center p-t-115\">\n          <span class=\"txt1\">\n            Don’t have an account?\n          </span>\n          <a   onclick=\"myFunction()\" id=\"all\" routerLink=\"/signup\">Sign Up</a>\n          \n        </div>\n      </form>\n\n     <div id=\"loading\"> Loading...</div>\n      <div id=\"myProgress\">\n          \n          <div id=\"myBar\"></div>\n      </div>\n    </div>\n  </div>\n</div>\n\n\n<div id=\"dropDownSelect1\"></div>\n"

/***/ }),

/***/ "./src/app/sockets/sockets.component.ts":
/*!**********************************************!*\
  !*** ./src/app/sockets/sockets.component.ts ***!
  \**********************************************/
/*! exports provided: SocketsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SocketsComponent", function() { return SocketsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _chat_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./chat.service */ "./src/app/sockets/chat.service.ts");



var SocketsComponent = /** @class */ (function () {
    function SocketsComponent(chat) {
        this.chat = chat;
    }
    SocketsComponent.prototype.ngOnInit = function () {
        this.chat.messages.subscribe(function (msg) {
            console.log("Messages are:");
            console.log(msg.text);
        });
    };
    SocketsComponent.prototype.sendMessage = function () {
        alert("clicked send");
        this.chat.sendMsg("Test Message");
    };
    SocketsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-sockets',
            template: __webpack_require__(/*! ./sockets.component.html */ "./src/app/sockets/sockets.component.html"),
            styles: [__webpack_require__(/*! ./sockets.component.css */ "./src/app/sockets/sockets.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_chat_service__WEBPACK_IMPORTED_MODULE_2__["ChatService"]])
    ], SocketsComponent);
    return SocketsComponent;
}());



/***/ }),

/***/ "./src/app/sockets/websocket.service.ts":
/*!**********************************************!*\
  !*** ./src/app/sockets/websocket.service.ts ***!
  \**********************************************/
/*! exports provided: WebsocketService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WebsocketService", function() { return WebsocketService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var socket_io_client__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! socket.io-client */ "./node_modules/socket.io-client/lib/index.js");
/* harmony import */ var socket_io_client__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(socket_io_client__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var rxjs_Observable__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/Observable */ "./node_modules/rxjs-compat/_esm5/Observable.js");
/* harmony import */ var rxjs_Rx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/Rx */ "./node_modules/rxjs-compat/_esm5/Rx.js");





var WebsocketService = /** @class */ (function () {
    function WebsocketService() {
    }
    WebsocketService.prototype.connect = function () {
        var _this = this;
        // If you aren't familiar with environment variables then
        // you can hard code `environment.ws_url` as `http://localhost:5000`
        this.socket = socket_io_client__WEBPACK_IMPORTED_MODULE_2__('http://localhost:5000/');
        // We define our observable which will observe any incoming messages
        // from our socket.io server.
        var observable = new rxjs_Observable__WEBPACK_IMPORTED_MODULE_3__["Observable"](function (observer) {
            _this.socket.on('message', function (data) {
                console.log("Received message from Websocket Server");
                observer.next(data);
            });
            return function () {
                _this.socket.disconnect();
            };
        });
        // We define our Observer which will listen to messages
        // from our other components and send messages back to our
        // socket server whenever the `next()` method is called.
        var observer = {
            next: function (data) {
                _this.socket.emit('message', JSON.stringify(data));
            },
        };
        // we return our Rx.Subject which is a combination
        // of both an observer and observable.
        return rxjs_Rx__WEBPACK_IMPORTED_MODULE_4__["Subject"].create(observer, observable);
    };
    WebsocketService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], WebsocketService);
    return WebsocketService;
}());



/***/ }),

/***/ "./src/app/start/start.component.css":
/*!*******************************************!*\
  !*** ./src/app/start/start.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3N0YXJ0L3N0YXJ0LmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/start/start.component.html":
/*!********************************************!*\
  !*** ./src/app/start/start.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  start works!\n</p>\n"

/***/ }),

/***/ "./src/app/start/start.component.ts":
/*!******************************************!*\
  !*** ./src/app/start/start.component.ts ***!
  \******************************************/
/*! exports provided: StartComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StartComponent", function() { return StartComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var StartComponent = /** @class */ (function () {
    function StartComponent() {
    }
    StartComponent.prototype.ngOnInit = function () {
    };
    StartComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-start',
            template: __webpack_require__(/*! ./start.component.html */ "./src/app/start/start.component.html"),
            styles: [__webpack_require__(/*! ./start.component.css */ "./src/app/start/start.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], StartComponent);
    return StartComponent;
}());



/***/ }),

/***/ "./src/app/submit-form/submit-form.component.css":
/*!*******************************************************!*\
  !*** ./src/app/submit-form/submit-form.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\npre {margin: 45px 0 60px;}\r\nh1 {margin: 60px 0 60px 0;}\r\np {margin-bottom: 10px;}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc3VibWl0LWZvcm0vc3VibWl0LWZvcm0uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQ0EsS0FBSyxtQkFBbUIsQ0FBQztBQUN6QixJQUFJLHFCQUFxQixDQUFDO0FBQzFCLEdBQUcsbUJBQW1CLENBQUMiLCJmaWxlIjoic3JjL2FwcC9zdWJtaXQtZm9ybS9zdWJtaXQtZm9ybS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXHJcbnByZSB7bWFyZ2luOiA0NXB4IDAgNjBweDt9XHJcbmgxIHttYXJnaW46IDYwcHggMCA2MHB4IDA7fVxyXG5wIHttYXJnaW4tYm90dG9tOiAxMHB4O31cclxuIl19 */"

/***/ }),

/***/ "./src/app/submit-form/submit-form.component.html":
/*!********************************************************!*\
  !*** ./src/app/submit-form/submit-form.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<link  href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css\"  rel=\"stylesheet\">\n<div class=\"container\">\n    <div class=\"site-index\">\n        <div class=\"body-content\">\n\n            <div class=\"row\">\n                <div class=\"col-md-12\">\n                    <h1>Default setting of wizard</h1>\n                    <form id=\"wizard_example\" action=\"\">\n                        <fieldset>\n                            <legend>Basic information</legend>\n                            <div class=\"row\">\n                                <div class=\"col-lg-6\">\n                                    <div class=\"form-group\">\n                                        <label for=\"exampleInputEmail1\">Email address</label>\n                                        <input type=\"email\" class=\"form-control\" id=\"exampleInputEmail1\"\n                                               name=\"exampleInputEmail1\" placeholder=\"Enter email\">\n                                    </div>\n                                    <div class=\"form-group\">\n                                        <label for=\"exampleInputPassword1\">Password</label>\n                                        <input type=\"password\" class=\"form-control\" id=\"exampleInputPassword1\"\n                                               name=\"exampleInputPassword1\" placeholder=\"Password\">\n                                    </div>\n                                    <div class=\"form-group\">\n                                        <label for=\"exampleInputPassword1\">Password again</label>\n                                        <input type=\"password\" class=\"form-control\" id=\"exampleInputPasswordAgain1\"\n                                               name=\"exampleInputPasswordAgain1\" placeholder=\"Password\">\n                                    </div>\n                                </div>\n                                <div class=\"col-lg-6\">\n                                    <div class=\"form-group\">\n                                        <label for=\"exampleInputPassword1\">Favorite number</label>\n                                        <select class=\"form-control\" name=\"favoriteNumber\">\n                                            <option value=\"1\">1</option>\n                                            <option value=\"2\">2</option>\n                                            <option value=\"3\">3</option>\n                                            <option value=\"4\">4</option>\n                                            <option value=\"5\">5</option>\n                                            <option value=\"6\">6</option>\n                                            <option value=\"7\">7</option>\n                                            <option value=\"8\">8</option>\n                                            <option value=\"9\">9</option>\n                                            <option value=\"10\">10</option>\n                                        </select>\n                                    </div>\n                                    <div class=\"form-group\">\n                                        <label>Own animals</label>\n\n                                        <div class=\"checkbox\">\n                                            <label>\n                                                <input type=\"checkbox\" name=\"animal[]\" value=\"goat\" data-sf-text=\"Koza\"> Goat\n                                            </label>\n                                        </div>\n                                        <div class=\"checkbox\">\n                                            <label>\n                                                <input type=\"checkbox\" name=\"animal[]\" value=\"cow\"> Cow\n                                            </label>\n                                        </div>\n                                        <div class=\"checkbox\">\n                                            <label>\n                                                <input type=\"checkbox\" name=\"animal[]\" value=\"rooster\" data-sf-text=\"Kohout\"> Rooster\n                                            </label>\n                                        </div>\n                                        <div class=\"checkbox\">\n                                            <label>\n                                                <input type=\"checkbox\" name=\"animal[]\" value=\"crocodile\"> Crocodile\n                                            </label>\n                                        </div>\n                                    </div>\n                                </div>\n                                <div class=\"col-lg-12\">\n                                    In publishing and graphic design, lorem ipsum is common placeholder text used to\n                                    demonstrate the graphic elements of a document or visual presentation, such as web\n                                    pages, typography, and graphical layout. It is a form of \"greeking\".\n                                </div>\n                            </div>\n                        </fieldset>\n                        <fieldset>\n                            <legend>Condition</legend>\n                            In publishing and graphic design, lorem ipsum is common placeholder text used to demonstrate\n                            the graphic elements of a document or visual presentation, such as web pages, typography,\n                            and graphical layout. It is a form of \"greeking\".\n                            Even though using \"lorem ipsum\" often arouses curiosity due to its resemblance to classical\n                            Latin, it is not intended to have meaning. Where text is visible in a document, people tend\n                            to focus on the textual content rather than upon overall presentation, so publishers use\n                            lorem ipsum when displaying a typeface or design in order to direct the focus to\n                            presentation. \"Lorem ipsum\" also approximates a typical distribution of letters in English.\n                            <div class=\"radio\">\n                                <label>\n                                    <input type=\"radio\" name=\"optionsRadios\" value=\"option1\" checked>\n                                    Yes, it is totaly right.\n                                </label>\n                            </div>\n                            <div class=\"radio\">\n                                <label>\n                                    <input type=\"radio\" name=\"optionsRadios\" value=\"option2\">\n                                    No, I check it twice and it is not right.\n                                </label>\n                            </div>\n                        </fieldset>\n                        <fieldset>\n                            <legend>Final step</legend>\n                            <div class=\"row\">\n                                <div class=\"col-lg-12\">\n                                    <p>\n                                        Even though using \"lorem ipsum\" often arouses curiosity due to its resemblance\n                                        to classical Latin, it is not intended to have meaning. Where text is visible in\n                                        a document, people tend to focus on the textual content rather than upon overall\n                                        presentation, so publishers use lorem ipsum when displaying a typeface or design\n                                        in order to direct the focus to presentation. \"Lorem ipsum\" also approximates a\n                                        typical distribution of letters in English.\n                                    </p>\n                                </div>\n                                <div class=\"col-lg-12\">\n                                    <div class=\"row\">\n                                        <div class=\"col-lg-6\">\n                                            <div class=\"form-group\">\n                                                <label class=\"sr-only\" for=\"exampleInputName1\">Your name</label>\n                                                <input type=\"text\" class=\"form-control\" id=\"exampleInputName1\"\n                                                       name=\"exampleInputName1\" placeholder=\"Your name\">\n                                            </div>\n                                            <div class=\"form-group\">\n                                                <label class=\"sr-only\" for=\"exampleInputCat1\">Name of your cat</label>\n                                                <input type=\"text\" class=\"form-control\" id=\"exampleInputCat1\"\n                                                       name=\"exampleInputCat1\" placeholder=\"Name of your cat\">\n                                            </div>\n                                        </div>\n                                        <div class=\"col-lg-6\">\n                                            <div class=\"form-group\">\n                                                <label class=\"sr-only\" for=\"exampleInputHam1\"></label>\n                                                <input type=\"text\" class=\"form-control\" id=\"exampleInputHam1\"\n                                                       name=\"exampleInputHam1\" placeholder=\"Name of your hamster\">\n                                            </div>\n                                        </div>\n                                        <div class=\"col-lg-6\">\n                                            <div class=\"form-group\">\n                                                <label class=\"sr-only\" for=\"exampleInputGrocer1\"></label>\n                                                <input type=\"text\" class=\"form-control\" id=\"exampleInputGrocer1\"\n                                                       name=\"exampleInputGrocer1\"\n                                                       placeholder=\"Name of your grocery seller\">\n                                            </div>\n                                        </div>\n                                    </div>\n                                </div>\n                                <div class=\"col-lg-12\">\n                                    <div class=\"row\">\n                                        <div class=\"col-lg-6\">\n                                            <div class=\"radio\">\n                                                <label>\n                                                    <input type=\"radio\" name=\"optionsRadios2\" value=\"option3\" checked>\n                                                    Option one is this and that&mdash;be sure to include why it's great\n                                                </label>\n                                            </div>\n                                            <div class=\"radio\">\n                                                <label>\n                                                    <input type=\"radio\" name=\"optionsRadios2\" value=\"option4\">\n                                                    Option two can be something else and selecting it will deselect\n                                                    option one\n                                                </label>\n                                            </div>\n                                        </div>\n                                        <div class=\"col-lg-6\">\n                                            <div class=\"checkbox\">\n                                                <label>\n                                                    <input type=\"checkbox\" name=\"superPower\"> I want have super-power\n                                                </label>\n                                            </div>\n                                            <div class=\"checkbox\">\n                                                <label>\n                                                    <input type=\"checkbox\" name=\"moreSuperPower\"> I have one or more\n                                                    super-power already\n                                                </label>\n                                            </div>\n                                        </div>\n                                    </div>\n                                </div>\n                                <div class=\"col-lg-12\">\n                                    <p>\n                                        Even though using \"lorem ipsum\" often arouses curiosity due to its resemblance\n                                        to classical Latin, it is not intended to have meaning. Where text is visible in\n                                        a document, people tend to focus on the textual content rather than upon overall\n                                        presentation, so publishers use lorem ipsum when displaying a typeface or design\n                                        in order to direct the focus to presentation. \"Lorem ipsum\" also approximates a\n                                        typical distribution of letters in English.\n                                    </p>\n\n                                    <p>\n                                        Using \"lorem ipsum\" often arouses curiosity due to its resemblance to classical\n                                        Latin, it is not intended to have meaning. Where text is visible in a document,\n                                        people tend to focus on the textual content rather than upon overall\n                                        presentation, so publishers use lorem ipsum when displaying a typeface or design\n                                        in order to direct the focus to presentation. \"Lorem ipsum\" also approximates a\n                                        typical distribution of letters in English.\n                                    </p>\n                                </div>\n                                <noscript>\n                                    <input class=\"nocsript-finish-btn sf-right nocsript-sf-btn\" type=\"submit\"\n                                           name=\"no-js-clicked\" value=\"finish\"/>\n                                </noscript>\n                            </div>\n                        </fieldset>\n                    </form>\n                </div>\n            </div>\n           \n           \n\n            <footer class=\"footer\">\n                <div class=\"container\">\n                    <p class=\"pull-left\">© ajoke.cz/wizard 2015</p>\n                </div>\n            </footer>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/submit-form/submit-form.component.ts":
/*!******************************************************!*\
  !*** ./src/app/submit-form/submit-form.component.ts ***!
  \******************************************************/
/*! exports provided: SubmitFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SubmitFormComponent", function() { return SubmitFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var SubmitFormComponent = /** @class */ (function () {
    function SubmitFormComponent() {
    }
    SubmitFormComponent.prototype.ngOnInit = function () {
    };
    SubmitFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-submit-form',
            template: __webpack_require__(/*! ./submit-form.component.html */ "./src/app/submit-form/submit-form.component.html"),
            styles: [__webpack_require__(/*! ./submit-form.component.css */ "./src/app/submit-form/submit-form.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], SubmitFormComponent);
    return SubmitFormComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! G:\GymPlanManager\src\front-end\src\main.ts */"./src/main.ts");


/***/ }),

/***/ 1:
/*!********************!*\
  !*** ws (ignored) ***!
  \********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map