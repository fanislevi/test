var mongoose = require("../model/connection")
var MerchantinfoSchema = new mongoose.Schema({
    username: String,
    email:String,
    image: String
  });
  const merchants = mongoose.model('Merchant', MerchantinfoSchema);
  module.exports =  merchants;
