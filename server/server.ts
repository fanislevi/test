/*
command line commands
show dbs
use my
show collections
db.userinfos.find().limit(10)

*/
var http = require('http'),
    fs = require('fs');
const bodyParser = require('body-parser')
var express = require('express');
var app     = express();
var Router = require('express');
var port    = 	process.env.PORT || 8080;
let router = Router();
var path = require('path');
var session = require('express-session');
var controller = require('./api/controller/controller');
let ThingController= require( './api/controller/controller');
app.use(express.static(path.join(__dirname, '/')));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(session({secret: 'ssshhhhh'}));
app.use(bodyParser.json());
//router.get('/test', ThingController.index);
var server = app.listen(3000).on('error', console.log);
ThingController.routes(app);  
module.exports = router;

