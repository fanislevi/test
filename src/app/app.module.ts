import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SigninComponent } from './signin/signin.component';
import { SignupComponent } from './signup/signup.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule }   from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { NgCircleProgressModule } from 'ng-circle-progress';
import { PushNotificationsModule } from 'ng-push'; //import the module
import { ScrollingModule } from '@angular/cdk/scrolling';
import { PushNotificationService } from 'ngx-push-notifications';
import { CommonModule } from '@angular/common';
import { NgxEditorModule } from 'ngx-editor';
import { ZXingScannerModule } from '@zxing/ngx-scanner';
import { SocketIoModule, SocketIoConfig } from 'ng6-socket-io';
import { NgxQRCodeModule } from 'ngx-qrcode2';
const config: SocketIoConfig = { url: 'http://localhost:8988', options: {} };
@NgModule({
  declarations: [
    AppComponent,
    SigninComponent,
    SignupComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule, ScrollingModule,
    PushNotificationsModule ,// add it to imports
    CommonModule,
    NgxQRCodeModule,
    ZXingScannerModule,
     NgCircleProgressModule.forRoot({
      radius: 100,
      outerStrokeWidth: 16,
      innerStrokeWidth: 8,
      outerStrokeColor: "#78C000",
      innerStrokeColor: "#C7E596",
      animationDuration: 300, 
    }),
    NgxEditorModule,
    SocketIoModule.forRoot(config)
  ],
  providers: [PushNotificationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
