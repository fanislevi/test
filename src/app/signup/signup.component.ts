import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import * as $ from 'jquery';
import { Signup } from './signup';
import { SignupService } from './signup.service';
import { Router } from '@angular/router';
import {  HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  constructor(private http: HttpClient,private SignupService: SignupService,private router: Router) { }
   validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}
  ngOnInit() {
    $('#main').hide();


   // $('#main').hide();
    $('#myProgress').hide();
    $('#loading').hide();


   // usernameerror



  /*  $('#username').on('blur', function() {
      //alert( "Handler for .blur() called." );
    // alert(this.username)
      $("#usernameerror").text("username required");
      $("#usernameerror").css("color","red");
  });
     
      // alert("Emial validator is:"+this.validateEmail("fanis@gmail.com"));
      $('#emailup').on('blur', function() {
        //alert( "Handler for .blur() called." );
        $("#emailerror").text("Ops should inclu");
        $("#emailerror").css("color","red");
    });*/
       
   // $("#emailerror").text("Ops should inclu");
   // $("#emailerror").css("color","red");
  }

  username: string;
  email:string;
  password:string;
  height:string;
  age:string;
  weight:string;
  male:string;
  
  
  submit(formData) {
//alert("val is"+this.validateEmail(this.email));
    if(this.username==''){
      $("#usernameerror").text("username required");
      $("#usernameerror").css("color","red");
      return;

    }
    if(this.validateEmail(this.email)==false){
    //  alert("vali hier");
      $("#emailerror").text("email required");
      $("#emailerror").css("color","red");
      return;
    }else if(this.validateEmail(this.email)==true){
      $("#emailerror").hide();

    }

    if(this.password==''){
      $("#passworderror").text("password required");
      $("#passworderror").css("color","red");
      return;
    }else{
      $("#passworderror").hide();

    }

    if (Number(this.age) === parseInt(this.age, 10)){
      $("#ageerror").hide();
      // return;
     } else{
     // ageerror

      $("#ageerror").text("please correct required");
      $("#ageerror").css("color","red");
     //  alert("data is not an integer")
       return;
     }
     if(Number(this.age)<0||Number(this.age)>100){
      $("#ageerror").text("please correct required");
      $("#ageerror").css("color","red");
      return;
     }

     if(Number(this.height)<150||Number(this.height)>220){
      $("#heighterror").text("please in cm required");
      $("#heighterror").css("color","red");
      return;

     }else if(Number(this.height)>150||Number(this.height)<220){
      $("#heighterror").hide();
     }


     if (Number(this.weight) === parseInt(this.weight, 10)){
      $("#weighterror").hide();
      // return;
     } else{
     // ageerror

      $("#weighterror").text("please correct required");
      $("#weighterror").css("color","red");
     //  alert("data is not an integer")
       return;
     }
    $('#loadbar').hide();
    $('#myProgress').show();
    $('#loading').show();
    function delay(ms: number) {
      return new Promise( resolve => setTimeout(resolve, ms) );
  }
    var width = 1;
    var elem = document.getElementById("myBar");   
    var id = setInterval(frame, 10);
    function frame() {
      if (width >= 100) {
        clearInterval(id);
      } else {
        width++; 
        elem.style.width = width + '%'; 
      }
    }
    var gender = $("input[name='gender']:checked").val();
     new Signup().setSignup(this.username,this.email,gender,this.password,this.weight,this.height);
     this.SignupService.setSignup(this.username,this.password,this.email,gender,this.height,this.weight,this.age).subscribe((res)=>{
       alert("Heys guysaa");
       alert(JSON.stringify(res));
       var i=JSON.stringify(res);
       var stringify = JSON.parse(i);
       //alert("Hier vars"+stringify.name);
      // alert(stringify['name']);

       (async () => { 
        await delay(2000);
        if (typeof(Storage) !== "undefined") {
          // Store
          sessionStorage.setItem("lastname", String(this.username));
          // Retrieve
           document.getElementById("session").innerHTML = sessionStorage.getItem("lastname");
        } else {
           document.getElementById("session").innerHTML = "Sorry, your browser does not support Web Storage...";
        }
  
        test();
    })();
     
     }, (error) =>
     {
         alert("error hier"+error.message);
        
     });
     
     function test(){
       var i=location.host.toString().concat("/calendar").toString().trim();
       var all="http://".concat(i);
       window.location.href =all;
     
     }
    }


    

}
