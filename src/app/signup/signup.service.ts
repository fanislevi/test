import { Injectable } from '@angular/core';
import {  HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class SignupService {
  apiURL: string = 'http://192.168.1.3:3000/api/things/signup';
  constructor(private httpClient: HttpClient) {


   }


   public setSignup(username:String,password:String,email: String,gender:String,height:String,weight: String,age:String){
    const data = {
	
      "name": "fr"
   }
  const httpOptions = {
    headers: new HttpHeaders({
     
      'Content-Type': 'application/json',
      
    
    })
  }
  const params = new HttpParams()
  .set('username',  String(username))
  .set('password', String(password))
  .set('email', String(email))
  .set('gender',  String(gender))
  .set('age',  String(age))
  .set('height', String(height))
  .set('weight', String(weight))

  const options = {
    httpOptions,
    params,
    withCredentials: false
  };

  return this.httpClient.post(`${this.apiURL}`,null,options);
   

  
  }



  public setLastHistory(username:String,password:String,email: String,gender:String,height:String,weight: String,age:String){
    const data = {
	
      "name": "fr"
   }
  const httpOptions = {
    headers: new HttpHeaders({
     
      'Content-Type': 'application/json',
      
    
    })
  }
  const params = new HttpParams()
  .set('username',  String(username))
  .set('password', String(password))
  .set('email', String(email))
  .set('gender',  String(gender))
  .set('age',  String(age))
  .set('height', String(height))
  .set('weight', String(weight))

  const options = {
    httpOptions,
    params,
    withCredentials: false
  };

  return this.httpClient.post(`${this.apiURL}`,null,options);
   

  
  }


}
